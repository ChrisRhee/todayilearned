package com.ssafy.user;
public class CustomerNameLengthException extends Exception {

	public CustomerNameLengthException(String message) {
		super(message);
	}
}
