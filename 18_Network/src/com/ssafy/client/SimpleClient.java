package com.ssafy.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import com.ssafy.user.Customer;
import com.ssafy.user.CustomerNameLengthException;

public class SimpleClient {
	public SimpleClient() {
		//2 ip랑 port 필요함 
		try {
			Socket socket = new Socket("127.0.0.1", 8888);
			//Socket socket = new Socket("127.0.0.1", 8888);
			System.out.println("서버 오케이!!!!!!!!!!!!!!");
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			
			//메세지를 받았다 그럼 내입장에서?
			System.out.println(ois.readObject());
			
			//client -> server에 여러번 메세지를 줄거야
			new Thread() {
				public void run() {
					ObjectOutputStream oos;
					try {
						oos = new ObjectOutputStream(socket.getOutputStream());
						oos.writeObject(new Customer("홍길동길"));
						oos.flush();
						//oos.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch(CustomerNameLengthException e){
						e.printStackTrace();
					}
					
				};
			}.start();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	public static void main(String[] args) {
		new SimpleClient();
	}
}
