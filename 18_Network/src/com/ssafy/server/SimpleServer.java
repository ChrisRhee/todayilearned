package com.ssafy.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class SimpleServer {
	public SimpleServer() {
		// 1서버는 대기해야한다.
		try {
			ServerSocket server = new ServerSocket(8888);
			while(true) {
				System.out.println("1서버 대기중...ㅠㅠㅠㅠ");
				//3번 소켓을 받아서 리턴했다는 의미는? 서버에서 클라이언트를 인증ㅇㅋ
				Socket socket = server.accept();
				System.out.println("Client 인증 ㅇㅋㅇㅋㅇㅋ");
				// server -> client greeting message
				ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
				oos.writeObject("어서오너라 이자시가");
				oos.flush();
				//oos.close();
				
				ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
				while(true) {
					System.out.println(ois.readObject());
				}
				
			} //while
		} catch (IOException e) {
			e.printStackTrace();
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		new SimpleServer();
	}
}
