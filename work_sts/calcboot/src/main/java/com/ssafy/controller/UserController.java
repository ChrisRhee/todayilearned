package com.ssafy.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ssafy.mvc.dto.User;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins= {"*"},maxAge=6000)
@RestController
@RequestMapping("/api")
@Api(value="SSAFY")
public class UserController {

	@GetMapping("test")
	public User test() {
		return new User(123, "홍길동", "서울시");
	}
	@ApiOperation(value="모든 유저 조회",response=List.class)
	@GetMapping("/users")
	public ResponseEntity<List<User>> getusers() {
		List<User> users = new ArrayList<>();
		users.add(new User(123, "홍길동", "서울시"));
		users.add(new User(125, "손오공", "부산시"));
		return new ResponseEntity<List<User>>(users,HttpStatus.OK);
	}

	@ApiOperation(value="주어진 UID로 유저 조회",response=User.class)
	@GetMapping("/users/{uid}")
	public ResponseEntity<User> getuser(@PathVariable int uid) {
		User user = null;
		if (uid == 123) {
			user = new User(123, "홍길동", "서울시");
		} else {
			user = new User();
		}
		return new ResponseEntity<User>(user,HttpStatus.OK);
	}
	
	@ApiOperation(value="유저 추가")
	@PostMapping("users")
	public ResponseEntity<Void> users(@RequestBody User user){
		System.out.println("POST: "+user);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
//	@PutMapping("users/{uid}") 해당 uid 업데이트하게 할 수 있음, 패스베리어블 필요
	@ApiOperation(value="유저 변경")
	@PutMapping("users")
	public ResponseEntity<Void> putusers(@RequestBody User user){
		System.out.println("PUT: "+user);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@ApiOperation(value="유저 삭제")
	@DeleteMapping("users/{uid}")
	public ResponseEntity<Void> delusers(@PathVariable int uid){
		System.out.println("DELETE: "+uid);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
