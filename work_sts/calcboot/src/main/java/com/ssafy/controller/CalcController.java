package com.ssafy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ssafy.mvc.dto.Calc;

@Controller
@RequestMapping("my")
public class CalcController {

//	@RequestMapping(value = "calc.do", method = RequestMethod.GET)
	@GetMapping("calc.do")
	public String calc() {
		return "my/calc";
	}
	
//	@RequestMapping(value = "calc.do", method = RequestMethod.POST)
	//public String calc(String nm,int n1,String op,int n2,Model m)
	@PostMapping("calc.do")
	public String calc(@ModelAttribute("c") Calc calc,Model m) {
		calc.runCalc();
		System.out.println(calc);
		return "my/calcresult";
	}
}
