package com.ssafy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalcbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalcbootApplication.class, args);
	}

}
