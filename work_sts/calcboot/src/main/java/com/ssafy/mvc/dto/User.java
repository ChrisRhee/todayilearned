package com.ssafy.mvc.dto;

public class User {
	private int uid;
	private String name;
	private String addr;

	public User() {
	}

	public User(int uid, String name, String addr) {
		setUid(uid);
		setName(name);
		setAddr(addr);
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	@Override
	public String toString() {
		return "User [uid=" + uid + ", name=" + name + ", addr=" + addr + "]";
	}

}
