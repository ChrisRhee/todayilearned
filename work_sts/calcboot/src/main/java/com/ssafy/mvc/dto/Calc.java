package com.ssafy.mvc.dto;

public class Calc {
	private String nm;
	private int n1;
	private String op;
	private int n2;
	private int rr;

	public Calc() {
	}

	public Calc(String nm, int n1, String op, int n2) {
		this(nm, n1, op, n2, 0);
	}

	public Calc(String nm, int n1, String op, int n2, int rr) {
		setNm(nm);
		setN1(n1);
		setOp(op);
		setN2(n2);
		setRr(rr);
	}

	public void runCalc() {
		switch (op) {
		case "+":
			rr = n1 + n2;
			break;
		case "-":
			rr = n1 - n2;
			break;
		}
	}

	public String getNm() {
		return nm;
	}

	public void setNm(String nm) {
		this.nm = nm;
	}

	public int getN1() {
		return n1;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}

	public int getN2() {
		return n2;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public int getRr() {
		return rr;
	}

	public void setRr(int rr) {
		this.rr = rr;
	}

	@Override
	public String toString() {
		return "Calc [nm=" + nm + ", n1=" + n1 + ", op=" + op + ", n2=" + n2 + ", rr=" + rr + "]";
	}

}
