<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Calc Result Page</title>
</head>
<body>
<h1>계산 결과 페이지</h1>
<p>
이름: ${c.nm}<br>
계산: ${c.n1} ${c.op} ${c.n2} = ${c.rr}
<p>
<a href="/">메인으로</a>
</body>
</html>