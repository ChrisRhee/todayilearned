package com.ssafy.di.calc;

import com.ssafy.di.log.Log;

public class CalcImpl implements Calc {
	private Log log;
	
//	public CalcImpl(Log lll) {
//		this.log = lll;
//	}
//	
//	public CalcImpl() {
//		System.out.println("CalcImpl()");
//	}
	
	public void setLog(Log log) {
		this.log = log;
	}
	
	@Override
	public int plus(int i, int j) {
		log.info("plus: i="+i+", j="+j);
		//if(true) throw new NullPointerException("널포인터 예외발생ㅋ");
		return i+j;
	}

	@Override
	public int minus(int i, int j) {
		log.info("minus: i="+i+", j="+j);
		return i-j;
	}

}
