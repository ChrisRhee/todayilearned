package com.ssafy.di.calc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CalcMain {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		Calc bean = (Calc) ctx.getBean("myCalc");
		System.out.println(bean.plus(20, 10));
		//System.out.println(bean.minus(5, 10));
	}
}
