package com.ssafy.aop;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

public class LogAspect {
	public void myLog(JoinPoint jp) {
		System.out.println("실행전 : myLog()");
		Object[] args= jp.getArgs();
		System.out.println("인자수 : "+args.length);
		System.out.println(Arrays.toString(args));
		String ls = jp.getSignature().toLongString();
		System.out.println("롱인자 : "+ls);
		String ss = jp.getSignature().toShortString();
		System.out.println("숏인자 : "+ss);
	}
	
	public void after(JoinPoint jp) {
		System.out.println("실행후 : after()");
	}
	
	public Object around(ProceedingJoinPoint pjp) throws Throwable{
		System.out.println("실행전전 : around() " + Arrays.toString(pjp.getArgs()));
		Object o = pjp.proceed();
		System.out.println("실행후후 : around() " + o);
		return o;
	}
	
	public void afterReturning(JoinPoint jp, Object re) {
		String name = jp.getSignature().toShortString();
		System.out.println("실행 후 정상 : afterReturning() name=["+name+"] re=["+re+"]");
	}
	
	public void afterThrowing(JoinPoint jp, Throwable ex) {
		String name = jp.getSignature().toShortString();
		System.out.println("실행 후 예외 : afterThrowing() name=["+name+"] re=["+ex+"]");
	}
}
