drop table if exists employee;

create table employee (
  num    int primary key,
  name   varchar(20) not null,
  salary  int
);

insert into employee values (101, '홍길동', 1000);
insert into employee values (102, '손오공', 2000);
insert into employee values (103, '홍길순', 3000);
insert into employee values (104, '사오정', 4000);
insert into employee values (105, '저팔계', 5000);

commit;

select * from employee;
