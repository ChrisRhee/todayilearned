package com.ssafy.mybatis;

public class Employee {
	private int num;
	private String name;
	private int salary;
	
	public Employee() {
		this(0, "", 0);
	}
	
	public Employee(int num, String name, int salary) {
		super();
		setNum(num);
		setName(name);
		setSalary(salary);
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Employee [num=");
		builder.append(getNum());
		builder.append(", name=");
		builder.append(getName());
		builder.append(", salary=");
		builder.append(getSalary());
		builder.append("]");
		return builder.toString();
	}
	
	
}
