package com.ssafy.mybatis;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class MybatisMain {
	public static void main(String[] args) {
		SqlSession session  = null;
		try {
			String resource = "com/ssafy/mybatis/mybatis.xml";
			InputStream inputStream = Resources.getResourceAsStream(resource);
			SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
			session = sqlSessionFactory.openSession();
			
			//session.insert("emp.insert", new Employee(106,"하하하",6000));
			
//			Employee e = session.selectOne("emp.select", 106);
//			System.out.println(e);
//			System.out.println();
			
//			Map<String, Integer> map = new HashMap<String, Integer>();
//			map.put("num", 101);
//			map.put("salary", 1111);
//			session.update("emp.update",map);
//			
			List<Employee> list = session.selectList("emp.selectAll");
			for (Employee emp : list) System.out.println(emp);
//			
			session.delete("emp.delete",0);
			
			session.commit();
			
		} catch (Exception e) {
			if(session != null) session.rollback();
			e.printStackTrace();
		}finally {
			if(session != null) session.close();
		}
	}
}
