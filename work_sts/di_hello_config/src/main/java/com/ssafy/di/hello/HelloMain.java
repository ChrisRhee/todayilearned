package com.ssafy.di.hello;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import com.ssafy.config.ApplicationConfig;

public class HelloMain {
	public static void main(String[] args) {
//		ApplicationContext ctx = new FileSystemXmlApplicationContext("src/main/resources/applicationContext.xml");
//		ApplicationContext ctx = new GenericXmlApplicationContext("classpath:applicationContext.xml");
		ApplicationContext ctx = new AnnotationConfigApplicationContext(ApplicationConfig.class);
		//Hello bean = ctx.getBean(Hello.class);
		Hello bean = (Hello) ctx.getBean("myHello");
		//Hello bean = (Hello) ctx.getBean("myHello", Hello.class);
		bean.sayHello("이찬호");
		
	}
}
