package com.ssafy.di.log;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ssafy.config.ApplicationConfig;

public class LogMain {
	public static void main(String[] args) {
		//ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		ApplicationContext ctx = new AnnotationConfigApplicationContext(ApplicationConfig.class);
		Log bean= (Log) ctx.getBean("MyLog");
		Log bean2= (Log) ctx.getBean("DbLog");
		bean.info("로구당");
		bean2.info("로구당");
		
		Log ml = new MyLog();
		Log dl = new DbLog();
		ml.info("죠습니다");
		dl.info("죠습니다");
	}
}
