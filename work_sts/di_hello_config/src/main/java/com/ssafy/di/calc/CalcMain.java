package com.ssafy.di.calc;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ssafy.config.ApplicationConfig;

public class CalcMain {
	public static void main(String[] args) {
//		BeanFactory ctx = new AnnotationConfigApplicationContext(ApplicationConfig.class);
		ApplicationContext ctx = new AnnotationConfigApplicationContext(ApplicationConfig.class);
		
		Calc bean = (Calc) ctx.getBean("myCalc");
		System.out.println(bean.plus(5, 10));
		System.out.println(bean.minus(5, 10));
	}
}
