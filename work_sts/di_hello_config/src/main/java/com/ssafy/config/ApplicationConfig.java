package com.ssafy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ssafy.di.calc.Calc;
import com.ssafy.di.calc.CalcImpl;
import com.ssafy.di.hello.Hello;
import com.ssafy.di.hello.HelloImpl;
import com.ssafy.di.log.DbLog;
import com.ssafy.di.log.Log;
import com.ssafy.di.log.MyLog;

@Configuration
public class ApplicationConfig {
	@Bean
	public Hello myHello() {
		return new HelloImpl();
	}
	
	@Bean
	public Log MyLog() {
		return new MyLog();
	}
	
	@Bean
	public Log DbLog() {
		return new DbLog();
	}
	
	@Bean
	public Calc myCalc() {
		CalcImpl calc = new CalcImpl();
		calc.setLog(new MyLog());
		return calc;
	}
	
	
	
}
