package test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class test {
	public static void main(String[] args) {
		
		Comparable<String> c = new Comparable<String>() {
			public int compareTo(String s) {
				System.out.print("c " +s+" ");
				return 123;
			}
		};
		
		Comparable<String> c2 = (String s)->{
				System.out.print("c2 " +s+" ");
				return 123;
		};
		
		Comparable<String> c3 = (s)->{
			System.out.print("c2 " +s+" ");
			return 123;
		};
		
		Comparable<String> c4 = s-> 100+Integer.parseInt(s);	
		System.out.println("c4 "+ c4.compareTo("23"));
		
		
		
		
		List<String> a = new ArrayList<>();
		a.add("이찬호");
		a.add("박찬호");
		a.add("원찬호");
		System.out.println(a);
		
		Collections.sort(a, (String o1, String o2)->{
			return o1.compareTo(o2);
		});
		
		System.out.println(a);
		
		System.out.println(c.compareTo("홍길동"));
		System.out.println(c2.compareTo("홍길동"));
		
		/*
		new Thread(new Runnable(){
			public void run() {
				System.out.println("Runnuble.run(1)");
			}
		}).start();
		
		new Thread(()->{
				System.out.println("Runnuble.run(2)");
		}).start();
		
		new Thread(()-> System.out.println("Runnuble.run(3)") ).start();
		*/
		System.out.println("end main");
	}
}
