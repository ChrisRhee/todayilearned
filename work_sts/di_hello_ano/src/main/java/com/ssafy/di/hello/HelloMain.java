package com.ssafy.di.hello;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class HelloMain {
	public static void main(String[] args) {
//		ApplicationContext ctx = new FileSystemXmlApplicationContext("src/main/resources/applicationContext.xml");
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
//		ApplicationContext ctx = new GenericXmlApplicationContext("classpath:applicationContext.xml");
		//Hello bean = ctx.getBean(Hello.class);
		Hello bean = (Hello) ctx.getBean("myHello");
//		Hello bean = (Hello) ctx.getBean("myHello", Hello.class);
		bean.sayHello("이찬호");
		
	}
}
