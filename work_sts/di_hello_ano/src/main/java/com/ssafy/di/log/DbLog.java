package com.ssafy.di.log;

import org.springframework.stereotype.Repository;

@Repository("dbLog")
public class DbLog implements Log {
	@Override
	public void info(String log) {
		System.out.println("DB 로그: " + log);
	}
}
