package com.ssafy.di.hello;

import org.springframework.stereotype.Component;

@Component ("myHello")
public class HelloImpl implements Hello {

	public void onCreate() {
		System.out.println("onCreate()");
	}
	
	public void onDestroy() {
		System.out.println("onDestroy()");
	}
	
	@Override
	public void sayHello(String name) {
		System.out.println("Hi~, "+name);
	}
	
}
