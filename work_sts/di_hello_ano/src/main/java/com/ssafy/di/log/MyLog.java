package com.ssafy.di.log;

import org.springframework.stereotype.Repository;

@Repository("myLog")
public class MyLog implements Log {
	@Override
	public void info(String log) {
		System.out.println("마이 로그: " + log);
	}
}
