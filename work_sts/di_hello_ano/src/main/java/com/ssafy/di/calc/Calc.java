package com.ssafy.di.calc;

public interface Calc {
	public int plus(int i, int j);
	public int minus(int i, int j);
}
