package com.ssafy.di.log;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class LogMain {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		Log bean= (Log) ctx.getBean("myLog");
		Log bean2= (Log) ctx.getBean("dbLog");
		bean.info("로구당");
		bean2.info("로구당");
		
	}
}
