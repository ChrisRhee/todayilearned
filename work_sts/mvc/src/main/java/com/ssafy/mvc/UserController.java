package com.ssafy.mvc;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ssafy.mvc.dto.User;

@RestController
public class UserController {
	@GetMapping("test")
	public User test() {
		return new User(123,"홍길동","서울시");
	}
	
	@GetMapping("users")
	public Map<String, Object> users(){
		User[] users = {new User(123,"홍길동","서울시"), new User(123,"손오공","서울시")};
		Map<String, Object> map = new HashMap<>();
		map.put("result", Boolean.TRUE);
		map.put("data", users);
		return map;
	}
	
	@GetMapping("users/{uid}")
	public User user(@PathVariable int uid){
		if(uid==123) {
			return new User(123,"홍길동","서울시");
		}
		else {
			return null;
		}
	}
	
	
	@PostMapping("users")
	public Map<String, Object> users(@RequestBody User user){
		System.out.println("POST: "+user);
		Map<String, Object> map = new HashMap<>();
		map.put("result", Boolean.TRUE);
		map.put("data", user);
		return map;
	}
	
	@PutMapping("users")
	public Map<String, Object> putusers(@RequestBody User user){
		System.out.println("PUT: "+user);
		Map<String, Object> map = new HashMap<>();
		map.put("result", Boolean.TRUE);
		map.put("data", user);
		return map;
	}
	
	@DeleteMapping("users/{uid}")
	public Map<String, Object> delusers(@RequestBody int uid){
		System.out.println("DELETE: "+uid);
		
		Map<String, Object> map = new HashMap<>();
		map.put("result", Boolean.TRUE);
		map.put("data", new User(123,"홍길동","서울시"));
		return map;
	}
}
