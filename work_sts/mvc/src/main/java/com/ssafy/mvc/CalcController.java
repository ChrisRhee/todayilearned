package com.ssafy.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ssafy.mvc.dto.Calc;

@Controller
@RequestMapping("my")
public class CalcController {
	
	//@RequestMapping(value="calc.do", method=RequestMethod.GET)
	@GetMapping("calc.do")
	public String calc() {
		return "calc";
	}
	
	// 리퀘스트 매핑 하는 방법,
	//@RequestMapping(value="calc.do", method=RequestMethod.POST)
	
	//public String calc(@RequestParam("irum") String nm, int n1, String op, int n2, Model m) {
	
	// get인지 post인지 상관 없으면 그냥 이렇게 PostMapping 이렇게 해서 불러도 됨.
	@PostMapping("calc.do")
//	public String calc(Calc calc, Model m) {
//		calc.runCalc();
//		System.out.println(calc);
//		m.addAttribute("c",calc);
//		return "calcresult";
//	}
	
	//이렇게 하면 c로 접근해야한다.
	public String calc(@ModelAttribute("c") Calc calc, Model m) {
		calc.runCalc();
		System.out.println(calc);
		m.addAttribute("c",calc);
		return "calcresult";
	}
}
