package com.ssafy.mvc.dto;

public class User {
	private int uid;
	private String name;
	private String addr;
	
	public User() {
		this(0, "", "");
	}
	public User(int uid, String name, String addr) {
		super();
		this.uid = uid;
		this.name = name;
		this.addr = addr;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [uid=");
		builder.append(uid);
		builder.append(", name=");
		builder.append(name);
		builder.append(", addr=");
		builder.append(addr);
		builder.append("]");
		return builder.toString();
	}
	
	
}
