package com.ssafy.mvc;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	//@RequestMapping(value = "/", method = RequestMethod.GET)
	//@RequestMapping("/")
	@GetMapping("/")
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		model.addAttribute("name","헤헤헿");
		
		return "home";
	}
	
	// request로 set하기
	/*
	@RequestMapping("hello.do")
	public String hello(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("msg", "request 반갑다 이자시가" );
		return "result";
	}
	*/
	
	// model에 add하기
	/*
	@RequestMapping("hello.do")
	public String hello(Model model) {
		model.addAttribute("msg", "model 반갑다 이자시가" );
		return "result";
	}
	*/
	
	// model 안받고 안에서 선언하기 
	@RequestMapping("hello.do")
	public ModelAndView hello() {
		ModelAndView mav = new ModelAndView();
		mav.addObject("msg", "ModelAndView 반갑다 이자시가" );
		mav.setViewName("result");
		return mav;
	}
}
