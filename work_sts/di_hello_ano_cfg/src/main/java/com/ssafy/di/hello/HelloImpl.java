package com.ssafy.di.hello;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component ("myHello")
@Scope("prototype")
public class HelloImpl implements Hello {

	@PostConstruct
	public void onCreate() {
		System.out.println("onCreate()");
	}
	
	@PreDestroy
	public void onDestroy() {
		System.out.println("onDestroy()");
	}
	
	@Override
	public void sayHello(String name) {
		System.out.println("Hi~, "+name);
	}
	
}
