package com.ssafy.di.calc;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.ssafy.di.log.Log;

@Service("myCalc")
public class CalcImpl implements Calc {
//	@Resource(name = "myLog")
	
	@Autowired
	@Qualifier("myLog")
	private Log log;
	
//	public CalcImpl(Log lll) {
//		this.log = lll;
//	}
//	
//	public CalcImpl() {
//		System.out.println("CalcImpl()");
//	}
	
	public void setLog(Log log) {
		this.log = log;
	}
	
	@Override
	public int plus(int i, int j) {
		log.info("plus: i="+i+", j="+j);
		return i+j;
	}

	@Override
	public int minus(int i, int j) {
		log.info("minus: i="+i+", j="+j);
		return i-j;
	}

}
