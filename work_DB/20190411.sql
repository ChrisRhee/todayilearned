#update
select @@autocommit;
set autocommit = false;

select count(*) from c_emp;
delete from c_emp;
select count(*) from c_emp;
savepoint delete_cemp;
select count(*) from t_emp;
delete from t_emp;
select count(*) from t_emp;
savepoint delete_temp;

select count(*) from s_location;
delete from s_location;
select count(*) from s_location;
-- update s_emp set ... where .... ;
rollback;


select * from t_emp;
update t_emp
set name='홍길홍', salary=1111, dept_name='기획부'
where name ='홍길동';

#t_emp 형식으로 새로운 테이블 만들기
create table c_emp
as select * from t_emp where 1!=1;

insert into c_emp(id, name, salary, phone, dept_name)
select * from t_emp where id<4;

update c_emp
set name=(select name from t_emp where id=2)
where name = '홍길홍';

delete from c_emp 
where dept_name=(select dept_name 
				 from t_emp 
                 where salary = 1111);

create table emp_113
as select id 사번, name 이름, mailid 메일, start_date 입사일 from s_emp where dept_id = 113;
describe emp_113;
select * from emp_113 where 1 = 1;

drop table if exists t_emp;
# 테이블 만들기
create table t_emp(
 id INT(6) primary key auto_increment,
 name char(25) not null,
 salary decimal(7,2) ,
 phone char(15) default '000-000-0000',
 dept_name char(25) 
);
# 테이블에 뭔가 넣기
insert into t_emp (id, name, salary, phone, dept_name) 
values (1,'홍길동', 1000, '010-111-1111', '인사부');
insert into t_emp (id, name, salary, phone, dept_name) 
values (2,'사오정', 2000, '010-222-2222', '총무부');
# id는 auto increment이므로 굳이 값을 주지 않아도 알아서 증가하면서 등록된다.
insert into t_emp (id, name, salary) 
values (NULL,'손오공', 3000);
insert into t_emp (name, salary) 
values ('저팔계', NULL);


delete from t_emp;
rollback;
select @@ autocommit;
set aurocoomit = false;
select * from t_emp;
desc t_emp;

# 이 안쪽에 있는 쿼리는 단독으로 실행이 되지 않는다. 
select e.id , e.name, e.dept_id, t.name
from s_emp e, s_dept t
where salary >= 2500
and e.dept_id = t.dept_id
order by (select name
		  from s_dept d
          where e.dept_id = d.dept_id) asc;
          


#서정주와 같은 mamager_id와 dept_id를 가진 사람을 출력하자
select manager_id, dept_id from s_emp where name = '서정주';
# 시작
select * 
from s_emp 
where manager_id=(select manager_id from s_emp where name = '서정주')
and dept_id = (select dept_id from s_emp where name = '서정주');
# 이렇게 하나로 합칠 수 있다.
select * 
from s_emp 
where (manager_id,dept_id)=(select manager_id,dept_id from s_emp where name = '서정주')
and name != '서정주';

select e.name, e.dept_id 
from s_emp e, s_dept d, s_region r
where e.dept_id = d.dept_id 
and d.region_id = r.region_id
and r.name = '서울특별시';

#이걸 합쳐보자
select name, dept_id from s_emp where dept_id in (101,102,110,118) order by dept_id;
select dept_id, region_id from s_dept where region_id =1;
select region_id from s_region where name = '서울특별시';

select name, dept_id
from s_emp
where dept_id in(select dept_id from s_dept where region_id = 3);

#지금까지 쿼리들은 값이 딱 1개만 리턴하는게 특징이다. 이렇게 해서 '=' 으로 비교가 가능하고 크고 작다로 비교가 가능했다.
select name, salary, title
from s_emp
where title = (select title from s_emp where name = '최정선')
order by salary;


#subquery 작동 예시
#select dept_id from s_emp where name = '김정미' 이 쿼리문은 111을 보여주는데, 저렇게 where안에 
#넣어서 실행 할 수 있다. subquery는 독립적으로 따로 실행을 해도 실행이 되는 쿼리이다. 저 쿼리를 다른 쿼리에 넣어서 사용하는 것을
# Nested Subquery라고 한다.
select name, title, dept_id 
from s_emp 
where dept_id = (select dept_id from s_emp where name = '김정미');
select E.name, E.title, T.dept_id 아이디
from s_emp E, (select dept_id from s_emp where name = '김정미') T
where E.dept_id = T.dept_id;

# 이렇게 테이블로 부를 수 있따.
select *
from (select dept_id from s_emp where name = '김정미') T;

select e.id, e.name, e.title, e.salary, e.manager_id, m.name, m.title, m.salary, m.manager_id
from s_emp e, s_emp m
where e.manager_id = m.id
and e.manager_id = 1;

select s_emp.name, s_emp.dept_id, s_dept.name, count(*)
from s_emp, s_dept
where s_emp.dept_id = s_dept.dept_id;

select * from s_region; # 5
select * from s_location; # 5
select * from s_region,s_location; # 25
select * from s_region natural join s_location; # 5
select * from s_region join s_location using(region_id); # 5

# 주로 이걸 많이 쓴다. 이걸 알아두도록 하자. join을 하는데, 보고자 하는 것을 명시해서 준다.
# 이건 ansi 표준이라고 한다.
select * from s_region join s_location 
on(s_region.region_id = s_location.region_id); # 5
select * from s_region join s_location 
on(s_region.region_id = s_location.region_id) # 5
where s_region.region_id >2;
# 위와 같은 문장이다. 이렇게도 쓴다. 주로 오라클(실무)에서 이렇게 쓴다. 이걸 inner join 이라고 한다.
select * from s_region,s_location 
where s_region.region_id = s_location.region_id; # 5
select * from s_region,s_location 
where s_region.region_id = s_location.region_id
and s_region.region_id>2;

select r.name 지역명, r.region_id 아이디, l.location_name 위치명
from s_region r, s_location l
where r.region_id = l.region_id;


select * from s_emp;
select * from s_emp, s_dept;