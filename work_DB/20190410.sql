-- 72페이지 
-- 각 부서별 평균 급여를 나타내시오
select dept_id 부서id, round(sum(salary),0) 합, round(avg(salary),0) 평균, count(*) 개수
from s_emp
group by dept_id
order by 3 desc;


select region_id, count(name) 카운트
from s_dept
where region_id!=1
group by region_id
having 카운트 > 1
order by region_id desc
limit 2,2;

select name, dept_id, salary
from s_emp
order by dept_id;

select s_emp.name, s_emp.dept_id, s_dept.dept_id, s_emp.salary 
from s_emp
where s_emp.dept_id = s_dept.dept_id;


select *
from s_emp
order by name desc
limit 0,5;

select id,name from s_emp;

select id 사번, name 이름, format(salary*1000,0) 급여, date_format(start_date,'%Y/%m/%d') 입사일
from s_emp
where salary >= 3000;

select name, title, salary, dept_id
from s_emp
where dept_id in(110,113) and salary >= 2000;


select id 사번,name 이름, salary 급여, commission_pct 커미션
from s_emp
where commission_pct is not null
order by 3 desc;

select distinct title
from s_emp;


select count(commission_pct) from s_emp;
select commission_pct from s_emp;

select name, date_format(start_date, '%Y/%m/%d') from s_emp;

select name, format(salary*1000,0) from s_emp;
select datediff('2019-04-10', '2019-12-10');
select cast('2019-04-10' as date);
select year('2019-04-10') 년도, month('2019-04-10') 월 , day('2019-04-10') 일;
select sysdate(), sleep(2), current_date();
select sysdate(), sleep(2), sysdate();
select now() 지금, sleep(2), sysdate() 2초후;
select now() 지금, sleep(2), now() 2초후;

select 1 from dual;

describe s_emp;
desc s_emp;

select round(135.355442, 0 ), truncate(135.355442, 1) from dual;

select count(*) 인원, sum(salary)총급여, sum(salary)/count(*)평균1, round(avg(salary),2) 평균2 from s_emp;

select name, start_date, title,substr(start_date,1,4) 연도,substr(start_date,6,2) 월,substr(start_date,9) 일
from s_emp
where substr(start_date,1,4)= '1991';

select name, start_date, title
from s_emp
where name like '박%'; -- 이렇게 뒷 글자를 모르겠다 싶을때는 like를 쓴다. 속도는 느리다.
-- %는 뒤에 붙는게 속도가 빠르다. 
-- 한글자를 정의할떄는 '_' 이거 하나만 쓰고, 그건 모르겠고 다 찾으려면 '%' 이렇게 한다.

select name, salary
from s_emp
where salary between 1000 and 1500;
-- between A and B A와 B사이에 있는 값을을 출력한다. 

select name, commission_pct 
from s_emp 
where commission_pct =10 or commission_pct=15;

select name, start_date, dept_id
from s_emp
where start_date>'1990-01-01';

select name, commission_pct 
from s_emp 
where commission_pct is not null
order by commission_pct; -- null 이 아닌 것을 출력한다.

select name, commission_pct 
from s_emp 
where commission_pct is null
order by commission_pct; -- null만 출력한다.

select name, commission_pct from s_emp order by commission_pct desc; -- 여기는 null이 먼저 나온다.

select name, salary*18 연봉, title from s_emp order by 연봉 asc, name; -- 연봉이 같으면 이름순
select name, salary*18 연봉, title from s_emp order by 3 asc; -- row 1,2,3 번줄로 역순 정렬
select name, salary*18 연봉, title from s_emp order by 연봉 desc; -- '연봉'으로 역순 정렬
select name, salary*18, title from s_emp order by salary*18 asc; -- 정렬
select name, salary*18, title from s_emp order by name asc; -- 정렬
select name, salary*18, title from s_emp order by name desc; -- 역순 정렬
select distinct title from s_emp;
select title from s_emp;
select concat(name,' ', title, ' ' , salary) from s_emp;
select name, start_date, salary, salary*18 as 연봉 from s_emp;
select * from s_dept;
select count(*) from s_dept;

