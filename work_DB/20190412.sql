alter table c_emp
modify salary decimal(9,3) not null;

alter table c_emp
add constraint c_phone_nn not null(phone);

alter table c_emp
add constraint c_phone_uk unique(phone);

create table c_emp(
id int(5) primary key auto_increment,
name char(25) not null,
salary decimal (7,2),
phone char(15) check(phone like '3429-%'),
dept_id decimal(7,0),
foreign key (dept_id) references s_dept(dept_id)
);



# 5.2.2 4) forien key
alter table s_emp 
add foreign key (dept_id) references s_dept(dept_id) on delete set null;

delete from s_dept where dept_id = 111;

INSERT INTO s_emp VALUES (
   27, '하하하', 'haha98',
   str_to_date('03-MAR-1990','%d-%b-%Y'), NULL, '사장',
   333, 5000, NULL);

set @rownum:=0;
select @rownum:=@rownum+1 순서, name, salary, title
from s_emp
where (@rownum:=0)= 0;
# 4-2 top-N 쿼리
select name, salary, title, row_number() over (order by salary desc) 순서
from s_emp
limit 0,5;

# 4-1 inline view
select e.name, e.title, d.name
from (select name , title, dept_id
	from s_emp
    where title = '사원') e, s_dept d
where e.dept_id = d.dept_id;

# subquery
select e.name, e.salary, e.dept_id
from s_emp e
where e.salary <(select avg(t.salary) 
			   from s_emp t
               where t.dept_id = 118);

# outer join
select *
from s_emp e left join s_dept d  -- 오른쪽 table 기준으로 보여준다. 
on e.dept_id = d.dept_id

union -- union 을 하면 왼쪽을 join한게 먼저 나오고, 그 다음 right join이 나온다. 풀 조인이 된다. 

select *
from s_emp e right join s_dept d  -- 오른쪽 table 기준으로 보여준다. 
on e.dept_id = d.dept_id;

insert into s_dept values (120, '회계부', 5);
INSERT INTO s_emp VALUES (
   26, '강호동', 'hodong',
   str_to_date('12-APR-1991','%d-%b-%Y'), 10, '사원',130, 3300, NULL);
   
# non-equijoin
select e.ENAME, e.sal, s.grade
from emp e, salgrade s
where e.sal between s.losal and s.hisal;

select count(*) from emp;
select count(*) from salgrade;

# rank 
select dept_id,name,salary,
	rank() over (partition by dept_id order by salary) 순위
from s_emp
order by dept_id, 순위;

# case 문 작성해보자
select name, salary, case when salary >= 4000 then 'A'
						 when salary >= 3000 then 'B'
						 when salary >= 2000 then 'C'
                         when salary >= 1000 then 'D'
                         else 'E'
					end 급여등급
from s_emp;

select name, dept_id, case dept_id
						when 101 then '총무부'
                        when 102 then '영업부'
                        when 110 then '기획부'
                        else '부서없음'
					  end 부서
from s_emp;
# 값에 직접 비교와 연산자등을 주려면 when 안에 변수를 적어준다.
select name, dept_id, case  
						when dept_id=101 then '총무부'
                        when dept_id=102 then '영업부'
                        when dept_id >= 110 then '기획부'
                        else '부서없음'
					  end 부서
from s_emp;

# null fuction
select name, title, salary*ifnull(commission_pct,1)/100 커미션, commission_pct 커미션
from s_emp;
select name, title, salary*if(commission_pct is not null,5,3)/100 커미션, if(commission_pct is not null,5,3) 커미션
from s_emp;

# index 
drop index s_emp_name_idx on s_emp;
create index s_emp_name_idx 
on s_emp(name);

select id, name, salary
from s_emp
where name = '구원상';

drop view empvu113;

create or replace view empvu113
as select id,name ,title from s_emp where dept_id=113;

select * from s_emp;
select * from empvu113;
