import java.sql.*;

public class UpdatePrepare {
	public static String url = "jdbc:mysql://localhost:3306/ssafydb?characterEncoding=UTF-8&serverTimezone=UTC";
	public static String user = "ssafy";
	public static String password = "ssafy";
	
	public static void main(String[] args) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, user, password);
			String sql = "update s_dept set dept_id=?, name=?, region_id=? where dept_id = ?";
			st = con.prepareStatement(sql);
			st.setInt(1,  204);
			st.setString(2, "호히호히호");
			st.setInt(3, 12);
			st.setInt(4, 201);
			int cnt = st.executeUpdate();
			System.out.println("Update : " + cnt);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(st!=null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			try {
				if(con!=null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
