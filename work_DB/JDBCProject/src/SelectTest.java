import java.sql.*;

public class SelectTest {
	public static String url = "jdbc:mysql://localhost:3306/ssafydb?characterEncoding=UTF-8&serverTimezone=UTC";
	public static String user = "ssafy";
	public static String password = "ssafy";
	
	public static void main(String[] args) {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		
		try {
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();
			
			String sql = "select * from s_dept order by dept_id";
			rs = st.executeQuery(sql);
			while(rs.next()) {
				
				System.out.println(rs.getInt(1)+"\t"+rs.getString("name")+"\t"+rs.getString(3));
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs!=null) rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
			
			try {
				if(st!=null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			try {
				if(con!=null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
