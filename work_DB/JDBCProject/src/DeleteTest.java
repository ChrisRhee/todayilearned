import java.sql.*;

public class DeleteTest {
	public static String url = "jdbc:mysql://localhost:3306/ssafydb?characterEncoding=UTF-8&serverTimezone=UTC";
	public static String user = "ssafy";
	public static String password = "ssafy";
	
	public static void main(String[] args) {
		Connection con = null;
		Statement st = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();
			//String sql = "create table aaa(id int)";
			//String sql = "drop table aaa";
			
			String sql = "delete from s_dept where dept_id = 201";
			int cnt = st.executeUpdate(sql);
			System.out.println("Delete : " + cnt);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(st!=null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			try {
				if(con!=null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
