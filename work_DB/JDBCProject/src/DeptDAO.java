import java.sql.*;

public class DeptDAO {
	public static String url = "jdbc:mysql://localhost:3306/ssafydb?characterEncoding=UTF-8&serverTimezone=UTC";
	public static String user = "ssafy";
	public static String password = "ssafy";
	
	static {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void close(Statement rs) {
		try {
			if(rs!=null) rs.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void close(Connection con) {
		try {
			if(con!= null) con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	private void close(ResultSet rs) {
		try {
			if(rs!= null) rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int count() {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		int cnt = 0;
		try {
			con= getConnection();
			String sql = "select count(*) from s_dept order by dept_id";
			st= con.prepareStatement(sql);
			rs=st.executeQuery(sql);
			while(rs.next()) {
				cnt=rs.getInt(1);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rs);
			close(st);
			close(con);
		}
		return cnt;
	}
	
	private Connection getConnection() throws SQLException{
		
		return DriverManager.getConnection(url, user, password);
	}

}
