import java.sql.*;

public class TransactionTest {
	public static String url = "jdbc:mysql://localhost:3306/ssafydb?characterEncoding=UTF-8&serverTimezone=UTC";
	public static String user = "ssafy";
	public static String password = "ssafy";
	
	public static void main(String[] args) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, user, password);
			
			String sql = "delete from s_dept where dept_id < ?";
			st = con.prepareStatement(sql);
			st.setInt(1, 100);
			int cnt = st.executeUpdate();
			System.out.println("Delete1 : " + cnt);
			Savepoint sp1=con.setSavepoint("delete dept_id <= 100");
			
			sql = "delete from s_dept where dept_id < ?";
			st = con.prepareStatement(sql);
			st.setInt(1, 200);
			cnt = st.executeUpdate();
			System.out.println("Delete1 : " + cnt);
			Savepoint sp2=con.setSavepoint("delete dept_id <= 200");
			
			con.rollback(sp1);
			con.commit();
			
		} catch (ClassNotFoundException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				con.setAutoCommit(true);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				
				if(st!=null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			try {
				if(con!=null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
