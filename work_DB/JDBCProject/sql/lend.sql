-- 대여
CREATE TABLE `ssafydb`.`TABLE` (
	`COL`   <데이터 타입 없음> NOT NULL COMMENT '대여번호', -- 대여번호
	`COL2`  <데이터 타입 없음> NULL     COMMENT '대여시작일', -- 대여시작일
	`COL3`  <데이터 타입 없음> NULL     COMMENT '대여반납일', -- 대여반납일
	`COL4`  <데이터 타입 없음> NULL     COMMENT '실제반납일', -- 실제반납일
	`COL5`  <데이터 타입 없음> NULL     COMMENT '도서번호', -- 도서번호
	`COL11` <데이터 타입 없음> NULL     COMMENT '도서명', -- 도서명
	`COL6`  <데이터 타입 없음> NULL     COMMENT '저자', -- 저자
	`COL7`  <데이터 타입 없음> NULL     COMMENT '출판사', -- 출판사
	`COL8`  <데이터 타입 없음> NULL     COMMENT '회원번호', -- 회원번호
	`COL9`  <데이터 타입 없음> NULL     COMMENT '회원명', -- 회원명
	`COL10` <데이터 타입 없음> NULL     COMMENT '회원연락처' -- 회원연락처
)
COMMENT '대여';

-- 대여
ALTER TABLE `ssafydb`.`TABLE`
	ADD CONSTRAINT `PK_TABLE` -- 대여 기본키
		PRIMARY KEY (
			`COL` -- 대여번호
		);

-- 아이템
CREATE TABLE `ssafydb`.`ITEM` (
	`LNUM`  INT(4)     NOT NULL COMMENT '대여번호', -- 대여번호
	`ISBN`  VARCHAR(8) NOT NULL COMMENT '도서번호', -- 도서번호
	`RDATE` DATE       NULL     COMMENT '실제반납일' -- 실제반납일
)
COMMENT '아이템';

-- 아이템
ALTER TABLE `ssafydb`.`ITEM`
	ADD CONSTRAINT `PK_ITEM` -- 아이템 기본키
		PRIMARY KEY (
			`LNUM`, -- 대여번호
			`ISBN`  -- 도서번호
		);

-- 대여
CREATE TABLE `ssafydb`.`LEND` (
	`LNUM`  INT(4) NOT NULL COMMENT '대여번호', -- 대여번호
	`SDATE` DATE   NULL     COMMENT '대여시작일', -- 대여시작일
	`EDATE` DATE   NULL     COMMENT '대여반납일', -- 대여반납일
	`CNUM`  INT(5) NULL     COMMENT '회원번호' -- 회원번호
)
COMMENT '대여';

-- 대여
ALTER TABLE `ssafydb`.`LEND`
	ADD CONSTRAINT `PK_LEND` -- 대여 기본키
		PRIMARY KEY (
			`LNUM` -- 대여번호
		);

-- 도서
CREATE TABLE `ssafydb`.`BOOK` (
	`ISBN`      VARCHAR(8)  NOT NULL COMMENT '도서번호', -- 도서번호
	`TITLE`     VARCHAR(30) NULL     COMMENT '도서명', -- 도서명
	`AUTHOR`    VARCHAR(20) NULL     COMMENT '저자', -- 저자
	`PUBLISHER` VARCHAR(20) NULL     COMMENT '출판사' -- 출판사
)
COMMENT '도서';

-- 도서
ALTER TABLE `ssafydb`.`BOOK`
	ADD CONSTRAINT `PK_BOOK` -- 도서 기본키
		PRIMARY KEY (
			`ISBN` -- 도서번호
		);

-- 회원
CREATE TABLE `ssafydb`.`Customer` (
	`CNUM`   INT(5)      NOT NULL COMMENT '회원번호', -- 회원번호
	`CNAME`  VARCHAR(20) NULL     COMMENT '회원명', -- 회원명
	`CPHONE` VARCHAR(12) NULL     COMMENT '회원연락처' -- 회원연락처
)
COMMENT '회원';

-- 회원
ALTER TABLE `ssafydb`.`Customer`
	ADD CONSTRAINT `PK_Customer` -- 회원 기본키
		PRIMARY KEY (
			`CNUM` -- 회원번호
		);

-- 아이템
ALTER TABLE `ssafydb`.`ITEM`
	ADD CONSTRAINT `FK_LEND_TO_ITEM` -- 대여 -> 아이템
		FOREIGN KEY (
			`LNUM` -- 대여번호
		)
		REFERENCES `ssafydb`.`LEND` ( -- 대여
			`LNUM` -- 대여번호
		);

-- 아이템
ALTER TABLE `ssafydb`.`ITEM`
	ADD CONSTRAINT `FK_BOOK_TO_ITEM` -- 도서 -> 아이템
		FOREIGN KEY (
			`ISBN` -- 도서번호
		)
		REFERENCES `ssafydb`.`BOOK` ( -- 도서
			`ISBN` -- 도서번호
		);

-- 대여
ALTER TABLE `ssafydb`.`LEND`
	ADD CONSTRAINT `FK_Customer_TO_LEND` -- 회원 -> 대여
		FOREIGN KEY (
			`CNUM` -- 회원번호
		)
		REFERENCES `ssafydb`.`Customer` ( -- 회원
			`CNUM` -- 회원번호
		);