-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`emp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`emp` (
  `eno` INT NOT NULL,
  `dno` VARCHAR(45) NULL,
  PRIMARY KEY (`eno`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Customer` (
  `cno` INT NOT NULL,
  `cname` VARCHAR(45) NULL,
  `ctel` VARCHAR(45) NULL,
  PRIMARY KEY (`cno`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Order` (
  `onum` INT NOT NULL,
  `odate` VARCHAR(45) NULL,
  `emp_eno` INT NOT NULL,
  `Customer_cno` INT NOT NULL,
  PRIMARY KEY (`onum`),
  INDEX `fk_Order_emp1_idx` (`emp_eno` ASC) VISIBLE,
  INDEX `fk_Order_Customer1_idx` (`Customer_cno` ASC) VISIBLE,
  CONSTRAINT `fk_Order_emp1`
    FOREIGN KEY (`emp_eno`)
    REFERENCES `mydb`.`emp` (`eno`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Order_Customer1`
    FOREIGN KEY (`Customer_cno`)
    REFERENCES `mydb`.`Customer` (`cno`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`product` (
  `pnum` INT NOT NULL,
  `price` VARCHAR(45) NULL,
  PRIMARY KEY (`pnum`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`item` (
  `onum` INT NOT NULL,
  `pnum` VARCHAR(45) NULL,
  `quantity` VARCHAR(45) NULL,
  `itemcol` VARCHAR(45) NULL,
  `Order_onum` INT NOT NULL,
  `product_pnum` INT NOT NULL,
  PRIMARY KEY (`onum`, `Order_onum`, `product_pnum`),
  INDEX `fk_item_Order1_idx` (`Order_onum` ASC) VISIBLE,
  INDEX `fk_item_product1_idx` (`product_pnum` ASC) VISIBLE,
  CONSTRAINT `fk_item_Order1`
    FOREIGN KEY (`Order_onum`)
    REFERENCES `mydb`.`Order` (`onum`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_product1`
    FOREIGN KEY (`product_pnum`)
    REFERENCES `mydb`.`product` (`pnum`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
