# 1. 각 부서별로 급여의 최소값과 최대값을 나타내시오, 최소값과 최대값이 같은 부서를 출력하지 마시오.
select min(salary), max(salary)
from s_emp
group by dept_id
having min(salary) != max(salary);

# 2. 각 지역번호별로 몇 개의 부서가 있는지 나타내시오.
select s_region.region_id, s_region.name, count(*)
from s_dept, s_region
where s_dept.region_id = s_region.region_id
group by s_dept.region_id;

select region_id, s_region.name, count(*)
from s_dept join s_region using(region_id)
group by s_dept.region_id;

# 3. s_dept, s_region 테이블을 이용하여 각 지역별로 해당지역의 총 부서수를 구하되, ansi sql을 사용하라
select s_region.name, count(*)
from s_dept join s_region on(s_dept.region_id = s_region.region_id)
group by s_dept.region_id;

# 4. 송명성이 관리자인 사원에 대하여 그들의 이름과 직책 급여를 나타내시오
select name, title, salary 
from s_emp
where manager_id =(select id from s_emp where name= '손명성')
and name != '손명성';
