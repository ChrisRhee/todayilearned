DROP TABLE IF EXISTS s_region;
DROP TABLE IF EXISTS s_location;

CREATE TABLE s_region(
 region_id DECIMAL(7) NOT NULL,
 name VARCHAR(25) NOT NULL
);

INSERT INTO s_region VALUES (1,'서울특별시');
INSERT INTO s_region VALUES (2,'경기도');
INSERT INTO s_region VALUES (3,'충정/강원');
INSERT INTO s_region VALUES (4,'전라/제주');
INSERT INTO s_region VALUES (5,'부산/경상');

CREATE TABLE s_location(
 location_id DECIMAL(7) NOT NULL,
 location_name VARCHAR(15),
 region_id DECIMAL(7) NOT NULL
);

INSERT INTO s_location VALUES (10,'북아메리카',1);
INSERT INTO s_location VALUES (20,'남아메리카',2);
INSERT INTO s_location VALUES (30,'아프리카/중동',3);
INSERT INTO s_location VALUES (40,'아시아',4);
INSERT INTO s_location VALUES (50,'유럽',5);
-- INSERT INTO s_location VALUES (60,'한국',6);

COMMIT;
