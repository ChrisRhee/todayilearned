package com.ssafy;

import java.sql.*;
import java.util.*;

import javax.naming.InitialContext;
import javax.sql.DataSource;

public class BookDAO {
	private static DataSource ds = null;
	static {
		try {
			InitialContext ctx = new InitialContext();
			ds =(DataSource)ctx.lookup("java:comp/env/jdbc/ssafydb");
			Class.forName("com.mysql.cj.jdbc.Driver");
			System.out.println("LookUp OK");
		}catch(Exception e) {
			System.out.println("LookUp Error");
			e.printStackTrace();
		}
	}
	
	public void close(Statement st) {
		try {
			if(st!=null) st.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void close(Connection con) {
		try {
			if(con!= null) con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	private void close(ResultSet rs) {
		try {
			if(rs!= null) rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int count() {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		int cnt = 0;
		try {
			con= getConnection();
			String sql = "select count(*) from book";
			st= con.prepareStatement(sql);
			rs=st.executeQuery(sql);
			while(rs.next()) {
				cnt=rs.getInt(1);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rs);
			close(st);
			close(con);
		}
		return cnt;
	}
	
	public Connection getConnection() throws SQLException{
		return ds.getConnection();
	}
	
	public void insertBook(Book book) throws ClassNotFoundException{
		
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = getConnection();
			String sql = "Insert into book values (?, ?, ?, ?, ?, ?)";
			st = con.prepareStatement(sql);
			st.setString(1, book.getIsbn());
			st.setString(2, book.getTitle());
			st.setString(3, book.getAuthor());
			st.setString(4, book.getPublisher());
			st.setInt(5, book.getPrice());
			st.setString(6, book.getDescription());
			int cnt = st.executeUpdate();
			System.out.println("insert :" + cnt);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(st);
			close(con);
		}
	}
	public void updateBook(Book book) throws ClassNotFoundException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = getConnection();
			String sql = "update book set isbn=?, title=?, author=?, publisher=?, price=?, description=? where isbn = ?";
			st = con.prepareStatement(sql);
			st.setString(1, book.getIsbn());
			st.setString(2, book.getTitle());
			st.setString(3, book.getAuthor());
			st.setString(4, book.getPublisher());
			st.setInt(5, book.getPrice());
			st.setString(6, book.getDescription());
			st.setString(7, book.getIsbn());
			
			int cnt = st.executeUpdate();
			System.out.println("Update : " + cnt);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(st);
			close(con);
		}
	}
	public void deleteBook(String isbn) throws ClassNotFoundException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = getConnection();
			String sql = "delete from book where isbn = ?";
			st = con.prepareStatement(sql);
			st.setString(1, isbn);
			
			int cnt = st.executeUpdate();
			System.out.println("Delete : " + cnt);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(st);
			close(con);
		}
	}
	
	public Book findBook(String isbn) throws ClassNotFoundException {
		Book temp = new Book();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			String sql = "select * from book where isbn='"+isbn+"'";
			st = con.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next()) {
				temp.setIsbn(rs.getString(1));
				temp.setTitle(rs.getString(2));
				temp.setAuthor(rs.getString(3));
				temp.setPublisher(rs.getString(4));
				temp.setPrice(rs.getInt(5));
				temp.setDescription(rs.getString(6));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rs);
			close(st);
			close(con);
		}
		
		return temp;
	}
	
	public List<Book> listBooks() throws ClassNotFoundException {
		List<Book> temp = new ArrayList<>();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			st = con.createStatement();
			String sql = "select * from book";
			rs = st.executeQuery(sql);
			while(rs.next()) {
				Book b = new Book();
				b.setIsbn(rs.getString(1));
				b.setTitle(rs.getString(2));
				b.setAuthor(rs.getString(3));
				b.setPublisher(rs.getString(4));
				b.setPrice(rs.getInt(5));
				b.setDescription(rs.getString(6));
				
				temp.add(b);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rs);
			close(st);
			close(con);
		}
		
		return temp;
	}
}
