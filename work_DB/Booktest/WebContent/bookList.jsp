
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.ssafy.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>도서목록</h1>
	<%
		BookDAO dao = new BookDAO();
		List<Book> books = dao.listBooks();
		request.setAttribute("books",books);
	%>
	<c:choose>
	<c:when test ="${empty books}">
	도서정보 없음 <br>
	</c:when>
	<c:otherwise>
		<c:forEach var="b" items="${books}">
			${b.isbn}&nbsp;${b.title}&nbsp;${b.price}<br>
		</c:forEach>
	</c:otherwise>
	</c:choose>
</body>
</html>