# 1. book 설계
drop table if exists book;
create table Book(
 isbn char(8) primary key,
 title varchar(50) not null,
 author varchar(10) not null,
 publisher varchar(15) not null,
 price int not null,
 description varchar(200)
);

# 2. 도서 데이터 5개이상 저장
insert into book (isbn, title, author, publisher, price, description) 
values('123-1-14', 'Java와 coffee', 'diana','자바닷컴',8000,'삶의 즐거움');
insert into book (isbn, title, author, publisher, price,description) 
values('555-23-2', 'AI와 미래', '김현주','미래닷컴',20000,'');
insert into book (isbn, title, author, publisher, price, description) 
values('123-2-15', 'Java와 놀기', '김태희','자바닷컴',22000,'Java 정복');
insert into book (isbn, title, author, publisher, price, description) 
values('123-6-24', 'Java와 알고리즘', '서민규','자바닷컴',20000,'성능 업!!');
insert into book (isbn, title, author, publisher, price, description) 
values('423-5-36', 'IoT세상', '이세준','미래닷컴',25000,'');

savepoint num2;
# 3. 도서가격 10% 인하 수정
update book
set price=price*90/100;

savepoint num3;
# 4. AI 관련도서들은 20% 가격 인상 수정
update book
set price = (price + price*20/100)
where title like '%AI%';
savepoint num4;

# 5. 도서 제목에 java 포함하고  가격이 10000미만인 도서 삭제 
delete from book
where title like '%Java%'
and price < 10000;

savepoint num5;

# 6. 출판사별 도서의 개수와 가격의 합계, 가격의 평균을 검색
select publisher 출판사, count(*) 개수, sum(price) 합계, avg(price) 평균
from book
group by publisher;

rollback to num2;
select * from book;