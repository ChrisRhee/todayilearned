-- 1) emp 테이블 정보의 구조를 확인하는 sql을 작성하세요.
select *
from emp;

-- 2) 이름이 K로 시작하는 사원의 사원번호, 이름, 입사일, 급여 ㄱㄱ
select EMPNO, ENAME, HIREDATE, SAL
from emp
where ENAME like 'K%';

-- 3) 입사일이 2000년도인 사람의 모든 정보
select *
from emp
where year(hiredate) = 2000;

-- 4) 커미션이 NULL이 아닌 사람의 모든 정보 ㄱㄱ
select *
from emp
where comm is not null;

-- 5) 부서가 30번 부서이고 급여가 $1500 이상인 사람의 이름, 부서, 월급 
select ename, deptno, sal
from emp
where deptno = 30 and sal >= 1500;

-- 6) 부서번호가 30인 사람중 사원번호 sort하여 출력
select *
from emp
where deptno = 30
order by empno;

-- 7) 급여가 많은순으로 sort
select *
from emp
order by sal desc;

-- 8) 부서번호 asceding sort 급여가 많은 순으로 검색
select *
from emp
order by deptno asc, sal desc;

-- 9) 부서번호 desc, 이름순 asc, 급여순 desc
select *
from emp
order by deptno desc, ename asc, sal desc;

-- 10) emp table에서 이름, 급여, 커미션, 총액을 구하여 총액이 많은 순 커미션이 null인 사람 제외
select ename, sal 급여, sal*comm/100 커미션, sal+sal*comm/100 총액
from emp
where comm is not null
order by sal+sal*comm/100 desc;

-- 11) 10번 부서의 모든 사람들에게 13*의 보너스로 지불하기로 했다. 이름, 급여, 보너스 금액 부서번호
select ename 이름, sal 급여, sal*13/100 '13%', deptno 부서번호
from emp
where deptno = 10;

-- 12) 부서번호 20의 시간당 임금, 1달의 근무일수 = 12, 1일 근무시간 = 5
-- 출력 : 이름, 급여, 시간당 임금 (소수이하 1번째 자리에서 반올림)
select ename 이름, sal 급여, round((sal/12)/5,1) '시간당 임금'
from emp;

-- 13) 급여가 $1500~3000 사이의 사람은 급여의 15%를 회비로 지불 당함
-- 출력 : 이름, 급여, 회비 (소수 두번째 자리에서 반올림)
select ename 이름, sal 급여, round((sal*15)/100,2) 회비
from emp
where sal between 1500 and 3000;

-- 14) 모든 사원의 실 수력액 검색
-- 출력 : 급여가 많은 순으로 이름, 급여, 실수령액. 실수력액 = 급여에서 10% 세금 뺀 금액
select ename 이름, sal 급여, sal*90/100 실수령액
from emp
order by sal;

-- 15) 이름의 글자수가 6자 이상인 사람의 이름을 앞에서 3자만 구하여 소문자로 이름만 검색
select lower(substr(ename,1,3))
from emp
where LENGTH(ename) >5 ;

-- 16) 10번 부서 월급의 평균, 최고, 최저, 인원수를 구하여 검색
select avg(sal), max(sal), min(sal), count(*)
from emp
where deptno = 10
group by deptno;

-- 17) 각 부서별 같은 업무를 하는 사람의 인원수를 구하여 부서번호, 업무명, 인원수를 검색
select deptno, job, count(job)
from emp
group by deptno, job
order by deptno;

-- 18) 같은 업무를 하는 사람의 수가 4명 이상인 업무와 인원수를 검색
select job , count(*)
from emp
group by job
having count(*)>=4;

-- 19) 입사일로부터 오늘까지의 일수를 구하여 이름, 입사일, 근무일수 검색
select ename 이름, hiredate, datediff(sysdate(),hiredate) 근무일수
from emp;

-- 20) 직원의 이름, 근속년수를 구하여 검색
select ename 이름, year(sysdate())-year(hiredate) 근속년수
from emp;

CREATE TABLE EMP(
 EMPNO INT(4) PRIMARY KEY,
 ENAME VARCHAR(10),
 JOB VARCHAR(9),
 MGR INT(4),
 HIREDATE DATE,
 SAL DECIMAL(7,2),
 COMM DECIMAL(7,2),
 DEPTNO INT(2)
);

INSERT INTO EMP VALUES (7369,'SMITH','CLERK',7902,cast('2010-10-19' as date),800,NULL,20);
INSERT INTO EMP VALUES (7499,'ALLEN','SALESMAN',7698,cast('2000-10-19' as date),1600,300,30);
INSERT INTO EMP VALUES (7521,'WARD','SALESMAN',7698,cast('2013-05-19' as date),1250,500,30);
INSERT INTO EMP VALUES (7566,'JONES','MANAGER',7839,cast('2018-10-19' as date),2975,NULL,20);
INSERT INTO EMP VALUES (7654,'MARTIN','SALESMAN',7698,cast('2008-04-19' as date),1250,1400,30);
INSERT INTO EMP VALUES (7698,'BLAKE','MANAGER',7839,cast('2016-11-19' as date),2850,NULL,30);
INSERT INTO EMP VALUES (7782,'CLARK','MANAGER',7839,cast('2017-10-19' as date),2450,NULL,10);
INSERT INTO EMP VALUES (7788,'SCOTT','ANALYST',7566,cast('2013-10-11' as date),3000,NULL,20);
INSERT INTO EMP VALUES (7839,'KING','PRESIDENT',NULL,cast('2014-08-19' as date),5000,NULL,10);
INSERT INTO EMP VALUES (7844,'TURNER','SALESMAN',7698,cast('2010-10-19' as date),1500,0,30);
INSERT INTO EMP VALUES (7876,'ADAMS','CLERK',7788,cast('2000-09-19' as date),1100,NULL,20);
INSERT INTO EMP VALUES (7900,'JAMES','CLERK',7698,cast('2018-10-19' as date),950,NULL,30);
INSERT INTO EMP VALUES (7902,'FORD','ANALYST',7566,cast('2003-10-19' as date),3000,NULL,20);
INSERT INTO EMP VALUES (7934,'MILLER','CLERK',7782,cast('2002-10-19' as date),1300,NULL,10);