# 1. dept 테이플 생성 아래 데이터 추가
INSERT INTO DEPT VALUES (10,'ACCOUNTING','NEW YORK');
INSERT INTO DEPT VALUES (20,'RESEARCH','DALLAS');
INSERT INTO DEPT VALUES (30,'SALES','CHICAGO');
INSERT INTO DEPT VALUES (40,'OPERATIONS','BOSTON');

# 2. emp 수정완료
CREATE TABLE EMP(
 EMPNO INT(4) PRIMARY KEY,
 ENAME VARCHAR(10),
 JOB VARCHAR(10),
 MGR INT(4),
 HIREDATE DATE,
 SAL DECIMAL(7,2),
 COMM DECIMAL(7,2),
 DEPTNO INT(2),
 foreign key (DEPTNO) REFERENCES DEPT(DEPTNO)
);

CREATE TABLE DEPT(
 DEPTNO INT(2) PRIMARY KEY,
 DNAME VARCHAR(14),
 LOC VARCHAR(13)
);
# Alter table emp ann 

# 3. emp와 dept table을 join하여 이름 급여 부서명을 검색
select emp.ename 이름, emp.sal 급여, dept.DNAME
from emp join dept
on emp.DEPTNO = dept.DEPTNO;

# 4. 이름이 king인 사원의 부서명을 검색하세요
select DNAME
from dept
where deptno = (select deptno from emp where ename = 'KING');
# where e.deptno-d.eptno and e.ename = 'KING'


# 5. dept table에 있는 모든 부서 출력, emp table에 있는 data와 join하여 모든 사원의 이름, 부서번호, 부서명, 급여 출력alter
select emp.ENAME, emp.EMPNO, dept.DNAME, emp.SAL
from dept left join emp  -- 오른쪽 table 기준으로 보여준다. 
on dept.deptno = emp.deptno;
# from emp e right join dept d on d.deptno = e. deptno;


# 6. scott의 매니저는 jones이다.
select concat(e.ENAME,'의 매니저는 ',m.ename,'입니다.') 문장
from emp e join emp m
on m.empno = e.mgr;

# 7. scott의 직무와 같은사람의 이름 부서명, 급여, 직무를 검색
select emp.ENAME 이름 , dept.dname, emp.sal, emp.JOB
from emp join dept
on emp.DEPTNO = dept.DEPTNO
and emp.JOB = (select job from emp where ename = 'SCOTT');

# 8. scott이 속해있는 부서의 모든 사람의 사원번호 이름 입사일 급여 검색
select emp.EMPNO 사원번호, emp.ename 이름, emp.HIREDATE 입사일, emp.sal 급여
from emp join dept
on emp.DEPTNO = dept.DEPTNO
and emp.deptno = (select deptno from emp where ename = 'SCOTT');

# 9. 전체 사원의 평균급여보다 급여가 많은 사원번호
select emp.EMPNO, emp.ENAME, dept.DNAME, emp.HIREDATE, dept.LOC, emp.sal
from emp join dept
on emp.DEPTNO = dept.DEPTNO
and emp.sal > (select avg (sal) from emp);

# 10. 30번 부서와 같은 일을 하는 사원의 번호, 이름 부서명 지역 급여, 급여가 많은 순
select emp.EMPNO, emp.ENAME, dept.DNAME, emp.HIREDATE, dept.LOC, emp.sal
from emp join dept
on emp.DEPTNO = dept.DEPTNO
and emp.DEPTNO = 30
order by emp.sal desc;

# 11
select emp.EMPNO, emp.ENAME, dept.DNAME, emp.HIREDATE, dept.LOC
from emp join dept
on emp.DEPTNO = dept.DEPTNO
and emp.DEPTNO = 10
and emp.JOB not in (select job from emp where deptno = 30);

# 12
select emp.EMPNO, emp.ENAME, emp.sal
from emp
where emp.SAL in (select sal from emp where ename in ('KING','JAMES'));

# 13
select emp.EMPNO, emp.ENAME, emp.sal
from emp 
where sal > (select max(sal) from emp where deptno = 30);

# 14 인덱스생성도 알아야할것같다.
drop index emp_name_idx on emp;
create index emp_name_idx 
on emp(ename);

# 15
select emp.ename, emp.sal
from emp 
where year(emp.HIREDATE) in (select year(HIREDATE) from emp where ename = 'ALLEN');

# 16
drop view empvu_salsum;
create or replace view empvu_salsum
as 
select deptno 부서, sum(sal) 합계
from emp 
group by deptno;
#뷰는 알터 뷰가 안먹힌다..!?


# 17
select * from empvu_salsum;

select *
from empvu_salsum
order by 2 desc;


select * from dept;

