insert into book (isbn, title, author, publisher, price, description) 
values('123-1-15', 'Java와 coffee', 'diana','자바닷컴',8000,'삶의 즐거움');
insert into book (isbn, title, author, publisher, price,description) 
values('555-23-3', 'AI와 미래', '김현주','미래닷컴',20000,'');
insert into book (isbn, title, author, publisher, price, description) 
values('123-2-16', 'Java와 놀기', '김태희','자바닷컴',22000,'Java 정복');
insert into book (isbn, title, author, publisher, price, description) 
values('123-6-25', 'Java와 알고리즘', '서민규','자바닷컴',20000,'성능 업!!');
insert into book (isbn, title, author, publisher, price, description) 
values('423-5-37', 'IoT세상', '이세준','미래닷컴',25000,'');

select * from book;