package com.ssafy;

import java.util.List;

public class Test {
	
	private static void printAllBooks(List<Book> list) {
		for(int i=0; i<list.size(); i++) {
			System.out.println(list.get(i));
		}
	}
	
	public static void main(String[] args) throws ClassNotFoundException {
		BookDAO dao = new BookDAO();
		System.out.println("----- findBook ----- ");
		System.out.println(dao.findBook("555-23-3"));
		System.out.println("\n----- printAllBooks ----- ");
		printAllBooks(dao.listBooks());
		
		System.out.println("\n----- 555-23-3 deleted ----- ");
		dao.deleteBook("555-23-3");
		printAllBooks(dao.listBooks());
		
		System.out.println("\n----- Insert Book 죠스책 ----- ");
		dao.insertBook(new Book("999-99-9","상어는 죠습니다", "김죠스", "상어출판사", 540000, "상어는 언제나 죠습니다."));
		printAllBooks(dao.listBooks());
		
		System.out.println("\n----- Update Book Java와 놀기 -> 죠스와 놀기 ----- ");
		dao.updateBook(new Book("123-2-15","상어와 놀기", "김죠스", "상어닷컴", 50000, "상어 정복"));
		printAllBooks(dao.listBooks());
		
		System.out.println("\n----- count() ----- ");
		System.out.println(dao.count());
	}
}
