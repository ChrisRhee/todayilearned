package com.ssafy.model.dto;

public class Book {
	private String isbn;
	private String title;
	private String author;
	private String publisher;
	private String description;
	
	public Book() {
		this("", "", "", "", "");
	}
	
	public Book(String isbn, String title, String author, String publisher, String description) {
		super();
		setIsbn(isbn);
		setTitle(title);
		setAuthor(author);
		setPublisher(publisher);
		setDescription(description);
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Book [isbn=");
		builder.append(getIsbn());
		builder.append(", title=");
		builder.append(getTitle());
		builder.append(", author=");
		builder.append(getAuthor());
		builder.append(", publisher=");
		builder.append(getPublisher());
		builder.append(", description=");
		builder.append(getDescription());
		builder.append("]");
		return builder.toString();
	}
	
	
}
