package com.ssafy.model.service;

import java.util.List;

import com.ssafy.model.dto.UserInfo;
import com.ssafy.model.repository.UserRepository;

public class UserServiceImpl implements UserService {
	
	private UserRepository userRepo;
	
	public UserServiceImpl(UserRepository userRepo) {
		super();
		this.userRepo = userRepo;
	}

	@Override
	public UserRepository getUserRepo() {
		return userRepo;
	}

	@Override
	public UserInfo login(String id, String pw) {
		return null;
	}

	@Override
	public UserInfo select(String id) {
		return userRepo.select(id);
	}

	@Override
	public List<UserInfo> selectAll() {
		return userRepo.selectAll();
	}

	@Override
	public int joinDayTime(UserInfo userinfo) {
		return userRepo.insert(userinfo);
	}

	@Override
	public int update(UserInfo userinfo) {
		return userRepo.update(userinfo);
	}

	@Override
	public int leaveDayTime(String userid) {
		return userRepo.delete(userid);
	}

}
