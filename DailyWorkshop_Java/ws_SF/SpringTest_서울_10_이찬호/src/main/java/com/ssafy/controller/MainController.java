package com.ssafy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ssafy.model.dto.Book;
import com.ssafy.model.service.BookService;

@Controller
public class MainController {
	private static final Logger logger=LoggerFactory.getLogger(MainController.class);
	
	@Autowired
	private BookService service;
	
	@GetMapping("/book/book")
	public String bookaddForm(Model model) {
		return "book/bookadd";
	}
	
	@GetMapping("/main/index")
	public String mainForm(Model model) {
		return "main/index";
	}
	
	@GetMapping("/book/booklist")
	public String booklistForm(Model model) {
		List<Book> books = service.selectAll();
		model.addAttribute("books", books);
		return "book/booklist";
	}
	
	@GetMapping("/book/bookview")
	public String bookviewForm(Model model, String isbn) {
		Book book = service.select(isbn);
		model.addAttribute("b",book);
		return "book/bookview";
	}
	
	@GetMapping("/book/bookdelete")
	public String bookdelete(Model model, String isbn) {
		int result = service.delete(isbn);
		model.addAttribute("msg", "책을 삭제 했습니다.");
		return "result/result";
	}
	
	@PostMapping("/book/search")
	public String booksearch(Model model,String searchField, String searchText) {
		List<Book> books = null; 
		switch(searchField) {
		case "LIST":
			books = service.selectAll();
			break;
		case "TITLE":
			books = service.searchByTitle(searchText);
			break;
		case "AUTHOR":
			books = service.searchByAuthor(searchText);
			break;
		case "PRICE":
			books = service.searchByPrice(Integer.parseInt(searchText));
			break;
		}
		model.addAttribute("books", books);
		return "book/booklist";
	}
	
	@PostMapping("/booksave")
	public String booksave(Model model, Book book, String isbn1, String isbn2, String isbn3) {
		book.setIsbn(isbn1+"-"+isbn2+"-"+isbn3);
		service.insert(book);
		model.addAttribute("msg", "정상적으로 등록 되었습니다");
		return "result/result";
	}
	
	
	@GetMapping("/book/bookupdate")
	public String bookupdateform(Model model, HttpSession session, String isbn) {
		Book book = service.select(isbn);
		String[] s = book.getIsbn().split("-");
		book.setIsbn1(s[0]);
		book.setIsbn2(s[1]);
		book.setIsbn3(s[2]);
		session.setAttribute("book", book);
		return "book/bookadd";
	}
	
	@PostMapping("/book/bookupdateaction")
	public String bookupdate(Model model, HttpSession session, Book book, String isbn1, String isbn2, String isbn3) {
		book.setIsbn(isbn1+"-"+isbn2+"-"+isbn3);
		service.update(book);
		session.setAttribute("msg", "정상적으로 수정 되었습니다");
		return "result/result";
	}
	
	@GetMapping("/session/main")
	public String main(Model model) {
		return "session/main";
	}

	@GetMapping("/session/modify")
	public String modifyForm(Model model) {
		return "session/modifyform";
	}

	@PostMapping("/session/modify")
	public String modify(Model model, RedirectAttributes redir, Book book, HttpSession session) {
		//service.updateDayTime(info);
		session.setAttribute("book", book);
		redir.addFlashAttribute("alarm", "수정성공");
		return "redirect:/session/main";
	}

	@ExceptionHandler(RuntimeException.class)
	public ModelAndView exceptionParameter(RuntimeException e, HttpServletRequest req) {
		Map<String, String> model = new HashMap<>();
		model.put("info", e.getMessage());
		ModelAndView mav = new ModelAndView("error/servererror", model);
		return mav;
	}	
}
