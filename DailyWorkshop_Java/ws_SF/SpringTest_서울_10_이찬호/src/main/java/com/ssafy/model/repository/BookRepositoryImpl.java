package com.ssafy.model.repository;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ssafy.model.dto.Book;

@Repository
public class BookRepositoryImpl implements BookRepository {

	private static final String ns = "com.ssafy.model.dts.Book.";
	
	@Autowired
	SqlSessionTemplate template;
	
	@Override
	public int insert(Book book) {
		return template.insert(ns+"insert", book);
	}

	@Override
	public int update(Book book) {
		return template.update(ns+"update", book);
	}

	@Override
	public int delete(String isbn) {
		return template.delete(ns+"delete", isbn);
	}

	@Override
	public Book select(String isbn) {
		return template.selectOne(ns+"select", isbn);
	}

	@Override
	public List<Book> selectAll() {
		return template.selectList(ns+"selectAll");
	}

	@Override
	public List<Book> searchByTitle(String title) {
		return template.selectList(ns+"searchByTitle", title);
	}

	@Override
	public List<Book> searchByAuthor(String author) {
		return template.selectList(ns+"searchByAuthor", author);
	}
	
	@Override
	public List<Book> searchByPrice(int price) {
		return template.selectList(ns+"searchByPrice", price);
	}

}
