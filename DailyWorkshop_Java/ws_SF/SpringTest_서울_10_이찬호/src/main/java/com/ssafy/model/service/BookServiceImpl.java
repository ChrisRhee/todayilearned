package com.ssafy.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafy.model.dto.Book;
import com.ssafy.model.repository.BookRepository;

@Service
public class BookServiceImpl implements BookService {
	
	@Autowired
	BookRepository repo;
	
	@Override
	public int insert(Book book) {
		return repo.insert(book);
	}

	@Override
	public int update(Book book) {
		return repo.update(book);
	}

	@Override
	public int delete(String isbn) {
		return repo.delete(isbn);
	}

	@Override
	public Book select(String isbn) {
		return repo.select(isbn);
	}

	@Override
	public List<Book> selectAll() {
		return repo.selectAll();
	}
	
	@Override
	public List<Book> searchByTitle(String title) {
		return repo.searchByTitle(title);
	}

	@Override
	public List<Book> searchByAuthor(String author) {
		return repo.searchByAuthor(author);
	}

	@Override
	public List<Book> searchByPrice(int price) {
		return repo.searchByPrice(price);
	}

}
