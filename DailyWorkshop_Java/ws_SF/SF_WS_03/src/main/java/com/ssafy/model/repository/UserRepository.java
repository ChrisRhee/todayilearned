package com.ssafy.model.repository;

import java.util.List;

import com.ssafy.model.dto.Book;
import com.ssafy.model.dto.UserInfo;

public interface UserRepository {
	public int insert(UserInfo userinfo);
	public int update(UserInfo userinfo);
	public int delete(String userid);
	public UserInfo select(String userid);
	public List<UserInfo> selectAll();
}
