package com.ssafy.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafy.model.dto.Book;
import com.ssafy.model.repository.BookRepository;


@Service
public class BookServiceImpl implements BookService {
	
	@Autowired
	private BookRepository bookRepo;
	
	public BookServiceImpl(BookRepository bookRepo) {
		super();
		this.bookRepo = bookRepo;
	}

	@Override
	public int insert(Book book) {
		return bookRepo.insert(book);
	}

	@Override
	public int update(Book book) {
		return bookRepo.update(book);
	}

	@Override
	public int delete(String isbn) {
		return bookRepo.delete(isbn);
	}

	@Override
	public Book select(String isbn) {
		return bookRepo.select(isbn);
	}

	@Override
	public List<Book> selectAll() {
		return bookRepo.selectAll();
	}

}
