package com.ssafy.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ssafy.model.repository.UserRepository;
import com.ssafy.model.service.UserService;

public class Client {
	private static final Logger logger = LoggerFactory.getLogger(Client.class);
	
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring/application-config.xml");
		UserRepository repoJDBC = ctx.getBean(UserRepository.class);
		logger.trace("jdbc repo: {}", repoJDBC);
		UserService service = ctx.getBean(UserService.class);
		logger.trace("service: {}", service);
		logger.trace("빈의 동일성 확인: {}", service.getUserRepo()==repoJDBC);
		System.out.println("test ok");
		 
	}
}
