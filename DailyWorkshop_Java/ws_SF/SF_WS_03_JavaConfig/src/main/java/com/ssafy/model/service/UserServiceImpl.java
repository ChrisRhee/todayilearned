package com.ssafy.model.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ssafy.model.dto.UserInfo;
import com.ssafy.model.repository.UserRepository;
import com.ssafy.model.repository.UserRepositoryMysqlImpl;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	private static final Logger logger=LoggerFactory.getLogger(UserRepositoryMysqlImpl.class);
	@Autowired
	private UserRepository userRepo;
	
	
	
	public UserServiceImpl(UserRepository userRepo) {
		super();
		this.userRepo = userRepo;
	}

	@Override
	public UserRepository getUserRepo() {
		return userRepo;
	}

	@Override
	public UserInfo login(String id, String pw) {
		UserInfo info = userRepo.select(id);
		if(info!=null && info.getPassword().equals(pw)) {
			return info;
		}else {
			return null;
		}
	}

	@Override
	public UserInfo select(String id) {
		return userRepo.select(id);
	}

	@Override
	public List<UserInfo> selectAll() {
		return userRepo.selectAll();
	}

	@Override
	public int joinDayTime(UserInfo userinfo) {
		int result = userRepo.insert(userinfo);
		//int i = 1/0;
		UserInfo selected = userRepo.select(userinfo.getUserid());
		logger.trace("조회 결과 : {}" , selected);
		return result;
	}

	@Override
	public int updateDayTime(UserInfo userinfo) {
		return userRepo.update(userinfo);
	}

	@Override
	public int leaveDayTime(String userid) {
		return userRepo.delete(userid);
	}

}
