package com.ssafy.exception;

public class TimeOutException extends RuntimeException {

	public TimeOutException() {
		super("지금은 서비스 이용 시간이 아닙니다.");
	}

	public TimeOutException(String message) {
		super(message);
	}

}
