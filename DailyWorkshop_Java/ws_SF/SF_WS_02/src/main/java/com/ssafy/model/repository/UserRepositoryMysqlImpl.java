package com.ssafy.model.repository;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ssafy.model.dto.UserInfo;

@Repository
public class UserRepositoryMysqlImpl implements UserRepository {
	private static final Logger logger=LoggerFactory.getLogger(UserRepositoryMysqlImpl.class);
	private static final String ns = "com.ssafy.model.mapper.UserInfo.";
	
	@Autowired
	SqlSessionTemplate template;
	
	@Override
	public UserInfo select(String userid) {
		logger.trace("select() id: {}",userid);
		return template.selectOne(ns+"select", userid);
	}

	@Override
	public List<UserInfo> selectAll() {
		logger.trace("selectAllUsers()");
		return template.selectList(ns+"selectAll");
	}
	
	@Override
	public int insert(UserInfo userinfo) {
		logger.trace("insert() UserInfo: {}",userinfo.getUserid());
		return template.insert(ns+"insert", userinfo);
	}

	@Override
	public int update(UserInfo userinfo) {
		logger.trace("update() UserInfo: {}",userinfo.getUserid());
		return template.update(ns+"update", userinfo);
	}

	@Override
	public int delete(String userid) {
		logger.trace("delete() id: {}",userid);
		return template.delete(ns+"delete", userid);
	}

}
