package com.ssafy.login;

public class Book {
	private String Number;
	private String Title;
	private String bookClass;
	private String Nation;
	private String Date;
	private String Publisher;
	private String Auther;
	private String Price;
	private String Contents;
	
	
	public Book(String number, String title, String bookClass, String nation, String date, String publisher,
			String auther, String price, String contents) {
		super();
		setNumber(number);
		setTitle(title);
		setBookClass(bookClass);
		setNumber(number);
		setDate(date);
		setPublisher(publisher);
		setAuther(auther);
		setPrice(price);
		setContents(contents);
	}
	
	public Book() {
		this("", "", "", "", "", "", "", "", "");
	}

	public String getNumber() {
		return Number;
	}
	public void setNumber(String number) {
		Number = number;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getBookClass() {
		return bookClass;
	}
	public void setBookClass(String bookclass) {
		bookClass = bookclass;
	}
	public String getNation() {
		return Nation;
	}
	public void setNation(String nation) {
		Nation = nation;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getPublisher() {
		return Publisher;
	}
	public void setPublisher(String publisher) {
		Publisher = publisher;
	}
	public String getAuther() {
		return Auther;
	}
	public void setAuther(String auther) {
		Auther = auther;
	}
	public String getPrice() {
		return Price;
	}
	public void setPrice(String price) {
		Price = price;
	}
	public String getContents() {
		return Contents;
	}
	public void setContents(String contents) {
		Contents = contents;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Book [Number=");
		builder.append(Number);
		builder.append(", Title=");
		builder.append(Title);
		builder.append(", bookClass=");
		builder.append(bookClass);
		builder.append(", Nation=");
		builder.append(Nation);
		builder.append(", Date=");
		builder.append(Date);
		builder.append(", Publisher=");
		builder.append(Publisher);
		builder.append(", Auther=");
		builder.append(Auther);
		builder.append(", Price=");
		builder.append(Price);
		builder.append(", Contents=");
		builder.append(Contents);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
