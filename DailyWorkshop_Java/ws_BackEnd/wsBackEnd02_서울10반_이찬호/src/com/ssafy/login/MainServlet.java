package com.ssafy.login;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/controller")
public class MainServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url= "index.html";
		//1. 요청받은 데이터 부분
		String cmd = request.getParameter("cmd");
		System.out.println("cmd:"+cmd);
		
		//2. JavaBean을 통해 처리
		switch(cmd) {
		case "loginUI":
			url = "Login.html";
			break;
		case "loginAction":
			url = loginAction(request);
			break;
		case "bookAddAction":
			url = bookAddAction(request);
			break;
		}
		
		//3. JSP 페이지로 이동
		if(url.charAt(0) == '/') {
			request.getRequestDispatcher(url).forward(request, response);
		}else {
			response.sendRedirect(url);
		}
	}
	
	private String bookAddAction(HttpServletRequest request) throws UnsupportedEncodingException {
		//  북을 다 등록해야지
		//입력 받은거 읽어오기
		Book b = new Book();
		request.setCharacterEncoding("utf-8");
		String booknum1 = request.getParameter("booknum1");
		String booknum2 = request.getParameter("booknum2");
		String booknum3 = request.getParameter("booknum3");
		b.setNumber(booknum1+"-"+booknum2+"-"+booknum3);
		b.setTitle(request.getParameter("bookname"));
		b.setBookClass(request.getParameter("bookclass"));
		b.setNation(request.getParameter("radio1"));
		b.setDate(request.getParameter("date"));
		b.setPublisher(request.getParameter("bookpublisher"));
		b.setAuther(request.getParameter("auther"));
		b.setPrice(request.getParameter("price"));
		b.setContents(request.getParameter("contents"));
		
		request.setAttribute("book", b);
		
		return "/Result.jsp";
	}
	
	private String loginAction(HttpServletRequest request) {
		// 로그인 admin/1234
		if(request.getParameter("id").equals("admin") && request.getParameter("pw").equals("1234"))
			return "/LoginSuccess.jsp";
		return "/Error.jsp";
	}
}
