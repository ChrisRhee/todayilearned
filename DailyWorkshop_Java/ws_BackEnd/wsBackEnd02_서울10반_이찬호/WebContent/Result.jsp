<%@page import="com.ssafy.login.Book"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<% request.setCharacterEncoding("utf-8"); 
Book book = (Book) request.getAttribute("book"); %>
<table class = "resultTable">
	<tr>
		<td class="result-info" colspan="2">등록된 정보는 다음과 같습니다.</td>
	</tr>
	<tr>
		<td class="result-label">도서번호</td>
		<td class="book-number"><%= book.getNumber() %></td>
	</tr>
	<tr>
		<td class="result-label">도서명</td>
		<td class="book-name"><%= book.getTitle() %></td>
	</tr>
	<tr>
		<td class="result-label">도서종류</td>
		<td class="book-class"><%= book.getBookClass() %></td>
	</tr>
	<tr>
		<td class="result-label">출판국가</td>
		<td class="book-nation"><%= book.getNation() %></td>
	</tr>
	<tr>
		<td class="result-label">출판일</td>
		<td class="book-date"><%= book.getDate() %></td>
	</tr>
	<tr>
		<td class="result-label">출판사</td>
		<td class="book-publisher"><%= book.getPublisher() %></td>
	</tr>
	<tr>
		<td class="result-label">저자</td>
		<td class="book-auther"><%= book.getAuther() %></td>
	</tr>
	<tr>
		<td class="result-label">도서가격</td>
		<td class="book-price"><%= book.getPrice() %></td>
	</tr>
	<tr>
		<td class="result-label">요약내용</td>
		<td class="book-contents"><%= book.getContents() %></td>
	</tr>
</table>