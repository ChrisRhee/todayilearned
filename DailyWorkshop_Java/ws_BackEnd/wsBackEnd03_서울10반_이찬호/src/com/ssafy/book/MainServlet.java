package com.ssafy.book;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/controller")
public class MainServlet extends HttpServlet {
	private BookMgr mgr = new BookMgr();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
	public void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; Charset=utf-8");
		String url= "index.html";
		//1. 요청받은 데이터 부분
		String cmd = request.getParameter("cmd");
		System.out.println("cmd:"+cmd);
		//2. JavaBean을 통해 처리
		switch(cmd) {
		case "BookAdd":
			url = "Book.html";
			break;
		case "Result":
			url = save(request, response);
			break;
		case "BookList":
			url = list(request, response);
			break;
		case "search":
			url = search(request, response);
			break;
		case "BookView":
			url = view(request, response);
			break;
		case "BookDelete":
			url = delete(request, response);
			break;
			
		}
		
		//3. JSP 페이지로 이동
		if(url.charAt(0) == '/') {
			request.getRequestDispatcher(url).forward(request, response);
		}else {
			response.sendRedirect(url);
		}
	}
	
	public String search(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
		String sch = request.getParameter("search");
		String stext = request.getParameter("searchFD");
		List<Book> bs = new ArrayList<>();
		switch(sch){
			case "whole":
				bs = mgr.search();
				break;
			case "bookname":
				bs = mgr.searchByTitle(stext);
				break;
			case "publisher":
				bs = mgr.searchByPublisher(stext);
				break;
			case "price":
				bs = mgr.searchByPrice(stext);
				break;
			}
		request.setAttribute("books", bs);
		return "/BookList2.jsp";
	}
	
	public String list(HttpServletRequest request, HttpServletResponse response) {
		List<Book> temp = mgr.search();
		request.setAttribute("books", temp);
		return "/BookList2.jsp";
	}
	
	public String view(HttpServletRequest request, HttpServletResponse response) {
		String bookisbn = request.getParameter("isbn");
		Book book = mgr.search(bookisbn);
		request.setAttribute("book", book);
		return "/BookView.jsp";
	}
	
	public String delete(HttpServletRequest request, HttpServletResponse response) {
		Book b = (Book) request.getAttribute("deleteBook");
		System.out.println(request.getParameter("isbn"));
		try {
			mgr.delete(request.getParameter("isbn"));
			request.setAttribute("msg", "삭제");
			return "/Result.jsp";
		} catch (Exception e) {
			request.setAttribute("msg", "삭제시 에러가 발생했습니다.");
			return "/Error.jsp";
		}
	}
	
	public String save(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Book b = new Book();
		String booknum1 = request.getParameter("booknum1");
		String booknum2 = request.getParameter("booknum2");
		String booknum3 = request.getParameter("booknum3");
		b.setIsbn(booknum1+"-"+booknum2+"-"+booknum3);
		b.setTitle(request.getParameter("bookname"));
		b.setCatalogue(request.getParameter("bookclass"));
		b.setNation(request.getParameter("radio1"));
		b.setPublishDate(request.getParameter("date"));
		b.setPublisher(request.getParameter("bookpublisher"));
		b.setAuthor(request.getParameter("auther"));
		b.setPrice(request.getParameter("price"));
		b.setDescription(request.getParameter("contents"));
		
		//북추가
		mgr.add(b);
		
		request.setAttribute("msg", "저장");
		return "/Result.jsp";
	}
}
