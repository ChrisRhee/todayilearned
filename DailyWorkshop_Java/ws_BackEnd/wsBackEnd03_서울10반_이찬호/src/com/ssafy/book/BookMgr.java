package com.ssafy.book;

import java.util.ArrayList;
import java.util.List;

public class BookMgr {
	private List<Book> books  = new ArrayList<>();
	
	public void add(Book b) {
		books.add(b);
	}
	
	public List<Book> search(){
		List<Book> temp = new ArrayList<>(books);
		return temp;
	}
	
	public Book search(String isbn){
		Book temp = new Book();
		for(int i=0; i<books.size(); i++) {
			temp = books.get(i);
			if(temp.getIsbn().equals(isbn)) {
				return temp;
			}
		}
		return null;
	}
	
	public void delete(String isbn) throws Exception {
		Book temp = search(isbn);
		if(temp != null)
			books.remove(search(isbn));
		else
			throw new Exception();
	}
	
	public List<Book> searchByTitle(String title) {
		List<Book> b = new ArrayList<>();
		for(int i=0; i<books.size(); i++) {
			if(books.get(i).getTitle().contains(title)) {
				b.add(books.get(i));
			}
		}
		return b;
	}
	
	public List<Book> searchByPublisher(String publisher) {
		List<Book> b = new ArrayList<>();
		for(int i=0; i<books.size(); i++) {
			if(books.get(i).getPublisher().contains(publisher)) {
				b.add(books.get(i));
			}
		}
		return b;
	}
	
	public List<Book> searchByPrice(String price) {
		List<Book> b = new ArrayList<>();
		for(int i=0; i<books.size(); i++) {
			if(books.get(i).getPrice().contains(price)) {
				b.add(books.get(i));
			}
		}
		return b;
	}
}
