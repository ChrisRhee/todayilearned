<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.ssafy.book.Book"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% List<Book> bss = (List<Book>) request.getAttribute("books");
%>

<h1>도서 목록 화면</h1>
<div>
<form method="post" action="controller?cmd=search">
	<select name="search">
		<option value="whole">전체</option>
		<option value="bookname">도서명</option>
		<option value="publisher">출판사</option>
		<option value="price">가격</option>
	</select> 
	<input type="text" name="searchFD">
	<input type="submit" name= "searchButton" value="검색">
</form>
</div>

<table class = "resultTable">
	<tr>
		<td class="book-number">도서번호</td>
		<td class="book-name">도서명</td>
		<td class="book-catalogue">도서분류</td>
		<td class="book-author">저자</td>
	</tr>
	<c:choose>
		<c:when test="${empty books}"></c:when>
		<tr><td colspan="4">원하는 목록 없습니다.</td></tr>
		<c:otherwise>
			<c:forEach var="bs" items="${books}">
				<tr>
					<td class='book-number'>${bs.isbn}</td>
					<td class='book-name'><a href="controller?cmd=BookView&isbn=${bs.isbn}">${bs.title}</a></td>
					<td class='book-catalogue'>${bs.catalogue()}</td>
					<td class='book-author'>${bs.author()}</td>
				</tr>
			</c:forEach>
		</c:otherwise>
	</c:choose>
</table>

<div>
	<a href="controller?cmd=BookAdd">추가 등록</a>
</div>