<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<% String msg = (String) request.getAttribute("msg"); %>
<h1>결과 페이지</h1>
<h4>정상적으로 <%= msg %> 되었습니다</h4>
<div>
	<a href="controller?cmd=BookAdd">추가 등록</a>
	<a href="controller?cmd=BookList">도서목록</a>
</div>