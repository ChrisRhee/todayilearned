<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.ssafy.book.Book"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% List<Book> bs = (List<Book>) request.getAttribute("books");
%>

<h1>도서 목록 화면</h1>
<div>
<form method="post" action="controller?cmd=search">
	<select name="search">
		<option value="whole">전체</option>
		<option value="bookname">도서명</option>
		<option value="publisher">출판사</option>
		<option value="price">가격</option>
	</select> 
	<input type="text" name="searchFD">
	<input type="submit" name= "searchButton" value="검색">
</form>
</div>

<table class = "resultTable">
	<tr>
		<td class="book-number">도서번호</td>
		<td class="book-name">도서명</td>
		<td class="book-catalogue">도서분류</td>
		<td class="book-author">저자</td>
	</tr>
	<% for(int i=0; i<bs.size(); i++){ 
	%>
		<tr><td class='book-number'><%=bs.get(i).getIsbn()%></td>
		<td class='book-name'>
			<a href="controller?cmd=BookView&isbn=<%=bs.get(i).getIsbn()%> "> <%=bs.get(i).getTitle()%></a></td>
		<td class='book-catalogue'><%=bs.get(i).getCatalogue()%></td>
		<td class='book-author'><%=bs.get(i).getAuthor()%></td></tr>
	<%
	}
	%>
</table>

<div>
	<a href="controller?cmd=BookAdd">추가 등록</a>
</div>