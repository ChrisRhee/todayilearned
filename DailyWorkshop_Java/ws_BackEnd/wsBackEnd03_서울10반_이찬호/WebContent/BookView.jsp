<%@page import="com.ssafy.book.Book"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("utf-8"); 
Book book = (Book) request.getAttribute("book"); 
%>
<h1>입력된 도서 정보</h1>
<table class = "resultTable">
	<tr>
		<td class="result-info" colspan="2">도서 정보</td>
	</tr>
	<tr>
		<td class="result-label">도서명</td>
		<td class="book-name"><%= book.getTitle() %></td>
	</tr>
	<tr>
		<td class="result-label">도서번호</td>
		<td class="book-number"><%= book.getIsbn() %></td>
	</tr>
	<tr>
		<td class="result-label">도서분류</td>
		<td class="book-class"><%= book.getCatalogue() %></td>
	</tr>
	<tr>
		<td class="result-label">도서국가</td>
		<td class="book-nation"><%= book.getNation() %></td>
	</tr>
	<tr>
		<td class="result-label">출판일</td>
		<td class="book-date"><%= book.getPublishDate() %></td>
	</tr>
	<tr>
		<td class="result-label">출판사</td>
		<td class="book-publisher"><%= book.getPublisher() %></td>
	</tr>
	<tr>
		<td class="result-label">저자</td>
		<td class="book-auther"><%= book.getAuthor() %></td>
	</tr>
	<tr>
		<td class="result-label">도서가격</td>
		<td class="book-price"><%= book.getPrice() %></td>
	</tr>
	<tr>
		<td class="result-label">도서설명</td>
		<td class="book-contents"><%= book.getDescription() %></td>
	</tr>
</table>

<div>
	<a href="controller?cmd=BookList">도서목록으로</a>
	<a href="controller?cmd=BookList">돌아가기</a>
	<a href="controller?cmd=BookDelete&isbn=<%=book.getIsbn()%> ">도서삭제</a>
</div>

