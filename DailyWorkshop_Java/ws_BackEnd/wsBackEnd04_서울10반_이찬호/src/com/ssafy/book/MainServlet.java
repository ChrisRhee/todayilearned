 package com.ssafy.book;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import org.apache.catalina.Session;

@WebServlet("/controller")
public class MainServlet extends HttpServlet {
	private BookMgr mgr = new BookMgr();
	private UserDAO userDao = new UserDAO();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
	public void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; Charset=utf-8");
		String url= "index.html";
		//1. 요청받은 데이터 부분
		String cmd = request.getParameter("cmd");
		System.out.println("cmd:"+cmd);
		//2. JavaBean을 통해 처리
		switch(cmd) {
		case "LoginUI":
			url = "/Login.html";
			break;
		case "LogOut":
			url = logout(request);
			break;
		case "loginAction":
			url = login(request);
			break;
		case "BookAdd":
			url = bookAdd(request);
			break;
		case "Result":
			url = save(request, response);
			break;
		case "BookList":
			url = list(request);
			break;
		case "search":
			url = search(request);
			break;
		case "BookView":
			url = view(request);
			break;
		case "BookDelete":
			url = delete(request);
			break;
		case "EndBook":
			url = endBook(request);
			break;
		}
		
		//3. JSP 페이지로 이동
		if(url.charAt(0) == '/') {
			request.getRequestDispatcher(url).forward(request, response);
		}else {
			response.sendRedirect(url);
		}
	}
	
	private String logout(HttpServletRequest request) {
		HttpSession session = request.getSession();
		if(session != null) {
			session.invalidate();
		}
		return "/Login.html";
	}
	
	private String bookAdd(HttpServletRequest request) {
		return "/Book.jsp";
	}

	private String login(HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		String userId = userDao.login(id, pw);
		//로그인 성공
		if(userId != null) {
			request.setAttribute("msg", userId);
			return "/LoginSuccess.jsp";
		}
		else {
			request.setAttribute("msg", "로그인에 실패했습니다.");
			return "/Error.jsp";
		}
	}

	public String endBook(HttpServletRequest request){
    	Cookie[] cookies = request.getCookies();
    	Book book = new Book();
   		if(cookies!=null){
   			book = mgr.search(cookies[cookies.length-1].getValue());
   			request.setAttribute("book", book);
   			return "/BookView.jsp";
   		}
   		else {
	   		System.out.println("없을때");
	   		request.setAttribute("msg", "등록된 도서가 없거나, 도서 등록된 후 7일이 지났습니다.");
			return "/Result.jsp";
   		}
	}
	
	public String search(HttpServletRequest request) throws UnsupportedEncodingException {
		String sch = request.getParameter("search");
		String stext = request.getParameter("searchFD");
		List<Book> bs = new ArrayList<>();
		switch(sch){
			case "whole":
				bs = mgr.search();
				break;
			case "bookname":
				bs = mgr.searchByTitle(stext);
				break;
			case "publisher":
				bs = mgr.searchByPublisher(stext);
				break;
			case "price":
				bs = mgr.searchByPrice(stext);
				break;
			}
		request.setAttribute("books", bs);
		return "/BookList.jsp";
	}
	
	public String list(HttpServletRequest request) {
		List<Book> temp = mgr.search();
		request.setAttribute("books", temp);
		return "/BookList.jsp";
	}
	
	public String view(HttpServletRequest request) {
		String bookisbn = request.getParameter("isbn");
		Book book = mgr.search(bookisbn);
		request.setAttribute("book", book);
		return "/BookView.jsp";
	}
	
	public String delete(HttpServletRequest request) {
		Book b = (Book) request.getAttribute("deleteBook");
		System.out.println(request.getParameter("isbn"));
		try {
			mgr.delete(request.getParameter("isbn"));
			request.setAttribute("msg", "삭제");
			return "/Result.jsp";
		} catch (Exception e) {
			request.setAttribute("msg", "삭제시 에러가 발생했습니다.");
			return "/Error.jsp";
		}
	}
	
	public String save(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Book b = new Book();
		String booknum1 = request.getParameter("booknum1");
		String booknum2 = request.getParameter("booknum2");
		String booknum3 = request.getParameter("booknum3");
		b.setIsbn(booknum1+"-"+booknum2+"-"+booknum3);
		b.setTitle(request.getParameter("bookname"));
		b.setCatalogue(request.getParameter("bookclass"));
		b.setNation(request.getParameter("radio1"));
		b.setPublishDate(request.getParameter("date"));
		b.setPublisher(request.getParameter("bookpublisher"));
		b.setAuthor(request.getParameter("auther"));
		b.setPrice(request.getParameter("price"));
		b.setDescription(request.getParameter("contents"));
		
		Cookie c1 = new Cookie("endBook",b.getIsbn());
	    c1.setHttpOnly(true);
	    response.addCookie(c1);
	    
		//북추가
		mgr.add(b);
		
		request.setAttribute("msg", "저장");
		return "/Result.jsp";
	}
}
