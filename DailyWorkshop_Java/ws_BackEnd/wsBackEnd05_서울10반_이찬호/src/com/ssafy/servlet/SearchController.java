package com.ssafy.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.book.Book;
import com.ssafy.book.BookMgr;

public class SearchController implements Controller {
	private BookMgr mgr = BookMgr.getInstance();
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String sch = request.getParameter("search");
		String stext = request.getParameter("searchFD");
		System.out.println(sch+" "+stext);
		List<Book> bs = new ArrayList<>();
		switch(sch){
			case "whole":
				bs = mgr.search();
				break;
			case "bookname":
				bs = mgr.searchByTitle(stext);
				break;
			case "publisher":
				bs = mgr.searchByPublisher(stext);
				break;
			case "price":
				bs = mgr.searchByPrice(stext);
				break;
			}
		request.setAttribute("books", bs);
		request.getRequestDispatcher("/BookList.jsp").forward(request, response);
	}
}
