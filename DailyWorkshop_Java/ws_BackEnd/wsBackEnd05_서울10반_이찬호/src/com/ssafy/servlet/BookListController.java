package com.ssafy.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.book.Book;
import com.ssafy.book.BookMgr;

public class BookListController implements Controller {
	private BookMgr mgr = BookMgr.getInstance();
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Book> temp = mgr.search();
		request.setAttribute("books", temp);
		request.getRequestDispatcher("/BookList.jsp").forward(request, response);
	}
}
