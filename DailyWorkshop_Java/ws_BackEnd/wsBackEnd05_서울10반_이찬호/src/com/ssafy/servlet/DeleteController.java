package com.ssafy.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.book.Book;
import com.ssafy.book.BookMgr;

public class DeleteController implements Controller {
	private BookMgr mgr = BookMgr.getInstance();
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Book b = (Book) request.getAttribute("deleteBook");
		System.out.println(request.getParameter("isbn"));
		try {
			mgr.delete(request.getParameter("isbn"));
			request.setAttribute("msg", "삭제");
			request.getRequestDispatcher("/Result.jsp").forward(request, response);
		} catch (Exception e) {
			request.setAttribute("msg", "삭제시 에러가 발생했습니다.");
			request.getRequestDispatcher("/Error.jsp").forward(request, response);
		}
	}
}
