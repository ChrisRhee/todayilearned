package com.ssafy.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.book.Book;
import com.ssafy.book.BookMgr;
import com.ssafy.customer.CustomerDAO;

public class BookAddActionController implements Controller {
	private BookMgr mgr = BookMgr.getInstance();
	
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Book b = new Book();
		String booknum1 = request.getParameter("booknum1");
		String booknum2 = request.getParameter("booknum2");
		String booknum3 = request.getParameter("booknum3");
		b.setIsbn(booknum1+"-"+booknum2+"-"+booknum3);
		b.setTitle(request.getParameter("bookname"));
		b.setCatalogue(request.getParameter("bookclass"));
		b.setNation(request.getParameter("radio1"));
		b.setPublishDate(request.getParameter("date"));
		b.setPublisher(request.getParameter("bookpublisher"));
		b.setAuthor(request.getParameter("auther"));
		b.setPrice(request.getParameter("price"));
		b.setDescription(request.getParameter("contents"));
		
		Cookie c1 = new Cookie("endBook",b.getIsbn());
	    c1.setHttpOnly(true);
	    response.addCookie(c1);
		//북추가
		mgr.add(b);
		request.setAttribute("msg", "저장");
		request.getRequestDispatcher("/Result.jsp").forward(request, response);
	}

}
