package com.ssafy.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.customer.Customer;
import com.ssafy.customer.CustomerDAO;

public class LoginController implements Controller {
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 시작페이지 역할
		request.getRequestDispatcher("/login.html").forward(request, response);
	}
}
