package com.ssafy.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.book.Book;
import com.ssafy.book.BookMgr;
import com.ssafy.customer.Customer;
import com.ssafy.customer.CustomerDAO;

@WebServlet("*.do")
public class FrontControllerServlet extends HttpServlet {
	private Map<String, Controller> clist;
	private CustomerDAO dao = CustomerDAO.getInstance();
	private BookMgr mgr = BookMgr.getInstance();
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config); // WAS제공되는 config 유지
		baseInit();
		clist= new HashMap<>();
		clist.put("/logIn.do", new LoginController());
		clist.put("/logout.do", new LogOutController());
		clist.put("/loginAction.do", new LoginActionController());
		clist.put("/memberInsert.do", new MemberInsertController());
		clist.put("/memberInsertAction.do", new MemberInsertActionController());
		clist.put("/BookAdd.do", new BookAddController());
		clist.put("/BookAddAction.do", new BookAddActionController());
		clist.put("/BookList.do", new BookListController());
		clist.put("/BookView.do", new BookViewController());
		clist.put("/Result.do", new ResultController());
		clist.put("/Search.do", new SearchController());
		clist.put("/Delete.do", new DeleteController());
	}
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; Charset=utf-8");
		String cmd = request.getRequestURI().substring(request.getContextPath().length());
		System.out.println(cmd);
		System.out.println("frontControllerServlet");
		clist.get(cmd).excute(request, response);
	}
	
	private void baseInit() {
		Customer t = new Customer();
		t.setId("ssafy");
		t.setName("이찬호");
		t.setPw("1111");
		dao.addCustomer(t);
		
		Book b = new Book();
		b.setIsbn("111-111-111");
		b.setTitle("이찬호의여행");
		b.setCatalogue("수필");
		b.setNation("해외");
		b.setPublishDate("2019-03-22");
		b.setPublisher("상어출판사");
		b.setAuthor("이찬호");
		b.setPrice("130000");
		b.setDescription("진짜 핵꿀잼입니다 이거");
		mgr.add(b);
		
		b = new Book();
		b.setIsbn("1-1-11");
		b.setTitle("이찬호의프로그래밍");
		b.setCatalogue("프로그래밍");
		b.setNation("국내");
		b.setPublishDate("2019-03-19");
		b.setPublisher("죠스출판사");
		b.setAuthor("이찬호");
		b.setPrice("100000");
		b.setDescription("진짜 핵꿀잼입니다 이거");
		mgr.add(b);
		
		b = new Book();
		b.setIsbn("22-22-21");
		b.setTitle("박규빈의프로그래밍");
		b.setCatalogue("프로그래밍");
		b.setNation("국내");
		b.setPublishDate("2019-03-19");
		b.setPublisher("상어출판사");
		b.setAuthor("박규빈");
		b.setPrice("20000");
		b.setDescription("진짜 핵꿀잼입니다 이거");
		mgr.add(b);
	}
}
