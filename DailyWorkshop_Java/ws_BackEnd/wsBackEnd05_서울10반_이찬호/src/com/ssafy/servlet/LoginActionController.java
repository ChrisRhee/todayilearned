package com.ssafy.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssafy.customer.CustomerDAO;

public class LoginActionController implements Controller {
	private CustomerDAO dao = CustomerDAO.getInstance();
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 시작페이지 역할
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		// 서버에서 처리할 내용 있으면 처리 하고
		System.out.println(id+" "+pw);
		// 실패
		if(dao.search(id)==null) {
			request.setAttribute("msg","["+id+"]는 존재하지 않는 아이디 입니다.");
			request.getRequestDispatcher("/Error.jsp").forward(request, response);
		}
		else { // 성공
			if(dao.search(id).getId().equals(id) && dao.search(id).getPw().equals(pw)) {
				HttpSession session = request.getSession(true);
				session.setAttribute("user", dao.search(id).getName());
				request.getRequestDispatcher("/LoginSuccess.jsp").forward(request, response);
			}else {
				request.setAttribute("msg","잘못 된 비밀번호입니다. 다시 입력해주세요.");
				request.getRequestDispatcher("/Error.jsp").forward(request, response);
			}
		}
	}
}
