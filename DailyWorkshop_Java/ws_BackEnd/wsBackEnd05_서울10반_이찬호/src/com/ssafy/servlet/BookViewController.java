package com.ssafy.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.book.Book;
import com.ssafy.book.BookMgr;

public class BookViewController implements Controller {
	private BookMgr mgr = BookMgr.getInstance();
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String bookisbn = request.getParameter("isbn");
		Book book = mgr.search(bookisbn);
		request.setAttribute("book", book);
		request.getRequestDispatcher("/BookView.jsp").forward(request, response);
	}

}
