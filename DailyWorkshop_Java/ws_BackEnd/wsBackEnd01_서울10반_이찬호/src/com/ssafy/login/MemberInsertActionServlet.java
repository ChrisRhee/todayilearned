package com.ssafy.login;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/memberInsertAction")
public class MemberInsertActionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//입력값 받기
//		Map<String, String[]> inputs = request.getParameterMap();
//		System.out.println(inputs);
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset =utf-8");
		Enumeration<String> names = request.getParameterNames();
		StringBuilder sb = new StringBuilder();
		while(names.hasMoreElements()) {
			String name = names.nextElement();
			String value = request.getParameter(name);
			if(name.equals("h")) {
				String [] vs = request.getParameterValues(name);
				if(vs.length>=2) {
					for(int i=1; i<vs.length; i++) {
						value += vs[i]+" ";
					}
				}
			}
			sb.append(name).append(" <input value='").append(value).append("'><br>");
		}
		//화면에 출력하즈아
		PrintWriter out = response.getWriter();
		out.println(sb.toString());
		out.flush();
		out.close();
	}
}
