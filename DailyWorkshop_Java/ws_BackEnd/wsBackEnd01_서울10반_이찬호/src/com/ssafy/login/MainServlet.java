package com.ssafy.login;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/login")
public class MainServlet extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doPost work");
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		if(id.equals("ssafy") && pw.equals("1111")) {
			// 관리자 전용 페이지로 이동
			response.sendRedirect("Result.html");
		}else {
			// 에러 페이지 브라우저에 전송
			response.sendRedirect("Login.html");
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doGet work");
		response.setCharacterEncoding("euc-kr");
		String booknum1 = request.getParameter("booknum1");
		String booknum2 = request.getParameter("booknum2");
		String booknum3 = request.getParameter("booknum3");
		String bookname = request.getParameter("bookname");
		String bookclass = request.getParameter("bookclass");
		String radio1 = request.getParameter("radio1");
		String date = request.getParameter("date");
		String bookpublisher = request.getParameter("bookpublisher");
		String auther = request.getParameter("auther");
		String price = request.getParameter("price");
		String contents = request.getParameter("contents");
		System.out.println(bookclass+" "+radio1+" "+bookpublisher);
		PrintWriter out = response.getWriter();
		out.print("<h1>도서정보</h1>");
		out.print("<span>Book Name : </span><span>"+bookname+"</span><br>");
		out.print("<span>Book Number : </span><span>"+booknum1+"-"+booknum2+"-"+booknum3+"</span><br>");
		out.print("<span>Book Class : </span><span>"+bookclass+"</span><br>");
		out.print("<span>Book Nation : </span><span>"+radio1+"</span><br>");
		out.print("<span>출판일 : </span><span>"+date+"</span><br>");
		out.print("<span>출판사 : </span><span>"+bookpublisher+"</span><br>");
		out.print("<span>저자 : </span><span>"+auther+"</span><br>");
		out.print("<span>도서가격 : </span><span>"+price+"</span><br>");
		out.print("<span>도서설명 : </span><span>"+contents+"</span><br>");
		
		out.flush();
		out.close();
	}
}
