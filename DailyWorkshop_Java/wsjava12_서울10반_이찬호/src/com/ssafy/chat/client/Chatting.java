package com.ssafy.chat.client;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.net.*;
import java.util.*;

import javax.swing.*;

class ImageWindow extends JDialog{
    public static JLabel jlb = new JLabel("");
    private JButton jb= new JButton("전송");
    public ImageWindow(JLabel temp){
        getContentPane().add(jlb);
        getContentPane().add(jb, BorderLayout.SOUTH);
        this.setLocation((int) (Chatting.f.getX()+Chatting.f.getSize().getWidth()),Chatting.f.getY());
        this.pack();
        //this.setModal(true);
        this.setVisible(true);
        jb.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					System.out.println("보냈다"+temp.toString());
					Chatting.oos.writeObject(temp);
					Chatting.oos.flush();
					this.setVisible(false);
				} catch (IOException e1) {
					System.out.println("왜터짐?");
					//e1.printStackTrace();
				}
			}

			private void setVisible(boolean b) {
				
			}
		});
    }
}

public class Chatting {
	public static Frame f = new Frame("Chatting");
	private TextArea ta = new TextArea();
	private JTextField tf = new JTextField();
	private Panel p = new Panel();
	@SuppressWarnings("rawtypes")
	private JList list = new JList();

	private JLabel imgl = new JLabel();
	JButton b;
	
	private Socket s = null;
	public static ObjectOutputStream oos = null;
	private ObjectInputStream ois = null;
	private String ip;
	private int port;
	private String name;
	
	private ImageWindow iw;

	public Chatting() {
		ip = "70.12.109.166";
		//ip = "127.0.0.1";
		port = 8000;
		this.name = "이찬호";
		createGUI();
		addEvent();
		go(ip, port, this.name);
	}
	
	public static void main(String[] args) {
		new Chatting();
	}
	
	public void createGUI() {
		//ta.setFocusable(false);
		tf.setFocusable(true);
		p.add(list);
		f.add(ta, BorderLayout.WEST);
		f.add(p, BorderLayout.EAST);
		f.add(tf, BorderLayout.SOUTH);
		f.setBounds(200, 200, 480, 400);
		// f.setSize(500, 400);
		f.setIconImage(Toolkit.getDefaultToolkit().getImage("img/safe.png"));
		f.setVisible(true);
		tf.requestFocusInWindow();
	}

	private void addEvent() {
		tf.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				String s = tf.getText();
				if (!s.equals(""))
					if(s.equals("/clear") || s.equals("/clean") || s.equals("/c")|| s.equals("/ㅊ")) {
						ta.setText(" *** Cleaned! *** ");
						ta.append("\n");
					}else if(s.contains("/닉") || s.contains("/nick") || s.contains("/닉변")) {
						String ss[] = s.split(" ");
						ta.append(" *** 닉네임이 ["+ss[1]+"]로 변경되었습니다. ***");
						ta.append("\n");
						name = ss[1];
					}
					else {
						try {
							oos.writeObject("[" + name + "] : " + s);
							oos.flush();
						} catch (IOException e) {
							e.printStackTrace();
							System.exit(1);
						}
					}
				tf.setText("");
			}
		});
		tf.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
					System.exit(0);
				if (e.isControlDown())
					if (e.getKeyCode() == 87)
						System.exit(0);
				//ctrl v
				if (e.isControlDown())
					if (e.getKeyCode() == 86 || e.getKeyCode() == 118) {
						try {
							imgl = new JLabel(new ImageIcon( (BufferedImage) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.imageFlavor)));
							ImageWindow.jlb.setText("");
							ImageWindow.jlb.setIcon(imgl.getIcon());
							iw = new ImageWindow(imgl);
							System.out.println(f.getX()+" "+f.getSize().getWidth()+" "+f.getY()+" "+f.getSize().getHeight()+" x오른쪽값 "+(f.getX()+f.getSize().getWidth()));
							//System.out.println(imgl);
						} catch (HeadlessException e1) {
							System.out.println("오류1");
						} catch (UnsupportedFlavorException e1) {
							//글씨면
							try {
								imgl = new JLabel((String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor));
							} catch (HeadlessException e2) {
								System.out.println("오류1");
							} catch (UnsupportedFlavorException e2) {
								System.out.println("오류2");
							} catch (IOException e2) {
								System.out.println("오류3");
							}
						} catch (IOException e1) {
							System.out.println("오류3");
						}
					}
			}
		});
		ta.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.isControlDown())
					if (e.getKeyCode() == 87)
						System.exit(0);
				if(e.getKeyCode()==KeyEvent.VK_ESCAPE)
					System.exit(0);
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == 9) {
					//System.out.println(e.getKeyCode());
					tf.requestFocusInWindow();
					ta.append("\n");
				}
			}

		});
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}

	public void go(String ip, int port, String name){
		try {
			s = new Socket(ip, port);
			oos = new ObjectOutputStream(s.getOutputStream());
			ois = new ObjectInputStream(s.getInputStream());
			new ChatClientThread(ois).start();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			System.out.println(" *** 서버가 닫혀있습니다. *** ");
			ta.append(" *** 서버가 닫혀있습니다. *** ");
			//e1.printStackTrace();
		}
	}

	class ChatClientThread extends Thread {
		private ObjectInputStream ois = null;

		public ChatClientThread(ObjectInputStream ois) {
			this.ois = ois;
		}

		@SuppressWarnings("unchecked")
		@Override
		public void run() {
			Object data;
			try {
				while (true) {
					data = ois.readObject();
					String msg = "";
					//alarm();
					if (data instanceof ArrayList) {
						dataUpdate((ArrayList<String>) data);
						continue;
					} else if (data instanceof String) {
						msg = (String) data;
					} else if (data instanceof JLabel) {
						iw = new ImageWindow((JLabel) data);
					}
					ta.append(msg + "\n");
				}
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				ta.append(" *** 서버와 접속이 종료되었습니다. *** ");
				//e1.printStackTrace();
			}
		}
		
		private void setWindow(ArrayList<String> o) {
			int max = Integer.MIN_VALUE;
			for (String string : o) {
				if(max < string.length()) max = string.length();
			}
			f.setSize(480+max*11, 400);
		}

		private void alarm() {
			f.setVisible(true);
		}

		@SuppressWarnings("unchecked")
		public void dataUpdate(ArrayList<String> o) {
			list.removeAll();
			list.setListData(o.toArray());
			setWindow(o);
		}
	}
}

