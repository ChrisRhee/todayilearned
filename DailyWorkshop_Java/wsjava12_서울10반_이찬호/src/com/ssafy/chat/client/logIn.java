package com.ssafy.chat.client;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JLabel;
import javax.swing.JTextField;

public class logIn{
	private Frame f = new Frame();
	private JLabel tl = new JLabel("닉네임 입력하세요."); 
	private JTextField tf = new JTextField();
	
	public logIn() {
		createGUI();
		addEvent();
	}
	public void createGUI() {
		f.add(tl, BorderLayout.NORTH);
		f.add(tf, BorderLayout.SOUTH);
		tf.setFocusable(true);
		f.setBounds(200, 200, 200, 200);
		// f.setSize(500, 400);
		f.setVisible(true);
	}
	private void addEvent() {
		tf.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				String name = tf.getText();
				if (!name.equals("")) {
					f.dispose();
					new Chatting();
				} else {
					tl.setText("입력해라!ㅡ.ㅡ");
					tl.setForeground(Color.RED);
				}
				tf.setText("");
			}
		});
		tf.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
					System.exit(0);
				if (e.isControlDown())
					if (e.getKeyCode() == 87)
						System.exit(0);
			}
		});
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		f.setIconImage(Toolkit.getDefaultToolkit().getImage("img/ssafy.png"));

		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

	}
	public static void main(String[] args) {
		new logIn();
	}
}
