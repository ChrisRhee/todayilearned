package game;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GameUI {
	private JFrame f = new JFrame();
	private JButton shop = new JButton("상점");
	private JButton rein = new JButton("강화");
	private JPanel buttonPanel = new JPanel();
	private JLabel totalMoney = new JLabel();
	private JLabel needMoney = new JLabel();
	private JLabel reinMoney = new JLabel();
	private JLabel swardImg;
	private JLabel possible = new JLabel();;
	
	private JPanel moneyPanel = new JPanel();
	private JPanel shopPanel =new JPanel();
	
	private JPanel northP = new JPanel();
	private JPanel centerP = new JPanel();
	private JPanel southP = new JPanel();
	
	
	public void createGUI() {
		moneyPanel.add(totalMoney);
		moneyPanel.add(needMoney);
		moneyPanel.add(reinMoney);
		shopPanel.add(shop);
		buttonPanel.add(rein);
		buttonPanel.add(shop);
		centerP.add(moneyPanel,BorderLayout.WEST);
		centerP.add(shopPanel,BorderLayout.EAST);
		centerP.add(swardImg, BorderLayout.CENTER);
		centerP.add(possible, BorderLayout.SOUTH);
		f.add(buttonPanel, BorderLayout.SOUTH);
		f.add(centerP, BorderLayout.CENTER);
		f.setBounds(200, 200, 200, 200);
		f.setVisible(true);
	}
	
	public GameUI() {
		setMain();
		createGUI();
		addEvent();
	}
	
	private void setMain() {
		totalMoney.setText("12345");
		needMoney.setText("100");
		reinMoney.setText("1000");
		
		Icon icon = new ImageIcon("img/icon.png");
		swardImg = new JLabel("", icon, JLabel.CENTER);
	}

	private void addEvent() {
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		f.setIconImage(Toolkit.getDefaultToolkit().getImage("ssafy.png"));
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}
	
	public static void main(String[] args) {
		new GameUI();
	}
}
