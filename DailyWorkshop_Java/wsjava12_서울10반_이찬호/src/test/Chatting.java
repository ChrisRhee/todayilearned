package test;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.swing.JList;
import javax.swing.JTextField;

public class Chatting {
	private Frame f = new Frame("Chatting");
	private TextArea ta = new TextArea();
	private JTextField tf = new JTextField();
	private Panel p = new Panel();
	@SuppressWarnings("rawtypes")
	private JList list = new JList();

	private Socket s = null;
	private ObjectOutputStream oos = null;
	private ObjectInputStream ois = null;
	private String ip;
	private int port;
	private String name;
	
	public Chatting() {
		ip = "70.12.109.166";
		//ip = "127.0.0.1";
		port = 8000;
		this.name = "테스트";
		createGUI();
		addEvent();
		go(ip, port, this.name);
	}
	
	public static void main(String[] args) {
		new Chatting();
	}
	
	public void createGUI() {
		//ta.setFocusable(false);
		tf.setFocusable(true);
		p.add(list);
		f.add(ta, BorderLayout.WEST);
		f.add(p, BorderLayout.CENTER);
		f.add(tf, BorderLayout.SOUTH);
		f.setBounds(200, 200, 480, 400);
		// f.setSize(500, 400);
		f.setIconImage(Toolkit.getDefaultToolkit().getImage("img/safe.png"));
		f.setVisible(true);
		tf.requestFocusInWindow();
	}

	private void addEvent() {
		tf.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				String s = tf.getText();
				if (!s.equals(""))
					if(s.equals("/clear") || s.equals("/clean") || s.equals("/c")|| s.equals("/ㅊ")) {
						ta.setText(" *** Cleaned! *** ");
						ta.append("\n");
					}else if(s.contains("/닉") || s.contains("/nick") || s.contains("/닉변")) {
						String ss[] = s.split(" ");
						ta.append(" *** 닉네임이 ["+ss[1]+"]로 변경되었습니다. ***");
						ta.append("\n");
						name = ss[1];
					}
					else {
						try {
							oos.writeObject("[" + name + "] : " + s);
							oos.flush();
						} catch (IOException e) {
							e.printStackTrace();
							System.exit(1);
						}
					}
				tf.setText("");
			}
		});
		tf.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
					System.exit(0);
				if (e.isControlDown())
					if (e.getKeyCode() == 87)
						System.exit(0);
			}
		});
		ta.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.isControlDown())
					if (e.getKeyCode() == 87)
						System.exit(0);
				if(e.getKeyCode()==KeyEvent.VK_ESCAPE)
					System.exit(0);
			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == 9) {
					System.out.println(e.getKeyCode());
					tf.requestFocusInWindow();
					ta.append("\n");
				}
			}

		});
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}
	

	public void go(String ip, int port, String name){
		try {
			s = new Socket(ip, port);
			oos = new ObjectOutputStream(s.getOutputStream());
			ois = new ObjectInputStream(s.getInputStream());
			new ChatClientThread(ois).start();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			System.out.println(" *** 서버가 닫혀있습니다. *** ");
			ta.append(" *** 서버가 닫혀있습니다. *** ");
			//e1.printStackTrace();
		}
	}

	class ChatClientThread extends Thread {
		private ObjectInputStream ois = null;

		public ChatClientThread(ObjectInputStream ois) {
			this.ois = ois;
		}

		@SuppressWarnings("unchecked")
		@Override
		public void run() {
			Object data;
			try {
				while (true) {
					data = ois.readObject();
					String msg = "";
					//alarm();
					if (data instanceof ArrayList) {
						dataUpdate((ArrayList<String>) data);
						continue;
					} else if (data instanceof String)
						msg = (String) data;
					ta.append(msg + "\n");
				}
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				ta.append(" *** 서버와 접속이 종료되었습니다. *** ");
				//e1.printStackTrace();
			}
		}
		
		private void setWindow(ArrayList<String> o) {
			int max = Integer.MIN_VALUE;
			for (String string : o) {
				if(max < string.length()) max = string.length();
			}
			f.setSize(480+max*11, 400);
		}

		private void alarm() {
			f.setVisible(true);
		}

		@SuppressWarnings("unchecked")
		public void dataUpdate(ArrayList<String> o) {
			list.removeAll();
			list.setListData(o.toArray());
			setWindow(o);
		}
	}
}

