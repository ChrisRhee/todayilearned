package test;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TestServer {
	private ArrayList<User> users = new ArrayList<>();
	private ArrayList<String> helps = new ArrayList<>();
	private List<String> lunchs = new ArrayList<>();
	// 아이피, 이름
	private Map<String,String> userNames = new HashMap<>();
	// 이름, 아이피
	private Map<String,String> userNickNames = new HashMap<>();
	
	private static int startDay = 18;
	
	private static int TODAY = Integer.parseInt(new SimpleDateFormat ("dd").format(System.currentTimeMillis()));
	
	private int port = 9000;
	private SimpleDateFormat format1 = new SimpleDateFormat ("HH:mm");
	private SimpleDateFormat lunchDate = new SimpleDateFormat ("MM:dd");
	public void go() {
		try {
			ServerSocket ss = new ServerSocket(port);
			System.out.println("ServerSocket port = "+port);
			
			// 명령어 등록, 점심 등록 
			helplist();
			getLunchListFromFile();
			getUserList();
			
			while(true) {
				Socket s = ss.accept();
				System.out.println("Sockect Accepted()");
				ObjectInputStream ois=new ObjectInputStream(s.getInputStream());
				ObjectOutputStream oos=new ObjectOutputStream(s.getOutputStream());
				User u = new User(s,ois,oos);
				System.out.println("ois oos created");
				// 테스트 
				users.add(u);
				System.out.println(userNames.get(u)+"님 접속.");
				new ChatServerThread(ois).start();
				System.out.println("ChatServerThread Start");
				broadcast("   *** "+userNames.get(u)+"님이 입장하셨습니다. ***");
				broadcast(users);
				// 입장 메세지
//				if(users.isEmpty()) {
//					System.out.println("비었다");
//					users.add(u);
//					System.out.println(userNames.get(u.getS().getInetAddress().toString())+"님은 접속.");
//					new ChatServerThread(ois).start();
//					System.out.println("ChatServerThread Start");
//					// 입장 메세지
//					broadcast("   *** "+userNames.get(u.getS().getInetAddress().toString())+"님이 입장하셨습니다. ***");
//					broadcast(users);
//				}
//				else {
//					for (User user : users) {
//						if(user.equals(u)) {
//							System.out.println(userNames.get(u)+"님은 이미 존재합니다.");
//							u.getOos();
//							oos.writeObject(userNames.get(u.getS().getInetAddress().toString())+"님은 이미 접속했습니다");
//							oos.flush();
//						}else {
//							users.add(u);
//							System.out.println(userNames.get(u.getS().getInetAddress().toString())+"님은 접속.");
//							new ChatServerThread(ois).start();
//							System.out.println("ChatServerThread Start");
//							// 입장 메세지
//							broadcast("   *** "+userNames.get(u.getS().getInetAddress().toString())+"님이 입장하셨습니다. ***");
//							broadcast(users);
//						}
//					}
//				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void broadcast(ArrayList<User> list) {
		ArrayList<String> tmp = new ArrayList<>();
		for (User u : list) {
			tmp.add(userNames.get(u.toString()));
		}
		Collections.sort(tmp);
		for (int i = 0; i < users.size(); i++) {
			User u = users.get(i);
			ObjectOutputStream oos = u.getOos();
			try {
				oos.writeObject(tmp);
				oos.flush();
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void broadcast(String msg) {
		for (int i = 0; i < users.size(); i++) {
			User u = users.get(i);
			ObjectOutputStream oos = u.getOos();
			try {
				oos.writeObject(msg);
				//oos.writeObject(users);
				oos.flush();
			}catch(IOException e) {
				System.out.println(userNames.get(u)+"님이 퇴장했습니다.");
				//e.printStackTrace();
			}
		}
	}
	
	public void removeClient(ObjectInputStream ois) {
		for (int i = 0; i < users.size(); i++) {
			User u = users.get(i);
			ObjectInputStream uois=u.getOis();
			if(uois == ois) {
				try {
					uois.close();
					ObjectOutputStream uoos = u.getOos();
					uoos.close();
					Socket us = u.getS();
					// 퇴장 메세지
					broadcast("   *** "+userNames.get(us.getInetAddress().toString())+"님이 퇴장하셨습니다. ***");
					us.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				users.remove(u);
				broadcast(users);
				System.out.println("User Removed");
			}
		}
	}
	
	public void helplist() {
		helps.add(" *** 명령어 *** ");
		helps.add("/help : 사용가능한 명령어를 보여줍니다.");
		helps.add("/도움말 : 사용가능한 명령어를 보여줍니다.");
		helps.add("/h : 사용가능한 명령어를 보여줍니다.");
		
		helps.add("/점심 : 오늘 점심을 보여줍니다.");
		
		helps.add("/내일 점심 : 내일 점심을 보여줍니다.");
	}
	
	public void getHelp() {
		for (String string : helps) {
			broadcast(string);
		}
	}
	
	// 현재 접속자들을 다 보여주고 총 인원수를 보여준다.
	public void getWhom() {
		broadcast(" *** 현재 접속자 "+users.size()+"명 *** ");
		for (User user : users) {
			broadcast(userNames.get(user.getS().getInetAddress().toString()));
		}
	}

	// 좌석 배치에 맞는 이름들을 파일에서 읽어와서 userNames에 저장한다.
	public void getUserList() throws IOException {
		try {
			System.setIn(new FileInputStream("res/users.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		for (int i = 0; i <25; i++) {
			String user[] = br.readLine().split(" ");
			System.out.println(Arrays.toString(user));
			userNames.put(user[0], user[1]);
		}
	}
	
	//점심 메뉴를 파일에서 읽어와서 lunchs에 저장한다.
	public void getLunchListFromFile() {
		Charset cs = StandardCharsets.UTF_8;
		//Path path = Paths.get("C:\\Users\\student\\Desktop\\chris\\todayilearned\\DailyWorkshop_Java\\wsjava12_서울10반_이찬호\\res\\lunch.txt");
		Path path = Paths.get("C:\\Users\\student\\Desktop\\SSAFY\\todayilearned\\DailyWorkshop_Java\\wsjava12_서울10반_이찬호\\res\\lunch.txt");
		try {
			lunchs = Files.readAllLines(path,cs);
			lunchClean();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//점심 메뉴에 있는 휴무, 띄어쓰기, 공맥 문자 등을 삭제 해서 저장한다.
	public void lunchClean() {
		List<String> temp = new ArrayList<String>();
		for (String string : lunchs) {
			if(string.equals(" ") || 
					string.length()<=1 ||
					string.equals("　") || 
					string.equals("휴무")|| 
					string.equals("구정연휴")|| 
					string.equals("신정") ||
					string.equals("external_image")
					)
			{
				continue;
			}
			else {
				temp.add(string);
			}
		}
		lunchs = new ArrayList<String>();
		for (String string : temp) {
			lunchs.add(string.trim());
		}
	}
	
	// 날짜를 입력하면 오늘의 식단이 화면에 broadcast 된다. 날짜+1 하면 내일 메뉴가 출력됨.
	public void getTodayLunch(int date) {
		int dayss = (lunchs.size()-2)/16;
		int today = date - startDay;
		int count = 0;
		broadcast(lunchDate.format(System.currentTimeMillis()).substring(0, 2)+"월 "+ date+"일 식단");
		for (String menu : lunchs) {
			if(menu.contains("kcal")){
				continue;
			}
			else if( menu.contains("코스") ) {
				broadcast(" *** "+menu+ " *** ");
				System.out.println(menu);
			}else {
				if(count++%dayss==today) {
					broadcast(menu);
				}
			}
		}
		broadcast("");
	}
	
	public static void main(String[] args) {
		new TestServer().go();
	}
	
	class ChatServerThread extends Thread{
		private ObjectInputStream ois;
		
		public ChatServerThread(ObjectInputStream ois) {
			this.ois = ois;
		}
		
		@Override
		public void run() {
			super.run();
			String msg;
			
			try {
				while(true) {
					msg = (String)ois.readObject();
					if(msg.equals("")) {
					}
					else {
						broadcast(msg+" ("+format1.format(System.currentTimeMillis())+")");
						if(msg.contains("/ㅈ") || msg.contains("/접속자") ||msg.contains("/total")  || msg.equals("/w")) {
							broadcast(" *** 현재 접속자 "+users.size()+"명 *** ");
						}else if(msg.contains("/help") || msg.contains("/도움말") || msg.contains("/h")|| msg.contains("/?")) {
							getHelp();
						}else if(msg.contains("/점심")||msg.contains("/wjatla")||msg.contains("/lunch")||msg.contains("/식단")||msg.contains("/tlreks")) {
							getTodayLunch(TODAY);
						}else if(msg.contains("/내일점심")||msg.contains("/ㄵ")||msg.contains("/내일 점심")||msg.contains("/sw")||msg.contains("/tlreks")) {
							getTodayLunch(TODAY+1);
						}else if(msg.contains("/ㄴㄱ") || msg.contains("/누구") || msg.contains("/who") || msg.contains("/snrn") || msg.contains("/sr")) {
							getWhom();
						}
					}
				}
			} catch (ClassNotFoundException e1) {
				System.out.println("접속이 끊어진것 같다 애미야");
				//e1.printStackTrace();
			} catch (IOException e1) {
				// 연결끊어짐?
				removeClient(ois);
			}
		}
	}
}

