package test;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServerTest {
	private static List<String> lunchs = new ArrayList<String>();
	private static Map<String,String> userNames = new HashMap<>();
	private static int TODAY = Integer.parseInt(new SimpleDateFormat ("dd").format(System.currentTimeMillis()));
	
	public static void getLunchListFromFile() {
		Charset cs = StandardCharsets.UTF_8;
		Path path = Paths.get("C:\\Users\\student\\Desktop\\chris\\todayilearned\\DailyWorkshop_Java\\wsjava12_서울10반_이찬호\\res\\lunch.txt");
		try {
			lunchs = Files.readAllLines(path,cs);
			lunchClean();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void lunchClean() {
		List<String> temp = new ArrayList<String>();
		for (String string : lunchs) {
			if(string.equals(" ") || 
					string.length()<=1 ||
					string.equals("　") || 
					string.equals("휴무")|| 
					string.equals("구정연휴")|| 
					string.equals("신정") ){
				continue;
			}
			else {
				temp.add(string);
			}
		}
		lunchs = new ArrayList<String>();
		for (String string : temp) {
			lunchs.add(string.trim());
		}
	}
	
	public static void getTodayLunch(int date) {
		int dayss = (lunchs.size()-2)/16;
		int today = date- TODAY;
		int count = 4;
		System.out.println(date+"일 식단");
		for (String menu : lunchs) {
			if(menu.contains("kcal")){
				continue;
			}
			else if(menu.equals("A코스")||menu.equals("B코스")) {
				System.out.println(menu);
			}else {
				if(count++%dayss==today) {
					System.out.println(menu);
				}
			}
		}
	}
	
	public static void getUserList() throws IOException {
		try {
			System.setIn(new FileInputStream("res/users.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		for (int i = 0; i <25; i++) {
			String user[] = br.readLine().split(" ");
			System.out.println(Arrays.toString(user));
			userNames.put(user[0], user[1]);
		}
	}
	
	public static void main(String[] args) throws IOException {
		getLunchListFromFile();
		getUserList();
		
		System.out.println((lunchs.size()-2)/16);
		System.out.println(lunchs);
		getTodayLunch(TODAY);
		System.out.println();
		
	}
}
