package test;

import java.awt.*;
import javax.swing.*;
import java.awt.datatransfer.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.*;
import java.io.IOException;

class imagetest extends JFrame{
	JLabel l;
	JLabel imgl;
	JPanel p;
	JButton b;
	JFrame f;
	public imagetest()
	{
		p = new JPanel(new GridLayout(0, 1));
		f = new JFrame();
		b = new JButton("보기");
		l = new JLabel();
		//imgl = new JLabel();
		b.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				//imgl.setText("hello");
				//System.out.println(imgl);
				//f.add(imgl, BorderLayout.WEST);
				try {
					System.out.println(new ImageIcon((BufferedImage)Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.imageFlavor)));
					imgl = new JLabel(new ImageIcon( (BufferedImage) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.imageFlavor)));
					l.setText("");
					l.setIcon(imgl.getIcon());
					//System.out.println(imgl);
					f.pack();
				} catch (HeadlessException e1) {
					System.out.println("오류1");
				} catch (UnsupportedFlavorException e1) {
					try {
						imgl = new JLabel((String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor));
						l.setIcon(null);
						l.setText(imgl.getText());
						f.pack();
					} catch (HeadlessException e2) {
						System.out.println("오류1");
					} catch (UnsupportedFlavorException e2) {
						System.out.println("오류2");
					} catch (IOException e2) {
						System.out.println("오류3");
					}
				} catch (IOException e1) {
					System.out.println("오류3");
				}
				System.out.println("넣었따");
				
			}
		});
		
		//	Set frame properties
		f.setTitle("Paste Image");
		f.setLayout(new BorderLayout());
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		f.setVisible(true);
		//l.setText("눌림");
		// Add the JLabel to the frame
		//f.add(imgl, BorderLayout.WEST);
		f.add(l, BorderLayout.CENTER);
		f.add(b, BorderLayout.EAST);
		// Pack it tightly!
		f.pack();
		// Center the JFrame
		f.setLocationRelativeTo(null);
	}
	public static void main(String args[])
	{
		new imagetest();
	}
}