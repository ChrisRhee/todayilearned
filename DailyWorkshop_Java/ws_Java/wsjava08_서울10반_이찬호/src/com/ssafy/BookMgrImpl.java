package com.ssafy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class BookMgrImpl implements IBookMgr{
	private Collection<Book> books;
	
	
	// 생성자
	public BookMgrImpl() {
		this(new HashSet());
	}
	
	public BookMgrImpl(Collection<Book> books) {
		this.books= books;
	}
	

	/** 1. 데이터 입력 기능 */
	@Override
	public boolean add(Book book) {
		return books.add(book);
	}

	/** 2. 데이터 전체 검색 기능  */
	@Override
	public Collection<Book> search() {
		Collection<Book> temp = new ArrayList();
		for (Book book : books) {
			temp.add(book);
		}
		return temp;
	}

	/** 3. Isbn 으로 정보를 검색하는 기능  */
	@Override
	public Book searchIsbn(String isbn) {
		for (Book book : books) {
			if(book.getIsbn().equals(isbn)) return book;
		}
		return null;
	}

	/** 4. Title로 정보를 검색하는 기능 (파라메터로 주어진 제목을 포함하는 모든 정보) */
	@Override
	public Collection<Book> searchTitle(String title) {
		Collection<Book> temp = new ArrayList();
		for (Book book : books) {
			if(book.getTitle().equals(title))
				temp.add(book);
		}
		return temp;
	}

	/** 5. Book만 검색하는 기능 */
	@Override
	public Collection<Book> searchBooks() {
		Collection<Book> temp = new ArrayList();
		for (Book book : books) {
			if(book instanceof Magazine)
			{}
			else {
				temp.add(book);
			}
		}
		// TODO Auto-generated method stub
		return temp;
	}

	/** 6. Magazine만 검색하는 기능 */
	@Override
	public Collection<Magazine> searchMagazines() {
		Collection<Magazine> temp = new ArrayList();
		for (Book book : books) {
			if(book instanceof Magazine) temp.add((Magazine)book);
		}
		return temp;
	}

	/** 7. Magazine중 올해 잡지만 검색하는 기능 */
	@Override
	public Collection<Magazine> searchMagazines(int year) {
		Collection<Magazine> temp = new ArrayList();
		for (Book book : books) {
			if(book instanceof Magazine && ((Magazine) book).getYear() == year) 
				temp.add((Magazine)book);
		}
		return temp;
	}
	
	/** 8. 출판사로 검색 */
	@Override
	public Collection<Book> searchPublisher(String publisher) {
		Collection<Book> temp = new ArrayList();
		for (Book book : books) {
			if(book.getPublisher().equals(publisher))
				temp.add(book);
		}
		return temp;
	}

	/** 9. 가격으로 검색 기능 (파라메터로 주어진 가격보다 낮은 도서 정보 검색)  */
	@Override
	public Collection<Book> searchPrice(int price) {
		Collection<Book> temp = new ArrayList();
		for (Book book : books) {
			if(book.getPrice() < price)
				temp.add(book);
		}
		return temp;
	}

	/** 10. 저장된 모든 도서의 금액 합계를 구하는 기능 */
	@Override
	public int totalPrice() {
		int total = 0;
		for (Book book : books) {
			total += book.getPrice();
		}
		return total;
	}

	/** 11. 저장된 모든 도서의 금액 평균을 구하는 기능 */
	@Override
	public int averagePrice() {
		int total = totalPrice();
		return total/books.size();
	}
	
}
