package com.ssafy;

import java.util.Collection;

public interface IBookMgr {
	/** 1. 데이터 입력 기능 */
	boolean add(Book book);
	
	/** 2. 데이터 전체 검색 기능  */
	Collection<Book> search();
	
	/** 3. Isbn 으로 정보를 검색하는 기능  */
	Book searchIsbn(String isbn);
	
	/** 4. Title로 정보를 검색하는 기능 (파라메터로 주어진 제목을 포함하는 모든 정보) */
	Collection<Book> searchTitle(String title);
	
	/** 5. Book만 검색하는 기능 */
	Collection<Book> searchBooks();
	
	/** 6. Magazine만 검색하는 기능 */
	Collection<Magazine> searchMagazines() ;
		
	/** 7. Magazine중 올해 잡지만 검색하는 기능 */
	Collection<Magazine> searchMagazines(int year);
	
	/** 8. 출판사로 검색 */
	Collection<Book> searchPublisher(String publisher);
	
	/** 9. 가격으로 검색 기능 (파라메터로 주어진 가격보다 낮은 도서 정보 검색)  */
	Collection<Book> searchPrice(int price);
	
	/** 10. 저장된 모든 도서의 금액 합계를 구하는 기능 */
	int totalPrice();
	
	/** 11. 저장된 모든 도서의 금액 평균을 구하는 기능 */
	int averagePrice();
}
