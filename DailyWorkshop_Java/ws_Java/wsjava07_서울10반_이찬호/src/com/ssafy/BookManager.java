package com.ssafy;

public class BookManager {
	private Book blist[] = new Book[100];
	private int index;
	
	/** Singleton */
	private static BookManager instance = new BookManager();
	private BookManager() {}
	public static BookManager getInstance() {
		return instance;
	}
	
	/** 1. 데이터 입력 기능
	 * 기능 : Book 배열 blist에 Book 객체를 추가한다.
	 * 입력 : Book 객체
	 * */
	public void add(Book book) {
		blist[index++] = book;
	}
	
	/** 2. 데이터 전체 검색 기능
	 * 기능 : Book 배열 전체를 검색 한다.
	 * 입력 : x
	 * 리턴 : 배열 전체
	 * */
	public Book[] search() {
		Book result[] = new Book[index];
		for(int i=0; i<index; i++) {
			result[i] = blist[i];
		}
		return result;
	}
	
	/** 3. Isbn 으로 정보를 검색하는 기능
	 * 기능 : Isbn으로 Book 정보를 검색한다.
	 * 입력 : String Isbn
	 * 리턴 : 찾으면 Book, 못찾으면 null
	 * */
	public Book searchIsbn(String isbn) {
		Book temp = null;
		for (Book book : blist) {
			if(book!=null && book.getIsbn().equals(isbn)) {
				temp = book;
			}
		}
		return temp;
	}
	
	/** 4. Title로 정보를 검색하는 기능 (파라메터로 주어진 제목을 포함하는 모든 정보)
	 * 기능 : Title로 Book 정보를 검색한다.
	 * 입력 : String Title
	 * 리턴 : 찾으면 Book, 못찾으면 null
	 * */
	public Book searchTitle(String title) {
		Book temp = null;
		for (Book book : blist) {
			if(book!=null && book.getTitle().equals(title)) {
				temp = book;
			}
		}
		return temp;
	}
	
	/** 5. Book만 검색하는 기능
	 * 기능 : blist에서 Book만 검색한다.
	 * 입력 : x
	 * 리턴 : Book만 담은 배열
	 * */
	public Book[] searchBooks() {
		Book temp[] = new Book[index];
		int tempIndex=0;
		for (int i = 0;  i < index; i++) {
			if(blist[i] instanceof Magazine) {
			}
			else {
				if(blist[i] != null) {
					temp[i] = blist[i];
					tempIndex++;
				}
			}
		}
		
		Book result[] = new Book[tempIndex];
		for (int i = 0;  i < tempIndex; i++) {
			result[i] = temp[i];
		}
		return result;
	}
	
	/** 6. Magazine만 검색하는 기능
	 * 기능 : blist에서 Magazine만 검색한다.
	 * 입력 : x
	 * 리턴 : Magazine만 담은 배열
	 * */
	public Book[] searchMagazines() {
		Book temp[] = new Book[index];
		int tempIndex=0;
		for (int i = 0;  i < index; i++) {
			if(blist[i] instanceof Magazine) {
				if(blist[i]!=null) {
					temp[tempIndex] = blist[i];
					tempIndex++;
				}
			}
		}
		
		Book result[] = new Book[tempIndex];
		for (int i = 0;  i < tempIndex; i++) {
			result[i] = temp[i];
		}
		return result;
	}
	
	/** 7. Magazine중 올해 잡지만 검색하는 기능
	 * 기능 : blist에서 Magazine중 올해 잡지만 검색한다.
	 * 입력 : 올해 년도
	 * 리턴 : Magazine만 담은 배열
	 * */
	public Magazine[] searchMagazines(int year) {
		Magazine temp[] = new Magazine[index];
		int tempIndex=0;
		for (int i = 0;  i < index; i++) {
			if(blist[i]!=null && blist[i] instanceof Magazine) {
				if(((Magazine)blist[i]).getYear() == year) {
					temp[tempIndex] = (Magazine)blist[i];
					tempIndex++;
				}
			}
		}
		
		Magazine result[] = new Magazine[tempIndex];
		for (int i = 0;  i < tempIndex; i++) {
			result[i] = temp[i];
		}
		return result;
	}
	
	/** 8. 출판사로 검색
	 * 기능 : blist에서 출판사로 검색해서 해당 출판사가 출판한 책들 다 리턴
	 * 입력 : 출판사
	 * 리턴 : Book 담은 배열
	 * */
	public Book[] searchPublisher(String publisher) {
		Book temp[] = new Book[index];
		int tempIndex=0;
		for (int i = 0;  i < index; i++) {
			if(blist[i].getPublisher().equals(publisher)) {
				temp[tempIndex] = blist[i];
				tempIndex++;
			}
		}
		
		Book result[] = new Book[tempIndex];
		for (int i = 0;  i < tempIndex; i++) {
			result[i] = temp[i];
		}
		return result;
	}
	
	/** 9. 가격으로 검색 기능 (파라메터로 주어진 가격보다 낮은 도서 정보 검색)
	 * 기능 : blist에서 입력된 가격보다 낮은 도서 정보 검색
	 * 입력 : 가격
	 * 리턴 : Book 담은 배열
	 * */
	public Book[] searchPrice(int price) {
		Book temp[] = new Book[index];
		int tempIndex=0;
		for (int i = 0;  i < index; i++) {
			if(blist[i]!= null && blist[i].getPrice()<price) {
				temp[tempIndex] = blist[i];
				tempIndex++;
			}
		}
		Book result[] = new Book[tempIndex];
		for (int i = 0;  i < tempIndex; i++) {
			result[i] = temp[i];
		}
		return result;
	}
	
	/** 10. 저장된 모든 도서의 금액 합계를 구하는 기능
	 * 기능 : blist에 저장된 모든 도서 금액 합계 구하기
	 * 입력 : x
	 * 리턴 : 금액 합계
	 * */
	public int totalPrice() {
		int total = 0;
		for (Book book : blist) {
			if(book != null)
				total += book.getPrice();
		}
		
		return total;
	}
	
	/** 11. 저장된 모든 도서의 금액 평균을 구하는 기능
	 * 기능 : blist에 저장된 모든 도서 금액 평균 구하기
	 * 입력 : x
	 * 리턴 : 금액 평균
	 * */
	public int averagePrice() {
		int total = totalPrice();
		return total/index;
	}
}
