package com.ssafy;

public class BookTest {
	public static void main(String[] args) {
		BookManager mgr = BookManager.getInstance();
		
		/** 1. 데이터 입력 */
		mgr.add(new Book("21424","Java Pro","김하나","Jean.kr",15000,"Java 기본 문법"));
		mgr.add(new Book("33455","JDBC Pro","김철수","Java.com",23000,""));
		mgr.add(new Book("55355","Servlet/JSP","박자바","Jean.kr",41000,"Mode12 기반"));
		mgr.add(new Book("35332","Android App","홍길동","Java.com",25000,"Lightweight"));
		mgr.add(new Book("35355","OOAP 분석,설계","소나무","Jean.kr",30000,""));
		mgr.add(new Magazine("35535","Java World","편집부","Java.com",7000,"",2013,2));
		mgr.add(new Magazine("33434","Mobile World","편집부","Java.com",8000,"",2013,8));
		mgr.add(new Magazine("75342","Next Web","편집부","Jean.kr",10000,"AJAX 소개",2012,2));
		mgr.add(new Magazine("76543","Architecture","편집부","Jean.kr",5000,"Java 시스템",2013,2));
		mgr.add(new Magazine("76534","Data Modeling","편집부","Jean.kr",14000,"",2012,13));
		
		/** 2. 데이터 전체 검색 기능 */
		System.out.println("--- 2. 데이터 전체 검색 기능 ---");
		for (Book book : mgr.search()) {
			System.out.println(book.toString());
		}
		
		/** 3. Isbn 으로 정보를 검색하는 기능 */
		System.out.println("\n--- 3. Isbn 으로 정보를 검색하는 기능 | Isbn = 35355 ---");
		System.out.println(mgr.searchIsbn("35355"));
		
		/** 4. Title로 정보를 검색하는 기능 (파라메터로 주어진 제목을 포함하는 모든 정보) */
		System.out.println("\n--- 4. Title로 정보를 검색하는 기능 | Title = Android App ---");
		System.out.println(mgr.searchTitle("Android App"));
		
		/** 5. Book만 검색하는 기능 */
		System.out.println("\n--- 5. Book만 검색하는 기능 ---");
		for (Book book : mgr.searchBooks()) {
			System.out.println(book.toString());
		}
		
		/** 6. Magazine만 검색하는 기능 */
		System.out.println("\n--- 6. Magazine만 검색하는 기능 ---");
		for (Book book : mgr.searchMagazines()) {
			if(book != null) System.out.println(book.toString());
		}
		
		/** 7. Magazine중 올해 잡지만 검색하는 기능 */
		System.out.println("\n--- 7. Magazine중 올해 잡지만 검색하는 기능 --- | year = 2012");
		for (Magazine magazine : mgr.searchMagazines(2012)) {
			System.out.println(magazine.toString());
		}
		
		/** 8. 출판사로 검색 */
		System.out.println("\n--- 8. 출판사로 검색 --- | publisher = Java.com");
		for (Book book : mgr.searchPublisher("Java.com")) {
			System.out.println(book.toString());
		}
		
		/** 9. 가격으로 검색 기능 */
		System.out.println("\n--- 9. 가격으로 검색 기능 --- | price = 15000 ");
		for (Book book : mgr.searchPrice(15000)) {
			System.out.println(book.toString());
		}
		
		/** 10. 저장된 모든 도서의 금액 합계를 구하는 기능 */
		System.out.println("\n--- 10. 저장된 모든 도서의 금액 합계를 구하는 기능 ");
		System.out.println("총 가격 : "+ mgr.totalPrice()+"원");
		
		/** 11. 저장된 모든 도서의 금액 평균을 구하는 기능 */
		System.out.println("\n--- 11. 저장된 모든 도서의 금액 평균을 구하는 기능 ");
		System.out.println("금액 평균 : "+ mgr.averagePrice()+"원");
	}
}
