package com.ssafy.weather;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class WeatherDAO {
private DocumentBuilder p;
	
	public WeatherDAO() {
		try {
			p=DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	
//	<hour>18</hour>    // 시간
//	<day>0</day>       // 날짜 : 0오늘, 1 내일, 2 모래
//	<temp>1.0</temp>   // 온도 : 1.0도
//	<tmx>-999.0</tmx>  // 최고 온도
//	<tmn>-999.0</tmn>  // 최저 온도
//	<sky>1</sky>
//	<pty>0</pty>
//	<wfKor>맑음</wfKor> // 날씨 : 한글
//	<wfEn>Clear</wfEn> // 날씨 : 영어
//	<pop>0</pop>       // 강수 확률
//	<r12>0.0</r12>
//	<s12>0.0</s12>
//	<ws>4.5</ws>            
//	<wd>7</wd>         // 풍속 : 7m/s 
//	<wdKor>북서</wdKor> // 풍향 : 한글
//	<wdEn>NW</wdEn>    // 풍향 : 영어 
//	<reh>30</reh>      // 습도 : 30%
//	<r06>0.0</r06>     // 강우량(최대) : 0mm
//	<s06>0.0</s06>     // 강설량(최대) : 0mm
	
	
	// 서버의 모든 Weather를 출력하세요
	public Collection<Weather> getWeathers() throws SAXException, IOException{
		Collection<Weather> list = new ArrayList<Weather>();
		Document d = p.parse("http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4113567000");
		
		// channel
		Element root = d.getDocumentElement();
		
		NodeList weathers = root.getElementsByTagName("data");
		for(int i=0; i<weathers.getLength(); i++) {
			Weather wd = new Weather();
			Node t = weathers.item(i);
			NodeList weather = t.getChildNodes();
			for (int j = 0; j < weather.getLength(); j++) {
				Node temp = weather.item(j);
				if(temp.getNodeType()==Node.ELEMENT_NODE) {
					switch(temp.getNodeName()) {
						case "hour":
							wd.setHour(temp.getFirstChild().getNodeValue());
							break;
						case "temp":
							wd.setTemperature(temp.getFirstChild().getNodeValue());
							break;
						case "wfKor":
							wd.setWeather(temp.getFirstChild().getNodeValue());
							break;
						case "reh":
							wd.setHumidity(temp.getFirstChild().getNodeValue());
							break;
					}
				}
			}
			list.add(wd);
		}
		return list;
	}
	
}
