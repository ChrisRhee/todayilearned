package com.ssafy.weather;

public class Weather {
	
	private String hour;
	private String temperature;
	private String weather;
	private String humidity;
	
	public String getHour() {
		return hour;
	}
	public void setHour(String hour) {
		this.hour = hour;
	}
	public String getTemperature() {
		return temperature;
	}
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	public String getWeather() {
		return weather;
	}
	public void setWeather(String weather) {
		this.weather = weather;
	}
	public String getHumidity() {
		return humidity;
	}
	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getHour());
		builder.append("시  ");
		builder.append(getTemperature());
		builder.append("℃  ");
		builder.append(getWeather());
		builder.append("  ");
		builder.append(getHumidity());
		builder.append("%  ");
		return builder.toString();
	}
	
	
	
}
