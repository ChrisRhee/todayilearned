package com.ssafy.weather;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class WeatherDAO {
	private List<Weather> list;
	
	private static WeatherDAO instance = null;
	private WeatherDAO() {
		list = new ArrayList<>();
		try {
			connectXML();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static WeatherDAO getInstance() {
		if(instance == null) {
			instance = new WeatherDAO();
		}
		return instance;
	}

	// 서버의 모든 Weather를 출력하세요
	public List<Weather> getWeathers(){
		
		return list;
	}
	
	public void connectXML() throws SAXException, IOException, ParserConfigurationException {
		DocumentBuilder p = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document d = p.parse("http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4113567000");
		
		// channel
		Element root = d.getDocumentElement();
		
		NodeList weathers = root.getElementsByTagName("data");
		for(int i=0; i<weathers.getLength(); i++) {
			Weather wd = new Weather();
			Node t = weathers.item(i);
			NodeList weather = t.getChildNodes();
			for (int j = 0; j < weather.getLength(); j++) {
				Node temp = weather.item(j);
				if(temp.getNodeType()==Node.ELEMENT_NODE) {
					switch(temp.getNodeName()) {
						case "hour":
							wd.setHour(temp.getFirstChild().getNodeValue());
							break;
						case "temp":
							wd.setTemperature(temp.getFirstChild().getNodeValue());
							break;
						case "wfKor":
							wd.setWeather(temp.getFirstChild().getNodeValue());
							break;
						case "reh":
							wd.setHumidity(temp.getFirstChild().getNodeValue());
							break;
					}
				}
			}
			list.add(wd);
		}
	}
}
