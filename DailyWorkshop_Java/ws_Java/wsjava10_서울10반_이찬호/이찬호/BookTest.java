package com.ssafy;

import java.io.IOException;
import java.util.ArrayList;

public class BookTest {
	public static void main(String[] args) throws ISBNNotFoundException, QuantityException {
		// ArrayList로 넣기
		//BookMgrImpl mgr = new BookMgrImpl(new ArrayList());
		// HashSet 에 넣기 - Default는 HashSet이다
		BookMgrImpl mgr = BookMgrImpl.getInstance();
		
		/** 1. 데이터 입력 */ 
		mgr.add(new Book("21424","Java Pro",15000,10));
		mgr.add(new Book("33455","JDBC Pro",23000,20));
		mgr.add(new Book("55355","Servlet/JSP",41000,30));
		mgr.add(new Book("35332","Android App",25000,40));
		mgr.add(new Book("35355","OOAP 분석,설계",30000,50));
		mgr.add(new Magazine("35535","Java World",7000,11,2));
		mgr.add(new Magazine("33434","Mobile World",8000,22,8));
		mgr.add(new Magazine("75342","Next Web",10000,12,2));
		mgr.add(new Magazine("76543","Architecture",5000,13,2));
		mgr.add(new Magazine("76534","Data Modeling",14000,12,13));
		mgr.add(new Book("12345","이찬호설계",12000,60));
		mgr.add(new Magazine("54321","이찬호월드",7000,12,12));
		
		/** 2. 데이터 전체 검색 기능 */
		System.out.println("--- 2. 데이터 전체 검색 기능 ---");
		for (Book book : mgr.search()) {
			System.out.println(book.toString());
		}
		
		/** 3. 판매 */
		System.out.println("\n--- 3. 판매 | 12345, 30개");
		try {
		mgr.sell("12345", 30);
		mgr.sell("12345", 30);
		mgr.sell("12345", 30);
		}catch(ISBNNotFoundException e) {
			e.printStackTrace();
		}catch(QuantityException e) {
			e.printStackTrace();
		}
		
		for (Book book : mgr.search()) {
			System.out.println(book.toString());
		}
		
		/** 4. 구매 */
		System.out.println("\n--- 4. 구매 | 12dsafsdf, 30개");
		try {	
		mgr.buy("12345", 30);
		mgr.buy("12dsafsdf", 30); 
		}
		catch(ISBNNotFoundException e) {
			e.printStackTrace();
		}

		for (Book book : mgr.search()) {
			System.out.println(book.toString());
		}
		
		/** 5. 저장된 모든 도서의 금액 합계를 구하는 기능 */
		System.out.println("\n--- 10. 저장된 모든 도서의 금액 합계를 구하는 기능 ");
		System.out.println("총 가격 : "+ mgr.getTotalAmount()+"원");
		
		/** 6. 파일에서 값 읽어오기 */
		try {
			mgr.Open();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/** 7. 파일에서 값 저장하기 */
		try {
			mgr.Close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
