package com.ssafy;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class BookMgrImpl implements IBookMgr{
	private Collection<Book> books;
	
	private static BookMgrImpl instance = null;
	
	// 생성자
	private BookMgrImpl() {
		this(new ArrayList());
	}
	
	public BookMgrImpl(Collection<Book> books) {
		this.books= books;
	}
	
	public static BookMgrImpl getInstance() {
		if(instance == null) {
			instance = new BookMgrImpl(new ArrayList());
		}
		return instance;
	}

	/** 1. 데이터 입력 기능 */
	@Override
	public boolean add(Book book) {
		return books.add(book);
	}

	/** 2. 데이터 전체 검색 기능  */
	@Override
	public Collection<Book> search() {
		Collection<Book> temp = new ArrayList();
		for (Book book : books) {
			temp.add(book);
		}
		return temp;
	}

	
	@Override
	public void sell(String isbn, int quantity) throws ISBNNotFoundException, QuantityException {
		boolean found = false;
		for (Book book : books) {
			if(book.getIsbn().equals(isbn)) {
				if(book.getQuantity() < quantity) throw new QuantityException(""+quantity);
				book.setQuantity(book.getQuantity()-quantity);
				found = true;
			}
		}
		if (found == false) throw new ISBNNotFoundException(isbn);
	}

	@Override
	public void buy(String isbn, int quantity) throws ISBNNotFoundException {
		boolean found = false;
		for (Book book : books) {
			if(book.getIsbn().equals(isbn)) {
				book.setQuantity(book.getQuantity()+quantity);
				found = true;
			}
		}
		if (found == false) throw new ISBNNotFoundException(isbn);
	}

	@Override
	public int getTotalAmount() {
		int total = 0;
		for (Book book : books) {
			total += book.getPrice()*book.getQuantity();
		}
		return total;
	}

	@Override
	public void Open() throws IOException, ClassNotFoundException  {
		ObjectInputStream ibj = new ObjectInputStream(new FileInputStream("book.dat"));
		Collection<Book> tmp = new ArrayList();
		while(ibj.available() >0) {
			Book temp = (Book)ibj.readObject();
			tmp.add(temp);
		}
		books = tmp;
	}

	@Override
	public void Close() throws Exception {
		ObjectOutputStream obj = new ObjectOutputStream(new FileOutputStream("book.dat"));
		obj.writeObject(books);
		obj.flush();
		obj.close();
	}

}
