package com.ssafy;

public class Magazine extends Book{
	private int month;
	
	
	public Magazine(String isbn, String title, int price, int quantity, int month) {
		super(isbn, title, price, quantity);
		setMonth(month);
	}
	
	
	public Magazine() {
		this("", "", 0, 0, 0);
	}


	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + month;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Magazine other = (Magazine) obj;
		if (month != other.month)
			return false;
		return true;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.toString());
		builder.append(" | ");
		builder.append(getMonth());
		builder.append("월 호");
		return builder.toString();
	}
	
}
