package com.ssafy.Magazine;

public class Magazine {
	private String isbn;
	private String title;
	private String author;
	private String publisher;
	private int year;
	private int month;
	private int price;
	private String desc;
	
	public Magazine(String isbn, String title,String author,String publisher,int year, int month, int price, String desc) {
		setIsbn(isbn);
		setTitle(title);
		setAuthor(author);
		setPublisher(publisher);
		setPrice(price);
		setDesc(desc);
		setYear(year);
		setMonth(month);
	}
	
	public Magazine(String isbn, String title,String author,String publisher,int year, int month, int price) {
		this(isbn, title, author, publisher, year, month, price, "");
	}
	
	/*
	public Magazine(String isbn, String title,String author,String publisher,int year, int month, int price) {
		this.isbn = isbn;
		this.title = title;
		this.author = author;
		this.publisher = publisher;
		this.year = year;
		this.month = month;
		this.price = price;
	}
	*/
	
	
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	
	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append(isbn);
		str.append(" | ");
		str.append(title);
		str.append(" | ");
		str.append(author);
		str.append(" | ");
		str.append(publisher);
		str.append(" | ");
		str.append(price);
		str.append(" | ");
		str.append(desc);
		str.append(" | ");
		str.append(year);
		str.append(".");
		str.append(month);
		return str.toString();
	}
}
