package com.ssafy.Manage;

public class Teacher {
	// member data
	private int studentId;
	private String sex;
	private String number;
	private int payward[] = new int[12];
		
	// constructor
	public Teacher(int studentId, String sex, String number, int payward[]) {
		this.studentId = studentId;
		this.sex = sex;
		setNumber(number);
		this.payward = payward;
	}
	
	// encapsulation
	public void setNumber(String number) {
		this.number = number;
	}

	public void setPayward(int month,int payward) {
		this.payward[month] = payward;
	}
	
	public int getTotal() {
		int total = 0;
		for(int i =0; i <payward.length ; i++) {
			total += payward[i];
		}
		return total;
	}
	
	// operators
	public String toString() {
		return "Id : "+ studentId+ ", sex : "+ sex+", number : "+number+", payward : "+ getTotal()+"만 원" ;
	}
}
