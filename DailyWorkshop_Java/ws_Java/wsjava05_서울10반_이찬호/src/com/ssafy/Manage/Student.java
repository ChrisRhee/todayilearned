package com.ssafy.Manage;

public class Student {
	// member data
	private int studentId;
	private String sex;
	private String number;
	private double score[] = new double[10];
	
	// 생성자는 구현부에서 다른생성자를 1줄 1번만 호출된다.
	// 그래서 생성자는 매개인자 많은것부터 구현해서 재사용한다.
	// constructor
	public Student() {}
	
	public Student(int studentId, String sex, String number) {
		this(studentId, sex, number, new double[10]);
//		this.studentId = studentId;
//		this.sex = sex;
//		this.number = number;
//		this.score = score;
	}
	
	public Student(int studentId, String sex, String number, double score[]) {
		this.studentId = studentId;
		this.sex = sex;
		setNumber(number);
		this.score = score;
	}
	
	// encapsulation
	public void setNumber(String number) {
		this.number = number;
	}

	public void setScore(int month, double score) {
		this.score[month-1] = score;
	}
	
	// operators
	public String toString() {
		StringBuffer str = new StringBuffer();
		// 이렇게 append로 하나씩 하나씩 append하는게 합리적이고 권장을 한다. 
		str.append("Id : "+ studentId+ ", sex : "+ sex+", number : "+number+", score : ");
		for(int i =0; i<this.score.length; i++) {
			if(score[i] <=0) continue;
			str.append("| "+(i+1)+ "학기: " + this.score[i]+" ");
		}
		
		String stuscore = "Id : "+ studentId+ ", sex : "+ sex+", number : "+number+", score : ";
		for(int i =0; i<this.score.length; i++) {
			if(score[i] <=0) continue;
			stuscore += "| "+(i+1)+ "학기: " + this.score[i]+" ";
		}
		
		return stuscore ;
	}
}
