package com.ssafy.Book;

public class Book {
	private String isbn;
	private String title;
	private String author;
	private String publisher;
	private int price;
	private String desc;
	
	
	/** Constructors */
	public Book(String isbn, String title,String author,String publisher,int price) {
		this.isbn = isbn;
		this.title = title;
		this.author = author;
		this.publisher = publisher;
		this.price = price;
		this.desc = "";
	}
	
	public Book(String isbn, String title,String author,String publisher,int price, String desc) {
		this.isbn = isbn;
		this.title = title;
		this.author = author;
		this.publisher = publisher;
		this.price = price;
		this.desc = desc;
	}
	
	/** Encapsulation */
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	/** Operator */
	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append(isbn);
		str.append(" | ");
		str.append(title);
		str.append(" | ");
		str.append(author);
		str.append(" | ");
		str.append(publisher);
		str.append(" | ");
		str.append(price);
		str.append(" | ");
		str.append(desc);
		return str.toString();
	}
}
