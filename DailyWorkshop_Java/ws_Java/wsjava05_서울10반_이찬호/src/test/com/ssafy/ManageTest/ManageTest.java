package test.com.ssafy.ManageTest;

import com.ssafy.Manage.Student;
import com.ssafy.Manage.Teacher;

public class ManageTest {
	public static void main(String[] args) {
		double s1score[] = new double[10];
		Student s1 = new Student(2011104038,"남자","010-0000-0000",s1score);
		// 스코어, 번호 수정
		s1.setScore(1, 1.2);
		s1.setScore(2, 3.2);
		s1.setScore(3, 4.2);
		s1.setNumber("010-1234-5678");
		
		//스코어 추가
		double s2score[] = new double[10];
		Student s2 = new Student(2013112158,"여자","010-6672-9145",s2score);
		s2.setScore(1, 4.3);
		
		double s3score[] = new double[] {1.4,3.4,2.2,3.5,2.2,0.4,3.0,4.3,4.0,4.1};
		Student s3 = new Student(2015164094,"남자","010-1542-3548",s3score);
		
		//출력
		System.out.println("--------------------- Student ---------------------");
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
		System.out.println();
		
		int t1pay[] = new int[12];
		Teacher t1 = new Teacher(2203, "남자", "010-1234-5678", t1pay);
		// t1 급여 추가
		t1.setPayward(1, 10);
		t1.setPayward(2, 30);
		
		int t2pay[] = new int[] {10,10,12,34,50,10,34,49,90};
		Teacher t2 = new Teacher(1203, "여자", "010-1234-5678", t2pay);
		// t2 번호 수정
		t2.setNumber("019-4050-7242");
		
		int t3pay[] = new int[] {5,5,5,5,5,5,5,5,5,5,5};
		Teacher t3 = new Teacher(4203, "남자", "010-1234-5678", t3pay);
		
		System.out.println("--------------------- Teacher ---------------------");
		System.out.println(t1);
		System.out.println(t2);
		System.out.println(t3);
		
	}
}
