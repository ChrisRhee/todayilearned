package test.com.ssafy.BookTest;

import com.ssafy.Book.*;
import com.ssafy.Magazine.Magazine;

public class BookTest {
	public static void main(String[] args) {
		
		Book b1 = new Book("21424","Java Pro","김하나","Jean.kr",15000,"Java 기본 문법");
		Book b2 = new Book("33455","JDBC Pro ","김철수","Jean.kr",23000);
		Book b3 = new Book("55355","Servlet/JSP","박자바","Jean.kr",41000,"Mode12 기반");
		Book b4 = new Book("35332","Android App","홍길동","Jean.kr",25000,"Lightweight Framework");
		Book b5 = new Book("35355","OOAD 분석,설계   ","소나무","Jean.kr",30000);
		Book books[] = new Book[] {b1,b2,b3,b4,b5};
		
		Magazine m1 = new Magazine("35535","Java World","편집부","Jean.kr",2013,2,7000);
		Magazine m2 = new Magazine("33434","Mobile World","편집부","Jean.kr",2013,8,8000);
		Magazine m3 = new Magazine("75342","Next Web","편집부","Jean.kr",2012,10,10000,"AJAX ?���?");
		Magazine m4 = new Magazine("76543","Architecture","편집부","Jean.kr",2010,3,5000,"java ?��?��?��");
		Magazine m5 = new Magazine("76534","Data Modeling","편집부","Jean.kr",2012,12,14000);
		Magazine magazines[] = new Magazine[] {m1,m2,m3,m4,m5};
		
		System.out.println("*********************** 도서 목록 **************************");
		for(int i=0; i<books.length;i++) {
			System.out.println(books[i]);
		}
		
		System.out.println();
		
		System.out.println("*********************** 잡지 목록 **************************");
		for(int i=0; i<magazines.length;i++) {
			System.out.println(magazines[i]);
		}
	}
}

