package com.ssafy.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ssafy.vo.Food;

public class FoodDAO {
	
	private List<Food> list;
	
	private static FoodDAO instance = null;
	private FoodDAO() {
		list = new ArrayList<>();
		foodLoad();
	}
	public static FoodDAO getInstance() {
		if(instance == null) {
			instance = new FoodDAO();
		}
		return instance;
	}
	
	// 정보들 list에 저장
	private void foodLoad() {
		try {
			getNutritionInfo();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		
		try {
			getFoodInfo();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	// 서버의 모든 Food를 출력하세요
	public List<Food> getFoods(){
		return list;
	}
	
	// 이름으로 정렬
	public List<Food> SortByName() {
		List<Food> tmp = list;
		if(list.size() ==0) {
			return null;
		}
		Collections.sort(tmp);
		return tmp;
	}
	
	// 제조사로 정렬
	public List<Food> SortByMaker() {
		List<Food> tmp = list;
		if(list.size() ==0) {
			return null;
		}
		Collections.sort(tmp, new MakerComparator());
		return tmp;
	}
	
	// 원재료로 정렬
	public List<Food> SortByMaterial() {
		List<Food> tmp = list;
		if(list.size() ==0) {
			return null;
		}
		Collections.sort(tmp, new MaterialComparator());
		return tmp;
	}
	
	// 이름검색
	public Food searchName(String name) {
		for (Food f : getFoods()) {
			if(f.getName().equals(name))
				return f;
		}
		return null;
	}
	
	// 제조사 검색
	public List<Food> searchMaker(String maker){
		List<Food> list=new ArrayList<Food>();
		for (Food f : getFoods()) {
			if(f.getMaker().equals(maker))
				list.add(f);
		}
		return list;
	}
	
	public List<Food> searchMaterial(String material){
		List<Food> list=new ArrayList<Food>();
		for (Food f : getFoods()) {
			if(f.getMaterial().contains(material))
				list.add(f);
		}
		return list;
	}
	
	public void getNutritionInfo() throws SAXException, IOException, ParserConfigurationException {
		DocumentBuilder p = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document d = p.parse("res/FoodNutritionInfo.xml");
		
		// response
		Element root = d.getDocumentElement();
		NodeList foods = root.getElementsByTagName("item");
		for(int i=0; i<foods.getLength(); i++) {
			Food f = new Food();
			Node t = foods.item(i);
			
			NodeList food = t.getChildNodes();
			for (int j = 0; j < food.getLength(); j++) {
				Node temp = food.item(j);
				if(temp.getNodeType()==Node.ELEMENT_NODE) {
					switch(temp.getNodeName()) {
						case "SERVING_WT":
							f.setSupportpereat(temp.getFirstChild().getNodeValue());
							break;
						case "NUTR_CONT1":
							f.setCalory(temp.getFirstChild().getNodeValue());
							break;
						case "NUTR_CONT2":
							f.setCarbo(temp.getFirstChild().getNodeValue());
							break;
						case "NUTR_CONT3":
							f.setProtein(temp.getFirstChild().getNodeValue());
							break;
						case "NUTR_CONT4":
							f.setFat(temp.getFirstChild().getNodeValue());
							break;
						case "NUTR_CONT5":
							f.setSugar(temp.getFirstChild().getNodeValue());
							break;
						case "NUTR_CONT6":
							f.setNatrium(temp.getFirstChild().getNodeValue());
							break;
						case "NUTR_CONT7":
							f.setChole(temp.getFirstChild().getNodeValue());
							break;
						case "NUTR_CONT8":
							f.setFattyacid(temp.getFirstChild().getNodeValue());
							break;
						case "NUTR_CONT9":
							f.setTransfat(temp.getFirstChild().getNodeValue());
							break;
					}
				}
			}
			list.add(f);
		}
	}
	
	
	public void getFoodInfo() throws SAXException, IOException, ParserConfigurationException {
		DocumentBuilder p = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document d = p.parse("res/foodInfo.xml");
		
		// response
		Element root = d.getDocumentElement();
		NodeList foodlist = root.getElementsByTagName("food");
		for(int i=0; i<foodlist.getLength(); i++) {
			Food f = list.get(i);
			Node t = foodlist.item(i);
			
			NodeList food = t.getChildNodes();
			for (int j = 0; j < food.getLength(); j++) {
				Node temp = food.item(j);
				if(temp.getNodeType()==Node.ELEMENT_NODE) {
					switch(temp.getNodeName()) {
					case "code":
						f.setCode(Integer.parseInt(temp.getFirstChild().getNodeValue()));
						break;
					case "name":
						f.setName(temp.getFirstChild().getNodeValue());
						break;
					case "maker":
						f.setMaker(temp.getFirstChild().getNodeValue());
						break;
					case "material":
						f.setMaterial(temp.getFirstChild().getNodeValue());
						break;
					case "image":
						f.setImg(temp.getFirstChild().getNodeValue());
						break;
					}
				}
			}
			list.set(i,f);
		}
	}
}
