package com.ssafy.dao;

import java.util.Comparator;

import com.ssafy.vo.Food;

public class MaterialComparator implements Comparator<Food> {

	@Override
	public int compare(Food o1, Food o2) {
		return o1.getMaterial().compareTo(o2.getMaterial());
	}

}
