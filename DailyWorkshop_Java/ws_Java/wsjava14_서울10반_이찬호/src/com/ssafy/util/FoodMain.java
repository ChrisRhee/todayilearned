package com.ssafy.util;

import java.io.IOException;

import org.xml.sax.SAXException;

import com.ssafy.dao.FoodDAO;
import com.ssafy.vo.Food;

public class FoodMain {
	public static void main(String[] args) {
		FoodDAO dao = FoodDAO.getInstance();
		
		Object[] s = dao.getFoods().toArray();
		for (Object o : s) {
			System.out.println(o);
		}
		System.out.println();
		
		//검색
		System.out.println("serach by name");
		System.out.println(dao.searchName("메로나"));
		
		System.out.println("\nserach by Maker");
		for (Food food : dao.searchMaker("빙그레")) {
			System.out.println(food);
		}
		
		System.out.println("\nserach by Material");
		for (Food food : dao.searchMaterial("정제수")) {
			System.out.println(food);
		}

		System.out.println("Program End");
	}
}
