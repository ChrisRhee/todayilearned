package com.ssafy.vo;

/** 식품 정보 */
public class Food implements Comparable<Food>{
	/**식품을 구별하는 코드 */
	protected int code;
	/**식품 이름*/
	protected String name;
	
	/**일회 제공 량*/
	protected String supportpereat;
	/**일회 제공되는 칼로리*/
	protected String calory;
	/**일회 제공되는 탄수화물*/
	protected String carbo;
	/**일회 제공되는 단백질*/
	protected String protein;
	/**일회 제공되는 지방*/
	protected String fat;
	/**일회 제공되는 당류*/
	protected String sugar;
	/**일회 제공되는 나트륨*/
	protected String natrium;
	/**일회 제공되는 콜레스테롤*/
	protected String chole;
	/**일회 제공되는 포화지방산*/
	protected String fattyacid;
	/**일회 제공되는 트렌스지방*/
	protected String transfat;
	
	/**제조사*/
	protected String maker;
	/**원료*/
	protected String material;
	/**이미지 경로*/
	protected String img;
	protected String allergy;
	public Food() {
	}
	public Food(int code) {
		super();
		this.code = code;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSupportpereat() {
		return supportpereat;
	}
	public void setSupportpereat(String supportpereat) {
		this.supportpereat = supportpereat;
	}
	public String getCalory() {
		return calory;
	}
	public void setCalory(String calory) {
		this.calory = calory;
	}
	public String getCarbo() {
		return carbo;
	}
	public void setCarbo(String carbo) {
		this.carbo = carbo;
	}
	public String getProtein() {
		return protein;
	}
	public void setProtein(String protein) {
		this.protein = protein;
	}
	public String getFat() {
		return fat;
	}
	public void setFat(String fat) {
		this.fat = fat;
	}
	public String getSugar() {
		return sugar;
	}
	public void setSugar(String sugar) {
		this.sugar = sugar;
	}
	public String getNatrium() {
		return natrium;
	}
	public void setNatrium(String natrium) {
		this.natrium = natrium;
	}
	public String getChole() {
		return chole;
	}
	public void setChole(String chole) {
		this.chole = chole;
	}
	public String getFattyacid() {
		return fattyacid;
	}
	public void setFattyacid(String fattyacid) {
		this.fattyacid = fattyacid;
	}
	public String getTransfat() {
		return transfat;
	}
	public void setTransfat(String transfat) {
		this.transfat = transfat;
	}
	public String getMaker() {
		return maker;
	}
	public void setMaker(String maker) {
		this.maker = maker;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	
	public String getAllergy() {
		return allergy;
	}
	public void setAllergy(String allergy) {
		this.allergy = allergy;
	}
	@Override
	public String toString() {
		return "Food [code=" + code + ", name=" + name + ", supportpereat=" + supportpereat + ", calory=" + calory
				+ ", carbo=" + carbo + ", protein=" + protein + ", fat=" + fat + ", sugar=" + sugar + ", natrium="
				+ natrium + ", chole=" + chole + ", fattyacid=" + fattyacid + ", transfat=" + transfat + ", maker="
				+ maker + ", material=" + material + ", img=" + img + "]";
	}
	
	@Override
	public int compareTo(Food o) {
		return getName().compareTo(o.getName());
	}
	
}
