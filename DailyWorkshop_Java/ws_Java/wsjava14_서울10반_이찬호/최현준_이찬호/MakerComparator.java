package com.ssafy.dao;

import java.util.Comparator;

import com.ssafy.vo.Food;

public class MakerComparator implements Comparator<Food> {

	public int compare(Food o1, Food o2) {
		return o1.getMaker().compareTo(o2.getMaker());
	}

}
