package com.ssafy.movie;

public class Movie {
	private String title;
	private String director;
	private int grade;
	private String genre;
	private String summary;
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public Movie(String title, String director, int grade, String genre, String summary) {
		setTitle(title);
		setDirector(director);
		setGrade(grade);
		setGenre(genre);
		setSummary(summary);
	}
	
	public Movie(String title, String director, int grade, String genre) {
		this(title, director, grade, genre, "");
	}
	
	public Movie() {
		this("", "", 0, "", "");
	}
	
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("title: ")
		.append(getTitle())
		.append(", director: ")
		.append(getDirector())
		.append(", grade: ")
		.append(getGrade())
		.append(", genre: ")
		.append(getGenre())
		.append(", summary: ")
		.append(getSummary());
		
        return str.toString();
	}
	
	
	
}
