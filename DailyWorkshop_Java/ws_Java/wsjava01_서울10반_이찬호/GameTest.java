package com.ssafy.java;

import java.util.Scanner;

public class GameTest {
	public static void main(String[] args) {
		System.out.println(">>가위바위보 게임을 시작합니다. 아래 보기 중 하나를 고르세요.");
		System.out.println("1. 5판 3승");
		System.out.println("2. 3판 2승");
		System.out.println("3. 1판 1승");
		Scanner game_num = new Scanner(System.in);
		System.out.print("번호를 입력하세요. ");
		int game = game_num.nextInt();
		int win = 0;
		int comwin = 0;
		
		if(game == 1) {
			win = 3;
			comwin = 3;
			while(win > 0 && comwin > 0){
				int com = (int)(Math.random()*3)+1;
				Scanner sc = new Scanner(System.in);
				System.out.print("가위(1),바위(2),보(3)? ");
				int gamer = sc.nextInt();
				
				if((com==1 && gamer==3)||(com==2 && gamer==1)||(com==3 && gamer==2)) {
					System.out.println("졌습니다!!!");
					comwin --;
				}
				else if(com == gamer) {
					System.out.println("비겼습니다!!!");
				}
				else {
					System.out.println("이겼습니다!!!");
					win--;
				}
			}
		}
		else if (game == 2) {
			win = 2;
			comwin = 2;
			while(win > 0 && comwin > 0){
				int com = (int)(Math.random()*3)+1;
				Scanner sc = new Scanner(System.in);
				System.out.print("가위(1),바위(2),보(3)? ");
				int gamer = sc.nextInt();
				
				//System.out.println("컴퓨터="+com);
				//System.out.println("게이머="+gamer);
				if((com==1 && gamer==3)||(com==2 && gamer==1)||(com==3 && gamer==2)) {
					System.out.println("졌습니다!!!");
					comwin --;
				}
				else if(com == gamer) {
					System.out.println("비겼습니다!!!");
				}
				else {
					System.out.println("이겼습니다!!!");
					win--;
				}
			}
			
		}
		else if (game == 3) {
			win = 1;
			comwin = 1;
			while(win > 0 && comwin > 0){
				int com = (int)(Math.random()*3)+1;
				Scanner sc = new Scanner(System.in);
				System.out.print("가위(1),바위(2),보(3)? ");
				int gamer = sc.nextInt();
				
//				System.out.println("컴퓨터="+com);
//				System.out.println("게이머="+gamer);
				if((com==1 && gamer==3)||(com==2 && gamer==1)||(com==3 && gamer==2)) {
					System.out.println("졌습니다!!!");
					comwin --;
				}
				else if(com == gamer) {
					System.out.println("비겼습니다!!!");
				}
				else {
					System.out.println("이겼습니다!!!");
					win--;
				}
				sc.close();
			}
		}
		
		if(comwin == 0) {
			System.out.println("### 컴퓨터 승!!!");
		}
		if(win == 0) {
			System.out.println("### 사용자 승!!!");
		}
		// 1 = 가위, 2 = 주먹, 3 = 보
		game_num.close();
	}
}
