package com.ssafy.java;

public class DigitTest1 {
	public static void main(String[] args) {
		int n = 1;
		for (int i = 1; i <= 5; i++) {
			//공백 찍고
			for (int j = 1; j < i; j++) {
				System.out.print("   ");
			}
			//숫자를 찍는다
			for (int j = i; j <= 5; j++) {
				if(n < 10)
					System.out.print(n++ +"  ");
				else
					System.out.print(n++ +" ");
			}
			System.out.println();
		}
	}
}
