package com.ssafy;

import java.util.Collection;

public interface IBookMgr {
	/** 1. 도서 최초 입력 기능*/
	boolean add(Book book);
	
	/** 2. 전체 도서 정보 출력 기능  */
	Collection<Book> search();
	
	/** 3. 도서 판매되어 재고 수량을 빼는 기능  
	 * @throws ISBNNotFoundException 
	 * @throws QuantityException */
	void sell(String isbn, int quantity) throws ISBNNotFoundException, QuantityException;
	
	/** 4. 도서 구매되어 재고 수량을 더하는 기능  
	 * @throws ISBNNotFoundException */
	void buy(String isbn, int quantity) throws ISBNNotFoundException;
	
	/** 5. 재고 도서들의 수량*금액을 구해 총 재고 금액을 구하여 리턴 */
	int getTotalAmount();
	
	// 클래스 생성시 호출, 파일("book.dat)"에 저장된 파일을 읽어서 ArrayList에 저장
	void Open() throws Exception;
	
	// 프로그램 종료시 호출, ArrayList에 있는 도서 정보를 파일로 저장.
	void Close() throws Exception;
	
	void send();
}
