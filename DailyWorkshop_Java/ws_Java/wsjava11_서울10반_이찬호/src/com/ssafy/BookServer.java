package com.ssafy;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;

public class BookServer {
	public BookServer() {
		//1서버는 대기해야 한다.
		try {
			while(true) {
				ServerSocket server=new ServerSocket(8888);
				System.out.println("1서버 준비중");
				// 서버로 들어오는거 accept해줌
				Socket socket=server.accept(); //3
				// 그 소캣이 어느 주소에서 왔는지, 접속확인 ㅇㅋ 출력해줌
				System.out.println(socket.getInetAddress().toString()+" 접속 확인 ㅇㅋ");
				ObjectInputStream ois=new ObjectInputStream(socket.getInputStream());
				System.out.println("debug" + ois.available());
				while(ois.available() > 0) {
					ois=new ObjectInputStream(socket.getInputStream());
					System.out.println((Book)ois.readObject());
				}
					
				//server-->client 환영 메세지 
				ObjectOutputStream oos=new ObjectOutputStream(socket.getOutputStream());
				oos.writeObject("저희 서버에 어서오세요!!!!");
				oos.flush();
				oos.close();//
				
				
				//while(ois.available() >0) {
				//	System.out.println((Book)ois.readObject());
				//}
			}//while
		} catch (IOException e) {			
			e.printStackTrace();
		}
		catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		new BookServer();
	}
}
