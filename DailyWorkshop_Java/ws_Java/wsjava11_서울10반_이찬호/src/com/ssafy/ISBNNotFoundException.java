package com.ssafy;

public class ISBNNotFoundException extends Exception {
	public ISBNNotFoundException(String message) {
		super(message);
	}
}
