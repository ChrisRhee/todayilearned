package com.ssafy;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class BookMgrImpl implements IBookMgr{
	private Collection<Book> books;
	
	private static BookMgrImpl instance = null;
	
	// 생성자
	private BookMgrImpl() {
		this(new ArrayList());
	}
	
	public BookMgrImpl(Collection<Book> books) {
		this.books= books;
	}
	
	public static BookMgrImpl getInstance() {
		if(instance == null) {
			instance = new BookMgrImpl(new ArrayList());
		}
		return instance;
	}

	/** 1. 데이터 입력 기능 */
	@Override
	public boolean add(Book book) {
		return books.add(book);
	}

	/** 2. 데이터 전체 검색 기능  */
	@Override
	public Collection<Book> search() {
		Collection<Book> temp = new ArrayList();
		for (Book book : books) {
			temp.add(book);
		}
		return temp;
	}

	
	@Override
	public void sell(String isbn, int quantity) throws ISBNNotFoundException, QuantityException {
		boolean found = false;
		for (Book book : books) {
			if(book.getIsbn().equals(isbn)) {
				if(book.getQuantity() < quantity) throw new QuantityException(isbn+" "+quantity);
				book.setQuantity(book.getQuantity()-quantity);
				found = true;
			}
		}
		if (found == false) throw new ISBNNotFoundException(isbn);
	}

	@Override
	public void buy(String isbn, int quantity) throws ISBNNotFoundException {
		boolean found = false;
		for (Book book : books) {
			if(book.getIsbn().equals(isbn)) {
				book.setQuantity(book.getQuantity()+quantity);
				found = true;
			}
		}
		if (found == false) throw new ISBNNotFoundException(isbn);
	}

	@Override
	public int getTotalAmount() {
		int total = 0;
		for (Book book : books) {
			total += book.getPrice()*book.getQuantity();
		}
		return total;
	}

	@Override
	public void Open() throws Exception {
		ObjectInputStream ibj = new ObjectInputStream(new FileInputStream("book.dat"));
		Collection<Book> tmp = new ArrayList();
		while(ibj.available() >0) {
			Book temp = (Book)ibj.readObject();
			tmp.add(temp);
		}
		books = tmp;
	}

	@Override
	public void Close() throws Exception {
		ObjectOutputStream obj = new ObjectOutputStream(new FileOutputStream("book.dat"));
		obj.writeObject(books);
		obj.flush();
		obj.close();
	}
	
	@Override
	// 너가 클라이언트야.
	// 서버로 도서 정보 전송!!!
	public void send() {
		// ----------- 서버로 내보낼 것 --------------
		// book.dat에서 읽어와서 서버로 내보냄.
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(new FileOutputStream("book.dat"));
			oos.writeObject(books);
			oos.flush();
			oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// BookCliend 
	class BookClient extends Thread{
		public BookClient() {
			try {
				Socket socket = new Socket("127.0.0.1", 8888);
				System.out.println("server ok..");
				ObjectInputStream ois=new ObjectInputStream(socket.getInputStream());
				// 서버에 오신것을 환영합니다. 이런거 받을거임.
				System.out.println(ois.readObject());
				
				new Thread() {
					//시작
					public void run() {
						// 서버로 북 정보 바바이 내보내자
						send();
					};
				}.start();
				
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
