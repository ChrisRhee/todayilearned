package com.ssafy;

public class MovieTest {
	public static void main(String[] args) {
		MovieMgr mm = MovieMgr.getInstance();
		
		
		mm.add(new Movie("호빗","이찬호",15,"액션","존잼꿀잼"));
		mm.add(new Movie("해리포터","해리",12,"SF판타지","핵잼허니잼"));
		mm.add(new Movie("어벤져스","이찬호",12,"SF판타지","와 진짜 역대급"));
		mm.add(new Movie("아이언맨","이찬호",7,"액션","아이언맨죠습니다"));
		mm.add(new Movie("토르","이찬호",7,"로맨스코미디","토르도짱"));
		mm.add(new Movie("캡틴아메리카","이찬호",19,"SF판타지","캡틴짱"));
		
		mm.commandLine();
		// 2. 영화정보 전체검색
		System.out.println("\n-영화 전체 조회-----");
		Movie ma[] = mm.search();
		for(Movie a: ma) System.out.println(a);
		
		// 3. 영화명 검색
		System.out.println("\n-영화 이름으로 검색-----");
		Movie serchedM = mm.search("호빗");
		System.out.println(serchedM);
		
		// 4.감독 이름 검색
		System.out.println("\n-감독 이름으로 검색-----");
		ma = mm.searchDirector("이찬호");
		for(Movie a: ma) System.out.println(a);
		
		// 5.장르별 검색
		System.out.println("\n-장르 이름으로 검색-----");
		ma = mm.searchGenre("SF판타지");
		for(Movie a: ma) System.out.println(a);
		
		// 6.영화 정보 삭제
		System.out.println("\n-영화 정보 삭제-----");
		mm.delete("해리포터");
		ma = mm.search();
		for(Movie a: ma) System.out.println(a);
		
	}
}
