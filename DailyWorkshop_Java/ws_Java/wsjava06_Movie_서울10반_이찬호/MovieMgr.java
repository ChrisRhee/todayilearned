package com.ssafy;

import java.util.Arrays;
import java.util.Scanner;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class MovieMgr {
	private Movie movies[] = new Movie[100];
	private int index;
	
	private static MovieMgr instance = new MovieMgr();
	
	private MovieMgr() {}
	
	public static MovieMgr getInstance() {
		return instance;
	}


	public void clearScreen() {  
	    System.out.println("\n");
	}  
	
	
	public void add(Movie m) {
		movies[index++] = m;
	}
	
	public Movie[] search() {
		Movie[] temp = new Movie[index];
    	for(int i=0; i<index; i++) {
    		temp[i] = movies[i];
    	}
        return temp;
	}
	
	public Movie search(String title) {
    	for(int i=0; i<index; i++) {
    		Movie temp; 
    		if(movies[i].getTitle().equals(title)) {
    			temp = movies[i];
    			return temp;
    		}
    	}
        return null;
	}
	
	public Movie[] searchDirector(String name) {
		Movie[] temp = new Movie[index];
		int tempIndex =0;
    	for(int i=0; i<index; i++) {
    		if(movies[i].getDirector().equals(name)) {
    			temp[tempIndex] = movies[i];
    			tempIndex++;
    		}
    	}
    	Movie[] result = new Movie[tempIndex];
    	for(int i=0; i<tempIndex; i++) {
    		result[i] = temp[i];
    	}
    	return result;
	}
	
	public Movie[] searchGenre(String genre) {
		Movie[] temp = new Movie[index];
		int tempIndex =0;
    	for(int i=0; i<index; i++) {
    		if(movies[i].getGenre().equals(genre)) {
    			temp[tempIndex] = movies[i];
    			tempIndex++;
    		}
    	}
    	Movie[] result = new Movie[tempIndex];
    	for(int i=0; i<tempIndex; i++) {
    		result[i] = temp[i];
    	}
    	return result;
	}
	
	public void delete(String title) {
		for(int i=0; i<index; i++) {
    		if(movies[i].getTitle().equals(title)) {
    			for(int j=i; j<index-1; j++) {
    				movies[j] = movies[j+1];
        			movies[index] =null;
    			}
    			index--;
    		}
    	}
	}
	
	public int getSize() {
		return index+1;
	}
	
	public void commandLine() {
		Scanner sc = new Scanner(System.in);
		int num = 7;
		while(num != 0) {
			System.out.println("<<< 영화 관리 프로그램 >>>");
			System.out.println("1. 영화 정보 입력");
			System.out.println("2. 영화 정보 전체 검색");
			System.out.println("3. 영화 이름 검색");
			System.out.println("4. 감독 이름 검색");
			System.out.println("5. 영화 장르별 검색");
			System.out.println("6. 영화 정보 삭제");
			System.out.println("0. 종료");
			System.out.println("원하는 번호를 선택하세요. : ");
			num = sc.nextInt();
			
			switch(num) {
			case 1:
				Movie m = new Movie();
				System.out.println("영화정보를 입력해주세요");
				System.out.print("영화제목 : ");
				m.setTitle(sc.next());
				
				System.out.print("감독이름 : ");
				m.setDirector(sc.next());
				
				System.out.print("영화등급 : ");
				m.setGrade(sc.nextInt());
				
				System.out.print("영화장르 : ");
				m.setGenre(sc.next());
				
				System.out.print("영화내용 : ");
				m.setSummary(sc.next());
				add(m);
				clearScreen();
				
				break;
			// 영화 정보 전체 검색
			case 2:
				for(int i =0; i<index; i++) {
					System.out.println(movies[i]);
				}
				clearScreen();
				break;
			
			// 영화이름으로 검색
			case 3:
				System.out.print("영화 이름 : ");
				String mName = sc.next();
				System.out.println(search(mName));
				clearScreen();
				break;
			
			// 감독이름으로 검색
			case 4:
				System.out.print("감독 이름 : ");
				String deName = sc.next();
				for(int i =0; i<index; i++) {
					if(movies[i].getDirector().equals(deName)){
						System.out.println(movies[i]);
					}
				}
				clearScreen();
				break;
				
			// 장르로 검색
			case 5:
				System.out.print("장르 : ");
				String geName = sc.next();
				for(int i =0; i<index; i++) {
					if(movies[i].getGenre().equals(geName)){
						System.out.println(movies[i]);
					}
				}
				clearScreen();
				break;
			// 영화 삭제
			case 6:
				System.out.print("삭제할 영화 제목 : ");
				mName = sc.next();
				for(int i =0; i<index; i++) {
					if(movies[i].getTitle().equals(mName)){
						delete(mName);
					}
				}
				System.out.println("영화 "+mName+"이(가) 삭제되었습니다.");
				clearScreen();
				break;
			// 종료
			case 0:
				break;
				
			default:
				System.out.println("잘못 입력했습니다");
				break;
			}
		}
	}
}
