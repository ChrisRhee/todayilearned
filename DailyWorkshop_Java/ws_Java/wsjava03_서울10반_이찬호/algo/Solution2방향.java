package com.ssafy.algo;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution2방향 {
	static int TestCase, N;
	//                        상         우         하           좌
	//static int dir[][] = {{-1,0},{0,1},{1,0},{0,-1}};
	//                      우상        우하        좌하       좌상	
	//static int dir[][] = {{-1,-1},{-1,1},{1,1},{1,-1}};
	static int dir[][] = {{-1,0},{0,1},{1,0},{0,-1},{-1,-1},{-1,1},{1,1},{1,-1}};
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		System.setIn(new FileInputStream("input2방향.txt"));
		Scanner sc = new Scanner(System.in);
		TestCase=sc.nextInt();
		
		for(int test_case = 1; test_case <=TestCase; test_case++) {
			// 작성하세요...
			System.out.println("#"+test_case);
			N=sc.nextInt();
			int ia[][]=new int[N][N];
			int i =sc.nextInt();
			int j =sc.nextInt();
			
			ia[i][j] =1;
			for(int d = 0; d<dir.length; d++) {
				int ii=i+dir[d][0];
				int jj=j+dir[d][1];
				if((0<=ii&&ii<N) && ( 0<=jj && jj<N ) )
					ia[ii][jj] =1;
			}
			
			for(i = 0; i < N; i++){
				for(j = 0; j< N; j++) {
					System.out.print(ia[i][j]);
				}
				System.out.println();
			}
			
			System.out.println();
			
		}
		sc.close();
	}
}
