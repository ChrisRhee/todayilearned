package com.ssafy.algo;

import java.util.Scanner;

public class DigitTest2 {
	public static void main(String[] args) {
		int num = 100;
		Scanner sc = new Scanner(System.in);
		int second_num = sc.nextInt();
		int temp = 0;
		
		// 1,2번째 항까지 출력
		System.out.print(num+ " " + second_num + " ");
		// 3번째 항 구하기
		while (temp >= 0) {
			temp = num - second_num;
			System.out.print(temp+" ");
			num = second_num;
			second_num = temp;
		}
		sc.close();
	}
}
