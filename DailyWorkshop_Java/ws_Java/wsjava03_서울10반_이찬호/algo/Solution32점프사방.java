package com.ssafy.algo;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution32점프사방 {
	static int Answer;
	static int Y, X, N, P;
	//                            우        하           좌           상  
	static int dir[][] = {{0,0},{0,1},{1,0},{0,-1},{-1,0}};

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("Solution32점프사방.txt"));
		Scanner sc = new Scanner(System.in);

		int T = sc.nextInt(); // Test case

		// fill up the data
		for (int test_case = 1; test_case <= T; test_case++) {

			Y = sc.nextInt(); // position row
			X = sc.nextInt(); // position col
			N = sc.nextInt(); // number of player

			int[][] room = new int[Y + 1][X + 1];// because start from 1;
			// fill up table
			for (int i = 1; i <= Y; i++) {
				for (int j = 1; j <= X; j++) {
					room[i][j] = sc.nextInt();
				}
			}

			int[][] player = new int[N][3];
			for (int i = 0; i < N; i++) {
				player[i][0] = sc.nextInt(); // coord Y
				player[i][1] = sc.nextInt(); // coord X
				player[i][2] = sc.nextInt(); // steps
			}

			P =sc.nextInt(); // 트랩 갯수
			int[][] trap=new int[P][2];
			for(int i = 0; i < P; i++){
				trap[i][0] = sc.nextInt(); //트랩 Y
				trap[i][1] = sc.nextInt(); //트랩 X
			}
			//////////////////////////////
			//( 이 부분에 알고리즘 구현을 한다. )//
			//////////////////////////////
			Answer = 0;
			int cost = 1000;
			for(int p=0; p<P; p++) { 
				room[trap[p][0]][trap[p][1]] = -1;
			}
			
			loop:for(int p=0; p<player.length; p++) {
				int money = 0;
				int pnum = 0;
				int person_money =0;
				
				int i = player[p][0];
				int j = player[p][1];
				// 넘쳤거나, 지금위치가 함정이면 패스
				if( 1>i || i>Y || 1>j || j >X || room[i][j] == -1) {
					Answer -= 1000;
					continue loop;
				}
				
				int num_steps = player[p][2];
				// 시작위치 
				pnum = room[i][j];
				
				// steps 횟수만큼 이동 고고
				for(int s=0; s<num_steps; s++) {
					//앞 숫자 = 방향
					int d = pnum/10;
					//뒤 숫자 만큼 이동
					int step = pnum%10;
					i = i + dir[d][0]*step;
					j = j + dir[d][1]*step;
					// 넘쳤거나, 지금위치가 함정이면 패스
					if( 1>i || i>Y || 1>j || j >X || room[i][j] == -1) {
						Answer -= 1000;
						continue loop;
					}
					// 문제없으면 이동
					pnum = room[i][j];
					// 문제없으면 상금
					person_money = room[i][j]*100 - 1000 ;
				} // end for(int s=0; s<num_steps; s++)
				Answer = Answer+person_money;
			} // end loop:for
			
			System.out.println("#" + test_case + " " + Answer);
		}

	}
}
