package com.ssafy.algo;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution22소금쟁이합계2 {
	static long Answer;
	static int N, S;
	
	public static void strider_Move(int[][] lake, int[][] strider) {
		
	}
	public static void strider_Jump(int direction) {
		
	}
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("Solution22소금쟁이합계.txt"));
		Scanner sc=new Scanner(System.in);
		int T=sc.nextInt();//테스트 케이스 수

		for(int test_case=1; test_case<=T; test_case++){
			N=sc.nextInt(); //배열의 크기 NxN
			int[][] lake=new int[N][N];

			S=sc.nextInt();//소금쟁이(Strider)수
			int[][] strider=new int[S][3];
			for(int k=0; k<S; k++){
				strider[k][0]=sc.nextInt();//행위치
				strider[k][1]=sc.nextInt();//열위치
				strider[k][2]=sc.nextInt();//방향(1:↑  2:↓  3:←  4:→)
			}
			//////////////////////////////
			//( 이 부분에 알고리즘 구현을 한다. )//
			//////////////////////////////

			//방향(1:↑  2:↓  3:←  4:→)
			// 소금쟁이 연못에 배치하기
/*연못배치*/	for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					//배치 시작
					for(int k=0; k<S; k++){
						// i번째는 소금쟁이의 행, j번쨰는 소금쟁이 열이면,
						if(i==strider[k][0] && j ==strider[k][1])
							//거기에 소금쟁이 방향을 넣는다.
							lake[i][j]=strider[k][2];
					}//다 배치했음
				}
/*연못배치*/	}
			
			// 배치 다 했으니 소금쟁이를 움직여보자
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					//소금쟁이 0번부터 움직인다 S번까지 고
					for(int k=0; k<S; k++){
						//방향(1:↑  2:↓  3:←  4:→)
						switch(strider[k][2]) {
							case 1: // ↑
								// 점프 횟수
								for(int d = 3; d>=1; d--) {
									// 점프 횟수만큼 점프를 한다.
									
									// 강물 위 뚫고 나간것이니 죽음.
									if(i-d < 0) {
										lake[strider[k][0]][strider[k][1]] = -1;
										break;
									}
									
									// 1,2,3,4중 뭘 만나면 죽음
									if(lake[i-d][j] > 0) {
										// k번째 스파이더 위치에있던 스파이더는 죽어 = -1
										lake[strider[k][0]][strider[k][1]] = -1;
									}
									// 안만났다면
									else {
										//j-1 (1칸윗쪽을) j 값으로 주고,
										lake[i-d][j]= lake[strider[k][0]][strider[k][1]];
										// 기존 자리는 0으로 초기화
										lake[strider[k][0]][strider[k][1]]=0;
									}
									
								}
								break;
							case 2: // ↓
								// 점프 횟수
								for(int d = 3; d>=1; d--) {
									// 점프 횟수만큼 점프를 한다.
									
									// 강물 위 뚫고 나간것이니 죽음.
									if(i+d > N) {
										lake[strider[k][0]][strider[k][1]] = -1;
										break;
									}
									
									// 1,2,3,4중 뭘 만나면 죽음
									if(lake[i+d][j] > 0) {
										// k번째 스파이더 위치에있던 스파이더는 죽어 = -1
										lake[strider[k][0]][strider[k][1]] = -1;
									}
									// 안만났다면
									else {
										//j-1 (1칸윗쪽을) j 값으로 주고,
										lake[i+d][j]= lake[strider[k][0]][strider[k][1]];
										// 기존 자리는 0으로 초기화
										lake[strider[k][0]][strider[k][1]]=0;
									}
									
								}
								break;
							case 3: // ←
								// 점프 횟수
								for(int d = 3; d>=1; d--) {
									// 점프 횟수만큼 점프를 한다.
									
									// 강물 위 뚫고 나간것이니 죽음.
									if(j-d < 0) {
										lake[strider[k][0]][strider[k][1]] = -1;
										break;
									}
									
									// 1,2,3,4중 뭘 만나면 죽음
									if(lake[i][j-d] > 0) {
										// k번째 스파이더 위치에있던 스파이더는 죽어 = -1
										lake[strider[k][0]][strider[k][1]] = -1;
									}
									// 안만났다면
									else {
										//j-1 (1칸윗쪽을) j 값으로 주고,
										lake[i][j-d]= lake[strider[k][0]][strider[k][1]];
										// 기존 자리는 0으로 초기화
										lake[strider[k][0]][strider[k][1]]=0;
									}
									
								}
								break;
							case 4: // →
								// 점프 횟수
								for(int d = 3; d>=1; d--) {
									// 점프 횟수만큼 점프를 한다.
									
									// 강물 위 뚫고 나간것이니 죽음.
									if(j+d > N) {
										lake[strider[k][0]][strider[k][1]] = -1;
										break;
									}
									
									// 1,2,3,4중 뭘 만나면 죽음
									if(lake[i][j+d] > 0) {
										// k번째 스파이더 위치에있던 스파이더는 죽어 = -1
										lake[strider[k][0]][strider[k][1]] = -1;
									}
									// 안만났다면
									else {
										//j-1 (1칸윗쪽을) j 값으로 주고,
										lake[i][j+d]= lake[strider[k][0]][strider[k][1]];
										// 기존 자리는 0으로 초기화
										lake[strider[k][0]][strider[k][1]]=0;
									}
									
								}
								break;
						}
					}
				}
			}
			
			int num_strider = 0;
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					if(lake[i][j] > 0) {
						num_strider++;
					}
				}
			}
			
			Answer = num_strider;
			System.out.println("#"+test_case+" "+Answer);
		}
		sc.close();
	}
}
