package com.ssafy.algo;

import java.io.FileInputStream;
import java.util.Scanner;

public class DigitTest1 {
	public static void main(String[] args) throws Exception {
		//이렇게 하면 파일에 저장된 내용을 알아서 입력해서 읽을 수 있다.
		System.setIn(new FileInputStream("DigitTest1.txt"));
		int num = 1;
		int ia[] = new int[10];
		Scanner sc = new Scanner(System.in);
		
		//while (num !=0) {
		//	num = sc.nextInt();
		// 이렇게 사용 가능하다.
		while((num=sc.nextShort())!=0) {
			if(num<100) {
				ia[num/10]++;
			}
		}
		for (int i =0; i<ia.length ; i++) {
			if(ia[i] != 0) {
				System.out.println(i + " : "+ ia[i] + "개");
			}
		}
		sc.close();
	}
}
