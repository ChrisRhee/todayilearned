package com.ssafy.algo;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution22소금쟁이합계 {
	static long Answer;
	static int N, S;
	// 						             상         하          좌          우
	static int dir[][] = {{0,0},{-1,0},{1,0},{0,-1},{0,1}};
	static int jump[] = {3,2,1};
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("Solution22소금쟁이합계.txt"));
		Scanner sc=new Scanner(System.in);
		int T=sc.nextInt();//테스트 케이스 수

		for(int test_case=1; test_case<=T; test_case++){
			N=sc.nextInt(); //배열의 크기 NxN
			int[][] lake=new int[N][N];

			S=sc.nextInt();//소금쟁이(Strider)수
			int[][] strider=new int[S][3];
			for(int k=0; k<S; k++){
				strider[k][0]=sc.nextInt();//행위치
				strider[k][1]=sc.nextInt();//열위치
				strider[k][2]=sc.nextInt();//방향(1:↑  2:↓  3:←  4:→)
			}
			//////////////////////////////
			//( 이 부분에 알고리즘 구현을 한다. )//
			//////////////////////////////
			Answer = 0;
			
			loop:for(int s=0; s<S; s++) {
				//현재 자리에 누군가 있다면
				if(lake[strider[s][0]][strider[s][1]]==1) 
					continue loop;
				int d = strider[s][2];
				int i = strider[s][0];
				int j = strider[s][1];
				for(int k=0; k<jump.length; k++) {
					i = i + dir[d][0]*jump[k];
					j = j + dir[d][1]*jump[k];
					// 넘쳤거나, 지금위치에 무언가가 있으면 패스
					if( 0>i || i>=N || 0>j || j >=N || lake[i][j]==1 ) 
						continue loop;
				}
				lake[i][j]=1;
				Answer++;
			}
			
			System.out.println("#"+test_case+" "+Answer);
		}
		sc.close();
	}
}
