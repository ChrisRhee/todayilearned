package com.ssafy.algo;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution13빌딩 {
	static int T, N;
	static int Answer;
	static int dir[][] = {{-1,0},{0,1},{1,0},{0,-1},{-1,-1},{-1,1},{1,1},{1,-1}};
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("Solution13빌딩.txt"));
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			// N= 넓이
			N = sc.nextInt();
			char[][] area=new char[N][N];
			// N x N 에 만들어짐
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					// 문자로 다 입력
					area[i][j] = sc.next().charAt(0);
				}
			}
			//////////////////////////////
			//( 이 부분에 알고리즘 구현을 한다. )//
			//////////////////////////////
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					// 0,0부터 순회 시작하다 B를 만나면
					
					if(area[i][j]=='B') {
						// 그 B위치 주변 B의 상, 하, 좌, 우
						for(int d = 0; d<dir.length; d++) {
							int ii=i+dir[d][0];
							int jj=j+dir[d][1];
							if((0<=ii&&ii<N) && ( 0<=jj && jj<N ) ) {
								if(area[ii][jj]=='G') {
									//검색해서 G가 있으면 0
									area[i][j] = 'T';
									break;
								}
							}//end if((0<=ii&&ii<N) && ( 0<=jj && jj<N ) )
						}//end for(int d = 0; d<dir.length; d++)
					}//end if(area[i][j]=='B')
				}// end for(int j=0; j<N; j++)
			}// end for(int i=0; i<N; i++)
			
			Answer = 0;
			int count = 0;
			// B를 찾아서 그 행,열에 G뺀 값을 구하자
			for(int i = 0; i < N; i++){
				for(int j = 0; j< N; j++) {
					// 쭉 가다가 B가 나왔다!?!?!?
					if(area[i][j]=='B') {
						//3,4 면?
						for(int k =0; k<N; k++) {
							//i열(세로)을 먼저 돌고
							if(area[i][k] != 'G') count++;
							//j열(가로)를 돈다.
							if(area[k][j] != 'G') count++;
						}
						//max_count에 count를 넣는다.
						if(Answer < count) {
							Answer = count;
						}
						//다 넣었으니 count = 0;
						count = 0;
					}
				}
			}//for(int i = 0; i < N; i++)
			Answer--;
			System.out.println("#"+test_case+" "+Answer);
		}
	}
}