package com.ssafy.algo;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution13빌딩_teacher {
	static int T, N;
	static int Answer;
	static int dir[][] = {{-1,0},{0,1},{1,0},{0,-1},{-1,-1},{-1,1},{1,1},{1,-1}};
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("Solution13빌딩.txt"));
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			// N= 넓이
			N = sc.nextInt();
			char[][] area=new char[N][N];
			// N x N 에 만들어짐
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					// 문자로 다 입력
					area[i][j] = sc.next().charAt(0);
				}
			}
			//////////////////////////////
			//( 이 부분에 알고리즘 구현을 한다. )//
			//////////////////////////////
			Answer =0;
			for(int i =0;i<N;i++) {
				int count =0;
				loop:for(int j =0;j<N;j++) {
					if(area[i][j]=='B') {
						for(int d=0; d<dir.length; d++) {
							int ii = i + dir[d][0];
							int jj = j + dir[d][1];
							if(( 0<=ii && ii<N ) && ( 0<=jj && jj<N ))
								if(area[ii][jj]=='G') {
									count=2;
									continue loop;
								}	
						}
						for(int k=0; k<N; k++) {
							if(area[i][k]=='B') count++;
							if(area[k][j]=='B') count++;
						}
						count--;
					}
					if(Answer<count) Answer = count;
				}// end loop:for
			}
			
			
			System.out.println("#"+test_case+" "+Answer);
		}
	}
}