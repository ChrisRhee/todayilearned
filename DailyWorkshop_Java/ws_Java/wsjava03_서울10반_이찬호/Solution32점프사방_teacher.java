package com.ssafy.algo;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution32점프사방_teacher {
	static int Answer;
	static int Y, X, N, P;
	//                            우        하           좌           상  
	static int dir[][] = {{0,0},{0,1},{1,0},{0,-1},{-1,0}};

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("Solution32점프사방.txt"));
		Scanner sc = new Scanner(System.in);

		int T = sc.nextInt(); // Test case

		// fill up the data
		for (int test_case = 1; test_case <= T; test_case++) {

			Y = sc.nextInt(); // position row
			X = sc.nextInt(); // position col
			N = sc.nextInt(); // number of player

			int[][] room = new int[Y + 1][X + 1];// because start from 1;
			// fill up table
			for (int i = 1; i <= Y; i++) {
				for (int j = 1; j <= X; j++) {
					room[i][j] = sc.nextInt();
				}
			}

			int[][] player = new int[N][3];
			for (int i = 0; i < N; i++) {
				player[i][0] = sc.nextInt(); // coord Y
				player[i][1] = sc.nextInt(); // coord X
				player[i][2] = sc.nextInt(); // steps
			}

			P =sc.nextInt(); // 트랩 갯수
			int[][] trap=new int[P][2];
			for(int i = 0; i < P; i++){
				trap[i][0] = sc.nextInt(); //트랩 Y
				trap[i][1] = sc.nextInt(); //트랩 X
			}
			//////////////////////////////
			//( 이 부분에 알고리즘 구현을 한다. )//
			//////////////////////////////
			
			for(int p=0; p<P; p++) { 
				room[trap[p][0]][trap[p][1]] = -1;
			}
			Answer =0;
			loop:for(int n=0; n<N; n++) {
				int i = player[n][0];
				int j = player[n][1];
				if( 0>=i || i>Y || 0>=j || j >X || room[i][j] == -1) {
					Answer -= 1000;
					continue loop;
				}
				int c = player[n][2];
				for(int k=0; k<c; k++) {
					int d = room[i][j]/10;
					int m = room[i][j]%10;
					i = i+dir[d][0]*m;
					j = j+dir[d][1]*m;
					if( 0>=i || i>Y || 0>=j || j >X || room[i][j] == -1) {
						Answer -= 1000;
						continue loop;
					}
				}
				Answer = Answer - 1000+ room[i][j]*100;
			}
			
			System.out.println("#" + test_case + " " + Answer);
		}

	}
}
