package com.ssafy.Magazine;

public class Magazine {
	private String isbn;
	private String title;
	private String author;
	private String publisher;
	private int year;
	private int month;
	private int price;
	private String desc;
	
	
	public Magazine(String isbn, String title,String author,String publisher,int year, int month, int price, String desc) {
		this.isbn = isbn;
		this.title = title;
		this.author = author;
		this.publisher = publisher;
		this.year = year;
		this.month = month;
		this.price = price;
		this.desc = desc;
		
	}
	public String toString() {
		System.out.printf("%-8s| %-14s| %-6s| %-10s| %-6d| %-20s| %d.%d",isbn,title,author,publisher,price,desc,year,month);
		System.out.println();
		String result = "";
		return result;
	}
}
