package com.ssafy.Book;

public class Book {
	private String isbn;
	private String title;
	private String author;
	private String publisher;
	private int price;
	private String desc;
	
	public Book(String isbn, String title,String author,String publisher,int price, String desc) {
		this.isbn = isbn;
		this.title = title;
		this.author = author;
		this.publisher = publisher;
		this.price = price;
		this.desc = desc;
		
	}
	public String toString() {
		System.out.printf("%-8s| %-14s| %-6s| %-10s| %-6d| %s",isbn,title,author,publisher,price,desc);
		System.out.println();
		String result = "";
		return result;
	}
}
