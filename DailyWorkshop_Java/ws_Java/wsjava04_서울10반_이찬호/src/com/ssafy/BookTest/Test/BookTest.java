package com.ssafy.BookTest.Test;

import com.ssafy.Book.*;
import com.ssafy.Magazine.Magazine;

public class BookTest {
	public static void main(String[] args) {
		Book b = new Book("21424","Java Pro","김하나","Jean.kr",15000,"Java 기본 문법");
		                           //이거 한글이랑 영어랑 띄어쓰기 크기가 달라서 줄이 안맞습니다.. ㅠㅠ
		Book b2 = new Book("35355","분석설계                ","소나무","Jean.kr",30000,"SW 모델링");
		Magazine m = new Magazine("35535","Java World","편집부","Java.com",2018,2,7000,"첫걸음");
		
		System.out.println("*********************** 도서 목록 **************************");
		b.toString();
		b2.toString();
		m.toString();
		
	}
}
