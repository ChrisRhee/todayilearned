package com.ssafy.reservation;

import java.util.ArrayList;
import java.util.List;

import com.ssafy.movie.Movie;

public class ReservationMgr {
	private List<Reservation> list = new ArrayList<>(); 
	
	private static ReservationMgr instance = new ReservationMgr();
	
	private ReservationMgr() {}
	
	public static ReservationMgr getInstance() {
		return instance;
	}
	
	public boolean add(Reservation reserv) {
		list.add(reserv);
		return false;
	}
	
	public List<Reservation> search(){
		return list;
	}
}
