package com.ssafy.reservation;

public class Reservation {
	private String memName;
	private String MovName;
	private int count;
	
	public Reservation(String memName, String movName, int count) {
		setMemName(memName);
		setMovName(movName);
		setCount(count);
	}
	
	public String getMemName() {
		return memName;
	}
	public void setMemName(String memName) {
		this.memName = memName;
	}
	public String getMovName() {
		return MovName;
	}
	public void setMovName(String movName) {
		MovName = movName;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("이름: ");
		builder.append(memName);
		builder.append(", 영화: ");
		builder.append(MovName);
		builder.append(", 표 수: ");
		builder.append(count);
		return builder.toString();
	}
	
}
