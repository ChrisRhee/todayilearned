package com.ssafy.member;

import java.util.ArrayList;
import java.util.List;

public class MemberTest {
	public static void main(String[] args) {
		IMemberMgr mgr = MemberMgrImpl.getInstance();
		mgr.add(new Member("홍길동", 20, "hgd@email.com"));
		mgr.add(new Member("손오공", 25, "sog@email.com"));
		mgr.add(new Member("사오정", 30, "soj@email.com"));
		mgr.add(new Member("홍길순", 35, "soj@email.com"));
		mgr.add(new Member("홍길동", 20, "hgd@email.com"));
		mgr.add(new Member("손오공", 25, "sog@email.com"));
		mgr.add(new Member("사오정", 30, "soj@email.com"));
		mgr.add(new Member("홍길순", 35, "soj@email.com"));
		for(Member m: mgr.search()) System.out.println(m);
		System.out.println();
		
		Member me = mgr.search("손오공");
		System.out.println(me);
		System.out.println();
		
		me= new Member("손오공", 55, "sog@email.com"); 
		mgr.update(me);
		System.out.println(me);
		
		
//		List<Member> li = new ArrayList<Member>();
//		for(Member m : li) System.out.println(m);
//		
//		Collections.sort(li);
//		System.out.println("\n--- sorted by name ---");
//		for(Member m : li) System.out.println(m);
//		
//		Collections.sort(li, new AgeComparator());
//		System.out.println("\n--- sorted by age ---");
//		for(Member m : li) System.out.println(m);
	}
}
