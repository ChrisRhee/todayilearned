package com.ssafy.member;

public class Member implements Comparable<Member>{
	private String name;
	private int age;
	private String email;
	
	public Member(String name, int age, String email) {
		setName(name);
		setAge(age);
		setEmail(email);
	}
	
	public Member(String name, int age) {
		this(name, age, "");
	}
	
	public Member(String name, String email) {
		this(name, 101, email);
	}
	
	public Member(String name) {
		this(name, 101, "");
	}
	
	public Member() {
		this("", 101, "");
	}
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public int hashCode() {
		int namehash= getName().hashCode();
		int agehash = new Integer(getAge());
		return namehash^agehash;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Member) {
			Member m = (Member)obj;
			if(getEmail().equals(m.getEmail()) && 
					getAge()==m.getAge()) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getName()).append(" ")
		.append(getAge()).append(" ")
		.append(getEmail());
		
		return sb.toString();
	}

	@Override
	public int compareTo(Member m) {
		// 앞에거가 뒤에보다 큰가? 1 작으면 0 인가봄
		//return name.compareTo(m.getName());
		
		int comp = name.compareTo(m.getName());
		//이름이 같으면?
		if(comp ==0) {
			// 그담엔 나이를 비교한다.
			// 이렇게 하면 장비의 종류대로 정렬하고, 직업군 대로 정렬하고, 그이후에 레벨순으로 정렬할 수 있다.
			comp=age-m.getAge();
		}
		return comp;
	}
	
}
