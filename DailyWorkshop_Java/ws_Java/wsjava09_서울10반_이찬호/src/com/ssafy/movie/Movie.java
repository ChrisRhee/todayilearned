package com.ssafy.movie;

public class Movie implements Comparable<Movie>{
	private String title;
	private String director;
	private int grade;
	private String genre;
	private String summary;
	private int time;
	
	public Movie(String title, String director, int grade, String genre, String summary, int time) {
		setTitle(title);
		setDirector(director);
		setGrade(grade);
		setGenre(genre);
		setSummary(summary);
		setTime(time);
	}
	
	public Movie(String title, String director, int grade, String genre) {
		this(title, director, grade, genre, "",0);
	}
	
	public Movie() {
		this("", "", 0, "", "",0);
	}
	
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Movie [title=");
		builder.append(title);
		builder.append(", director=");
		builder.append(director);
		builder.append(", grade=");
		builder.append(grade);
		builder.append(", genre=");
		builder.append(genre);
		builder.append(", summary=");
		builder.append(summary);
		builder.append(", time=");
		builder.append(time);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int compareTo(Movie movie) {
		// 이름순으로 고고
		return title.compareTo(movie.getTitle());
	}
	
}
