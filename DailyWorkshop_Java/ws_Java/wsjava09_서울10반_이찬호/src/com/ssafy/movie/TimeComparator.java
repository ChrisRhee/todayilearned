package com.ssafy.movie;

import java.util.Comparator;

public class TimeComparator implements Comparator<Movie>{

	@Override
	public int compare(Movie o1, Movie o2) {
		// TODO Auto-generated method stub
		return o1.getTime()-o2.getTime();
	}
}
