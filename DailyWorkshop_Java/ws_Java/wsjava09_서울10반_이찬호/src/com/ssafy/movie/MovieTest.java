package com.ssafy.movie;

import java.util.*;

import com.ssafy.member.IMemberMgr;
import com.ssafy.member.Member;
import com.ssafy.member.MemberMgrImpl;
import com.ssafy.reservation.Reservation;
import com.ssafy.reservation.ReservationMgr;

public class MovieTest {
	static IMovieMgr movMgr = MovieMgrImpl.getInstance();
	static IMemberMgr memMgr = MemberMgrImpl.getInstance();
	static ReservationMgr resvs = ReservationMgr.getInstance();
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		// 영화 추가
		movMgr.add(new Movie("호빗","이찬호",15,"액션","존잼꿀잼",10));
		movMgr.add(new Movie("해리포터","해리",12,"SF판타지","핵잼허니잼",11));
		movMgr.add(new Movie("어벤져스","이찬호",12,"SF판타지","와 진짜 역대급",13));
		movMgr.add(new Movie("어벤져스2","토니",13,"SF판타지","와 ",6));
		movMgr.add(new Movie("어벤져스3","스타",14,"SF판타지","와 진짜",12));
		movMgr.add(new Movie("어벤져스4","크크",15,"SF판타지","역대급",13));
		movMgr.add(new Movie("아이언맨","이찬호",7,"액션","아이언맨죠습니다",15));
		movMgr.add(new Movie("토르","웅컄캬",10,"로맨스","토르짱",13));
		movMgr.add(new Movie("토르2","사과우유",7,"개그","도르마무",7));
		movMgr.add(new Movie("토르3","이찬호",7,"로맨스코미디","토르도짱",8));
		movMgr.add(new Movie("캡틴아메리카","이찬호",19,"SF판타지","캡틴짱",20));
		
		// 사람 추가
		memMgr.add(new Member("홍길동", 20, "hgd@email.com"));
		memMgr.add(new Member("손오공", 25, "sog@email.com"));
		memMgr.add(new Member("사오정", 30, "soj@email.com"));
		memMgr.add(new Member("홍길순", 35, "soj@email.com"));
		memMgr.add(new Member("홍길동", 20, "hgd@email.com"));
		memMgr.add(new Member("손오공", 25, "sog@email.com"));
		memMgr.add(new Member("사오정", 30, "soj@email.com"));
		memMgr.add(new Member("홍길순", 35, "soj@email.com"));
		
		int end = -1;
		while(end !=0) {
			System.out.println("--- 영화 예매 관리 ---");
			System.out.println("1. 영화 관리");
			System.out.println("2. 멤버 관리");
			System.out.println("3. 예매 관리");
			System.out.println("0. 종료");
			System.out.print("선택: ");
			end = sc.nextInt();
			
			switch(end) {
				case 1: //영화관리
					movie();
					break;
				case 2: // 멤버관리
					member();
					break;
				case 3: // 예매 관리
					reserve();
					break;
				case 0: // 종료
					break;
			}
			System.out.println();
		}
		sc.close();
	}
	
	private static void member() {
		int end=-1;
		while(end!=0) {
			System.out.println("");
			System.out.println("**1.회원관리**");
			System.out.println("1.회원등록");
			System.out.println("2.회원수정");
			System.out.println("3.회원삭제");
			System.out.println("4.회원조회");
			System.out.println("5.전체조회");
			System.out.println("0.종료");
			System.out.print("선택? ");
			end=sc.nextInt();
			switch(end) {
				case 1:{
					System.out.print("회원이름: ");
					String name=sc.next();
					System.out.print("회원나이: ");
					int age=sc.nextInt();
					System.out.print("회원이메일: ");
					String email=sc.next();
					Member m=new Member(name, age, email);
					memMgr.add(m);
					break;
				}
				case 2:{
					System.out.print("수정할 회원이름: ");
					String name=sc.next();
					System.out.print("수정할 회원나이: ");
					int age=sc.nextInt();
					System.out.print("수정할 회원이메일: ");
					String email=sc.next();
					Member m=new Member(name, age, email);
					memMgr.update(m);
					break;
				}
				case 3:{
					System.out.print("삭제할 회원이름: ");
					String name=sc.next();
					memMgr.delete(name);
					break;
				}
				case 4:{
					System.out.print("조회할 회원이름: ");
					String name=sc.next();
					Member m=memMgr.search(name);
					System.out.println(m);
					break;
				}
				case 5:{
					for(Member m:memMgr.search()){
						System.out.println(m);
					}
					break;
				}
			}
		}
	}
	private static void movie() {
		int end=-1;
		while(end!=0) {
			System.out.println("");
			System.out.println("**2.영화관리**");
			System.out.println("1.영화등록");
			System.out.println("2.영화수정");
			System.out.println("3.영화삭제");
			System.out.println("4.영화조회");
			System.out.println("5.전체조회");
			System.out.println("6.시간대별조회");
			System.out.println("7.시간대별전체조회");
			System.out.println("0.종료");
			System.out.print("선택? ");
			end=sc.nextInt();
			switch(end) {
				case 1:{ 
					System.out.print("영화타이틀: ");
					String title=sc.next();
					System.out.print("영화시간: ");
					int time=sc.nextInt();
					System.out.print("영화감독: ");
					String director=sc.next();
					System.out.print("영화등급: ");
					int grade=sc.nextInt();
					System.out.print("영화장르: ");
					String genre=sc.next();
					System.out.print("영화설명: ");
					String summary=sc.next();
					Movie m=new Movie(title, director, grade, genre, summary, time);
					movMgr.add(m);
					break;
				}
				case 2:{  
					System.out.print("수정할 영화타이틀: ");
					String title=sc.next();
					System.out.print("수정할 영화시간: ");
					int time=sc.nextInt();
					System.out.print("수정할 영화감독: ");
					String director=sc.next();
					System.out.print("수정할 영화등급: ");
					int grade=sc.nextInt();
					System.out.print("수정할 영화장르: ");
					String genre=sc.next();
					System.out.print("수정할 영화설명: ");
					String summary=sc.next();
					Movie m=new Movie(title, director, grade, genre, summary, time);
					movMgr.update(m);
					break;
				}
				case 3:{ 
					System.out.print("삭제할 영화타이틀: ");
					String title=sc.next();
					movMgr.delete(title);
					break;
				}
				case 4:{ 
					System.out.print("조회할 영화이름: ");
					String title=sc.next();
					Movie m=movMgr.search(title);
					System.out.println(m);
					break;
				}
				case 5:{ 
					for(Movie m:movMgr.search()){
						System.out.println(m);
					}
					break;
				}
				case 6:{ 
					System.out.print("조회할 영화의 시간: ");
					int time=sc.nextInt();
					for(Movie m:movMgr.search(time)){
						System.out.println(m);
					}
					break;
				}
				case 7:{ 
					for(Movie m:movMgr.searchTime()){
						System.out.println(m);
					}
					break;
				}
			}
		}
	}
	
	private static void reserve() {
		int end=-1;
		while(end!=0) {
			System.out.println("");
			System.out.println("**3.영화예매**");
			System.out.println("1.예매");
			System.out.println("2.조회");
			System.out.println("0.종료");
			System.out.print("선택? ");
			end=sc.nextInt();
			switch(end) {
				case 1:{ 
					System.out.print("예매할 회원이름: ");
					String name=sc.next();
					System.out.print("예매할 영화이름: ");
					String title=sc.next();
					System.out.print("예매할 수량: ");
					int cnt=sc.nextInt();
					resvs.add(new Reservation(name,title,cnt));
					break;
				}
				case 2:{  
					for(Reservation resv : resvs.search()) {
						System.out.println(resv);
					}
					break;
				}
			}
		}
	}
}
