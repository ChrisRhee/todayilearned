package com.ssafy.movie;

import java.util.List;

public interface IMovieMgr {
	void add(Movie m);
	// 전체 넘기는데 영화이름으로 소팅해서 넘김
	List<Movie> search();
	// 전체 넘기는데 시간으로 소팅해서 넘김
	List<Movie> searchTime();
	// 영화명 검색 하나만 리턴
	Movie search(String title);
	// 영화명별 검색 = 소트되게만 comparable 구현
	// 시간대별 검색
	List<Movie> search(int time);
	void update(Movie m);
	void delete(String title);
	
	//  
	/** reservation
	 * String Member name
	 * String Movie title
	 * String reserv num
	 * int reserv count
	 * */
	
	/** reservationMgr
	 * 예약하는 메소드
	 * 
	 * */
}
