package com.ssafy.movie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class MovieMgrImpl implements IMovieMgr{
	private List<Movie> movies = new ArrayList<>(); 
	
	private static MovieMgrImpl instance = new MovieMgrImpl();
	
	private MovieMgrImpl() {}
	
	public static MovieMgrImpl getInstance() {
		return instance;
	}

	@Override
	public void add(Movie m) {
		movies.add(m);
	}

	@Override
	public List<Movie> search() {
		List<Movie> tmp = movies;
		if(movies.size() == 0) {
			return null;
		}
		Collections.sort(tmp);
		return tmp;
	}

	@Override
	public List<Movie> searchTime() {
		List<Movie> tmp = movies;
		if(movies.size() == 0) {
			return null;
		}
		Collections.sort(tmp,new TimeComparator());
		return tmp;
	}

	@Override
	public Movie search(String title) {
		for (Movie movie : movies) {
			if(movie.getTitle().equals(title))
				return movie;
		}
		return null;
	}

	@Override
	public List<Movie> search(int time) {
		List<Movie> tmp = new ArrayList<>();
		for (Movie movie : movies) {
			if(movie.getTime()==time)
				tmp.add(movie);
		}
		return tmp;
	}

	@Override
	public void update(Movie m) {
		Movie old = search(m.getTitle());
		if(old != null) {
			int index = movies.indexOf(old);
			movies.set(index, m);
		}
	}

	@Override
	public void delete(String title) {
		for (Movie movie : movies) {
			if(movie.getTitle().equals(title)){
				movies.remove(movie);
				break;
			}
		}
	}
}
