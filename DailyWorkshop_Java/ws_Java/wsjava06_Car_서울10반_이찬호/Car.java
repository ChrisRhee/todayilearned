package com.ssafy;

public class Car {
    private String num;
    private String model;
    private int price;
    
    
    public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Car(String num, String model, int price){
		setNum(num);
		setModel(model);
		setPrice(price);
    }
    
    public Car(){
    	this("", "", 0);
    }
	
	
	public String toString(){
		StringBuilder str = new StringBuilder();
		str.append("num: ")
		.append(getNum())
		.append(", model: ")
		.append(getModel())
		.append(", price: ")
		.append(getPrice());
		
        return str.toString();
    }

}
