package com.ssafy;

import java.util.Arrays;

public class CarTest {

    public static void main(String[] args){
    	CarMgr mgr = new CarMgr();
    	
    	mgr.add(new Car("111","소나타",1000));
    	mgr.add(new Car("112","그랜저",2000));
    	mgr.add(new Car("113","이찬호",3000));
    	mgr.add(new Car("114","람보르",2000));
    	mgr.add(new Car("115","티코리",2200));
    	mgr.add(new Car("116","아우디",2000));
    	
    	// - 조회 ------------
    	System.out.println("- 조회 -");
    	Car cars[] = mgr.search();
    	for(Car c:cars)System.out.println(c);
    	System.out.println();
    	
    	// - 차량 번호조회
    	System.out.println("- 차량 번호 조회 -");
    	Car c = mgr.search("112");
    	System.out.println(c);
    	System.out.println();
    	
    	// - 가격조회
    	System.out.println("- 가격 조회 -");
    	cars = mgr.search(2000);
    	for(Car s:cars)System.out.println(s);
    	System.out.println();
    	
    	// - 차량변경
    	System.out.println("- 차량 변경 -");
    	mgr.update("112", 3334);
    	cars = mgr.search();
    	for(Car d:cars)System.out.println(d);
    	System.out.println();
    	
    	// - 삭제
    	System.out.println("- 차량 삭제 -");
    	mgr.delete("112");
    	cars = mgr.search();
    	for(Car g:cars)System.out.println(g);
    	System.out.println();
    	
    	// - 사이즈 / 총금액
    	System.out.println("- 사이즈 / 총금액 -");
    	System.out.println(mgr.size()+" / "+mgr.totalPrice());
    	System.out.println();
    }

}
