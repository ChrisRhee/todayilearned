package com.ssafy;

import java.util.Arrays;

public class CarMgr {
    private Car cars[] = new Car[100];
    private int index;

    public void add(Car c){
    	cars[index++]= c;
    }

    public Car[] search(){
    	Car[] temp = new Car[index];
    	for(int i=0; i<index; i++) {
    		temp[i] = cars[i];
    	}
        return temp;
    }

    public Car search(String num){
    	for(int i=0; i<index; i++) {
    		Car c = cars[i];
    		String nm = c.getNum();
    		if(nm.equals(num)) return c;
    	}
        return null;
    }

    public Car[] search(int price){
    	int tempIndex =0;
    	Car[] temp = new Car[index];
    	for(int i=0; i<index; i++) {
    		if(cars[i].getPrice() == price) {
    			temp[tempIndex] = cars[i];
    			tempIndex++;
    		}
    	}
        return temp;
    }

    public void update(String num, int price){
    	for(int i=0; i<index; i++) {
    		if(cars[i].getNum().equals(num)) 
    			cars[i].setPrice(price);
    	}
    }

    public void delete(String num){
    	for(int i=0; i<index; i++) {
    		if(cars[i].getNum().equals(num)) {
    			for(int j=i; j<index; j++) {
    				cars[j] = cars[j+1];
    			}
    			index--;
    		}
    	}
    }

    public int size(){
        return index+1;
    }

    public int totalPrice(){
    	int total =0;
    	for(int i=0; i<index; i++) {
    		total += cars[i].getPrice();
    	}
        return total;
    }

}
