package com.ssafy.java;

import java.util.Arrays;

public class ArrayTest {
	public static int[] list={34,23,64,25,12,75,22,88,53,37};
	public static void print(){
		 for(int i  = 0; i < list.length; i ++) {
			 System.out.print(list[i]+" ");
		 }
		 System.out.println();
	}
	public static void total(){
		int total = 0;
		for(int i  = 0; i < list.length; i ++) {
			 total += list[i];
		}
		System.out.println("배열의 합 : " + total);
	} 
	public static void average(){
		int total = 0;
		float average = 0.0f;
		float count = 0.0f;
		for(int i  = 0; i < list.length; i ++) {
			 total += list[i];
			 count++;
		}
		average = total/count;
		System.out.println("배열의 평균 : " + (int)average);
	}
	public static void minimum(){
		int min = list[0];
		for(int i  = 0; i < list.length; i ++) {
			 if(min > list[i]) {
				 min = list[i];
			 }
		}
		System.out.println("배열의 최소값 : " + min);
	}
	public static void selectionSort(){
		Arrays.sort(list);
	}
	
	public static void main(String[] args) {
		print();
		total();
		average();
		minimum();
		System.out.println("=== selection sort (Ascending Order)===");
		selectionSort();
		print();
	}

}
