package com.ssafy.java;
import java.util.Random;

public class Lotto {
	public int check[] = new int[6];
	
	public Lotto() {
		int checks[] = new int[]{0,0,0,0,0,0};
		this.check = checks;
	}
	
	//1~45까지의 랜덤 수를 리턴
	public int getNum() {
		Random r = new Random();
		int num = r.nextInt(45)+1;
		return num;
	}
	
	// 기존에 나온 수였는지 체크하기
	public void numCheck() {
		for(int i = 0; i < check.length; i++) {
			int num = getNum();
			check[i] = num;
			for (int j = 0; j<i; j++) {
				if(check[i] == check[j]) {
					i--;
					break;
				}
			}
		}
	}
	
	public void printLotto() {
		numCheck();
		System.out.println("이번주 당첨 번호는");
		for(int i =0; i< check.length; i++) {
			System.out.print(check[i]+" ");
		}
		System.out.print("입니다.");
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 이렇게 랜덤수를 얻어낼 수 있다.
		Lotto l = new Lotto();
		l.printLotto();
		
	}

}
