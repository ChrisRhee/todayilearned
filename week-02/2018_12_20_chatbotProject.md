***
contents : python, chatbot, slack, team project

date : 2018 12 21
***

## 챗봇
챗봇을 이용한 팀 프로젝트를 시작했다. 우리팀은 챗봇으로 무얼 만들까 하다가 딴짓을 할 수 있는 **끝말잇기 챗봇**을 만들자고 기획했고, 바로 설계를 했다. 여러 기능들은 다 제쳐두고, 내가 한 단어 쓰면, 그거에 맞는 단어를 챗봇이 답하는 방식 정도만 생각했다. 왜냐하면 기능 구현하는데 그정도로 시간이 많지가 않았기 때문에 ㅠㅠ 개발은 3명이서 진행하는 것이었기 때문에, 챗봇에서 필요한 기능들을 하나씩 맡아서 제작했고 전체적인 틀은 내가 제작했다. 

끝말잇기를 하는데는 생각보다 많은 조건들이 필요했다. 챗봇이 대답하는 답의 값을 사이트에서 긁어와서, 그중에 답할 수 있는 단어들만 필터링을 거쳤어야 했다. 그 사이트에서는 동사, 형용사 등 끝말잇기에는 부적합한 단어들도 많았기에 필터링을 하는 함수들이 꽤 많았다.

~~~python
# 2~3글자만 남기는 함수
def word_2to3(main_keywords):
# user의 입력 단어가 괜찮은 단어인지 판별
def iscurrect_user_word(text):
# 유저의 단어 마지막 글자로 시작되는 단어들 검색하는 함수
def startswith_endof_userword(text):
# 동사, 형용사등의 조사들이 붙은 단어들 삭제하는 함수
def remove_letter (word):
# 북한어, 방언 등 끝말잇기에 부적절한 단어를 거르는 함수
def contants_filter(contents):
~~~

사실 이거 외에도 더 필요한 함수들이 많았지만, 최소한으로 필요한 것들을 고른게 저 것들이었다. 여기에서 크롤링, 문자 편집 등 1,2째 날에 배웠던 것들을 매우매우 많이 활용하게 됐었다. 
