***
contents : python

date : 2018 12 17 
***

# python start
파이썬을 배웠다. 파이썬은 마지막 학기에 기초 수업을 들은 것 말고는 배운적이 없었다. 그때도 기본만 배우고 뒤로갈수록 학점이 개판이었다. ㅋ 그런 상황인데 파이썬을 가지고 **크롤링**을 한다니 매우 큰 걱정이 됐지만 따라 가기로 했다.

# 크롤링
```python
import urllib.request
from bs4 import BeautifulSoup

def main():
    print("커뮤니티 댓글")

    # 댓글를 수집할 사이트 주소 입력
    url = "https://pann.nate.com/talk/344083297"

    # URL 주소에 있는 HTML 코드를 soup에 저장합니다.
    soup = BeautifulSoup(urllib.request.urlopen(url).read(), "html.parser")

    # 1. 댓글이 있는 태크 dd 찾기
    # 2. class_="usertxt class 찾기"
    # for 반복문과 get_text()를 사용해서 출력

    for i in soup.find_all("dd",class_="usertxt"):
        print(i.get_text().strip("\n"))

if __name__ == "__main__":
    main()
```

파이썬에 BeautifulSoup을 이용하면 html에 있는 정보를 긁어올 수 있게 된다. 진짜 충격 받은 기능. 이 기능을 통해 챗봇을 만드는 것에 이용했다.

# 텍스트 가공
정보들을 가공하는 방법을 배웠다. 문자열을 가공하는 기초적인 방법부터, 정규식, 엑셀 등 여러곳에서의 정보를 가공하는 방법을 배웠다. 처음에 학교에서 파이썬을 배울때는 아 왜 이런 쓸데없는걸 굳이 가르칠까 했는데, **크롤링**을 배우고 나니까 이 가공이 **왜 필요한지**를 알게 됐다. 너무 다양하고 가공되지 않은 데이터에서 내가 원하는 데이터를 가져다 쓸 수가 없는 상황인 경우가 많다. 그래서 내가 원하는 데이터를 얻기 위해 받은 데이터를 가공해야 더 원하는 결과를 얻을 수 있었다. 이건 진짜 프로젝트 할 때 많이 쓰였다.

# 데이터 시각화
데이터들을 차트로 보여주는 것을 배웠다. 보통 파워포인트로 직접 차트를 그렸는데 파이썬에서는 이걸 기본적으로 제공을 해줘서 데이터 값만 넣으면 차트로 만들어주어 보여주었다. 차트는 확실히 한눈으로 데이터 파악하기는 좋다.

# 미션
## 블로그 댓글 수집
블로그에 있는 댓글을 가져오는 문제. 배운대로 적용해서 해당 사이트 댓글에있는 태그와 클래스만 넣고 가져와서 보여주면 되는 문제였다.

~~~ python
import urllib.request
from bs4 import BeautifulSoup

def main():
    # URL 데이터를 가져올 사이트 url 입력
    url = "https://himchanyoon1992.tistory.com/1"

    # URL 에 있는 HTML 코드를 soup에 저장합니다.
    soup = BeautifulSoup(urllib.request.urlopen(url).read(), "html.parser")

    comments = []

    # 댓글 부분을 찾아서 list에 하나씩 댓글을 삽입합니다.
    for comment in soup.find_all("span",class_="txt_reply"):
        comments.append(comment.get_text())

    # list를 출력합니다.
    print(comments)

if __name__ == "__main__":
    main()
~~~

## 여러 페이지 댓글 수집
여러페이지에 접근을 하는데, 텍스트 가공을 이용해 url 부분에 for문을 통해 차례대로 사이트를 방문하도록 한 후 댓글을 가져오는 것이었다. 사이트주소도 결국 문자열로 입력하고 받는 것이다보니 이런게 가능하더라. 이 기능은 차후 만들 끝말잇기 봇에서 유용하게 쓰였다.

## 정규표현식 사용해보기
정규표현식을 사용해서 글에 있는 숫자들만 출력하는 프로그램이었다. 정규식의 무서움을 느낄 수 있는 부분이었다. 정규식은 어떻게든 많이 쓰이기 때문에 쓸 때마다 연습해두면 도움이 되겠구나 싶었다. 

숫자만 가져오는 정규식 : "[0-9]+"

영어만 가져오는 정규식 : "[A-Z]

여기서 +는 **한글자 이상의** 라는 의미를 담고 있다. 그래서 한글자 이상의 영어단어들, 한글자 이상의 숫자들. 이렇게 쓰인다. 

한글만 가져오는 정규식 : "[가-힣]+" 

이것도 가능하더라. 

정규식은 아래의 사이트에서 직접 실험해보고 작성해 볼 수 있다. 유용한 사이트니 첨부함

https://regexr.com/