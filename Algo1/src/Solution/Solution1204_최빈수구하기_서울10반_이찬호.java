package Solution;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;
public class Solution1204_최빈수구하기_서울10반_이찬호 {
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1204.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			int max = 0;
			int scores[] = new int[101];
			int score = 0;
			int index =0;
			// text_case �ϳ� �Է¹ް�,
			int t = sc.nextInt();
			// �״������� �л� 1000��
			for (int i =0; i<1000;i++) {
				score =sc.nextInt();
				scores[score]++;
			}
			
			for(int i=0; i<scores.length; i++) {
				if(max <= scores[i]) {
					max = scores[i];
					index = i;
				}
			}
			sc.close();
			System.out.println("#"+test_case + " "+index);
		}
	}
}
