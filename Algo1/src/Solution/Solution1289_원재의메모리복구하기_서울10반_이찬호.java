package Solution;
import java.util.Scanner;
import java.io.FileInputStream;
class Solution1289_원재의메모리복구하기_서울10반_이찬호{
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1289.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			/////////////////////////////////////////
			/*
				 이 부분에 여러분의 알고리즘 구현이 들어갑니다.
			 */
			/////////////////////////////////////////
			int answer = 0;
			String array_num [] = sc.next().split("");
			String firstNum, secondNum;
			
			boolean isfirstOne = true;
			//그 자릿수 동안
			for(int i =0;i<array_num.length-1;i++) {
				firstNum = array_num[i];
				if(i==0) {
					if(firstNum.equals("1")) isfirstOne = true;
					else isfirstOne = false;
				}
				secondNum = array_num[i+1];
				// 두 숫자 비교해서 같은 숫자면 pass, 다른숫자면 answer 증가
				if(firstNum.equals(secondNum)) {
					continue;
				}
				else {
					answer++;
				}
			}
			
			System.out.println("#"+test_case + " "+answer);
		}
	}
}