package Solution;

import java.util.Scanner;
import java.io.FileInputStream;

class Solution5431_민석이의과제체크하기_서울10반_이찬호{
	
	/*
	public static int[] countSort(int[] array) {
		int data[] = new int[array.length];
		int max = 0;
		for(int i =0; i<array.length; i++) {
			if(max < array[i]) max = array[i];
		}
		//int max = Arrays.stream(data).max().getAsInt();
		int c[]=new int[max+1];
		int temp[] = new int[array.length];
		for(int i=0; i<array.length; i++) c[array[i]]++;
		for(int i=1; i<c.length;    i++) c[i] +=c[i-1];
		for(int i=0; i<array.length; i++) {
			c[data[i]]--;
			temp[c[data[i]]] = array[i];
		}
		return temp;
	}
	*/
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input5431.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			StringBuilder str = new StringBuilder();
			// 이 부분에 여러분의 알고리즘 구현이 들어갑니다.
			//학생 총 수
			int stuNum = sc.nextInt();
			int students[] = new int[stuNum];
			int doneNum = sc.nextInt();
			
			for(int i =0; i<doneNum ; i++) {
				//과제 다 한 사람
				int stu = sc.nextInt();
				students[stu-1] = 1;
			}
			
			for(int i =0; i<students.length ; i++) {
				//과제 다 한 사람
				if(students[i] == 0) str.append((i+1)+" "); 
			}
			
			
			System.out.println("#"+test_case + " "+str);
		}
		sc.close();
	}
}