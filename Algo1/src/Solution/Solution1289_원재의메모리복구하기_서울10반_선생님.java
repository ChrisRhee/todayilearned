package Solution;
import java.util.Scanner;
import java.io.FileInputStream;
class Solution1289_원재의메모리복구하기_서울10반_선생님{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1289.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			String line = sc.next();
			
			int cnt = 0;
			if(line.charAt(0)=='1') cnt=1;
			for(int i=1;i<line.length();i++) {
				if(line.charAt(i-1)!=line.charAt(i)) cnt++;
			}
			
			System.out.println("#"+test_case + " "+cnt);
		}
	}
}