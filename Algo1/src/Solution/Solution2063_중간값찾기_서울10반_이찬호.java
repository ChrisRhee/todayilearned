package Solution;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;

class Solution2063_중간값찾기_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input2063.txt"));
		Scanner sc = new Scanner(System.in);
		
		int numScore = sc.nextInt();
		int temp;
		
		int ia[]= new int[numScore];
		for(int i =0; i<numScore; i++) {
			ia[i] =  sc.nextInt();
		}
		
		//Arrays.sort(ia);
		
		//Bubble Sort
		/*
		for(int i=ia.length-1; i>=0; i--) {
			for(int j=0; j<i; j++) {
				int temp;
				if(ia[j] > ia[j+1]) {
					temp = ia[j];
					ia[j] = ia[j+1];
					ia[j+1] = temp;
				}
			}
		}
		*/
		
		//Bubble Srot Refactoring
		for(int i=ia.length-1; i>=(ia.length/2)-1; i--) {
		    for(int j=0; j<i; j++) {
		        if(ia[j] > ia[j+1]) {
		            temp = ia[j];
		            ia[j] = ia[j+1];
		            ia[j+1] = temp;
		        }
		    }
		}
		
		System.out.println(ia[numScore/2]);
		
	}
}