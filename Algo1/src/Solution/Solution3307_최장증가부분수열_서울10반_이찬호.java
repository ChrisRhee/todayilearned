package Solution;
import java.util.Scanner;
import java.io.FileInputStream;
class Solution3307_최장증가부분수열_서울10반_이찬호{
	static int Answer;
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3307.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			// 수열 길이
			int N = sc.nextInt();
			int nums[] = new int[N];
			int indexI=1;
			int indexJ=1;
			int count = 0;
			int maxCount =0;
			int firstNum = 0;
			int secondNum = 0;
			int moveTo=0;
			// 모든 번호들 배열에 넣는다.
			for(int i =0; i<nums.length ; i++) {
				nums[i] = sc.nextInt();
			}
			
			total:for(int i=0; i<nums.length;i++) {
				first:for(int j =i; j<nums.length;j++) {
					if(moveTo > j) continue;
					System.out.println("i:"+i+" j:" +j+ " "+ firstNum+ " "+secondNum);
					second:for(int k = j+indexJ; k<nums.length;k++) {
						firstNum = nums[j];
						secondNum = nums[k];
						if(firstNum < secondNum) {
							moveTo = k;
							count++;
							break first;
						}
					}
					
					System.out.println();
				}
				moveTo =0;
				/*
				//nextNum이 배열 최대길이랑 작거나 같을때까지만
				indexI= indexI+i;
				if(indexI >= nums.length) break;
				
				//초기화를 하고
				firstNum = nums[i];
				
				for(int j=indexI;j<nums.length-1;j++) {
					// 앞 수가 뒷수보다 작으면 고고
					indexJ = indexI+j;
					secondNum = nums[indexJ];
					if(firstNum < secondNum) {
						firstNum = secondNum;
						//다음 index 넘기고
						count++;
					}
				}
				*/
				
				if(maxCount < count) maxCount = count;
				indexI = 1;
				count = 0;
			}
			
			Answer = maxCount +1;
			
			System.out.println("#"+test_case + " "+Answer);
		}
	}
}