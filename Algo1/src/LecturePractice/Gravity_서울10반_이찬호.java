package LecturePractice;
import java.util.Scanner;
import java.io.FileInputStream;
class Gravity_서울10반_이찬호{
		public static void main(String args[]) throws Exception	{
			
		System.setIn(new FileInputStream("res/Gravity_input.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			// 높이
			int height = 10;
			// 가로
			int wide = sc.nextInt();
			// 높이 * 가로 크기만큼 배열 만들기
			// 어차피 오른쪽으로 눞힐거니까 눞인 맵을 만듦
			int map[][] = new int[wide][height];
			int max = 0;
			int count = 0;
			// 초기화 다 해놓고
			for(int i =0; i<wide ; i++) {
				int block = sc.nextInt();
				for(int j =0; j<block; j++) {
					map[i][j]=1;
				}
			}
			
			for(int i =0; i<wide-1; i++) {
				for(int j =0; j<height; j++) {
					// 칸이 블럭 칸이면
					if(map[i][j]==1) {
						for(int k=i;k<wide;k++) {
							if(map[k][j]==1) continue;
							count ++;
						}
					}
					if(max<count) max = count;
					count = 0;
				}
			}
			
			System.out.println("#"+test_case + " "+max);
		}
		sc.close();
	}
}