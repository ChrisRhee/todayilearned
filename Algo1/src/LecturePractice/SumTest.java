package LecturePractice;


public class SumTest {
	
	// sum
	public static int sum(int n) {
		int sum =0;
		for(int i=1; i<=n ;i++) sum += i;
		return sum;
	}
	
	public static int sumR(int n) {
		if (n == 0) return 0;
		return n+sumR(n-1);
	}
	
	
	// factorial	
	public static int factorial(int n) {
		int sum =1;
		for(int i=1; i<=n ;i++) {
			sum *= i;
		}
		return sum;
	}
	

	public static int factorialRecursive(int n) {
		if (n <= 1) return 1;
		return n*factorialRecursive(n-1);
	}
	
	
	// Fibonacci numbers
	public static int fibo(int n) {
		if (n <=1 ) return n;
		
		int first =0;
		int second =1;
		int temp = 0;
		for(int i=2; i<=n ;i++) {
			temp = first + second;
			first = second;
			second = temp;
		}
		return second;
	}
	
	public static int fiboRecursive(int n) {
		if (n <=1 ) return n;
		return fiboRecursive(n-1) + fiboRecursive(n-2);
	}
	
	public static int gcd(int a, int b) {
		if(a<b) {
			int t=a; a=b; b=t;
		}
		while(b!=0) {
			int n = a%b;
			a=b;
			b=n;
		}
		return a; 
	}
	
	public static int gcdRecursive(int a, int b) {
		if(b==0) return a;
		return gcdRecursive(b,a%b);
	}
	
	
	public static void main(String[] args) {
		System.out.println(sum(10));
		System.out.println(sumR(1));
		System.out.println();
		System.out.println(factorial(4));
		System.out.println(factorialRecursive(5));
		System.out.println();
		System.out.println(fibo(5));
		System.out.println(fiboRecursive(4));
		System.out.println();
		System.out.println(gcd(3*11*5,2*11*5));
		System.out.println(gcdRecursive(3*11*5,2*11*5));
	}
}
