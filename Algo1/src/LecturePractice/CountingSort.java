package LecturePractice;

import java.util.Arrays;

public class CountingSort {
	public static void main(String[] args) {
		int a[] = new int[] {0,4,1,3,1,2,4,1};
		int temp[]= new int[a.length];
		int max = 0;
		
		for(int i =0; i<a.length; i++) {
			if(max < a[i]) max = a[i];
		}
		
		int counting[] = new int[max+1];
		
		for(int i =0; i<a.length; i++) {
			counting[a[i]]++;
		}
		System.out.println(Arrays.toString(counting));
		System.out.println(Arrays.toString(a));
		
		for(int i =1; i<counting.length; i++) {
			counting[i] = counting[i-1]+counting[i]; 
		}
		
		for(int i = 0; i<a.length-1 ; i++) {
			counting[a[i]]--;
			temp[counting[a[i]]] = a[i];
		}
		
		System.out.println(Arrays.toString(temp));
	}
}
