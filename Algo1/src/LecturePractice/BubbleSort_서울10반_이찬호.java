package LecturePractice;

import java.util.Arrays;

public class BubbleSort_서울10반_이찬호 {
	public static void main(String[] args) {
		int data[] = {55,7,78,12,42};
		System.out.println(Arrays.toString(data));
		
//		for(int i= 0;i<data.length-1;i++) {
//			for(int j=0;j<data.length-1-i;j++) {
//				int temp;
//				if(data[j] > data[j+1]) {
//					temp = data[j];
//					data[j] = data[j+1];
//					data[j+1] = temp;
//				}
//			}
//		}
		
		for(int i=data.length-1; i>=0; i--) {
			for(int j=0; j<i; j++) {
				int temp;
				if(data[j] > data[j+1]) {
					temp = data[j];
					data[j] = data[j+1];
					data[j+1] = temp;
				}
			}
		}
		System.out.println(Arrays.toString(data));
	}
}
