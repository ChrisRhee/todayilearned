package main;

import java.util.Scanner;

class Main1169_주사위던지기1_서울10반_이찬호{
	public static int r, n, data[];
	
	// n개중에 r개 뽑음, 중복 포함
	public static void permutation1(int count) {
		if(count ==n) {
			for( int i = 0; i < data.length; i++) 
				System.out.print(data[i]+ " ");
			System.out.println();
			return;
		}
		for(int i=1; i<=6; i++) {
			data[count] = i;
			permutation1(count+1);
		}
	}
	
	// 4개의 수에서 중복되는 배열 빼고 
	public static void permutation2(int start, int count) {
		if(count ==n) {
			for( int i = 0; i < data.length; i++) 
				System.out.print(data[i]+ " ");
			System.out.println();
			return;
		}
		for(int i=start; i<=6; i++) {
			data[count] = i;
			permutation2(i,count+1);
		}
	}
	

	// 같은 값 하나도 없음
	public static void permutation3(int count) {
		//카운트가 던지는 횟수랑 같아지면
		if(count ==n) {
			for( int i = 0; i < data.length; i++) 
				System.out.print(data[i]+ " ");
			System.out.println();
			return;
		}
		loop:for(int i=1; i<=6; i++) {
			//data 0~ 3번째를 돌면서 다 확인함
			for(int j=0; j<count ; j++) {
				// 0번째 자리숫자가 지금 넣으려는 수랑 같으면 패스
				if( data[j] == i) continue loop;
				// 다르면 넣자.
			}
			data[count] = i;
			permutation3(count+1);
		}
	}
		
	public static void main(String args[]) throws Exception	{
		Scanner sc = new Scanner(System.in);
		n = sc.nextInt();           // 주사위를 N번 던진다.
		int caseNum = sc.nextInt(); // 어떤 방식으로 출력할 것인가
		data = new int[n];
		//어떤 방식으로 풀지 선택함.
		switch(caseNum) {
			//  주사위를 N번 던져서 나올 수 있는 모든 경우
			case 1:
				permutation1(0);
				break;
			case 2:
				permutation2(1,0);
				break;
			case 3:
				permutation3(0);
				break;
		}
		sc.close();
	}
}