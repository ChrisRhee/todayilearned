***
contents : Java, Architecture, Map, Comparable

date : 2019 01 18 Fri
***

# 박상진 교수님
정올, 백준
- 파일명 : Main문제번호_문제이름_서울10반_이찬호.java

sw expert
- 파일명 : Solution문제번호_문제이름_서울10반_이찬호.java

***
# 수업
## Constructer
- this()는 생성자안에서 제일 처음 와야한다.
```java
public Member(String name, int age){
    this(name, age,"");
    set(getafsdf)...
    ...
}
```
- 이런식으로 생성자의 첫줄에 와야한다.

## Java.lang API (80/111)
- toString
    - 투스트링이 오버라이드가 안되어있으면 이상하게 출력된다.
    - 그래서 필요하면 오버라딩 해줘야함 

## abstract (92/111)
- 이건 메소드가 구련되지 않은게 있으면 객체를 만들 수 없다. 그걸 구현을 해야 객체를 만들 수 있음.
- 인터페이스를 받아서 앱스트랙트 클래스로 설계를 하고, 이걸 상속 받아서 수퍼 클래스를 만든다.
- 인터페이스의 메소드를 임플 받아서 코딩할때는 무조건 public으로 해야한다. 

## Collection API(96/111)
- 클래스가 interface를 상속할떄만 implements를 쓰고, 나머지는 다 extends를 쓴다. 
 
## MAP
- 폴리몰피즘을 배운 것은 Map으로 만들고 내가 원하는 것 ( HashMap, Hashtable, TreeMap) 으로 사용하다가. 상황이 바뀌면 (멀티쓰레드에서 불안정해도 빨랐으면 좋겠다., 무조건 안정적이면 좋겠다.) 사용하는 것만 바꿔서 사용 하면된다. 안쪽에 있는 기본적인 것들은 다 공유가 되므로 내가 사용하고자 하는 용도에 맞게 활용하고 바꾸면 된다. 이것이 갓리몰피즘


## Comparable
- CompareTo


- Comparator
    - Compare
    - 앞에서 뒤에걸 비교한다.
- 오브젝트 = Comparable interface를 받아서 한다. 
- 기본 오브젝트 단위로 하고, 거기에 내가 원하는 것을 수정하고 싶으면
```java
public class AgeComparator implements Comparator<Member>{
```
- Comparator를 임플 해서 compare함수를 수정한다.

```java
public int compare(Member m1, Member m2) {
		
		return m1.getAge()-m2.getAge();
	}
```

```java
public int compare(Member m1, Member m2) {
		
		return m2.getAge()-m1.getAge();
	}
```
- 역순으로 하고 싶으면 return 에서 앞뒤를 바꿔준다.
- 이게 기준이 되서 정렬이 된다.
- 알고리즘에서 object sort를 할때도 이런식으로 기준을 주고 정려랗ㄴ다.


***
# 수업 외 꿀팁

***
# 공지사항
## 다음주 월요일 시험
- 25문제
    - 아주 까다로운거 1개
    - 어려운거 5개
    - 중간 10개
    - 쉬운거 7개
- 온라인 화면으로 시험을 보고 자동채점 함
- 4지선다 객관식
- 단답형일때는 대소문자 지킬 것
- 월요일 첫시간에 시험봄
- 오픈북 x 
- 부정행위 끝장
- Java 전 범위
    - 다형성
    - 오브젝트 클래스
    - 컨스트럭터
    - 오버로딩
    - 스태틱
    - 인플리먼트
    - 셋
    - 상속
    - 소프트웨어 설계
        - UML?
    - 싱글튼 패턴
    - this
        - 생성자의 제일 첫번째에 와야한다.
    - instanceof
    - foreach
    - 변수 초기화
    - interface
    - abstract 
    - 
