package SWExpertAcademy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
 
public class Solution1216_회문2_서울10반_이찬호2{
    static char pan[][];
    static int maxLen;
 
    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
 
        // loop Test-Case
        for (int T = 1; T <= 10; T++) {
            // init
            int t = Integer.parseInt(br.readLine());
            maxLen = 0;
            pan = new char[100][100];
            for (int i = 0; i < 100; i++) {
                String line = br.readLine();
                for (int j = 0; j < 100; j++) {
                    pan[i][j] = line.charAt(j);
                }
            }
 
            // find horizontal
            for (int i = 0; i < 100; i++) {
                for (int j = 0; j < 100; j++) {
                    // find odd palin
                    int depth = 1;
                    while (j + depth < 100 && j - depth > 0 && pan[i][j + depth] == pan[i][j - depth]) {
                        depth++;
                    }
                    if (depth != 1 && maxLen < depth * 2 - 1)
                        maxLen = depth * 2 - 1;
 
                    // find even palin
                    depth = 0;
                    while (j + depth + 1 < 100 && j - depth > 0 && pan[i][j + depth + 1] == pan[i][j - depth]) {
                        depth++;
                    }
                    if (depth != 0 && maxLen < depth * 2)
                        maxLen = depth * 2;
                }
            }
            // find vertical
            for (int j = 0; j < 100; j++) {
                for (int i = 0; i < 100; i++) {
                    // find odd palin
                    int depth = 1;
                    while (i + depth < 100 && i - depth > 0 && pan[i + depth][j] == pan[i - depth][j]) {
                        depth++;
                    }
                    if (depth != 1 && maxLen < depth * 2 - 1)
                        maxLen = depth * 2 - 1;
                    // find even palin
                    depth = 0;
                    while (i + depth + 1 < 100 && i - depth > 0 && pan[i + depth + 1][j] == pan[i - depth][j]) {
                        depth++;
                    }
                    if (depth != 0 && maxLen < depth * 2)
                        maxLen = depth * 2;
                }
            }
            System.out.println("#" + T + " " + maxLen);
        }
 
    }
 
}