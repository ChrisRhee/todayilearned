package SWExpertAcademy;
import java.io.FileInputStream;
import java.util.Scanner;
class Solution1215_회문1_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1215.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int test_case = 1; test_case <= 10; test_case++)
		{
			int T=sc.nextInt();
			int Answer =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			String pal[][]= new String[8][8];
			//집어 넣기
			for(int i =0; i<8; i++) {
				String[] line = sc.next().split("");
				for(int j =0; j<8; j++) {
					pal[i][j] = line[j];
				}
			}
			
			for(int i =0; i<8; i++) {
				for(int j =0; j<8-T+1; j++) {
					StringBuilder a = new StringBuilder();
					StringBuilder b = new StringBuilder();
					for(int k=j; k<j+T; k++) {
						a.append(pal[i][k]);
						b.append(pal[k][i]);
					}
					if(a.toString().equals(a.reverse().toString()))	Answer ++;
					if(b.toString().equals(b.reverse().toString()))	Answer ++;
				}
			}
			System.out.println("#"+test_case + " "+Answer);
		}
		sc.close();
	}
}