package SWExpertAcademy;
import java.io.FileInputStream;
import java.util.Scanner;
class Solution1216_회문2_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1216.txt"));
		Scanner sc = new Scanner(System.in);
		for(int test_case = 1; test_case <= 10; test_case++)
		{
			int T=sc.nextInt();
			int Answer =0;
			int lan = 100;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			String pal[][]= new String[lan][lan];
			//집어 넣기
			for(int i =0; i<lan; i++) {
				String[] line = sc.next().split("");
				for(int j =0; j<lan; j++) {
					pal[i][j] = line[j];
				}
			}
			
			
			for(int d=100; d>=1; d--) {
			//for(int d=1; d<=100; d++) {
				if(Answer > d) continue;
				for(int i =0; i<100-d+1; i++) {
					for(int j =0; j<100-d+1; j++) {
						StringBuilder a = new StringBuilder();
						StringBuilder b = new StringBuilder();
						for(int k=j; k<j+d; k++) {
							a.append(pal[i][k]);
							b.append(pal[k][i]);
						}
						if(a.toString().equals(a.reverse().toString())) {
							if(a.length() > Answer)Answer = a.length();
						}
						if(b.toString().equals(b.reverse().toString())) {
							if(b.length() > Answer) Answer = b.length();
						}
					}
				}
			}
			
			System.out.println("#"+test_case + " "+Answer);
		}
		sc.close();
	}
}