package com.ssafy.collect;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class ListTest {
	public static void main(String[] args) {
		List<String> list = new LinkedList<>();
		list.add("홍길동");
		list.add("홍길동");
		list.add("사오동");
		list.add("고길동");
		list.add("저팔동");
		list.add("가츠동");
		list.add("홍길동");
		list.add("사오동");
		list.add("고길동");
		list.add("저팔동");
		System.out.println(list);
		
		Collections.sort(list);
		
		System.out.println(Collections.binarySearch(list, "고길동"));
		System.out.println("-- sysout");
		for (String string : list) System.out.println(string);
		
		System.out.println("-- iterator");
		Iterator<String> it = list.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
		list.remove("사오동");
		list.remove(0);
		
		System.out.println("-- list.get()");
		for(int i=0; i<list.size(); i++) {
			System.out.println(list.get(i));
		}
		
		// 다 삭제 고고
		System.out.println("-- 저팔동 삭제");
		for(int i=0; i<list.size(); i++) {
			if(list.get(i).equals("저팔동")) {
				list.remove(i);
				i--;
			}
		}
		
		list.set(1, "이찬호");
		System.out.println(list);
		
		int index = list.indexOf("이찬호");
		System.out.println("index="+index);
		list.set(1, "뽀짝");
		System.out.println(list);
	}

}
