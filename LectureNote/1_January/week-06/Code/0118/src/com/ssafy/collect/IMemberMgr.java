package com.ssafy.collect;

import java.util.List;

public interface IMemberMgr {

	void add(Member m);

	List<Member> search();

	Member search(String name);

	void update(Member m);

	void delete(String name);

}