package com.ssafy.collect;

import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MapTest {
	public static void main(String[] args) {
		Map<String,Integer> map = new TreeMap<>();
		map.put("홍길동", 11);
		map.put("손오공", 22);
		map.put("사오정", 33);
		map.put("홍길동", 44);
		System.out.println(map);
		System.out.println(map.get("사오정"));
		System.out.println(map.size());
		Set<String> keys =map.keySet();
		// key로 value는 찾을 수 있다.
		for(String a: keys) System.out.println(a+" "+map.get(a));
		
		// values에서 key는 찾을 수가 없다.
		Collection<Integer> values = map.values();
		for(Integer v:values) System.out.println(v);
		map.remove("손오공");
		System.out.println(map);
	}
}
