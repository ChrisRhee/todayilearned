package com.ssafy.collect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MemberMgrMap implements IMemberMgr{
	private Map<String,Member> map = new HashMap<>();
	private static MemberMgrMap instace = new MemberMgrMap();
	private MemberMgrMap() {}
	
	public static MemberMgrMap getInstace() {
		return instace;
	}

	@Override
	public void add(Member m) {
		map.put(m.getName(), m);
		
	}

	@Override
	public List<Member> search() {
		List<Member> list = new ArrayList<Member>();
		for (Member member : map.values()) {
			list.add(member);
		}
		return list;
	}

	@Override
	public Member search(String name) {
		return map.get(name);
	}

	@Override
	public void update(Member m) {
		map.put(m.getName(), m);
	}

	@Override
	public void delete(String name) {
		map.remove(name);
	}

}
