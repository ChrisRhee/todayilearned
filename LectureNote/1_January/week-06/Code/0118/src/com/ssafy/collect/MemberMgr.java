package com.ssafy.collect;

import java.util.ArrayList;
import java.util.List;

public class MemberMgr implements IMemberMgr {
	private List<Member> list= new ArrayList<Member>(); 
	
	// Singleton
	private static IMemberMgr instance = null;
	
	private MemberMgr() {
		
	}
	public static IMemberMgr getInstance() {
		return instance;
	}
	
	@Override
	public void add(Member m) {
		list.add(m);
	}
	
	@Override
	public List<Member> search(){
		return list;
	}
	
	@Override
	public Member search(String name){
		for(Member m : list) {
			if(m.getName().equals(name))
				return m;
		}
		return null;
	}
	
	@Override
	public void update(Member m) {
		Member old = search(m.getName());
		if(old != null) {
			int index = list.indexOf(old);
			list.set(index, m);
		}
	}
	
	@Override
	public void delete(String name) {
		Member old = search(name);
		if(old != null)
			list.remove(old);
	}
	
	
}

