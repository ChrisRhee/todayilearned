package com.ssafy.collect;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;


public class SetTest {
	public static void main(String[] args) {
		Set<String> set = new HashSet<>();
		set.add("헤헤");
		set.add("그림자");
		set.add("헤헤");
		set.add("사오정");
		set.add("헤헤");
		set.add("손오공");
		set.add("헤헤");
		System.out.println(set);
		
		// 트리셋은 알아서 정렬해줌.
		set= new TreeSet<>(set);
		System.out.println(set);
		
		// 요즘버전
		for(String s:set)System.out.println(s);
		
		// 옛날버전
		Iterator<String> it = set.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
		// 요소 삭제
		set.remove("손오공");
		System.out.println(set);
	}
}
