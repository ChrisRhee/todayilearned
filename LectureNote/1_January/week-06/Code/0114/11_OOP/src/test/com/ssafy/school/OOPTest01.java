package test.com.ssafy.school;

import com.ssafy.school.Person;
import com.ssafy.school.Student;

public class OOPTest01 {
	public static void main(String[] args) {
		Person p1=new Person("chrisrhee");
		System.out.println(p1.toString());
		p1.setPhoneNumber("010-1111-2222");
		System.out.println(p1);
		
		Student s1= new Student("hello","s001");
		System.out.println(s1.toString());
		s1.setPhoneNumber("010-3333-4444");
		System.out.println(s1);
		
		p1 = s1;
		System.out.println(p1);
	}
}
