package test.com.ssafy.school;

import com.ssafy.school.Person;
import com.ssafy.school.Student;

public class MemoryTest {
	public static void main(String[] args) {
		int a1 = 'A';
		int a2 = 65;
		
		String s1 = "ABC";
		String s2 = new String("ABC");
		
		Person p1 = new Person("ABC");
		Person p2 = new Person("ABC");
		
		Student p3 = new Student("s001","ABC");
		Student p4 = new Student("s001","ABC");
		
		Person p5 = new Student("s001","ABC");
		Person p6 = p5;
		
		
		System.out.println(a1 == a2);
		System.out.println(s1 == s2);
		System.out.println(p1 == p2);
		System.out.println(p5 == p6);
		
		// 주소를 비교하기 위해서는 equals()?
		System.out.println(s1.equals(s2));
		System.out.println(p1.equals(p2));
		
	}
}
