package test.com.ssafy.school;

import com.ssafy.school.Person;
import com.ssafy.school.SchoolMgr;
import com.ssafy.school.Student;

public class MgrTest {
	public static void main(String[] args) {
		
		SchoolMgr s = SchoolMgr.getInstace();
		s.add(new Person("ChrisRhee"));
		s.add(new Student("eden","s001"));
		s.add(new Student("hazar","s002"));
		s.add(new Person("Chris1"));
		s.add(new Person("Chris2"));
		s.add(new Student("jane","s003"));
		s.add(new Student("michael","s004"));
		System.out.println();
		System.out.println(s.getStudents());
		System.out.println(s.getNames());
		System.out.println(s.getSearch("s003"));
	}
}
