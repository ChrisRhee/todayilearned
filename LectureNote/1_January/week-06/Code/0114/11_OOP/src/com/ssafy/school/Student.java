package com.ssafy.school;

public class Student extends Person{
	private String studentNumber;

	public Student(String name, String phoneNumber, String studentNumber) {
		// 얘는 부모한테 가는거.
		super(name, phoneNumber);
		setStudentNumber(studentNumber);
	}
	
	public Student( String name, String studentNumber) {
		this(name, "", studentNumber);
	}

	public String getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}

	@Override
	public String toString() {
		// 학생 : 학번, 이름, 전화번호
		// 이름,전화번호는 person = super.toString()에 있다.
		
		return "StuNum : " + studentNumber+", "+super.toString();
	}
	
	

}
