package com.ssafy.school;

import java.util.Arrays;

public class SchoolMgr {
	
	//사람 또는 학생
	private Person pList[]= new Person[10];
	private int index;
	
	private static SchoolMgr instace = new SchoolMgr();
	private SchoolMgr() {}
	public static SchoolMgr getInstace() {
		return instace;
	}
	
	//사람 또는 학생의 객체를 추가 할 수 있다.
	public void add (Person person) {
		pList[index++] = person;
	}
	
	//등록된 정보중에서 학생의 정보만 출력하세요.
	public String getStudents() {
		StringBuilder str = new StringBuilder();
		for (Person person : pList) {
			if(person != null && person instanceof Student) {
				str.append(person.toString()).append("\n");
			}
		}
		return str.toString();
	}
	
	//모든사람의 이름을 출력하세요.
	public String getNames() {
		StringBuilder str = new StringBuilder();
		for (Person person : pList) {
			if(person != null) {
				str.append(person.getName()).append("\n");
			}
		}
		return str.toString();
	}
	
	/** 학번 입력, 학생의 모든 정보 출력
	 * 1. 출력은 출력인데 "학생"에 대한 정보이다. 
	 * 2. 같은 사람 어차피 없음.
	 * 3. 프로그램에서 print 하지마라. toString으로 하셈.
	 * 4. 근데 내가 원하는 게 정해져있다면 그객체를 내주는게 좋다.
	 * */
	public Student getSearch(String studentNumber) {
		Student s = null;
		for (Person person : pList) {
			if(person!=null && person instanceof Student) {
				Student tmp = (Student)person;
				if(tmp.getStudentNumber().equals(studentNumber))
					s = tmp;
			}
		}
		return s;
	}
	
	
	
	
	public String toString() {
		return Arrays.toString(pList);
	}
}
