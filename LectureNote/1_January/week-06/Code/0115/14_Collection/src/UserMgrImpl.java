import java.util.Collection;
import java.util.HashSet;

public class UserMgrImpl implements UserMgr {
	private Collection<User> users;
	
	public UserMgrImpl() {
		this(new HashSet());
	}
	
	public UserMgrImpl(Collection<User> users) {
		this.users = users;
	}
	
	@Override
	public boolean add(User u) {
		return users.add(u);
	}

	@Override
	public User search(String name) {
		for (User user : users) {
			if(user.getName().equals(name)) return user;
		}
		return null;
	}

	@Override
	public Collection<User> search(int age) {
		Collection<User> temp = new HashSet(); // 검색결과는 중복 데이터를 허용 안함
		for (User user : users) {
			if(user.getAge() == age) {
				temp.add(user);
			}
		}
		return temp;
	}

	@Override
	public boolean update(String name, int updateAge) {
		User tmp = search(name);
		if(tmp ==null) return false;
		
		tmp.setAge(updateAge);
		return true;
	}

	@Override
	public String toString() {
		return users.toString();
	}

	
	
}
