import java.util.Collection;

public interface UserMgr {
	// 고객 정보를 추가할 수 있다
	boolean add(User u);
	
	// 특정 고객(이름)의 정보를 확인 할 수 있다.
	User search(String name);
	
	// 나이가 같은 고객의 정보를 확인
	// 몇명이나 나올지 알 수가 없어진다. 그래서 배열(갯수가 정해짐)을 사용해서 할 수 없다.
	// 컬렉션 안에는 무엇이 들어가있는지 알 수 없다. 그래서 그중에 User만 담아야 하는것을 이렇게 표현한다.
	// <> 이걸 generic 타입이라고 한다. 매번 타입 채크를 안해줘도 된다.
	Collection<User> search(int age);
	
	// 특정 고객의 나이를 수정 할 수 있다.
	boolean update(String name, int updateAge);
	
	
}
