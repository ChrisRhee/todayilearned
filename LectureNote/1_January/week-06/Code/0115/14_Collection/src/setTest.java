import java.util.*;

/** 어떤 객체를 중복저장하지 않는다. */

public class setTest {
	public static void main(String[] args) {
		Collection c;
		c= new HashSet();
		System.out.println(c.add("hello"));
		System.out.println(c.add(new String("hello")));
		System.out.println(c.add(1234)); //5.0 int 는 알아서 Integer로 연결시킨다. 
		//int는 객체가 아니지만 Integer는 객체이다. 그래서 원래 정식 코드는 이렇게 작성된다.
		System.out.println(c.add(new Integer(1234)));
		System.out.println(c.add(new User("이찬호", 20)));
		System.out.println(c.add("hello"));
		System.out.println(c.add(new String("hello")));
		System.out.println(c.add(1234));
		System.out.println(c.add(new Integer(1234)));
		System.out.println(c.add(new User("이찬호", 20)));
		System.out.println(c);
		
	}
}
