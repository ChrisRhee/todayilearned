import java.util.ArrayList;

public class UserTest {
	public static void main(String[] args) {
		UserMgr m = new UserMgrImpl();
		m.add(new User("이찬호", 50));
		m.add(new User("개복치", 100));
		m.add(new User("이순신", 50));
		m.add(new User("홍길동", 50));
		System.out.println(m);
		System.out.println(m.search("이찬호"));
		System.out.println(m.search("홍길"));
		System.out.println(m.search(50));
		m.update("이찬호", 120);
		System.out.println(m.search(50));
	}
}
