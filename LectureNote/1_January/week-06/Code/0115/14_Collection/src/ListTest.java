import java.util.*;

/** 어떤 객체를 순서대로 입력하고 싶다. */
// 여기서는 "순서" 만 이야기 해줬다.
public class ListTest {
	public static void main(String[] args) {
		Collection c;
		c= new ArrayList();
		System.out.println(c.add("hello"));
		System.out.println(c.add(new String("Hello")));
		System.out.println(c.add(1234)); //5.0 int 는 알아서 Integer로 연결시킨다. 
		//int는 객체가 아니지만 Integer는 객체이다. 그래서 원래 정식 코드는 이렇게 작성된다.
		System.out.println(c.add(new Integer(1234)));
		System.out.println(c.add(new User("이찬호", 20)));
		System.out.println(c.add("hello"));
		System.out.println(c.add(new String("Hello")));
		System.out.println(c.add(1234));
		System.out.println(c.add(new Integer(1234)));
		System.out.println(c.add(new User("이찬호", 20)));
		System.out.println(c);
		
	}
}
