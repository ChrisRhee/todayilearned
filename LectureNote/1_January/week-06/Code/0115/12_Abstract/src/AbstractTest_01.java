/** 추상클래스 - 상속을 유도하는 방법 1.4 생성자는 있지만 new는 못한다.
 * 1. 일부 메서드를 재정의 유도 - 구현부를 지운다. 
 * 
 * */
abstract class AbstractSuper01{ // 1.3 한개라도 추상 메서드면 클래스는 추상클래스가 되어야한다.
	public void methodA() {}
	// 1.2 구현부가 없는 메서드는 abstract 선언해준다.
	public abstract void methodB() ;//1.1{} 구현부를 지운다.
}

class Sub01 extends AbstractSuper01{//1.5 그래서 상속을 유도당했으니 상속해준다

	@Override
	// 1.6 추상메서드는 자식클래스가 필수로 재정의 해줘야한다.
	public void methodB() {
		// TODO Auto-generated method stub
		System.out.println("1.6 추상메서드는 자식클래스가 필수로 재정의 해줘야한다.");
	} 
	
}

public class AbstractTest_01 {
	public static void main(String[] args) {
		AbstractSuper01 as;
		//as = new AbstractSuper01(); // 그래서 이게 안되는중임
		//as = new AbstractSuper01(); // 그래서 이게 안되는중임
		as = new Sub01(); //1.7 자기로 부르면 안되고, 자기의 자식으로 불러야 한다.
		as.methodB(); // 결과는 잘 나온다.
	}
}
