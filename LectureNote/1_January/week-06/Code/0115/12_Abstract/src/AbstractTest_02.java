/** 2. 클래스만 추상 클래스 - N개의 메서드 중 필수 재정의 애매할 때 상속만 유도
 * ex) 클릭 더블클릭, UI event에 많이 나온다. 
 * 
 */

abstract class AbstractSuper02{
	public void methodA() {}
	public void methodB() {}
}

class Sub02 extends AbstractSuper02{
	public Sub02() {
		super(); //부모의 생성자 호출 // new를 못하니까 부모를 부른다.
	}
}

public class AbstractTest_02 {
	public static void main(String[] args) {
		AbstractSuper02 as;
		//as = new AbstractSuper02() ;
		as = new Sub02();
	}
}
