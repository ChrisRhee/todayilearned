package com.ssafy;

public class StudentTest {
	public static void main(String[] args) {
		StudentMgr mgr = new StudentMgr();
		// 학생 들어감
		mgr.add(new Student());
		// 로그인 했음
		mgr.login();
		// 내 정보 확인했음
		System.out.println(mgr.toString());
	}
}
