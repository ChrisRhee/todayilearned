/** 3. 모든 메서드가 추상 메서드
 * - interface 라는 문법으로 정의한다
 * - 모든 메서드가 추상이면 구현부때문에 발생하는 충돌이 일어나지 않는다
 * - 구현부 X, 생성자 X, 다중 상속 가능
 * - 구현부가 없으면 초기화 할게 없다. 그럼 생성자도 의미가 없어진다. 이럴땐 새로운 문법을 만드는게 좋다.
 * - 그래서 자바가 다중상속이 안되는데, 이런 특징을 이용해서 다중상속이 가능하다. 이걸 interface라고 한다.
 * - 근데 지금 abstract가 다 붙어서 너무 짜증난다. 이걸 다 붙었을때를 interface라는걸 만듦
 * - 재저으이 할때 modifier는 축소 할수 없다. 이걸 재정의 할때는 public으로 한다.
 * - 
 */

abstract class AbstractSuper03{
	public abstract void methodA();
	public abstract void methodB();
}

/** interface
 * public abstract 이거 넣는것도 귀찮아서 안넣어도 된다.
 * 특징
 * 1. 생성자가 존재하지 않는다.
 * 2. 데이터를 올리려면? static으로 한다.
 * */
interface SuperInterface {
	//멤버 데이터는 변수의 개념이 아니라 사용자 정의 상수 static final의 개념이다
	String NAME="홍길동"; //생성자 없이 메모리에 등록하는 방법은 static밖에 없다.
	
	void methodA();
	void methodB();
}

/** 구현되어있는 것은 extends , 
 * 	내가 받아서 구현해야하는것은 implements
 * */
class SubInter implements SuperInterface{
	@Override
	public void methodA() {}
	
	@Override
	public void methodB() {}
	
}
public class AbstractTest_03 {
	public static void main(String[] args) {
		SuperInterface si;
		//si= new SuperInterface(): //추상은new가 안되는거고, 얘는 생성자가 없어서 이게 안됨.
		si = new SubInter();
		System.out.println(SuperInterface.NAME);
	}
}
