package com.ssafy.school;

public interface LoginMgr {
	/** 학생 로그인 */
	boolean login(Student student);
	boolean login(String id, String pw);
}
