package com.ssafy.school;

public interface StudentInfoMgr {
	/** 학생 정보 확인 */
	Student getStudent();
	Student getStudent(Student student);
}
