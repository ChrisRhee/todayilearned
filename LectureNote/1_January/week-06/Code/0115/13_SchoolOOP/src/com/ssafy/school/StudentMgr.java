package com.ssafy.school;

public interface StudentMgr extends LoginMgr, StudentInfoMgr{
	
	/** 학생 정보 확인 */
	Student getStudent();
	Student getStudent(Student student);
}
