package com.ssafy.school;

public class Student {
	private String studentNumber;
	private String pw;
	
	public Student(String studentNumber, String pw) {
		this.studentNumber = studentNumber;
		this.pw = pw;
	}

	public String getStudentNumber() {
		return studentNumber;
	}

}
