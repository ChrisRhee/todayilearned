package com.ssafy.school;

public class MgrTest {
	public static void main(String[] args) {
		// 다형성할때 하지 말아야 하는 것
		// 자식으로 선언하고 자식으로 초기화하는것.
		StudentMgr m = new StudentMgrImpl();
		System.out.println(m.login("1111","1111"));
		System.out.println(m.login("jeon","1111"));
	}
}
