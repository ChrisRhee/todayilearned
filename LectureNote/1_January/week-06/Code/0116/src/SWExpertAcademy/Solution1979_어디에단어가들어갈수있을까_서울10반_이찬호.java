package SWExpertAcademy;
import java.io.FileInputStream;
import java.util.Scanner;
class Solution1979_어디에단어가들어갈수있을까_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1979.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			int Answer =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			// n x n 크기
			int n = sc.nextInt();
			// 길이
			int len = sc.nextInt();
			int data[][]= new int[n][n];
			for(int i=0; i<n; i++) {
				for(int j=0; j<n; j++) {
					data[i][j] = sc.nextInt();
				}
			}
			
			// 가로 1 1 0 1 1 이거 어떻게 거를거?
			for(int i=0; i<n; i++) {
				int countI = 0, countJ=0;
				for(int j=0; j<n; j++) {
					countI += data[i][j];
					countJ += data[j][i];
					if(data[i][j]==0) {
						if(countI == len) Answer++;
							countI=0;
					}
					if(data[j][i]==0) {
						if(countJ == len) Answer++;
						countJ=0;
					}
					
				}
				if(countI == len) Answer++;
				if(countJ == len) Answer++;
			}
			
			System.out.println("#"+test_case + " "+Answer);
		}
		sc.close();
	}
}