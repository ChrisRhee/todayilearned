package SWExpertAcademy;
import java.io.FileInputStream;
import java.util.Scanner;

class Solution5356_의석이의세로로말해요_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input5356.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			String Answer = "";
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			int max = 0;
			String data[] = new String[5];
			for(int i =0; i<5; i ++) {
				data[i] = sc.next();
				if(max < data[i].length()) max = data[i].length();
			}
			String total[] = new String[max*max];
			for(int i =0; i<5; i ++) {
				String[] temp;
				temp = data[i].split("");
				for(int j=0; j<temp.length; j++)
					total[i+j*max] = temp[j];
			}
			for(int i =0; i<total.length; i++) {
				if(total[i]!= null)
					Answer = Answer + total[i];
			}
			
			System.out.println("#"+test_case + " "+Answer);
		}
		sc.close();
	}
}