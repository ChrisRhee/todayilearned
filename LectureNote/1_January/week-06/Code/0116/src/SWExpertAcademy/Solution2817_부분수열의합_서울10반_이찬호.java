package SWExpertAcademy;
import java.util.Scanner;
import java.io.FileInputStream;
class Solution2817_부분수열의합_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input2817.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			
			int Answer =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			int index = sc.nextInt(); // 몇개의 수
			int sumTotal = sc.nextInt();
			int data[] = new int[index];
			for(int i =0; i<index; i ++) {
				data[i] = sc.nextInt();
			}
			
			int n = data.length;
			for(int i=1; i<(1<<n); i++) {
				int sum =0;
				for(int j=0; j<n; j++) {
					if((i&(1<<j))>0) {
						sum+= data[j];
					}
				}
				if(sum == sumTotal) Answer++;
			}
			
			System.out.println("#"+test_case + " "+Answer);
		}
		sc.close();
	}
}