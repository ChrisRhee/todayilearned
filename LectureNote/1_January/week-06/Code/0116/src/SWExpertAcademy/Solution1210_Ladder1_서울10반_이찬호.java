package SWExpertAcademy;
import java.util.Scanner;
import java.io.FileInputStream;
class Solution1210_Ladder1_서울10반_이찬호{
	public static int di [] = {-1,1,0,0};
	public static int dj [] = {0,0,-1,1};
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1210.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int test_case = 1; test_case <= 10; test_case++)
		{
			int T=sc.nextInt();
			int Answer =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			int N = 100;
			int data[][]= new int[N][N];
			int fi = 0;
			int fj = 0;
			boolean left = false;
			boolean right = false;
			boolean up = true;
			boolean isFound = false;
			
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					data[i][j] = sc.nextInt();
					if(data[i][j] == 2) {
						fi = i;
						fj = j;
					}
				}
			}
			//좌우만 검색
			while(!isFound) {
				
				while(up) {
					// 다 확인했는데 0 아니고 직진이면
					fi = fi - 1;
					// 도착했을 때
					if(fi-1 < 0) {
						Answer = fj;
						isFound = true;
						break;
					}
					
					// 왼쪽 확인
					if(fj-1 >= 0 && data[fi][fj-1] == 1) {
						left = true;
						up = false;
						break;
					}
					
					// 오른쪽 확인 오른쪽 끝이 아니고 오른쪽이 1이면
					if(fj+1 < N && data[fi][fj+1] == 1) {
						right = true;
						up = false;
						break;
					}
					
					
				}
				
				while(left) {
					// 그거아니면 계속 왼쪽으로
					fj = fj - 1;
					// 끝에 도달했거나 0에 도달하면
					if(fj -1 < 0 || data[fi][fj-1] == 0) {
						left = false;
						up = true;
						break;
					}
					
				}
				
				while(right) {
					// 계속 오른쪽으로
					fj = fj + 1;
					// 오른쪽 끝에 도달했거나 0에 도달하면
					if(fj + 1 >= N || data[fi][fj+1] == 0) {
						right = false;
						up = true;
						break;
					}
					
				}
			}
			
			
			System.out.println("#"+T + " "+Answer);
		}
		sc.close();
	}
}