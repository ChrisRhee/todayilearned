***
contents : Java, Algorithms, String

date : 2019 01 17 Thu
***

# 박상진 교수님
정올, 백준
- 파일명 : Main문제번호_문제이름_서울10반_이찬호.java

sw expert
- 파일명 : Solution문제번호_문제이름_서울10반_이찬호.java


***
# 수업


## Sort
- int 배열은
```java
Arrays.sort(ia);
```
이런식으로 구현 할 수 있다. 그럼 Tv 배열같은 경우는 어떻게 할까?
```java
Arrays.sort(ta);
```
이렇게 사용가능하지만, Tv의 어떤 것을 key로 정렬을 할 것인지 모르기 때문에, sort를 implements 해서 키 값을 받아서 그 키 값을 통해 정렬 하도록 오버라이딩 해주면 된다.
```java
Arrays.sort(ta,key);
```
그럼 Tv를 담은 배열도 정렬이 가능하다. 
- 메이플 아이템 창 같은 경우도 아이템이 들어오는데, 아이템의 어떤 것을 기준으로 정렬 할 것인지 key에 대해서 생각해보아야 한다. 
    - 장비아이템 : 부위별로 정렬을 하고, 그 부위들 직업군으로 정렬하고, 레벨 낮은 순으로 정렬하면 되겠다.


## 3. 문자열(String)
### 문자의 표현

```java
StringBuilder sb = new StringBuilder(s1);
// 데코레이터 디자인 패턴
// 메소드 Chaining 이라고 한다. 
sb.reverse().append(")").insert(0, "[").setLength(2);
String s = sb.toString();
System.out.println(s);
```
- 스트링 빌더를 사용해서 이런식으로 스트링을 조절하고 변경할 수 있다. 빌더 짱짱 맨


***
# 수업 외 꿀팁

***
# 공지사항
