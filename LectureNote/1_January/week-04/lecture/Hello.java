public class Hello {
	final int i = 10; //final을 넣으면 변하지 않는 상수를 만든다.
	// new로 객체를 생성하는데, static을 쓰면 생성 안하고 쓸수있따?

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int j = 20;
		j = 30;
		
		System.out.println("hihihihi");
		System.out.println("j = "+ j++);
		System.out.println("j = "+ j);
		
		boolean b = true;
		char c = 'A';
		String s = "A";
		String t = new String("A");
		int k = 123;
		long l = 123L;
		double pi = 3.14;
		float f = 3.14f;
		
		//float와 double은 소숫점 이하를 계산하는 방법이 다르다. 그래서 서로 다르다.
		System.out.println(pi);
		System.out.println(f);
		System.out.println(pi== 3.14);
		System.out.println(s == "A");
		//기본 데이터 타입이 같을 땐 == 로, 그게 아닐땐 equals() 같은 해당 함수로 비교한다.
		System.out.println(s == t);
		//자바에서는 s.equals(t) 이렇게 비교를 한다.
		System.out.println(s.equals(t));
		
		//작은 타입을, 큰 타입에 넣을 수 있다.
		byte by = 11;
		// short > byte 이므로 이렇게 넣는게 가능하다.
		short sh = by;
		// 이렇게도 가능
		double db = sh;
		System.out.println(db);
		
		// double > float > long > int > short > byte 
		// 데이터 타입이 표현할 수 있는 범위이다. float는 1.1 (실수가능 but 32bit) 
		// long은 1.1 표현 불가능. 상수만 가능하다. (but 64 bit) 
		// 이 크다 작다는 메모리의 크기가 아니라 데이터를 표현할 수 있는 범위다.
		// int > char 이렇게 가능하다. 
		
		//이렇게 하면 안된다.
		// l = pi
		//그래서 이렇게 형변환을 해줘야한다. 이것을 '명시적 type casting' 이라고 한다
		l = (long)pi;
		System.out.println(pi);
		System.out.println(l);
		
		
	}
}