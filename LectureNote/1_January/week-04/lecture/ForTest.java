
public class ForTest {
	public static void main(String[] args) {
		int sum = 0;
		for (int i =1 ; i<=10; i++){
			sum += i;
			//System.out.println("i = "+ i+ ", sum = "+sum);
			
		}
		
		int[] ia = {1,2,3,4,5,6,7,8,9,10};
		sum = 0;
		for (int i =0; i < ia.length; i ++) {
			sum += ia[i];
			//System.out.println(ia[i]);
		}
		System.out.println(sum);
		
		//for each 문 파이썬의 for 문과 같다.
		// for n in ia
		//배열이 등장하면 이런식으로 자주 쓰긴 한
		sum = 0;
		for(int n:ia) {
			sum += n;
			//System.out.println(n);
		}
		System.out.println(sum);
	}
}
