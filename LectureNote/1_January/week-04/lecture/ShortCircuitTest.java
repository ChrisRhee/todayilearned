
public class ShortCircuitTest {
	public static void main(String[] args) {
		int x = 0;
		int y = 0;
		// & 를 하나만 쓰면 앞 뒤 둘다 확인한다. 
		if((2>5) & (x++>y))
			y++;
		System.out.println("x = " +x+", y = "+y);
		
		x  =0;
		y  =0;
		// &&일땐 false가 나올 조건을 먼저 주면 앞에것만 연산하고 끝나버린다.
		// ||일땐 true가 나올 조건을 먼저주면 앞에 것마나 연산하고 끝나니 이득
		if((2>5) && (x++>y))
			y++;
		System.out.println("x = " +x+", y = "+y);
	}
}
