package com.ssafy.java.array;

import java.util.Arrays;

public class ArrayTest3 {

	public static void main(String[] args) {
		// 1
		int id[][];
		
		// 2
		id = new int[3][2];
		
		// 1+2
		int is[][] = new int [3][2];
		
		//3
		id[0][0] = 10;
		id[0][1] = 11;
		id[1][0] = 20;
		id[1][1] = 21;
		id[2][0] = 30;
		id[2][1] = 31;
		
		// 1+2+3
		int ia[][] = {
				{10,11},
				{20,21,22},
				{30}
				};
		
		// 2중 배열은 이렇게해서 확인 할 수 있다
		for(int a[]:ia) System.out.println(Arrays.toString(a));
		System.out.println();
		// 1+2+3
		int ig[][] = new int[][]{
				{10,11},
				{20,21},
				{30,31}
				};
		
		// 4
		// 각 배열의 길이를 검색해보면 ia는 3, 나머지는 2,2,2 나온다
		System.out.println(ia.length);
		System.out.println(ia[0].length);
		System.out.println(ia[1].length);
		System.out.println(ia[2].length);
		System.out.println();
		// 5
		// la.length = 5;
		// 이렇게 length에 값을 대입할 순 없다. 
		
		// 6
		// 2차원 배열은 이렇게 출력한다.
		for(int i = 0; i < ia.length; i++) {
			for(int j = 0; j < ia[i].length; j ++) {
				System.out.print("ia["+i+"]["+j+"]="+ia[i][j]+" ");				
			}
			System.out.println();
		}
		System.out.println();
		
		for (int a[] : ia) {
			for (int n : a) {
				System.out.print(n+" ");
			}
			System.out.println();
		}
		System.out.println();
		/*
		int one =1;
		int two =2;
		
		int sum =one+two;
		int ia[] ;
		ia = new int[3];
		
		//선언과 생성을 한번에 한다
		int id[]=new int[3];
		
		ia[0]=10;
		ia[1]=11;
		ia[2]=12;
		
		//왜이렇게 다른거야 ㅡㅡ
		int ik[];
		ik = new int[] {10,11,12};
		System.out.println(ik.length);
		System.out.println(ia);
		System.out.println(Arrays.toString(ia));
		*/
	}

}
