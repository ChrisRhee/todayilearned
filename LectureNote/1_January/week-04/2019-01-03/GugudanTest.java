package com.ssafy.java.array;

public class GugudanTest {
	
	public static void main(String[] args) {
		// 클래스 이름을 쓰고 함수를 호출하면 된다.
		Gugudan g = new Gugudan();
		g.inputDan();
		//같은 클래스에선 Gugudan 안써줘도 된다.
		g.printDan();
	}
}
