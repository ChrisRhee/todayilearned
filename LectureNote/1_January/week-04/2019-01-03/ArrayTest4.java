package com.ssafy.java.array;

import java.util.Arrays;

public class ArrayTest4 {

	public static void main(String[] args) {
		
		
		// 1
//		Person pa[][];
//
//		// 2
//		pa = new Person[3][2];
//
//		// 1+2
//		int is[][] = new int [3][2];
		Person pa[][] = new Person[3][];
		pa[0] = new Person[2];
		pa[1] = new Person[3];
		pa[2] = new Person[1];
		pa[0][0] = new Person("이찬호",29);
		pa[0][1] = new Person("크리스",25);
		pa[1][0] = new Person("박지연",28);
		pa[1][1] = new Person("지냐니",39);
		pa[1][2] = new Person("바보",11);
		pa[2][0] = new Person("죠습니다",22);
//		//3
//		pa[0][0] = new Person("이찬호",29);
//		pa[0][1] = new Person("크리스",25);
//		pa[1][0] = new Person("박지연",28);
//		pa[1][1] = new Person("지냐니",39);
//		pa[2][0] = new Person("상어는영어로",11);
//		pa[2][1] = new Person("죠습니다",22);
//		
//		// 1+2+3
//		Person pa[][] = {
//				{new Person("이찬호",29),new Person("크리스",25)},
//				{new Person("상어는",22),new Person("영어로",33),new Person("죠습니다",44)},
//				{new Person("지냐니바보",28)}
//				};
//		
		// 2중 배열은 이렇게해서 확인 할 수 있다
		for(Person a[]:pa) System.out.println(Arrays.toString(a));
		System.out.println();
//		// 1+2+3
//		int ig[][] = new int[][]{
//				{10,11},
//				{20,21},
//				{30,31}
//				};
//		
//		// 4
		// 각 배열의 길이를 검색해보면 ia는 3, 나머지는 2,2,2 나온다
//		System.out.println(pa.length);
//		System.out.println(pa[0].length);
//		System.out.println(pa[1].length);
//		System.out.println(pa[2].length);
//		System.out.println();
//		// 5
//		// la.length = 5;
//		// 이렇게 length에 값을 대입할 순 없다. 
//		
//		// 6
		// 2차원 배열은 이렇게 출력한다.
		for(int i = 0; i < pa.length; i++) {
			for(int j = 0; j < pa[i].length; j ++) {
				System.out.print("pa["+i+"]["+j+"]="+pa[i][j]+" ");				
			}
			System.out.println();
		}
		System.out.println();
		
		for (Person a[] : pa) {
			for (Person n : a) {
				System.out.print(n+" ");
			}
			System.out.println();
		}
		System.out.println();
		
		
		
		/*
		int one =1;
		int two =2;
		
		int sum =one+two;
		int ia[] ;
		ia = new int[3];
		
		//선언과 생성을 한번에 한다
		int id[]=new int[3];
		
		ia[0]=10;
		ia[1]=11;
		ia[2]=12;
		
		//왜이렇게 다른거야 ㅡㅡ
		int ik[];
		ik = new int[] {10,11,12};
		System.out.println(ik.length);
		System.out.println(ia);
		System.out.println(Arrays.toString(ia));
		*/
	}

}
