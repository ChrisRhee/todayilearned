package com.ssafy.java.array;

import java.util.Arrays;

public class ArrayTest1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int one =1;
		int two =2;
		
		int sum =one+two;
		int ia[] ;
		ia = new int[3];
		
		//선언과 생성을 한번에 한다
		int id[]=new int[3];
		
		ia[0]=10;
		ia[1]=11;
		ia[2]=12;
		
		//왜이렇게 다른거야 ㅡㅡ
		int ik[];
		ik = new int[] {10,11,12};
		System.out.println(ik.length);
		System.out.println(ia);
		System.out.println(Arrays.toString(ia));
	}

}
