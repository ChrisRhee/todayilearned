package com.ssafy.java.array;

import java.util.Arrays;

public class CopyTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int src[] = {1,5,3,4,2,6,7,8,9};
		Arrays.sort(src);
		int dst [ ]=new int[9];
		System.out.println(Arrays.toString(src));
		System.out.println(Arrays.toString(dst));
//		for (int i =0; i< src.length;i++) {
//			dst [i] = src[i];
//		}
		System.out.println();
		System.arraycopy(src, 0, dst, 0, src.length);
		System.out.println(Arrays.toString(src));
		System.out.println(Arrays.toString(dst));
	}

}
