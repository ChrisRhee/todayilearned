package com.ssafy.java.array;

public class Person {
	private String name;
	private int age;
	
	//생성자: 생성자 이름은 클래스 이름과 같아야 한다. 
	
	public Person(String name, int age) {
		setName(name);
		setAge(age);
	}
	
	public Person() {
		this("죠습니다",100);
	}
	
	//메소드: 메소드 이름은 개발자 마음대로 결정함
	public void info() {
		System.out.println(getName()+","+getAge());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public String toString() {
		return getName()+"("+getAge()+")";
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i ; 
		int ia[];
		Person p= new Person();
		Person p2 = new Person("장한별개복치",23);
		Person p3 = new Person();
		
		p.setAge(29);
		p.setName("이찬호");
		p.info();
		p2.info();
		p3.info();
		System.out.println(p);
		System.out.println(p.toString());
	}

}
