package com.ssafy.java.array;

import java.util.Scanner;

public class Gugudan {
	private int dan;
	
	public void inputDan() {
		Scanner sc = new Scanner(System.in);
		System.out.print("수? ");
		dan = sc.nextInt();
		System.out.println();
	}
	
	public void printDan() {
		
		for (int i = 1; i <=9; i++) {
			for(int j = 2; j<=dan; j ++) {
				System.out.printf("%dx%d=%2d ", j,i,i*j);
			}
			System.out.println();
		}
	}
}
