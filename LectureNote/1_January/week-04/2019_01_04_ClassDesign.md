***
contents : Java, Class Design, OOP(object oriented program)

date : 2019 01 04 fri
***

## 자바의 goto문

```java
// 기존에 나온 수였는지 체크하기
public void numCheck() {
    for(int i = 0; i < check.length; i++) {
        int num = getNum();
        check[i] = num;
        for (int j = 0; j<i; j++) {
            if(check[i] == check[j]) {
                i--;
                break;
            }
        }
    }
}
```
기존 코드,

```java
// 기존에 나온 수였는지 체크하기
public void numCheck() {
    top:for(int i = 0; i < check.length; i++) {
        int num = getNum();
        
        for (int j = 0; j<i; j++) {
            if(check[i] == check[j]) {
                i--;
                break top;
            }
        }

        check[i] = num;
    }
}
```
변형 코드

top 을 이용하면, break 해서 top으로 바로 나갈 수 있다. goto 문인데 객체지향에선 사용하길 지양하지만, 알고리즘 문제를 풀 때 빠르게 쓰는 치트키 처럼 쓸 수 있다. 

## Collections
콜렉션 API가 있다. 이걸 이용하면 객체를 관리하기 좋다.

## System.setIn

```java
public static void main(String[] args) throws Exception {
    //이렇게 하면 파일에 저장된 내용을 알아서 입력해서 읽을 수 있다.
    System.setIn(new FileInputStream("DigitTest1.txt"));
```
10 55 2 63 85 61 85 0
이걸 입력 테스트때 일일이 입력 안하고 저 파일에 적어놓으면, 알아서 가져와서 저 내용을 입력한 결과를 보여준다. 개이득
기본 directory는 프로젝트를 기준으로 한다. 여기에선 Java1 폴더에 있다.
```java
System.setIn(new FileInputStream("src/com/ssafy/algo/DigitTest1.txt"));
```
그래서 이렇게 지저분하게 써야 한다. 근데,이렇게 할 경우 패키지를 매번 교체를 해야하니 그냥 프로젝트 폴더에 넣고 하는게 편하다.

## 삼성 알고리즘 시험
시험에서는 System.setIn 이부분을 주석처리 하고 제출해야한다.
```java
System.setIn(new FileInputStream("input.txt"));
```
이렇게 사용할 수 있지만, 기본적으로 주석처리가 되어있어서 테스트 할때만 쓰고 제출할때는 다시 삭제하고 제출해야한다. 테스트 할때만 사용 할 것. 근데 개이득이긴 하다.

```java
public class Solution {
	static int TestCase, N, Answer;
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		//System.setIn(new FileInputStream("input.txt"));
		Scanner sc = new Scanner(System.in);
		TestCase=sc.nextInt();
		for(int test_case = 1; test_case <=TestCase; test_case++) {
//			N=sc.nextInt();
//			Answer = N*N;
			// 작성하세요...
			
			//이렇게 주어진다.
			
			
			System.out.println("#"+test_case + " "+Answer);
		}
		sc.close();
	}
}
```
실제 시험에선 저렇게 주어져서 작성하세요 부분에다가 내 알고리즘을 작성하는 식으로 작성 된다. 그리고 테스트 케이스가 많은 경우
```java
for(int test_case = 1; test_case <=1 /*TestCase*/; test_case++) {
```
이렇게 테스트케이스 1만 해서 확인해볼 수 있다. 

