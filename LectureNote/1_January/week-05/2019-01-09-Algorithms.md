***
contents : Java, Algorithms

date : 2019 01 09 Wed
***

# 박상진 교수님
정올, 백준
- 파일명 : Main문제번호_문제이름_서울10반_이찬호.java

sw expert
- 파일명 : Solution문제번호_문제이름_서울10반_이찬호.java

## 쉬운 문제를 많이 풀어봐야함
https://www.swexpertacademy.com/main/code/problem/problemList.do?problemLevel=1&problemTitle=&orderBy=PASS_RATE&select-1=P&pageSize=10&pageIndex=1

코드 -> problem -> 난이도 1 -> 정답률순으로 검색해서 보면 된다.

IM or ad를 따려면

3,4 단계를 풀어봐야한다. 

## 알고리즘 (5/430)
유한한 단계를 해결하기 위한 절차나 방법. 

### 무엇이 좋은 알고리즘인가?
1. 정확성 : 정확하게 동작하는가?
2. 작업량 : 얼마나 적은 연산으로 원하는 결과를 얻어냈는가
    - 처음부터 가능하면 루프 덜 돌릴 수 있는 방법으로 짜라
3. 메모리사용량 : 가능하면 적은 메모리로 해라
4. 단순성 : 얼마나 단순한가. 
    - 코드는 깔끔해야한다.
    - 누가봐도 군더더기 없도록
5. 최적성 : 더 이상 개선할 여지없이 최적화되었는가 


## hw
Solution1204_최빈수구하기
Sulution2063_중간값찾기