//package com.ssafy.user;

public class Teacher {
	private int teacherNumber;
	private String gender;
	private String phoneNumber;
	private int[] salary;

	public Teacher() {
	}
	public Teacher(int teacherNumber, String gender, String phoneNumber) {
		this.teacherNumber = teacherNumber;
		this.gender = gender;
		this.phoneNumber = phoneNumber;
		this.salary = new int[12];
	}
	public Teacher(int teacherNumber, String gender, String phoneNumber, int[] salary) {
		this.teacherNumber = teacherNumber;
		this.gender = gender;
		this.phoneNumber = phoneNumber;
		this.salary = salary;
	}
	/** 전화번호를 phoneNumber로 수정하는 함수*/
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/** month월의 급여를 salary로 수정하는 함수*/
	public void setSalary(int month, int salary) {
		this.salary[month-1] = salary;
	}
	/** 마지막으로 급여가 입련된 달의 다음달의 급여에 salary를 급여로 추가하는 함수*/
	public void addSalary(int salary) {
		for(int i=0; i<this.salary.length; i++) {
			if(this.salary[i] == 0) {
				this.salary[i] = salary;
				break;
			}
		}
	}
	/** 교사번호, 성별, 폰번호 그리고 급여를 출력한다. (급여는 입력된 월까지 출력한다.)*/
	public String toString() {
		String str = teacherNumber + "\t" + gender + "\t" + phoneNumber + "\t";
		for(int i=0; i<this.salary.length; i++) {
			if(this.salary[i] != 0) {
				str += (i+1) + "월 : " + this.salary[i] + "  ";
			}
		}
		return str;
	}
	
}
