package test.com.ssafy.object;

import com.ssafy.object.Reservation;

public class ObjectTest {
	public static void main(String[] args) {
		String title="영화 예매";  //new String("")
		// 생성자에 의한 초기화
		Reservation r=new Reservation("일반", -1);
		r.print();//System.out.println(r.type+" "+r.quantity);
		
		Reservation r2=new Reservation();		
		r2.print();//System.out.println(r2.type+" "+r2.quantity);
	}
}
