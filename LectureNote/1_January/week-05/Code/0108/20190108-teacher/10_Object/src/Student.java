//package com.ssafy.user;

public class Student {
	private int studentNumber;
	private String gender;
	private String phoneNumber;
	private double[] grade;
	
	public Student() {
	}
	public Student(int studentNumber, String gender, String phoneNumber) {
		this.studentNumber = studentNumber;
		this.gender = gender;
		this.phoneNumber = phoneNumber;
		this.grade = new double[10];
	}
	public Student(int studentNumber, String gender, String phoneNumber, double[] grade) {
		this.studentNumber = studentNumber;
		this.gender = gender;
		this.phoneNumber = phoneNumber;
		this.grade = grade;
	}
	/** 전화번호를 phoneNumber로 수정하는 함수*/
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**  semester학기의 학점을 grade로 수정하는 함수*/
	public void setGrade(int semester, double grade) {
		this.grade[semester-1] = grade;
	}
	/**  마지막으로 학점이 입력된 다음학기에  grade를 학점으로 추가하는 함수*/
	public void addGrade(double grade) {
		for(int i=0; i<this.grade.length; i++) {
			if(this.grade[i] == 0) {
				this.grade[i] = grade;
				break;
			}
		}
	}
	/** 학번, 성별, 폰번호 그리고 학점을 출력한다. (학기는 성적이 입력된 학기까지 출력한다.)*/
	public String toString() {
		StringBuffer str =new StringBuffer(); 
			str.append(studentNumber + "\t" + gender + "\t" + phoneNumber + "\t");
		for(int i=0; i<this.grade.length; i++) {
			if(this.grade[i] != 0) {
				str.append( (i+1) + "학기 : " + this.grade[i] + "  ");
			}
		}
		return str.toString();
	}
}
