package test.com.ssafy.object;

import com.ssafy.object.Reservation;

//타입 - 기본형-stack, 객체 - new heap  ==>VM 의 메모리 체계
public class MemoryTest {
	public static void main(String[] args) {
		int a1=10;
		long a2=10;
		char a3='H';
		String s1="Hi";
		String s2="Hi";
		String s3=new String("Hi");
		String s4=new String("Hi");
		Reservation r1=new Reservation();
		Reservation r2=new Reservation("Hi", 10);
		System.out.println(a1);
		System.out.println(a2);
		System.out.println(a3);
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
		System.out.println(s4);
		System.out.println(r1);
		System.out.println(r2);
		
	}
}
