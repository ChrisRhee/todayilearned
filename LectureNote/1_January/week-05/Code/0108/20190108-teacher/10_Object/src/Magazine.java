//package com.ssafy.object;

public class Magazine {
	private String isbn;
	private String title;
	private String author;
	private String publisher;
	private int price;
	private String desc;
	private int year;
	private int month;
	
		
	
	
	public Magazine(String isbn, String title, String author, String publisher, int price, String desc, int year,
			int month) {
		
		setIsbn(isbn);//this.isbn = isbn;
		setTitle(title);//this.title = title;
		setAuthor(author);//this.author = author;
		setPublisher(publisher);//this.publisher = publisher;
		setPrice(price);//this.price = price;
		setDesc(desc);//this.desc = desc;
		setYear(year);//this.year = year;
		setMonth(month);//this.month = month;
	}
	public Magazine(String isbn, String title, String author, String publisher, int price, int year,
			int month) {
		this( isbn,  title,  author,  publisher,  price, "",  year, month);
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}
	
	public String toString() {
		String str = String.format("%-7s| %-15s| %-7s| %-12s| %-6d| ",this.isbn,this.title,this.author,this.publisher,this.price);
		if(this.desc !=null)str+=this.desc;
		str+=String.format("%5s\t| %d.%d","",this.year,this.month);
		return str;
	}
}
