//package test.com.ssafy.object;

//import com.ssafy.object.Book;
//import com.ssafy.object.Magazine;

public class BookTest {

	public static void main(String[] args) {
		Book[] b=new Book[5];
		b[0]=new Book("21424","Java Basic","김하나","Jaen.kr",15000,"자바 기본 문법");
		b[1]=new Book("33455","JDBC Pro","김철수","Jaen.kr",23000);
		b[2]=new Book("55355","Servlet/JSP","박자바","Jaen.kr",41000,"Model2 기반");
		b[3]=new Book("35332","Android App","홍길동","Jaen.kr",25000,"Lightweight Framework");
		b[4]=new Book("35355","OOAD 분석,설계","소나무","Jaen.kr",30000);
		
		Magazine[] m=new Magazine[5];
		m[0]=new Magazine("35535","Java World","편집부","Jaen.kr",7000,2013,2);
		m[1]=new Magazine("33434","Mobile World","편집부","Jaen.kr",8000,2013,8);
		m[2]=new Magazine("75342","Next Web","편집부","Jaen.kr",10000,"AJAX 소개",2012,10);
		m[3]=new Magazine("76543","Architecture","편집부","Jaen.kr",5000,"java 시스템",2010,3);
		m[4]=new Magazine("76534","Data Modeling","편집부","Jaen.kr",14000,2012,12);
		
		
		System.out.println("******************** 도서 목록 ********************");
		for(int i=0;i<b.length;i++)System.out.println(b[i]);
		
		System.out.println("\n******************** 잡지 목록 ********************");
		for(int i=0;i<m.length;i++)System.out.println(m[i]);

	}

}
