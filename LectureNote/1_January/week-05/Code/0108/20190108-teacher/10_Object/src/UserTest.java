//package test.com.ssafy.user;

//import com.ssafy.user.Student;
//import com.ssafy.user.Teacher;

public class UserTest {

	public static void main(String[] args) {
		double[] g = {2.3, 4.3, 2.0, 2.7, 4.3, 3.7, 4.0, 0.0, 0.0, 0.0};
		Student s1 = new Student(51342, "남자", "01029547116", g);
		Student s2 = new Student(21543, "남자", "01077038162");
		Student s3 = new Student(34124, "여자", "01054715454");
		s2.addGrade(2.7);		// 학점추가
		s2.addGrade(4.3);
		s3.addGrade(3.7);
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
		s3.setGrade(1, 4.3);	// 학점수정
		System.out.println(s3 + "<- 수정 후");
		
		int[] salary = {5, 10, 20, 15, 5, 5, 5, 12, 0, 0, 0, 0};
		Teacher t1 = new Teacher(11154, "남자", "01084842241", salary);
		Teacher t2 = new Teacher(11355, "여자", "01015421454");
		Teacher t3 = new Teacher(11222, "여자", "01084515431");
		t2.addSalary(77);
		t2.addSalary(44);
		t3.addSalary(11);
		System.out.println(t1);
		System.out.println(t2);
		System.out.println(t3);
		t3.setSalary(4, 33);
		System.out.println(t2+ "<- 수정 후");
		
	}
}
