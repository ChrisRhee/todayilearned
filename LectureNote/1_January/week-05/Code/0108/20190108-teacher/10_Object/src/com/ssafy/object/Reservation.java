package com.ssafy.object;
//Stack 중복 구조 ==  구조체
public class Reservation {
	//캡슐화, 재사용도 되면 좋겠다.
	//접근 지정자를 변경해서 직접 접근을 막는다.
	//중간에 연결해서 접근(setXxx, getXxx)할 수 있는 메서드를 제공
	//+public -private 
	private String type;
	private int quantity;
	/**생성자  overloading
	 * 1. 클래스 이름과 같다.
	 * 2. 초기화를 담당 - 매개인자가 있다.
	 * 3. 만일 생성자가 없으면 매개인자없는 생성자를 제공(VM)
	 * 4. 사용자가 생성자를 추가할 때는 매개인자 없는 생성자 필요 여부 결정*/
	public Reservation() {}			
	public Reservation(String type, int quantity) {
		this.type = type;
		this.quantity = quantity;
	}
	/*//문법은 필수, 디자인 패턴(권장사항), 환경제공( 프레임워크) 
	public void setType(String type) {
		//
		this.type = type;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		//if >0
		this.quantity = quantity;
	}
	public String getType() {
		return type;
	}*/
	/** 예약 정보를 확인*/
	public  void print() {
		System.out.println(type+ " "+ quantity);
	}
	
}
