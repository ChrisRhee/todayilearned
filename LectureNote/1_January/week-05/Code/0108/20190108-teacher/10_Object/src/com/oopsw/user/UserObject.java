package com.oopsw.user;
/**   사용자 정의 <b> 클래스 </b>*/
public class UserObject {
	/**1  일반 적으로 멤버 데이터*/
	private String privateData;              // 묵시적 null
	
	/**2. 패키지가 없거나 기초 책*/
	String defaultData="접근 지정자를 생략";  //  명시적
	
	/**3. 나중에 배우지만 상속 이후 의미*/
	protected String  proData;
	
	/**4. 멤버 데이터가 고정값일때  ex 최대값, 기본값...*/
	public final static String DEFAULT_NAME="홍길동";
	//overloading
	public UserObject(String proData) {
		this.proData = proData;
	}
	private UserObject() {} //ex Math's 생성자
	
	//print() --> toString()
	public String toString() {
		return "private : "+ privateData+", default : "+defaultData+
				", protected : "+ proData+", public : "+DEFAULT_NAME ;
	}
}
