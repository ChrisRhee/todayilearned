//영화를 예매하는데 일반:11000원 청소년:9000원 어린이:7000원
//예매하는 사람에 따라 가격이 다르다.
//   종류 :  일반
//   예매수 :2
//  총 가격은 22000원 입니다.
public class UserTest {
	public static void main(String[] args) {
		//1 입력
		String reservationType="일반";
		int reservationQuantity=2;
		//2. 처리
		int result = 11000 *reservationQuantity;
		
		//3. 출력
		System.out.println(result);
	}
}
