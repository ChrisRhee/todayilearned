package test.com.ssafy.object;

import com.ssafy.object.Reservation;

public class ObjectTest {
	public static void main(String[] args) {
		//String title="영화 예매";
		Reservation r=new Reservation();
		r.setType("일반");//r.type="일반";
		r.setQuantity(-1);//r.quantity=-1;
		
		r.print();//System.out.println(r.type+" "+r.quantity);
		
		Reservation r2=new Reservation();
		//r2.type="일반";
		//r2.quantity=10;
		r2.print();//System.out.println(r2.type+" "+r2.quantity);
	}
}
