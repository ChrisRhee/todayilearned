package com.ssafy.object;
//Stack 중복 구조 ==>  구조체
public class Reservation {
	//캡슐화, 재사용도 되면 좋겠다.
	//접근 지정자를 변경해서 직접 접근을 막는다.
	//중간에 연결해서 접근(setXxx, getXxx)할 수 있는 메서드를 제공
	//+public -private 
	private String type;
	private int quantity;
	//문법은 필수, 디자인 패턴(권장사항), 환경제공( 프레임워크) 
	public void setType(String type) {
		this.type = type;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getType() {
		return type;
	}
	/** 예약 정보를 확인*/
	public  void print() {
		System.out.println(type+ " "+ quantity);
	}
	
}
