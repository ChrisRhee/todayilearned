package test.com.ssafy.object;

import com.ssafy.object.Reservation;

public class ObjectTest {
	public static void main(String[] args) {
		//String title = "영화 예매";
		//여기에서 바로 초기화를 할 수 있따             이렇게
		Reservation r = new Reservation("일반",-1);
		/** 여기부터는 기존 값을 '수정'하는 단계이다. 불필요함 */
		r.print();
		
		
		Reservation r2 = new Reservation();
		//r2.setType("일반");
		//r2.setQuantity(10);
		r2.print();
	}
}
