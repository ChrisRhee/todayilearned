import java.util.Scanner;

// 영화 예매하는데 일반:11,000원 청소년: 9,000원 어린이: 7,000원
// 예매하는 사람에 따라 가격이 다르다.
// 입력: 
//	종류 : 일반
//  예매 수 : 2
//  총 가격은 22,000원 입니다.
// 2명 이상이 예매를 하려고 한다. 

public class UserTest {
	public static void main(String[] args) {
		int cost = 0;
		String person = "";
		int total = 0;
		
		// 입력
		Scanner sc = new Scanner(System.in);
		System.out.print("종류 : ");
		person = sc.next();
		System.out.print("예매수 : ");
		cost = sc.nextInt();
		
		// 처리
		if(person.equals("일반")) {
			total = 11000 * cost;
		}
		else if (person.equals("청소년")) {
			total = 9000 * cost;
		}
		else if (person.equals("어린이")) {
			total = 7000 * cost;
		}
		
		//출력
		sc.close();
		System.out.println("총 가격은 "+total+"원 입니다.");
	}
}
