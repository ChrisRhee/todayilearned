package test.com.ssafy.object;

import com.ssafy.object.Movie;

public class MovieTest {
	public static void main(String[] args) {
		Movie m = new Movie();
		m.setDirector("이찬호");
		m.setTitle("이찬호의 모험");
		m.setGrade(19);
		m.setGenre("액션 코미디 로맨스 어드벤쳐 스릴");
		m.setSummary("놀라운 재미!");
		m.movieInfo();
		
	}
}
