package com.ssafy.object;
//stack 중복 구조를 끌어 왔다 => 구조체
public class Reservation {
	// 캡슐화, 재사용도 되면 좋다.
	// 접근 지정자를 변경해서 직접 접근을 막는다.
	// 중간에 연결해서 접근할 수 있는 메서드를 제공한다.
	// 값을 넣거나(set), 가져오는 것 (get)할 수 있따.
	// +public, -private
	private String type;
	private int quantity;
	
	/** 생성사 조건
	 * 1. 클래스 이름과 같다.
	 * 2. 초기화를 담당 - 매개인자가 있다.
	 * 3. 만일 생성자가 없으면 매개인자 없는 생성자를 (Virtual machine)이 제공한다. 
	 * 4. 사용자가 생성자를 추가 할 때는 매개인자 없는 생성자 필요 여부를 결정해줘야한다.
	 * 
	 * */

	// 생성자 overloading : 매개인자가 다른 생성자를 여러개 만드는 것 
	public Reservation() {	}
	public Reservation(String type, int quantity) {
		this.type = type;
		this.quantity = quantity;
	}


	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	/** 예약 정보를 확인*/
	public void print() {
		System.out.println(type+ " " + quantity);
	}
}
