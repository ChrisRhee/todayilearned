package com.ssafy.object;

public class Movie {
	private String title;
	private String director;
	private int grade;
	private String genre;
	private String summary;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	
	
	public void movieInfo() {
		System.out.println("title: "+this.title);
		System.out.println("director: "+this.director);
		System.out.println("grade: "+this.grade);
		System.out.println("genre: "+this.genre);
		System.out.println("summary: "+this.summary);
	}
	
	public String toString() {
		movieInfo();
		String result = "";
		return result;
	}
}
