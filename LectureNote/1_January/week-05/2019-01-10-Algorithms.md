***
contents : Java, Algorithms

date : 2019 01 10 Thu
***

# 박상진 교수님
정올, 백준
- 파일명 : Main문제번호_문제이름_서울10반_이찬호.java

sw expert
- 파일명 : Solution문제번호_문제이름_서울10반_이찬호.java


문제는 언제든 터진다. 중요한건 어떻게 대처하느냐이다.

## 버블소트 리팩토링
- 버블소트를 직접 사용하는 이유는?
- 굳이 Arrays.Sort를 사용하지 않는 이유는? 
    1. 직접 소트 알고리즘을 구현해야 할 때.
    2. 소트에 내 알고리즘을 넣을 수 있다.
- 지금 이 과제에서는 수가 **홀수**로만 들어오기 때문에 이 버블소트를 바꿀 수 있다. 
- 그래서 중간값을 찾는 것이니, 소트를 딱 반만 해서 올려보자.

```java
//Bubble Sort
for(int i=ia.length-1; i>=0; i--) {
    for(int j=0; j<i; j++) {
        int temp;
        if(ia[j] > ia[j+1]) {
            temp = ia[j];
            ia[j] = ia[j+1];
            ia[j+1] = temp;
        }
    }
}

//Bubble Srot Refactoring
for(int i=ia.length-1; i>=0; i--) {
    for(int j=0; j<i/2; j++) {
        int temp;
        if(ia[j] > ia[j+1]) {
            temp = ia[j];
            ia[j] = ia[j+1];
            ia[j+1] = temp;
        }
    }
}
```


## 재귀호출
- 재귀호출은 잘 쓰는 방법을 알고 있어야한다.
- 이거 없이 for loop문으로 다해결할 수 있긴 하다.
- 재귀호출을 사용하면 깔끔하게 프로그램을 할 수 있따.
