***
contents : Java, S/w Architecture

date : 2019 01 08 tue
***

## 전혜영 교수님
* Email : OOPSW@tistory.com
* 서버 : http://70.12.109.160:8888
* 숙제 : 이름.zip 
* 질문하는 방법
    - 코딩은 에러메세지를 찍어서 보내라
    - 수업자료는 수업 끝나고 배포가 된다. 
    - 배포된 자료는 나만 볼 수 있는 곳에 올려놓고 보세요.
    - 이해가 안되는 부분은 이 부분이 왜 이해가 안되는지를 설명해서 보내자
- 질문에 대한 반응
    1. 다음주에 할거야. 잘하고있음
    2. main말고 가지 질문인 경우가 있따. 필요는한데, main으로 중요한건 아닌경우
    3. main은 여기있는데 완전 저기 바깥쪽에서 질문하는 사람이있따. 

## 요구사항 분석
- 페르소나
    - 요구사항 분석은 사람을 관찰 해야 한다. 
    - 내 시각 x, 그 사람의 입장에서 요구사항을 분석하는 것.
    - 반대로, 내 입장에서 설명할 때 명확하게 상대에게 이야기를 못하면 페르소나가 전달되지 않는다. 
        - 불편함, 니즈, 목표가 무엇인지를 잘 전달해야한다. 
    - 프로젝트에서만 있지 않다. 가장 중요한 학습법
        1. "나"의 범위에서 찾자.
        2. ws나 hw은 배운거 이상으로 나오지 않는다. 누적될 뿐
        3. 작은 만족감이 계속 쌓여야 한다. 

- 행복은 절대적인가 상대적인가?
    - 절대적 : 
        - 자기가 만족해야한다?
        - 무슨일인가?
    - 상대적 :

- 난 오늘 무엇이 행복했는가
    - 이불 개고 나옴 ㅋ
    - 지하철 잘옴 ㅋ
    - 맥모닝 맛있음 ㅋ 

- 전문 용어를 썼다면, 내가 그 용어를 내 언어로 설명 할 줄 알아야 한다. 
- 문제를 코드로만 짜는게 문제가 아니라, 문제에 대한 요구사항이 무엇인지 그걸 파악해서 어떻게 구현했는지를 설명할 수 있어야 한다. 
- 알고리즘 문제에서는 입력,출력 이 답이 정확하게 있다. 하지만 이 문제에서는 틀을 만들었는지를 보는 문제이므로 입력 출력에 스트레스 받을 필요가 없다. 
- 요구사항을 관점을 잘 찾아서 이야기를 해야한다. 
- 소프트웨어를 평가하는 기준은 **요구사항**이다. 

- 요구사항을 만족하면 일단 잘한거다, 그 이후에는 "그럼 이건 왜 있을까?"를 물으면 된다. 

- 내가 사용하는 클래스와 메소드는 메모리에 한번만 올라간다. 
    파워포인트가 처음 켜지면 메모리에 한번 올라가고, 그 이후에 새로운걸 부르든 불러오든 더 올라가는게 아니라 올라가있는 파워포인트에서 쭉 한다.

- 아케텍쳐 프로그램에서는 인풋과 아웃풋에 대한 결과를 체크하기가 어렵다. 실행도 되고 에러는 안나는데 내가 예상한 결과와 안맞을 수 있기 떄문이다
    - 그래서 하나하나 확실하게하면서 코딩을 해야 한다. 


## Java Memory
![JavaMemory1](./img/0108/javaMemory1.png)

- java로 실행하면 컴퓨터의 콘솔 영역, 이클립스 밖에 A에서 실행한다. 
- javaw로 하면 백 프로세스로 연결되어서 A와 다른곳에서 진행된다. 그래서 둘의 결과가 다를 수 있따.
- 소스가 실행될 수 있는 코드 영역이 메모리에 올라간다. 그래서 같은 코드 영역은 메모리에 또 올릴 필요 없다. 

- 스택 메모리를 저장하는 단위를 어떻게 할 것인가?
    - int, long, ... 다 쌓이는데 단위 기준이 없다.
        책을 쌓을때 같은 크기면 상관없는데 크기가 다 다르면? 이상하게 쌓인다.
    - 10이 지금 몇번 들어갔나? 2번 들어갔다.
    - Hi는? 4번 들어갔다. 

- 2번 반복이면 무조건 **규칙**이다.
    - 중복코드 문제는 for문 안에서 돌아가는 연산에서 문제가 되는 경우가 많다. 
    - 소스코드의 반복는 눈에 보이지만, 메모리의 반복는 눈에 안보인다. 그래서 그림으로 그려봐야한다. 

- 이렇기 때문에 2가지를 해결봐야 한다.
    1. 스택의 메모리
    2. 중복 코드 

### Pool
![JavaMemory2](./img/0108/javaMemory2.png)

- 코드를 실행할때 쫙 스캔을 한다. 그래서 내가 기본적으로 사용하는 정수, 실수 , 고정값들은 literal pool(기본형), string pool 이라는 메모리가 있다. 
    - 특징 : 1번만 공유되지만, 수정은 안된다 => r.java 랑 같은 기능 
    - 처음에 할때도 처음부터 쭉 스캔해서 같은게 있는지 확인하고 pool을 생성한다. 
    - 그래서 그 이후에 뭔가 수정이 되면 pool에 또 새로운 값이 들어간다. 
    - 기본형에 10이 있어? 없으면 10 ㄱㄱ. 

- 스택을 중심으로 연결되는 주소까지만 설명한다. 
- 힙 메모리도 중복되면 안된다. 
- 자바는 기본적으로 다 refference로 작동한다. pool 메모리를 공유하느냐? or heap 메모리를 공유하느냐에 따라서 수정을 할수도 있고, 안될수도 있다. 
    - heap 메모리는 구조는 수정 안되지만, 그 안에 값은 변경 할 수 있다. 

- 스택 메모리를 효과적으로 쓰는 방법
    - 한 메모리가 계속 자리에 있는것보다 계속 왔다갔다 하는게 좋다.
    - 식탁에서 식판을 계쏙 왔다 치우는게 궁중 식장보다 더 편하고 빠르다.
    - 이걸 빠르게 하려면 매개인자가 **작으면** 더 빠르게 왔다갔다 할 수 있다.
        1. 소스코드를 짧게 잡아서 관리한다.
        2. 

    - 코딩습관만 달라져도 자바에서 메모리를 매우 효과적으로 쓸 수 있따.

![JavaMemory3](./img/0108/javaMemory3.png)
![JavaMemory4](./img/0108/javaMemory4.png)



## 클래스 구성과 오브젝트의 사용법 (49/111)
- modifiers


## API doc 만들기 : 프로젝트 할 때 클래스가 많아지면
- 클래스다이어그램이 너무 많으니까 다 보진 못하는 경우.
- API 도큐멘테이션을 제출한다. 
- API 도큐로 내보내는법
    1. File
    2. Export
    3. Java - Javadoc , Next >
    4. 내보낼 프로젝트 클린 후, members visibility 선택 (private, package, protected, public) , Next >
    5. VM options에 "-locale ko_KR -encoding UTF-8 -charset UTF-8 -docencoding UTF-8" 이거 추가
    6. Finish


## OOP
- 제일 중요한 품질 속성 : **유지 보수 효율성**
- 유지 보수를 잘하려면?
    1. 가독성 ↑ , 소스보단 **그림!!** 으로 구조를 이해하는게 좋다.
    - 이 구조 만 그림으로 보여주는 것을 **Class Diagram** 이라 한다.

```java
public String toString(){}

{} // 구현부
public // 접근 지정자
String // 출력, 리턴값
toString // identifier 
() // argument 매개변수
```
그리고 저걸 다 합치면 **interface**라고 부른다.

## Class Diagram
- data와 operation만 표현하면 class 라고 한다.
- 그 Class 들 간의 **관계**가 들어가면 Diagram이 된다. 
- 관계 : 기준을 이야기 해준다.  
    1. 선택관계
    2. 필수관계 : 상속할 때 설명


## 요구사항


- String은 내가 문자를 고정으로만 쓸 때 쓰는게 좋다. 
    - String 은 char[] 형이 므로, for문으로 계산하면 배열을 계속 생성하고 연산해야한다. 
    - 이 말은 스택에 계속 쌓이고, s.pool에도 계속 쌓인다는 의미이다. 
    - 그래서 string을 변경을 할때는

## String의 변경은 StringBuilder
```java
String stuscore = "Id : "+ studentId+ ", sex : "+ sex+", number : "+number+", score : ";
for(int i =0; i<this.score.length; i++) {
    if(score[i] <=0) continue;
    stuscore += "| "+(i+1)+ "학기: " + this.score[i]+" ";
}
```
이렇게 하는 것보단

```java
StringBuffer str = new StringBuffer();
// 이렇게 append로 하나씩 하나씩 append하는게 합리적이고 권장을 한다. 
str.append("Id : "+ studentId+ ", sex : "+ sex+", number : "+number+", score : ");
for(int i =0; i<this.score.length; i++) {
    if(score[i] <=0) continue;
    str.append("| "+(i+1)+ "학기: " + this.score[i]+" ");
}
    return str.toString();
```
- 이렇게 StringBuffer를 이용해서 append 해주는게 더 좋다. 가독성도 좋고 메모리 관리도 더 도움 된다.
- 근데 str은 StringBuffer인데 () 안에 "|" 따옴표로 묶인 애들은 String이므로 저렇게 + 로 하는 것은 좋지 않다. 다 append로 통일하는게 최고.



## 생성자는 매개변수 제일 많은 것부터 선언한다!
- 메서드(method)는 매개인자가 작은것부터 만든다. 아무때나 호출 할 수 있게 하려고.
- 매서드(method)랑 생성자(constructor)가 같이있을때는 생성자(constructor)는 무조건 나중에 만든다. 
- 매개인자가 적은게있고 많은게 있으면 생성자(constructor) 중에서 많은것부터 만든다
    - why? : 매서드는 아무떄나 호출가능하지만, 생성자(constructor)는 첫번째 줄에서 한번만 호출이 된다. 
- 이게 왜 필요한가?

```java
public Magazine(String isbn, String title,String author,String publisher,int year, int month, int price, String desc) {
    this.isbn = isbn;
    this.title = title;
    this.author = author;
    this.publisher = publisher;
    this.year = year;
    this.month = month;
    this.price = price;
    this.desc = desc;
}

public Magazine(String isbn, String title,String author,String publisher,int year, int month, int price) {
    this.isbn = isbn;
    this.title = title;
    this.author = author;
    this.publisher = publisher;
    this.year = year;
    this.month = month;
    this.price = price;
    this.desc = "";
}
```
이렇게 중복되는게 같은게 많은 상황이면,

```java
public Magazine(String isbn, String title,String author,String publisher,int year, int month, int price) {
    this(isbn, title, author, publisher, price, "", year, month);
}
```
이렇게 **this**를 이용해서 확 줄일 수 있다. 개이득이네

