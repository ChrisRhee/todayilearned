***
contents : Java, Algorithms

date : 2019 01 11 Fri
***

# 박상진 교수님
정올, 백준
- 파일명 : Main문제번호_문제이름_서울10반_이찬호.java

sw expert
- 파일명 : Solution문제번호_문제이름_서울10반_이찬호.java


## SOLID 객체 지향 설계
1. S (SRP) 단일 책임 원칙 (Single responsibility principle)
    - 한 **클래스**(하나의 생성자, 하나의 메소드, 하나의 인터페이스, 하나의 서비스, 하나의 프레임워크)는 하나의 책임만 가져야 한다.
2. O (OCP) 개방-폐쇄 원칙 (Open/closed principle)
    - "소프트웨어 요소는 확장에는 열려 있으나 변경에는 닫혀 있어야 한다."
    - 확장할때 쓰기는 제일 편한 것
        1. **상속** (Extense): 상속해서 개개인에게 필요한 속성을 추가한다.
            - 학생 -> 초딩
            - 학생 -> 중딩
            - 이런식으로 학생이라는 것은 동일하지만, 개개인의 특성과 속성을 이후에 추가 하면 된다.
        2. 위임 (멤버 변수 관계): 클래스 안에 변수로 클래스가 선언될 수 있도록 설계 하는 것. 
            - 기본 데이타 타입(int, string)이 아니라 이 변수 자체가 다른 변수(student, owner)가 될 수 있다. 이런식으로.
            - owner.person : 이렇게 해서 person에 있는 함수들을 쓸 수 있다.

    - 부모 클래스가 바뀌면, 하위 클래스에게 전부 전달되서 바뀐다. 하위에선 무적권 받아야 한다. 
    - **Overriding**
        - 물려 받았지만 고치고 싶은거를 고치는 것

3. L (LSP) 리스코프 치환 원칙 (Liskov substitution principle)
    - "프로그램의 객체는 프로그램의 정확성을 깨뜨리지 않으면서 하위 타입의 인스턴스를 바꿀 수 있어야 한다."
4. I (ISP) 인터페이스 분리 법칙 (Interface segregation priciple)
    - "특정 클라이언트를 위한 인터페이스 여러개가 범용 인터페이스 하나보다 낫다."
    - ex) A와 B가 있는데 AB로 구현을 해버리면 둘다 해결 할 수 있지만, A는 AB를 받으면 필요없는 B까지 받으니 불필요한게 생긴다. 그래서 A,B각각 만드는게 좋다.
    - 인터페이스도 목적이 1개가 되도록 설계한다.
    - 클래스는 1개만 상속하고, 인터페이스는 분리 하도록 만든다. 

![DIP](./img/0111/OOP-DIP.png)

5. D (DIP) 의존관계 역전 원칙 (Dependency inversion principle)
    - 프로그래머는 "추상화에 의존해야지, 구체화에 의존하면 안된다."


## 관리 어플을 만드려면 CRUD
1. C : Creat, Add
    - 등록하는 것을 먼저 작성한다.
2. R : Search, Read, find, SelectAll
    - 등록이 잘 되었는지 서치를 하는 것을 작성한다.
    - 그 이후에 하나'만' 조회 하는 것을 작성해야한다.
3. U : Update
    - 업데이트를 하려면 먼저 찾아야한다.
4. D : Delete, Remove


## 객체를 만들지 않고 클래스를 접근하는 방법
1. 추상클래스 abstract 사용
    ```java
    public class Singleton {
    }
    ```
    여기에서

    ```java
    public abstract class Singleton {
    }
    ```
    abstract 이걸 붙이면 추상 클래스가 되어서 객체 생성 안하고 바로 

    ```java
    Singleton s1 ; 
    Singleton s2 ; 
    ```
    이렇게 객체를 만들지 않고 바로 접근할 수 있따.

2. 클래스를 private 생성자를 만든다.
    - 중앙 배열로 관리하고 싶을때. 객체를 딱 1개만 만들어야 할때가 언제있을까?
        1. 은행에서 번호표를 낸 기계. 시퀀스 번호가 필요한 객체
        2. 예매 번호도 하나로 관리해야한다. 자리가 중복되면 안되기 때문에
        3. 풀링하는 객체
        4. 리소스를 로딩 하는 객체, 
        등등등
        
    - 객체를 하나로 만등러야 하는 경우가 많다. 

    ```java
        public class Singleton {
            public Singleton(){
                System.out.println("...");
            }
        }
    ```
    - 이렇게 되는데, 
    ```java
        public class Singleton {
            private Singleton(){
            }
        }
    ```
    - 객체를 private으로 생성자를 만들면 외부에서 객체 생성이 안된다. 
    - 그럼 객체 생성을 어떻게 하나?
    ```java
        public class Singleton {
            //2
            private Singleton instance = new Singleton();
            //1
            private Singleton(){
            }
            //3
            public Singleton getInstance(){
                return instance;
            }
        }
    ```
    이렇게 1,2,3 번 스테을 통해 private 싱글톤을 받을 수 있다.
    - 근데 여기엔 논리적 오류가 있다. 무엇이 문제일까?
        - 다른데서 쓰려면 객체를 생성해서 써야하는데, getInstance로 접근을 할수가 없다. 그래서 static으로 만들어서 객체를 만들지 않고 getInstance를 바로 호출 할 수 있게 해준다.
    ```java
        public class Singleton {
            //2
            private Singleton instance = new Singleton();
            //1
            private Singleton(){
            }
            //3
            public static Singleton getInstance(){
                return instance;
            }
        }
    ```
    - 이렇게 해도 안된다. 왜냐면 static은 클래스가 통체로 메모리에 할 당될때 같이 올라오기 때문에 다른 멤버들이랑 올라오는 시간이 다르다. 그래서 멤버 변수를 사용하면서 그 멤버 변수도 static으로 만들어줘야 한다.

    ```java
        public class Singleton {
            //2
            private static Singleton instance = new Singleton();
            //1
            private Singleton(){
            }
            //3
            public static Singleton getInstance(){
                return instance;
            }
        }
    ```
    - 이렇게 싱글톤이 완성되었다.

    