***
contents : Java, Exception

date : 2019 01 21 Mon
***

## 전혜영 교수님
* Email : OOPSW@tistory.com
* 서버 : http://70.12.109.160:8888
* 숙제 : 이름.zip 
* 질문하는 방법
    - 코딩은 에러메세지를 찍어서 보내라
    - 수업자료는 수업 끝나고 배포가 된다. 
    - 배포된 자료는 나만 볼 수 있는 곳에 올려놓고 보세요.
    - 이해가 안되는 부분은 이 부분이 왜 이해가 안되는지를 설명해서 보내자
- 질문에 대한 반응
    1. 다음주에 할거야. 잘하고있음
    2. main말고 가지 질문인 경우가 있따. 필요는한데, main으로 중요한건 아닌경우
    3. main은 여기있는데 완전 저기 바깥쪽에서 질문하는 사람이있따.
## Markdown 문법
### 이미지 넣기
![](./img/0114/.png)

----------------------------------------------------------
# 수업
## 08 Exception (102/111)

## 예외는
- 프로그램 실행중에서만 발생 할 수 있따!!! = 실행 환경이나 조건에 따라 다를 수 있다.
- 예외처리는 사실 환경에 따라 실행시점을 관리하는 것.

## Error
- 확률이 매우 높은 상황. 그래서 입구에서 바로 처리해야하는걸 에러라고 함. 확률상 문제가 심각해질 수 있는것이므로 무조건 막는다. 
    - 만약 에러가 뜨면? 메모리 에러가 대부분이다.
    - 메모리를 늘려주거나
    - 소프트웨어를 새로 리모델링한다.
- 개발자로서 프로젝트에 참여했는데 에러가떴다? 그럼 개발자가 의사결정을 할 수 없다. 
- 근데 확률이 조금 낮은거임 이런걸 Exception이라고 한다.

## Exception
- 배열은 클래스가 없어서 단위테스트로만 예외를 확인할 수 있다.
- 근데 nextInt()처럼 메서드가 정의되면 주석이라도 예외 발생 여부를 알려주는 것을 권장한다.
- 내가 메소드를 짤 때도 이렇게 알려주는것이 죠습니다.
- 프로젝트 할 때마다 Exception이 필요하다. 진짜 프로젝트 잘하는 팀은 Exception이 잘되어있다. 오류 검출을 미리 다 한다는 것이다.


## checkedException
- 장점 : 오류 없는 코드인데, checked 라서 무조건 안전장치를 하게 해서 오류가 절대로 안남.
- 단점 : 귀찮음 ㅋ
- checked? unchecked?
    - 지금은 **unchecked**를 선호한다.

## finally 
```java
try {
    Class.forName("java.lang.String");
} catch (ClassNotFoundException e) {
    e.printStackTrace();
} finally {
    System.out.println("try or catch 했던 무적권 마지막 출력");
    System.out.println("자원을 반환할 때");
}
```
- finally는 트라이 됐든 안됐든, 내가 썼던 자원을 넘겨줘야 한다. 그럴 때 쓴다. 
- finally일떄 불필요한 힙, 스택 메모리들 해제하는게 좋다. 그러면 메모리를 수퍼하게 관리 할 수 있다.
- 잔소리를 하기 위해 예외 클래스를 많이 만드는 것도 메모리 낭비이다. 
- 그래서 공통의 잔소리를 만들어놓고, 개별 잔소리, 특수 케이스 잔소리는 그때마다 최소화 하는 것이 좋다. 


## 사용자 정의 예외 - Customize Exception

## 종료
- break : 반복, while, switch
- return : 1개 동작
- throw : N개 동작 종료, 비정상 종료
    - throws : interface에서만 쓰임  
- System.exit() : 강제종료. 


![](./img/0121/exceptionMessage.png)

## 에러 볼때
- 내가 가지고 있는 오류는, UI가 있는 곳에다가 알려줘야함. 나만 오류가 났는지 알면 안되고, 이걸 UI단까지 보여줘야 한다.
- 제일 아래서부터 순서대로 보는것은 맞다. try catch는 내 소스부터 시작한다. 
- checked exception인데 내가 체크 안하고 일단 던진다. "이런 오류가 생길수 있어~" 정도로
```java
public void methodA() {
    methodB();
    System.out.println("methodA");
}

private void methodB() throws ClassNotFoundException {
    Integer.parseInt("1234d");
    Class.forName("java.lang.String");
    System.out.println("methodB");
}
```
- 이렇게 던진다.
- throw는 예외를 처리하는게 아니라 **예외를 전달**해주는 것이다. 
- 저렇게 위로 다 던져주기 시작하면
```java
class UserClass{
	public UserClass() throws ClassNotFoundException {
		methodA();
		System.out.println("생성자");
	}
	
	public void methodA() throws ClassNotFoundException {
		methodB();
		System.out.println("methodA");
	}

	private void methodB() throws ClassNotFoundException {
		Integer.parseInt("1234d");
		Class.forName("java.lang.String");
		System.out.println("methodB");
	}
}

public class UserExceptionTest {
	public static void main(String[] args) {
		try {
			new UserClass();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Program End");
	}
}
```
- 이렇게 메인에서 그 throw한것을 받아서 캐치를 한다. 
- 이러면 오류가 사라진다.

- 에러를 검출할때 No를 먼저 할 것인가? Yes를 먼저 할 것인가?에 따라 코딩이 다르다. 예외관리 정책은 **오류**상황이다. 
- if else에서는 Yes의 요구사항이 명확하면 Yes로, No가 명확하면 No로 하라. 근데 No를 위주로 하면 N개의 상황에서 걸러지는게 많아서 좋다. 
- 잘못입력했다고 멈출것인가? 아님 다시 부분적이라도 돌릴것인가? 


## 중요한거 2가지
1. 모든 프로그램은 API이다. 그 API의 **예외를 볼 수 있는 습관**을 들여야한다.
2. IO에서 Stream이란 단어는 **Byte**를 나타낸다. 
    - 긴 문장이나 한글이 안깨지는 이유는 Serializable 이기 때문이다. 
    - 받을 때도 뭉탱이로 받아서 묶어서 관리한다.
    - 두개의 타입이 맞아야한다.?


----------------------------------------------------------
# 수업 외 꿀팁
- 정보처리 기사는 무조건 따고
- 시간 남으면 정보 보안 따세요.

## 답을 구하는 방법
1. 뚫어지게쳐다본다.
2. 마음으로 센다.
3. 똑바로 센다. 
ㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋ


----------------------------------------------------------
# 공지사항
