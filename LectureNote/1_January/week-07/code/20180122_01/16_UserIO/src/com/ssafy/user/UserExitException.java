package com.ssafy.user;

public class UserExitException extends Exception {
	public UserExitException(String message){
		super(message);
	}
	
}
