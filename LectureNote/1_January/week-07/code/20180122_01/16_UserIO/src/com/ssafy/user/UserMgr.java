package com.ssafy.user;

import java.util.Collection;

public interface UserMgr {
	//고객 추가
	void add(Customer c) throws CustomerNameLengthException;
	//고객 확인
	Collection<Customer> getCustomers();
	//프로그램 종료
	void close(); //Runtime 계열에서 결정하면 된다.
}
