package com.ssafy.user;
//사용자 정의 예외 클래스
//1. XxxxException
//2. CheckedException Exception 상속
//   UnCheckedException RuntimeException 상속
//3. 생성자를 부모로 전달super()
public class CustomerNameLengthException extends Exception {
	public CustomerNameLengthException() {
		this("고객 이름");
	}
	public CustomerNameLengthException(String message) {
		super(message);
	}
}
