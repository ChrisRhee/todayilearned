package com.ssafy.user;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;

public class UserMgrImpl implements UserMgr{
	private Collection<Customer> list;
	
	//생성자
	public UserMgrImpl() {
		list=new ArrayList<>();
		try {
			ObjectInputStream ios=
					new ObjectInputStream(new FileInputStream("customer.txt"));
			while(true) {
				Customer c=(Customer)ios.readObject();
				list.add(c);
			}
		}catch(EOFException e) {
			System.out.println("파일 내용 다 읽었다.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("신규고객 작성");
		} catch(ClassNotFoundException e) {
			
		}
	}

	@Override
	public void add(Customer c) throws CustomerNameLengthException {
		list.add(c);		
	}

	@Override
	public Collection<Customer> getCustomers() {
		return list;
	}
	private void sendServer() throws IOException {
		new Thread() {
			public void run() {
				try {
				Socket socket=new Socket("127.0.0.1", 8888);
				ObjectOutputStream oos=
						new ObjectOutputStream(socket.getOutputStream());
					for (Customer customer : list) {
						if(customer!=null) {
							oos.writeObject(customer);
							oos.flush();
						}
					}
					oos.close();
				}catch(Exception e) {
					e.printStackTrace();
				}
			}; 
		}.start();
		
	}
	@Override
	public void close() {  //상속받은 예외가 있을때만 throws 권장
		//메모리에 있는 데이터를 죽기전에  file  저장
		try {
		sendServer();
		ObjectOutputStream oos=
				new ObjectOutputStream(
						new FileOutputStream("customer.txt"));
			for (Customer customer : list) {
				if(customer!=null) {
					oos.writeObject(customer);
					oos.flush();
				}
			}
			oos.close();
			throw new RuntimeException("정상종료");
		}catch(Exception e) {
			//e.printStackTrace();
		}
		
	}

	

}
