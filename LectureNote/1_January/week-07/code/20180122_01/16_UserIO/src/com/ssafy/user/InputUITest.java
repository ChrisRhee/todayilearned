package com.ssafy.user;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

public class InputUITest {
	public static void main(String[] args) throws IOException {
		Scanner sc=new Scanner(System.in);
		String s=sc.nextLine(); //5.0
		//byte
		//InputStream is=System.in;
		//byte->char
		//InputStreamReader isr=new InputStreamReader(System.in);
		//char->String
		BufferedReader br=new BufferedReader(
				new InputStreamReader(System.in));
		String s2=br.readLine();
		System.out.println(s);
		System.out.println(s2);
		
		
	}
}
