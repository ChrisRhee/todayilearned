package com.ssafy.user;

import java.util.Scanner;

public class UserUI {
	private Scanner sc;
	private UserMgr mgr; //
	public UserUI() throws UserExitException {
		mgr=new UserMgrImpl();
		sc=new Scanner(System.in);		
		startUI();
	}
	private void startUI() throws UserExitException {
		while(true){			
			try {
				menuCheck(inputData("menu(1입력, 2출력, 3종료) "));
			} catch (CustomerNameLengthException e) {
				//UI -  메세지 창
				//e.printStackTrace();
				System.out.println("error"+e.getMessage());
			}
		}//while		
	}
	private void menuCheck(String menu) throws CustomerNameLengthException, UserExitException {
		switch(menu) {
		case "1":
			add();
			break;
		case "2":
			print();
			break;
		case "3":
			close();
			break;
		default:
			System.out.println("메뉴는 1~3 까지만 가능");
		}//
		
	}
	private void close() throws UserExitException {
		try {
		mgr.close();
		}catch(RuntimeException e) {
			
		}finally {
			///throw new UserExitException("정상종료!!!!!");
		}
		 System.exit(0);//?		
	}
	private void print() {
		System.out.println(mgr.getCustomers());
		
	}
	private void add() throws CustomerNameLengthException {
		String name=inputData("이름");
		Customer c=new Customer(name);
		mgr.add(c);		
	}
	private String inputData(String title) {
		System.out.print(title + ":");
		return sc.nextLine();
	}
	public static void main(String[] args) {
		try {
			new UserUI();
		} catch (UserExitException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("UI정상종료 입니다.");
		}
		System.out.println("end");
	}
}
