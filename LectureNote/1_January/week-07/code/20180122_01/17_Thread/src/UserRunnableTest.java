//1. Runnable interface를 상속받아 run() 재정의
class UserRunnable implements Runnable{
	private int count;
	@Override
	public void run() {  //MultiThread 필요한 객체 공유
		while(true) {
			if(count>=50) break;
			print();
//			try {
//				Thread.sleep(1500);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}//while
	}
	private synchronized void print() {
		System.out.println(++count+" "+Thread.currentThread());	
	}	
}
public class UserRunnableTest {
	public static void main(String[] args) {
		System.out.println(Thread.currentThread());
		UserRunnable u1=new UserRunnable();
		//u1.run(); MultiThread XXXXX
		Thread t1=new Thread(u1);
		Thread t2=new Thread(u1);
		Thread t3=new Thread(u1);
		t1.start();
		t2.start();
		t3.start();
	}
}










