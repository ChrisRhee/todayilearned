//UI에서 쓰는 Thread - 동시에만 일하고 데이터를 공유하지 않는경우
public class UIThreadTest {
	public UIThreadTest() {
		new Thread() { 
			public void run() {
				while(true) {
					System.out.println(toString());
				}
			}; 
		}.start();
		
	}
	
	public static void main(String[] args) {
		new UIThreadTest();
	}
}
