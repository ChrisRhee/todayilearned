package com.ssafy.user;
import java.io.Serializable;

/**고객의 정보를 이름, 포인트 관리
이름은 2자 이상
포인트는 최소 0점 이상
회원 가입하면 포인트는 5점이 자동 부여 됩니다.*/
public class Customer implements Serializable{
	private String name;
	private int point;	
	
	public Customer(String name) throws CustomerNameLengthException {
		setName(name);
		setPoint(5);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) throws CustomerNameLengthException {
		if(name==null || name.length()<2)
			throw new CustomerNameLengthException(name);
		this.name = name;
	}
	public int getPoint() {
		return point;
	}
	public void setPoint(int point) {
		this.point = point;
	}
	@Override
	public String toString() {
		return name+":"+point;
	}
	
	
}
