package com.ssafy.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class SimpleServer {
	public SimpleServer() {
		//1서버는 대기해야 한다.
		try {
			ServerSocket server=new ServerSocket(8888);
			while(true) {
				System.out.println("1서버 준비중");
				Socket socket=server.accept(); //3
				System.out.println(socket.getInetAddress().toString()+" 접속 확인 ㅇㅋ");
				//server-->client 환영 메세지 
				ObjectOutputStream oos=new ObjectOutputStream(
						socket.getOutputStream());
				oos.writeObject("저희 서버에 어서오세요!!!!");
				oos.flush();
				//oos.close();//
				
				ObjectInputStream ois=new ObjectInputStream(socket.getInputStream());
//				while(true) {
//					System.out.println(ois.readObject());
//				}
			}//while
		} catch (IOException e) {			
			e.printStackTrace();
		}
//		catch(ClassNotFoundException e) {
//			e.printStackTrace();
//		}
	}
	public static void main(String[] args) {
		new SimpleServer();
	}
}
