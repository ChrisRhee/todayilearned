package com.ssafy.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import com.ssafy.user.Customer;
import com.ssafy.user.CustomerNameLengthException;

public class SimpleClient {
	public SimpleClient() {
		//2
		try {
			Socket socket = new Socket("127.0.0.1", 8888);
			System.out.println("server ok..");
			ObjectInputStream ois=new ObjectInputStream(
					socket.getInputStream());
			System.out.println(ois.readObject());
			
			//client--> server  N개
			new Thread() {
				public void run() {
					try {
					ObjectOutputStream oos=new ObjectOutputStream(
							socket.getOutputStream());
					oos.writeObject(new Customer("홍길동"));
					oos.flush();
					//oos.close();//
					}catch(IOException e) {
						e.printStackTrace();
					}catch(CustomerNameLengthException e) {
						e.printStackTrace();
					}
				};
			}.start();
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(ClassNotFoundException e) {
			
		}
	}
	public static void main(String[] args) {
		new SimpleClient();
	}
}
