package com.ssafy.awt;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

// abstract window toolkit
public class TextTest {
	private Frame f;
	private TextArea ta;
	private TextField tf;
	
	public TextTest() {
		f= new Frame("chris");
		ta = new TextArea();
		tf = new TextField();
	}
	
	private void display() {
		f.setSize(300, 200);
		
		ta.setBackground(Color.GREEN);
		tf.setForeground(Color.RED);
		
		f.add(ta,BorderLayout.CENTER);
		f.add(tf,BorderLayout.SOUTH);
		
		ta.setFocusable(false);
		
		f.setVisible(true);
	}
	
	private void addEvent() {
		tf.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TextField tf = (TextField) e.getSource();
				String string = tf.getText();
				ta.append(string+"\n");
				tf.setText("");
			}
		});
		
		
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				f.dispose();
			}
		});
	}
	
	public static void main(String[] args) {
		TextTest t = new TextTest();
		t.display();
		t.addEvent();
	}
}
