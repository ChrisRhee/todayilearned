package com.ssafy.awt;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class YourHandler implements ActionListener{
	@Override
	public void actionPerformed(ActionEvent e) {
		//System.out.println("actionPerformed called");
		String s = e.getActionCommand();
		System.out.println(s);
	}
}

// abstract window toolkit
public class EventTest1 {
	private Frame f;
	private Panel p;
	private Button b;
	private Label l;
	

	public EventTest1() {
		p= new Panel();
		f= new Frame("chris");
		b= new Button("확인OK");
		l= new Label("라벨이당");
	}
	
	private void display() {
		f.setSize(300, 200);
		
		b.setForeground(Color.RED);
		l.setBackground(Color.GREEN);
		
		p.setLayout(new BorderLayout());
		p.add(b,BorderLayout.SOUTH);
		p.add(l,BorderLayout.CENTER);
		//f.add(b,BorderLayout.SOUTH);
		//f.add(l,BorderLayout.CENTER);
		f.add(p);
		f.setVisible(true);
	}
	
	private void addEvent() {
		ActionListener al = new YourHandler();
		b.addActionListener(al);
		f.addWindowListener(new WindowAdapter() { 
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				f.dispose();
			}
		});
		
	}
	
	public static void main(String[] args) {
		EventTest1 t = new EventTest1();
		t.display();
		t.addEvent();
	}
}
