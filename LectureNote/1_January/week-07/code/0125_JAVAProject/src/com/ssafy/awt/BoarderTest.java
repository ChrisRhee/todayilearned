package com.ssafy.awt;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

// abstract window toolkit
public class BoarderTest {
	private Frame f;
	private Button bn,bw,be,bs,bc;
	
	public BoarderTest() {
		f= new Frame("chris");
		bn= new Button("North");
		be= new Button("East");
		bs= new Button("South");
		bw= new Button("West");
		bc= new Button("Center");
	}
	
	private void display() {
		f.setSize(300, 200);
		f.setLayout(new BorderLayout(20,20));
		f.add(bs,BorderLayout.SOUTH);
		f.add(be,BorderLayout.EAST);
		f.add(bw,BorderLayout.WEST);
		f.add(bn,BorderLayout.NORTH);
		f.add(bc,BorderLayout.CENTER);
		f.setVisible(true);
	}
	
	private void addEvent() {
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				f.dispose();
			}
		});
	}
	
	public static void main(String[] args) {
		BoarderTest t = new BoarderTest();
		t.display();
		t.addEvent();
	}
}
