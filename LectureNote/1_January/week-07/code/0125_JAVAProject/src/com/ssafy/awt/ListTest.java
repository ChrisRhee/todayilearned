package com.ssafy.awt;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.List;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

// ListTest
public class ListTest {
	private Frame f;
	private Button b;
	private List list;
	
	private Panel p,pp;
	private Label namel;
	private TextField nameTf;
	private Label agel;
	private TextField ageTf;
	private Label addrl;
	private TextField addrTf;
	
	private Button registerB;
	
	
	public ListTest() {
		f= new Frame("chris");
		b= new Button("확인");
		list= new List();
		
		p = new Panel();
		pp = new Panel();
		registerB = new Button("등록");
		namel = new Label("이름",Label.CENTER);
		nameTf = new TextField();
		agel = new Label("나이",Label.CENTER);
		ageTf = new TextField();
		addrl = new Label("주소",Label.CENTER);
		addrTf = new TextField();
	}
	
	private void display() {
		f.setSize(300, 200);
		f.add(b,BorderLayout.NORTH);
		f.add(list,BorderLayout.CENTER);
		
		p.setLayout(new GridLayout(3,2));
		p.add(namel);
		p.add(nameTf);
		p.add(agel);
		p.add(ageTf);
		p.add(addrl);
		p.add(addrTf);
		
		pp.setLayout(new BorderLayout());
		pp.add(p, BorderLayout.CENTER);
		pp.add(registerB, BorderLayout.SOUTH);
		f.add(pp,BorderLayout.SOUTH);
		
		f.setVisible(true);
	}
	
	static int i = 0;
	private void addEvent() {
		b.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String s ="이찬호"+i+" "+(20+i)+ " 분당"+i;
				list.add(s);
				i++;
			}
		});
		
		list.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				String s = list.getSelectedItem();
				String[] sa = s.split(" ");
				nameTf.setText(sa[0]);
				ageTf.setText(sa[1]);
				addrTf.setText(sa[2]);
			}
		});
		
		
		registerB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String name = nameTf.getText();
				int age = Integer.parseInt(ageTf.getText());
				String address = addrTf.getText();
				list.add(name+" "+age+" "+address);
				nameTf.setText("");
				ageTf.setText("");
				addrTf.setText("");
			}
		});
		
		
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				
				f.dispose();
			}
		});
	}
	
	public static void main(String[] args) {
		ListTest t = new ListTest();
		t.display();
		t.addEvent();
	}
}
