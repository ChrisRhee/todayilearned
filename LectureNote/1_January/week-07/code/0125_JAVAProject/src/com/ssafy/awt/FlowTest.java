package com.ssafy.awt;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

// abstract window toolkit
public class FlowTest {
	private Frame f;
	private Button bn,bw,be,bs,bc;
	
	public FlowTest() {
		f= new Frame("chris");
		bn= new Button("1");
		be= new Button("2");
		bs= new Button("3");
		bw= new Button("4");
		bc= new Button("5");
	}
	
	private void display() {
		f.setSize(300, 200);
		f.setLayout(new FlowLayout());
		f.add(bn);
		f.add(be);
		f.add(bs);
		f.add(bw);
		f.add(bc);
		
		f.setVisible(true);
	}
	
	private void addEvent() {
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				f.dispose();
			}
		});
	}
	
	public static void main(String[] args) {
		FlowTest t = new FlowTest();
		t.display();
		t.addEvent();
	}
}
