package com.ssafy.awt;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

// Event handling 1 : this
public class EventTest2 implements ActionListener{
	private Frame f;
	private Panel p;
	private Button b;
	private Label l;
	

	public EventTest2() {
		p= new Panel();
		f= new Frame("chris");
		b= new Button("확인OK");
		l= new Label("라벨이당");
	}
	
	private void display() {
		f.setSize(300, 200);
		
		b.setForeground(Color.RED);
		l.setBackground(Color.GREEN);
		
		p.setLayout(new BorderLayout());
		p.add(b,BorderLayout.SOUTH);
		p.add(l,BorderLayout.CENTER);
		//f.add(b,BorderLayout.SOUTH);
		//f.add(l,BorderLayout.CENTER);
		f.add(p);
		f.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		//System.out.println("actionPerformed called");
		String s = e.getActionCommand();
		System.out.println(s);
		l.setText("Button Clicked "+ i++ +" times!");
	}
	
	static int i =0;
	
	private void addEvent() {
		b.addActionListener(this);
		f.addWindowListener(new WindowAdapter() { 
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				f.dispose();
			}
		});
		
	}
	
	public static void main(String[] args) {
		EventTest2 t = new EventTest2();
		t.display();
		t.addEvent();
	}

	
}
