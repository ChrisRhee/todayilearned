package com.ssafy.awt;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

// abstract window toolkit
public class AWTTest {
	private Frame f;
	private Button b;
	private Label l;
	
	public AWTTest() {
		f= new Frame("chris");
		b= new Button("확인");
		l= new Label("라벨이당");
	}
	
	private void display() {
		f.setSize(300, 200);
		f.add(b,BorderLayout.SOUTH);
		f.add(l,BorderLayout.CENTER);
		f.setVisible(true);
	}
	
	private void addEvent() {
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				f.dispose();
			}
		});
	}
	
	public static void main(String[] args) {
		AWTTest t = new AWTTest();
		t.display();
		t.addEvent();
	}
}
