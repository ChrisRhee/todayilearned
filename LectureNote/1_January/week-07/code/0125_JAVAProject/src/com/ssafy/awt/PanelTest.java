package com.ssafy.awt;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

// abstract window toolkit
public class PanelTest {
	private Frame f;
	private Panel p;
	private Button b;
	private Label l;
	
	public PanelTest() {
		p= new Panel();
		f= new Frame("chris");
		b= new Button("확인");
		l= new Label("라벨이당");
	}
	
	private void display() {
		f.setSize(300, 200);
		
		b.setForeground(Color.RED);
		l.setBackground(Color.GREEN);
		
		p.setLayout(new BorderLayout());
		p.add(b,BorderLayout.SOUTH);
		p.add(l,BorderLayout.CENTER);
		//f.add(b,BorderLayout.SOUTH);
		//f.add(l,BorderLayout.CENTER);
		f.add(p);
		f.setVisible(true);
	}
	
	private void addEvent() {
		f.addWindowListener(new WindowAdapter() { 
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				f.dispose();
			}
		});
	}
	
	public static void main(String[] args) {
		PanelTest t = new PanelTest();
		t.display();
		t.addEvent();
	}
}
