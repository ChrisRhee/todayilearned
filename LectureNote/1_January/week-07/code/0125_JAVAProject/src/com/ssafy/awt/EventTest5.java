package com.ssafy.awt;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

// Event handling 3 : 어노니머스로 만든걸 그냥 바로 집어넣음
public class EventTest5 {
	private Frame f;
	private Panel p;
	private Button b;
	private Label l;
	

	public EventTest5() {
		p= new Panel();
		f= new Frame("chris");
		b= new Button("확인OK");
		l= new Label("라벨이당");
	}
	
	private void display() {
		f.setSize(300, 200);
		
		b.setForeground(Color.RED);
		l.setBackground(Color.GREEN);
		
		p.setLayout(new BorderLayout());
		p.add(b,BorderLayout.SOUTH);
		p.add(l,BorderLayout.CENTER);
		//f.add(b,BorderLayout.SOUTH);
		//f.add(l,BorderLayout.CENTER);
		f.add(p);
		f.setVisible(true);
	}
	
	
	
	static int i=0;
	private void addEvent() {
		// anonymous class 를 아예 집어넣음
		b.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				//System.out.println("actionPerformed called");
				String s = e.getActionCommand();
				System.out.println(s);
				l.setText("hello"+i++);
			}
		});
		
		f.addWindowListener(new WindowAdapter() { 
			@Override
			public void windowClosing(WindowEvent e) {
				f.dispose();
			}
		});
		
	}
	
	public static void main(String[] args) {
		EventTest5 t = new EventTest5();
		t.display();
		t.addEvent();
	}
}
