package com.ssafy.awt;

import java.awt.Button;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

// abstract window toolkit
public class GridTest {
	private Frame f;
	private Button bn,bw,be,bs,bc;
	
	public GridTest() {
		f= new Frame("chris");
		bn= new Button("1");
		be= new Button("2");
		bs= new Button("3");
		bw= new Button("4");
		bc= new Button("5");
	}
	
	private void display() {
		f.setSize(300, 200);
		f.setLayout(new GridLayout(3,2));
		f.add(bn);
		f.add(be);
		f.add(bs);
		f.add(bw);
		//f.add(bc);
		
		f.setVisible(true);
	}
	
	private void addEvent() {
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				f.dispose();
			}
		});
	}
	
	public static void main(String[] args) {
		GridTest t = new GridTest();
		t.display();
		t.addEvent();
	}
}
