package com.ssafy.awt;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

// Event handling 2 : 클래스를 아예 넣음
public class EventTest3 {
	private Frame f;
	private Panel p;
	private Button b;
	private Label l;
	

	public EventTest3() {
		p= new Panel();
		f= new Frame("chris");
		b= new Button("확인OK");
		l= new Label("라벨이당");
	}
	
	private void display() {
		f.setSize(300, 200);
		
		b.setForeground(Color.RED);
		l.setBackground(Color.GREEN);
		
		p.setLayout(new BorderLayout());
		p.add(b,BorderLayout.SOUTH);
		p.add(l,BorderLayout.CENTER);
		//f.add(b,BorderLayout.SOUTH);
		//f.add(l,BorderLayout.CENTER);
		f.add(p);
		f.setVisible(true);
	}
	
	class ButtonHandler implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			//System.out.println("actionPerformed called");
			String s = e.getActionCommand();
			System.out.println(s);
			l.setText("hello"+i++);
		}
	}
	static int i=0;
	private void addEvent() {
		ActionListener al = new ButtonHandler();
		b.addActionListener(al);
		f.addWindowListener(new WindowAdapter() { 
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				f.dispose();
			}
		});
		
	}
	
	public static void main(String[] args) {
		EventTest3 t = new EventTest3();
		t.display();
		t.addEvent();
	}
}
