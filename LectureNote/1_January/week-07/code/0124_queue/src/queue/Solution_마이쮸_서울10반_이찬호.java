package queue;

import java.util.LinkedList;
import java.util.Queue;
class Person{
	int name;
	int mNum;
	public Person(int name) {
		this.name = name;
		this.mNum = 1;
	}
}
class Solution_마이쮸_서울10반_이찬호{
	public static void main(String[] args) {
		Queue<Person> q = new LinkedList<>();
		int count = 1;
		Person p1 = new Person(count);
		int mai = 20;
		// 일단 하나 넣음
		q.offer(p1);
		while(mai>0) {
			// 기존에 있던애 앞에서 나옴
			Person t = q.poll();	
			// 마이쮸에서 내 숫자만큼 뺌
			mai -= t.mNum;
			if(mai <= 0 ) {
				System.out.println("범인은 " +t.name);
				break;
			}
			System.out.println(mai);
			// 마이쮸 하나 더 받아야하고
			t.mNum++;
			// 뒤에 새로 줄섬
			q.offer(t);
			// 카운트 증가
			count++;
			
			// 새로운 친구 입장
			Person t2 = new Person(count);
			// 새친구 큐에 넣기
			q.offer(t2);
		}
		
	}
	
}