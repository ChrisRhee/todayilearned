package queue;

import java.util.PriorityQueue;
import java.util.Queue;

class Node{
	int data;
	Node link;
}

public class queueLinked {
	public static Node front = null;
	public static Node rear = null;
	
	public static boolean isEmpty() {
		if(front == null) return true;
		else return false;
	}
	
	public static void enqueue(int item) {
		Node end = new Node();
		end.data=item;
		if(isEmpty()) {
			front=end;
			rear=end;
		}else {
			rear.link = end;
			rear= end;
		}
	}
	
	public static int dequeue() {
		if(isEmpty()) {
			System.out.println("EMPTY");
			return -1;
		}
		int data= front.data;
		front = front.link;
		if(front == null) rear = null;
		return data;
	}
	
	public static int qpeek() {
		if(isEmpty()) {
			System.out.println("EMPTY");
			return -1;
		}
		return front.data;
	}
	
	public static void main(String[] args) {
		// 자바의 API
		enqueue(3);
		enqueue(2);
		enqueue(1);
		System.out.println(qpeek());
		
		Queue<Integer> pq = new PriorityQueue<>();
		pq.offer(3);
		pq.offer(2);
		pq.offer(1);
		pq.offer(4);
		pq.offer(10);
		pq.offer(40);
		pq.offer(43);
		pq.offer(14);
		System.out.println(pq);
		
		
	}
}
