package queue;

public class queueCircular {
	public static int [] queue = new int [10];
	public static int n = queue.length;
	public static int front = 0;
	public static int rear = 0;
	
	public static boolean isEmpty() {
		if( front == rear ) return true;
		else return false;
	}
	
	public static boolean isFull() {
		if( (1+rear)%n == front ) return true;
		else return false;
	}
	
	public static void enqueue(int item) {
		if( isFull() ) {
			System.out.println("FULL");
			return;
		}
		queue[(++rear)%n] = item;
	}
	public static int dequeue() {
		if(isEmpty()) {
			System.out.println("EMPTY");
			return -1;
		}
		return queue[(++front)%n];
	}
	
	public static int qpeek() {
		if(isEmpty()) {
			System.out.println("EMPTY");
			return -1;
		}
		return queue[(1+front)%n];
	}
	
	public static void main(String[] args) {
		// 자바의 API
		enqueue(1);
		enqueue(2);
		enqueue(3);
		enqueue(3);
		enqueue(3);
		enqueue(3);
		enqueue(3);
		enqueue(3);
		enqueue(3);
		
		System.out.println(qpeek());
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		enqueue(3);
		enqueue(3);
		enqueue(3);
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
	}
}
