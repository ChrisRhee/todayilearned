//checkedException = VM checks Exceptions without 안전장치
// 장점: 오류 없는 코드인데, checked 라서 무조건 안전장치를 하게 해서 오류가 절대로 안남.
// 단점: 귀찮음 ㅋ 

public class Exception_Test06 {
	public static void main(String[] args) {
		
		try {
			Class.forName("java.lang.String");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			System.out.println("try or catch 했던 무적권 마지막 출력");
		}
		
		System.out.println("Program End");
	}
}