/** exception은 프로그램 실행중에만 확인이 가능
 * 
 * */
public class Exception_Test03 {
	public static void main(String[] args) {
		char [] sizes={'M','L'};
		for( char c:sizes) {
			System.out.println(c);
		}
		
		// for문 내에 오류되는 것만 빼고 나머지는 다 출력해보기
		for (int i = -1; i < 3; i++) {
			try {
				System.out.println(sizes[i]);
			}catch(ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
			}
		}
		
		// for문에서 에러가 나면 그냥 포문 빠이 하기
		try {
			for (int i = -1; i < 3; i++) {
				System.out.println(sizes[i]);
			}
		}
		catch(ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
		}
		
		System.out.println("end");
	}
}