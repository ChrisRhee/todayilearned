import java.util.Scanner;

/** nextInt() 같은 메소드로 정의되면 익셉션이 발생할 것에 대해서 주의를 주는것을 추천함
 * 
 * */
public class Exception_Test05 {
	public static void main(String[] args) {
		System.out.println("please enter the number");
		try {
			int input = new Scanner(System.in).nextInt();
			System.out.println(input);
		}catch(RuntimeException e) {
			e.printStackTrace();
		}
		
		
		System.out.println("Program End");
	}
}