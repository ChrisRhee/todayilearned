import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ObjectIOTest {
	public static void main(String[] args) throws IOException, CustomerNameLengthException, ClassNotFoundException {
		ObjectOutputStream obj = new ObjectOutputStream(new FileOutputStream("customer.txt"));
		obj.writeObject(new Customer_teacher("이찬호"));
		obj.flush();
		obj.close();
		
		ObjectInputStream ibj = new ObjectInputStream(new FileInputStream("customer.txt"));
		Customer_teacher c = (Customer_teacher)ibj.readObject();
		System.out.println(c);
	}
}
