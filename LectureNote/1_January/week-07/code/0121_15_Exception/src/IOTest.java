import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class IOTest {
	public static void main(String[] args) throws IOException {
		/** 
		 *  화면 출력
		 */
		System.out.println("한글123English");
		// 키보드 입력 받기
		InputStream is = System.in;
		// byte -> char
		InputStreamReader isr = new InputStreamReader(is);
		int input = isr.read();
		System.out.println(input+" "+(char)(input+1));
	}
}
