
public class Customer {
	private String name;
	private int point;
	
	public Customer(String name, int point) {
		setName(name);
		setPoint(point);
	}
	
	public Customer(String name) {
		setName(name);
		setPoint(5);
	}
	
	public Customer() {
		this("", 5);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		if(name.length() <2) System.out.println("이름은 최소 2글자 이상이어야 합니다!!! 다시 ㄱㄱ");
		else this.name = name;
	}
	public int getPoint() {
		return point;
	}
	
	public void setPoint(int point) {
		if(point <0) System.out.println("포인트는 0보다 커야합니다! 아무튼 0 이상임");
		else this.point = point;	
	}
}
