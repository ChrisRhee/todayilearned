/** 사용자 정의 예외 클래스 규칙
 * 1. 이름이 OOOOOException 이런식이다 
 * 2. CheckedException 이면 Exception을 상속받으면 되고
 * 3. UnCheckedException 이면 RuntimeException을 상속받으면 된다.
 * 4. eclipse는 기본으로 Checked를 쓴다.
 * 5. 자바 교재에서는 기초일때는 Checked를 권장하는데, 뒤로 가면 UnChecked를 권장한다 ㅋ
 * 6. 생성자를 부모로 전달하는 애만 있으면 된다. super()
 *
 */
public class CustomerNameLengthException extends Exception {

	public CustomerNameLengthException(String message) {
		super(message);
	}
}
