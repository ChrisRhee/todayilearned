import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileIOTest {
	public static void main(String[] args) throws IOException {
		FileInputStream fi = new FileInputStream("out.bat");
		int input = fi.read();
		System.out.println(input);
		
		FileOutputStream out = new FileOutputStream("out.bat");
		out.write(80);
		out.flush();
		out.close(); // close 직전에 flush를 한다.
		
	}
}
