package SWExpertAcademy;
import java.util.Scanner;
import java.io.FileInputStream;
class Solution1217_거듭제곱_서울10반_이찬호{
	
	public static int power(int base, int exponent) {
		if(exponent == 0) return 0;
		if(exponent == 1) return base;
		if((exponent & 1) ==0) {
			int newbase = power(base, exponent/2);
			return newbase * newbase;
		}else {
			int newbase = power(base, (exponent-1)/2);
			return newbase * newbase * base;
		}
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1217.txt"));
		Scanner sc = new Scanner(System.in);
		for(int test_case = 1; test_case <= 10; test_case++)
		{
			sc.nextInt();
			System.out.println("#"+test_case + " "+power(sc.nextInt(),sc.nextInt()));
		}
		sc.close();
	}
}