package com.ssafy.UserIO;

public class UserExitException extends Exception{
	
	public UserExitException(String message) {
		super(message);
	}
}
