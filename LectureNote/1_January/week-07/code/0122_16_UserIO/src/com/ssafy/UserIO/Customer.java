package com.ssafy.UserIO;
import java.io.Serializable;

public class Customer implements Serializable{
	private String name;
	private int point;
	
	
	public Customer(String name) throws CustomerNameLengthException {
		setName(name);
		setPoint(5);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) throws CustomerNameLengthException {
		if(name.length()<2 || name == null) {
			throw new CustomerNameLengthException(name);
		}
		this.name = name;
	}
	public int getPoint() {
		return point;
	}
	public void setPoint(int point) {
		this.point = point;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(name);
		builder.append("|");
		builder.append(point);
		return builder.toString();
	}

	
}
