package com.ssafy.UserIO;

import java.util.Collection;

public interface UserMgr {
	// User Add
	void add(Customer c) throws CustomerNameLengthException;
	
	// User Search
	Collection<Customer> getCustomers();
	
	// Program Exit
	void close();
}
