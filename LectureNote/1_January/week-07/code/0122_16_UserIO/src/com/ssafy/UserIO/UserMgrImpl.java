package com.ssafy.UserIO;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.text.StyledEditorKit.ForegroundAction;

public class UserMgrImpl implements UserMgr{
	private Collection<Customer> list;
	
	// Constructor
	private static UserMgrImpl instance ;
	private UserMgrImpl() {
		list = new ArrayList<>();
		try {
			//파일 읽기 잘해라
			ObjectInputStream ios = new ObjectInputStream(new FileInputStream("customer.txt"));
			while(true) {
				Customer c = (Customer)ios.readObject();
				list.add(c);
				
			}
		}
		catch (IOException e) {
			System.out.println("신규고객 작성");
		} catch(ClassNotFoundException e) {
			
		}
		
	}
	public static UserMgrImpl getInstance() {
		instance = new UserMgrImpl();
		return instance ;
	}

	@Override
	public void add(Customer c) throws CustomerNameLengthException {
		list.add(c);
	}

	@Override
	public Collection<Customer> getCustomers() {
		return list;
	}

	@Override
	public void close() { // 상속받은 예외가 있을때만
		// close() saves data in memory to file before closed 
		// 메모리에 있는 데이터를 죽기 전에 file에 저장
		try {
			sendServer();
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("customer.txt"));
			for (Customer customer : list) {
				if(customer !=null) {
					oos.writeObject(customer);
					oos.flush();
				}
			}
			oos.close();
			throw new RuntimeException("정상종료");
		}
		catch(Exception e) {
			//e.printStackTrace();
		}
	}
	private void sendServer() throws IOException {
		new Thread() {
			public void run() {
				Socket socket;
				try {
					socket = new Socket("127.0.0.1",8888);
					ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
					for (Customer customer : list) {
						if(customer != null) {
							oos.writeObject(customer);
							oos.flush();
						}
					}
					oos.close();
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			};
		}.start();
	}
	
}
