package com.ssafy.Thread;

class UserRunnable implements Runnable{
	private int count;
	@Override
	public void run() { // share the Object needs MultiThread
		while(true) {
			if(count >= 50) break;
			print();
		}
	}
	
	private void print() {
		System.out.println(++count+" "+Thread.currentThread());
		
	}
}
public class UserRunnableTest {
		public static void main(String[] args) {
			
			System.out.println(Thread.currentThread());
			UserRunnable u1 = new UserRunnable();
			//u1.run();
			
			Thread t1 = new Thread(u1);
			Thread t2 = new Thread(u1);
			Thread t3 = new Thread(u1);
			
			t1.start();
			t2.start();
			t3.start();
		}
}
