package com.ssafy.UserIO;
public class CustomerNameLengthException extends Exception {

	public CustomerNameLengthException(String message) {
		super(message);
	}
}
