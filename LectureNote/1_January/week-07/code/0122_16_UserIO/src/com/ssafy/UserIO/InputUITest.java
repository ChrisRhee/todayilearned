package com.ssafy.UserIO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

public class InputUITest {
	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in); // 1.5버전
		String s = sc.nextLine(); // 5.0버전
		
		// byte만 받을 수 있었음
		//InputStream is = System.in; // 1.0버전
		// 많이받으려면? byte -> char
		//InputStreamReader isr = new InputStreamReader(is);
		// char -> String
		//BufferedReader br = new BufferedReader(isr);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s2 = br.readLine();
		System.out.println(s);
		System.out.println(s2);
	}
}
