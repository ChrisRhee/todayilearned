package com.ssafy.UserIO;

import java.util.Scanner;

public class UserUI {
	private Scanner sc;
	private UserMgr mgr;
	public UserUI() throws UserExitException {
		mgr = UserMgrImpl.getInstance();
		sc = new Scanner(System.in);
		startUI();
	}
	
	private void startUI() throws UserExitException {
		while(true) {
			//String menuFlag = menuMessage("menu(1입력, 2출력, 3종료)");
			//String menuFlag = inputData("menu(1입력, 2출력, 3종료)");
			//menuCheck(menuFlag);
			try {
				menuCheck(inputData("menu(1입력, 2출력, 3종료)"));
			} catch (CustomerNameLengthException e) {
				System.out.println("이름은 1글자 이상이어야 합니다. " + e.getMessage());
			}
		}
	}

	private void menuCheck(String menu) throws CustomerNameLengthException, UserExitException {
		switch(menu) {
		case "1":
			// add만 멈출까? menu를 새롭게 돌릴까?
			add();
			break;
		case "2":
			print();
			break;
		case "3":
			close();
			break;
		default:
			System.out.println("메뉴는 1~3까지만 가능");
		}
	}

	// throws는 UI에서 한다.
	private void add() throws CustomerNameLengthException {
		String name = inputData("이름");
		Customer c = new Customer(name);
		mgr.add(c);
	}

	private void print() {
		System.out.println(mgr.getCustomers());
	}

	private void close() throws UserExitException {
		try {
			mgr.close();
		}
		catch(RuntimeException e) {
			//throw new UserExitException(e.getMessage());
		} finally {
			//throw new UserExitException("정상종료");
		}
		System.exit(0);
	}

	private String inputData(String title) {
		System.out.print(title+": ");
		
		return sc.nextLine();
	}

	public static void main(String[] args) {
		try {
			new UserUI();
		} catch (UserExitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("end");
	}
}