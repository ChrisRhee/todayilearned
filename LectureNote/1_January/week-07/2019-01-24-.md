***
contents : Java, Algorithms, 

date : 2019 01 23 Thu
***

# 박상진 교수님
정올, 백준
- 파일명 : Main문제번호_문제이름_서울10반_이찬호.java

sw expert
- 파일명 : Solution문제번호_문제이름_서울10반_이찬호.java

## Markdown 문법
### 이미지 넣기
![](./img/0114/.png)

----------------------------------------------------------
# 수업
## Queue
- 

## taggin interface
- Serializebal , Cloneable 과같이 아무런 메소드 없이 interface로 선언된 애들이 있다. 이 애들은 기능이 아니라 이 문자 자체로 클래스의 성질을 **표시**만 해주는 인터페이스이다. 

## BufferdReader
- 버퍼를 사용하면 파일 입출력 할때 속도가 매우 빠르다. 그래서 new InputStreamReader로 읽어올때 꼭 BufferedReader로 부르는게 좋다.
```java
BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileInputStream("out.txt"))));
ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("file.txt")));
```
- stream : byte를
- output : 출력
- input : 입력
- Buffered : 버퍼해서
- Writer : 문자로 쓴다.
- Reader : 문자를 읽는다
----------------------------------------------------------
# 수업 외 꿀팁


----------------------------------------------------------
# 공지사항
- 다음주 월요일 재시험 
    - 시간 : 7:50~8:50
    - 지각하면 
- 다음주 금요일 월말 평가
    - JAVA 만 집중해서 진행
    - 2시간
    - 실기 평가
    - 금요프로젝트처럼 스펙을 주고 작성해서 제출. 
- 보충수업 월,수,금
    - 강의장 하나를 아침에 빌린다. 
    - 시간 : 7:45~8:45
    - 보충수업이 진행되면 해당 반에 그때까지 못들어옴 ㅠㅠ 
    흑흑흑흐그
