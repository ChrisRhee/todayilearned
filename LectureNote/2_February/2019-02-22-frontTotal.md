***
contents : 
date : 2019 02 22
***

## 전혜영 교수님
* Email : OOPSW@tistory.com
* 서버 : http://70.12.109.160:8888
* 숙제 : 이름.zip 
* 질문하는 방법
    - 코딩은 에러메세지를 찍어서 보내라
    - 수업자료는 수업 끝나고 배포가 된다. 
    - 배포된 자료는 나만 볼 수 있는 곳에 올려놓고 보세요.
    - 이해가 안되는 부분은 이 부분이 왜 이해가 안되는지를 설명해서 보내자
- 질문에 대한 반응
    1. 다음주에 할거야. 잘하고있음
    2. main말고 가지 질문인 경우가 있따. 필요는한데, main으로 중요한건 아닌경우
    3. main은 여기있는데 완전 저기 바깥쪽에서 질문하는 사람이있따.

# 박상진 교수님
정올, 백준
- 파일명 : Main문제번호_문제이름_서울10반_이찬호.java

sw expert
- 파일명 : Solution문제번호_문제이름_서울10반_이찬호.java


## Markdown 문법
### 이미지 넣기
![](./img/0114/.png)

----------------------------------------------------------
# 수업
- ㅎㅇㅎㅇㅎㅇㅎㅇ 
- ㅎㅇㅎㅇㅎ


## HTML
- M : model = HTML로 한다. 
    - object document model = DOM
    - 화면하고는 관계없이 데이터 중심으로 만들어라 !
    - 이것이 HTML5의 기본 사상임.
- V : View = CSS
    - 디자인
- C : Control = JavaScript, Jquery
    - 뒤에서 작동하는 모든 로직
    - Jquery는 JS를 쉽게 쓸 수 있게 만들어주는 FrameWork이다. 진짜 편하긴함. 이해하기가 어렵지 ㅋ.ㅋ
    - 지금은 Jquery보다 그거보다 더 편한게 또 나오고있다. 

- 이렇게 3단으로 분리를 시켜서 문제가 발생하는 곳만 수정할 수 있도록 분리하는 것이다. 

### HTML5에서 새롭게 추가된 태그들
- Semantic elements
    - ![](./img/0222_html5_sementic.png)
    ```html
    <header>
    <nav>
    <section>
        //이것저것 많을 때 쓴다.
    <article>
       //글들이 많을 때 쓴다.
    <aside>
    <footer>
    ```
    - 설명 : 이 시맨틱들은 사실 ```<div id="header"> <div id="footer">``` 이렇게 써도 된다. 근데 이거보다 더 명확하게 ```<header>```로 묶어서 활용하면 개발, 디자인 하는 입장에서 명확하게 활용 할 수 있다. 그래서 div 에 id를 주는 것보다 HTML5에서는 저 시멘틱 태그를 사용하는 것을 **권장** 한다. 짠다면 저렇게 짜는 습관을 들이자.

- Attributes of **form** elements 

    - number
    <input type="number">
    - date
    <input type="date">
    - time
    <input type="time">
    - calender
    <input type="date">

- Graph elements
    - <svg>
    - <canvas>
- Multimedia elements
    - <audio>
        - 오디오 재생하는 태그
        - controls 에서 소스를 가져오거나 다른 작업들을 할 수 있다.
    ```html
    <audio controls>
        <source src="horse.ogg" type="audio/ogg">
        <source src="horse.mp3" type="audio/mpeg">
    </audio>
    ```
    - <video>  
        - 비디오를 재생하는 태그
    ```html
    <video width="400" controls>
        <source src="mov_bbb.mp4" type="video/mp4">
    </video>
    ```
    - 이 태그들도 뒤에 script를 통해서 width값이나 play(), pause() 등 의 동작을 수행 할 수 있다. 


- **html에 가장 중요한 것 : block level이 글자 취급이냐, 라인 취급이냐**
    - block level 
        - div, article, address, nav, main, h1~h6, ul, 등 이 속한다.
        - 한 블럭을 잡아서 새로운 라인에서 시작한다.
        - 이 블럭의 크키는 좌우로 갈 수 있는 화면의 최대치이다.
    - inline level
        - 새로운 라인에서 시작하지 않고, 문장 안에서 필요한 길이 만큼만 자리를 차지한다. **이런식으로** 전체가 아니라 중간에 적용 하는 것
        - span, a, b, script, var, q, br 등이 속한다. 
    - 이 두개 구분할 수 있어야 함.

## CSS
- html의 태그를 가져오려고 한다. 
    - **select하는 방식**
    - ID로 가져오기 : #
        - ID는 unique하게 사용되어야 한다. 그 page에서 해당 ID를 사용하는 태그가 **딱 하나**여야 함. 
    - Class 로 가져오기 : .
        - p, span, div의 클래스가 center로 같으면, css에서 .center로 스타일을 줄 시, p, span, div 모두에 적용된다. 그래서 p의 center에만 적용을 하고 싶으면
        ```css
        p.center{ color : red }
        ```
        이런식으로 그 태그의 클래스에만 따로 적용할 수 있다. 
    - element 태그명으로 가져오기 : 태그 이름으로 가져온다.

    - 우선순위 : 태그명 < class < id
        - 먼저 태그명으로 적용을 다 하고, class 명을 그 위에 적용을 하고, 마지막으로 id 태그로 최종 적용을 한다. 따라서 id로 준 css는 확실하게 보여질 수 있다. 
 
- css밑에 속성들은 너무 많으니까 적어도 수업시간에 가장 많이 언급했던것들을 기억해야한다.
    - color : 글씨 바꾸기
    - backgound-color : 배경색 바꾸기
    - float : 가로로 정렬하기 

- 화면에 위치를 잡을때 
    - 디폴트 = static, 
    - absolue(부모르르 기준으로 절대좌표), 
    - relateve(상대경로), 
    - fixed(view 파인더에 딱 고정을 시킨다.)
    - 각 포지션의 역할을 알아야한다.
    - float 속성도 알아야한다.

- css 보이기 문법
    ```css
    p { visibility: visible; }
    p.invis { visibility: hidden;}
    ```
    - visiblility는 블럭 or line으로 잡힌 공간은 그대로 유지하되, 안에 있는 내용만 보이지 않는다. 

    ```css
    p.display1 { display : none; }
    p.display2 { display : inline; }
    p.display3 { display : block; }
    p.display4 { display : inline-block; }
    ```
    - display는 해당 태그가 가진 영역 자체를 안보이게 하거나, inline으로 잡아서 보이거나, block을 잡아서 보이는 지를 결정한다.

## JS
- 브라우저 오브젝트 모델, 스크립트, 윈도우 가  깊이는 몰라도 되는데, DOM의 반대말 BOM. DOM이 중요하기 때문에 BrowserOM에는 뭐가 있었는지 아는게 중요하다
    - location : 위치정보 
    - window.alert , window.confirm, window.dialog 
    - 이런 오브젝트가 있다 정도느 ㄴ알아야한다.
- DOM 스펙은 당연히 알아야 한다.
- 시험에는 관계ㅓㅅ는데 DOM 이벤트 handler 이게 나올진 모르겠는데
    - 이벤트 핸들링 할때는 앞쪽에서 먼저 쭉 이벤트 처리하고, 해당되는 타겟에서 이벤트 처리하고, 다하고 뒷단에서 또한다. 이렇게 Dom tree structure가 있고, 버튼이 있는 곳 위에 페어런트들이 있다. 버튼을 클릭하면 버튼에만 이벤트 전달되는게 아니고, dts의 parent에 오든 이벤트가 다 전달된다. 버튼 끝나고나서 백프로파 게이션에서 등록된 스크립트가 다 진행된다. 
- 자바는 앞에선 안되고, 해당 이벤트에서 되고, 백에서 된다. 
- 기본적으로 JS는 언어다. Hoxy

- 이벤트 핸들
- AddEventListner로 이벤트 등록해야하는데, 
```html
<body>
	<a href="link.html">A방식</a> <br>
	<a onclick="location.href='blink.html'">B방식</a><br>
	<a id="Ctype">C방식</a>
    <script type = "text/javascript">
        var a = document.querySelector("#Ctype");

        //재사용 불가능
        a.addEventListener("click", function(){
            loction.href = "https://www.naver.com";
        });

        //aclick이라는 함수는 따로 만들었으므로,
        var aclick = function(){
            loction.href = "https://www.naver.com";
        }

        // a에 click할 때 aclick을 실행하도록 이벤트 핸들링함.
        a.addEventListener("click",aclick);

```
- Onload
    - window에 있는 tag들을 다 읽고 나서 Onload에있는 script를 실행한다. 
    ```html 
    <head>
        <script>
            window.onload = function(){
                alert("process1");
            };
        </script>
    </head>
    <body>
        <h1>alert("process2");</h1>
    </body>
    ```
    - 이렇게하면 2를 먼저 하고, 1을 그 이후에 한다.

    - 같은 기능을 jquery에서는 이렇게 구현다.
     ```html 
    <head>
        <script>
            $(documnet).ready(function(){
                alert("process1");
            });
            // 이렇게 줄여서 사용한다.
            $(function(){
                alert("process1");
            });
        </script>
    </head>
    <body>
        <h1>alert("process2");</h1>
    </body>
    ```
    - 그럼 먼저 페이지에 있는 것들을 다 읽어놓고, 그 이후에 실행을 한다.
- OnClick 

## AJAX
- 비 동기적인 통신을 할수 있다.
- 웹페이지를 다 가져왔어야했는데, 이제는 필요한 부분만 서버랑 통신해서 그부분만 가져와서 refresh해서 필요한부분만 하니까 퍼포먼스가 살려냈다. 
- XMLHttpRequest .
    - **이것을 반드시 알아야한다.**
    - 비동기가 핵심이다. 
    - XML을 받아오는데 text기반의 html을 받아올 수 있다. 
    - **json 을 무조건 알아야한다.**
    - 이제는 json으로 주고 받겠다! 

- 처음에 웹페이지 한번 받고나서는 웹은 더이상 안받고 그 필요한 부분 정보만 받아온다.

## jquery
- js에서 이벤트 핸들링을 하려면 getElementById로 해당되는 태그를 찾아와야 그거로 뭔가 조작을 할 수 있다. 
- 근데 제이쿼리에서 이걸 엄청 쉽게 해준다. $('#') 이렇게 해서 Id를 바로 찾아올 수 있다. 
- 제이쿼리에서 태그를 생성 할 수 있다. 태그를 만들어서 append로 뒤에다 붙여서 넣을 수 있다. 
- $에 함수를 넣으면 실행을 해준다. 
- **$의 기본 사용법 다 알아야 한다.**
    - 함수 : 실행
    - css selector : 검색
    - html : 해당되는 태그 동적으로 새성
- 이벤트 핸들러 ready, click, On, 등등 다 알아야함. 
- this
    - 제이쿼리의 this는 해당되는 버튼, div태그, 이런식으로 쓰인다. this 활용법도 알아야한다.
    - foreach 구문을 알아야한다. 여기에서 this는 이벤트 핸들러에서의 this와 또 다르다.
- 제이쿼리도 css라는 명령어를 통해서 css를 입힐 수 있다. 이때 css적용 우선순위도 알고 있어야한다. 

## 저장소
- 로컬저장소 : 웹 브라우저를 삭제하지 않는이상 데이터를 영구적으로 저장
    - getItem,
    - setItem
    - removeItem
    - key(number)
    - clear()
    - map 계열로 사용하는 느낌이다. 
- 세션 저장소 : 웹 브라우저가 종료될 떄 까지만 데이터를 저장
- 두 저장소 모두 window 객체 안에 들어있음. 
```html  
<head>
    <script>
        $(document).ready(function(){
            if(window.localStrorage){
                //로컬 저장소를 사용합니다.
            }else{
                alert('로컬 저장소를 사용할 수 없는 브라우저입니다.);
            }
        });
    </script>
</head>
```
- 이렇게 해서 로칼 브라우저를 실험해보 수 있다. 



----------------------------------------------------------
# 수업 외 꿀팁
- 상태공간트리로 만드는거로 permtest 외우는걸 연습해라. 그럼 적용하기 더 쉬움 

----------------------------------------------------------
# 공지사항
- ㄴ으만ㄹ 아침에 잘하느 ㄴ사라뭊ㅇ 함분이 다음주 월화는 알고리즘 수업을 할 것이니다!
그리고 수요일에 목요일은 DP프로그램을 해야하는데 목요일에 시험을 친다. 
- 수, 목은 알고리즘 강사가 DP를 함
- 원래 목요일 시험은 뭐다? 월말평가를 만약에 평범하게 자바 진도를 나가고 치면 직접 영화예매 프로젝트를 구현해야한다. Jquery로 해라, 이부분은 js로 만들어라, 이부분은 css를 적용해라! 이런식으로 주어질 예정입니다.
- Ajax를 기본적으로 사용한다. DWR 기법이 있다.
- Direct Web Remoting : 자바스크립트에서 자바에서 띄울 수 있다. 
- 웹프론트는 우리가 작성해야한다. List페이지나 홈페이지
- 뒤쪽에 알고리즘 문제는 알고리즘 문제를 한시간에 풀어야해서 어렵진 않을텐데 우린 어렵다.
- 다 어려움 ㅠㅠㅠㅠ 어려우어여루어ㅓㅇ 휴ㅠㅠ
- 웹프론트에 페이지 하나 만들고!
    - 아니면 지금 워크샵 페이지를 주고 개편하거나
- 아니면 알고리즘을 관련있는 것을 할 것이기도 하다. 
 
- css나 이런것도 토씨하나 안틀리고 쓸 수 있게 해야한다.
- 자바스크립트나 xhr 대문자같은거 안틀리고 써야한다. 
- 목요일에는 월말평가에 처음부터 오늘했던 내용들을 시험양식이었다. 이걸 구현할듯 ㅋ.ㅋ
    - 제이쿼리를 이용해서 데이터를 입력 할 수있는 화면을 만들 수 있어야한다.
        - ex) 1~100까지 더하는 프로그램을 작성해라?
        - 1을 입력할 수 있는 화면을 만들고, 100을 입력하는 화면을 넣고, 이걸 실행하는 버튼으르 만들고, 이버튼을 누르면 1,100으르 가져오고 이 1,100을 자바스크립트로 구해도 되지만,
        - 이걸 함수 호출해서 자바??? Test.java가 있고, 여기에서 
```java
public class Test{
    public int execute(int s, int end){
        int sum = 0;
        return sum;
    }
}
```
- 이렇게 자바로 구현하면 result div 태그 밑으로 이 값을 집어넣는다.
- 웹에 제이쿼리를 이용해서 html에 기본적으로 값 가져오고, 버튼하고, 함수 호출하고 결과가 넘어온 것을 div 태그에 append해서 코딩할 줄 알아야한다.
- 2시간동안 IM에 해당하는 문제 2문제를 푼다. 
    - 기본적으로 저런거는 샘플코드를 다 주니까 확인만 하면 된다.
- 입력하는 html, 출력, 실행하는 html 양식을 주니까 
    - input id를 이용해서 값 가져오기
    - button에 event handler 호출하기
    - 거기서 execute를 호출하는건 줄거다
    - 그럼 그거로 계산하고 나온 값을 div에 append해서 출력할 수 있어야한다.
    - 그래서 이게 처음 주어지면 반환 값이 div에 먼저 append되는지를 출력해보자. 

- ????????????????
- 월요일 시험
    - 비디오 태그
    - js 이벤트 핸들러
    - jquery 이벤트 핸들러

- 목요일 월말 평가
    - jqeury로 화면에 입력 받고, div에 어팬트 할줄 알아야하고,
    - 기본 함수 호출 파일이나 이런건 다 준다. 그래서 그거에 im문제를 풀 정도면 된다. 
    