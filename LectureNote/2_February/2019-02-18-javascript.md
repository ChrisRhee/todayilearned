***
contents : javascript
date : 2019 02 18 Mon
***

## 전혜영 교수님
* Email : OOPSW@tistory.com
* 서버 : http://70.12.109.160:8888
* 숙제 : 이름.zip 
* 질문하는 방법
    - 코딩은 에러메세지를 찍어서 보내라
    - 수업자료는 수업 끝나고 배포가 된다. 
    - 배포된 자료는 나만 볼 수 있는 곳에 올려놓고 보세요.
    - 이해가 안되는 부분은 이 부분이 왜 이해가 안되는지를 설명해서 보내자
- 질문에 대한 반응
    1. 다음주에 할거야. 잘하고있음
    2. main말고 가지 질문인 경우가 있따. 필요는한데, main으로 중요한건 아닌경우
    3. main은 여기있는데 완전 저기 바깥쪽에서 질문하는 사람이있따.

# 박상진 교수님
정올, 백준
- 파일명 : Main문제번호_문제이름_서울10반_이찬호.java

sw expert
- 파일명 : Solution문제번호_문제이름_서울10반_이찬호.java


## Markdown 문법
### 이미지 넣기
![](./img/0114/.png)

----------------------------------------------------------
# 수업
- HTML에서는 디자인과 관련된 속성은 안쓰는게 좋다. 

```html
<h1>아이디를 입력하세요</h1>
<form action="aaaaa.jsp">
    아이디: <input id="inputId" name="inputId">
    <input type = "submit">
</form>
```
- A방식은 HTML에서 화면+Event+Handling 을 한번에 다한다.

```html
<script type="text/javascript">
	function sendId(){
		alert('send...');
	}
</script>
</head>
<body>
<h1>아이디를 입력하세요</h1>
    아이디: <input id="inputId" name="inputId"> 
    <input type="submit">
    <button onclick="sendId()">전송</button>
```
- B방식 : 화면+Event 발생만 HTML에서 하고 Handling은 다른곳에서 한다.
    - 이렇게 버튼을 만들어서 sendId()라는 함수 이벤트만 발생시킨다. 
    - 발생시킨 이벤트는 자바 스크립트에서 funtion을 실행한다. 
    - 스크립트는 위에다 놓고, HTML에서 미리 읽었으니까 자유롭게 호출할 수 있게된다.
- 원래 B방식이 가장 디폴트이다. 

```html
<body>
	<h1>아이디를 입력하세요</h1>
	아이디:
	<input>
	<button>전송</button>
	<script type="text/javascript">
		document.querySelector("button").onclick = function(){
			var i = document.querySelector("input").value;
			alert(i);
		};
	</script>
</body>
```
- C방식 : 디자인 코드 이후 이벤트를 정의하기 때문에 하단에 정의하는 것을 권장한다.
    - Html을 읽어야 하므로 html의 디자인 코드 밑에 정의를 한다. 
- 이게 Jquery를 만드는 가장 기본적인 방법이 된다.
- 심플하고, 어플 보안 방식으로 하면 좋다. 

- 옛날엔 해커가 툴로 했는데, 
- xss 공격이라고 한다.
    https://namu.wiki/w/XSS


```JavaScript
var userFunction = function(){
		console.log(this);
	}
userFunction(); //window 계열로 관리
new userFunction(); // heap에 새로운 오브젝트 객체가 만들어지는것 
// function으로 정의된 것은 일반 함수, 생성자로 사용가능
// 구분하기 위해서 new를 유도하기 위해서 생서자는 대문자로 쓰는것을 권장한다.
```
- 내장객체의 종류
    1. 웹 브라우저 내장객체
        - window를 조작 가능
    2. Javascript 내장 객체
        - 화면하고 연결된 코드가 없다.
- function으로 정의된 것은 일반 함수, 생성자로 사용가능
- 구분하기 위해서 new를 유도하기 위해서 생서자는 대문자로 쓰는것을 권장한다.

- 함수에서 매개인자는 선택이다. 왜 넣는가?
    - 식별자 이름을 저렇게 주면 가독성이 좋아지기 때문이다.
    - 막판에 소스분석할때 짜증나는게 매개인자가 2갠데 4개를 넣어도 돌아간다(?) 짜증나지만 자바스크립트에서는 유연성을 확보하기 위해 만들어놨다. 

- 생성자에서 매개인자는 필수는 아니다. 가독성을 위해서 필요한 부분만 넣어주는 것이 바랍직하다.

- 내가 쓰는 객체가 이벤트과 관리된건지, 처리와 관리된 건지 구분을 해야한다.

- 자바스크립트 공부할 사이트 
https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Global_Objects/Array

- 노출해도 되고 속도를 빠르게 하고 싶은것 : CSS 같은
    - 이런건 Local Storage에 저장한다.
    - 속도는 빠르다. 근데 다 노출될 수 있어서 보안에 취약하다.
- 처리하는 동안 중요정보가 있고 브라우저가 꺼지면 사라지게 하는 것 = Session Storage
    - 개인정보나 중요정보 있는걸 다루는 경우는 세션에 저장하면 된다.

----------------------------------------------------------
# 수업 외 꿀팁


----------------------------------------------------------
# 공지사항
