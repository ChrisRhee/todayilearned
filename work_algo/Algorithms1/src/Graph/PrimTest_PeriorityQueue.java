package Graph;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

/*
 * 최소신장트리 알고리즘
 * Prim 알고리즘
 * PeriorityQueue 사용
 */

class PrimTest_PeriorityQueue{
	
	public static int N;
	public static long Graph[][] = 
		{{0, 32, 31, 0, 0, 60, 51},
		{32, 0, 21, 0, 0, 0, 0},
		{31, 21, 0, 0, 46, 0, 25},
		{0, 0, 0, 0, 34, 18, 0},
		{0, 0, 46, 34, 0, 40, 51},
		{60, 0, 0, 18, 40, 0, 0},
		{51, 0, 25, 0, 51, 0, 0},};

	public static long[] Weight;
	public static long prim() {
		PriorityQueue<long[]> pq = new PriorityQueue<long[]>(new Comparator<long[]>() {
			@Override
			public int compare(long[] o1, long[] o2) {
				return Long.compare(o1[0],  o2[0]);
			}
		});
		boolean[] visited = new boolean[N];
		visited[0] = true;
		for(int next=0; next<N; next++) {
			if(Graph[0][next]!=0)
				pq.offer(new long[] {Graph[0][next], next} );
		}
		
		long sumCost = 0;
		while(!pq.isEmpty()) {
			long[] currArr= pq.poll();
			int curr =(int)currArr[1];
			long weight=currArr[0];
			
			// 방문처리, 값 추가
			if(visited[curr]) continue;
			visited[curr] = true;
			sumCost += weight;
			
			for(int next=0; next<N; next++) {
				// 원래 최소값 찾아야하는데, - 를 0으로 해놔서 0 아닌것중에 최소값 구해야함
				
				if(Graph[curr][next]!=0)
					pq.offer(new long[]{Graph[curr][next],next});
			}
		}
		return sumCost;
	}
	
	public static void main(String[] args) {
		N = 7;
		Weight = new long[N];
		for (long[] s: Graph) {
			System.out.println(Arrays.toString(s));
		}
		System.out.println(Arrays.toString(Weight));
		System.out.println(prim());
	}

}