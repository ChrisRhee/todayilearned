package Graph;

import java.util.Arrays;

/*
 * 최소신장트리 알고리즘
 * Prim 알고리즘
 */
class PrimTest{
	
	public static int N;
	public static long Graph[][] = 
		{{0, 32, 31, 0, 0, 60, 51},
		{32, 0, 21, 0, 0, 0, 0},
		{31, 21, 0, 0, 46, 0, 25},
		{0, 0, 0, 0, 34, 18, 0},
		{0, 0, 46, 34, 0, 40, 51},
		{60, 0, 0, 18, 40, 0, 0},
		{51, 0, 25, 0, 51, 0, 0},};

	public static long[] Weight;
	public static long[] Parent;
	public static long prim() {
		for(int i=0; i<N; i++) Weight[i] = -1;
		Weight[0] = 0;
		for(int k=1;k<N; k++) {
			long minWeight = Long.MAX_VALUE;
			int minVertax= 0;
			for(int i=0; i<N; i++) {
				if(Weight[i] < 0) continue;
				for(int j=0; j<N; j++) {
					if(Weight[j] >= 0 )continue;
					// graph값중 제일 작은 가중치를 잡아야한다. 
					if(Graph[i][j]<minWeight && Graph[i][j] != 0) {
						minVertax = j;
						
						minWeight = Graph[i][j];
					}
				}
			}
			Weight[minVertax]= minWeight;
		}
		long sumCost =0;
		for(int i=0; i<N; i++) {sumCost += Weight[i];
		}
		return sumCost;
	}
	
	public static void main(String[] args) {
		N = 7;
		Weight = new long[N];
		long result = prim();
		System.out.println(Arrays.toString(Weight));
		System.out.println(result);
	}

}