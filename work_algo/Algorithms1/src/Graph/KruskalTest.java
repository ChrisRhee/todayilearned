package Graph;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;

/*
 * 최소신장트리 알고리즘
 * Prim 알고리즘
 */
class KruskalTest{
	public static class Edge{
		int u;
		int v;
		long cost;
		public Edge() {
			
		}
		public Edge(int u, int v, long cost) {
			this.u = u;
			this.v= v;
			this.cost = cost;
		}
	}
	public static int N;
	public static Edge[] EdgeArr;
	public static int EdgeCnt;
	public static double E;
	public static long Graph[][];
	public static long[] Weight;
	public static int[] Parent;
	public static int xNum[], yNum[];
	
	public static int findset(int i) {
		if(i == Parent[i]) return i;
		return Parent[i] = findset(Parent[i]);
	}
	
	public static long kruskal() {
		Arrays.sort(EdgeArr, new Comparator<Edge>() {
			@Override
			public int compare(Edge o1, Edge o2) {
				return Long.compare(o1.cost, o2.cost);
			}
		});
		for(int i=0; i<N; i++) Parent[i] = i; //MakeSet
		long sumCost = 0;
		long selectCnt =0;
		for(int i = 0; i < EdgeCnt; i++) {
			int u = EdgeArr[i].u;
			int v = EdgeArr[i].v;
			if(findset(u) == findset(v)) continue; // 사이클 체크
			
			Parent[findset(u)] = findset(v); // union
			sumCost += EdgeArr[i].cost;
			if(++selectCnt >= N - 1) break;
		}

		return sumCost;
	}
	
	

	public static void main(String[] args) throws Exception, IOException {
		System.setIn(new FileInputStream("res/input1251.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			
			N= Integer.parseInt(br.readLine());
			Parent = new int[N];
			Graph = new long[N][N];
			Weight = new long [N];
			EdgeArr = new Edge[N*(N-1)/2];
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			String x[] = br.readLine().split(" ");
			String y[] = br.readLine().split(" ");
			xNum = new int[N];
			yNum = new int[N];
			for(int i =0; i<N; i++) {
				xNum[i] = Integer.parseInt(x[i]);
				yNum[i] = Integer.parseInt(y[i]);
			}
			E= Double.parseDouble(br.readLine());
			
			EdgeCnt =0;
			for(int i=0; i<N; i++) {
				for(int j = i+1 ; j<N; j++) {
					if(i==j)continue;
					long xx = xNum[j] - xNum[i];
					long yy = yNum[j] - yNum[i];
					long cost = xx*xx + yy*yy; 
					EdgeArr[EdgeCnt++] = new Edge(i,j,cost);
				}
			}
			long ans = Math.round(kruskal() * E);
			System.out.println("#"+tc + " "+ans);
		}
		br.close();
	}

}