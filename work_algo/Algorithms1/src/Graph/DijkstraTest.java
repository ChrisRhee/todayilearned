package Graph;

import java.util.*;

/*
      (1)   2     (2)
       1  5     2

       (4)      
             3      3
                    (3)
       
          1  1
          (5) 
             2     5

               (6)      
*/
public class DijkstraTest {
	public static int N=6;
	public static int INF=1000000000;
	public static int[][] a={
		{  0,  2, 5,  1,INF,INF},
		{  2,  0, 3,  2,INF,INF},
		{  5,  3, 0,  3,  1,  5},
		{  1,  2, 3,  0,  1,INF},
		{INF,INF, 1,  1,  0,  2},
		{INF,INF, 5,INF,  2,  0},
	};
	public static boolean[] v;
	public static int[] d;
	
	public static int getSmallIndex(){
		int min=INF;
		int index=0;
		for(int i=0; i<N; i++) {
			if(d[i]<min && !v[i]) {
				min=d[i];
				index=i;
			}
		}
		return index;
	}
	public static void dijkstra(int start){
		for(int i=0; i<N; i++) {
			d[i]=a[start][i];
		}
		v[start]=true;
		for(int i=0; i<N-2; i++) {
			int curr=getSmallIndex();
			v[curr]=true;
			for(int j=0; j<N; j++) {
				if(!v[j]) {
					if(d[j]>d[curr]+a[curr][j]) {
						d[j]=d[curr]+a[curr][j];
					}
				}
			}
		}
	}
	public static void main(String[] args) {
		v=new boolean[N];
		d=new int[N];
		dijkstra(0);
		for(int i=0; i<N; i++) {
			System.out.print(d[i]+" ");
		}
	}
}
