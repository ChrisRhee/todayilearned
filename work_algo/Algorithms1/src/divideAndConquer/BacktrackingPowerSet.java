package divideAndConquer;

import java.util.Arrays;

public class BacktrackingPowerSet {
	public static int[] d= {1,3,5};
	public static int cnt ;
	public static void main(String[] args) {
		int [] a= new int[d.length];
		backtrack(a,0,a.length);
		System.out.println(cnt);
	}
	
	private static void backtrack(int[] a, int k, int n){
		int[] c = new int[a.length];
		if(k==n) {
			cnt++;
			//System.out.println(Arrays.toString(a));
			for(int i=0; i<n; i++) {
				if(a[i]==1) System.out.print(d[i]+" ");
			}
			System.out.println();
		}else {
			int ncands = candidates(a,k,c);
			for(int i =0; i<ncands; i++) {
				a[k] = c[i];
				backtrack(a,k+1,n);
			}
		}
	}

	private static int candidates(int[] a, int k, int[] c) {
		c[0] = 1;
		c[1] = 0;
		
		return 2;
	}

}
