package divideAndConquer;

import java.util.Arrays;

public class BacktrackingPermutation {
	public static int[] d= {1,3,5};
	public static int cnt ;
	public static void main(String[] args) {
		int [] a= new int[d.length];
		backtrack(a,0,a.length);
		System.out.println(cnt);
	}
	
	private static void backtrack(int[] a, int k, int n){
		int[] c = new int[a.length];
		if(k==n) {
			cnt++;
			//System.out.println(Arrays.toString(a));
			for(int i=0; i<n; i++) {
				if(a[i]==1) System.out.print(d[i]+" ");
			}
			System.out.println();
		}else {
			int ncands = candidates(a,k,c);
			for(int i =0; i<ncands; i++) {
				a[k] = c[i];
				backtrack(a,k+1,n);
			}
		}
	}

	private static int candidates(int[] a, int k, int[] c) {
		boolean[] perm = new boolean[a.length];
		for(int i=0; i<k; k++) perm[a[i]] = true;
		int ncands = 0;
		for(int i=0; i<perm.length; i++) {
			if(perm[i] == false) c[ncands++] = i;
			
		}
		return ncands;
	}

}
