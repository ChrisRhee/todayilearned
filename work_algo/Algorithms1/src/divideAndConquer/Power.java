package divideAndConquer;

public class Power {
	public static int pow(int base, int exponent) {
		int result = 1;
		while(exponent>0) {
			if((exponent&1)==1) result = result*base;
			exponent = exponent >>1;
			base= base*base;
		}
		return result;
	}
	
	public static int power(int base, int exponent) {
		if(exponent ==0 )return 1;
		if(exponent ==1 )return base;
		// 0이면 짝수, 1이면 홀수 이게 더 빠르다
		// exponent%2 == 0 이랑 같음
		if( (exponent&1) == 0) {
			int newbase = power(base, exponent/2);
			return newbase*newbase;
		}
		else {
			int newbase = power(base, (exponent-1)/2);
			return newbase*newbase*base;
		}
	}
	public static void main(String[] args) {
		
		
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		
		int n = 10;
		for(int i =0 ; i< n; i++) {	
			System.out.println(pow(3, i)+"\t"+Math.pow(3, i)+"\t"+power(3,i));
		}
		
		long start= System.nanoTime();
		System.out.println(pow(3,5));
		long end = System.nanoTime();
		System.out.println("pow(3,5) time : "+(end-start)+"ns\n");
		
		long start2= System.nanoTime();
		System.out.println(power(3,5));
		long end2 = System.nanoTime();
		System.out.println("power(3,5) time : "+(end2-start2)+"ns\n");
	}
}
