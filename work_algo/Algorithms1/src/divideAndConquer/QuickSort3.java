package divideAndConquer;

import java.util.Arrays;

public class QuickSort3 {
	public static int[] a = {69,10,30,2,16,8,31,22};
	
	private static void quick(int begin, int end) {
		if(begin<end) {
			int p = hoare(begin,end);
			System.out.println(Arrays.toString(a)+"\t p="+p+" a["+p+"]="+a[p]+ " begin="+begin+" end="+end);
			quick(begin,p-1);
			quick(p+1,end);
		}
	}
	
	private static int hoare(int begin, int end) {
		int p = a[begin]; 
		int I = begin;
		int J = end;
		while(I<J) {
			while( a[I] <= p && I<end ) I++;
			while( a[J] >= p && begin<J ) J--;
			if(I<J) {
				int temp = a[J];
				a[J] = a[I];
				a[I] = temp;
			}
		}
		int T =a[J]; a[J]=a[begin]; a[begin] = T;
		return J;
	}

	public static void main(String[] args) {
		System.out.println(Arrays.toString(a));
		System.out.println();
		quick(0,a.length-1);
		System.out.println(Arrays.toString(a));
		System.out.println("Program End");
	}
}
