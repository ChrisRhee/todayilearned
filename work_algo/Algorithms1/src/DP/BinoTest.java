package DP;

import java.util.Arrays;

public class BinoTest {
	public static int nCr(int n, int r) {
		if(r==0 || n==r) return 1;
		else return nCr(n-1,r-1)+nCr(n-1,r);
	}
	public static int bino(int n, int r) {
		if(r==0 || n==r) return 1;
		else return bino(n-1,r-1)+bino(n-1,r);
	}
	
	public static int[][] b;

	public static int bino1(int n, int r) {
		if (r == 0 || n == r)
			return 1;
		if (b[n][r] != -1)
			return b[n][r];
		b[n][r] = bino1(n - 1, r - 1) + bino1(n - 1, r);
		return b[n][r];
	}
	
	public static int bino2(int n, int r) {
		for (int i = 0; i <= n; i++) {
			for (int j = 0; j <= i; j++) {
				if (j == 0 || j == i)
					b[i][j] = 1;
				else
					b[i][j] = b[i - 1][j - 1] + b[i - 1][j];
			}
		}
		return b[n][r];
	}
	
	
	public static void main(String[] args) {
		int n = 5;
		int r = 2;
		b= new int[n+1][r+1];
		for (int[] a : b) Arrays.fill(a, -1);
		//for (int[] a : b) System.out.println(Arrays.toString(a));
		System.out.println(bino1(n,r));
		
		b= new int[n+1][n+1];
		System.out.println(bino2(n,r));
		//System.out.println(bino(n,r));
		for (int[] a : b) System.out.println(Arrays.toString(a));
	}
}
