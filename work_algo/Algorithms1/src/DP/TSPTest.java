package DP;

import java.io.*;
import java.util.*;

public class TSPTest {
	public static int INF=987654321;//Integer.MAX_VALUE/2;
	public static int N;
	public static int[][] g;
	//public static int[][] m;
	
	public static int solve(int pos, int visited) {
		//if(m[pos][visited]!=0) return m[pos][visited];
		if(visited==(1<<N)-1) return 0;
		
		int ret=INF;
		for(int next=0; next<N; next++) {
			if((visited&(1<<next))==0 && g[pos][next]>0){
				int tmp=g[pos][next]+solve(next,visited|(1<<next));
				if(tmp<ret) ret=tmp;
			}
		}
		//m[pos][visited]=ret;
		//for(int[] a:m) System.out.println(Arrays.toString(a));
		//System.out.println();
		return ret;
	}
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/inputtsp.txt"));
		Scanner sc=new Scanner(System.in);
		int T=sc.nextInt();
		
		for(int tc=1; tc<=T; tc++) {
			N=sc.nextInt();
			g=new int[N][N];
			//m=new int[N][1<<N];
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					g[i][j]=sc.nextInt();
				}
			}
			
			int ans=INF;
			for(int i=0; i<N; i++) {
				int tmp=solve(i,1<<i);
				if(ans>tmp) ans=tmp;
			}
			
			System.out.println("#"+tc+" "+ans);
		}
		sc.close();
	}
}
