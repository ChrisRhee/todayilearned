package DP;

import java.util.Arrays;

public class KnapsackTest {
	public static void main(String[] args) {
		int N=4;
		int W=10;
		int [] wi = {5,4,6,3};     // Kg
		int [] vi = {10,40,30,50}; // 만원
		
		int [][] k_memoi = new int[N+1][W+1];
		for (int i = 1; i <= N; i++) {
			for (int j = 1; j <= W; j++) {
				if (wi[i - 1] > j) {
					k_memoi[i][j] = k_memoi[i-1][j];
				}else {
					k_memoi[i][j] = Math.max(vi[i-1]+k_memoi[i-1][j-wi[i-1]], k_memoi[i-1][j]);
				}
			}
		}
		System.out.println(k_memoi[N][W]);
		for (int[] is : k_memoi) {
			System.out.println(Arrays.toString(is));
		}
	}
}
