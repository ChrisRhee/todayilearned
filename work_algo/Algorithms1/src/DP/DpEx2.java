package DP;

public class DpEx2 {
	public static void main(String[] args) {
		int n=6;
		int[] dp = new int[n+1];
		dp[0]=1;
		dp[1]=2;
		System.out.println(dp[0]);
		System.out.println(dp[1]);
		for(int i=2; i<=n; i++) {
			dp[i]=2*dp[i-1]+dp[i-2];
			System.out.println(dp[i]);
		}
	}
}
