package DP;

import java.util.Arrays;

public class CoinTest3 {
	public static void main(String[] args) {
		int [] coin = {1,4,6};
		int money = 8;
		int[][] C= new int[coin.length][money+1];
		int min;
		for(int i=0; i<coin.length; i++) {
			for(int j=1; j<=money; j++) {
				if(i-1<0) {
					C[i][j]=C[i][j-coin[i]]+1;
				}else if(j-coin[i]<0) {
					C[i][j] = C[i-1][j];
				}else { //i-1>=0 && j-coin[i]>=0
					C[i][j] = Math.min(C[i][j-coin[i]]+1, C[i-1][j]);
				}
				for(int[] c:C) System.out.println(Arrays.toString(c));
				System.out.println();
			}
			System.out.println();
		}
	}
}
