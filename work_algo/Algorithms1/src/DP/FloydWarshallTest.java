package DP;

import java.util.Arrays;

/*
              5>      (2)
      (1)     <7        
     <8   <         9 
    v      ^2<      v
  (4)    <4   ^   (3)          
         3>
*/
public class FloydWarshallTest {
	public static int N=4;
	public static int INF=987654321;
	public static int[][] a={
		{  0,  5,INF,  8},
		{  7,  0,  9,INF},
		{  2,INF,  0,  4},
		{INF,INF,  3,  0}
	};
	public static int[][] d;
	
	public static void floydWarshall(){
		d=new int[N][N];
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				d[i][j]=a[i][j];
			}
		}
		for(int k=0; k<N; k++) {
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					// 경유지     출발지    도착지 
					// 직접 가는게, 돌아서 가는 것보다 크면
					if(d[i][j]>d[i][k]+d[k][j]) {
						// 돌아가는걸로 초기화 
					   d[i][j]=d[i][k]+d[k][j];
					}
				}
			}
		}
		for(int[] t:d) System.out.println(Arrays.toString(t));
	}
	public static void main(String[] args) {
		floydWarshall();
	}
}
