package DP;

import java.util.Arrays;

public class LisTest {
	public static void main(String[] args) {
		int[] a= {3,2,6,4,5,1};
		int n =a.length;
		
		int[] l = new int[n];
		int max = 0;
		for (int i = 0; i < l.length; i++) {
			l[i] = 1;
			for (int j = 0; j < l.length; j++) {
				if (a[j] < a[i] && l[i] < l[j]+1) {
					l[i]=l[j]+1;
				}
			}
			if(max < l[i]) max = l[i];
		}
		System.out.println(max);
		System.out.println(Arrays.toString(l));
	}
}
