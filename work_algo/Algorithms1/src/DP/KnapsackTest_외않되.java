package DP;

import java.io.*;
import java.util.Arrays;

public class KnapsackTest_외않되 {
	public static void main(String args[]) throws Exception {
		System.setIn(new FileInputStream("res/input3282.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++) {
			String[] temp = br.readLine().split(" ");
			int N = Integer.parseInt(temp[0]);
			int W = Integer.parseInt(temp[1]);
			System.out.println(N + " " + W);
			int[] wi = new int[N+1];
			int[] vi = new int[N+1];
			int[][] k = new int[N+1][W+1];
			for(int i = 1; i <= N; i++) {
				String[] line = br.readLine().split(" ");
				wi[i] = Integer.parseInt(line[0]);
				vi[i] = Integer.parseInt(line[1]);
			}
			for(int i = 1; i <= N; i++) {
				for(int j = 1; j <= W; j++) {
					if(wi[i] > j) {
						k[i][j] = k[i-1][j];
					} else {
						k[i][j] =  Math.max(vi[i] + k[i-1][j - wi[i]], k[i-1][j]);
					}
				}
			}
			System.out.println("#" + tc + " " + k[N][W]);
			for (int[] is : k) {
				System.out.println(Arrays.toString(is));
			}
		}
		br.close();
	}

}
