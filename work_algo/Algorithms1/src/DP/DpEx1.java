package DP;

public class DpEx1 {
	public static void main(String[] args) {
		int n=8;
		int[][] dp = new int[n+1][2];
		dp[1][0]=1; //노란
		dp[1][1]=1; //파란
		System.out.println(dp[0][0]);
		System.out.println(dp[1][0]);
		for(int i=2; i<=n; i++) {
			dp[i][0]=dp[i-1][0]+dp[i-1][1];
			dp[i][1]=dp[i-1][0];
			
			System.out.println(dp[i][0]+dp[i][1]);
		}
		
		System.out.println(dp[n][0]+dp[n][1]);
	}
}
