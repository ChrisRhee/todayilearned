package tree;

class Node2{
	char data;
	Node2 left, right;
}

class Tree2{
	Node2 root;
	Node2 MakeTree(Node2 left, char data, Node2 right) {
		Node2 node = new Node2();
		node.left = left;
		node.data = data;
		node.right = right;
		return node;
	}
	
	public void preOrder(Node2 root) {
		if(root !=null) {
			System.out.print(root.data+" ");
			preOrder(root.left);
			preOrder(root.right);
		}
	}

	public void inOrder(Node2 root) {
		if(root !=null) {
			inOrder(root.left);
			System.out.print(root.data+" ");
			inOrder(root.right);
		}
	}

	public void postOrder(Node2 root) {
		if(root !=null) {
			postOrder(root.left);
			postOrder(root.right);
			System.out.print(root.data+" ");
		}
	}
	
}

public class TreeTest2 {
	
	public static void main(String[] args) {
		Tree2 t = new Tree2();
		
		// leaf node
		Node2 h = t.MakeTree(null, 'H', null);
		Node2 i = t.MakeTree(null, 'I', null);
		Node2 d = t.MakeTree(null, 'D', null);
		Node2 f = t.MakeTree(null, 'F', null);
		Node2 g = t.MakeTree(null, 'G', null);
		
		// sub tree
		Node2 e = t.MakeTree(h, 'E', i);
		Node2 b = t.MakeTree(d, 'B', e);
		Node2 c = t.MakeTree(f, 'C', g);
		Node2 a = t.MakeTree(b, 'A', c);
		
		// root
		t.root=a;
		
		System.out.println("Preorder");
		t.preOrder(a);
		System.out.println("\nInorder");
		t.inOrder(a);
		System.out.println("\nPostorder");
		t.postOrder(a);
	}
}
