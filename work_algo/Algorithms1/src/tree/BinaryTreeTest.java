package tree;

import java.util.Arrays;

class BinaryTree{
	class Node{
		int data;
		Node left,right;
		Node(int data){ this.data = data; }
	}
	
	Node root;
	public void makeTree(int[] a) {
		root = makeTree(a,0,a.length-1);
	}
	
	public Node makeTree(int[] a, int start, int end) {
		if(start>end) return null;
		int mid = (start+end)/2;
		Node node = new Node(a[mid]);
		node.left=makeTree(a,start,mid-1);
		node.right=makeTree(a,mid+1,end);
		return node;
	}
	public void inorder(Node root) {
		if(root!=null) {
			inorder(root.left);
			System.out.print(root.data+" ");
			inorder(root.right);
		}
	}
	public void search(Node root, int num) {
		if(num<root.data) {
			System.out.println("Smaller "+root.data);
			search(root.left, num);
		}
		else if(num>root.data) {
			System.out.println("Bigger "+root.data);
			search(root.right, num);
		}
		else {
			System.out.println("Found!");
		}
	}
}
public class BinaryTreeTest {
	public static void main(String[] args) {
		int[] a = {1,4,9,11,14,15,22,27,38,49};
		//for(int i=0; i<a.length; i++) {
		//	a[i] = i;
		//}
		BinaryTree t = new BinaryTree();
		t.makeTree(a);
		t.inorder(t.root);
		System.out.println();
		t.search(t.root,11);
		
	}
}
