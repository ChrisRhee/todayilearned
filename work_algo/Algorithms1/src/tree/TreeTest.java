package tree;

class Node{
	char data;
	Node left, right;
}

class Tree{
	Node root;
	Node MakeTree(Node left, char data, Node right) {
		Node node = new Node();
		node.left = left;
		node.data = data;
		node.right = right;
		return node;
	}
	
	public void preOrder(Node root) {
		if(root !=null) {
			System.out.print(root.data+" ");
			preOrder(root.left);
			preOrder(root.right);
		}
	}

	public void inOrder(Node root) {
		if(root !=null) {
			inOrder(root.left);
			System.out.print(root.data+" ");
			inOrder(root.right);
		}
	}

	public void postOrder(Node root) {
		if(root !=null) {
			postOrder(root.left);
			postOrder(root.right);
			System.out.print(root.data+" ");
		}
	}
	
}

public class TreeTest {
	
	public static void main(String[] args) {
		Tree t = new Tree();
		
		// leaf node
		Node d = t.MakeTree(null, 'D', null);
		Node h = t.MakeTree(null, 'H', null);
		Node i = t.MakeTree(null, 'I', null);
		Node f = t.MakeTree(null, 'F', null);
		Node g = t.MakeTree(null, 'G', null);
		
		// sub tree
		Node e = t.MakeTree(h, 'E', i);
		Node b = t.MakeTree(d, 'B', e);
		Node c = t.MakeTree(f, 'C', g);
		Node a = t.MakeTree(b, 'A', c);
		
		// root
		t.root=a;
		
		System.out.println("Preorder");
		t.preOrder(a);
		System.out.println("\nInorder");
		t.inOrder(a);
		System.out.println("\nPostorder");
		t.postOrder(a);
	}
}
