package tree;

class BST{
	class Node{
		int data;
		Node left,right;
		Node(int data) { this.data = data; }
		
		public String toString() {
			return ""+data;
		}
	}
	Node root;
	
	public void insert(int data) {
		root = insert(root,data);
	}
	public Node insert(Node root, int data) {
		if(root==null) {
			root=new Node(data);
			return root;
		}
		if(data<root.data) {
			root.left = insert(root.left,data);
		}else if (data>root.data) 
			root.right = insert(root.right, data);
		return root;
	}
	
	public void delete(int data) {
		root = delete(root,data);
	}
	public Node delete(Node root, int data) {
		if(root==null) return null;
		
		if(data<root.data) {
			root.left = delete(root.left,data);
		}else if (data>root.data) {
			root.right = delete(root.right, data);
		}else {
			if(root.left == null && root.right==null) return null;
			else if(root.left==null) return root.right;
			else if(root.right==null) return root.left;
			
			root.data = fideMin(root.right);
			root.right = delete(root.right, root.data);
		}
		return root;
	}
	
	public int fideMin(Node root) {
		int min = root.data;
		while(root.left!=null) {
			min=root.left.data;
			root=root.left;
		}
		return min;
	}
	
	
	public Node search(Node root, int data) {
		if(root==null || data==root.data) return root;
		if(data<root.data) return search(root.left, data);
		else return search(root.right, data);
	}
	
	public void inorder() {
		inorder(root);
		System.out.println();
	}
	public void inorder(Node root) {
		if(root!=null) {
			inorder(root.left);
			System.out.print(root+" ");
			inorder(root.right);
		}
	}
	
}
public class BinarySearchTreeTest {
	public static void main(String[] args) {
		BST t = new BST();
		t.insert(4);
		t.insert(2);
		t.insert(1);
		t.insert(7);
		t.insert(3);
		t.insert(5);
		t.insert(6);
		t.inorder();
		System.out.println(t.search(t.root, 6));
	}
}
