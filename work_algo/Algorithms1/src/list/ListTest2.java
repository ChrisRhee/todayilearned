package list;


class MyLinkedList2{
	class Node{
		int data;
		Node next;
	}
	
	// header points Empty node. 
	Node header = new Node();
	
	public void addFirst(int data) {
		Node n = new Node();
		n.data = data;
		n.next = header.next;
		header.next = n;
	}
	
	public void print() {
		if(header.next ==null) return;
		
		Node n = header.next;
		while(n.next!=null) {
			System.out.print(n.data+"->");
			n = n.next;
		}
		System.out.println(n.data);
	}
	
	public void append(int data) {
		if(header.next==null) {
			addFirst(data);
		}
		else {
			Node end = new Node();
			end.data = data;
			Node n = header.next;
			while(n.next!=null) {
				n = n.next;
			}
			n.next = end;
		}
	}
	
	public void delete(int data) {
		if(header.next == null) {
			return;
		}
		Node n = header;
		while(n.next!=null) {
			if(n.next.data==data) {
				n.next=n.next.next;
				return;
			}else {
				n = n.next;
			}
		}
	}
}

public class ListTest2 {
	public static void main(String[] args) {
		MyLinkedList2 ll = new MyLinkedList2();
		ll.append(4);
		ll.append(5);
		ll.append(6);
		ll.append(7);
		
		ll.print();
	}
}