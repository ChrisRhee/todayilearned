package list;

import java.util.Arrays;

public class InsertionSort {
	public static int data[] = {69, 10, 30, 2, 16, 8, 31, 22};
	
	public static void main(String[] args) {
		System.out.println(Arrays.toString(data));
		for (int i = 1; i < data.length; i++) {
			int k = i;
			for (int j = k-1; j >= 0; j--) {
				if(data[j]>data[k]) {
					int t = data[j];
					data[j] = data[k];
					data[k] = t;
					k = j;
					System.out.println(Arrays.toString(data));
				}
			}
			System.out.println();
		}
	}
}
