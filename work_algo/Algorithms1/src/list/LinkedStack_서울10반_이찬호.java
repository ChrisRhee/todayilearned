package list;

import java.util.Stack;


class sNode{
	int data;
	sNode prev;
}

public class LinkedStack_서울10반_이찬호 {
	
	public static sNode top = null;
	
	public static boolean push(int value) {
		//push()문장
		sNode newNode = new sNode();
		
		newNode.prev = top;
		newNode.data = value;
		top = newNode;
		return true;
	}
	
	public static int pop() {
		if(top==null) return -1; // isEmpty()
		int temp = top.data;
		top = top.prev;
		return temp; // pop()
	}
	
	public static int peek() {
		return top.data;
	}
	
	public static void main(String[] args) {
		push(1);
		push(2);
		push(3);
		push(4);
		push(5);
		push(6);
		System.out.println("peek: "+ peek());
		System.out.println(pop());
		System.out.println(pop());
		System.out.println(pop());
		System.out.println(pop());
		System.out.println("peek: "+ peek());
		System.out.println(pop());
		
		System.out.println("-------- 절취선 ---------");
		Stack<Character> s = new Stack<>();
		System.out.println(s.isEmpty());
		s.push('A');
		s.push('B');
		s.push('C');
		s.push('D');
		// peek 지금 최고 위에있는 아이템이 무엇인지 보여줌
		// size 전체 스택의 크기를 보여줌
		System.out.println(s.size()+" "+s.peek());
		System.out.println(s.pop());
		System.out.println(s.size()+" "+s.peek());
		System.out.println(s.pop());
		System.out.println(s.pop());
		System.out.println(s.pop());
		
	}
	
}
