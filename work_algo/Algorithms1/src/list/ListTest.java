package list;

class MyLinkedList{
	class Node{
		int data;
		Node next;
	}
	
	Node header = null;
	
	public void addFirst(int data) {
		Node n = new Node();
		n.data = data;
		n.next = header;
		header = n;
	}
	
	public void print() {
		if(header ==null) return;
		Node n = header;
		while(n.next!=null) {
			System.out.print(n.data+"->");
			n = n.next;
		}
		System.out.println(n.data);
	}
	
	public void append(int data) {
		if(header==null) {
			addFirst(data);
		}
		else {
			Node end = new Node();
			end.data = data;
			Node n = header;
			while(n.next!=null) {
				n = n.next;
			}
			n.next = end;
		}
	}
	
	public void delete(int data) {
		if(header == null) {
			return;
		}
		if(header.data==data) {
			header = header.next;
			return;
		}
		Node n = header;
		while(n.next!=null) {
			if(n.next.data==data) {
				n.next=n.next.next;
				return;
			}else {
				n = n.next;
			}
		}
	}
}

public class ListTest {
	public static void main(String[] args) {
		MyLinkedList ll = new MyLinkedList();
		ll.addFirst(3);
		ll.addFirst(2);
		ll.addFirst(1);
		ll.append(4);
		ll.append(5);
		ll.append(6);
		ll.append(7);
		ll.append(4);
		ll.print();
	}
}
