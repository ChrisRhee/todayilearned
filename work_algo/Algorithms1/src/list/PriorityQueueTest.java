package list;

public class PriorityQueueTest {
	public static int [] queue = new int [100];
	public static int front = -1;
	public static int rear = -1;
	
	public static boolean isEmpty() {
		if(front == rear) return true;
		else return false;
	}
	
	public static boolean isFull() {
		if(rear==queue.length-1) return true;
		else return false;
	}
	
	public static void enqueue(int item) {
		if(isFull()) {
			System.out.println("FULL");
			return;
		}
		queue[++rear] = item;
		int k = rear;
		for (int j = k-1; j > front; j--) {
			if(queue[j]>queue[k]) {
				int t = queue[j];
				queue[j] = queue[k];
				queue[k] = t;
				k = j;
			}
		}
	}
	public static int dequeue() {
		if(isEmpty()) {
			System.out.println("EMPTY");
			return -1;
		}
		return queue[++front];
	}
	
	public static int qpeek() {
		if(isEmpty()) {
			System.out.println("EMPTY");
			return -1;
		}
		return queue[1+front];
	}
	
	public static void main(String[] args) {
		// 자바의 API
		enqueue(6);
		enqueue(4);
		enqueue(3);
		enqueue(2);
		enqueue(1);
		enqueue(9);
		
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		
		
	}
}
