package list;

class MyPriorityLinkedList{
	class Node{
		int data;
		Node next;
	}
	
	Node header = null;
	
	public void addFirst(int data) {
		Node n = new Node();
		n.data = data;
		n.next = header;
		header = n;
	}
	
	public void add (int data) {
		if(header == null) {
			addFirst(data);
		}else {
			Node end = new Node();
			end.data = data;
			if(header.data>data) {
				end.next=header;
				header= end;
				return;
			}
			Node n = header;
			while(n.next!= null) {
				if(n.next.data>data) {
					end.next = n.next;
					n.next = end;
					return;
				}else {
					n=n.next;
				}
			}
			n.next = end;
		}
	}
	
	
	public void print() {
		
		if(header ==null) return;
		Node n = header;
		while(n.next!=null) {
			System.out.print(n.data+"->");
			n = n.next;
		}
		System.out.println(n.data);
	}
	
	public void append(int data) {
		if(header==null) {
			addFirst(data);
		}
		else {
			Node end = new Node();
			end.data = data;
			Node n = header;
			while(n.next!=null) {
				n = n.next;
			}
			n.next = end;
		}
	}
	
	public void delete(int data) {
		if(header == null) {
			return;
		}
		if(header.data==data) {
			header = header.next;
			return;
		}
		Node n = header;
		while(n.next!=null) {
			if(n.next.data==data) {
				n.next=n.next.next;
				return;
			}else {
				n = n.next;
			}
		}
	}
}

public class PriorityListTest {
	public static void main(String[] args) {
		MyPriorityLinkedList ll = new MyPriorityLinkedList();
		ll.add(3);
		ll.add(2);
		ll.add(1);
		ll.add(5);
		ll.add(8);
		ll.add(4);
		
		ll.print();
	}
}
