package list;

import java.util.PriorityQueue;
import java.util.Queue;

class qNode{
	int data;
	qNode link;
}

public class PriorityQueueLinked {
	
	public static qNode front = null;
	public static qNode rear = null;
	
	public static boolean isEmpty() {
		if(front == null) return true;
		else return false;
	}
	
	public static void enqueue(int item) {
		qNode end = new qNode();
		end.data=item;
		if(isEmpty()) {
			front=end;
			rear=end;
			return;
		}
		
		if(front.data>item) {
			end.link = front;
			front = end;
			return;
		}
		
		qNode n = front;
		while(n.link != null) {
			if(n.link.data> item) {
				end.link = n.link;
				n.link = end;
				return;
			}else {
				n = n.link;	
			}
		}
		n.link = end;
		rear = end;
	}
	public static int dequeue() {
		if(isEmpty()) {
			System.out.println("EMPTY");
			return -1;
		}
		int data= front.data;
		front = front.link;
		if(front == null) rear = null;
		return data;
	}
	
	public static int qpeek() {
		if(isEmpty()) {
			System.out.println("EMPTY");
			return -1;
		}
		return front.data;
	}
	
	public static void main(String[] args) {
		// 자바의 API
		enqueue(6);
		enqueue(7);
		enqueue(8);
		enqueue(1);
		enqueue(2);
		enqueue(3);
		enqueue(4);
		enqueue(5);
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		System.out.println(dequeue());
		
		System.out.println(qpeek());
		
		System.out.println(" ------- 절취선 ------- ");
		Queue<Integer> pq = new PriorityQueue<>();
		pq.offer(3);
		pq.offer(2);
		pq.offer(1);
		pq.offer(4);
		pq.offer(10);
		pq.offer(40);
		pq.offer(43);
		pq.offer(14);
		System.out.println(pq);
		
		
	}
}

