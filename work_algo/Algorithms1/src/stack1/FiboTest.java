package stack1;

public class FiboTest { 
	public static int fibo(int n) {
		if(n<2) return n;
		else return fibo(n-1)+fibo(n-2);
	}
	
	public static int memo[];
	
	public static int fibo1(int n) {
		if( n>=2 && memo[n]==0) {
			memo[n] = fibo1(n-1)+fibo1(n-2);
		}
		return memo[n];
	}
	
	public static int fibo2(int n) {
		for(int i=2; i<=n; i++) {
			memo[i] = memo[i-1]+memo[i-2];
		}
		return memo[n];
	}
	
	public static void main(String[] args) {
		int num = 40;
		
		long st =System.nanoTime();
		long start =System.currentTimeMillis();
		//System.out.println(fibo(num));
		long en=System.nanoTime();
		long end =System.currentTimeMillis();
		System.out.println("fibo "+(end-start)+"ms "+(en-st)+"ns");
		
		
		st =System.nanoTime();
		start =System.currentTimeMillis();
		memo = new int[1000];
		memo[0]= 0;
		memo[1]= 1;
		System.out.println(fibo1(num));
		en=System.nanoTime();
		end =System.currentTimeMillis();
		System.out.println("fibo1 "+(end-start)+"ms "+(en-st)+"ns");
		
		
		
		st =System.nanoTime();
		start =System.currentTimeMillis();
		memo = new int[1000];
		memo[0]= 0;
		memo[1]= 1;
		System.out.println(fibo2(num));
		en=System.nanoTime();
		end =System.currentTimeMillis();
		System.out.println("fibo2 "+(end-start)+"ms "+(en-st)+"ns");
	}
}
