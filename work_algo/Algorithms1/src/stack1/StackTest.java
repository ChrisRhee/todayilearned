package stack1;

import java.util.Stack;

public class StackTest {
	public static int stack[] = new int[1000];
	public static int top = -1;
	
	public static boolean push(int value) {
		if(top >= stack.length) {
			return false;
		}
		//push()문장
		stack[++top]= value;
		return true;
	}
	
	public static int pop() {
		if(top==-1) return -1; // isEmpty()
		return stack[top--]; // pop()
	}
	
	public static void main(String[] args) {
		push(1);
		push(2);
		push(3);
		System.out.println(pop());
		System.out.println(pop());
		System.out.println(pop());
		
		System.out.println("-------- 절취선 ---------");
		Stack<Character> s = new Stack<>();
		System.out.println(s.isEmpty());
		s.push('A');
		s.push('B');
		s.push('C');
		s.push('D');
		// peek 지금 최고 위에있는 아이템이 무엇인지 보여줌
		// size 전체 스택의 크기를 보여줌
		System.out.println(s.size()+" "+s.peek());
		System.out.println(s.pop());
		System.out.println(s.size()+" "+s.peek());
		System.out.println(s.pop());
		System.out.println(s.pop());
		
	}
	
}
