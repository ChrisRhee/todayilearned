package stack1;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class DfsTest {
	public static int V,E;
	public static int graph[][];
	public static int stack[];
	public static int top;
	public static boolean visit[];
	public static Stack<Integer> s;
	
	private static void dfsRecursive(int node) {
		visit[node] = true;
		System.out.print(node+" ");
		for(int next=0; next<V; next++) {
			if(visit[next]==false && graph[node][next]==1)
				dfsRecursive(next);
		}
	}
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/inputDfs.txt"));
		Scanner sc = new Scanner(System.in);
				
		V= sc.nextInt();
		E= sc.nextInt();
		s= new Stack<>();
		graph = new int[V][V];
		visit = new boolean[V];
		// 넉넉하게 크게 주자. 오버플로우 발생하지 않게
		stack = new int[100];
		top = -1;
		for(int i=0; i<E; i++) {
			int v1 = sc.nextInt();
			int v2 = sc.nextInt();
			// ij, ji 대칭되게 넣는다. 
			graph[v1][v2]=graph[v2][v1]=1;
		}
		for(int [] a: graph) System.out.println(Arrays.toString(a));
		System.out.print("#1 ");
		//dfsRecursive(0);
		dfs(0);
		sc.close();
	}
	
	// 이거도 암기를 꼭 해보세요 ㅠㅠㅠㅠㅠ 으허읗거ㅡㅇ허그흥흑허읗
	// 배열을 스택으로 만들어서 구현
	private static void dfs(int node) {
		stack[++top]=node;  // push()
		while(top!= -1) {   // isEmpty()
			int curr=stack[top--]; // pop()
			if(visit[curr]==false) {
				visit[curr]=true;
				System.out.print(curr+" ");
				
				for(int next=0; next<V; next++) {
				//for(int next=V-1; next>=0; next--) {
					if(visit[next]==false && graph[curr][next]==1)
						stack[++top]=next;  // push()
				}
			}
		}
	}
	
	//스택으로 이용해서 구현 
	private static void dfsStack(int node) {
		s.push(node);
		while(!s.isEmpty()) {   // isEmpty()
			int curr=s.pop(); // pop()
			if(visit[curr]==false) {
				visit[curr]=true;
				System.out.print(curr+" ");
				
				for(int next=0; next<V; next++) {
				//for(int next=V-1; next>=0; next--) {
					if(visit[next]==false && graph[curr][next]==1)
						s.push(next);  // push()
				}
			}
		}
	}
}
