package JungOl;
import java.io.FileInputStream;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
class Main1462_보물섬_서울10반_아버지코드{
	public static int M, N;
	public static int max; 
	public static char[][] m;
	public static int [][] v;
	public static Queue<int[]> q;
	public static int di[] = { -1,1,0,0 };
	public static int dj[] = { 0 ,0,-1,1};
	
	private static void bfs(int i, int j) {
		v = new int[N][M];
		q.offer(new int[] { i, j });
		v[i][j] = 1;
		while (!q.isEmpty()) { // isEmpty()
			int[] curr = q.poll(); // dequeue()
			for (int d = 0; d < 4; d++) {
				int ii = curr[0] + di[d];
				int jj = curr[1] + dj[d];
				if (ii < 0 || ii >= N || jj < 0 || jj >= M) {
					continue;
				}
				if (m[ii][jj] == 'L' && v[ii][jj] == 0) {
					v[ii][jj] = v[curr[0]][curr[1]] + 1;
					if (v[ii][jj] > max)
						max = v[ii][jj];
					q.offer(new int[] { ii, jj });
				}
			}
		}
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/JungOl1462.txt"));
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		M = sc.nextInt();
		m= new char[N][M];
		for (int i = 0; i < N; i++) {
			String s = sc.next();
			for(int j=0; j<M; j++) {
				m[i][j]= s.charAt(j);
			}
			
		}
		max = 0;
		q = new LinkedList<int[]>();
		for (int i = 0; i < N; i++) {
			for(int j=0; j<M; j++) {
				if(m[i][j]=='L') {
					bfs(i,j);
				}
			}
		}	
		System.out.println(max-1);
		sc.close();
	}
}