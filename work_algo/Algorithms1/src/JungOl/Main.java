package JungOl;
import java.io.*;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Main{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		String s[] = br.readLine().split(" ");
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
		br.close();
	}
}