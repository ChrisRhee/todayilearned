package JungOl;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
class Babygin2_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/inputbaby.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++)
		{
			int Answer =0;
			int tri =0, run =0;
			String baby[] = br.readLine().split(" ");
			int[] count = new int[10];
			for(int i =0; i<baby.length; i++) {
				count[Integer.parseInt(baby[i])]++;
			}
			
			//triple
			for(int i=0; i<count.length ; i ++) {
				if(count[i]==6) {
					tri = 2;
				}
				if(count[i]>=3) {
					count[i] = count[i]-3;
					tri++;
				}
			}
			for(int i=0; i<count.length ; i ++) {
				if(count[i]==6) {
					tri = 2;
					break;
				}
				if(count[i]>=3) {
					count[i] = count[i]-3;
					tri++;
				}
			}
			
			//run
			for(int i=0; i<count.length-2; i++) {
				if(count[i] >=1 && count[i+1] >=1 &&count[i+2] >=1) {
					run+=count[i];
				}
			}

			
			Answer = run+tri;
			System.out.println("#"+tc + " "+Answer/2);
		}
		br.close();
	}

	
}