package JungOl;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
class Main1863_종교_서울10반_이찬호{
	public static int []p ;
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1863.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
		String[] r = br.readLine().split(" ");
		HashSet<Integer> ans = new HashSet<>();
		int numStu = Integer.parseInt(r[0]);
		int numPair = Integer.parseInt(r[1]);
		p = new int [50001]; 
		for(int i =1; i<=numStu; i++) {
			p[i] = i;
		}
		
		for(int i=0; i<numPair; i++) {
			r = br.readLine().split(" ");
			int x = Integer.parseInt(r[0]);
			int y = Integer.parseInt(r[1]);
			if(x > y) unionSet(y,x);
			else unionSet(x,y);
		}
		//System.out.println(Arrays.toString(p));
		
		for(int i =1; i<=numStu; i++) {
			ans.add(findSet(p[i]));
		}
		System.out.println(ans.size());
		br.close();
	}
	
	
	public static int findSet(int x) {
		if(x == p[x]) return x;
		else return p[x] = findSet(p[x]);
	}
	
	public static void unionSet(int x, int y) {
		if( findSet(x)!= findSet(y))
			p[findSet(y)] = findSet(x);
	}
	

}