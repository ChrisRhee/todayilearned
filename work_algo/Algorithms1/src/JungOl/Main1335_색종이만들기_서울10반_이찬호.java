package JungOl;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
class Main1335_색종이만들기_서울10반_이찬호{
	public static int AnsWhite = 0;
	public static int AnsBlue = 0;
	public static void check(int[][] square, int x1, int y1, int n) { 
		int value = square[x1][y1]; 
		// 길이가 1이면 바로 체크 
		if (n == 1) { 
			if (value == 0) {
				AnsWhite++; } 
			else {
				AnsBlue++; }
			return; 
		} 
		boolean isOne = true; 
		// 사각형이 하나의 색인지 체크 
		for (int i = x1; i < x1 + n; i++) { 
			for (int j = y1; j < y1 + n; j++) {
				if (value != square[i][j]) {
					isOne = false; 
					break; 
				}
			} 
			if (!isOne) { break; } 
		} 
		// 하나의 색이면 색에 따라 체크
		if (isOne) {
			if (value == 0) { AnsWhite++; } 
			else { AnsBlue++; } 
			return; 
		} 
		else { 
			// 하나의 색이 아니면 4조각으로 나누어서 계산 
			check(square, x1, y1, n/2); 
			check(square, x1+n/2, y1, n/2); 
			check(square, x1, y1+n/2, n/2); 
			check(square, x1+n/2, y1+n/2, n/2); 
		} 
	}

	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1335.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		int cp[][] = new int[T][T];
		for(int i =0; i<T; i++) {
			String s[] = br.readLine().split(" ");
			for(int j =0; j<s.length; j++) {
				cp[i][j] = Integer.parseInt(s[j]);
			}
		}
		
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
		check(cp,0,0,T);
		System.out.println(AnsWhite);
		System.out.println(AnsBlue);
		br.close();
	}
}