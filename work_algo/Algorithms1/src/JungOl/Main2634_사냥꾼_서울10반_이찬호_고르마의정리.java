package JungOl;
import java.io.*;
import java.util.*;
class Main2634_사냥꾼_서울10반_이찬호_고르마의정리{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main2634.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		// 초기화
		ArrayList<int[]> Ns = new ArrayList<>();
		String s[] = br.readLine().split(" ");
		int M = Integer.parseInt(s[0]);
		int N = Integer.parseInt(s[1]);
		int L = Integer.parseInt(s[2]);
		s = br.readLine().split(" ");
		int Ms[] = new int[s.length];
		int count =0;
		
		for(int i=0; i<M; i++) {
			Ms[i] = Integer.parseInt(s[i]);
		}
		
		for(int i=0; i<N; i++) {
			s = br.readLine().split(" ");
			int si = Integer.parseInt(s[0]);
			int sj = Integer.parseInt(s[1]);
			Ns.add(new int[] {si,sj});
		}
		
		Arrays.sort(Ms);
		Collections.sort(Ns, new Comparator<int []>() {
			@Override
			public int compare(int[] o1, int[] o2) {
				return o1[0]-o2[0];
			}
		});
		
		for(int i=0; i<N; i++) {
			if(Ns.get(i)[1] > L) continue;
		}
		
		System.out.println(count);
		
		br.close();
	}
}