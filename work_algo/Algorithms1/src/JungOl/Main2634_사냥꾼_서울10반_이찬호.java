package JungOl;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
class Main2634_사냥꾼_서울10반_이찬호{
	public static Queue<int[]> q;
	public static int[][] visit;
	public static int[] di = {-1,1,0,0};
	public static int[] dj = {0,0,-1,1};
	public static int[][] map;
	public static int count = 0;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main2634.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		// 초기화
		q = new LinkedList<>();
		
		// 파일에서 입력 읽어오기
		String s[] = br.readLine().split(" ");
		int M = Integer.parseInt(s[0]);
		int N = Integer.parseInt(s[1]);
		int L = Integer.parseInt(s[2]);
		s = br.readLine().split(" ");
		int Ms[] = new int[s.length];
		
		int max = 0;
		for(int i=0; i<M; i++) {
			Ms[i] = Integer.parseInt(s[i]);
			if (max < Ms[i]) max = Ms[i];
		}
		
		int Ns[][] = new int[N][2];
		
		for(int i=0; i<N; i++) {
			s = br.readLine().split(" ");
			int si = Integer.parseInt(s[0]);
			int sj = Integer.parseInt(s[1]);
			Ns[i][0] = si;
			Ns[i][1] = sj;
			if (max < si) max = si;
			if (max < sj) max = sj;
		}
		//map = new int[max+1][max+1];
		map = new int[max+1][max+1];
		for(int i=0; i<N; i++) map[Ns[i][0]][Ns[i][1]] = 1;
		
//		for (int[] is : map) {
//			System.out.println(Arrays.toString(is));
//		}
//		System.out.println();
		for(int i=0; i<M; i++) bfs(Ms[i],L,max);
		
		System.out.println(count);
		br.close();
	}

	private static void bfs(int i, int len, int im) {
		q.offer(new int[] {i,1});
		visit = new int[im+1][im+1];
		visit[i][1] = 1;
		while(!q.isEmpty()) {
			int temp = len;
			int[] curr = q.poll();
			for(int d =0; d<4; d++) {
				int ii= curr[0] + di[d];
				int jj= curr[1] + dj[d];
				if(ii > im || ii <= 0 || jj > im || jj <= 0) continue;
				if(visit[ii][jj] == 0 && visit[curr[0]][curr[1]] < len) {
					visit[ii][jj] = visit[curr[0]][curr[1]]+1;
					q.offer(new int[] {ii,jj});
					count += map[ii][jj];
					map[ii][jj] = 0;
				}
			}
		}
	}
}