package JungOl;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Main1828_냉장고_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1828.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int ans =0;
		int N= Integer.parseInt(br.readLine());
		int max =0;
		int maxIndex =0;
		ArrayList<int[]> temp = new ArrayList<>();
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
		for(int i=0; i<N; i++) {
			String n[] = br.readLine().split(" ");
			temp.add(new int[] {Integer.parseInt(n[0]), Integer.parseInt(n[1])});
		}
		
		Collections.sort(temp, new Comparator<int[]>() {
			@Override
			public int compare(int[] o1, int[] o2) {
				return o1[0]-o2[0];
			}
		});
		
		int index = 0;
		int high = temp.get(index)[1];
		for(int i=0; i<temp.size()-1; i++) {
			if(high < temp.get(i+1)[0]) {
				high = temp.get(i+1)[1];
				ans++;
				temp.remove(i);
			}else {
				temp.remove(i+1);
			}
		}
		
		System.out.println(ans+temp.size());
	}
}