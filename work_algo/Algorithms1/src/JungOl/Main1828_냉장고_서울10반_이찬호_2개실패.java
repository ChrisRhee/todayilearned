package JungOl;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class a implements Comparator<int []>{

	@Override
	public int compare(int[] o1, int[] o2) {
		return o1[0]-o2[0];
	}
	
}
class Main1828_냉장고_서울10반_이찬호_10개실패{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1828.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int ans =0;
		int N= Integer.parseInt(br.readLine());
		int max =0;
		int maxIndex =0;
		ArrayList<int[]> temp = new ArrayList<>();
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
		for(int i=0; i<N; i++) {
			String n[] = br.readLine().split(" ");
			temp.add(new int[] {Integer.parseInt(n[0]) + 270, Integer.parseInt(n[1])+270});
		}
		int ref[] = new int[10271];	
		
		for(int i=0; i<N; i++) {
			for(int j=temp.get(i)[0]; j<=temp.get(i)[1]; j++) {
				ref[j]++;
			}
		}
		Collections.sort(temp, new a());
		
		while(true) {
			for(int i=0; i< ref.length; i++) {
				if(max < ref[i]) {
					max = ref[i];
					maxIndex =i;
				}
			}
			if(max == 1) break;
			System.out.println(max+" "+maxIndex);
			
			for(int i=temp.size()-1; i>=0; i--) {
				if(temp.get(i)[0] <= maxIndex && maxIndex <= temp.get(i)[1]) {
					for(int j=temp.get(i)[0]; j<= temp.get(i)[1]; j++) {
						ref[j]--;
					}
					System.out.println(temp.get(i)[0]+" "+temp.get(i)[1]);
					temp.remove(i);
					max--;
				}
			}
			ans++;
		}
			
		
		
		System.out.println(ans+temp.size());
	}
}