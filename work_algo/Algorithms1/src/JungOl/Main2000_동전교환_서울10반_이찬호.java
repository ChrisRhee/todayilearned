package JungOl;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
class Main2000_동전교환_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/Jungol2000.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int N= Integer.parseInt(br.readLine());
		int Ans =0;
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
		int [] coin = new int[N];
		String[] s = br.readLine().split(" ");
		for(int i=0; i<s.length; i++) {
			coin[i] = Integer.parseInt(s[i]);
		}
		
		int money = Integer.parseInt(br.readLine());
		int[] C= new int[money+1];
		int min;
		for(int i=1; i<=money; i++) {
			min = 64000;
			for(int j=0; j<coin.length; j++) {
				if(i-coin[j]>=0) {
					if(min>C[i-coin[j]]) {
						min = C[i-coin[j]];
					}
				}
			}
			C[i] = min+1;
		}
		if(C[money] >= 64000) System.out.println("impossible");
		else System.out.println(C[money]);
		br.close();
	}
}