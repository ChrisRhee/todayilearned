package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
class Solution1240_단순2진암호코드_서울10반_이찬호{
	public static int getNum(String code) {
		int num = 0;
		switch(code) {
			case "1011000":
				num = 0;
				break;
			case "1001100":
				num = 1;
				break;
			case "1100100":
				num = 2;
				break;
			case "1011110":
				num = 3;
				break;
			case "1100010":
				num = 4;
				break;
			case "1000110":
				num = 5;
				break;
			case "1111010":
				num = 6;
				break;
			case "1101110":
				num = 7;
				break;
			case "1110110":
				num = 8;
				break;	
			case "1101000":
				num = 9;
				break;
		}
		return num;
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1240.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++)
		{
			int Ans =0;
			String[] s = br.readLine().split(" ");
			String ins = new String();
			String re = new String();
			int codeNum = 8;
			int []code = new int[codeNum];
			codeNum--;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			// 암호문 찾아오기
			for(int i=0; i<Integer.parseInt(s[0]); i++) {
				String t = br.readLine();
				if(t.contains("1"))
					ins = t;
			}
			
			// 암호문 뒤집기
			for(int i=ins.length()-1; i>=0; i--) 
				re += ins.charAt(i);
			
			int startI = 0;
			// 1로 시작하는 곳 찾기
			for(int i=0;i<re.length();i++) {
				if(re.charAt(i)=='1') {
					startI = i;
					break;
				}
			}
			
			// 그 1부터 7자리까지 잘라서 숫자로 저장하기
			for(int i = code.length-1 ; i>= 0 ; i--) {
				code[i] = getNum(re.substring(startI, startI+7));
				startI += 7;
			}
			
			// 암호코드 연산
			Ans = (code[0]+code[2]+code[4]+code[6])*3+code[1]+code[3]+code[5];
			
			// 잘된 암호인가 안된 암호인가
			if((Ans+code[7])%10 == 0) {
				Ans = code[0]+code[1]+code[3]+code[2]+code[4]+code[5]+code[6]+code[7];
			}else {
				Ans =0;
			}
			
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
}