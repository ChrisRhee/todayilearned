package SWExpertAcademy;
import java.util.Scanner;
import java.util.Stack;
import java.io.FileInputStream;
class Solution1222_계산기1_서울10반_이찬호{
	
	public static char[] stack = new char[1000];
	public static int top= -1;
	
	public static int[] intStack = new int[1000];
	public static int intTop = -1;
	
	public static int getIcp(char c) {
		switch(c) {
			case '+':
			case '-':
				return 1;
			case '*':
			case '/':
				return 2;
			case '(':
				return 3;
		}
		return 0;
	}
	
	public static int getIsp() {
		char c =(top==-1)? '(' : stack[top];
		switch(c) {
			case '+':
			case '-':
				return 1;
			case '*':
			case '/':
				return 2;
			case '(':
				return 0;
		}
		return -1;
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1222.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int test_case = 1; test_case <= 10; test_case++)
		{
			int Answer =0;
			int T=sc.nextInt();
			String s = sc.next();
			
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < s.length(); i++) {
				char c = s.charAt(i);
				if('0'<=c && c<='9') {
					sb.append(c);
				}
				else if(c ==')') {
					char t;
					while( (t=stack[top--]) != '(') {
						sb.append(t);
					}
				}
				else {
					while(getIcp(c)<= getIsp()) {
						char t = stack[top--];
						sb.append(t);
					}
					stack[++top]= c;
				}
			}
			while(top!=-1) sb.append(stack[top--]);
		
			// ---------------------- 후입 연산자로 변환 완료
			
			s = sb.toString();
			for (int i = 0; i < s.length(); i++) {
				char c = s.charAt(i);
				if('0'<=c && c<='9')
					intStack[++intTop]= c-'0';
				else {
					int n2 = intStack[intTop--];
					int n1 = intStack[intTop--];
					int nn =0;
					switch(c) {
						case '+': nn = n1+n2; break;
						case '-': nn = n1-n2; break;
						case '*': nn = n1*n2; break;
						case '/': nn = n1/n2; break;
					}
					intStack[++intTop]=nn;
				}
			}
			Answer = intStack[intTop--];
			
			
			
			System.out.println("#"+test_case + " "+Answer);
		}
		sc.close();
	}
}
