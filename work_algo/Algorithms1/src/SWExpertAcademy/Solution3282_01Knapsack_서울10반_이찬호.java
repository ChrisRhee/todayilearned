package SWExpertAcademy;
import java.io.*;
import java.util.Arrays;
class Solution3282_01Knapsack_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3282.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			String r[] = br.readLine().split(" ");
			int num[] = new int[r.length];
			for(int i =0; i<r.length; i++) 
				num[i] = Integer.parseInt(r[i]);
			int N=num[0];
			int W=num[1];
			int [] wi = new int [N];     // Kg
			int [] vi = new int [N]; // 만원
			for(int i=0; i<N ; i++) {
				r = br.readLine().split(" ");
				wi[i] = Integer.parseInt(r[0]);
				vi[i] = Integer.parseInt(r[1]);
			}
			
			int [][] k_memoi = new int[N+1][W+1];
			for (int i = 1; i <= N; i++) {
				for (int j = 1; j <= W; j++) {
					if (wi[i - 1] > j) k_memoi[i][j] = k_memoi[i-1][j];
					else k_memoi[i][j] = Math.max(vi[i-1]+k_memoi[i-1][j-wi[i-1]], k_memoi[i-1][j]);
				}
			}
			System.out.println("#"+tc + " "+k_memoi[N][W]);
			for (int[] is : k_memoi) {
				System.out.println(Arrays.toString(is));
			}
		}
		br.close();
	}
}