package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
class Solution1251_하나로_Prim_서울10반_이찬호{
	public static int N;
	public static double E;
	public static long Graph[][];
	public static long[] Weight;
	public static int xNum[], yNum[];
	
	public static long prim() {
		for(int i=0; i<N; i++) {
			Weight[i] = -1;
		}
		Weight[0] = 0;
		for(int k=1; k<N; k++) {
			long minWeight = Long.MAX_VALUE;
			int minVertax= 0;
			for(int i=0; i<N; i++) {
				if(Weight[i] < 0) continue;
				for(int j=0; j<N; j++) {
					if(Weight[j] >= 0 )continue;
					// graph값중 제일 작은 가중치를 잡아야한다. 
					if(Graph[i][j]<minWeight) {
						minVertax = j;
						minWeight = Graph[i][j];
					}
				}
			}
			Weight[minVertax]= minWeight;
		}
		long sumCost =0;
		for(int i=0; i<N; i++) {sumCost += Weight[i];
		}
		return (long)(sumCost*E+0.5);
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1251.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			
			N= Integer.parseInt(br.readLine());
			Graph = new long[N][N];
			Weight = new long [N];
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			String x[] = br.readLine().split(" ");
			String y[] = br.readLine().split(" ");
			xNum = new int[N];
			yNum = new int[N];
			for(int i =0; i<N; i++) {
				xNum[i] = Integer.parseInt(x[i]);
				yNum[i] = Integer.parseInt(y[i]);
			}
			E= Double.parseDouble(br.readLine());
			
			for(int i=0; i<N; i++) {
				for(int j =0; j<N; j++) {
					if(i==j)continue;
					long xx = xNum[j] - xNum[i];
					long yy = yNum[j] - yNum[i];
					Graph[i][j] = xx*xx + yy*yy;
				}
			}
			long ans = prim();
			System.out.println("#"+tc + " "+ans);
		}
		br.close();
	}
}