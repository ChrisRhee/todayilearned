package SWExpertAcademy;
import java.io.*;
import java.util.HashSet;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 학생들이 받을 수 있는 점수로 가능한 경우의 수 구하기
 * 조건
 * 	- 문제 개수와 각각 배점이 주어진다.
 * 입력
 *  - 테스트 케이스
 *  - 
 * 출력
 * 	- 가능한 경우의 수
 * 풀이 
 * 	- 조합을 사용해보자.
 * */
class Solution3752_가능한시험점수_서울10반_이찬호{
	public static int data[]; 
	public static int n;
	public static HashSet<Integer> h;
	public static void subset() {
		for (int i = 1; i < (1 << n); i++) {
			int sum = 0;
			for (int j = 0; j < n; j++) {
				if ((i & (1 << j)) > 0) {
					System.out.print(data[j] + " ");
					sum += data[j];
				}
			}
			h.add(sum);
			System.out.println();
		}
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3752.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			int N= Integer.parseInt(br.readLine());
			
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			String s[] = br.readLine().split(" ");
			h = new HashSet<>();
			data = new int[N];
			n = N;
			for(int i =0; i<N; i++) {
				data[i] = Integer.parseInt(s[i]);
			}
			subset();
			h.add(0);
			System.out.println("#"+tc + " "+h.size());
		}
		br.close();
	}
}