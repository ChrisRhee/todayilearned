package SWExpertAcademy;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
class Solution1230_암호문3_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1230.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int test_case = 1; test_case <= 10; test_case++)
		{
			List<Integer> list = new ArrayList<>();
			String Answer ="";
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			
			int codeNum = sc.nextInt();
			for(int i=0; i<codeNum; i ++) {
				list.add(sc.nextInt());
			}
			int commandNum = sc.nextInt();
			for(int i=0; i<commandNum; i++) {
				String command = sc.next();
				
				switch(command) {
				case "I":{
					int start = sc.nextInt();
					int comNum = sc.nextInt();
					for(int j=0; j<comNum; j++) {
						list.add(start+j, sc.nextInt());
					}
					break;
				}
				case "D":{
					int start = sc.nextInt();
					int comNum = sc.nextInt();
					for(int j=0; j<comNum; j++) {
						list.remove(start);
					}
					break;
				}
				case "A":
					int comNum = sc.nextInt();
					for(int j=0; j<comNum; j++) {
						list.add(sc.nextInt());
					}
					break;
				}
				
			}
			
			for(int i=0; i<10; i++) Answer += list.get(i)+" ";
			
			System.out.println("#"+test_case + " "+Answer);
		}
		sc.close();
	}
}