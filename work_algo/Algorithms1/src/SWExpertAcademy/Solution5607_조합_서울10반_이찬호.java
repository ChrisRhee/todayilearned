package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
class Solution5607_조합_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input5607.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		
		for(int test_case = 1; test_case <= T; test_case++)
		{
			long Answer =1;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			int mod = 1234567891; //1234567891;
			int mods = mod-2;
			String modNum[] = Integer.toBinaryString((mod-2)).split("");
			String s[] = br.readLine().split(" ");
			int firstNum = Integer.parseInt(s[0]);
			long a=1,b=1;
			for(int i =1; i<= Integer.parseInt(s[1]) ; i++) {
				a *= firstNum--;
				a = a% mod;
				b = b * i % mod;
			}
			Answer = Answer * a;
			for(int i=modNum.length-1; i>=0 ; i--) {
				if(Integer.parseInt(modNum[i])==1) Answer = Answer * b % mod;
				b = b * b % mod;
			}
			
			System.out.println("#"+test_case + " "+Answer);
		}
		br.close();
	}
}