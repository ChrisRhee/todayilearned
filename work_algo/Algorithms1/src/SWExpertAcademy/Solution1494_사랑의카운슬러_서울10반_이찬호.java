package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
class Solution1494_사랑의카운슬러_서울10반_이찬호{
	public static int N;
	public static Long min ;
	public static ArrayList<Long[]> swList;
	public static ArrayList<Long[]> temp;
	public static int n;
	public static int r;
	
	private static void comb(int n, int r) {
		if(r==0) {
			int ax =0;
			int ay =0;
			//System.out.println(temp);
			for(int i=0; i<temp.size()-1; i++) {
				ax+= temp.get(i)[0] - temp.get(i+1)[0];
				ay+= temp.get(i)[1] - temp.get(i+1)[1];
			}
			if(min > vec(ax,ay)) min = vec(ax,ay);
			return;
		}
		if(n<r) return;
		if(temp.size() < swList.size()/2)
			temp.add(swList.get(n-1));
		else temp.set(r-1, swList.get(n-1));
		
		comb(n-1, r-1);
		comb(n-1, r);
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1494.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			min = Long.MAX_VALUE;
			N= Integer.parseInt(br.readLine());
			swList = new ArrayList<>();
			temp = new ArrayList<>();
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			long num[][] = new long[N][2];
			for(int i =0; i<N; i++) {
				String r[] = br.readLine().split(" ");
				swList.add(new Long[] {Long.parseLong(r[0]),Long.parseLong(r[1])} );
			}
			comb(swList.size(), swList.size()/2);
			
			System.out.println("#"+tc + " "+min);
		}
		br.close();
	}
	
	public static long vec(long x, long y) {
		return x*x+y*y;
	}
}