package SWExpertAcademy;
import java.io.*;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			String s[] = br.readLine().split(" ");
			int num[] = new int[s.length];
			for(int i =0; i<s.length; i++) {
				num[i] = Integer.parseInt(s[i]);
			}
			
			
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
}