package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
class Solution6485_삼성시의버스노선_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input6485.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			int bus= Integer.parseInt(br.readLine());
			
			int ways[] = new int[5001];
			for(int i =0; i<bus; i++) {
				String r[] = br.readLine().split(" ");
				for(int j = Integer.parseInt(r[0]); j<= Integer.parseInt(r[1]); j++) {
					ways[j] ++;
				}
			}
			System.out.print("#"+tc + " ");
			
			int wayNum= Integer.parseInt(br.readLine());
			for(int i=0; i<wayNum; i++) {
				System.out.print(ways[Integer.parseInt(br.readLine())]+" ");
			}
			
			System.out.println();
			
		}
		br.close();
	}
}