package SWExpertAcademy;
import java.io.*;
import java.util.*;
class Solution1258_행렬찾기_서울10반_이찬호{
	public static int[] di= {-1,1,0,0};
	public static int[] dj= {0,0,-1,1};
	public static int[][] map;
	public static int N;
	public static boolean[][] visit;
	public static Stack<int[]> s;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1258.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			StringBuilder sb = new StringBuilder();
			N= Integer.parseInt(br.readLine());
			ArrayList<int[]> ansList = new ArrayList<>();
			map = new int[N][N];
			visit = new boolean[N][N];
			s= new Stack<>();
			for(int i =0; i<N; i++) {
				String r[] = br.readLine().split(" ");
				for(int j =0; j<N; j++) {
					map[i][j] = Integer.parseInt(r[j]);
				}
			}
			
			for(int i =0; i<N; i++) {
				for(int j =0; j<N; j++) {
					if(visit[i][j] == true || map[i][j] == 0) continue;
					ansList.add(dfs(i,j));
				}
			}
			
			Collections.sort(ansList, new Comparator<int[]>() {
				@Override
				public int compare(int[] o1, int[] o2) {
					if( o1[0]-o2[0] == 0) return o2[1]-o1[1];
					return o1[0]-o2[0];
				}
			});
			
			sb.append("#").append(tc).append(" ").append(ansList.size()).append(" ");
			//System.out.println("#"+tc+" "+ansList.size()+" ");
			for (int i = 0; i < ansList.size(); i++) {
				//System.out.println(ansList.get(i)[2]+" "+ansList.get(i)[2]+" ");
				sb.append(ansList.get(i)[2]).append(" ").append(ansList.get(i)[1]).append(" ");
			}
			//System.out.println();
			System.out.println(sb.toString());
		}
		br.close();
	}
	private static int[] dfs(int i, int j) {
		int count =1;
		int width =0;
		int height =0;
		s.push(new int[] {i,j});
		visit[i][j] = true;
		while(!s.isEmpty()) {
			int curr[] = s.pop();
			for(int d= 0; d<4; d++) {
				int ii = curr[0] + di[d];
				int jj = curr[1] + dj[d];
				if(ii >= N || ii< 0 || jj >= N || jj< 0) continue;
				if(d==3 && ii == i) width++;
				if(d==1 && jj == j) height++;
				if(visit[ii][jj] == false && map[ii][jj] !=0) {
					s.push(new int[] {ii,jj});
					visit[ii][jj] = true;
					count ++;
				}
			}
		}
		return new int[] {count,width,height};
	}
}