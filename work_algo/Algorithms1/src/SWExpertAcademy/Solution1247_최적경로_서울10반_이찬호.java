package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
class Solution1247_최적경로_서울10반_이찬호{
	public static int[] d= {1,2,3,4};
	public static int cnt;
	public static int minNum= 10000;
	
	public static int[] swap(int[] map, int i, int k) {
		int tempX = map[i*2] ;
		map[i*2] = map[k*2];
		map[k*2] = tempX;
		int tempY = map[i*2+1];
		map[i*2+1] = map[k*2+1];
		map[k*2+1] = tempY;
		return map;
	}
	
	private static void perm(int[] map, int n, int k) {
		int dis = 0;
		if(k==n) {
			//System.out.println(Arrays.toString(map));
			dis = Math.abs(map[0]-map[4])+Math.abs(map[1]-map[5]);
			//System.out.println(dis);
			for(int i=2; i<(map.length/2)-1; i++) {
				dis += Math.abs(map[i*2]-map[(i+1)*2])+Math.abs(map[i*2+1]-map[(i+1)*2+1]);
				if(dis > minNum) return;
			}
			dis += Math.abs(map[2]-map[map.length-2])+Math.abs(map[3]-map[map.length-1]);
			if(minNum > dis) minNum = dis;
		}else {
			for(int i=k; i<=n; i+=1) {
				//System.out.println(cnt);
				swap(map,i,k);
				perm(map,n,k+1);
				swap(map,i,k);
			}
		}
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1247.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++)
		{
			int num = Integer.parseInt(br.readLine());
			String s[] = br.readLine().split(" ");
			int[] map = new int[s.length];
			// 숫자로 옮겼다.
			for(int i =0; i<s.length; i++) {
				map[i] = Integer.parseInt(s[i]);

			}
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			
			perm(map, (map.length/2)-1, 2);
			
			System.out.println("#"+tc + " "+minNum);
			minNum = 10000;
		}
		br.close();
	}

	
}