package SWExpertAcademy;
import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 숫자 만들기 게임
 * 조건
 * 	- 시작수는 자연수 1 <= N <= 99999
 *  - 시작 수의 사이를 터치하면 수는 두개 또는 그 이상으로 쪼개져
 *  - 쪼개진 수를 모두 곱한다
 *  - 곱한수가 10 이상이면 2~3을 반복한다.
 *  - 한번 쪼개지면 turn 수가 증가 한다.
 *  - 최대 턴수가 나오도록 한다.
 * 입력
 *  - 테스트 케이스 
 *  - 시작수 입력
 * 출력
 * 	- 턴수
 * 풀이 
 * 	- 
 * */
class Solution7206_숫자게임_서울10반_이찬호2{
	public static int d[] = {0,1,2,3};
	public static int t[];
	public static int N,R,ss;
	public static int count = 0;
	public static int[] memo;

	public static void nCr(int n, int r, int num) {
		if (r == 0) {
			int temp = num;
			while(true) {
				if(temp < 10) break;
				
				// 일단 템프 하나 증가
				memo[temp]++;
				
				// calc 해봄
				temp = calc("" + temp, t);
				System.out.println(num+" "+temp);
			}
			return;
		}
		if (n < r)
			return;
		t[r - 1] = d[n - 1];
		nCr(n - 1, r - 1, num);
		nCr(n - 1, r, num);
	}

	private static int calc(String s, int[] t2) {
		int temp[] = new int[t2.length];
		for (int i = 0; i < t2.length; i++) 
			temp[i] = t2[i];
		int ans = 1;
		int end = 0;
		int len = 0;
		for (int i = 0; i < temp.length; i++) {
			if (i > 0) temp[i] = temp[i] - (temp[i - 1] + 1);
			len = s.substring(0, temp[i] + 1).length();
			ans = ans * Integer.parseInt(s.substring(0, temp[i] + 1));
			s = s.substring(temp[i] + 1);
			end = Integer.parseInt(s);
			if (i > 0) temp[i] = temp[i] + (temp[i - 1] + 1);
		}
		ans = ans * end;
		return ans;
	}
	
	public static void main(String args[]) throws Exception {
		System.setIn(new FileInputStream("res/input7206.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int T = Integer.parseInt(br.readLine());
		int nn= 2000;
		memo = new int[nn];
		
		for (int i = 0; i < nn; i++) {
			int sl = (""+i).length();
			d = new int[sl];
			for(int j=0; j<sl-1; j++) {
				d[j] = j;
			}
			
			N = sl - 1;
			for (int j = 1; j <= N; j++) {
				R = j;
				t = new int[R];
				nCr(N, R, i);
			}
		}
		
		for (int tc = 1; tc <= T; tc++) {
			ss = Integer.parseInt(br.readLine());
			System.out.println("#" + tc + " " + memo[ss]);
		}
		br.close();
	}
}