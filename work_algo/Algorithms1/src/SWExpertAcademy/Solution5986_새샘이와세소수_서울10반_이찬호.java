package SWExpertAcademy;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

class Solution5986_새샘이와세소수_서울10반_이찬호 {
	public static void main(String args[]) throws Exception {
		System.setIn(new FileInputStream("res/input5986.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine());
		
		int[] t = new int[1000];
		
		for (int i = 2; i < t.length; i++)
			t[i] = i;
		t[1] = 0;
		for (int i = 2; i < t.length; i++) {
			if (t[i] == 0)
				continue;
			for (int j = i + i; j < t.length; j += i)
				t[j] = 0;
		}
		
		for (int tc = 1; tc <= T; tc++) {
			int Ans = 0;
			int N = Integer.parseInt(br.readLine());

			for (int i = 0; i < t.length; i++) {
				if (t[i] == 0)
					continue;
				for (int j = i; j < t.length; j++) {
					if (t[j] == 0)
						continue;
					int rest = N - t[i] - t[j];
					if (rest <= 0)
						continue;
					if (rest >= j && t[rest] != 0)
						Ans++;
				}
			}

			System.out.println("#" + tc + " " + Ans);
		}
		br.close();
	}
}
