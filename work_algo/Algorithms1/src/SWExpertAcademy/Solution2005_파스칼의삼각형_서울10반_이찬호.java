package SWExpertAcademy;
import java.io.*;
class Solution2005_파스칼의삼각형_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input2005.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			int N= Integer.parseInt(br.readLine());
			System.out.println("#"+tc);
			int pas[][]= new int[N][N];
			pas[0][0] = 1;
			for(int i =0; i<N; i++) {
				for(int j= 0; j<=i; j++) {
					if(j==0 || j == i) {
						pas[i][j]= 1;
					}else {
						if(pas[i-1][j]!=0)
							pas[i][j]= pas[i-1][j]+pas[i-1][j-1];
					}
				}
			}
			for(int i =0; i<N; i++) {
				for(int j= 0; j<N; j++) {
					if(pas[i][j]==0) continue;
					System.out.print(pas[i][j]+" ");
				}
				System.out.println();
			}
		}
		br.close();
	}
}