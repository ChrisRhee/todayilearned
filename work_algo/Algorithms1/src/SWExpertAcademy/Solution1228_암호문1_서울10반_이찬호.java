package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
class Solution1228_암호문1_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1228.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		for(int test_case = 1; test_case <= 10; test_case++)
		{
			List<String> totalList = new ArrayList<>();
			String Answer ="";
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			br.readLine();
			// 기존 코드 리스트
			String total[] = br.readLine().trim().split(" ",-1);
			br.readLine();
			String commandList[] = br.readLine().trim().split("I",-1);
			for (String string : total) totalList.add(string);
			
			for(int i =1; i< commandList.length; i++) {
				String commands[] = commandList[i].trim().split(" ",-1);
				
				// 다 밀었으면 그 자리에 암호문 넣기
				for(int j = Integer.parseInt(commands[1])-1; j >=0; j--) {
					totalList.add(Integer.parseInt(commands[0]),commands[2+j]);
				}
			}
			
			for (int i =0; i< 10; i++) Answer += totalList.get(i)+" ";
			System.out.println("#"+test_case + " "+Answer);
		}
		br.close();
		
	}
}