package SWExpertAcademy;
import java.io.FileInputStream;
import java.util.Scanner;
class Solution1209_Sum_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1209.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int q=0; q<10; q++) {
			
			int T=sc.nextInt();
			
			int Answer =0;
			int data[][] = new int[100][100];
			for(int i=0; i<100; i++) {
				for(int j=0; j<100; j++) {
					data[i][j] = sc.nextInt();
				}
			}
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			
			// sum구하기
			for(int i=0; i<100; i++) {
				int sum1 = 0,sum2=0,sum3 =0, sum4=0;
				for(int j=0; j<100; j++) {
					sum1 += data[i][j];
					sum2 += data[j][i];
					if(i == j) sum3  += data[i][j];
					if(i == 99-j) sum4  += data[i][j];
				}
				if(Answer < sum1) Answer = sum1;
				if(Answer < sum2) Answer = sum2;
				if(Answer < sum3) Answer = sum3;
				if(Answer < sum4) Answer = sum4;
			}
			
			System.out.println("#"+	T + " "+Answer);
		}
		sc.close();
	}
}