package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
class Solution1266_소수완제품확률_서울10반_이찬호{
	public static Double[] sosu = {2.0, 3.0, 5.0, 7.0, 11.0, 13.0, 17.0};
	
	private static double nCr(Double n, Double r) {
		if(r==0) return 1.0;
		return 1.0*n/r*nCr(n-1,r-1);
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1266.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++)
		{
			double a =0;
			double b =0;
			String s[] = br.readLine().split(" ");
			double A = Integer.parseInt(s[0]);
			double B = Integer.parseInt(s[1]);
			for(int i=0; i<sosu.length ; i++) {
				double r = sosu[i];
				a += nCr(18.0, r) * Math.pow((A/100), r)* Math.pow(((100-A)/100), 18-r);
				b += nCr(18.0, r) * Math.pow((B/100), r)* Math.pow(((100-B)/100), 18-r);
			}
			System.out.printf("#%d %.6f\n",tc,(a+b)-(a*b));
		}
		br.close();
	}
}