package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
class Solution1231_중위순회_서울10반_이찬호{
	public static String list[];
	public static int T;
	public static String Answer = "";
	
	public static void inorder(int i) {
		if(i<=T && list[i]!=null) {
			inorder(2*i);
			Answer += list[i];
			inorder(2*i+1);
		}
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1231.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		for(int test_case = 1; test_case <= 10; test_case++)
		{
			T= Integer.parseInt(br.readLine());
			list = new String[T+1];
			String node[] ;
			for(int i=1; i<=T;i++) {
				node = br.readLine().split(" ",-1);
				list[i] = node[1];
			}
			System.out.println(Arrays.toString(list));
			inorder(1);
			System.out.println("#"+test_case + " "+ Answer);
			Answer = "";
		}
		br.close();
	}
}