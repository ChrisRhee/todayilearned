package SWExpertAcademy;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;
class Solution1213_String_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1213.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int test_case = 1; test_case <= 10; test_case++)
		{
			int T=sc.nextInt();
			int Answer =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			
			String word = sc.next();
			String str = sc.next();
			String a[] = str.split(word,-1);
			Answer = a.length-1;
			
			System.out.println("#"+T + " "+Answer);
		}
		sc.close();
	}
}