package SWExpertAcademy;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * 배틀필드 개발 전차는 사용자 전차 1개 방향키로 방향 바꾸고 1칸 전진 총을 쏘면 강철 판은 안뚫림. 돌은 총맞으면 땅이 됨. 맵 밖으로
 * 나가면 ㅂㅂ 막혀있거나, 맵 밖이면 전차 이동 불가 초기 맵상태가 주어지고, 입력순서대로 입력 다 하고 났을때 맵 상태를 구해라. S는
 * 슛, UDLR 위아래 왼쪽오른쪽
 */

class Solution1873_상호의배틀필드_서울10반_이찬호 {
	// 상 하 좌 우
	public static int[] di = { -1, 1, 0, 0 };
	public static int[] dj = { 0, 0, -1, 1 };
	public static int ii, jj;
	public static int[] tank;
	public static String[][] map;

	public static void main(String args[]) throws Exception {
		System.setIn(new FileInputStream("res/input1873.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine());
		for (int tc = 1; tc <= T; tc++) {
			int Ans = 0;
			String r[] = br.readLine().split(" ");
			ii = Integer.parseInt(r[0]);
			jj = Integer.parseInt(r[1]);
			map = new String[ii][jj];
			tank = new int[3];

			for (int i = 0; i < ii; i++) {
				String s[] = br.readLine().split("");
				for (int j = 0; j < jj; j++) {
					map[i][j] = s[j];
					switch (s[j]) {
					case "^":
						tank[0] = i;
						tank[1] = j;
						tank[2] = 0;
						break;
					case "v":
						tank[0] = i;
						tank[1] = j;
						tank[2] = 1;
						break;
					case "<":
						tank[0] = i;
						tank[1] = j;
						tank[2] = 2;
						break;
					case ">":
						tank[0] = i;
						tank[1] = j;
						tank[2] = 3;
						break;
					}
				}
			}
			int c = Integer.parseInt(br.readLine());
			String coms[] = br.readLine().split("");
			for (int i = 0; i < c; i++) {
				int ai = tank[0] + di[tank[2]];
				int aj = tank[1] + dj[tank[2]];
				switch (coms[i]) {
				case "S": {
					while (true) {
						if (ai < 0 || ai >= ii || aj < 0 || aj >= jj)
							break;
						if (map[ai][aj].equals(".") || map[ai][aj].equals("-")) {
							ai = ai + di[tank[2]];
							aj = aj + dj[tank[2]];
						} else if (map[ai][aj].equals("*")) { // 벽돌이면 뿌수고 멈춤
							map[ai][aj] = ".";
							break;
						} else {
							break;
						}
					}
					break;
				}
				case "U":
					tank[2] = 0;
					if (move(tank[2]) == 0)
						break;
					break;
				case "D":
					tank[2] = 1;
					if (move(tank[2]) == 0)
						break;
					break;
				case "L":
					tank[2] = 2;
					if (move(tank[2]) == 0)
						break;
					break;
				case "R":
					tank[2] = 3;
					if (move(tank[2]) == 0)
						break;
					break;
				}
			}

			System.out.print("#" + tc + " ");
			for (int i = 0; i < ii; i++) {
				for (int j = 0; j < jj; j++) {
					System.out.print(map[i][j]);
				}
				System.out.println();
			}
		}
		br.close();
	}

	private static int move(int direction) {
		int ai = tank[0] + di[direction];
		int aj = tank[1] + dj[direction];
		if (ai < 0 || ai >= ii || aj < 0 || aj >= jj || map[ai][aj].equals("-") || map[ai][aj].equals("*")
				|| map[ai][aj].equals("#")) {
			switch (direction) {
			case 0:
				map[tank[0]][tank[1]] = "^";
				break;
			case 1:
				map[tank[0]][tank[1]] = "v";
				break;
			case 2:
				map[tank[0]][tank[1]] = "<";
				break;
			case 3:
				map[tank[0]][tank[1]] = ">";
				break;
			}
			return 0;
		}

		if (map[ai][aj].equals(".")) {
			map[tank[0]][tank[1]] = ".";
			tank[0] = ai;
			tank[1] = aj;
			switch (direction) {
			case 0:
				map[ai][aj] = "^";
				break;
			case 1:
				map[ai][aj] = "v";
				break;
			case 2:
				map[ai][aj] = "<";
				break;
			case 3:
				map[ai][aj] = ">";
				break;
			}
		}
		return 1;
	}
}