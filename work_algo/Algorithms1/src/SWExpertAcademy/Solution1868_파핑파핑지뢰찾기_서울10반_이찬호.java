package SWExpertAcademy;
import java.io.*;
import java.util.*;

/**
 * 작성자 : 이찬호
 * 문제
 *  - 지뢰찾기에서 최소 몇번을 눌러야 해결되는지 찾기
 * 조건
 * 	- 각칸에는 지뢰가 있을 수도 있고 없을 수도 있다.
 * 	- 지뢰가 없는 칸이면  맞닿아 있는 최대 8칸에 대해 몇 개의 지뢰가 있는지 0~8숫자로 표시
 * 	- 숫자가 0이면 근처 8방향에 지뢰가 없는것 확정이므로, 주변 8칸도 자동 표시해준다.
 * 입력
 *  - NxN 크기의 N, 여백 . 지뢰* 
 * 출력
 * 	- 최소 눌러야 하는 횟수
 * 풀이 
 * 	- .(땅)들을 방문하면서 주변의 *(지뢰)들을 검색해서 해당 .(땅)에 지뢰 수로 초기화 한다.
 * 	- 처음부터 방문하면서 0인 곳을 찾아서 bfs로 0과 0주변을 다 *로 바꿔주면서 count를 증가시킨다.
 * 	- 0으로 클릭되는 곳을 전부 했다면, 남은 숫자들 개수를 count에 더해준다.
 * */

class Solution1868_파핑파핑지뢰찾기_서울10반_이찬호{
	public static int N,Ans;
	public static String[][] map;
	public static Queue<int[]> q;
	public static int[] di = {-1,-1,0,1,1,1,0,-1};
	public static int[] dj = {0,1,1,1,0,-1,-1,-1};
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1868.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			// 초기화
			Ans =0;
			N= Integer.parseInt(br.readLine());
			map = new String[N][N];
			q = new LinkedList<>();
			
			// map 배치
			for(int i =0; i<N; i++) {
				String s[] = br.readLine().split("");
				for(int j =0; j<N; j++) map[i][j] = s[j];
			}
			
			// 처음부터 돌면서 각 칸 주변의 지뢰를 찾아 해당칸을 지뢰 숫자로 초기화한다.
			for(int i =0; i<N; i++) {
				for(int j =0; j<N; j++) {
					if(map[i][j].equals(".")) {
						int mine = 0;
						for(int d= 0; d<di.length; d++) {
							int ii= i + di[d];
							int jj= j + dj[d];
							if(ii < 0 || ii >= N || jj < 0 || jj >= N) continue;
							//다음게 *이면
							if(map[ii][jj].equals("*")) mine++;
						}
						map[i][j] = ""+mine;
					}
				}
			}
			
			// 0이면 자기와 주변을 *로 만들자 -> 나중에 count쉽게 하기위해
			for(int i =0; i<N; i++) {
				for(int j =0; j<N; j++) {
					if(map[i][j].equals("0")) {
						Ans++;
						map[i][j] = "*";
						q.offer(new int[] {i,j});
						bfs(i,j);
					}
				}
			}
			
			// 나머지 숫자들 카운트
			for(int i =0; i<N; i++) {
				for(int j =0; j<N; j++) {
					if(map[i][j].equals("*")) continue;
					Ans++;
				}
			}
			
			//출력
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
	
	// bfs
	private static void bfs(int i, int j) {
		while(!q.isEmpty()) {
			int curr[] = q.poll();
			for(int d= 0; d<di.length; d++) {
				int ii= curr[0] + di[d];
				int jj= curr[1] + dj[d];
				if(ii < 0 || ii >= N || jj < 0 || jj >= N) continue;
				if(map[ii][jj].equals("*")) continue;
				if(map[ii][jj].equals("0")) {
					q.offer(new int[] {ii,jj});
				}
				map[ii][jj]="*";
			}
		}
	}
}