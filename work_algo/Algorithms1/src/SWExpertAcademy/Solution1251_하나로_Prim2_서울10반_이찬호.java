package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
class Solution1251_하나로_Prim2_서울10반_이찬호{
	public static int N;
	public static double E;
	public static long Graph[][];
	public static long[] Weight;
	public static int xNum[], yNum[];
	
	public static long prim() {
		PriorityQueue<long[]> pq = new PriorityQueue<long[]>(new Comparator<long[]>() {
			@Override
			public int compare(long[] o1, long[] o2) {
				return Long.compare(o1[0], o2[0]);
			}
		});
		boolean[] visited = new boolean[N];
		visited[0] = true;
		for(int next=0; next<N; next++) {
			if(Graph[0][next]!=0)
				pq.offer(new long[] {Graph[0][next], next} );
		}
		
		long sumCost = 0;
		for(int i=0; i<N-1; i++) {
			long[] currArr= pq.poll();
			int curr =(int)currArr[1];
			long weight=currArr[0];
			
			// 방문처리, 값 추가
			if(visited[curr]) {i--;	continue;}
			visited[curr] = true;
			sumCost += weight;
			
			for(int next=0; next<N; next++) {
				// 원래 최소값 찾아야하는데, - 를 0으로 해놔서 0 아닌것중에 최소값 구해야함
				
				if(Graph[curr][next]!=0)
					pq.offer(new long[]{Graph[curr][next],next});
			}
		}
		return (long)(sumCost*E+0.5);
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1251.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			
			N= Integer.parseInt(br.readLine());
			Graph = new long[N][N];
			Weight = new long [N];
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			String x[] = br.readLine().split(" ");
			String y[] = br.readLine().split(" ");
			xNum = new int[N];
			yNum = new int[N];
			for(int i =0; i<N; i++) {
				xNum[i] = Integer.parseInt(x[i]);
				yNum[i] = Integer.parseInt(y[i]);
			}
			E= Double.parseDouble(br.readLine());
			
			for(int i=0; i<N; i++) {
				for(int j =0; j<N; j++) {
					if(i==j)continue;
					long xx = xNum[j] - xNum[i];
					long yy = yNum[j] - yNum[i];
					Graph[i][j] = xx*xx + yy*yy;
				}
			}
			long ans = prim();
			System.out.println("#"+tc + " "+ans);
		}
		br.close();
	}
}