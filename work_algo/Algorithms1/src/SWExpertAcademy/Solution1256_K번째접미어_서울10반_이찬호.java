package SWExpertAcademy;
import java.io.*;
import java.util.*;
class Solution1256_K번째접미어_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1256.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			int N= Integer.parseInt(br.readLine());
			String k = br.readLine();
			ArrayList<String> a = new ArrayList();
			for(int i=0;i<k.length();i++) a.add(k.substring(i));
			a.sort(null);
			System.out.println("#"+tc + " " + a.get(N-1));
		}
	}
}