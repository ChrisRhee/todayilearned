package SWExpertAcademy;
import java.io.*;
import java.util.Arrays;

/**
 * @author Chris Rhee
 *	Problem) Bob ans Alice play Game.
 *
 *	Conditions) 
 *		- 양의 정수 N을 정하고, 1로 초기화 된 x를 가지고 있다.
 *		- Game start with Alice first
 *		- x -> 2x or 2x+1
 *		- if x > N then lose
 *		- 최선을 다해 게임을 한다면 -> 누가이김?
 *
 *	input) 
 *		- testCase T on first line
 *		- Natural Number(1 <= N <= 10^18) each testCase on first line
 *
 *	output)
 *		- winner of the game
 *
 *	solve)
 *		- bob*4^0, alice*4^1, bob*4^1, alice*4^2, bob*4^2, ....
 *		- make "ans" Array calculate an answer
 *		- 1 5 9 25 41 105 ...
 *		- 해당 숫자와 같거나 작으면 그 인덱스를 가져옴. 
 *		- 인덱스 %2 해서 짝수면 bob, 홀수면 alice 승리
 */
class Solution3459_승자예측하기_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3459.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		long[] fourTimes = new long[30];
		long[] ans = new long[59];
		fourTimes[0] = 1;
		ans[0]= 1;
		for(int i=1; i<30; i++) {
			fourTimes[i] = fourTimes[i-1]*4;
			ans[i*2-1] = ans[(i-1)*2]+fourTimes[i];
			ans[i*2] = ans[i*2-1]+fourTimes[i];
		}
		System.out.println(Arrays.toString(ans));
		for(int tc = 1; tc <= T; tc++){
			long N= Long.parseLong(br.readLine());
			int a = 0;
			for(int i=0; i<ans.length; i++) 
				if( N >= ans[i]) a = i; 
			if(N > ans[a]) a++; 
			if(a%2 == 1) System.out.println("#"+tc + " Alice");
			else System.out.println("#"+tc + " Bob");
		}
		br.close();
	}
}