package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
class Solution5515_요일맞추기_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input5515.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		int [] month= {0,
				31,29,31,30,31,
				30,31,31,30,31,
				30,31};
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			// 월 = 0 화 1 수 2 목 3 금 4 토 5 일 6
			String day[] = br.readLine().split(" ");
			for(int i=1; i< Integer.parseInt(day[0]); i++)
				Ans += month[i];
			System.out.println("#"+tc + " "+(Ans+Integer.parseInt(day[1])+3)%7);
		}
		br.close();
	}
}