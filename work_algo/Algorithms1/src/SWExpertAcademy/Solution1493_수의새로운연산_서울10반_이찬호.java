package SWExpertAcademy;
import java.io.*;

/**
 * 작성자 : 이찬호
 * 문제
 * 	- 수의 새로운 연산 ★ 구현하기
 * 조건
 * 	- 2차원 평면 제 1사분면 위의 격자점에 대각선 순서로 점에 수를 붙인다.
 * 	- 점(x,y)에 할당된 수는 #(x,y)로 나타낸다.
 * 	- 수 p 가 할당된 점을 &(p)로 나타는대.
 * 	- (x,y)+(z,w)=(x+z, y+w)로 정의
 * 	- p★q는 #(&(p)+&(q))
 * 입력
 *  - 테스트 케이스 수 T
 *  - 두 정수 p,q( 1 <= p, q <= 10000)
 * 출력
 * 	- p★q 한 값 출력.
 * 풀이 
 * 	- #(&(p)+&(q)) = (px+qx, py+qy) 에 할당된 수 구하기
 * 	- 그래프에 있는 수 규칙 찾기.
 *  - 찾아서 입력 받은 q에 대한 x,y좌표값, p에 대한 x,y좌표값을 구해서 서로 더하고,
 *  - 그 더한 값들이 위치하는 곳 찾기.
 *  - 시간 초과가 나므로, fun&값은 메모이션으로 값을 저장해서 바로바로 출력하게 한다.
 * */

class Solution1493_수의새로운연산_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1493.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		
		// 메모이제이션
		int Ans[][] = new int[10001][2];
		int aa[] = new int[2];
		for(int i =1; i<=10000; i++) {
			aa = fun8(i);
			Ans[i][0] = aa[0];
			Ans[i][1] = aa[1];
		}
		
		for(int tc = 1; tc <= T; tc++){
			// 입력 받기
			String s[] = br.readLine().split(" ");
			int p = Integer.parseInt(s[0]);
			int q = Integer.parseInt(s[1]);
			// 결과 출력
			System.out.println("#"+tc + " "+funShop(Ans[p][0]+Ans[q][0],Ans[p][1]+Ans[q][1]));
		}
		br.close();
	}

	// # 함수
	private static int funShop(int x, int y) {
		int temp = 0;
		for(int i=1; i<=x; i++) temp = temp + i;
		for(int i=1; i<y; i++) temp = temp + (i + x-1);
		return temp;
	}
	
	// & 함수
	private static int[] fun8(int q) {
		int count = 1;
		int xIndex = 1;
		int yIndex = 1;
		while(true) {
			if(q==count) break;
			if( yIndex == 1 ) {
				yIndex = xIndex + 1;
				xIndex = 1;
				count++;
			}else {
				yIndex--;
				xIndex++;
				count++;
			}
		}
		return new int[] {xIndex,yIndex};
	}
}