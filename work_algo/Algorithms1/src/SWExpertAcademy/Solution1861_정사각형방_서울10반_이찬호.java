package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;
/**
 * 남윤한 생일 축하해
 * 오늘 3월 5일은 남나니 생일
 * */

class Solution1861_정사각형방_서울10반_이찬호{
	public static int[][] map;
	public static int[][] v;
	public static boolean[][] v2;
	public static Queue<int[]> q;
	public static int[] di = {-1,1,0,0};
	public static int[] dj = {0,0,-1,1};
	public static int N;
	public static ArrayList<int[]> ans;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1861.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++)	{
			N= Integer.parseInt(br.readLine());
			q = new LinkedList<>();
			map = new int[N][N];
			v = new int[N][N];
			v2 = new boolean[N][N];
			ans = new ArrayList<>();
			int max = 0;
			for(int i =0; i<N; i++) {
				String[] s = br.readLine().split(" ");
				for(int j=0; j<N; j++) {
					map[i][j] = Integer.parseInt(s[j]);
				}
			}
			
			for(int i =0; i<N; i++) {
				for(int j=0; j<N; j++) {
					if(v2[i][j] == false) {
						v2[i][j] = true;
						dfs(i,j);
					}
				}
			}
			Collections.sort(ans, new Comparator<int[]>() {
				@Override
				public int compare(int[] o1, int[] o2) {
					int a = o1[0]-o2[0];
					if( a ==0 ) return o2[1]-o1[1];
					else return a;
				}
			});
			System.out.println("#"+tc + " "+ans.get(ans.size()-1)[1]+" "+ans.get(ans.size()-1)[0]);
		}
		br.close();
	}
	private static void dfs(int i, int j) {
		v = new int[N][N];
		int count =1;
		q.offer(new int[] {i,j});
		v[i][j] = 1;
		
		while(!q.isEmpty()) {
			int[] curr = q.poll();
			for(int d =0; d<4; d++) {
				int ii= curr[0] + di[d];
				int jj= curr[1] + dj[d];
				if(ii >= N || ii < 0 || jj >= N || jj < 0) continue;
				if(map[ii][jj]-map[curr[0]][curr[1]] != 1) continue;
				
				if(v[ii][jj] == 0 && map[ii][jj]-map[curr[0]][curr[1]]==1) {
					v2[ii][jj] = true;
					v[ii][jj] = v[curr[0]][curr[1]]+1;
					q.offer(new int[] {ii,jj});
					count++;
				}
			}
		}
		ans.add(new int[] {count,map[i][j]});
	}
}