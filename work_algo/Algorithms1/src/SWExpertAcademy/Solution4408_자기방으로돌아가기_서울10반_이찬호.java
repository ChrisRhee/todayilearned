package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Solution4408_자기방으로돌아가기_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input4408.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine().trim());
		for(int tc = 1; tc <= T; tc++){
			int N= Integer.parseInt(br.readLine().trim());
			int Ans =0;
			List<int[]> num = new ArrayList<>();
			int lan[] = new int [201];
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			for(int i =0; i<N; i++) {
				String r[] = br.readLine().split(" ");
				int s = Integer.parseInt(r[0]);
				int a = Integer.parseInt(r[1]);
				if(s > a) num.add(new int[] {a,s});
				else num.add(new int[] {s,a});
			}
			for (int i = 0; i < N; i++) {
				for(int j=(num.get(i)[0]+1)/2; j <= (num.get(i)[1]+1)/2; j++) {
					lan[j]++;
				}
			}
			Arrays.sort(lan);
			System.out.println("#"+tc + " "+lan[lan.length-1]);
		}
		br.close();
	}
}