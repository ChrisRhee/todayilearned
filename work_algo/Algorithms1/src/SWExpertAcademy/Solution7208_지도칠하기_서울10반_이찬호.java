package SWExpertAcademy;
import java.io.*;
import java.util.*;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 인접된 나라의 동일한 색을 최소 변경을 통해 지도의 모든나라 색 지정하기
 * 조건
 * 	- 최대 사용할 수 있는 색을 4가지
 * 	- 인접 국가가 동일 색이 나오지 않도록 모두 칠하기
 *  - 지도에 표시하는 나라 수 3 <= N <= 8
 * 입력
 *  - 테스트 케이스 T
 *  - 나라 수 N
 *  - 각 나라에 배정된 색상 값이 1~4까지 주어진다.
 *  - 국가간 인접 정보가 인접 행렬 N*N으로 주어짐
 *  - 인접된 국가는 1, 인접되지 않은 국가는 0
 * 출력
 * 	- 최소 변경 된 색의 수
 * 풀이 
 * 	- BFS로 각 노드를 한번씩 방문한다.
 *  - 방문한 노드와 연결 된 노드들과 색이 어떤지 검사한다.
 *  - 한 노드와 가장 많이 연결된 노드부터 탐색한다.
 * */
class Solution7208_지도칠하기_서울10반_이찬호{
	public static int N;
	public static int[][] map;
	public static int[] color,unUsedColor;
	public static ArrayList<int[]> aa;
	public static ArrayList<int[]> nextColor;
	public static int cnt;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input7208.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			cnt =0;
			N= Integer.parseInt(br.readLine());
			map = new int[N+1][N+1];
			aa = new ArrayList<>();
			nextColor = new ArrayList<>();
			String[] s = br.readLine().split(" ");
			color = new int[N+1];
			unUsedColor = new int[N+1];
			
			for(int i =1 ; i<=N; i++) {
				color[i]= Integer.parseInt(s[i-1]);
				unUsedColor[i] = i;
			}
			
						
			for(int i =1 ; i<=N; i++) {
				s = br.readLine().split(" ");
				for(int j =1; j<=N; j++) 
					map[i][j] = Integer.parseInt(s[j-1]);
			}
			
			// 1번째 국가부터 탐색 시작
			for (int i = 1; i <= N; i++) {
				int connetedNodes = 0;
				for (int j = 1; j <= N; j++) {
					if (map[i][j] == 1) {
						connetedNodes++;
					}
				}
				aa.add(new int[] { i, connetedNodes });
			}
			Collections.sort(aa, new Comparator<int[]>() {
				@Override
				public int compare(int[] o1, int[] o2) {
					return o2[1] - o1[1];
				}
			});
			
			for(int i=0; i<aa.size(); i++) {
				changeColor(aa.get(i)[0]);
			}
			System.out.println("#"+tc + " "+cnt);
		}
		br.close();
	}
	
	private static void changeColor(int i) {
		int myColor = color[i];
		colorCheck(i);
		for(int c=0; c<nextColor.size(); c++) {
			int tc = getColor();
			// 주변에 있는것과 색이 같으면
			if(myColor == nextColor.get(c)[1]) {
				// 안 쓴 색중에 하나로 바꿈
				color[i] = tc;
				// 바꿨으니 카운트 증가
				cnt++;
				return;
			}
		}
	}
	
	private static int getColor() {
		int newColor = 0; 
		for(int i =0; i < unUsedColor.length; i++) {
			if(unUsedColor[i]!=0) {
				newColor = unUsedColor[i];
				unUsedColor[i]=0;
				break;
			}
		}
		return newColor;
	}
	// 주변 컬러 체크 함
	private static void colorCheck(int i) {
		//System.out.println(i+"노드의 인접한 노드, 노드의 색");
		ArrayList<int[]> temp = new ArrayList<>();
		int[] temp2 = new int[N+1];
		for(int j=0; j<N+1; j++) temp2[j] = unUsedColor[j];
		for(int j=1; j<=N; j++) {
			if(i==j)continue;
			if(map[i][j]==0)continue;
			if(map[i][j] == 1 ) {
				temp.add(new int[] {j, color[j]});
				temp2[j] = 0;
				map[i][j] = map[j][i] = 0;
			}
		}
		nextColor = temp;
		System.arraycopy(temp2, 0, unUsedColor, 0, N+1);
		unUsedColor = temp2;
	}
}