package SWExpertAcademy;
import java.util.Scanner;
import java.io.FileInputStream;
class Solution2007_패턴마디의길이_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input2007.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			int Answer =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			
			String a = sc.next();
			for(int i=1; i<=10; i++) {
				String sub = a.substring(0, i);
				String sub2 = a.substring(i, i+i);
				if(sub.toString().equals(sub2.toString())) {
					Answer = sub.length();
					break;
				}
			}
			
			System.out.println("#"+test_case + " "+Answer);
		}
		sc.close();
	}
}