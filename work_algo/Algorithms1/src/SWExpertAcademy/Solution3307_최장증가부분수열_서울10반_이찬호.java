package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
class Solution3307_최장증가부분수열_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3307.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			int numL= Integer.parseInt(br.readLine());
			String r[] = br.readLine().split(" ");
			int num[] = new int[r.length];
			for(int i =0; i< numL; i++) {
				num[i] = Integer.parseInt(r[i]);
			}
			int n =num.length;
			
			int[] l = new int[n];
			int max = 0;
			for (int i = 0; i < l.length; i++) {
				l[i] = 1;
				for (int j = 0; j < l.length; j++) {
					if (num[j] < num[i] && l[i] <= l[j]+1) {
						l[i]=l[j]+1;
					}
				}
				if(max < l[i]) max = l[i];
			}
			
			System.out.println("#"+tc + " "+max);
		}
		br.close();
	}
}