package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
class Solution1219_길찾기_서울10반_이찬호{
	public static int Ans = 0;
	public static int graph[][];
	private static void dfsRecursive(int node ) {
		if(node == 99) {
			Ans =1;
			return;
		}
		for(int next=0; next<100; next++) {
			if(graph[node][next]==1) dfsRecursive(next);
		}
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1219.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		for(int tc = 1; tc <= 10; tc++)
		{
			graph = new int[100][100];
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			String T[] = br.readLine().split(" ");
			String r[] = br.readLine().split(" ");
			
			int num[] = new int[r.length];
			for(int i =0; i<r.length; i++) {
				num[i] = Integer.parseInt(r[i]);
			}
			
			for(int i =0; i<Integer.parseInt(T[1]); i++) {
				int v1 = num[i*2];
				int v2 = num[i*2+1];
				graph[v1][v2] = 1;
			}
			
			dfsRecursive(0);
			System.out.println("#"+tc + " "+Ans);
			Ans = 0;
		}
		br.close();
	}
}