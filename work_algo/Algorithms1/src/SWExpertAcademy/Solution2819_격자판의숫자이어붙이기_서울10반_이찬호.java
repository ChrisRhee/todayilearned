package SWExpertAcademy;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

class Solution2819_격자판의숫자이어붙이기_서울10반_이찬호 {
	// 1 2 3 4
	public static int dir[][] = { { 0, 0 }, { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };
	public static int ansMap[][] = new int[4][4];
	public static int cnt = 0;

	private static void perm(String a, int i, int j, HashSet<String> dp) {
		if (a.length() == 7) {
			dp.add(a);
			a = "";
		} else {
			a += ansMap[i][j];
			for (int k = 1; k <= 4; k++) {
				if (i + dir[k][0] < 0 || i + dir[k][0] >= 4 || j + dir[k][1] < 0 || j + dir[k][1] >= 4)
					continue;
				perm(a, i + dir[k][0], j + dir[k][1], dp);
			}
		}
	}

	public static void main(String args[]) throws Exception {
		System.setIn(new FileInputStream("res/input2819.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine());
		for (int tc = 1; tc <= T; tc++) {
			HashSet<String> dp = new HashSet<String>(); // 중복연산 방지
			int Ans = 0;
			String ss = "";
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			for (int i = 0; i < 4; i++) {
				String s[] = br.readLine().split(" ");
				for (int j = 0; j < 4; j++) {
					ansMap[i][j] = Integer.parseInt(s[j]);
				}
			}

			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					perm(ss, i, j, dp);
				}
			}
			Ans = dp.size();
			System.out.println("#" + tc + " " + Ans);
		}
		br.close();
	}
}