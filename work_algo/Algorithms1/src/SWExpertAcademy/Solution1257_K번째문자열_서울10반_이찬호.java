package SWExpertAcademy;
import java.io.*;
import java.util.*;
class Solution1257_K번째문자열_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1257.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			int N= Integer.parseInt(br.readLine());
			String k = br.readLine();
			HashSet<String> s = new HashSet<String>();
 			for(int i=1;i<=k.length();i++) {
				for(int j=0; j<=k.length()-i; j++) {
					s.add(k.substring(j,i+j));
				}
			}
 			List<String> a = new ArrayList<>(s);
			a.sort(null);
			if(a.size() > N-1)
				System.out.println("#"+tc + " " + a.get(N-1));
			else
				System.out.println("#"+tc + " none");
		}
	}
}