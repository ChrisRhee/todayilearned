package SWExpertAcademy;
import java.io.*;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 저수지 물의 총 깊이를 구하자
 * 조건
 * 	- 저수지 크기는 9 <= N <= 100
 *  - W 깊이는 W 주변의 W 갯수의 합
 *  - 주변이 다 G면 W는 1
 *  - 물 깊이 중 가장 깊은 물의 깊이가 저수지의 총 깊이!
 * 입력
 *  - 테스트 케이스 T
 *  - 구획의 크기 N
 *  - 구획의 정보들
 * 출력
 * 	- 저수지의 깊이
 * 풀이 
 * 	- W를 찾아서 주변 8방향에 있는 W를 센다.
 *  - 그중 제일 높은걸 출력한다.
 *  - 다 막혀있는 경우 주변 8방향을 더해도 0이므로, 물을 찾았는데 0이면 1만 출력,
 *  - 다 G 로 이루어진 경우 물이 아예 없으므로 0 을 출력한다.
 * */
class Solution7236_저수지의물의총깊이구하기_서울10반_이찬호{
	public static int di[] = {-1,-1,0,1,1,1,0,-1};
	public static int dj[] = {0,1,1,1,0,-1,-1,-1};
	public static int N, v[][];
	public static String map[][];
	public static int Ans;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input7236.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			Ans =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			N= Integer.parseInt(br.readLine());
			map = new String[N][N];
			boolean check = false;
			v = new int[N][N];
			for(int i =0; i<N; i++) {
				String s[] = br.readLine().split(" ");
				for(int j =0; j<N; j++) {
					map[i][j] = s[j]; 
				}
			}
			
			for(int i =0; i<N; i++) {
				for(int j =0; j<N; j++) {
					if(map[i][j].equals("W")) {
						check = true;
						ground(i,j); 
					}
				}
			}
			
			if(check==true) { 
				if(Ans==0) System.out.println("#"+tc + " "+1);
				else System.out.println("#"+tc + " "+Ans);
			}
			else System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}

	private static void ground(int i, int j) {
		for(int d= 0; d<di.length; d++) {
			int ii= i+di[d];
			int jj= j+dj[d];
			if(ii < 0 || ii >= N || jj < 0 || jj >= N ) continue;
			if(map[ii][jj].equals("W")) {
				v[i][j]++;
				if(Ans < v[i][j]) Ans = v[i][j];
			}
		}
	}
}