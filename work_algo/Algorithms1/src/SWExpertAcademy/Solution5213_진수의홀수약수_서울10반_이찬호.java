package SWExpertAcademy;
import java.io.*;
class Solution5213_진수의홀수약수_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input5213.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n= 1000001;
		long ans[] = new long[n];
		for (int i = 1; i < n; i++) {
			if(i%2==0) continue;
			for (int j = i ; j <n; j += i) {
				ans[j] += i;
			}
		}
		for(int i=1; i<n; i++) 
			ans[i] += ans[i-1];
		int T= Integer.parseInt(br.readLine());
		StringBuffer sb = new StringBuffer();
		for(int tc = 1; tc <= T; tc++){
			String r[] = br.readLine().split(" ");
			sb.append("#").append(tc).append(" ").append((ans[Integer.parseInt(r[1])]-ans[Integer.parseInt(r[0])-1])).append("\n");
		}
		System.out.println(sb.toString());
	}
}