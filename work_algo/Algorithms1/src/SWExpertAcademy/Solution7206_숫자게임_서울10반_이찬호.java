package SWExpertAcademy;
import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 숫자 만들기 게임
 * 조건
 * 	- 시작수는 자연수 1 <= N <= 99999
 *  - 시작 수의 사이를 터치하면 수는 두개 또는 그 이상으로 쪼개져
 *  - 쪼개진 수를 모두 곱한다
 *  - 곱한수가 10 이상이면 2~3을 반복한다.
 *  - 한번 쪼개지면 turn 수가 증가 한다.
 *  - 최대 턴수가 나오도록 한다.
 * 입력
 *  - 테스트 케이스 
 *  - 시작수 입력
 * 출력
 * 	- 턴수
 * 풀이 
 * 	- 
 * */
class Solution7206_숫자게임_서울10반_이찬호{
	public static int d[];
	public static int t[];
	public static int N;
	public static int R, Ans;
	public static String SS;
	public static Queue<int[]> q;
	public static int count = 0;
	public static void nCr(int n, int r,String s) {
		
		if(r==0) {
			int temp = calc(s,t);
			if(temp >= 10) {
				q.offer(new int[] {temp,count});
			}
			return;
		}
		if(n<r) return ;
		t[r-1] = d[n-1];
		nCr(n-1,r-1,s);
		nCr(n-1,r,s);
	}
	private static int calc(String s, int[] t2) {
		
		//System.out.println(Arrays.toString(t2));
		int temp [] = new int[t2.length];
		for(int i=0; i<t2.length; i++) {
			temp[i] = t2[i];
		}
		int ans = 1;
		int end = 0;
		int len = 0;
		for(int i=0; i<temp.length; i++) {
			if(i > 0) temp[i]= temp[i] - (temp[i-1]+1);
			len = s.substring(0, temp[i]+1).length();
			ans = ans * Integer.parseInt(s.substring(0, temp[i]+1));
			s= s.substring(temp[i]+1);
			end = Integer.parseInt(s);
			if(i > 0) temp[i]= temp[i] + (temp[i-1]+1);
		}
		ans = ans * end;
		return ans;
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input7206.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			Ans =0;
			q = new LinkedList<>();
			SS = br.readLine();
			q.offer(new int [] {Integer.parseInt(SS),0});
			while(true) {
				if(q.isEmpty()) break;
				int []curr = q.poll();
				 
				if(curr[0] < 10) break;
				
				int slen = (""+curr[0]).length();
				d = new int[slen];
				for(int i=0; i<slen-1; i++) {
					d[i] = i;
				}
				N = slen-1;
				for(int i=1; i<=slen-1; i++) {
					R = i;
					t= new int[R];
					count = ++curr[1]; 
					nCr(N,R,""+curr[0]);
				}
			}
			
			//System.out.println(Arrays.toString(d));
			//int N= Integer.parseInt(br.readLine());
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
//			while(true) {
//				if(N<10) break;
//				
//			}
			
			if(Ans < count) Ans = count;
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
}