package SWExpertAcademy;
import java.io.*;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 중첩된 안전기지 수 구하기
 * 조건
 * 	- 격자구역은 5 >= N >= 50 정수
 *  - 안전기지 개수 3 >= B >= 30
 *  - 동일한 지점에 여러개 가능
 * 입력
 *  - 테스트케이스 T
 *  - 격자구역의 길이 N과 안전기지의 개수 B
 *  - B줄에 걸쳐서 안전기지 좌표가 행, 열 순서로 쭉
 * 출력
 * 	- 가장 많이 중첩되는 기지 갯수
 * 풀이 
 * 	- 기지가 놓일 때마다, 자기 좌표에서 상하좌우 2칸씩 맵을 +1씩 해준다.
 *  - 기지들마다 다 하고 맵에서 가장 큰 수를 출력한다.
 * */
class Solution7234_안전기지_서울10반_이찬호{
	public static int map[][];
	public static int N,Ans;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input7234.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine().trim());
		
		for(int tc = 1; tc <= T; tc++){
			Ans =0;
			String s[] = br.readLine().split(" ");
			N = Integer.parseInt(s[0]);
			int B = Integer.parseInt(s[1]);
			map = new int[N+1][N+1];
			for(int i =0; i<B; i++) {
				s = br.readLine().split(" ");
				check(Integer.parseInt(s[0]),Integer.parseInt(s[1]));
			}
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}

	private static void check(int x, int y) {
		for(int i= -2; i<=2; i++) {
			if(i==0 || x+i < 1 || x+i > N) continue;
			map[x+i][y]++;
			if(Ans < map[x+i][y]) Ans = map[x+i][y];
		}
		for(int i= -2; i<=2; i++) {
			if(i==0 || y+i < 1 || y+i > N) continue;
			map[x][y+i]++;
			if(Ans < map[x][y+i]) Ans = map[x][y+i];
		}
	}
}