package SWExpertAcademy;
import java.util.Scanner;
import java.io.FileInputStream;
class Solution1989_초심자의회문검사_서울10반_이찬호2{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1989.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			int Answer =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			
			String str = sc.next();
			StringBuilder sb = new StringBuilder(str);
			if(sb.reverse().toString().equals(str.toString())) Answer = 1;
			else Answer = 0;
			
			System.out.println("#"+test_case + " "+Answer);
		}
		sc.close();
	}
}