package SWExpertAcademy;

import java.util.Arrays;
import java.util.Random;

class OddMagic_서울10반_이찬호{
	public static int N = 5;
	public static int magicCheck(int magic[][]) {
		int total = 0;
		for(int i =1; i<= N*N; i++) {
			total += i;
		}
		int row =0;
		int col =0;
		for(int i =0; i<N; i++) {
			for(int j=0; j<N; j++) {
				row += magic[i][j];
				col += magic[j][i];
			}
			//System.out.println("["+i+"]row:"+row+"  ["+i+"]col:"+col);
			if(row != total/N) return -1;
			if(col != total/N) return -2;
			row = 0;
			col = 0;
		}
		System.out.println("OddMagic Total: "+total);
		System.out.println("OddMagic Randon Line Total: "+(total/N));
		
		return 0;
	}
	
	public static void main(String args[]) throws Exception	{
		int magic[][] = new int[N][N];
		int magicBig[][] = new int[(N*2)-1][(N*2)-1];
		int k = 1;
		
		int l = 1;
		int firstlist = 0;
		int mid = (N*2-1)/2;
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				magicBig[firstlist+j+i][mid+j-i] = l++;
			}
		}
		int magiclen = magic.length/2;
		for(int i =0; i<N; i++) {
			for(int j=0; j<N; j++) {
				magic[i][j] = magicBig[magiclen+i][magiclen+j];
				if(i < magiclen && magicBig[magiclen+i+N][magiclen+j] != 0) 
					magic[i][j] = magicBig[magiclen+i+N][magiclen+j];
				if(i > magiclen && magicBig[magiclen+i-N][magiclen+j] != 0) 
						magic[i][j] = magicBig[magiclen+i-N][magiclen+j];
				if(j < magiclen && magicBig[magiclen+i][magiclen+j+N] != 0) 
						magic[i][j] = magicBig[magiclen+i][magiclen+j+N];
				if(j > magiclen && magicBig[magiclen+i][magiclen+j-N] != 0) 
						magic[i][j] = magicBig[magiclen+i][magiclen+j-N];
			}
		}
		
		//magic[0][0] = 40;
		
		//마방진 출력
		for (int[] is : magic) {
			System.out.println(Arrays.toString(is));
		}
		// 마방진 체크
		if(magicCheck(magic)==0) 
			System.out.println("마방진이다\n");
		else{
			System.out.println("마방진이 아니다");
		}
		
		System.out.println("Program End");
	}
}