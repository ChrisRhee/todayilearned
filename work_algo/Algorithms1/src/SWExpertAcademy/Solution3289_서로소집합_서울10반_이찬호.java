package SWExpertAcademy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
class Solution3289_서로소집합_서울10반_이찬호{
	public static int []p ;
	public static int []rank ;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3289.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			StringBuilder sb = new StringBuilder();
			String[] r = br.readLine().split(" ");
			int numSet = Integer.parseInt(r[0]);
			int numOper = Integer.parseInt(r[1]);
			
			p = new int [numSet+1]; 
			rank = new int[numSet+1];
			for(int i =1; i<=numSet; i++) {
				p[i] = i;
			}
			
			for(int i =0; i<numOper; i++) {
				String[] s = br.readLine().split(" ");
				int oper = Integer.parseInt(s[0]);
				int numX = Integer.parseInt(s[1]);
				int numY = Integer.parseInt(s[2]);
				if(oper == 0) {
					union(numX,numY);
					
				}else if(oper ==1) {
					if(findSet(numX)==findSet(numY)){
						sb.append(1);
					}else {
						sb.append(0);
					}
				}
			}
			System.out.println("#"+tc + " "+sb.toString());
		}
		br.close();
	}

	public static int findSet(int x) {
		if(x == p[x]) return x;
		else return p[x] = findSet(p[x]);
	}
	
	public static void union(int x, int y) {
		p[findSet(y)] =findSet(x);
	}
}