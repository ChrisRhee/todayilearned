package array1;

public class Permutation1_3 {

	// permutation3 같은 값 하나도 없음
	public static void main(String[] args) {
		int count =0;
		for(int i=1; i<=6; i++) {
			for(int j=1; j<=6; j++) {
				if(j==i) continue;
				for(int k=1; k<=6; k++) {
					if(k==i||k==j) continue;
					System.out.println(i+" "+j+" "+k);
					count++;
				}
			} 
		}
		System.out.println(count);
	}

}
