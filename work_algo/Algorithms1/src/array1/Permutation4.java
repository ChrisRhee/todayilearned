package array1;

import java.util.Arrays;
import java.util.Scanner;

public class Permutation4 {
	public static int casecount,r, n, data[];
	//permutation 4
	// 4개의 수에서 중복되는 배열 빼고 
	// 6개중 3개를 뽑는데 순서도 없고, 중복도 허용하지 않는다.
	// nPr/r! = nCr이 된다.
	// 6P3/3! = 6x5x4/(3x2x1) = 20
	public static void permutation4(int start, int count) {
		//if(count ==n) {
		if(count ==r) {
			// 중복되는 것을 빼고
			for(int i=0; i<r; i++) { 
				for(int j=i+1; j<r; j++) { 
					if(data[i]==data[j] ) { 
						return;
					}
				}
			}
			casecount ++;
			System.out.println(Arrays.toString(data));
			return;
		}
		
		for(int i=start; i<=n; i++) {
			data[count] = i;
			permutation4(i,count+1);
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		n =6; // sc.nextInt();
		r = 3; // sc.nextInt();
		//data = new int[n];
		data = new int[r];
		permutation4(1,0);
		System.out.println((casecount));
		sc.close();
	}
}
