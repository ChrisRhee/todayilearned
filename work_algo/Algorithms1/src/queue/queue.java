package queue;

import java.util.LinkedList;
import java.util.Queue;

public class queue {
	public static int [] queue = new int [100];
	public static int front = -1;
	public static int rear = -1;
	
	public static void main(String[] args) {
		// 자바의 API
		Queue<Integer> q = new LinkedList<>();
		for (int i = 1; i <= 3; i++) {
			q.offer(i);
		}
		System.out.println(q.size());
		System.out.println(q);
		System.out.println(q.poll());
		System.out.println(q.poll());
		System.out.println(q.poll());
		System.out.println(q.size());
		
	}
}
