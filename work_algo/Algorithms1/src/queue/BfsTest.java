package queue;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Queue;
import java.util.Scanner;

public class BfsTest {
	public static int V,E;
	public static int graph[][];
	public static int queue[];
	public static int front;
	public static int rear;
	public static boolean visit[];
	
	private static void bfsRecursive(int node) {
		visit[node] = true;
		System.out.print(node+" ");
		for(int next=0; next<V; next++) {
			if(visit[next]==false && graph[node][next]==1)
				bfsRecursive(next);
		}
	}
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/inputBfs.txt"));
		Scanner sc = new Scanner(System.in);
		
		V= sc.nextInt();
		E= sc.nextInt();
		graph = new int[V][V];
		visit = new boolean[V];
		//queue = new int[V*V];
		
		front = -1;
		rear = -1;
		for(int i=0; i<E; i++) {
			int v1 = sc.nextInt();
			int v2 = sc.nextInt();
			graph[v1][v2]=graph[v2][v1]=1;
		}
		for(int [] a: graph) System.out.println(Arrays.toString(a));
		System.out.print("#1 ");
		bfsRecursive(0);
		//bfs(0);
		sc.close();
		System.out.println("\n\n Program End");
	}
	
	// 이거도 암기를 꼭 해보세요 ㅠㅠㅠㅠㅠ 으허읗거ㅡㅇ허그흥흑허읗
	
	private static void bfs(int node) {
		queue[++rear]=node; // enqueue
		while(front!= rear) {   // isEmpty()
			int curr=queue[++front]; // dequeue()
			if(visit[curr]==false) {
				visit[curr]=true;
				System.out.print(curr+" ");
				
				for(int next=0; next<V; next++) {
				//for(int next=V-1; next>=0; next--) {
					if(visit[next]==false && graph[curr][next]==1)
						queue[++rear]=next;  // enqueue
				}
			}
		}
	}
	
	private static void bfsReduce(int node) {
		queue[++rear]=node; // enqueue
		while(front!= rear) {   // isEmpty()
			int curr=queue[++front]; // dequeue()
			if(visit[curr]==false) {
				visit[curr]=true;
				System.out.print(curr+" ");
				
				for(int next=0; next<V; next++) {
				//for(int next=V-1; next>=0; next--) {
					if(visit[next]==false && graph[curr][next]==1)
						queue[++rear]=next;  // enqueue
				}
			}
		}
	}
}
