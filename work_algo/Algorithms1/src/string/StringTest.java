package string;

import java.util.Arrays;

public class StringTest {
	public static void main(String[] args) {
		String s1 = "홍길동";
		String s2 = "홍길동";
		String t1 = new String("홍길동");
		String t2 = new String("홍길동");
		System.out.println(s1==s2);
		System.out.println(s1==t1);
		System.out.println(t1==t2);
		
		StringBuilder sb = new StringBuilder(s1);
		// 데코레이터 디자인 패턴
		// 메소드 Chaining 이라고 한다. 
		sb.reverse().append(")").insert(0, "[").setLength(2);
		String s = sb.toString();
		System.out.println(s.length());
		System.out.println("A한B글".length());
		System.out.println("A한B글".charAt(1));
		System.out.println("A한B글".charAt(3));
		
		// 스트링 빌더 결과 확인하기
		StringBuilder sb1 = new StringBuilder ("홍길동");
		StringBuilder sb2 = new StringBuilder ("홍길동");
		System.out.println(sb1==sb2);
		System.out.println(sb1.equals(sb2));
		System.out.println(sb1.toString().equals(sb2.toString()));
		
		// 단어 하나하나 자르기
		String sa[] = s1.split("");
		System.out.println(Arrays.toString(sa));
		
		// 특정 문자를 기준으로 잘라서 배열에 넣기
		String t= "AAtiBB==CC=tiD==EE==";
		String ta[] = t.split("ti",-1);
		System.out.println(Arrays.toString(ta));
		
		// string을 숫자로 바꾸기
		int i = Integer.parseInt("1100110101",10);
		System.out.println(i);
		
		// 숫자 String으로 바꾸기
		int j = 123;
		// 정석방법
		String r1 = new Integer(j).toString();
		// 이렇게 문자에 숫자를 더하면 되니까 쉽게하는 꼼수이다
		String r2 = ""+j;
		System.out.println(r1+" "+r2);
		
		// 0뒤로 전부다, 1뒤로 전부다, 2뒤로 전부다 출력
		System.out.println(s1.substring(0));
		System.out.println(s1.substring(1));
		System.out.println(s1.substring(2));
		// 0~0까지, 0~1앞까지, 0~2앞까지 0~3앞까지
		System.out.println(s1.substring(0,0));
		System.out.println(s1.substring(0,1));
		System.out.println(s1.substring(0,2));
		System.out.println(s1.substring(1,3));
		
	}
	
}
