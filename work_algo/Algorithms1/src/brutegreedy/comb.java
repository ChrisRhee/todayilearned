package brutegreedy;

import java.util.ArrayList;

public class comb {
	public static int[] d = {1,2,3,4,5,6};
	public static int[] t = new int[3];
	public static ArrayList<Integer> a = new ArrayList<>();
	public static ArrayList<Integer> ta = new ArrayList<>();
	public static int n;
	public static int r = ta.size();
	public static int cnt = 0;

	public static void main(String[] args) {
		
		a.add(1);
		a.add(2);
		a.add(3);
		a.add(4);
		a.add(5);
		a.add(6);
		n =  a.size();
		combtest(n,a.size()/2);
		System.out.println(cnt);
	}

	private static void combtest(int n, int r) {
		if(r==0) {
			cnt++;
			System.out.println(ta);
			return;
		}
		
		if(n<r) return;
		
		if(ta.size() >= a.size()/2) ta.set(r-1, a.get(n-1));
		else ta.add(a.get(n-1));
		combtest(n-1, r-1);
		combtest(n-1, r);
	}
}
