package brutegreedy;

import java.util.Arrays;

public class CombTest {
	public static int[] d= {0,1,2,3,4,5,6};
	public static int[] t = new int[3];
	public static int n = d.length;
	public static int r = t.length;
	public static int cnt;
	
	private static void comb(int n, int r) {
		if(r==0) {
			cnt++;
			System.out.println(Arrays.toString(t));
			return;
		}
		if(n<r) return;
		
		t[r-1] = d[n-1];
		comb(n-1, r-1);
		comb(n-1, r);
	}
	
	public static void main(String[] args) {
		comb(n, r);
		System.out.println(cnt);
	}
}
