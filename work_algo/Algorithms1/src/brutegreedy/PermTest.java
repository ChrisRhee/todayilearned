package brutegreedy;

import java.util.Arrays;

public class PermTest {
	public static int[] d= {1,2,3,4};
	public static int cnt;
	
	public static void swap(int i, int k) {
		int temp = d[i] ;
		d[i] = d[k];
		d[k] = temp;
	}
	
	private static void perm(int n, int k) {
		if(k==n) {
			cnt++;
			System.out.println(Arrays.toString(d));
		}else {
			for(int i=k; i<=n; i++) {
				swap(i,k);
				perm(n,k+1);
				swap(i,k);
			}
		}
	}
	
	public static void main(String[] args) {
		perm(d.length-1, 0);
		System.out.println(cnt);
	}
}
