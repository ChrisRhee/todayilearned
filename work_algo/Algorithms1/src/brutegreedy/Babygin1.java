package brutegreedy;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Babygin1 {
	public static int n,r,casecount,data[],narr[];
	
	private static int permutation(int before,int flag, int count) {
		if(count==r) {
			casecount++;
			//System.out.println(Arrays.toString(data));
//			for(int i=0; i<r; i++) {
//				System.out.print(data[i]+" ");
//			}
//			System.out.println();
			int tri =0; int run =0;
			for(int i = 0; i<2; i++) {
				if(data[3*i+0] == data[3*i+1] &&data[3*i+1]==data[3*i+2]) tri++;
				if(data[3*i+0]+1 == data[3*i+1] && data[3*i+1]+1==data[3*i+2]) run++;
				if(tri+run==2) return 1;
			}
			return 0;
		}
		for(int i=0; i<n; i++) {
			if((flag&(1<<i))==0) {
				data[count]=narr[i];
				if(permutation(i+1, flag|(1<<i), count+1)==1) return 1;
			}
		}
		
		return 0;
	}
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/inputbabygin.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();
		n =6;
		r=6;
		data = new int[r];
		narr = new int[r];
		
		for(int tc=1; tc<=T; tc++) {
			for(int i=0; i<r; i++) narr[i] = sc.nextInt();
			
			System.out.println("# "+tc+" "+permutation(1,0,0));
			//System.out.println(casecount);
		}
		sc.close();
	}
}
