package array2;

public class Subset1 {
	public static int bit[] = {1,2,3,4};
	
	public static void main(String[] args) {
		int cnt= 0;
		for(int i=0; i<2; i++) {
			bit[0] = (i%2==0)? 0:1;
			for(int j=0; j<2; j++) {
				bit[1] = (j%2==0)? 0:1;
				for(int k=0; k<2; k++) {
					bit[2] = (k%2==0)? 0:1;
					for(int l=0; l<2; l++) {
						bit[3] = (l%2==0)? 0:1;
						cnt++;
						//System.out.println(Arrays.toString(bit));
						for(int b=0; b<bit.length; b++) {
							if(bit[b]==1) System.out.print((b+1)+" ");
						}
						System.out.println();
					}
				}
			}
		}
		
		System.out.println(cnt);
	}
}
