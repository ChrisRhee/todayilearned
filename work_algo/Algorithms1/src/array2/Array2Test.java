package array2;

import java.util.Arrays;

public class Array2Test {
	public static int data[][] = {{1,2,3},{4,5,6},{7,8,9}};
	public static int N = data.length;
	public static int M = data.length;
	//                         상   하   좌   우
	public static int di[] = {-1, 1, 0, 0};
	public static int dj[] = { 0, 0,-1, 1};
	
	public static void main(String[] args) {
		//System.out.println(Arrays.deepToString(data));
		for (int[] a : data) System.out.println(Arrays.toString(a));
		System.out.println();
		
		// 순서대로 출력
		System.out.println("-- 순서대로 출력 --");
		for (int i=0; i<N; i++) {
			for (int j=0; j<M; j++) {
				System.out.print(data[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println();
		
		// i와 j 바꿈
		System.out.println("-- 세로로 출력 i , j 바꿈 --");
		for (int j=0; j<M; j++) {
			for (int i=0; i<N; i++) {
				System.out.print(data[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println();
		
		// 지그재그
		System.out.println("-- 지그재그 출력 --");
		for (int i=0; i<N; i++) {
			for (int j=0; j<M; j++) {
				System.out.print(data[i][j+(M-1-2*j)*(i%2)]+" ");
			}
			System.out.println();
		}
		System.out.println();
		
		// 전치행렬
		System.out.println("-- 전치행렬 --");
		for (int i=0; i<N; i++) {
			for (int j=0; j<M; j++) {
				if(i<j) {
					int tmp = data[i][j];
					data[i][j] = data[j][i];
					data[j][i]=tmp;
				}
			}
		}
		for (int[] a : data) System.out.println(Arrays.toString(a));
		System.out.println();
		
		// 상하좌우 더하기
		System.out.println("-- 상하좌우 더해서 두기 --");
		for (int i=0; i<N; i++) {
			for (int j=0; j<M; j++) {
				int sum = 0;
				for(int d=0; d<di.length; d++) {
					int ii= i+di[d];
					int jj= j+dj[d];
					if((ii>=0)&&(ii<N) && (jj>=0)&&(jj<M)) {
						sum += data[ii][jj];
					}
				}
				System.out.print(sum+" ");
			}
			System.out.println();
		}
		System.out.println();
	}
}
