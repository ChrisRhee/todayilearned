package array2;


// 이건 ad문제 풀때 외워놔야 한다!!!!! 
public class Subset2 {
	public static char data[] = {'l','o','v','e'};
	public static int n = data.length;
	// n개의 원소를 가진 배열중에 부분집합을 구하는 방법.
	// 공집합을 포함한 모든 부분집합을 구할 수 있다.
	// 비트연산자를 사용하면 메모리 사용이 적고,
	// 속도가 더 빠르다. 
	public static void main(String[] args) {
		// 부분집합의 합을 구할 수 있는 함수
		// 부분 집합을 구하는 함수
		for(int i=1; i<(1<<n); i++) {
			int sum =0;
			for(int j=0; j<n; j++) {
				//i=9 1001 1001 1001 1001
				//  & 0001 0010 0100 1000
				//  = 0001 0000 0000 1000
				//     j=0  j=1  j=2  j=3
				// 2,3에서는 선택 안된다. 그래서 j=0,3일떄만 들어감
				// 그래서 값은 data[0],data[3] 이렇게 나온다.
				if((i&(1<<j))>0) {
					System.out.print(data[j]+ " ");
					//sum+= data[j];
					sum++;
				}
			}
			//System.out.println("sum=" + sum);
			System.out.println();
		}
		// 부분집합의 합을 구할 수 있는 함수
		// 부분 집합을 구하는 함수
//		for(int i=0; i<(1<<n); i++) {
//			int sum =0;
//			for(int j=0; j<n; j++) {
//				if((i&(1<<j))>0) {
//					System.out.print(data[j]+ " ");
//					sum+= data[j];
//				}
//			}
//			System.out.println("sum=" + sum);
//		}
		
		//2^4
		// 1, 10, 100, 1000, 10000 = 16
		/*
		System.out.println(9&(1<<3));
		for(int i=0; i< (1<<4); i++) {
			System.out.printf("%4s %2d\n",Integer.toBinaryString(i),i);
		}
		*/
	}
}
