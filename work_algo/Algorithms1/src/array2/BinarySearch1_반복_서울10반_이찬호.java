package array2;

import java.util.Arrays;
import java.util.Scanner;

public class BinarySearch1_반복_서울10반_이찬호 {
	public static int data[] = {2,4,7,9,11,19,23};
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int key = sc.nextInt();
		System.out.println(binarySearch(key));
		System.out.println(Arrays.binarySearch(data, key));
		System.out.println(data[Arrays.binarySearch(data, key)]);
		
		sc.close();
	}

	public static boolean binarySearch(int key) {
		int start = 0;
		int end = data.length-1;
		while(start<=end) {
			int middle = (start+end)/2;
			if(key == data[middle]) return true;
			else if( data[middle] > key) end = middle -1;
			else start = middle +1;
		}
		return false;
	}
	
}
