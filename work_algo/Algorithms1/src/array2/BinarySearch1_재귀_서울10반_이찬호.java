package array2;

import java.util.Scanner;

public class BinarySearch1_재귀_서울10반_이찬호 {
	public static int data[] = {2,4,7,9,11,19,23};
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int key = sc.nextInt();
		System.out.println(binarySearch(0,data.length,key));
		int index = binarySearchInt(0,data.length,key);
		System.out.println("data["+index+"]="+data[index]);
		
		sc.close();
	}

	public static boolean binarySearch(int low, int high, int key) {
		if(low>high) return false;
		
		int middle = (low+high)/2;
		if(key == data[middle]) return true;
		else if( data[middle] > key) return binarySearch(low,middle -1,key);
		else return binarySearch(middle +1,high,key);
	}
	
	public static int binarySearchInt(int low, int high, int key) {
		if(low>high) return -1;
		int middle = (low+high)/2;
		if(key == data[middle]) return middle;
		else if( data[middle] > key) return binarySearchInt(low,middle -1,key);
		else return binarySearchInt(middle +1,high,key);
	}
}
