package stack2;

import java.util.Arrays;

public class QuickSort {
	public static int[] a = {69,10,30,2,16,8,31,22};
	
	private static void quick(int begin, int end) {
		if(begin<end) {
			int p = partition(begin,end);
			System.out.println(Arrays.toString(a)+"\t p="+p+" a["+p+"]="+a[p]+ " begin="+begin+" end="+end);
			quick(begin,p-1);
			quick(p+1,end);
		}
	}
	
	private static int partition(int begin, int end) {
		int p = (begin+end)/2;
		int L = begin;
		int R = end;
		while(L<R) {
			while( a[L]<a[p] && L<R ) L++;
			while( a[R]>=a[p] && L<R ) R--;
			if(L<R) {
				if(L==p) p=R;
				swap(R,L);
			}
		}
		swap(R,p);
		return R;
	}
	private static void swap(int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	public static void main(String[] args) {
		System.out.println(Arrays.toString(a));
		System.out.println();
		quick(0,a.length-1);
		System.out.println(Arrays.toString(a));
		System.out.println("Program End");
	}
}
