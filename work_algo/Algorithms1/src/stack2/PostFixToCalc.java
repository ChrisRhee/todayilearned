package stack2;

public class PostFixToCalc {
	public static String s="6528-*2/+";
	public static int[] stack = new int[100];
	public static int top = -1;
	
	public static void main(String[] args) {
		
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if('0'<=c && c<='9')
				stack[++top]= c-'0';
			else {
				int n2 = stack[top--];
				int n1 = stack[top--];
				int nn =0;
				switch(c) {
					case '+': nn = n1+n2; break;
					case '-': nn = n1-n2; break;
					case '*': nn = n1*n2; break;
					case '/': nn = n1/n2; break;
				}
				stack[++top]=nn;
			}
		}
		System.out.println(stack[top--]);
	}
}
