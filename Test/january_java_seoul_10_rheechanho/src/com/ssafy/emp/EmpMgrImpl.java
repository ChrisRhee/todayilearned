package com.ssafy.emp;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;


public class EmpMgrImpl implements IEmpMgr{	
	
    /** 직원나 매니저의 정보를 저장하기 위한 리스트 */
	private List<Employee> emps = new ArrayList<Employee>();

	private static EmpMgrImpl instance= null;
	private EmpMgrImpl() {
		load("emp.dat");
	}
	public static EmpMgrImpl getInstance() {
		if(instance == null) {
			instance = new EmpMgrImpl();
		}
		return instance;
	}

	/** 파일로 부터 자료 읽어서 메모리(ArrayList)에 저장하기*/
	public void load(String filename) {
		File file=new File(filename);
		System.out.println(file);
		if (!file.exists()) return; 
		emps.clear();
		ObjectInputStream ois=null;
		Object ob=null;
		try{	
			ois=new ObjectInputStream(new FileInputStream(file));
			while(true){//마지막 EOF Exception발생
				ob=ois.readObject();
				emps.add((Employee)ob);
			}
		}catch(EOFException ee){System.out.println("읽기 완료");
		}catch(FileNotFoundException fe){
			System.out.println("파일이 존재하지 않습니다");
		}catch(IOException ioe){
			System.out.println(ioe);
		}catch(ClassNotFoundException ce){
			System.out.println("같은 클래스 타입이 아닙니다");
		}finally{
			if(ois !=null){
				try{
					ois.close();
				}catch(IOException oe){System.out.println("파일을 닫는데 실패했습니다");}
			}
		}
	}
  
	public List<Employee> search(){
    	return emps;
    }

	
	/** 파일에 메모리(ArrayList)에 저장하기*/
	// filename에 객체를 쓰는 메서드
	@Override
	public void save(String filename) {
		File file=new File(filename);
		System.out.println(file);
		if (!file.exists()) return; 
		ObjectOutputStream oos=null;
		Object ob=null;
		try {
			oos=new ObjectOutputStream(new FileOutputStream(file));
			for (Employee employee : emps) {
				oos.writeObject(employee);
			}
			oos.flush();
		} catch (FileNotFoundException e) {
			System.out.println("파일이 존재하지 않습니다");
		} catch (IOException ioe) {
			System.out.println(ioe);
		}finally{
			if(oos !=null){
				try{
					oos.close();
				}catch(IOException oe){System.out.println("파일을 닫는데 실패했습니다");}
			}
		}
		
	}

	@Override
	public void add(Employee b) throws DuplicateException {
		try {
			search(b.getEmpNo());
			
			throw new DuplicateException();
		} catch (RecordNotFoundException e) {
			emps.add(b);
		}
	}

	/** 직원 찾기
	 * parameter : 직원 번호
	 * return : 해당 직원
	 * */
	@Override
	public Employee search(int num) throws RecordNotFoundException {
		for (Employee employee : search()) {
			if(employee.getEmpNo() == num) {
				return employee;
			}
		}
		throw new RecordNotFoundException();
	}

	/** 직원 정보 수정하기
	 * parameter : 직원
	 * 직원의 번호로 직원 찾아서 직원 정보 수정
	 * return : x
	 * */
	@Override
	public void update(Employee b) throws RecordNotFoundException {
		Employee old = search(b.getEmpNo());
		old.setName(b.getName());
		old.setPosition(b.getPosition());
		old.setDept(b.getDept());
	}

	/** 직원 정보 삭제
	 * parameter : 직원 번호
	 * 직원의 번호로 직원 찾아서 직원 삭제
	 * return : x
	 * */
	@Override
	public void delete(int num) throws RecordNotFoundException {
		emps.remove(search(num));
	}

}
