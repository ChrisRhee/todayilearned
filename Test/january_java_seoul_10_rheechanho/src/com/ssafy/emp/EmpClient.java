package com.ssafy.emp;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

public class EmpClient extends Thread{
	private List<Employee> emps;
	private String ip;
	private int port;
	
	public EmpClient(List<Employee> emps) {
		super();
		this.emps = emps;
		this.ip = "127.0.0.1";
		this.port = 6000;
	}
	
	@Override
	public void run() {
		super.run();
		
		Socket s = null;
		ObjectOutputStream oos = null;
		try {
			s = new Socket(ip, port);
			oos = new ObjectOutputStream(s.getOutputStream());
			oos.writeObject(emps);
			oos.flush();
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if(s!=null)
				try {
					s.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			if(oos!=null)
				try {
					oos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
}
