package com.ssafy.emp;

import java.io.Serializable;


public class Employee implements Serializable{
	private int empNo;
	private String name;
	private String position;
	private String dept;

	public Employee() {
	}

	public Employee(int empNo, String name, String position, String dept ) {
		super();
		setEmpNo(empNo);
		setName(name);
		setPosition(position);
		setDept(dept);
	}

	public int getEmpNo() {
		return empNo;
	}
	
	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getEmpNo());
		builder.append("  ");
		builder.append(getName());
		builder.append("  ");
		builder.append(getPosition());
		builder.append("  ");
		builder.append(getDept());
		return builder.toString();
	}
}
