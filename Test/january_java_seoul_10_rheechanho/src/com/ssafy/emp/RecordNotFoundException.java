package com.ssafy.emp;

public class RecordNotFoundException extends Exception {
	public RecordNotFoundException() {
		super("데이터가 없습니다.");
	}
}
