package com.ssafy.server;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.ssafy.emp.Employee;

public class EmpServer {

	private int port = 6000;

	public void receive() {
		
		// 서버 구현
		ServerSocket ss = null;
		try {
			ss = new ServerSocket(port);
			System.out.println("ServerSocket ok. port="+port);
		} catch (IOException e) {
			e.printStackTrace();
		}
		while(true) {
			Socket s = null;
			ObjectInputStream ois = null;
			System.out.println("Server ready...");
			try {
				s = ss.accept();
				ois = new ObjectInputStream(s.getInputStream());
				
				List<Employee> list = (ArrayList<Employee>) ois.readObject();
				// 추가기능2 : 직원목록 출력시 직원번호로 Sort하여 출력한다.
				Collections.sort(list, new empNumComparator());
				for (Employee employee : list) {
					System.out.println(employee);
				}
				
				System.out.println("receive ok");
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}finally {
				if(s!=null)
					try {
						s.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				if(ois!=null)
					try {
						ois.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
		
	}
	public static void main(String[] args) {
		new EmpServer().receive();
	}

}