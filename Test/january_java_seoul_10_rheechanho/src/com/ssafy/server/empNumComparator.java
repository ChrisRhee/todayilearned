package com.ssafy.server;

import java.util.Comparator;

import com.ssafy.emp.Employee;

public class empNumComparator implements Comparator<Employee> {
	@Override
	public int compare(Employee o1, Employee o2) {
		return o1.getEmpNo()-o2.getEmpNo();
	}
}
