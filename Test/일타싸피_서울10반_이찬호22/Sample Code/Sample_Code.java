import java.net.*;
import java.util.Arrays;
import java.io.*;
public class Sample_Code {

	// User and Game Server Information
	static final String NICKNAME = "죠스";
	static final String HOST = "70.12.109.160";
	static final int PORT = 1447; // Do not modify
	
	// predefined variables(Do not modify these values)
	static final int TABLE_WIDTH = 254;
	static final int TABLE_HEIGHT = 124;
	static final int NUMBER_OF_BALLS = 5;
	static final int[][] HOLES = {{0, 0}, {130, 0}, {260, 0}, {0, 130}, {130, 130}, {260, 130}};
	
	//
	static double BtoGdistance = 0;
	public static void main(String[] args) {
		
		Socket socket = null;
		String recv_data = null;
		byte[] bytes = new byte[1024];
		int[][] balls = new int[NUMBER_OF_BALLS][2];

		try {
			socket = new Socket();
			System.out.println("Trying Connect: " + HOST + ":" + PORT);
			socket.connect(new InetSocketAddress(HOST, PORT));
			System.out.println("Connected: " + HOST + ":" + PORT);
			
			InputStream is = socket.getInputStream();
			OutputStream os = socket.getOutputStream();

			String send_data = "9901/" + NICKNAME;
			bytes = send_data.getBytes("UTF-8");
			os.write(bytes);
			os.flush();
			System.out.println("Ready to play.");
			
			while (socket != null) {
				
				bytes = new byte[1024];
				
				int readByteCount = is.read(bytes);
				recv_data = new String(bytes, 0, readByteCount, "UTF-8");
				System.out.println("Data Received: " + recv_data);
				
				String[] split_data = recv_data.split("/");
				int idx = 0;
				try {
					for (int i = 0; i < NUMBER_OF_BALLS; i++) {
						for (int j = 0; j < 2; j++) {
							balls[i][j] = Integer.parseInt(split_data[idx++]);
						}
					}
				}
				catch (Exception e) {
					bytes = new byte[1024];
					balls = new int[NUMBER_OF_BALLS][2];
					bytes = "9902/9902".getBytes("UTF-8");
					os.write(bytes);
					os.flush();
					System.out.println("Received Data has been currupted, Resend Requested.");
					continue;
				}
				
				double angle = getCtoBAngle(balls);
				
				double power = getCtoBPower(balls)/2+BtoGdistance+2;
				
				////////////////////////////////////////////////////////////////////
				// 주어진 정보(balls)를 바탕으로 샷 방향(angle)과 세기(power)를 결정하는 코드 작성 //
				//////////////////////////////////////////////////////////////////
				
				String merged_data = angle + "/" + power;
				bytes = merged_data.getBytes("UTF-8");
				os.write(bytes);
				os.flush();
				System.out.println("Data Sent: " + merged_data);
			}
			os.close();
			is.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static double[] getBtoGAngle(int[][] balls) {
		double htheta = 0;
		double hangle = 0;
		double angles[] = new double[6];
		int currBall = getCurrBall(balls);
		for(int j=0; j<6; j++) {
			htheta = Math.atan2(HOLES[j][1]-balls[currBall][1], HOLES[j][0]-balls[currBall][0]);
			hangle = htheta * 180/Math.PI;
			hangle = 90- hangle;
			if(hangle < 0) hangle = 360+hangle;
			angles[j] = hangle; 
			
		}
		return angles;
	}

	private static double getCtoBPower(int[][] balls) {
		double dX = 0;
		double dY = 0;
		double distance = 0;
		int currBall = getCurrBall(balls);
		dX = balls[0][0] - balls[currBall][0];
		dY = balls[0][1] - balls[currBall][1];
		distance = Math.sqrt(dX*dX + dY*dY);
		return distance;
	}

	private static double getCtoBAngle(int[][] balls) {
		double angle = 0;
		double theta = 0;
		int currBall = getCurrBall(balls);
		double min = Double.MAX_VALUE;
		int minIndex = -1;
		
		theta = Math.atan2(balls[currBall][1]-balls[0][1], balls[currBall][0]-balls[0][0]);
		
		double angles[] = getBtoGAngle(balls);
		angle = theta * 180/Math.PI;
		angle = 90- angle;
		for(int i=0; i<6; i++) {
			if(angles[i] < angle-90 || angles[i] > angle+90) continue;
			if(min > angles[i]) {
				min = angles[i];
				minIndex = i;
			}
		}
		System.out.println(min+" "+minIndex+"번 홀에 넣자");
		if(minIndex == -1) minIndex = 0;
		double dX = 0;
		double dY = 0;
		dX = HOLES[minIndex][0] - balls[currBall][0];
		dY = HOLES[minIndex][1] - balls[currBall][1];
		
		BtoGdistance = Math.sqrt(dX*dX + dY*dY);
		
		if(angle < 0) angle = 360+angle;
		double dis = angle - angles[minIndex];
		System.out.println("angle = "+angle+" angles = "+angles[minIndex]+" dis= "+ dis);
		if(dis < 0) {
			if(Math.abs(dis) < 10) {
				angle --;
			}
			else if(Math.abs(dis) >= 10 && Math.abs(dis) <50) angle -= 1.5;
			else angle -= 2;
		}
		else {
			if(Math.abs(dis) < 10) angle ++;
			else if(Math.abs(dis) >= 10 && Math.abs(dis) <50) angle += 1.5;
			else angle += 2;
		}
		System.out.println("angle = "+angle);
		return angle;
	}
	
	private static int getCurrBall(int[][] balls) {
		int num = 0;
		for(int i= 1; i<balls.length; i++) {
			if(balls[i][0]== 0 && balls[i][1] == 0) continue;
			num = i;
			break;
		}
		return num;
	}
}
