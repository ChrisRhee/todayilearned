<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>결과</title>
</head>
<body>
<h1>결과 화면</h1>
<h3>${msg}</h3>
<hr>
<c:url value="/productadd" var="proadd"/>
<a href="${proadd}">물품 추가 등록</a>
<c:url value="/productlist" var="prolist"/>
<a href="${prolist}">물품 목록</a>
<hr>
</body>
</html>