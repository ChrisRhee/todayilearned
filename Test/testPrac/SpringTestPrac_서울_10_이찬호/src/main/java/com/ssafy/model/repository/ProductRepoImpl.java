package com.ssafy.model.repository;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ssafy.model.dto.Product;

@Repository
public class ProductRepoImpl implements ProductRepo {

	private String ns = "productns.";
	
	@Autowired
	SqlSessionTemplate template;
	
	@Override
	public int insert(Product p) {
		return template.insert(ns+"insert",p);
	}

	@Override
	public int update(Product p) {
		return template.update(ns+"update",p);
	}

	@Override
	public int delete(int num) {
		return template.delete(ns+"delete",num);
	}

	@Override
	public Product select(int num) {
		return template.selectOne(ns+"select",num);
	}

	@Override
	public List<Product> selectAll() {
		return template.selectList(ns+"selectAll");
	}

	@Override
	public List<Product> searchByName(String name) {
		return template.selectList(ns+"searchByName",name);
	}

	@Override
	public List<Product> searchByPrice(int price) {
		return template.selectList(ns+"searchByPrice",price);
	}

	@Override
	public List<Product> searchByMount(int mount) {
		return template.selectList(ns+"searchByMount",mount);
	}

}
