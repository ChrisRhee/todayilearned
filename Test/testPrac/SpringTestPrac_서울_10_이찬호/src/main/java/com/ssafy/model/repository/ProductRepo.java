package com.ssafy.model.repository;

import java.util.List;

import com.ssafy.model.dto.Product;

public interface ProductRepo {
	public int insert(Product p);
	public int update(Product p);
	public int delete(int num);
	public Product select(int num);
	public List<Product> selectAll();
	public List<Product> searchByName(String name);
	public List<Product> searchByPrice(int price);
	public List<Product> searchByMount(int mount);
	
}
