package com.ssafy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.ssafy.model.dto.Product;
import com.ssafy.model.service.ProductService;

@Controller
public class MainController {
	
	@Autowired
	ProductService service;
	
	@GetMapping("/index")
	public String mainForm(Model model) {
		return "../../index";
	}
	
	@GetMapping("/productadd")
	public String productAddForm(Model model) {
		return "productadd";
	}
	
	@GetMapping("/productlist")
	public String productListForm(Model model) {
		return "productlist";
	}
	
	@PostMapping("productAddAction")
	public String productAddAction(Model model, Product p) {
		System.out.println(p + "잘 넣었다.");
		model.addAttribute("msg","물품이 등록 되었습니다.");
		service.insert(p);
		return "result";
	}
	
}
