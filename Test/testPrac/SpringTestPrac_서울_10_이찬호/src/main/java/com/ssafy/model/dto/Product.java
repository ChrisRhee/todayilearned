package com.ssafy.model.dto;

public class Product {
	private int num;
	private String name;
	private int price;
	private int mount;
	
	public Product() {
		this(0, "", 0, 0);
	}
	
	public Product(int num, String name, int price, int mount) {
		super();
		setNum(num);
		setName(name);
		setPrice(price);
		setMount(mount);
	}
	
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getMount() {
		return mount;
	}
	public void setMount(int mount) {
		this.mount = mount;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [num=");
		builder.append(getNum());
		builder.append(", name=");
		builder.append(getName());
		builder.append(", price=");
		builder.append(getPrice());
		builder.append(", mount=");
		builder.append(getMount());
		builder.append("]");
		return builder.toString();
	}
	
	
}
