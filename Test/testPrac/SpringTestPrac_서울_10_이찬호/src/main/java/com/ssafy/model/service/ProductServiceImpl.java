package com.ssafy.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafy.model.dto.Product;
import com.ssafy.model.repository.ProductRepo;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepo repo;
	
	@Override
	public int insert(Product p) {
		return repo.insert(p);
	}

	@Override
	public int update(Product p) {
		return repo.update(p);
	}

	@Override
	public int delete(int num) {
		return repo.delete(num);
	}

	@Override
	public Product select(int num) {
		return repo.select(num);
	}

	@Override
	public List<Product> selectAll() {
		return repo.selectAll();
	}

	@Override
	public List<Product> searchByName(String name) {
		return repo.searchByName(name);
	}

	@Override
	public List<Product> searchByPrice(int price) {
		return repo.searchByPrice(price);
	}

	@Override
	public List<Product> searchByMount(int mount) {
		return repo.searchByMount(mount);
	}
}
