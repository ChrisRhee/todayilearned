create table product(
num int(10) primary key,
name varchar(20),
price int(20),
mount int(20)
);

insert into product values(1, "박규빈의 닭가슴살", 50000, 10);
insert into product values(2, "고르마의 알고리즘 정리", 30000, 2);
insert into product values(3, "원호명의 스윗달달", 40000, 5);

commit;

select * from product;
