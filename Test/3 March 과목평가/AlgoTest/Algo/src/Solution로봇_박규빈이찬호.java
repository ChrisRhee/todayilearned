import java.util.Scanner;
/**
 * 작성자
 *  - 이찬호
 * 문제
 *  - 로봇의 이동거리 구하기
 * 조건 
 *  - A는 상 B는 좌우, C는 상하좌우
 *  - 벽을 만나면 못움직임
 *  - 다른 로봇의 초기 위치를 넘어가지 못한다.
 * 입력 
 *  - 테스트 케이스 T
 *  - 각 테스트케이스의 N 칸
 *  - 각 N칸에 대한 로봇, 공간
 * 출력
 *  - 각 로봇들이 총 움직인 거리
 * 풀이
 *  - 로봇들이 움직일 수 있는 방향 만큼씩 움직여주면서 count를 더해준다.
 * */
public class Solution로봇_박규빈이찬호 {
	public static String[][] map;
	public static int[] di = {0,0,-1,1};
	public static int[] dj = {-1,1,0,0};
	public static int N;
	public static int count;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int tc=1; tc<=T; tc++) {
			N = sc.nextInt();
			map = new String[N][N];
			count = 0;
			
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					map[i][j] = sc.next();
				}
			}
			
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					if(!map[i][j].equals("W")&&!map[i][j].equals("S")) {
						switch(map[i][j]) {
						case "A":
							check(i,j,2,3);
							break;
						case "B":
							check(i,j,0,2);
							break;
						case "C":
							check(i,j,0,4);
							break;
							
						}
					}
				}
			}
			System.out.println("#"+tc+" "+count);
		}
	}

	private static void check(int x, int y, int k, int l) {
		for(int d =k; d<l; d++) {
			int ii= x;
			int jj= y;
			while(true) {
				ii += di[d];
				jj += dj[d];
				if(ii < 0 || ii >= N || jj < 0 || jj>= N) break;
				if(map[ii][jj].equals("S")) count++;
				else break;
			}
		}
	}
	
}
