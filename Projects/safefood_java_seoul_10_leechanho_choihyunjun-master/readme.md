# 관통프로젝트 - 안전먹거리

![](./SafeFoodJava_seoul_10_leechanho_choihyunjun/FoodResult.png)


## 내용
- 상품 정보 Jlist에 출력
- 상품 알레르기 유발 품목 검색 후 출력

## 추후 개발 내용
- 고객이 입력한 식품 별로 영양 정보 보여주기
- 로그인 로그아웃

## 개발자
- 이찬호
- 최현준

## 개발 환경
- JAVA EE
- Eclipse IDE

