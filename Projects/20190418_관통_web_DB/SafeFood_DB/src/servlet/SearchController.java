package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.dao.FoodDaoImpl;
import com.ssafy.vo.Food;

public class SearchController implements Controller {
	private FoodDaoImpl dao = FoodDaoImpl.getInstance();
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String sch = request.getParameter("search");
		String stext = request.getParameter("searchFD");
		System.out.println(sch+" "+stext);
		List<Food> foods = new ArrayList<>();
		switch(sch){
			case "whole":
				foods = dao.searchAll(); break;
			case "name":
				foods = dao.searchByName(stext); break;
			case "maker": 
				foods = dao.searchByMaker(stext); break;
			case "material":
				foods = dao.searchByMaterial(stext); break;
			}
		if(foods.size() == 0) request.setAttribute("msg", "검색결과가 없습니다.");
		else request.setAttribute("foods", foods);
		request.getRequestDispatcher("/main.jsp").forward(request, response);
	}
}
