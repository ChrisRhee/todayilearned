package servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.customer.Customer;
import com.ssafy.customer.CustomerDA;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet("*.do")
public class FrontControllerServlet extends HttpServlet {
	private Map<String, Controller> clist;
	private CustomerDA dao = CustomerDA.getInstance();
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config); // WAS제공되는 config 유지
		baseInit();
		clist= new HashMap<>();
		clist.put("/main.do", new IndexController());
		clist.put("/logIn.do", new LoginController());
		clist.put("/logout.do", new LogOutController());
		clist.put("/loginAction.do", new LoginActionController());
		clist.put("/foodInfo.do", new FoodInfoController());
		clist.put("/memberInsert.do", new MemberInsertController());
		clist.put("/memberInsertAction.do", new MemberInsertActionController());
		clist.put("/MemberInfoAction.do", new MemberInfoController());
		clist.put("/memberModify.do", new MemberModifyController());
		clist.put("/Result.do", new ResultController());
		clist.put("/Search.do", new SearchController());
		clist.put("/findpassword.do", new findpasswordController());
		clist.put("/findpasswordAction.do", new findpasswordActionController());
	}
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=utf-8");
		String cmd = request.getRequestURI().substring(request.getContextPath().length());
		System.out.println(cmd);
		System.out.println("frontControllerServlet");
		clist.get(cmd).excute(request, response);
	}
	
	private void baseInit() {
		Customer t = new Customer();
		t.setId("admin");
		t.setName("이찬호");
		t.setPw("1111");
		dao.addCustomer(t);
	}
}
