package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssafy.customer.Customer;
import com.ssafy.customer.CustomerDA;
import com.ssafy.vo.Food;

public class findpasswordActionController implements Controller {
	private CustomerDA cdao = CustomerDA.getInstance();
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		//아이디 입력 안했음
		if(cdao.search(id)==null) {
			if(id.length() == 0) request.setAttribute("msg","아이디를 입력해주세요."); 
			else request.setAttribute("msg","["+id+"]는 존재하지 않는 아이디 입니다.");
		}
		else { // 성공
			if(cdao.search(id).getId().equals(id) && cdao.search(id).getName().equals(name)) {
				request.setAttribute("msg","비밀번호는 ["+cdao.search(id).getPw()+"] 입니다.");
			}else if(cdao.search(id).getId().equals(id) && !cdao.search(id).getName().equals(name)){
				request.setAttribute("msg","잘못 된 이름입니다. 다시 입력해주세요.");
			}
		}
		request.getRequestDispatcher("/findpassword.jsp").forward(request, response);
	}
}
