package com.ssafy.customer;

import java.sql.*;
import java.util.*;

import javax.naming.InitialContext;
import javax.sql.DataSource;

public class CustomerDAOImpl implements CustomerDAO {
	
	private static DataSource ds = null;
	static {
		try {
			InitialContext ctx = new InitialContext();
			ds =(DataSource) ctx.lookup("java:comp/env/jdbc/ssafydb");
			System.out.println("LookUp OK");
		}catch(Exception e) {
			System.out.println("LookUp Error");
			e.printStackTrace();
		}
	}
	
	@Override
	public void close(Statement st) {
		try {
			if(st!=null) st.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void close(Connection con) {
		try {
			if(con!= null) con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void close(ResultSet rs) {
		try {
			if(rs!= null) rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int count() throws SQLException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		int cnt = 0;
		try {
			con= getConnection();
			String sql = "select count(*) from Customer";
			st= con.prepareStatement(sql);
			rs=st.executeQuery(sql);
			while(rs.next()) {
				cnt=rs.getInt(1);
			}
		}finally {
			close(rs);
			close(st);
			close(con);
		}
		return cnt;
	}
	
	@Override
	public Connection getConnection() throws SQLException {
		return ds.getConnection();
	}
	
	@Override
	public void insertCustomer(Customer customer) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = getConnection();
			String sql = "Insert into Customer values (?, ?, ?)";
			st = con.prepareStatement(sql);
			st.setString(1, customer.getId());
			st.setString(2, customer.getPw());
			st.setString(3, customer.getName());
			int cnt = st.executeUpdate();
			System.out.println("insert : " + cnt);
		}finally {
			close(st);
			close(con);
		}
	}

	@Override
	public void updateCustomer(Customer customer) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = getConnection();
			String sql = "update Customer set code=?, name=?, price=? where code = ?";
			st = con.prepareStatement(sql);
			st.setString(1, customer.getId());
			st.setString(2, customer.getPw());
			st.setString(3, customer.getName());
			st.setString(4, customer.getId());
			
			int cnt = st.executeUpdate();
			System.out.println("Update : " + cnt);
		} finally {
			close(st);
			close(con);
		}
	}

	@Override
	public void deleteCustomer(String id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = getConnection();
			String sql = "delete from Customer where code = ?";
			st = con.prepareStatement(sql);
			st.setString(1, id);
			int cnt = st.executeUpdate();
			System.out.println("Delete : " + cnt);
		}finally {
			close(st);
			close(con);
		}
	}

	@Override
	public Customer findCustomer(String id) throws SQLException {
		Customer temp = null;
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			String sql = "select * from Customer where id='"+id+"'";
			st = con.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next()) {
				temp = new Customer(rs.getString(1),rs.getString(2),rs.getString(3));
			}
		} finally {
			close(rs);
			close(st);
			close(con);
		}
		
		return temp;
	}

	@Override
	public List<Customer> listCustomers() throws SQLException {
		List<Customer> temp = new ArrayList<>();
		Customer b = null;
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			st = con.createStatement();
			String sql = "select * from Customer";
			rs = st.executeQuery(sql);
			while(rs.next()) {
				b = new Customer(rs.getString(1),rs.getString(2),rs.getString(3));
				temp.add(b);
			}
		}finally {
			close(rs);
			close(st);
			close(con);
		}
		return temp;
	}
}
