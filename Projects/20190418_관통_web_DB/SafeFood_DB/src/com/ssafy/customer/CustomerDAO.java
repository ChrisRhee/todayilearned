package com.ssafy.customer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;


public interface CustomerDAO {
	public void insertCustomer(Customer customer) throws SQLException;
	public void updateCustomer(Customer customer) throws SQLException;
	public void deleteCustomer(String code) throws SQLException ;
	public Customer findCustomer(String code) throws SQLException ;
	public List<Customer> listCustomers() throws SQLException ;
	public int count() throws SQLException;
	public void close(Statement st);
	public void close(Connection con) ;
	public void close(ResultSet rs);
	public Connection getConnection() throws SQLException;
}
