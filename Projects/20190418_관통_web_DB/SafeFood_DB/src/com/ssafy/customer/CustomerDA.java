package com.ssafy.customer;

import java.util.ArrayList;
import java.util.List;

public class CustomerDA {
	private List<Customer> Clist;
	
	
	private static CustomerDA instance;
	private CustomerDA() {
		Clist = new ArrayList<>();
	}
	public static CustomerDA getInstance() {
		if(instance ==null) {
			instance = new CustomerDA();
		}
		return instance;
	}

	public void addCustomer(Customer c) {
		Clist.add(c);
	}
	
	public Customer search(String id) {
		for(int i=0; i<Clist.size(); i++) {
			if(Clist.get(i).getId().equals(id)) {
				return Clist.get(i);
			}
		}
		return null;
	}
	
	// 비번 or 이름 변경
	public void editPW(Customer c,String newPw) {
		search(c.getId()).setPw(newPw);
	}
	
	public void delete(Customer c) {
		if(search(c.getId())!=null) {
			Clist.remove(c);
		}
	}
}
