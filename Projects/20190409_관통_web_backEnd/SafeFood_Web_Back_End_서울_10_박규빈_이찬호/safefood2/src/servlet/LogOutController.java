package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssafy.dao.FoodDaoImpl;
import com.ssafy.vo.Food;

public class LogOutController implements Controller {
	private FoodDaoImpl fdao = FoodDaoImpl.getInstance();
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if(session != null) {
			session.invalidate();
		}
		List<Food> temp = fdao.searchAll();
		request.setAttribute("foods", temp);
		request.getRequestDispatcher("/main.jsp").forward(request, response);
	}

}
