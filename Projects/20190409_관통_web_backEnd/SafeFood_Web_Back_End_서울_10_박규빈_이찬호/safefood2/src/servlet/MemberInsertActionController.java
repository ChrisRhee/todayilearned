package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssafy.customer.Customer;
import com.ssafy.customer.CustomerDAO;
import com.ssafy.dao.FoodDaoImpl;
import com.ssafy.vo.Food;

public class MemberInsertActionController implements Controller {
	private CustomerDAO dao = CustomerDAO.getInstance();
	private FoodDaoImpl fdao = FoodDaoImpl.getInstance();
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 1. 입력값
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		String pwCheck = request.getParameter("pwCheck");
		String name = request.getParameter("name");
		
		// 2. dao 에서 회원 가입
		if(dao.search(id)==null && pw.equals(pwCheck)) {
			System.out.println("회원가입할랭");
			Customer c = new Customer();
			c.setId(id);
			c.setPw(pw);
			c.setName(name);
			dao.addCustomer(c);
			
			HttpSession session = request.getSession(true);
			session.setAttribute("user", dao.search(id));
			
			List<Food> temp = fdao.searchAll();
			request.setAttribute("foods", temp);
			request.getRequestDispatcher("/main.jsp").forward(request, response);
		}else {
			if(dao.search(id)!=null) {
				System.out.println("이미 있는 아이디야!");
			}else {
				System.out.println("비밀번호가 틀려!");
			}
			request.getRequestDispatcher("/memberInsert.jsp").forward(request, response);
		}
	}
}
