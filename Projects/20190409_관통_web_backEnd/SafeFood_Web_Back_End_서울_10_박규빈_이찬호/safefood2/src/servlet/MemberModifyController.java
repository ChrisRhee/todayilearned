package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssafy.customer.Customer;
import com.ssafy.customer.CustomerDAO;
import com.ssafy.dao.FoodDaoImpl;
import com.ssafy.vo.Food;

public class MemberModifyController implements Controller {
	private CustomerDAO dao = CustomerDAO.getInstance();
	private FoodDaoImpl fdao = FoodDaoImpl.getInstance();
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		String name = request.getParameter("name");
		List<Food> temp = fdao.searchAll();
		Customer c = dao.search(id);
		if(c.getId().equals(id) && c.getName().equals(name) && c.getPw().equals(pw)) {
			//탈퇴
			dao.delete(c);
			System.out.println("삭제완료");
			HttpSession session = request.getSession();
			if(session != null) {
				session.invalidate();
			}
			//request.setAttribute("msg","["+id+"]는 존재하지 않는 아이디 입니다.");
			request.setAttribute("foods", temp);
			request.getRequestDispatcher("/main.jsp").forward(request, response);
		}else { // 수정
			c.setId(id);
			c.setPw(pw);
			c.setName(name);
			System.out.println("수정완료");
			request.setAttribute("foods", temp);
			request.getRequestDispatcher("/main.jsp").forward(request, response);
		}
	}

}
