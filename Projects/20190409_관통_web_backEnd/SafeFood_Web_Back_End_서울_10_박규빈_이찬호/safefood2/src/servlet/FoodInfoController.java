package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.dao.FoodDaoImpl;
import com.ssafy.vo.Food;

public class FoodInfoController implements Controller {
	private FoodDaoImpl dao = FoodDaoImpl.getInstance();
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String temp = request.getParameter("food");
		Food food = dao.search(Integer.parseInt(temp));
		request.setAttribute("food", food);
		request.getRequestDispatcher("/FoodInfo.jsp").forward(request, response);
	}
}
