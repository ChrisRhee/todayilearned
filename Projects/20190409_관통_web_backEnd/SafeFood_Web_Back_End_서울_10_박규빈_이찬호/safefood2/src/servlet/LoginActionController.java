package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssafy.customer.CustomerDAO;
import com.ssafy.dao.FoodDaoImpl;
import com.ssafy.vo.Food;

public class LoginActionController implements Controller {
	private CustomerDAO cdao = CustomerDAO.getInstance();
	private FoodDaoImpl fdao = FoodDaoImpl.getInstance();
	
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 시작페이지 역할
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		// 서버에서 처리할 내용 있으면 처리 하고
		System.out.println(id+" "+pw);
		// 실패
		if(cdao.search(id)==null) {
			if(id.length() == 0) request.setAttribute("msg","id를 입력해주세요."); 
			else request.setAttribute("msg","["+id+"]는 존재하지 않는 아이디 입니다.");
			request.getRequestDispatcher("/Login.jsp").forward(request, response);
		}
		else { // 성공
			if(cdao.search(id).getId().equals(id) && cdao.search(id).getPw().equals(pw)) {
				HttpSession session = request.getSession(true);
				List<Food> temp = fdao.searchAll();
				session.setAttribute("user", cdao.search(id));
				request.setAttribute("foods", temp);
				request.getRequestDispatcher("/main.jsp").forward(request, response);
			}else {
				request.setAttribute("msg","잘못 된 비밀번호입니다. 다시 입력해주세요.");
				request.getRequestDispatcher("/Login.jsp").forward(request, response);
			}
		}
	}
}
