package com.ssafy.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.dao.CustomerDAO;
import com.ssafy.vo.Customer;

public class MemberDeleteController implements Controller {
	private CustomerDAO dao = CustomerDAO.getInstance();

	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		System.out.println(id+"를 지울것이다!");
		Customer c = dao.search(id);
		dao.delete(c);
		
		request.getRequestDispatcher("/main.jsp").forward(request, response);
	}

}
