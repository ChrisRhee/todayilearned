package com.ssafy.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.dao.CustomerDAO;
import com.ssafy.vo.Customer;

public class MemberModifyController implements Controller {
	private CustomerDAO dao = CustomerDAO.getInstance();
	
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		String name = request.getParameter("name");
		
		Customer c = dao.search(id);
		if(c.getId().equals(id) && c.getName().equals(name) && c.getPw().equals(pw)) {
			//탈퇴
			dao.delete(c);
			System.out.println("삭제완료");
			request.getRequestDispatcher("/main.jsp").forward(request, response);
		}else { // 수정
			c.setId(id);
			c.setPw(pw);
			c.setName(name);
			System.out.println("수정완료");
			request.getRequestDispatcher("/main.jsp").forward(request, response);
		}
	}

}
