package com.ssafy.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.dao.CustomerDAO;
import com.ssafy.vo.Customer;

public class MemberInsertActionController implements Controller {
	private CustomerDAO dao = CustomerDAO.getInstance();

	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 1. 입력값
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		String name = request.getParameter("name");
		String pwCheck = request.getParameter("pwCheck");
		System.out.println(id+" "+pw+" "+pwCheck+" "+name);
		// 2. dao 에서 회원 가입
		if(dao.search(id)==null && pw.equals(pwCheck)) {
			System.out.println("회원가입할랭");
			Customer c = new Customer();
			c.setId(id);
			c.setPw(pw);
			c.setName(name);
			dao.addCustomer(c);
			request.getRequestDispatcher("/main.jsp").forward(request, response);
		}else {
			if(dao.search(id)!=null) {
				System.out.println("이미 있는 아이디야!");
			}else {
				System.out.println("비밀번호가 틀려!");
			}
			request.getRequestDispatcher("/memberInsert.jsp").forward(request, response);
		}
	}
}
