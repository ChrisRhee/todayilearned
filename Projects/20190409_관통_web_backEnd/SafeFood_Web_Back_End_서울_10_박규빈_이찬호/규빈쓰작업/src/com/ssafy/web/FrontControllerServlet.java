package com.ssafy.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MainController
 */
@WebServlet("*.do")
public class FrontControllerServlet extends HttpServlet {
	private Map<String, Controller> clist;
//	private CustomerDAO dao = CustomerDAO.getInstance();
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config); // WAS제공되는 config 유지
		clist= new HashMap<>();
		clist.put("/main.do",new IndexController());
		clist.put("/login.do", new LoginController());
		clist.put("/logout.do", new LogOutController());
		clist.put("/loginAction.do", new LoginActionController());
		clist.put("/memberInsert.do", new MemberInsertController());
		clist.put("/memberInsertAction.do", new MemberInsertActionController());
		clist.put("/memberInfo.do", new MemberInfoController());
		clist.put("/memberModify.do", new MemberModifyController());
		clist.put("/memberDelete.do", new MemberDeleteController());
		clist.put("/Result.do", new ResultController());
		clist.put("/Search.do", new SearchController());
		clist.put("/Delete.do", new DeleteController());
	}
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=utf-8");
		String cmd = request.getRequestURI().substring(request.getContextPath().length());
		System.out.println(cmd);
		System.out.println("frontControllerServlet");
		clist.get(cmd).excute(request, response);
	}

}
