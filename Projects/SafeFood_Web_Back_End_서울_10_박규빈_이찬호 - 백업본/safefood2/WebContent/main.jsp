<%@page import="com.ssafy.customer.Customer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.ssafy.vo.Food"%>
<%@page import="java.util.List"%>

<% List<Food> fs = (List<Food>) request.getAttribute("foods");
Customer cus = (Customer) session.getAttribute("user"); %>

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="styles.css">
<style type="text/css">
#searchInputField {
	width: 200px;
}
#cssmenu,
#cssmenu ul,
#cssmenu ul li,
#cssmenu ul li a {
  margin: 0;
  padding: 0;
  border: 0;
  list-style: none;
  line-height: 1;
  display: block;
  position: relative;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
#cssmenu:after,
#cssmenu > ul:after {
  content: ".";
  display: block;
  clear: both;
  visibility: hidden;
  line-height: 0;
  height: 0;
}
#cssmenu {
  width: auto;
  font-family: Raleway, sans-serif;
  line-height: 1;
}
#cssmenu > ul > li {
  float: left;
}
#cssmenu.align-center > ul {
  font-size: 0;
  text-align: center;
}
#cssmenu.align-center > ul > li {
  display: inline-block;
  float: none;
}
#cssmenu.align-right > ul > li {
  float: right;
}
#cssmenu.align-right > ul > li > a {
  margin-right: 0;
  margin-left: -4px;
}
#cssmenu > ul > li > a {
  z-index: 2;
  padding: 18px 25px 12px 25px;
  font-size: 15px;
  font-weight: 400;
  text-decoration: none;
  color: #ffffff;
  -webkit-transition: all .2s ease;
  -moz-transition: all .2s ease;
  -ms-transition: all .2s ease;
  -o-transition: all .2s ease;
  transition: all .2s ease;
  margin-right: -4px;
}
#cssmenu > ul > li:hover > a,
#cssmenu > ul > li > a:hover {
  color: #444444;
}
#cssmenu > ul > li > a:after {
  position: absolute;
  left: 0;
  bottom: 0;
  right: 0;
  z-index: -1;
  width: 100%;
  height: 120%;
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;
  content: "";
  -webkit-transition: all .2s ease;
  -o-transition: all .2s ease;
  transition: all .2s ease;
  -webkit-transform: perspective(5px) rotateX(2deg);
  -webkit-transform-origin: bottom;
  -moz-transform: perspective(5px) rotateX(2deg);
  -moz-transform-origin: bottom;
  transform: perspective(5px) rotateX(2deg);
  transform-origin: bottom;
}

#mainbar {
	background-image: url("img/background.png");
	width:100%;
}
#search {
	text-align: center;
	color : black;
}
#searchBox{
	width: 200px;
}

.navbar {
  background: hotpink;
  margin: 0; padding: 0;
  list-style: none;
  position: fixed;
  width: 100%;
}
.navbar>li {
  display: inline-block;
}
.navbar>li>a {
  display: block;
  text-decoration: none;
  padding: 10px 20px;
  color: white;
}
.navbar>li>a:hover {
  background: deeppink;
}
.aa{
  color: white;
  display: block;
}
</style>
</head>
<body>
	<div id="mainbar">
		<div class="aa">
			<li><a href='logIn.do'>로그인</a></li>
			<li><a href='MemberInsert.do'>회원가입</a></li>
		</div>

		<div id='cssmenu'>
			<ul>
				<li><a href='#'>공지사항</a></li>
				<li><a href=''>상품 정보</a></li>
				<li><a href=''>베스트 섭취 정보</a></li>
				<li><a href=''>내 섭취 정보</a></li>
				<li><a href=''>예상 섭취 정보</a></li>
			</ul>
		</div>

		<div id="search">
			<h1>WHAT WE PROVIDE</h1>
			<h3>건강한 삶을 위한 먹거리 프로젝트</h3>
			<div class='center-block' id="searchBox">
				<form method="post" action="Search.do">
					<select name="search">
						<option value="whole">전체</option>
						<option value="name">제품명</option>
						<option value="maker">제조사</option>
						<option value="material">재료</option>
					</select> <input type="text" name="searchFD"> <input type="submit"
						name="searchButton" value="검색">
				</form>
			</div>
		</div>
	</div>
	<section>
		<table class="resultTable">
			<h4>${msg}</h4>
			<%
				for (int i = 0; i < fs.size(); i++) {
			%>
			<tr>
				<td class='book-img'><img width="100" alt="img"
					src="<%=fs.get(i).getImg()%>"></td>
				<td class='book-name'>
					<div>
						<h4>
							<a href="foodInfo.do?food=<%=fs.get(i).getCode()%>"><%=fs.get(i).getName()%></a>
						</h4>
						<a href="foodInfo.do?food=<%=fs.get(i).getCode()%>"> <%=fs.get(i).getMaterial()%></a>
						<div>
							<input type="button" value="추가"> <input type="button"
								value="찜">
						</div>
					</div>
			</tr>
			<%
				}
			%>
		</table>
	</section>
	<footer>
		<div>
			<h3>Find Us</h3>
		</div>
	</footer>
</body>
