<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.ssafy.vo.Food"%>

<% Food food = (Food) request.getAttribute("food");
%>

<style type="text/css">
	#imgbox{
		float : left;
	}
	#textbox{
		float: left;
	}
</style>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="js/jquery-3.1.1.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="styles.css">

</head>
<body>
	<header>
		<div id='cssmenu'>
			<ul>
				<li><a href='#'>공지사항</a></li>
				<li><a href=''>상품 정보</a></li>
				<li><a href=''>베스트 섭취 정보</a></li>
				<li><a href=''>내 섭취 정보</a></li>
				<li><a href=''>예상 섭취 정보</a></li>
				<li><a href=''>로그인</a></li>
				<li><a href=''>회원정보</a></li>
			</ul>
		</div>
	</header>
	<div id="info">
		<h1>제품정보</h1>
		<h1 class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></h1>
		<br>
		
		<div>
			<div class="imgbox">
				<img width="300" class="foodimg" src="<%=food.getImg()%>">
			</div>
			<div class="textbox">
				<p>
					제품명 <span id="name"><%=food.getName()%></span>
				</p>
				<p>
					제조사 <span id="maker"><%=food.getMaker()%></span>
				</p>
				<p>
					원재료 <span id="material"><%=food.getMaterial()%></span>
				</p>
				<p>
					알레르기 성분 <span></span>
				</p>
				<p>Quantity</p>
				<input type="S">
				<button id="btn2" class="btn btn-outline-success my-2 my-sm-0"
					type="submit">
					<span class="glyphicon glyphicon-plus" aria-hidden="true">추가</span>
				</button>
				<button id="btn3" class="btn btn-outline-success my-2 my-sm-0"
					type="submit">
					<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true">찜</span>
				</button>
			</div>
		</div>
	</div>
	<br class="clear">
	<div id="donutchart" style="width: 900px; height: 500px;"></div>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>