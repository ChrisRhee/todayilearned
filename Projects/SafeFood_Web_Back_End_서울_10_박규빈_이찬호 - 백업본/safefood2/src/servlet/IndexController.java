package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.dao.FoodDaoImpl;
import com.ssafy.vo.Food;

public class IndexController implements Controller {
	private FoodDaoImpl dao = FoodDaoImpl.getInstance();
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 시작페이지 역할
		List<Food> temp = dao.searchAll();
		request.setAttribute("foods", temp);
		request.getRequestDispatcher("/main.jsp").forward(request, response);
	}
}
