package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.customer.Customer;
import com.ssafy.customer.CustomerDAO;

public class MemberInsertActionController implements Controller {
	private CustomerDAO dao = CustomerDAO.getInstance();

	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 1. 입력값
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		String pwCheck = request.getParameter("pwCheck");
		String name = request.getParameter("name");
		
		// 2. dao 에서 회원 가입
		if(pw.equals(pwCheck)) {
			Customer c = new Customer();
			c.setId(id);
			c.setPw(pw);
			c.setName(name);
			dao.addCustomer(c);
			request.getRequestDispatcher("/memberInsertSuccess.jsp").forward(request, response);
		}else {
			request.setAttribute("msg","패스워드가 일치하지 않습니다.");
			request.getRequestDispatcher("/Error.jsp").forward(request, response);
		}
	}
}
