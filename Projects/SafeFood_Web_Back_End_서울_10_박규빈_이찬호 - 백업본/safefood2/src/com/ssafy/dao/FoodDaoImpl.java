package com.ssafy.dao;

import java.util.*;
import com.ssafy.util.FoodSaxParser;
import com.ssafy.vo.Food;

public class FoodDaoImpl implements FoodDao{
	private List<Food> foods;
	
	private static FoodDaoImpl instance;
	/** Singleton */
	private FoodDaoImpl() {
		foods = new LinkedList<Food>();
		loadData();
	}
	public static FoodDaoImpl getInstance() {
		if(instance == null) {
			instance= new FoodDaoImpl();
		}
		return instance;
	}
	
	/**
	 * 식품 영양학 정보와 식품 정보를  xml 파일에서 읽어온다.
	 */
	public void loadData() {
		FoodSaxParser fsp=new FoodSaxParser();
		foods = fsp.getFoods();
	}
	
	/**
	 * 검색 조건(key) 검색 단어(word)에 해당하는 식품 정보(Food)를  검색해서 반환.  
	 * @param bean  검색 조건과 검색 단어가 있는 객체
	 * @return 조회한 식품 목록
	 */
	
	public List<Food> searchByName(String name){
		List<Food> finds = new LinkedList<Food>();
		for (Food food : foods) {
			if(food.getName().contains(name)) {
				finds.add(food);
			}
		}
		return finds;
	}
	
	public List<Food> searchByMaker(String maker){
		List<Food> finds = new LinkedList<Food>();
		for (Food food : foods) {
			if(food.getMaker().contains(maker)) {
				finds.add(food);
			}
		}
		return finds;
	}
	
	public List<Food> searchByMaterial(String material){
		List<Food> finds = new LinkedList<Food>();
		for (Food food : foods) {
			if(food.getMaterial().contains(material)) {
				finds.add(food);
			}
		}
		return finds;
	}
	
	public List<Food> searchAll(){
		return foods;
	}
	
	/**
	 * 식품 코드에 해당하는 식품정보를 검색해서 반환. 
	 * @param code	검색할 식품 코드
	 * @return	식품 코드에 해당하는 식품 정보, 없으면 null이 리턴됨
	 */
	public Food search(int code) {
		for (Food f : foods) {
			if(f.getCode()==code) return f;
		}
		// 코드에 맞는 식품 검색하여 리턴
		return null;
	}

	/**
	 * 가장 많이 검색한 Food  정보 리턴하기 
	 * web에서 구현할 내용.  
	 * @return
	 */
	public List<Food> searchBest() {
		return null;
	}
	
	public List<Food> searchBestIndex() {
		return null;
	}
	
	public static void print(List<Food> foods) {
		for (Food food : foods) {
			System.out.println(food);
		}
	}
}
