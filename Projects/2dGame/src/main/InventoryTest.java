package main;

import UI.Inventory.IConsumeInvenDAOImpl;
import UI.Inventory.IInventoryDAO;
import UI.Item.ItemConsume;

public class InventoryTest {
	public static void main(String[] args) {
		IInventoryDAO consumeInven = IConsumeInvenDAOImpl.getInstance();

		ItemConsume redPostion = new ItemConsume("빨간 포션","마시면 hp가 100이 회복된다.","소비 아이템",200, "");
		consumeInven.add(redPostion, 49);
		consumeInven.add(new ItemConsume("파란 포션","마시면 mp가 100이 회복된다.","소비 아이템",100, ""),50);
		consumeInven.add(redPostion, 50);
		consumeInven.add(redPostion, 50);
		consumeInven.add(redPostion, 50);
		consumeInven.add(redPostion, 50);
		consumeInven.add(redPostion, 50);
		consumeInven.add(redPostion, 50);
		consumeInven.add(redPostion, 50);
		consumeInven.add(redPostion, 50);
		consumeInven.add(redPostion, 50);
		consumeInven.add(redPostion, 50);
		
		int i = 1;
		for (ItemConsume item : consumeInven.searchAll()) {
			System.out.println(i++ +" "+ item);
		}
	}

}
