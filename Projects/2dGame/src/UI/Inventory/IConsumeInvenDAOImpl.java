package UI.Inventory;

import java.util.ArrayList;
import java.util.List;

import UI.Item.ItemList;
import UI.Item.ItemConsume;

public class IConsumeInvenDAOImpl implements IInventoryDAO{
	private List<ItemConsume> listConsume;
	private ItemList itemlist;
	
	private static IConsumeInvenDAOImpl instance;
	private IConsumeInvenDAOImpl() {
		listConsume = new ArrayList<ItemConsume>();
		itemlist = new ItemList();
	}
	public static IConsumeInvenDAOImpl getInstance() {
		if(instance==null) {
			instance = new IConsumeInvenDAOImpl();
		}
		return instance;
	}

	// 아이템 추가
	@Override
	public void add(ItemConsume item) {
		listConsume.add(item);
	}
	
	// sobi 추가
	public void add(ItemConsume item, int mount) {
		try {
			ItemConsume c = search(item); 
			if(c.getMount()+mount < c.getMaxMount())
				c.setMount(c.getMount()+mount);
			else {
				int m = (c.getMount()+mount) - c.getMaxMount();
				c.setMount(c.getMaxMount());
				ItemConsume d = itemlist.getItem(item.getName());
				d.setMount(m);
				listConsume.add(d);
			}
			
		} catch (NotFoundException e) {
			item.setMount(mount);
			listConsume.add(item);
		}
	}
	
	// 전체 검색
	public List<ItemConsume> searchAll() {
		return listConsume;
	}
	
	// 인벤토리에 있는 아이템 검색
	public ItemConsume search(ItemConsume itemsobi) throws NotFoundException {
		for (ItemConsume item : listConsume) {
			if(item.equals(itemsobi) && item.getMount() < item.getMaxMount()) {
				return item;
			}
		}
		throw new NotFoundException();
	}
	
	// 정렬
	
	
}
