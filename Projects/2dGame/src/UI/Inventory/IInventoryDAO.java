package UI.Inventory;

import java.util.List;

import UI.Item.ItemConsume;


public interface IInventoryDAO {
	public void add(ItemConsume item);
	public void add(ItemConsume item, int mount);
	public List<ItemConsume> searchAll();
}
