package UI.Item;

public class Item {
	private String name;            // 아이템 이름
	private String classification;  // 아이템 종류
	private String img;             // 아이템 이미지
	private String desc;            // 아이템 설명
	private String subDesc;         // 아이템 속성 설명 (주황글씨)
	
	
	public Item(String name, String classification, String img, String desc, String subDesc) {
		super();
		setName(name);
		setClassification(classification);
		setImg(img);
		setDesc(subDesc);
		setSubDesc(subDesc);
	}

	
	
	public Item(String name) {
		this(name, "", "", "", "");
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getClassification() {
		return classification;
	}
	public void setClassification(String classification) {
		this.classification = classification;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getSubDesc() {
		return subDesc;
	}
	public void setSubDesc(String subDesc) {
		this.subDesc = subDesc;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}



	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("name=");
		builder.append(name);
		return builder.toString();
	}
	
	
	
	
}
