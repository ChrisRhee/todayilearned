package UI.Item;

import java.util.ArrayList;
import java.util.List;

public class ItemList {
	List<ItemConsume> consumeList;
	
	// 아이템 이름
	// 아이템 종류
	// 아이템 이미지
	// 아이템 설명
	// 아이템 속성 설명 (주황글씨)
	// 아이템 수량
		
	public ItemList() {
		consumeList = new ArrayList<ItemConsume>();
		
		consumeList.add(new ItemConsume("빨간 포션","마시면 hp가 100이 회복된다.","소비 아이템",100, ""));
		consumeList.add(new ItemConsume("파란 포션","마시면 mp가 100이 회복된다.","소비 아이템",100, ""));
	}
	
	/** 이름으로 찾아서 그아이템 보내줌 
	 * param : String name
	 * return : ItemConsum
	 * */
	public ItemConsume getItem(String name) {
		for (ItemConsume item : consumeList) {
			if(item.getName().equals(name)) {
				return item;
			}
		}
		return null;
	}
}
