package UI.Item;

public class ItemEquipment extends Item{
	
//	private String name;            // 아이템 이름
//	private String classification;  // 아이템 종류
//	private String img;             // 아이템 이미지
//	private String desc;            // 아이템 설명
//	private String subDesc;         // 아이템 속성 설명 (주황글씨)
	
	private String job;             // 장비 직업
	private int reqLevel;           // 장비 최소레벨
	private int baseSTR;            // 장비 기본 STR
	private int baseDEX;            // 장비 기본 DEX
	private int baseINT;            // 장비 기본 INT
	private int baseLUK;            // 장비 기본 LUK
	private int baseATTACT;         // 장비 기본 공격력
	private int baseMASIC;          // 장비 기본 마법공격력
	private int equipClass;         // 장비 분류
	
	public ItemEquipment(String name, String classification, String img, String desc, String subDesc, String job,
			int reqLevel, int baseSTR, int baseDEX, int baseINT, int baseLUK, int baseATTACT, int baseMASIC,
			int equipClass) {
		super(name, classification, img, desc, subDesc);
		setJob(job);
		setReqLevel(reqLevel);
		setBaseSTR(baseSTR);
		setBaseDEX(baseDEX);
		setBaseINT(baseINT);
		setBaseLUK(baseLUK);
		setBaseATTACT(baseATTACT);
		setBaseMASIC(baseMASIC);
		setEquipClass(equipClass);
	}
	
	
	
	
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public int getReqLevel() {
		return reqLevel;
	}
	public void setReqLevel(int reqLevel) {
		this.reqLevel = reqLevel;
	}
	public int getBaseSTR() {
		return baseSTR;
	}
	public void setBaseSTR(int baseSTR) {
		this.baseSTR = baseSTR;
	}
	public int getBaseDEX() {
		return baseDEX;
	}
	public void setBaseDEX(int baseDEX) {
		this.baseDEX = baseDEX;
	}
	public int getBaseINT() {
		return baseINT;
	}
	public void setBaseINT(int baseINT) {
		this.baseINT = baseINT;
	}
	public int getBaseLUK() {
		return baseLUK;
	}
	public void setBaseLUK(int baseLUK) {
		this.baseLUK = baseLUK;
	}
	public int getBaseATTACT() {
		return baseATTACT;
	}
	public void setBaseATTACT(int baseATTACT) {
		this.baseATTACT = baseATTACT;
	}
	public int getBaseMASIC() {
		return baseMASIC;
	}
	public void setBaseMASIC(int baseMASIC) {
		this.baseMASIC = baseMASIC;
	}
	public int getEquipClass() {
		return equipClass;
	}
	public void setEquipClass(int equipClass) {
		this.equipClass = equipClass;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + baseATTACT;
		result = prime * result + baseDEX;
		result = prime * result + baseINT;
		result = prime * result + baseLUK;
		result = prime * result + baseMASIC;
		result = prime * result + baseSTR;
		result = prime * result + equipClass;
		result = prime * result + ((job == null) ? 0 : job.hashCode());
		result = prime * result + reqLevel;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemEquipment other = (ItemEquipment) obj;
		if (baseATTACT != other.baseATTACT)
			return false;
		if (baseDEX != other.baseDEX)
			return false;
		if (baseINT != other.baseINT)
			return false;
		if (baseLUK != other.baseLUK)
			return false;
		if (baseMASIC != other.baseMASIC)
			return false;
		if (baseSTR != other.baseSTR)
			return false;
		if (equipClass != other.equipClass)
			return false;
		if (job == null) {
			if (other.job != null)
				return false;
		} else if (!job.equals(other.job))
			return false;
		if (reqLevel != other.reqLevel)
			return false;
		return true;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.toString());
		builder.append("ItemEquipment [job=");
		builder.append(job);
		builder.append(", reqLevel=");
		builder.append(reqLevel);
		builder.append(", baseSTR=");
		builder.append(baseSTR);
		builder.append(", baseDEX=");
		builder.append(baseDEX);
		builder.append(", baseINT=");
		builder.append(baseINT);
		builder.append(", baseLUK=");
		builder.append(baseLUK);
		builder.append(", baseATTACT=");
		builder.append(baseATTACT);
		builder.append(", baseMASIC=");
		builder.append(baseMASIC);
		builder.append(", equipClass=");
		builder.append(equipClass);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	
	
}
