package UI.Item;

public class ItemConsume {
	private String name;
	private String desc;
	private String subDesc;
	private int mount;              // 아이템 수량
	private int maxMount;
	private String img;

	public ItemConsume(String name, String desc, String subDesc, int maxMount, String img) {
		super();
		setName(name);
		setDesc(subDesc);
		setSubDesc(subDesc);
		setMount(0);
		setMaxMount(maxMount);
		setImg(img);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getSubDesc() {
		return subDesc;
	}

	public void setSubDesc(String subDesc) {
		this.subDesc = subDesc;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public int getMount() {
		return mount;
	}

	public void setMount(int mount) {
		this.mount = mount;
	}

	public int getMaxMount() {
		return maxMount;
	}

	public void setMaxMount(int maxMount) {
		this.maxMount = maxMount;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemConsume other = (ItemConsume) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getName());
		builder.append(" ");
		builder.append(getMount());
		builder.append("개");
		return builder.toString();
	}

	
}
