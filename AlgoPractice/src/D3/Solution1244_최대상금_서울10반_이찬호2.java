package D3;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
class Solution1244_최대상금_서울10반_이찬호2{
	public static void swap(int a, int b) {
		int t = a;
		a = b;
		b = t;
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1244.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			String s[] = br.readLine().split(" ");
			int rest[] = new int[s[0].length()];
			String num[] = s[0].split("");
			int maxIndex =0;
			int jStart =0 ;
			System.out.println(Arrays.toString(num)+" "+s[1]);
			for(int i =0; i< Integer.parseInt(s[1]); i++){
				int max =0;
				for(int j=maxIndex; j<num.length-1; j++) {
					if(Integer.parseInt(num[j]) >= max && j!=maxIndex) {
						max = Integer.parseInt(num[j]);
						maxIndex++;
					}
				}
				for(int j= num.length-1; j>=jStart; j--) {
					if(j == jStart) continue;
					//System.out.println(num[jStart]+" "+num[j]);
					if(Integer.parseInt(num[j]) == max) {
						rest[j] = Integer.parseInt(num[j]);
						String t = num[jStart];
						num[jStart] = num[j];
						num[j] = t;
						jStart++;
					}
				}
				//System.out.println(max);
				//System.out.println(Arrays.toString(num) +" "+ (i+1));
			}
			System.out.println(Arrays.toString(rest));
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
}