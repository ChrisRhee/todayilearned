package D3;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution3307_최장증가부분수열{
	
	public static int N;
	public static int[] nums;
	
	private static int solve(int i, int count) {
		//if(i == N-1) return 1;
		int temp =count;
		for(int j=i+1; j<N; j++) {
			if(nums[i] >= nums[j]) continue;
			temp = solve(j, temp+1);
			break;
		}
		//System.out.println(temp);
		return temp;
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3307.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		//int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			int max =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			N= sc.nextInt();
			nums = new int[N];
			for(int i =0; i<N; i++) {
				nums[i] = sc.nextInt();
			}
			for(int i=0; i<N; i++) {
				Ans = solve(i,1);
				if(max<Ans) max = Ans;
				//System.out.println(i+"= "+solve(i,1));
			}
			System.out.println("#"+tc + " "+max);
		}
		br.close();
	}

	
}