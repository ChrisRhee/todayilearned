package D3;
import java.io.*;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution5215_햄버거다이어트_서울10반_이찬호{
	public static int[][] map;
	public static int n,L;
	
	public static int get() {
		int max =0;
		for(int i=1; i<(1<<n); i++) {
			int temp =0;
			int sum =0;
			for(int j=0; j<n; j++) {
				if(sum > L) break;
				if((i&(1<<j))>0) {
					sum+= map[j][1];
					temp+= map[j][0];
				}
			}
			if(sum > L) continue;
			if(max <temp) max = temp;
		}
		return max;
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input5215.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		for(int tc = 1; tc <= T; tc++){
			n= sc.nextInt();
			L= sc.nextInt();
			map = new int[n][2];
			for(int i =0; i<n; i++) {
				map[i][0] = sc.nextInt();
				map[i][1] = sc.nextInt();
			}
			System.out.println("#"+tc + " "+get());
		}
	}
}