package D3;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
class Solution1244_최대상금_서울10반_이찬호{
	public static int num[],count,tem[],index[],ii,max,curr;
	public static void swap(int a, int b) {
		int t = num[a];
		num[a] = num[b];
		num[b] = t;
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1244.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			String s[] = br.readLine().split(" ");
			String t[] = s[0].split("");
			num = new int[t.length];
			for(int i=0; i<t.length; i++) {
				num[i] = Integer.parseInt(t[i]);
			}
			count = Integer.parseInt(s[1]);
			tem = new int[count];
			index = new int[count];
			ii = 0;
			max =0;
			ArrayList<Integer> a = new ArrayList<>();
			ArrayList<Integer> ai = new ArrayList<>();
			curr = 0;
			int numi =0;
			for(int i=0; i<count; i++) {
				if(i >= num.length) {
					swap(num.length-1,num.length-2);
					continue;
				}
				getMax(curr);
				if(index[ii-1] == i) {
					ii--;
					i--;
					numi= ii+1;
					curr ++ ;
					continue;
				}
				if(num[numi] < num[index[i]]) {
					swap(index[i], numi);
					numi++;
				}
				System.out.println(Arrays.toString(num));
			}
			
//			if(index[0].length > 1) {
//				Arrays.sort(index, new Comparator<int[]>() {
//					@Override
//					public int compare(int[] o1, int[] o2) {
//						return o1[1]-o2[1];
//					}
//				});
//			}
			//System.out.println(Arrays.toString(index));
			for(int i=0; i<index.length; i++) {
				if(index[i]==0) continue;
				if(!a.contains(index[i])) {
					a.add(index[i]);
					ai.add(num[index[i]]);
				}
			}
			
			Collections.sort(a);
			Collections.sort(ai,new Comparator<Integer>() {
				@Override
				public int compare(Integer o1, Integer o2) {
					return o2-o1;
				}
			});
//			System.out.println("a ="+a);
//			System.out.println("ai="+ai);
			for(int i=0; i<a.size(); i++) {
				num[a.get(i)] = ai.get(i);
			}
			System.out.print("#"+tc+" ");
			for(int i=0; i<num.length; i++) {
				System.out.print(num[i]);
			}
			System.out.println();
		}
		br.close();
	}
	public static void getMax(int ind) {
		max =0;
		for(int i=ind; i<num.length; i++) {
			if(max <= num[i]) {
				max = num[i];
				index[ii] = i;
			}
		}
		curr++;
		ii++;
	}
}