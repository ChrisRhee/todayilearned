package D3;
import java.io.*;
import java.util.Scanner;
class Solution5215_햄버거다이어트_서울10반_이찬호_재귀{
	public static int[][] map;
	public static int n,L, max,count;
	public static boolean[] visit;
	
	private static void dfs(int start, int mat, int cal) {
		if(cal > L) return;
		if(max < mat) {
			max = mat;
		}
		for(int i=start ; i<n; i++) {
			if(visit[i]== true) continue;
			if(map[i][1]+cal < L) {
			visit[i] = true;
			dfs(i+1, map[i][0]+mat, map[i][1]+cal);
			visit[i] = false;
			}
		}
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input5215.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		for(int tc = 1; tc <= T; tc++){
			n= sc.nextInt();
			L= sc.nextInt();
			map = new int[n][2];
			visit = new boolean[n];
			count = 0;
			max = 0;
			for(int i =0; i<n; i++) {
				map[i][0] = sc.nextInt();
				map[i][1] = sc.nextInt();
			}
			
			dfs(0,0,0);
			
			System.out.println("#"+tc + " "+max);
		}
	}


	
}