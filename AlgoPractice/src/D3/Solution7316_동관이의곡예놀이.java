package D3;
import java.io.*;
import java.util.Scanner;
class Solution7316_동관이의곡예놀이{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input7316.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		
		for(int tc = 1; tc <= T; tc++){
			long Ans =0;
			long N= sc.nextInt();
			long curr = sc.nextInt();
			long index = 0;
			for(long i=1; i<N; i++) {
				int t = sc.nextInt();
				if(curr <= t) {
					if(Ans < i-index) Ans = i-index;
					curr = t;
					index = i;
				}
			}
			
			System.out.println("#"+tc + " "+Ans);
		}
	}
}