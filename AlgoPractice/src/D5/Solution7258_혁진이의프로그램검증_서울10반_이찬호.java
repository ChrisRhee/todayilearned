package D5;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 *  - 
 * 	- 문자명령
 *  - < 이동 방향을 왼쪽으로 바꾼다.
 *  - >	이동 방향을 오른쪽으로 바꾼다.
 *  - ^	이동 방향을 위쪽으로 바꾼다.
 *  - v	이동 방향을 아래쪽으로 바꾼다.
 *  - _	메모리에 0이 저장되어 있으면 이동 방향을 오른쪽으로 바꾸고, 아니면 왼쪽으로 바꾼다.
 *  - |	메모리에 0이 저장되어 있으면 이동 방향을 아래쪽으로 바꾸고, 아니면 위쪽으로 바꾼다.
 *  - ?	이동 방향을 상하좌우 중 하나로 무작위로 바꾼다. 바꾸는 확률은 네 방향 모두 같다.
 *  - .	아무것도 하지 않는다.
 *  - @	프로그램의 실행을 정지한다.
 *  - 0~9	메모리에 문자가 나타내는 값을 저장한다.
 *  - +	메모리에 저장된 값에 1을 더한다. 만약 +1 하기 전 값이 15이라면 0으로 바꾼다.
 *  - -	메모리에 저장된 값에 1을 뺀다. 만약 -1 하기 전 값이 0이라면 15로 바꾼다.
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution7258_혁진이의프로그램검증_서울10반_이찬호{
	public static int[] di= {-1,1,0,0};
	public static int[] dj= {0,0,-1,1};
	public static int mem,N,M,currI,currJ;
	public static String[][] map;
	public static String command;
	public static int[][] visit;
	public static boolean notEnd,flag;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input7258.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			String s[] = br.readLine().split(" ");
			N = Integer.parseInt(s[0]);
			M = Integer.parseInt(s[1]);
			map = new String[N][M];
			mem = 0;
			visit = new int[N][M];
			
			notEnd = true;
			
			for(int i=0; i<N; i++) {
				s= br.readLine().split("");
				for (int j=0; j<M; j++) {
					if(s[j].equals("@")) notEnd = false; 
					map[i][j] = s[j];
				}
			}
//			for (String[] is : map) {
//				System.out.println(Arrays.toString(is));
//			}

			flag = false;
			//처음 방향은 오른쪽
			currI = di[3];
			currJ = dj[3];
			
			//System.out.println(Arrays.toString(counts));
			if(bfs(0,0)) System.out.println("#"+tc + " YES"); 
			else System.out.println("#"+tc + " NO");
		}
		br.close();
	}
	
	public static boolean bfs(int i, int j) {
		command = map[i][j];
		visit[i][j]++;
		if(visit[i][j] > 17) {
			return false;
		}
		
		//명령어 수행
		switch(command) {
			case "^": // 0
				currI= di[0];
				currJ= dj[0];
				break;
			case "v": // 1
				currI= di[1];
				currJ= dj[1];
				break;
			case "<": //2
				//이동 방향을 왼쪽으로 바꾼다.
				currI= di[2];
				currJ= dj[2];
				break;
			case ">": //3
			    //이동 방향을 오른쪽으로 바꾼다.
				currI= di[3];
				currJ= dj[3];
				break;
			case "_": //메모리에 0이 저장되어 있으면 이동 방향을 오른쪽으로 바꾸고, 아니면 왼쪽으로 바꾼다.
				if(mem == 0) {
					currI= di[3];
					currJ= dj[3];
				}else {
					currI= di[2];
					currJ= dj[2];
				}
				break;
			case "|": //메모리에 0이 저장되어 있으면 이동 방향을 아래쪽으로 바꾸고, 아니면 위쪽으로 바꾼다.
				if(mem == 0) {
					currI= di[1];
					currJ= dj[1];
				}else {
					currI= di[0];
					currJ= dj[0];
				}
				break;
			case "?":{ //이동 방향을 상하좌우 중 하나로 무작위로 바꾼다. 바꾸는 확률은 네 방향 모두 같다.
				for(int d=0; d<4; d++) {
					currI = di[d];
					currJ = dj[d];
					i += currI;
					j += currJ;
					//이동
					if(i >= N) i = 0;
					if(i < 0 ) i = N-1;
					if(j >= M) j =0;
					if(j < 0) j = M-1;
					if(visit[i][j] > 0 ) continue;
					int[][] tempv = new int [N][M];
					for(int ii=0; ii< N; ii++) {
						for(int jj=0; jj< M; jj++) {
							tempv[ii][jj] = visit[ii][jj];
						}
					}
					boolean tempcheck = false;
					tempcheck = bfs(i,j);
					for(int ii=0; ii< N; ii++) {
						for(int jj=0; jj< M; jj++) {
							visit[ii][jj] = tempv[ii][jj];
						}
					}
					if(tempcheck)
						return true;
					else
						return false;
				}
				break;
			}
			case ".": //아무것도 하지 않는다.
				break;
			case "@"://프로그램의 실행을 정지한다.
				return true;
			case "0":
			case "1":
			case "2":
			case "3":
			case "4":
			case "5":
			case "6":
			case "7":
			case "8":
			case "9": //메모리에 문자가 나타내는 값을 저장한다.
				mem = Integer.parseInt(command);
				break;
			case "+": //메모리에 저장된 값에 1을 더한다. 만약 +1 하기 전 값이 15이라면 0으로 바꾼다.
				mem++;
				if(mem == 16) mem = 0;
				break;
			case "-": //메모리에 저장된 값에 1을 뺀다. 만약 -1 하기 전 값이 0이라면 15로 바꾼다.
				mem--;
				if(mem < 0) mem = 15;
				break;
		}
		
		i += currI;
		j += currJ;
		//이동
		if(i >= N) i = 0;
		if(i < 0 ) i = N-1;
		if(j >= M) j =0;
		if(j < 0) j = M-1;
		return bfs(i,j);
	}
}