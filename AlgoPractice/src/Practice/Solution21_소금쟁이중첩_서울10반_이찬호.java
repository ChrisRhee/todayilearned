package Practice;
import java.io.*;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 앞에서 뛰었던 영역을 밟은 소금쟁이를 구해라.
 * 조건
 * 	- 소금쟁이의 처음 위치와 방향이 주어짐 방향은 1:하, 2:우
 *  - 소금쟁이는 1번부터 번호가 주어짐.
 * 	- 소금쟁이는 3번씩 뛴다. 3칸,2칸,1칸
 *  - 뛰는 도중 연목 밖으로 나가면 그 소금쟁이는 stop
 *  - 어떤 소금쟁이의 시작 위치가 이미 뛰었던 자리면 그 소금쟁이 번호를 출력. -> 놓는순간 밟는거니까
 *  - 같은자리를 뛴 소금쟁이가 하나도 없으면 0 출력
 * 입력
 *  - 테스트 케이스 T
 *  - 배열의 크기 N, 소금쟁이 수 
 *  - 각 소금쟁이의 시작위치 행, 열, 방향 이 소금쟁이 수 만큼 주어짐
 * 출력
 * 	- 이전에 밟은 영역을 또 밟은 소금쟁이의 번호 출력 
 * 풀이 
 * 	- 소금쟁이 입력을 받을 때마다 점프를 하게하고, 밟는 위치에 표시 해두자.
 * 	- 표시된 땅에 점프하게 되면 그 소금쟁이 번호 출력
 * */
class Solution21_소금쟁이중첩_서울10반_이찬호 {
	public static int[] di = {0,  1, 0};
	public static int[] dj = {0,  0, 1};
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input21.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T =Integer.parseInt(br.readLine());
		for(int tc = 1; tc<=T; tc++) {
			int Ans = 0;
			String[] s = br.readLine().split(" ");
			int N = Integer.parseInt(s[0]);
			int solts = Integer.parseInt(s[1]);
			int map[][] = new int[N][N];
			// 소금쟁이 수만큼
			loop:for (int i = 0; i < solts; i++) {
				s = br.readLine().split(" ");
				int si = Integer.parseInt(s[0]);
				int sj = Integer.parseInt(s[1]);
				int sd = Integer.parseInt(s[2]);
				// 시작위치에 누가 점프 했었으면 바로 끗
				map[si][sj] = -(i+1);
				if(map[si][sj] == 1) {
					Ans = i+1;
					break;
				}
				int ii = si;
				int jj = sj;
				// 3,2,1을 방향으로 점프
				for(int j= 3; j >=1; j--) {
					ii = ii + j*di[sd];
					jj = jj + j*dj[sd];
					// 연못 밖으로 뛰면 멈춰
					if( ii < 0 || ii >= N || jj < 0 || jj >= N ) break;
					// 뛴 위치에 1이 있으면 끗 
					if(map[ii][jj] ==1 ) {
						Ans = i+1;
						continue loop;
					}
					map[ii][jj] = 1;
				}
			}
			
			System.out.println("#"+tc+" "+Ans);
		}
	}
}
