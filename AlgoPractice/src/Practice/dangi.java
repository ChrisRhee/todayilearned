package Practice;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

public class dangi {
	public static boolean[][] visit;
	public static int N;
	public static Stack<int[]> s;
	public static String map[][] ;
	public static int[] di = {-1,1,0,0};
	public static int[] dj = {0,0,-1,1};
	public static ArrayList<Integer> ans;
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/단지.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		N = Integer.parseInt(br.readLine());
		s = new Stack<>();
		map = new String[N][N];
		visit = new boolean[N][N];
		ans = new ArrayList<>();
		for(int i=0; i<N; i++) {
			String[] s = br.readLine().split("");
			for(int j=0; j<N; j++) {
				map[i][j] = s[j];
			}
		}
		
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				if(map[i][j].equals("1") && visit[i][j] == false)
					dfs(i,j);
			}
		}
		
		Collections.sort(ans);
		
		System.out.println(ans.size()+"\n"+ans);
	}
	
	public static void dfs(int i, int j) {
		int count = 1;
		
		s.push(new int[] {i,j});
		visit[i][j] = true;
		while(!s.isEmpty()) {
			int[] curr = s.pop();
			for(int d=0; d<4; d++) {
				int ii= curr[0] + di[d];
				int jj= curr[1] + dj[d];
				if(ii < 0 || ii>= N || jj < 0 || jj>= N ) continue;
				if(visit[ii][jj] == false && map[ii][jj].equals("1")) {
					count++;
					visit[ii][jj] = true;
					s.push(new int[] {ii,jj});
				}
			}
		}
		ans.add(count);
	}
}
