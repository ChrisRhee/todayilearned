package Practice;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
class Solution4311_오래된스마트폰{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input4311.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++)
		{
			int Ans =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			// N = 터치 가능한 숫자 개수
			// O = 연산자 개수
			// M = 최대 터치 가능한 개수
			String NOM[] = br.readLine().split(" ");
			String num[] = br.readLine().split(" ");
			String opr[] = br.readLine().split(" ");
			int n[] = new int[num.length];
			int o[] = new int[opr.length];
			int m = Integer.parseInt(NOM[2]);
			int finalNum = Integer.parseInt(br.readLine());
			for(int i =0; i<n.length; i++) {
				n[i] = Integer.parseInt(num[i]);
			}
			for(int i =0; i<o.length; i++) {
				o[i] = Integer.parseInt(opr[i]);
			}
			
			System.out.println(m);
			
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
}