package Practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class programers_1 {
	public static int[] d ;
	public static int[] t = new int[3];
	public static int n ;
	public static int r = t.length;
	public static int cnt;
	
	
	private static void comb(int n, int r, int a) {
		if(r==0) {
			if(t[0]+t[1]+t[2] > a) return;
			if(t[0]+t[1]+t[2] == a) {
				cnt++;
				System.out.println(Arrays.toString(t));
			}
			return;
		}
		
		if(n<r) return;
		
		t[r-1] = d[n-1];
		comb(n-1, r-1,a);
		comb(n-1, r,a);
	}
	
	private static boolean isPrime(int n) {
		int i=0;
		int last = n/2;
		if( n<=1) return false;
		for(i =2; i <=last; i++) {
			if((n%i)==0) return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		ArrayList<Integer> ss = new ArrayList<>();
		for(int i=0; i <=a; i++) {
			if(isPrime(i)) {
				ss.add(i);
			}
		}
		d= new int[ss.size()];
		n = d.length;
		for(int i=0; i<ss.size(); i++) {
			d[i] = ss.get(i);
		}
		comb(n,r,a);
		System.out.println(cnt);
	}
	
}
