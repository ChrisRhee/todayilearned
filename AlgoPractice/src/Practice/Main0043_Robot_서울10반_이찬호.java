package Practice;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Stack;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 로봇들이 갈 수 있는 거리를 구하라
 * 조건
 * 	- A는 오른쪽, B는 좌우, C는 상하좌우로 이동 가능하다.
 *  - 벽을 만나거나 다른로봇의 초기 위치는 가지 못한다.
 *  - 이동위치는 0부터 시작한다.
 *  - N은 자연수 3~20의 값이다.
 * 입력
 *  - 첫 줄에는 테스트 케이스  T가 주어진다.
 *  - 다음 줄에는 배열의 크기인 N이 주어진다.
 *  - 각 배열의 값 (S는 공백, W는 벽, A,B,C, 로봇) 이 배열의 개수만큼 입력 
 * 출력
 * 	- 모든 로봇의 이동거리합을 출력
 * 풀이 
 * 	- 초기 위치도 벽처럼 인식하게 만들고 움직이게 한다.
 *  - 각 로봇마다 DFS로 움직이는 최대 거리를 구하면 될 듯
 * */
class Main0043_Robot_서울10반_이찬호{
	public static int[] di= {-1,1,0,0};
	public static int[] dj= {0,0,-1,1};
	public static Stack<int[]> swid;
	public static Stack<int[]> shei;
	public static boolean[][] v;
	public static int ans, N;
	public static String[][] map;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input20.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			ans =0;
			swid = new Stack<>();
			shei = new Stack<>();
			N= Integer.parseInt(br.readLine());
			map = new String[N][N];
			
			for(int i =0; i<N; i++) {
				String s[] = br.readLine().split(" ");
				for(int j =0; j<N; j++) {
					map[i][j] = s[j];
				}
			}
			
			for(int i =0; i<N; i++) {
				for(int j =0; j<N; j++) {
					if(map[i][j].equals("A") || map[i][j].equals("B")) {
						DFS(i,j);
					}
					else if(map[i][j].equals("C")) {
						DFS(i,j);
						DFSUP(i,j);
					}
				}
			}
			
			System.out.println("#"+tc + " "+ans);
		}
		br.close();
	}

	private static void DFSUP(int i, int j) {
		v = new boolean[N][N];
		shei.push(new int[] {i,j});
		v[i][j] = true;
		while(!shei.isEmpty()) {
			int[] curr = shei.pop();
			for(int d=0; d<2;d++) {
				int ii= curr[0] + di[d];
				int jj= curr[1] + dj[d];
				if(ii < 0 || ii >= N || jj < 0 || jj >= N) continue;
				if(v[ii][jj] == false && map[ii][jj].equals("S")) {
					shei.push(new int[] {ii,jj});
					v[ii][jj] = true;
					ans++;
				}
			}
		}
	}

	private static void DFS(int i, int j) {
		v = new boolean[N][N];
		swid.push(new int[] {i,j});
		v[i][j] = true;
		while(!swid.isEmpty()) {
			int[] curr = swid.pop();
			for(int d=2; d<4;d++) {
				if(map[i][j].equals("A") && d ==2 ) continue;
				int ii= curr[0] + di[d];
				int jj= curr[1] + dj[d];
				if(ii < 0 || ii >= N || jj < 0 || jj >= N) continue;
				if(v[ii][jj] == false && map[ii][jj].equals("S")) {
					swid.push(new int[] {ii,jj});
					v[ii][jj] = true;
					ans++;
				}
			}
		}
	}
}