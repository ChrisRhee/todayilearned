package Practice;

import java.util.Arrays;

public class programers_3 {
	public static void main(String[] args) {
		int health1[] = {200,120,150};
						//공격력, 낮추는 체력
		int items1[][] = { {30,100}, {500,30}, {100,400} };
		int health2[] = {300,200,500};
		int items2[][] = { {1000,600}, {400,500}, {300,100} };
		
		// 체력이 100미만으로 내려가면 사용 불가
		// health1[1] - items1[1][1] = 20 < 100 이므로 불가능. 
		for(int i=0; i<health1.length; i++) {
			for(int j=0; j<items1.length; j++) {
				// 뺐는데 체력보다 높으면 사용해야지
				if(health1[i] - items1[j][1] >= 100) {
					System.out.println(i+" 에게 "+ j+" 사용가능");
				}
			}
		}
		
		System.out.println();
		for(int i=0; i<health2.length; i++) {
			for(int j=0; j<items2.length; j++) {
				// 뺐는데 체력보다 높으면 사용해야지
				if(health2[i] - items2[j][1] >= 100) {
					System.out.println(i+" 에게 "+ j+" 사용가능");
				}
			}
		}
	}
}
