package Practice;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
class Solution1249_보급로_BFS{
	public static Queue<int[]> q;
	public static int v[][],m[][] ;
	public static int N;
	public static int di[] ={-1,1,0,0};
	public static int dj[] ={0,0,-1,1};
	public static int min;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1249.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			N= Integer.parseInt(br.readLine());
			m = new int [N][N];
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			q = new LinkedList<>();
			min = Integer.MAX_VALUE;
			
			for(int i =0; i<N; i++) {
				String r[] = br.readLine().split("");
				for(int j=0; j<N; j++) {
					m[i][j] = Integer.parseInt(r[j]);
				}
			}
			
			bfs(0,0);
//			for (int[] vv : v) {
//				System.out.println(Arrays.toString(vv));
//			}
			
			System.out.println("#"+tc + " "+v[N-1][N-1]);
		}
		
		br.close();
	}
	
	public static void bfs(int i, int j) {
		v = new int [N][N];
		q.offer(new int[]{i,j});
		v[i][j] = m[i][j];
		//m[i][j] = -1;
		while(!q.isEmpty()) {
			int curr[] = q.poll();
			//핫 좌 만 가 
			for(int d =0; d<4; d++) {
				int ii = curr[0]+di[d];
				int jj = curr[1]+dj[d];
				if(ii < 0 || ii >= N || jj < 0 || jj >= N ) {
					continue;
				}
				if(ii ==0 && jj ==0) continue;
				
				// 방문 안한 곳이면
				if(v[ii][jj] > v[curr[0]][curr[1]]+m[ii][jj] || v[ii][jj] == 0 ) {// v[curr[0]][curr[1]]) {
					
					//visit에 거리 추가
					v[ii][jj] = v[curr[0]][curr[1]]+m[ii][jj];
					//m[ii][jj] = -1;
					q.offer(new int[] {ii,jj});
				}
				
//				for (int[] vv : v) {
//					System.out.println(Arrays.toString(vv));
//				}
//				System.out.println();
				
				
			}
		}
		
	}
}