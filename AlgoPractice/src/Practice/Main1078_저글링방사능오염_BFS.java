package Practice;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

class Main1078_저글링방사능오염_BFS{
	public static Queue<int[]> q;
	public static int di[] = {-1,1, 0,0};
	public static int dj[] = {0 ,0,-1,1};
	public static int[][] v;
	public static int I,J;
	public static char[][] m;
	public static int max;
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1078.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int rest = 0;
		max =1;
		q = new LinkedList<int[]>();
		String s[] = br.readLine().split(" ");
		I = Integer.parseInt(s[1]);
		J = Integer.parseInt(s[0]);
		
		m = new char[I][J];
		for(int i=0; i<I; i++) {
			String ss = br.readLine();
			for(int j=0; j<J; j++) {
				m[i][j]= ss.charAt(j);
			}
		}
		
		String s2[] = br.readLine().split(" ");
		bfs(Integer.parseInt(s2[1])-1,Integer.parseInt(s2[0])-1);
		
		for(int i=0; i<I; i++) {
			for(int j=0; j<J; j++) {
				if(m[i][j]== '1') rest++;
			}
		}
		
		
		System.out.println(max+2);
		System.out.println(rest);
		br.close();
	}
	
	public static void bfs(int i, int j) {
		v = new int[I][J];
		q.offer(new int[] {i,j});
		v[i][j] = 1;
		m[i][j] = '0';
		while(!q.isEmpty()) {   // isEmpty()
			int [] curr = q.poll(); // dequeue()
			for(int d=0; d<4; d++) {
				int ii = curr[0] + di[d];
				int jj = curr[1] + dj[d];
				if( ii < 0 || ii >= I || jj < 0 || jj >= J) {
					continue;
				}
				if(m[ii][jj] == '1' && v[ii][jj] == 0) {
					v[ii][jj] = v[curr[0]][curr[1]]+1;
					if(v[ii][jj]>max) max = v[ii][jj];
					q.offer(new int[] {ii,jj});
					m[ii][jj] = '0';
				}
			}
		}
	}
}