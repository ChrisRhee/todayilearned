package Practice;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
class Solution3431_준환이의운동관리{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3431.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			String r[] = br.readLine().split(" ");
			int num[] = new int[r.length];
			for(int i =0; i<r.length; i++) {
				num[i] = Integer.parseInt(r[i]);
			}
			if(num[0] > num[2]) Ans = num[0]-num[2];
			else if(num[2] > num[1]) Ans = -1;
			
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
}