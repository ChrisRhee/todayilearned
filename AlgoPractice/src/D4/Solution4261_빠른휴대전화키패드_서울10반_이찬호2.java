package D4;

import java.io.*;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution4261_빠른휴대전화키패드_서울10반_이찬호2{
	public static String keypad[][] = { {}, {},
										{"a","b","c"}, //2
										{"d","e","f"}, //3
										{"g","h","i"}, //4
										{"j","k","l"}, //5
										{"m","n","o"}, //6
										{"p","q","r","s"},
										{"t","u","v"},
										{"w","x","y","z"}};
	public static int Ans ;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input4261.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			Ans =0;
			String s[] = br.readLine().split(" ");
			String key = s[0];
			int N = Integer.parseInt(s[1]);
			String word[] = br.readLine().split(" ");
			for(int i=0; i<N; i++) 	solve(key, word[i]);
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
	private static void solve(String key, String string) {
		String t[] = key.split("");
		String s[] = string.split("");
		if(t.length != s.length) return;
		for(int j=0; j<s.length; j++) {
			boolean check = false;
			for(int i=0 ; i<keypad[Integer.parseInt(t[j])].length; i++) {
				if(keypad[Integer.parseInt(t[j])][i].equals(s[j])) {
					check = true;
					break;
				}
			}
			if(!check) return;
		}
		Ans ++;
	}
}