package D4;
import java.io.*;
import java.util.*;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 주어진 숫자를 최소 횟수를 눌러 구하자
 * 조건
 * 	- 곱하기와, 계산버튼, 몇개의 숫자만 가능
 * 입력
 *  - 테스트 케이스 T
 *  - 열개의 정수 i번째 정수는 계산기에서 i-1를 누를 수 잇는 버튼 상태 1이면 누름 0이면 못누름
 *  - 하나의 정수
 * 출력
 * 	- 최소 몇번을 눌러야 X를 계산할 수 있는지 출력
 * 풀이 
 * 	- 주어진 수의 약수를 다 구한다. 
 *  - check함수를 만들어서 지금 주어진 수가 누를 수 있는 숫자로 만들 수 있는 수인지 확인한다.
 *  - 약수중 가장 큰 수로 나누고 나머지를 주어진 계산기로 누를 수 있는지 확인 해본다.
 *  - 더이상 나눌게 없을때까지 실행해본다. 
 * */
class Solution1808_지희의고장난계산기_서울10반_이찬호{
	public static int[] calc, numSplit;
	public static ArrayList<Integer> yaks;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1808.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			calc = new int[10];
			yaks = new ArrayList<>();
			for(int i=0; i<10; i++) calc[i] = sc.nextInt();
			int num = sc.nextInt();
			setNumSplit(num);
			getYakNum(num);
			for(int i=0; i<yaks.size(); i++) setNumSplit(yaks.get(i));
			
			Collections.sort(yaks,  new Comparator<Integer>() {
				@Override
				public int compare(Integer o1, Integer o2) {
					return o2-o1;
				}
			});
			
			boolean flag = false;
			while(true) {
				flag = false;
				setNumSplit(num);
				if(check()) {
					flag = true;
					Ans += numSplit[10];
					Ans += 1;
					break;
				}
				if(num == 1) {
					flag = true;
					break;
				}
				for(int i=0; i<yaks.size(); i++) {
					if(num% yaks.get(i)==0) {
						flag = true;
						num /= yaks.get(i);
						setNumSplit(yaks.get(i));
						Ans+= numSplit[10];
						Ans+= 1;
						break;
					}
				}
				
				if(flag == false) break;
			}
			if(flag) System.out.println("#"+tc+" "+Ans);
			else System.out.println("#"+tc+" "+(-1));
		}
	}
	
	public static boolean check() {
		boolean flag = true;
		for(int i=0; i<10; i++) {
			if(calc[i] == 0 && numSplit[i] > 0) {
				flag = false;
				break;
			}
		}
		return flag;
	}
	
	public static void setNumSplit(int num) {
		numSplit = new int[11];
		int temp = num;
		int res = 0;
		if(num == 0) {
			numSplit[num]++;
			numSplit[10]++;
			return;
		}
		while(true) {
			if(temp == 0) break;
			res = temp%10;
			temp /= 10;
			numSplit[res]++;
			numSplit[10]++;
		}
	}
	
	public static void getYakNum(int num) {
		for(int i=2; i<= num; i++) {
			if(num%i == 0) {
				setNumSplit(i);
				if(check())
					yaks.add(i);
			}
		}
	}
}