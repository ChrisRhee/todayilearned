package D4;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 은비가 청소 하러 올라갈 층을 구하자
 * 조건
 * 	- 4가 들어가는 층은 그냥 패스한다.
 *  - 0층도 그냥 패스한다.
 * 입력
 *  - 테스트케이스 T
 *  - A,B (-10^12 < A < B < 10^12)
 *  - A,B를 이루는 숫자에는 4가 들어있지 않다.
 * 출력
 * 	- 은비가 올라가야 하는 층수
 * 풀이 
 * 	- 4가 빠지고 남은 층은 10^n 일때, 9^n 층씩 남는다.
 *  - 10층일 때 9개, 100층일 때 81개, 1000층일 때 729개
 *  - 그래서 맨 끝자리부터 한 숫자(res)씩 가져오면서 power 0부터 같이 증가시켜가면서 unit을 해당 숫자를 res만큼 곱해준다.
 *  - 곱해줄때, 4인 경우는 pass해서 곱해주면 res에 해당하는 층 값을 가져 올 수 있다.
 *  - 이렇게 매 10단위 층마다 구해서 전체를 구해준다.
 *  
 *  출력시 주의 할 것
 *  - 처음 시작점이 지하에서 시작하는 경우도 있지만, 둘다 양수일 때, 둘다 음수일때, 하나만 음수일때 층수를 다르게 해주어야한다. 
 * */
class Solution4530_극한의청소작업_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input4530.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		for(int tc = 1; tc <= T; tc++){
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			long Ans = 0;
			long A = 0;
			long B = 0;
			long start = sc.nextLong(); 
			long end = sc.nextLong();
			long res = 0;
			int po = 0;
			long tema = start;
			long temb = end;
			if(start < 0) start*= -1;
			po = 0;
			while(true) {
				if(start == 0) break;
				res = start%10;
				A += getLayer(res, po);
				po++;
				start /= 10;
			}
			if(end < 0) end*= -1;
			po = 0;
			while(true) {
				if(end == 0) break;
				res = end%10;
				B += getLayer(res, po);
				po++;
				end /= 10;
			}
			
			if (tema >= 0 && temb >= 0)
				Ans = B - A;
			else if (tema <= 0 && temb <= 0)
				Ans = A - B;
			else if (tema <= 0 && temb >= 0)
				Ans = A + B - 1;
			
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}

	private static long getLayer(long res, int po) {
		long temp = 0;
		long unit = getPower(po);
		for(int i=0; i<=res; i++) {
			if(i==4 || i==0 ) continue;
			temp += unit;
		}
		return temp;
	}
	private static long getPower(int power) {
		long temp = 1;
		if(power == 0) temp = 1;
		else 
			for(int i=1; i<=power; i++)	temp *= 9;
		return temp;
	}
}