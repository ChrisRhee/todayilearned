package D4;
import java.io.*;
import java.util.LinkedList;
import java.util.Queue;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution7250_탈출_서울10반_이찬호{
	public static Queue<int[]> q;
	public static int di[] = {-1,1,0,0};
	public static int dj[] = {0,0,-1,1};
	public static char m[][];
	public static int visit[][], visitV[][],lifeS[][];
	public static int I,J,W,ans,ansV;
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input7250.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
		int T = Integer.parseInt(br.readLine());
		for(int tc =1; tc<=T; tc++) {
			String s[] = br.readLine().split(" ");
			ans = 0;
			ansV = 0;
			q = new LinkedList<>();
			I = Integer.parseInt(s[0]);
			J = Integer.parseInt(s[1]);
			W = Integer.parseInt(s[2]);
			m = new char[I][J];
			visit = new int[I][J];
			lifeS = new int[I][J];
			visitV = new int[I][J];
			
			for(int i=0; i<I; i++) {
				String ss = br.readLine();
				for(int j=0; j<J; j++) {
					m[i][j] = ss.charAt(j);
				}
			}
			
			// 사람먼저 큐에 넣기
			for(int i=0; i<I; i++) {
				for(int j=0; j<J; j++) {
					if(m[i][j]=='S') {
						q.offer(new int[] {i,j,W});
						lifeS[i][j] = W;
						visit[i][j] = 1;
					}
				}
			}
			
			// 불 넣기
			for(int i=0; i<I; i++) {
				for(int j=0; j<J; j++) {
					if(m[i][j]=='F') {
						q.offer(new int[] {i,j,-1});
					}
				}
			}
			
			//악당 넣기
			for(int i=0; i<I; i++) {
				for(int j=0; j<J; j++) {
					if(m[i][j]=='V') {
						q.offer(new int[] {i,j,-1});
						visitV[i][j] = 1;
					}
				}
			}
			// 큐 ㄱㄱ
			bfs ();
			
			if(ans >= ansV) {
				System.out.println(ans-1);
			}else {
				System.out.println(-1);
			}
		}
		br.close();
	}
	
	
	public static void bfs() {
		int life = 0;
		char currc = ' ';
		while(!q.isEmpty()) {   // isEmpty()
			int [] curr = q.poll(); // dequeue()
			for(int d=0; d<4; d++) {
				int ii = curr[0] + di[d];
				int jj = curr[1] + dj[d];
				if( ii < 0 || ii >= I || jj < 0 || jj >= J) {
					continue;
				}
				life = curr[2];
				currc = m[curr[0]][curr[1]];
				// 불이면 다음칸에 불 붙임
				if(m[curr[0]][curr[1]] == 'F' && (m[ii][jj] == 'A'|| m[ii][jj] == 'S' || m[ii][jj] == 'V')) {
					m[ii][jj]= 'F';
					q.offer(new int[] {ii,jj,-1});
				}
				// 사람이면 땅일때만 이동
				else if( ( visit[curr[0]][curr[1]] > 0 && visit[ii][jj] < life ) || lifeS[ii][jj] < life || (m[ii][jj] == 'A'||m[ii][jj] == 'W' ||m[ii][jj] == 'E' ||m[ii][jj] == 'V')  ) {
					// 벽일때 체크
					if(life < lifeS[curr[0]][curr[1]])
						life = lifeS[curr[0]][curr[1]];
					if(m[ii][jj] == 'W') {
						if(life == 0) continue;
						life--;
					}
					// visit 추가
					lifeS[ii][jj] = life;
					visit[ii][jj] = visit[curr[0]][curr[1]] +1;
					if(m[ii][jj] =='E') { 
						ans = visit[ii][jj]; 
						return;
					}
					
					if(m[ii][jj] == 'A') life = W;
					if(m[ii][jj] != 'W') {
						m[curr[0]][curr[1]]= currc;
						m[ii][jj]= 'S';
					}
					
					
					q.offer(new int[] {ii,jj,life});
				}
				else if(visitV[curr[0]][curr[1]] > 0 && visitV[ii][jj] == 0 && (m[ii][jj] == 'A'||m[ii][jj] == 'S' || m[ii][jj] == 'F' || m[ii][jj] == 'E')) {
					visitV[ii][jj] = visitV[curr[0]][curr[1]] +1;
					//악당이 먼저 도착
					if(m[ii][jj] =='E') { 
						ansV = visitV[ii][jj]; 
					}
					//m[ii][jj]= 'V';
					q.offer(new int[] {ii,jj,-1});
				}
			}
		}
	}
}