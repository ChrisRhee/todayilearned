package D4;
import java.io.*;
import java.util.Scanner;
class Solution4796_의석이의우뚝선산_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input4796.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			int N= sc.nextInt();
			int up = 0;
			int down = 0;
			boolean upcheck = false;
			boolean downcheck = false;
			int map[] = new int[N];
			for(int i =0; i<N; i++) map[i] = sc.nextInt();
			for(int i =0; i<N-1; i++) {
				if(map[i]-map[i+1] < 0) {
					if(upcheck && downcheck) {
						Ans += up * down;
						upcheck = false;
						up = 0;
					}
					down = 0;
					downcheck = false;
					upcheck = true;
					up++;
				}else {
					downcheck = true;
					down++;
				}
			}
			if(upcheck && downcheck) Ans += up * down;
			System.out.println("#"+tc + " "+Ans);
		}
	}
}