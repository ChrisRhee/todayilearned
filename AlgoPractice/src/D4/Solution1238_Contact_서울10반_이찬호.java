package D4;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 가장 나중에 연락받게되는 사람 중 번호가 가장 큰 사람 구하기
 * 조건
 * 	- 같이 연결된 곳이면 한번에 연락 가능하다.
 *  - 한번 연락 한 사람은 다시 연락 안한다.
 *  - 마지막까지 연락 됐을 때 가장 큰 사람들을 구한다.
 * 입력
 *  - 데이터의 길이와 시작점
 *  - from to..순서대로 주어진다. 
 *  - 여러번 같은게 입력되도 상관 없음
 * 출력
 * 	- 마지막에 연락 됐을 때 가장 큰 사람 수 구하기  
 * 풀이 
 * 	- 2차원 배열 NxN을 만들고 그 사람 위치일 때 갈수있는 곳들을 회사 수로 초기화 해준다.
 *  - 0일때만 방문하도록 해서 2차원 배열을 만들어보자 
 * */
class Solution1238_Contact_서울10반_이찬호{
	public static int[][] map;
	public static int N,max,maxNode,startss;
	public static ArrayList<Integer> a;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1238.txt"));
		//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		
		for(int tc = 1; tc <= 10; tc++){
			N= sc.nextInt();
			map = new int [101][101];
			maxNode = 0;
			max = 0;
			a = new ArrayList<>();
			
			int Start = sc.nextInt();
			for(int i=0; i<N/2; i++) {
				int ii = sc.nextInt();
				int jj = sc.nextInt();
				map[ii][jj] = -1;
			}
			int count = 0;
//			for (int[] is : map) {
//				System.out.println(Arrays.toString(is));
//			}
			a.add(Start);
			tamsac(Start, count);
			
			int Ans =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			for(int i=1; i<=100; i++) {
				for(int j=1; j<=100; j++) {
					//if(map[i][j] > 0) System.out.println(map[i][j]+" "+max);
					if(map[i][j]==(max-1)) {
						//System.out.print(j+" ");
						if(maxNode < j) maxNode =j;
					}
				}
			} 
//			System.out.println();
//			for (int[] is : map) {
//				System.out.println(Arrays.toString(is));
//			}
//			System.out.println();
			System.out.println("#"+tc + " "+maxNode);
		}
		sc.close();
	}

	private static void tamsac(int start, int count) {
		count++;
		if(count > max) max = count;
		//System.out.println(start+" "+max);
		for(int i=1; i<=100; i++) {
			//if(i ==startss) continue;
			if(map[start][i] == -1) {
				if(map[start][i] == map[i][start]) {
					map[i][start] = 0;
				}
				map[start][i] = count;
				tamsac(i,count);
			}
		}
	}
}