package D4;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution3074_입국심사_서울10반_이찬호2{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3074.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			int N= sc.nextInt();
			int M= sc.nextInt();
			int[] sim = new int[N];
			int[] count = new int[N];
			for(int i =0; i<N; i++) {
				sim[i] = sc.nextInt();
			}
			Arrays.sort(sim);
			//1명 들어감
			count[0]++;
			M--;
			int max = sim[0] * count[0];
//			System.out.println(Arrays.toString(sim)+" "+M);
//			System.out.println(Arrays.toString(count));
			// 그 다음부터 ㄱㄱ
			while(true) {
				
				if(M == 0) break;
				for(int i=0; i<N; i++) {
					if(sim[i] > max) break;
					count[i]++;
					M--;
					if(M == 0) break;
				}
				max = sim[0]*count[0];
			}
			
			System.out.println("#"+tc + " "+max);
		}
	}
}