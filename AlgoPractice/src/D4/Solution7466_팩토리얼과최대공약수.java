package D4;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
class Solution7466_팩토리얼과최대공약수{
	private static int ans;
	private static void check(int n, int k, ArrayList<Integer> a, int val, boolean[] visit) {
		if(val > k) {
			return;
		}
		
		
		for(int i=0; i<a.size(); i++) {
			if(a.get(i) > n) break;
			if(visit[i] == true) continue;
			visit[i] = true;
			if( ans < val) ans = val;
			check(n,k,a,val*a.get(i),visit);
			visit[i] = false;
		}
		
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input7466.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		
		for(int tc = 1; tc <= T; tc++){
			ans =0;
			int n= sc.nextInt();
			int k= sc.nextInt();
			ArrayList<Integer> a = new ArrayList<>();
			
			for(int i=1; i<= k; i++) {
				if(k%i==0) a.add(i);
			}
			boolean visit[] = new boolean[a.size()];
			check(n,k,a,1,visit);
			
			//System.out.println(a);
			
			System.out.println("#"+tc + " "+ans);
		}
	}
}