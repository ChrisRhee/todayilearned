package D4;

import java.io.*;
import java.util.Scanner;

class Solution4261_빠른휴대전화키패드_서울10반_이찬호 {
	public static int Ans;

	public static void main(String args[]) throws Exception {
		System.setIn(new FileInputStream("res/input4261.txt"));
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int tc = 1; tc <= T; tc++) {
			Ans = 0;
			boolean check = false;
			char key[] = sc.next().toCharArray();
			int N = sc.nextInt();
			for (int i = 0; i < N; i++) {
				char s[] = sc.next().toCharArray();
				if (key.length != s.length) continue;
				for(int j=0; j<s.length; j++) {
					check = false;
					switch (s[j]) {
					case 'a': case 'b':	case 'c':
						if (key[j] == '2')	check = true;
						break;
					case 'd': case 'e': case 'f':
						if (key[j] == '3')	check = true;
						break;
					case 'g': case 'h':	case 'i':
						if (key[j] == '4')	check = true;
						break;
					case 'j': case 'k': case 'l':
						if (key[j] == '5')	check = true;
						break;
					case 'm': case 'n':	case 'o':
						if (key[j] == '6')	check = true;
						break;
					case 'p': case 'q':	case 'r': case 's':
						if (key[j] == '7')	check = true;
						break;
					case 't': case 'u': case 'v':
						if (key[j] == '8')	check = true;
						break;
					case 'w': case 'x': case 'y': case 'z':
						if (key[j] == '9') check = true;
						break;
					}
					if (!check)	break;
				}
				if (check) 	Ans++;
			}
			System.out.println("#" + tc + " " + Ans);
		} //end for(tc)
	}// end main
}
