package D4;
import java.io.*;
import java.util.Arrays;
class Solution4050_재광이의대량할인_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input4050.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			int N= Integer.parseInt(br.readLine());
			int map[] = new int[N];
			String s[] = br.readLine().split(" ");
			for(int i =0; i<N; i++) map[i] = Integer.parseInt(s[i]);
			int count = 1;
			Arrays.sort(map);
			for(int i= N-1; i>=0; i--) {
				if(count++%3==0) continue;
				Ans+= map[i];
			}
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
}