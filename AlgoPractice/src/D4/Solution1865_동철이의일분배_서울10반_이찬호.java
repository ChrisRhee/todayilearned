package D4;
import java.io.FileInputStream;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution1865_동철이의일분배_서울10반_이찬호{
	public static int N ;
	public static double Ans, max, map[][] ;
	public static boolean[] visit; 

	private static void dfs(int count, double per) {
		if(per <= max) return;
		if(count == N ) {
			if(max< per) max = per;
			return;
		}
		for(int i=0; i<N; i++) {
			if(visit[i] == true) continue;
			visit[i] = true;
			dfs(count+1 , per*map[count][i]);
			visit[i] = false;
		}
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1865.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		for(int tc = 1; tc <= T; tc++){
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			N= sc.nextInt();
			max = 0;
			map= new double[N][N];
			visit = new boolean[N];
			for(int i =0; i<N; i++) 
				for(int j =0; j<N; j++) 
					map[i][j] = sc.nextDouble()/100;
			dfs(0,1);
			System.out.println("#"+tc + " "+String.format("%.6f", max*100));
		}
	}

	
}