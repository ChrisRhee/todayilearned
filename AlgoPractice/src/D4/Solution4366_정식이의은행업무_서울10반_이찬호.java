package D4;
import java.io.FileInputStream;
import java.util.Scanner;
class Solution4366_정식이의은행업무_서울10반_이찬호{
	public static long Ans;
	public static boolean ch;
	private static void solve(int[] big, int[] small, int bigone, int smallone) {
		for(int i=0; i<big.length; i++) {
			if(big[i]== 0) continue;
			big[i]--;
			solveSmall(big,small,bigone,smallone);
			if(ch) return;
			big[i]++;
		}
		
		for(int i=0; i<big.length; i++) {
			if(big[i]== bigone-1) continue;
			big[i]++;
			solveSmall(big,small,bigone,smallone);
			if(ch) return;
			big[i]--;
		}
	}
	
	private static void solveSmall(int[] big, int[] small, int bigone, int smallone) {
		for(int i=0; i<small.length; i++) {
			if(small[i] == smallone-1) {
				small[i]--;
				check(big, small, bigone, smallone);
				if(ch) return;
				small[i]++;
				
				small[i]-=2;
				check(big, small, bigone, smallone);
				if(ch) return;
				small[i]+=2;
			}else if(small[i] == 1) {
				small[i]++;
				check(big, small, bigone, smallone);
				if(ch) return;
				small[i]--;
				
				small[i]--;
				check(big, small, bigone, smallone);
				if(ch) return;
				small[i]++;
			}else if(small[i] == 0) {
				small[i]++;
				check(big, small, bigone, smallone);
				if(ch) return;
				small[i]--;
				
				small[i]+=2;
				check(big, small, bigone, smallone);
				if(ch) return;
				small[i]-=2;
			}
		}
	}

	private static void check(int[] big, int[] small, int bigone, int smallone) {
		String s = "";
		for(int i=0; i<big.length; i++) {
			s += big[i];
		}
		long a = Long.parseLong(s,bigone); 
		s= "";
		for(int i=0; i<small.length; i++) {
			s += small[i];
		}
		long b = Long.parseLong(s,smallone);
		if(a==b) {
			Ans = a;
			ch = true;
		}
	}

	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input4366.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		for(int tc = 1; tc <= T; tc++){
			ch = false;
			Ans =0;
			String b= sc.next();
			String t= sc.next();
			String s[]= b.split("");
			int bl[] = new int[s.length];
			for(int i=0; i<bl.length; i++) {
				bl[i] = Integer.parseInt(s[i]);
			}
			s = t.split("");
			int tl[] = new int[s.length];
			for(int i=0; i<tl.length; i++) {
				tl[i] = Integer.parseInt(s[i]);
			}
			solve(bl,tl,2,3);
			
			System.out.println("#"+tc + " "+Ans);
		}
	}

	
}