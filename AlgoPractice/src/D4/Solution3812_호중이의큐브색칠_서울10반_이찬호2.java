package D4;
import java.io.*;
import java.util.Arrays;
class Solution3812_호중이의큐브색칠_서울10반_이찬호2{
	public static long color[]; 
	public static int x,y,z,N;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3812.txt"));
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			String s[] = br.readLine().split(" ");
			x = Integer.parseInt(s[0]);
			y = Integer.parseInt(s[1]);
			z = Integer.parseInt(s[2]);
			int a = Integer.parseInt(s[3]);
			int b = Integer.parseInt(s[4]);
			int c = Integer.parseInt(s[5]);
			N = Integer.parseInt(s[6]);
			color = new long[N];
			long tcc[] = new long[N];
			//메인 라인 준비
			int temp[] = new int[x];
			for(int i = 0; i<x; i++) {
				temp[i] = Math.abs(a-i);
			}
			for(int i = 0; i<x; i++) {
				color[temp[i]%N]++;
			}
			System.out.println("temp"+Arrays.toString(temp));
			System.out.println("col1"+Arrays.toString(color));
			
			// 한줄 입력
			// 0~N~b 나 포함 안함
			int bot = 0;
			if( b < N) bot = b;
			else bot = N;
			for(int i=1; i<=bot; i++) {
				for(int j=0; j<x; j++) {
					tcc[(temp[j]+i)%N]++;
				}
			}
			System.out.println("tcc"+Arrays.toString(tcc));
			System.out.println("col2"+Arrays.toString(color));
			for(int i=0; i<N; i++) {
				color[i] += tcc[i]* (b/N);
			}
			System.out.println("col3"+Arrays.toString(color));
			for(int i=1; i<=(b%N); i++) {
				for(int j=0; j<x; j++) {
					color[(temp[j]+i)%N]++;
				}
			}
			
			System.out.println("col4"+Arrays.toString(color));
			// b ~ N
			bot = 0;
			if( y-b < N) bot = y-b;
			else bot = N;
			for(int i=0; i<bot; i++) {
				for(int j=0; j<x; j++) {
					tcc[(temp[j]+i)%N]++;
				}
			}
			//System.out.println("tcc2"+Arrays.toString(tcc));
			System.out.println("col5"+Arrays.toString(color));
			for(int i=0; i<N; i++) {
				color[i] += tcc[i]*((y-b)/N);
			}
			System.out.println("col6"+Arrays.toString(color));
			for(int i=1; i<(y-b)%N; i++) {
				for(int j=0; j<x; j++) {
					color[(temp[j]+i)%N]++;
//					System.out.println("c"+Arrays.toString(color));
				}
			}
//			System.out.println("col7"+Arrays.toString(color));
			
			tcc = new long[N];
//			System.out.println("tcc"+Arrays.toString(tcc));
			
//			System.out.println("col"+Arrays.toString(color));
			//////////////////////////////
			int top = 0;
			if( c < N) top = c;
			else top = N;
			for(int i=1; i<=top; i++) {
				for(int j=0; j<N; j++) {
					tcc[(j+i)%N] += color[j];
				}
			}
//			System.out.println("col1"+Arrays.toString(tcc));
			top = 0;
			if( z-c < N) top = z-c;
			else top = N;
			for(int i=0; i<top; i++) {
				for(int j=0; j<N; j++) {
					tcc[(j+i)%N] += color[j];
//					System.out.println("c"+Arrays.toString(tcc));
				}
			}
			
			System.out.print("#"+tc+" ");
			for(int i=0; i<N; i++) {
				System.out.print(tcc[i]+" ");
			}
			System.out.println();
		}
		br.close();
	}

}