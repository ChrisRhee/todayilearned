package D4;
import java.io.*;
import java.util.Arrays;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution3074_입국심사_서울10반_이찬호{
	public static long sim[],N,M;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3074.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			String s[] = br.readLine().split(" ");
			N= Long.parseLong(s[0]);
			M= Long.parseLong(s[1]);
			sim = new long[(int) N];
			for(int i =0; i<N; i++) {
				sim[i] = Long.parseLong(br.readLine());
			}
			Arrays.sort(sim);
			long index = Long.MAX_VALUE-1;
			long mid = index;
			while(true) {
				boolean temp = check(index); 
				boolean temp2 = check(index-1); 
				if(mid%2==0) mid = mid/2;
				else mid = (mid+1)/2;
				if(temp==true && temp2==false) break;
				if(temp) {
					index = index-mid;
				}else {
					index = index+mid;
				}
			}
			
			System.out.println("#"+tc + " "+index);
		}
	}

	private static boolean check(long input) {
		long temp =0;
		for(int i=0; i<N; i++) {
			temp += input/sim[i];
			if(temp >= M) return true;
		}
		return false;
	}
}