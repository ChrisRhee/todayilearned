package D4;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

class Solution5643_키순서_서울10반_이찬호2{
	public static int N, M, map1[][];
	public static boolean visit[];
	
	private static int solve(int s, int i,int count) {
		int temp = count;
		visit[i] = true;
		for(int j=1; j<=N; j++) {
			if(i==j) continue;
			if(s== 0) {
				if(map1[i][j] == 0) continue;
			}
			else {
				if(map1[j][i] == 0) continue;
			}
			if(visit[j] == true) continue;
			temp = solve(s,j, temp+1);
		}
		return temp;
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input5643.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine().trim());
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			N= Integer.parseInt(br.readLine().trim());
			M= Integer.parseInt(br.readLine().trim());
			map1 = new int[N+1][N+1];
			for(int i=0; i<M; i++) {
				String s[] = br.readLine().split(" ");
				map1[Integer.parseInt(s[0])][Integer.parseInt(s[1])] = 1;
			}
			
			for (int[] is : map1) {
				System.out.println(Arrays.toString(is));
			}
			for(int i=1; i<=N; i++) {
				int t = 0;
				visit = new boolean[N+1];
				t += solve(0,i,0);
				visit = new boolean[N+1];
				t += solve(1,i,0);
				if(t == N-1) Ans++;
			}
			System.out.println("#"+tc + " "+Ans);
		}
	}
	
}