package D4;
import java.io.*;
class Solution3234_준환이의양팔저울_서울10반_이찬호{
	public static int cnt,total,fac[],pow[];
	
	
	private static void solve(int[] num,int count,int left,int right,boolean visit[]) {
		if(right > left) return;
		if(num.length == count) {
			cnt++;
			return;
		}
		
		if(left > total - left) {
			cnt += fac[num.length-count]*pow[num.length-count];
			return;
		}
		
		for(int i=0; i<num.length; i++) {
			if(visit[i]) continue;
			visit[i] = true;
			solve(num,count+1,left+num[i],right,visit);
			solve(num,count+1,left,right+num[i],visit);
			visit[i] = false;
		}
	}

	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3234.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		pow = new int[10];
		fac = new int[10];
		pow[0] = 1;
		for(int i=1; i<10; i++) pow[i] = pow[i-1]*2;
		
		fac[0] = 1;
		fac[1] = 1;
		for(int i=2; i<10; i++) fac[i] = fac[i-1]*i;
		
//		System.out.println(Arrays.toString(pow));
//		System.out.println(Arrays.toString(fac));
		
		for(int tc = 1; tc <= T; tc++){
			int N= Integer.parseInt(br.readLine());
			String s[] = br.readLine().split(" ");
			int[] num = new int[N];
			//visit = new boolean[N];
			total = 0;
			cnt = 0;
			for(int i =0; i<N; i++) {
				num[i] = Integer.parseInt(s[i]);
			}
			
			for(int i=0; i<N; i++) {
				total += num[i];
			}
			solve(num,0,0,0,new boolean[N]);
			System.out.println("#"+tc + " "+cnt);
		}
		br.close();
	}

}