package D4;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;
class Solution5643_키순서_서울10반_이찬호{
	public static int N, M, map1[][],map2[][];
	public static boolean visit[];
	
	private static int solve(int[][] m, int i,int count) {
		int temp = count;
		visit[i] = true;
		for(int j=1; j<=N; j++) {
			if(i==j) continue;
			if(m[i][j] == 0) continue;
			if(visit[j] == true) continue;
			temp = solve(m,j, temp+1);
		}
		return temp;
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input5643.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			N= sc.nextInt();
			M= sc.nextInt();
			map1 = new int[N+1][N+1];
			map2 = new int[N+1][N+1];
			
			for(int i=0; i<M; i++) {
				int t1 = sc.nextInt();
				int t2 = sc.nextInt();
				map1[t1][t2] = 1;
				map2[t2][t1] = 2;
			}
			
			for(int i=1; i<=N; i++) {
				int t = 0;
				visit = new boolean[N+1];
				t += solve(map1,i,0);
				visit = new boolean[N+1];
				t += solve(map2,i,0);
				if(t == N-1) Ans++;
			}
			
			System.out.println("#"+tc + " "+Ans);
		}
	}
	
}