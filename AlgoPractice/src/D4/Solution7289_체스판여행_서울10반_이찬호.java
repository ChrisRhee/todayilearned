package D4;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
class Solution7289_체스판여행_서울10반_이찬호{
	public static int[][] map;
	public static int[] di = {-1,1,0,0, -1,-1,1,1, -1,-2,-2,-1, 1, 2,2,1};
	public static int[] dj = {0,0,-1,1, -1,1,-1,1, -2,-1, 1, 2,-2,-1,1,2};
	public static int N;
	public static int steps[][];
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input7289.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		for(int tc = 1; tc <= 1; tc++){
			int Ans =1;
			N= sc.nextInt();
			map = new int[N][N];
			steps = new int[(N*N)+1][2];
			for(int i =0; i<N; i++) {
				for(int j=0; j<N; j++) {
					map[i][j] = sc.nextInt();
					steps[map[i][j]][0] = i;
					steps[map[i][j]][1] = j;
				}
			}
			int curr = 0;
			for(int i=1; i<=N*N; i++) {
				if(checkState(i) == -1) continue;
				curr = checkState(i);
				break;
			}
			for(int i=1; i<N*N; i++) {
				System.out.println(i+"= "+checkState(i));
				bfs(checkState(i),checkState(i+1));
			}
//			for (int[] is : steps) {
//				System.out.println(Arrays.toString(is));
//			}
			
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
	
	private static void bfs(int checkState, int checkState2) {
		
	}

	// 0 = 룩
	// 1 = 비숍
	// 2 = 나이트
	// -1 = 한방에 못감
	private static int checkState(int curr) {
		int ii = steps[curr+1][0]-steps[curr][0];
		int jj = steps[curr+1][1]-steps[curr][1];
		int t = Math.abs(ii);
		for(int d =0; d<16; d++) {
			if(ii== 0 || jj == 0) 
				return 0;
			if(ii/t == di[d] && jj/t == dj[d] && (d>=4 && d<8)) 
				return 1;
			if(ii == di[d] && jj == dj[d] && d>= 8) 
				return 2;
		}
		return -1;
	}
}