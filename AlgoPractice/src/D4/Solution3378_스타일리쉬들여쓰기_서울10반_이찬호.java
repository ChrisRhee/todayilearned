package D4;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution3378_스타일리쉬들여쓰기_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3378.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= 1; tc++){
			int Ans =0;
			String[] s = br.readLine().split(" ");
			int N= Integer.parseInt(s[0]);
			int M = Integer.parseInt(s[1]);
			int RCSans[] = new int[3];
			int RCS[] = new int[7];
			int counts[][] = new int[N][4];
			int curr =0;
			for(int i=0; i<N; i++) {
				char list[] = br.readLine().toCharArray();
				int count = 0;
				boolean flag = true;
				for(int j= 0; j<list.length; j++) {
					if(list[j]=='.') count ++;
					else { 
						if(flag == true) {
							counts[i][0] = count;
						}
						flag = false;
					}
					if (list[j] == '(') {
						counts[i][1]++;
					}  else if (list[j] == '{') {
						counts[i][2]++;
					} else if (list[j] == '[') {
						counts[i][3]++;
					} else if (list[j] == ')') {
						counts[i][1]--;
					} else if (list[j] == '}') {
						counts[i][2]--;
					}  else if (list[j] == ']') {
						counts[i][3]--;
					}
					
					if(i == 0) continue;
					if(counts[i-1][1] == 1 && counts[i-1][2] == 0 && counts[i-1][3] == 0) RCS[0] = counts[i][0]-curr;
					else if(counts[i-1][1] == 0 && counts[i-1][2] == 1 && counts[i-1][3] == 0) RCS[1] = counts[i][0]-curr;
					else if(counts[i-1][1] == 0 && counts[i-1][2] == 0 && counts[i-1][3] == 1) RCS[2] = counts[i][0]-curr;
					else if(counts[i-1][1] == 0 && counts[i-1][2] == 1 && counts[i-1][3] == 1) RCS[3] = counts[i][0]-curr;
					else if(counts[i-1][1] == 1 && counts[i-1][2] == 0 && counts[i-1][3] == 1) RCS[4] = counts[i][0]-curr;
					else if(counts[i-1][1] == 1 && counts[i-1][2] == 1 && counts[i-1][3] == 0) RCS[5] = counts[i][0]-curr;
					else if(counts[i-1][1] == 1 && counts[i-1][2] == 1 && counts[i-1][3] == 1) RCS[6] = counts[i][0]-curr;
				}
				curr =counts[i][0];
			}
			
			//System.out.println(Arrays.toString(RCS));
			System.out.print("#"+tc+" ");
			for(int i=0; i<M; i++) {
				char list[] = br.readLine().toCharArray();
				if(i==0) {
					System.out.print(0+" ");
				}
				else {
					int res =0;
					for(int j=0; j<3; j++) {
						if(RCSans[j] >= 1) res += RCSans[j]*RCS[j];
					}
					System.out.print(res+" ");
				}
				
				for(int j= 0; j<list.length; j++) {
					if (list[j] == '(') {
						RCSans[0]++;
					}  else if (list[j] == '{') {
						RCSans[1]++;
					} else if (list[j] == '[') {
						RCSans[2]++;
					} else if (list[j] == ')') {
						RCSans[0]--;
					} else if (list[j] == '}') {
						RCSans[1]--;
					}  else if (list[j] == ']') {
						RCSans[2]--;
					} 
				}
				
				
				//System.out.println(Arrays.toString(RCSans));
			}
			
			
		}
		br.close();
	}
}