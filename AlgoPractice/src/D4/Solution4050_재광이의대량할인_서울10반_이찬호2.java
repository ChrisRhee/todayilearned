package D4;
import java.io.*;
import java.util.*;
class Solution4050_재광이의대량할인_서울10반_이찬호2{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input4050.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();
		for(int tc=1;tc<=T;tc++){
			int N=sc.nextInt();
			int m[]=new int[N+1];
			for(int i=0;i<N;i++) m[i]=sc.nextInt();
			int c=1;
			Arrays.sort(m);
			for(int i=N;i>=1;i--) {
				if(c++%3==0) continue;
				m[0]+=m[i];
			}
			System.out.println("#"+tc+" "+m[0]);
		}
	}
}