package D4;
import java.io.FileInputStream;
import java.util.*;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution3143_가장빠른문자열타이핑_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3143.txt"));
		Scanner c = new Scanner(System.in);
		int T = c.nextInt();
		for(int tc = 1; tc <= T; tc++){
			String s = c.next();
			s = s.replace(c.next(), "1");
			System.out.println("#"+tc + " "+s.length());
		}
	}
}