package D4;
import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
class Solution3812_호중이의큐브색칠_서울10반_이찬호{
	public static Queue<int[]> q;
	public static long visit[][][],color[]; 
	public static int x,y,z,N;
	public static int di[]= {-1,1,0,0,0,0};
	public static int dj[]= {0,0,-1,1,0,0};
	public static int dk[]= {0,0,0,0,-1,1};
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3812.txt"));
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			long Ans = 0;
			String s[] = br.readLine().split(" ");
			x = Integer.parseInt(s[0]);
			y = Integer.parseInt(s[1]);
			z = Integer.parseInt(s[2]);
			int a = Integer.parseInt(s[3]);
			int b = Integer.parseInt(s[4]);
			int c = Integer.parseInt(s[5]);
			N = Integer.parseInt(s[6]);
			color = new long[N];
			q = new LinkedList<>();
			if(N != 1) {
				visit = new long[x][y][z];
				q.offer(new int[] {a,b,c});
				visit[a][b][c] = 1;
				color[(int) ((visit[a][b][c]-1)%N)]++;
				bfs(a,b,c);
				System.out.print("#"+tc+" ");
				for(int i=0; i<N; i++) {
					System.out.print(color[i]+" ");
				}
				System.out.println();
			}
			else {
				Ans = (long)x*(long)y*(long)z;
				System.out.println("#"+tc + " "+Ans);
			}
//			for(int i =0; i<x; i++) {
//				for(int j =0; j<y; j++) {
//					for(int k =0; k<z; k++) {
//						System.out.print(visit[i][j][k]+" ");
//					}
//					System.out.println();
//				}
//				System.out.println();
//			}
			
		}
		br.close();
	}

	private static void bfs(int a, int b, int c) {
		while(!q.isEmpty()) {
			int curr[] = q.poll();
			for(int d=0; d<6; d++) {
				int ii = curr[0] + di[d];
				int jj = curr[1] + dj[d];
				int kk = curr[2] + dk[d];
				if( ii < 0 || ii >= x || jj < 0 || jj >= y || kk < 0 || kk >= z) continue;
				if(visit[ii][jj][kk]==0) {
					visit[ii][jj][kk] = visit[curr[0]][curr[1]][curr[2]] + 1;
					color[(int) ((visit[ii][jj][kk]-1)%N)]++;
					q.offer(new int[] {ii,jj,kk});
				}
			}
		}
	}
}