package D4;
import java.io.*;
import java.util.Arrays;
class Solution4672_수진이의팰린드롬_서울10반_이찬호{
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input4672.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			String s[] = br.readLine().split("");
			boolean ch = false;
			int slen = s.length;
			Arrays.sort(s);
			int count = 1;
			for(int i=0; i<slen-1; i++) {
				if(s[i].equals(s[i+1])) {
					ch = true;
					count++;
				}
				else {
					Ans += solve(count);
					ch = false;
					count = 1;
				}
			}
			
			if(!ch) Ans += solve(1);
			else Ans += solve(count);
			
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}

	private static int solve(int i) {
		int temp =0;
		for(int j=1; j<= i; j++) temp+= j;
		return temp;
	}
	
}