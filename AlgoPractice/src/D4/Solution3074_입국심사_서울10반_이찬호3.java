package D4;
import java.io.*;
import java.util.Arrays;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution3074_입국심사_서울10반_이찬호3{
	public static long sim[],N,M,count[];
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input3074.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			String s[] = br.readLine().split(" ");
			N= Long.parseLong(s[0]);
			M= Long.parseLong(s[1]);
			//M=1000000000l;
			sim = new long[(int) N];
			count = new long[(int) N];
			for(int i =0; i<N; i++) {
				sim[i] = Long.parseLong(br.readLine());
			}
			Arrays.sort(sim);
			long index = Long.MAX_VALUE/10;
			long mid = index;
			while(true) {
				long temp = check(index);
				long temp2 = check(index-1);
				if( M <= temp && M > temp2) break;
				if(mid%2==0) mid = mid/2;
				else mid = (mid+1)/2;
				if(M <= temp) {
					index = index-mid;
				}else {
					index = index+mid;
				}
			}
			//System.out.println(sim[0]+" "+index);
			System.out.println("#"+tc + " "+sim[0]*index);
		}
	}

	private static long check(long index) {
		long temp =0;
		for(int i=0; i<N; i++) {
			if(sim[i] > sim[0]*index) break;
			temp += sim[0]*index/sim[i];
		}
		return temp;
	}
}