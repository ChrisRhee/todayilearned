package D2;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 마주보는 것들을 곱하고, 그것들을 더한값의 최대를 구해라
 * 조건
 * 	- 최대 밖으로 못나감
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution1959_두개의숫자열_서울10반_이찬호{
	public static int anum[],bnum[];
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1959.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		//int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			int max =0;
			int an = sc.nextInt();
			int bn = sc.nextInt();
			
			anum = new int[an];
			bnum = new int[bn];
			
			for(int i=0; i<an; i++) {
				anum[i] = sc.nextInt();
			}
			for(int i=0; i<bn; i++) {
				bnum[i] = sc.nextInt();
			}
			//System.out.println(Arrays.toString(anum));
			//System.out.println(Arrays.toString(bnum));
			if(an < bn) {
				for(int i=0; i<=bn-an; i++) {
					for(int j=0; j<an; j++) {
						//System.out.print(anum[j] * bnum[j+i]+" ");
						Ans += anum[j] * bnum[j+i];
					}
					//System.out.println("Ans="+Ans);
					if(max < Ans) max = Ans;
					Ans =0;
				}
			}else {
				for(int i=0; i<=an-bn; i++) {
					for(int j=0; j<bn; j++) {
						Ans += anum[j+i] * bnum[j];
					}
					if(max < Ans) max = Ans;
					Ans =0;
				}
			}
			
			System.out.println("#"+tc + " "+max);
		}
		br.close();
	}
}