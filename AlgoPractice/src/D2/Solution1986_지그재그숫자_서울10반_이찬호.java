package D2;
import java.io.*;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution1986_지그재그숫자_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1986.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		
		for(int tc = 1; tc <= T; tc++){
			int Ans=0;
			int num = sc.nextInt();
			
			for(int i=1; i<=num; i++) {
				if(i%2==1) {
					Ans +=i;
				}else {
					Ans -=i;
				}
			}
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
}