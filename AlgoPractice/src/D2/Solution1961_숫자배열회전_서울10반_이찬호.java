package D2;
import java.io.*;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution1961_숫자배열회전_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1961.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		//int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			int N= sc.nextInt();
			int num[][] = new int[N][N];
			
			for(int i =0; i<N; i++) {
				for(int j=0; j<N; j++) {
					num[i][j] = sc.nextInt();
				}
			}
			
			System.out.println("#"+tc);
			for(int i = 0; i<N; i++) {
				for(int k=0; k<3; k++) {
					if(k == 0) {
						for(int j=N-1; j>=0; j--) {
							System.out.print(num[j][i]);
						}
						System.out.print(" ");
					}else if(k==1) {
						for(int j=0; j<N; j++) {
							System.out.print(num[N-1-i][N-1-j]);
						}
						System.out.print(" ");
					}else {
						for(int j=0; j<N; j++) {
							System.out.print(num[j][N-1-i]);
						}
					}
				}
				System.out.println();
			}
		}
		br.close();
	}
}