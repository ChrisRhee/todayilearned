package D2;
import java.io.*;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 두번째날짜가 첫번째 날짜의 며칠째인지 구하자
 * 조건
 * 	- 
 * 입력
 *  - 첫날, 둘째날
 * 출력
 * 	- 차이
 * 풀이 
 * 	- 날짜 다 더해서 빼면 될듯
 * */
class Solution1948_날짜계산기_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1948.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		int month[] = {0,31,59,90,120,151,181,212,243,273,304,334};
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			int aM= sc.nextInt();
			int aD= sc.nextInt();
			int bM= sc.nextInt();
			int bD= sc.nextInt();
			Ans = (month[bM-1] + bD) - (month[aM-1]+aD) +1 ; 
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
}