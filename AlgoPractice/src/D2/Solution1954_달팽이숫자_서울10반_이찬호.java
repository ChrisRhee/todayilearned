package D2;
import java.io.*;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution1954_달팽이숫자_서울10반_이찬호{
	public static int[] di= {0,1,0,-1};
	public static int[] dj= {1,0,-1,0};
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1954.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			int N= sc.nextInt();
			int map[][] = new int[N][N];
			
			int si = 0;
			int sj = 0;
			int count = 1;
			int dcount = 0;
			while(true) {
				map[si][sj] = count;
				if(count >= N*N) break;
				count++;
				si += di[dcount];
				sj += dj[dcount];
				if(si == N || sj == N || si < 0 || sj < 0 || map[si][sj]>0) {
					si -= di[dcount];
					sj -= dj[dcount];
					dcount ++;
					if(dcount ==4 ) dcount = 0;
					si += di[dcount];
					sj += dj[dcount];
				}
				
			}
			
			System.out.println("#"+tc);
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					System.out.print(map[i][j]+" ");
				}
				System.out.println();
			}
		}
	}
}