package D2;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution1989_초심자의회문검사_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1989.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			int Ans = 1;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			String s[] = br.readLine().split("");
			for(int i =0; i<s.length/2; i++) {
				 if(s[i].equals(s[(s.length-1)-i])) continue;
				 else Ans = 0;
			}
			
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
}