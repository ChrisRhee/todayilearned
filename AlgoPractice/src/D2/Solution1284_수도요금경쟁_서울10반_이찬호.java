package D2;
import java.io.*;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution1284_수도요금경쟁_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1284.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		
		for(int tc = 1; tc <= T; tc++){
			int P = sc.nextInt(); // A1리터당 요금
			int Q = sc.nextInt(); // B기본요금
			int R = sc.nextInt(); // R 초과시
			int S = sc.nextInt(); // 1리터당 S요금
			int W = sc.nextInt(); // 총 수도량
			int Ans =0;
			int A=0;
			int B=0; 
			
			A = P*W;
			if(W <= R) B = Q;
			else B = Q + (W-R)*S;
			
			if(A < B) Ans = A;
			else Ans = B;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
}