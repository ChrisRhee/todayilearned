package D2;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 민석이가 잘수있도록 양을 세주자
 * 조건
 * 	- N의 배수번호인 양을 세기로 한다.
 *  - N, 2N, ... kN 이렇게 센다.
 *  - 각 자리수에서 모든 숫자를 보는 것은 최소 몇번 양을 센 시점일까?
 * 입력
 *  - 
 * 출력
 * 	- 0~9이 다 나왔을때의 x
 * 풀이 
 * 	- 
 * */
class Solution1288_새로운불면증치료법_서울10반_이찬호{
	public static int[] nums;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1288.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		
		for(int tc = 1; tc <= T; tc++){
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			int ans= 0;
			int num= sc.nextInt();
			nums = new int[10];
			int i=1;
			while(true) {
				int temp = num*i;
				ans = temp;
				while(true) {
					if(temp == 0) break;
					nums[temp%10] = 1;
					temp /= 10;
				}
				if(check()) break;
				i++;
			}
			
			System.out.println("#"+tc + " "+ans);
		}
		br.close();
	}

	private static boolean check() {
		boolean check = true;
		for(int i=0; i<10; i++) {
			if(nums[i]== 0) {
				check = false;
				break;
			}
		}
		return check;
	}
}