package D2;
import java.io.*;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution1966_숫자를정렬하자_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1966.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		//int T= Integer.parseInt(br.readLine());
		int temp =0;
		for(int tc = 1; tc <= T; tc++){
			int N= sc.nextInt();
			int num[] = new int[N];
			for(int i=0; i<N; i++) {
				num[i] = sc.nextInt();
			}
			
			for(int i=0; i<N; i++) {
				for(int j=i; j<N; j++) {
					if(i==j)continue;
					if(num[i] > num[j]) {
						temp = num[i];
						num[i] = num[j];
						num[j] = temp;
					}
				}
			}
			
			System.out.print("#"+tc+" ");
			for(int i=0; i<N;i++) {
				System.out.print(num[i]+" ");
			}
			System.out.println();
		}
		br.close();
	}
}