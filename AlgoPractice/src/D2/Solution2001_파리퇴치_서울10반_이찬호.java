package D2;
import java.io.*;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- check함수를 만들어서 해당 M만큼 탐색해서 값을 리턴하게 한다. 
 * */
class Solution2001_파리퇴치_서울10반_이찬호{
	public static int[][] map;
	public static int N,M;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input2001.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			int temp =0;
			N= sc.nextInt();
			M= sc.nextInt();
			map = new int[N][N];
			for(int i =0; i<N; i++) {
				for(int j =0; j<N; j++) {
					map[i][j] = sc.nextInt();
				}
			}
			
			for(int i =0; i<=N-M; i++) {
				for(int j =0; j<=N-M; j++) {
					temp = check(i,j);
					if(Ans < temp) Ans = temp;
				}
			}
			
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
	private static int check(int ii, int jj) {
		int temp=0;
		for(int i=ii; i<M+ii; i++) {
			for(int j=jj; j<M+jj; j++) {
				temp += map[i][j];
			}
		}
		return temp;
	}
}