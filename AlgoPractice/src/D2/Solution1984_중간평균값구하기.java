package D2;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
class Solution1984_중간평균값구하기{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1984.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			float Ans =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			String r[] = br.readLine().split(" ");
			int num[] = new int[r.length];
			for(int i =0; i<r.length; i++) {
				num[i] = Integer.parseInt(r[i]);
			}
			
			Arrays.sort(num);
			for(int i=1; i<num.length-1; i++) {
				Ans+= num[i];
			}
			Ans = Ans/(num.length-2);
			
			System.out.println("#"+tc + " "+Math.round(Ans));
		}
		br.close();
	}
}