package D2;
import java.io.*;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * - 0 현재속도 유지
 * - 1 가속
 * - 2 감속
 * 입력
 * -  
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution1940_가랏RC카_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1940.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			int command = 0;
			int currSpeed = 0;
			int N= sc.nextInt();
			
			for(int i=0; i<N; i++) {
				command = sc.nextInt();
				if(command == 1) {
					currSpeed += sc.nextInt();
				}else if(command ==2) {
					int tempSpeed=sc.nextInt();
					currSpeed -= tempSpeed;
					if(currSpeed < 0) currSpeed =0;
				}else {
					
				}
				Ans += currSpeed;
			}
			
			
			for(int i =0; i<N; i++) {
				
			}
			System.out.println("#"+tc + " "+Ans);
		}
	}
}