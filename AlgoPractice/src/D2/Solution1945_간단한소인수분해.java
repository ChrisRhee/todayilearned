package D2;
import java.io.*;
import java.util.*;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 소인수 분해 구하기
 * 조건
 * 	- N = 2^a * 3^b * 5^c * 7^d * 11^e 이다.
 * 입력
 *  - 2 <= N <= 10,000,000
 * 출력
 * 	- N이 주어졌을 때 a b c d e 출력
 * 풀이 
 * 	- 2,3,5,7,11 로 나눠서 0으로 떨어지면 그거로 나누고 다시 한다. 나눌게 더 없어질때까지 반복
 * */
class Solution1945_간단한소인수분해{
	public static int[] res = {2,3,5,7,11};
	public static int[] ans;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1945.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		for(int tc = 1; tc <= T; tc++){
			ans = new int[5];
			int num= sc.nextInt();
			while(true) {
				if(num == 1) break;
				for(int i=0; i<5; i++) {
					if(num%res[i]==0) {
						num = num/res[i];
						ans[i]++;
					}
				}
			}
			System.out.print("#"+tc + " ");
			for(int i=0; i<5; i++) System.out.print(ans[i]+" ");
			System.out.println();
		}
	}
}