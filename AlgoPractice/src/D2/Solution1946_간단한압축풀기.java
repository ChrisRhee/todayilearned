package D2;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
class Solution1946_간단한압축풀기{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1946.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++)	{
			int Ans =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			String r[] = br.readLine().split(" ");
			int num[] = new int[r.length];
			for(int i =0; i<r.length; i++) {
				num[i] = Integer.parseInt(r[i]);
			}
			
			
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
}