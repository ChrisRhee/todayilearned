package D2;
import java.io.*;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution1979_어디에단어가들어갈수있을까_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1979.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			int N= sc.nextInt();
			int K= sc.nextInt();
			int map[][] = new int[N][N];
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					map[i][j] = sc.nextInt();
				}
			}
			for(int i=0; i<N; i++) {
				int tempx =0;
				int tempy =0;
				for(int j=0; j<N; j++) {
					if(map[i][j]==1) tempx++;
					else {
						if(tempx==K) Ans++;
						tempx =0;
					}
				}
				if(tempx==K) Ans++; 
			}
			
			for(int i=0; i<N; i++) {
				int temp =0;
				for(int j=0; j<N; j++) {
					if(map[j][i]==1) temp++;
					else {
						if(temp==K) Ans++;
						temp =0;
					}
				}
				if(temp==K) Ans++; 
			}
			
			
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
}