package D2;
import java.io.*;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Solution1859_백만장자프로젝트_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1859.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			Long Ans = 0l;
			int N= Integer.parseInt(br.readLine());
			String s[] = br.readLine().split(" ");
			Long max = 0l;
			
			for(int i = N-1; i>=1; i++) {
				Long s1 = Long.parseLong(s[i]);
				Long s2 = Long.parseLong(s[i-1]);
				if(s1 <= s2) {
					continue;
				}else {
					if(max < s1) max = s1;
					Ans += max-s2;
				}
			}
			
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
}