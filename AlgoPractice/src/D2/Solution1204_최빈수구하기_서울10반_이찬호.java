package D2;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
class Solution1204_최빈수구하기_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1204.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			int Ans =0;
			int maxindex = 0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			int N= Integer.parseInt(br.readLine());
			String r[] = br.readLine().split(" ");
			int a[] = new int[101];
			for(int i =0; i<r.length; i++) {
				a[Integer.parseInt(r[i])]++;
			}
			for(int i=0; i<a.length; i++) {
				if(Ans < a[i]) { Ans = a[i];
					maxindex = i;
				}else if(Ans == a[i]) {
					if(maxindex < i ) maxindex = i;
				}
			}
			System.out.println("#"+tc + " "+maxindex);
		}
		br.close();
	}
}