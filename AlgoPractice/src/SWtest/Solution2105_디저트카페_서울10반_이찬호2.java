package SWtest;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 디저트 최고로 먹을 수 있는 방법
 * 풀이 
 * - DFS를 잘 꾸며보자. 
 * - DFS가 시작해서 처음 자기 위치로 돌아와야 한다.
 * 		- 시작 하는 위치를 dfs에 넣어줘서, 다음 위치가 start위치와 동일한지 체크 후 값을 ans에 저장한다.
 * - 이미 먹은 숫자면 가지 못한다.
 * 		- 디저트는 1~100까지 있다. 101개 디저트 배열을 만들어서 방문하면 1 아니면 0으로 한다. 1이면 방문 못함
 * - 방금 왔던 곳으로 못 돌아 간다.
 * 		- ex 카페를 dfs에 넣어준다. 다음 위치 ii,jj가 ex의 i,j 이면 패스한다.
 * - 4각형을 이뤄야 한다.
 * 		- 4방향 visit을 가지고 있는다. 한번 턴 하면 이전에 왔던 방향을 true로 만들어주고 다시 그 방향으로 못 움직이게 한다.
 * 		- 그렇게 하면 4각형을 이루는 방향으로만 진행 할 수 있다.
 * */
class Solution2105_디저트카페_서울10반_이찬호2{
	public static int N, map[][], des[], ans,count;
	public static boolean visit[][];
	public static int[] di= {1,1,-1,-1};
	public static int[] dj= {1,-1,-1,1};
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input2105.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		
		for(int tc = 1; tc <= T; tc++){
			N= Integer.parseInt(br.readLine());
			ans = -1;
			map = new int [N][N];
			des = new int[101];
			for(int i =0; i<N; i++) {
				String s[] = br.readLine().split(" ");
				for(int j=0; j<N; j++) map[i][j] = Integer.parseInt(s[j]);
			}
			for(int i =0; i<N; i++) {
				for(int j=0; j<N; j++) {
					visit = new boolean[N][N];
					visit[i][j] = true;
					des[map[i][j]] += 1;
					count = 1;
					dfs(i,j,0,new int[] {i,j}, new int[] {-1,-1});
					des[map[i][j]] -= 1;
				}
			}
			System.out.println("#"+tc + " "+ans);
		}
	}
	
	private static void dfs(int i, int j, int currDir, int[] start, int[] ex) {
		for (int d = currDir; d < 4; d++) {
			int ii = i + di[d];
			int jj = j + dj[d];
			if( ii < 0 || ii >= N || jj < 0 || jj >= N ) continue;
			if( ii == ex[0] && jj == ex[1]) continue;
			// 집도착
			if(ii == start[0] && jj == start[1]) {
				if(ans < count) ans = count;
				return;
			}
			if(visit[ii][jj] == false && des[map[ii][jj]]==0) {
				visit[ii][jj] = true;
				des[map[ii][jj]] += 1;
				count++;
				dfs(ii, jj, d, start, new int[] {i,j});
				count--;
				des[map[ii][jj]] -= 1;
				visit[ii][jj] = false;
			}
		}
	}
}