package SWtest;
import java.io.*;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 구슬은 좌, 우로만 움직일 수 있어서 항상 맨 위에 있는 별독만 깨트릴 수 있다.
 *  - 벽돌은 숫자 1~9로 표현됨. 
 *  - 구슬이 명중한 벽돌은 상하좌우로(벽돌에 적힌숫자-1)칸 만큼 같이 제거된다.
 *  - 제거되는 범위 내에 있는 벽돌은 동시에 제거된다. 
 * 입력
 *  - 테스트케이스 T
 *  - 1 <= N <= 4, 2 <= W <= 12, 2 <= H <= 15
 *  - 벽돌 정보들 0 은 없는거, 1~9 벽돌들
 * 출력
 * 	- 최대한 많이 없애고 남은 벽돌 개수
 * 풀이 
 * 	- 남은 벽돌수 체크하는 함수
 *  - 벽돌이 맞으면 그 연쇠작용까지 해서 다 삭제하는 함수,
 *  - 삭제된 벽돌들 밑으로 떨어지게 하는 함수
 *  - 구슬을 떨어트릴때 처음부터 다 해봐야할거같은데..? 구슬개수 N만큼 W방향 전체에 계속 떨어드려서 결과 확인해봐야할듯
 * */
class Solution5656_벽돌깨기_서울10반_이찬호{
	public static int N, W, H, Ans;
	public static int[][] map,temp;
	
	private static void solve(int[][] map2, int count) {
		
		if(count == N) {
			int aa = check(map2);
			if(Ans < aa) return;
			// 남은것 체크
			if(Ans > aa) Ans = aa;
			return;
		}
		
		for(int k=0; k<W; k++) {
			int[][] tem = new int[H][W];
			int[][] ttt = dropBoll(k,map2);
			for(int i =0; i<H; i++) 
				for(int j =0; j<W; j++) 
					tem[i][j] = ttt[i][j]; 
			
			ttt = dropblock(tem);
			for(int i =0; i<H; i++) 
				for(int j =0; j<W; j++) 
					tem[i][j] = ttt[i][j]; 
			
			solve(tem,count+1);
		}
	}
	// j줄에 돌 덜어트림
	private static int[][] dropBoll(int j,int map[][]) {
		temp = new int[H][W];
		for(int i=0; i<H; i++) 
			for(int k=0; k<W; k++) 
				temp[i][k] = map[i][k];
		int top = -1;
		for(int i=0; i<H; i++) {
			if(temp[i][j] > 0) {
				top = i;
				break;
			}
		}
		if(top== -1) return map;
		blockPop(top,j,temp[top][j]);
		return temp;
	}
	// 해당 블록 터트린다.
	private static void blockPop(int i, int j, int len) {
		if(temp[i][j] ==0)return;
		temp[i][j] = 0;
		for(int k=1; k<len; k++) {
			// 상하좌우0
			if(j+k<W) blockPop(i,j+k, temp[i][j+k]);
			if(j-k>=0) blockPop(i,j-k, temp[i][j-k]);
			if(i+k<H) blockPop(i+k,j, temp[i+k][j]);
			if(i-k>=0) blockPop(i-k,j, temp[i-k][j]);
		}
	}
	// 공중에 떠있는 돌들 내려트리기
	private static int[][] dropblock(int map[][]) {
		for(int j=0; j<W; j++) {
			int curr = 0;
			int count = 0;
			for(int i=H-1; i>=0; i--) {
				if(curr == 0 && map[i][j] > 0)continue;
				if(map[i][j]==0) {
					if(curr > 0)continue;
					curr = i;
				}
				else if(map[i][j] > 0) {
					swap(i,curr,j,map);
					curr = 0;
					i= H-1-count;
					count++;
				}
			}
		}
		return map;
	}
	// 내려트릴때 바로 옮김
	private static void swap(int i, int curr, int j,int map[][]) {
		int t = map[i][j];
		map[i][j] = map[curr][j];
		map[curr][j] = t;
	}

	private static int check(int[][] map2) {
		int temp =0;
		for(int i=0; i<H; i++) 
			for(int j=0; j<W; j++) 
				if(map2[i][j] > 0) temp++;
		return temp;
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input5656.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T= Integer.parseInt(br.readLine());
		for(int tc = 1; tc <= T; tc++){
			Ans =Integer.MAX_VALUE;
			String s[] = br.readLine().split(" ");
			N= Integer.parseInt(s[0]);
			W= Integer.parseInt(s[1]);
			H= Integer.parseInt(s[2]);
			
			map = new int[H][W];
			for(int i =0; i<H; i++) {
				s = br.readLine().split(" ");
				for(int j =0; j<W; j++) {
					map[i][j] = Integer.parseInt(s[j]);
				}
			}
			
			solve(map,0);
			
			//출력
			System.out.println("#"+tc + " "+Ans);
		}
		br.close();
	}
	
	
}