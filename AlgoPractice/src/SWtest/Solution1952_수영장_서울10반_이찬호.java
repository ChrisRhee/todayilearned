package SWtest;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
class Solution1952_수영장_서울10반_이찬호{
	public static int costs[],months[];
	
	private static int getYear() {
		int temp = 0;
		for(int i=0; i<12; i++) {
			months[i] = 0;
		}
		temp = costs[3];
		return temp;
	}
	private static int check(int[] temp) {
		int t = 0;
		int t2 = 0;
		for(int i=0; i<12; i++) {
			t += temp[i];
			t2 = getYear();
		}
		if( t < t2) return t;
		else return t2;
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1952.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		
		for(int tc = 1; tc <= T; tc++){
			costs = new int [4];
			for(int i=0; i<4; i++) {
				costs[i] = sc.nextInt();
			}
			months = new int [12];
			int temp[] = new int[12];
			for(int i =0; i<12; i++) {
				months[i] = sc.nextInt();
			}
			
			for(int i=0; i<2; i++) {
				for(int j= 0; j<12; j++) {
					switch(i) {
					case 0:
						temp[j] = months[j]*costs[0];
						break;
					case 1:
						temp[j] = Math.min(temp[j], costs[1]);
						break;
					}
					if(temp[j]==0) continue;
					//System.out.println(Arrays.toString(temp));
				}
			}
			
			System.out.println("c="+mon3check(temp));
			
			
			System.out.println("#"+tc + " "+check(temp));
		}
	}
	private static int mon3check(int[] temp) {
		int tt[] = new int [12];
		int min = Integer.MAX_VALUE;
		for(int i=0; i<12; i++) {
			int t = 0;
			for(int j=0; j<12; j++) tt[j] = temp[j];
			for (int k=0; k<3; k++) {
				if(i+k >=12 ) continue;
				t += tt[i+k];
				tt[i+k]= 0;
				tt[i] = Math.min(t, costs[2]);
			}
			t = 0;
			for(int j=0; j<12; j++) t += tt[j];
			System.out.println(Arrays.toString(tt) + " "+t);
			if(min > t ) min = t;
		}
		
		return min;
	}
	
}