package SWtest;
import java.io.FileInputStream;
import java.util.Scanner;
class Solution1952_수영장_서울10반_이찬호2{
	public static int costs[],months[],min;
	private static void solve(int month, int val) {
		if (month>12) {
            min = val<=min? val:min;
            return;
        }
		solve(month+1,val+months[month]*costs[0]);
		solve(month+1,val+costs[1]);
		solve(month+3,val+costs[2]);
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1952.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		
		for(int tc = 1; tc <= T; tc++){
			costs = new int [4];
			for(int i=0; i<4; i++)
				costs[i] = sc.nextInt();
			months = new int [13];
			min = costs[3];
			for(int i =1; i<=12; i++)
				months[i] = sc.nextInt();
			solve(1,0);
			System.out.println("#"+tc+" "+min);
		}
	}
}