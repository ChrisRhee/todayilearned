package SWtest;
import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
class Solution5644_무선충전_서울10반_이찬호{
	public static int[] di= {0,-1,0,1,0};
	public static int[] dj= {0,0,1,0,-1};
	public static int ap[][],map[][],M,numBC;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input5644.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		
		for(int tc = 1; tc <= T; tc++){
			
			int Ans = 0;
			M = sc.nextInt();
			numBC = sc.nextInt();
			map = new int[10+1][10+1];
			Queue<Integer> aq = new LinkedList<>();
			Queue<Integer> bq = new LinkedList<>();
			for(int i=0; i<M; i++) aq.offer(sc.nextInt());
			for(int i=0; i<M; i++) bq.offer(sc.nextInt());
			ap = new int[numBC+1][4];
			for(int i=1; i<=numBC; i++) {
				ap[i][1] = sc.nextInt();
				ap[i][0] = sc.nextInt();
				ap[i][2] = sc.nextInt();
				ap[i][3] = sc.nextInt();
			}
			for (int[] is2 : ap) {
				System.out.println(Arrays.toString(is2));
			}
			int ai = 1;
			int aj = 1;
			int bi = 10;
			int bj = 10;
			Ans += ap[check(ai,aj)[3]][3]; 
			Ans += ap[check(bi,bj)[3]][3];
			System.out.println();
			System.out.println(Arrays.toString(check(ai,aj)));
			System.out.println(Arrays.toString(check(bi,bj)));
			for(int i=0; i<M; i++) {
				int d = aq.poll();
				ai += di[d];
				aj += dj[d];
				d = bq.poll();
				bi += di[d];
				bj += dj[d];
				int alist[] = check(ai,aj);
				int blist[] = check(bi,bj);
				if(alist[numBC+1] == 1 && blist[numBC+1] == 1) {
					for(int j=0; j<numBC; j++) {
						// 같은곳 접속
						if(alist[j] ==1 && blist[j] ==1) {
							Ans -= ap[alist[3]][3];
							break;
						}
					}
				}
				else if(alist[numBC+1] >= 2 && blist[numBC+1] == 1) {
					for(int j=0; j<numBC; j++) {
						// 같은곳 접속
						if(alist[j] ==1 && blist[j] ==1) {
							Ans -= ap[alist[3]][3];
							break;
						}
					}
				}
				Ans += ap[alist[3]][3]; 
				Ans += ap[blist[3]][3]; 
			}
			System.out.println("#"+tc + " "+Ans);
		}
	}
	
	public static int[] check(int x, int y) {
		int[] temp = new int[numBC+2];
		int max =0;
		for(int d=1; d<=numBC; d++) {
			if(ap[d][2] >= Math.abs(x-ap[d][0])+Math.abs(y-ap[d][1])) {
				temp[d] = 1;
				temp[temp.length-1]++;
				if(max < ap[d][3]) {
					max = ap[d][3];
					temp[temp.length-2] = d;
				}
			}
		}
		return temp;
	}
	
}