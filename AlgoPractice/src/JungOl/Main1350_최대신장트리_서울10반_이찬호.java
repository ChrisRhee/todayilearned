package JungOl;
import java.io.*;
import java.util.Arrays;
class Main1350_최대신장트리_서울10반_이찬호{
	public static int map[][],N,edge,max,Ans;
	public static boolean visit[];
	private static void solve(int i,int count, int val) {
		if(count == N) {
			if(max < val) max = val;
			return;
		}
		int max =0;
		int mi =0;
		for(int j=1; j<=N; j++) {
			if(visit[j] == true) continue;
			if(map[i][j] == 0)continue;
			if( max < map[i][j]) {
				max = map[i][j];
				mi = j;
			}
		}
		visit[mi]= true;
		solve(mi, count +1,val + map[i][mi]);
		visit[mi] = false;
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1350.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
		N = Integer.parseInt(s[0]);
		edge = Integer.parseInt(s[1]);
		Ans = 0;
		map = new int[N+1][N+1];
		max =0;
		for(int i=0; i<=N; i++) {
			Arrays.fill(map[i], 9999999);
		}
		int tem[][] = new int[N+1][N+1];
		for(int i=1; i<=edge; i++) {
			s = br.readLine().split(" ");
			for(int j=1; j<=N; j++) {
				if(i==j)continue;
				int ii= Integer.parseInt(s[0]);
				int jj= Integer.parseInt(s[1]);
				int res= Integer.parseInt(s[2]);
				map[ii][jj] =map[jj][ii] = res* -1; 
			}
		}
		for (int[] is : map) {
			System.out.println(Arrays.toString(is));
		}
		
		visit = new boolean[N+1];
		for(int k=1; k<=N; k++) {
			for(int i=1; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					if(i==j)continue;
					if(map[i][j] > map[i][k]+map[k][j]) {
						map[i][j] = map[i][k]+map[k][j];
						tem[i][j] = k;
					}
				}
			}
		}
		
		for(int i=1; i<=N; i++) {
			visit[i] = true;
			solve(i,1,0);
			visit[i] = false;
		}
		
		for (int[] is : map) {
			System.out.println(Arrays.toString(is));
		}
		System.out.println();
		for (int[] is : tem) {
			System.out.println(Arrays.toString(is));
		}
		System.out.println(max);
		br.close();
		
	}

}