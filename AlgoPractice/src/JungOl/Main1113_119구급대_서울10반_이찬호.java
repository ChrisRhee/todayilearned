package JungOl;
import java.io.*;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 최소로 코너를 돌아서 구급대가 빨리 도착하게 해주자
 * 풀이 
 * - DFS로 방문을 다 해본다.
 * - 코너를 돌 때마다 count를 증가시키고, 목적지에 도착하면 count를 ans에 저장한다.
 * - ans보다 count가 작을때만 저장하고, 
 * - count가 ans보다 큰 상황이면 더이상 갈 필요 없으니 바로 return해서 백트랙킹한다. 
 * */
class Main1113_119구급대_서울10반_이찬호{
	public static int N,M,map[][], DI,DJ, count,ans;
	public static int di[] = {-1,1,0,0};
	public static int dj[] = {0,0,-1,1};
	public static boolean visit[][];
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1113.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		N = Integer.parseInt(s[0]);
		M = Integer.parseInt(s[1]);
		s = br.readLine().split(" ");
		ans = Integer.MAX_VALUE;
		DI= Integer.parseInt(s[0]);
		DJ= Integer.parseInt(s[1]);
		visit = new boolean[N][M];
		count = 0;
		map = new int[N][M];
		for(int i=0 ; i<N; i++) {
			s = br.readLine().split(" ");
			for(int j=0; j<M; j++) map[i][j] = Integer.parseInt(s[j]); 
		}
		
		visit[0][0]= true;
		dfs(0,0,0);
		System.out.println(ans-1);
		br.close();
	}

	private static void dfs(int i, int j, int currDir) {
		if(i==DI && j== DJ) {
			if(ans > count) ans = count;
			return;
		}
		if(ans < count) return;
		for(int d=0; d<4; d++) {
			int ii= i+di[d];
			int jj= j+dj[d];
			if(ii <0 || ii>=N || jj< 0 || jj>= M) continue;
			if(visit[ii][jj]==false && map[ii][jj] ==1 ) {
				visit[ii][jj] = true;
				if(currDir != d) count++;
				dfs(ii,jj,d);
				if(currDir != d) count--;
				visit[ii][jj] = false;
			}
		}
	}
}