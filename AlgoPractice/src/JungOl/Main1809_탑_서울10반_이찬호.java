package JungOl;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Main1809_탑_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1809.txt"));
		Scanner sc = new Scanner(System.in);
		int N= sc.nextInt();
		int map[] = new int[N];
		int ans[] = new int[N];
		for(int i =0; i<N; i++) {
			map[i] = sc.nextInt();
		}
		
		System.out.print(ans[0]+" ");
		for(int i=1; i<N; i++) {
			if(map[i] < map[i-1]) ans[i] = i;
			else ans[i] = ans[i-1];
			System.out.print(ans[i]+" ");
		}
		
	}
}