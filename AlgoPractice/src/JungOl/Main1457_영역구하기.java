package JungOl;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;
class Main1457_영역구하기{
	public static int[][] map;
	public static int[][] v;
	public static Stack<int[]> s;
	public static int[] di = {-1,1,0,0};
	public static int[] dj = {0,0,-1,1};
	public static int N,M;
	public static ArrayList<Integer> ans;
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1457.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		// 변수들 초기화
		s = new Stack<>();
		String[] ss = br.readLine().split(" ");
		N= Integer.parseInt(ss[0]);
		M= Integer.parseInt(ss[1]);
		int numR = Integer.parseInt(ss[2]);
		map = new int[N][M];
		v = new int[N][M];
		ans = new ArrayList<>();
		
		// 사각형들 집어 넣기
		for(int i =0; i<numR; i++) {
			ss = br.readLine().split(" ");
			for(int k=Integer.parseInt(ss[1]); k<Integer.parseInt(ss[3]); k++) {
				for(int j=Integer.parseInt(ss[0]); j<Integer.parseInt(ss[2]); j++) {
					if(map[k][j] == -1) continue;
					map[k][j] = -1;
				}
			}
		}
		
		// DFS로 검색하기
		for(int i=0; i<N; i++) {
			for (int j=0; j<M; j++) {
				if(v[i][j] ==0 && map[i][j] == 0) {
					dfs(i,j);
				}
			}
		}
		
		// 정렬하기
		Collections.sort(ans);
		
		// 출력하기
		System.out.println(ans.size());
		for(int i=0; i<ans.size(); i++) {
			System.out.print(ans.get(i)+" ");
		}
		br.close();
	}
	
	private static void dfs(int i, int j) {
		int count =1;
		s.push(new int[] {i,j});
		v[i][j] = 1;
		
		while(!s.isEmpty()) {
			int[] curr =s.pop();
			for(int d =0; d<4; d++) {
				int ii= curr[0] + di[d];
				int jj= curr[1] + dj[d];
				if(ii >= N || ii < 0 || jj >= M || jj < 0) continue;
				
				if(v[ii][jj] == 0 && map[ii][jj] ==0) {
					count++;
					v[ii][jj] = count;
					s.push(new int[] {ii,jj});
				}
			}
		}
		
		ans.add(count);
	}
}