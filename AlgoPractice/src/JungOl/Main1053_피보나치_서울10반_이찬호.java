package JungOl;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 주서진 정수 n에 대해 피보나시 수열 Fn의 마지막 4자리 수를 구하기
 * 조건
 * 	- 피보나치 공식을 이용하자
 * 입력
 *  - 여러개 tc를 포함할 수 있다.
 *  - 각 tc는 한줄에 n( 0<= n <= 1,000,000,000)을 포함한다.
 *  - 입력의 끝은 -1을 포함한 한 줄로이어짐.
 * 출력
 * 	- Fn의 마지막 4자리수를 출력 (Fn을 10,000로 mod한 나머지를 출력)
 *  - 네자리가 모두 0이라면 '0'출력
 *  - 그렇지 않으면 leading zero는 제거한다.
 * 풀이 
 * 	- 1000000은 (500000)^2 ,
 * */
class Main1053_피보나치_서울10반_이찬호{
	public static int[][] h= {{1,1}, {1,0}};
	public static int[][] ans;
	public static ArrayList<int[][]> a;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1053.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while(true) {
			int N= Integer.parseInt(br.readLine());
			if(N == -1) break;
			if(N == 0) { System.out.println(0); continue;}
			ans = new int[][]{ {1,1}, {1,0} };
			
			a = new ArrayList<>();
			while(true) {
				if(N==1) break;
				if(N%2==0) {
					N /= 2;
					ans = hang(ans, ans);
					ans = modhang(ans);
				}else {
					N--;
					a.add(ans);
				}
			}
			
			for(int i=0; i<a.size(); i++) {
				ans = hang(a.get(i), ans);
				ans = modhang(ans);
			}
			
			System.out.println(ans[0][1]);
		}
		br.close();
	}
	
	private static int[][] modhang(int[][] h){
		int[][] temp = new int[2][2];
		temp[0][0] = h[0][0] % 10000;
		temp[0][1] = h[0][1] % 10000;
		temp[1][0] = h[1][0] % 10000;
		temp[1][1] = h[1][1] % 10000;
		return temp;
	}
	
	private static int[][] hang(int[][] h, int[][] h2) {
		int[][] temp = new int[2][2];
		temp[0][0] = h[0][0] % 10000 * h2[0][0] % 10000 + h[0][1] % 10000 * h2[1][0] % 10000;
		temp[0][1] = h[0][0] % 10000 * h2[0][1] % 10000 + h[0][1] % 10000 * h2[1][1] % 10000;
		temp[1][0] = h[1][0] % 10000 * h2[0][0] % 10000 + h[1][1] % 10000 * h2[1][0] % 10000;
		temp[1][1] = h[1][0] % 10000 * h2[0][1] % 10000 + h[1][1] % 10000 * h2[1][1] % 10000;
		return temp;
	}
}