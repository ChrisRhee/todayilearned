package JungOl;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
class Main1106_장기{
	public static int [][]map;
	public static int[][] v;
	public static int di[]= {2,1,-1,-2,-2,-1,1,2};
	public static int dj[]= {1,2,2,1,-1,-2,-2,-1};
	public static int N,M;
	public static Queue<int[]> q;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1106.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
		String s[]= br.readLine().split(" ");
		N= Integer.parseInt(s[0]);
		M= Integer.parseInt(s[1]);
		map = new int [N+1] [M+1];
		q = new LinkedList<>();
		s= br.readLine().split(" ");
		int holi = Integer.parseInt(s[0]);
		int holj = Integer.parseInt(s[1]);
		int joli = Integer.parseInt(s[2]);
		int jolj = Integer.parseInt(s[3]);
		bfs(holi,holj,joli,jolj);
		System.out.println(v[joli][jolj]);
		br.close();
	}
	private static void bfs(int i, int j, int joli, int jolj) {
		v = new int[N][M];
		v[i][j] = 0;
		q.offer(new int[] {i,j});
		while(!q.isEmpty()) {
			int curr[] = q.poll();
			for(int d=0; d<di.length; d++) {
				int ii = curr[0] + di[d];
				int jj = curr[1] + dj[d];
				
				if (ii >= N || ii < 1 || jj >= M || jj < 1)
					continue;
				/*for (int[] k : v) {
					System.out.println(Arrays.toString(k));
				}
				System.out.println();*/
				if(v[ii][jj] == 0) {
					q.offer(new int[] {ii,jj});
					v[ii][jj] = v[curr[0]][curr[1]]+1;
					if(ii == joli && jj == jolj) return;
				}
			}
		}
		
	}
}