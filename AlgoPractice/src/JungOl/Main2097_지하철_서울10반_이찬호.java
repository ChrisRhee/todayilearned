package JungOl;
import java.io.*;
import java.util.Arrays;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 최단거리 구하기
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 플로이드 워샬 공식으로 방문을 한다.
 *  - 출력을 할때는 출발-도착을 체크해서, 해당 값이 있으면,
 *  - 출력 -해당값, 해당값- 도착 을 또 검사한다.
 *  - 해당값이 0이면 그냥 그 값이 제일 최소이므로 더이상 검사를 안하고, 0보다 크면 위를 반복한다.
 *  - 그러고 출력한다.
 * */
class Main2097_지하철_서울10반_이찬호{
	public static int[][] map,path;
	public static int N;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main2097.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		N= Integer.parseInt(s[0]);
		int D= Integer.parseInt(s[1]);
		path = new int [N+1][N+1];
		map = new int[N+1][N+1];
		for(int i=1; i<=N; i++) {
			s = br.readLine().split(" ");
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(s[j-1]);
			}
		}
		
		for(int k=1; k<=N; k++) {
			for (int i=1 ; i<=N; i++) {
				for(int j=1; j<=N; j++) {
					if(map[i][j] > map[i][k]+map[k][j]) {
						map[i][j] = map[i][k]+map[k][j];
						path[i][j]= k;
					}
				}
			}
		}
		
//		for (int[] is : map) {
//			System.out.println(Arrays.toString(is));
//		}
//		System.out.println();
//		for (int[] is : path) {
//			System.out.println(Arrays.toString(is));
//		}
		
		System.out.println(map[1][D]);
		System.out.print(1 +" ");
		check(1,D);
		System.out.print(D);
		
		br.close();
	}
	
	private static void check(int i,int j) {
		if(path[i][j]==0) return;
		int temp = path[i][j];
		check(i, temp);
		System.out.print(path[i][j]+" ");
		check(temp, j);
	}
}