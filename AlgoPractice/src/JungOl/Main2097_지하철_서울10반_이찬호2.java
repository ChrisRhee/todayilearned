package JungOl;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 최단거리 구하기
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 플로이드 워샬 공식으로 방문을 한다.
 *  - 출력을 할때는 출발-도착을 체크해서, 해당 값이 있으면,
 *  - 출력 -해당값, 해당값- 도착 을 또 검사한다.
 *  - 해당값이 0이면 그냥 그 값이 제일 최소이므로 더이상 검사를 안하고, 0보다 크면 위를 반복한다.
 *  - 그러고 출력한다.
 * */
class Main2097_지하철_서울10반_이찬호2{
	static int N, M,ans;
	static String trace;
	static int[][] map;
	static boolean[] visited;

	public static void main(String[] args) throws FileNotFoundException {
		System.setIn(new FileInputStream("res/main2097.txt"));
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt(); //역갯수
		M = sc.nextInt(); //도착역
		map = new int[N+1][N+1];
		visited = new boolean[N+1];
		ans = Integer.MAX_VALUE;
		for (int i = 1; i <= N; i++) {
			for (int j = 1; j <= N; j++) {
				map[i][j] = sc.nextInt();
			}
		}
		for(int[] a : map)System.out.println(Arrays.toString(a));
		//for(int i=1; i<=N; i++)
		dfs(0,0,map[0][0],"1"); //지금,다음,누적거리
		System.out.println(ans);
		System.out.println(trace);
	}

	private static void dfs(int here, int to, int dist, String pass) {
		if(ans<dist) return;
		if(to == M) {
			if(dist<ans) {
				ans = dist;
				trace = pass+" "+(to+1);
			}
			return;
		}
		
		for(int i=1; i<=N; i++) {
			if (i==to || visited[i]==true) continue;
			visited[i]=true;
			dfs(to,i,dist+map[to][i],pass+" "+(to+1));
			visited[i]=false;
		}
	}

}