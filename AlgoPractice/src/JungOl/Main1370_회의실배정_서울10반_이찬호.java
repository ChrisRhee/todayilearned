package JungOl;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
class Main1370_회의실배정_서울10반_이찬호{
	
		
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1370.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int numMeeting= Integer.parseInt(br.readLine());
		List<String[]> mts = new ArrayList<String[]>();
		List<String[]> ans = new ArrayList<String[]>();
		for(int i = 0; i < numMeeting; i++)	{
			String s[] = br.readLine().split(" ");
			mts.add(s);
		}
		
		// 끝나는 시간 순서로 정렬
		for(int i=0; i< mts.size(); i++) {
			for(int j=0; j<mts.size(); j++) {
				if(i==j) continue;
				if( Integer.parseInt(mts.get(i)[2]) < Integer.parseInt(mts.get(j)[2])) {
					String t[] = mts.get(i);
					mts.set(i, mts.get(j));
					mts.set(j, t);
				}
			}
		}
		
		ans.add(mts.get(0));
		for(int i=0; i< mts.size(); i++) {
			for(int j=0; j<mts.size(); j++) {
				if(i==j) continue;
				if(Integer.parseInt(mts.get(i)[2]) > Integer.parseInt(mts.get(j)[1])) {
					continue;
				}
				else {
					ans.add(mts.get(j));
					i=j;
				}
			}
		}
		
		System.out.println(ans.size());
		for (String[] strings : ans) {
			System.out.print(strings[0]+" ");
		}
		
		br.close();
	}

	
}