package JungOl;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
class Main2613_토마토고_서울10반_이찬호{
	public static int N,M,max;
	public static int di[] = {-1,1,0,0};
	public static int dj[] = {0,0,-1,1};
	public static Queue<int[]> q;
	public static int[][] v;
	public static int[][] map;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main2613.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		// 입력받기
		String s[] = br.readLine().split(" ");
		N = Integer.parseInt(s[1]);
		M = Integer.parseInt(s[0]);
		// 초기화
		map = new int[N][M];
		q= new LinkedList<>();
		v= new int[N][M];
		max = 0;
		// map 배치
		for(int i=0;i<N; i++) {
			s = br.readLine().split(" ");
			for(int j=0 ; j<M; j++) {
				map[i][j] = Integer.parseInt(s[j]);
			}
		}
		
		// 익은 토마토(1)이면 탐색 시작
		for(int i=0;i<N; i++) {
			for(int j=0 ; j<M; j++) {
				if(map[i][j] == 1) {
					q.offer(new int[] {i,j});
					v[i][j] = 0;
				}
			}
		}
		bfs();
		
		for(int i=0;i<N; i++) {
			for(int j=0 ; j<M; j++) {
				if(v[i][j]== 0 && map[i][j] ==0) max = -1;
			}
		}
		
		System.out.println(max);
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
		br.close();
	}
	private static void bfs() {
		while(!q.isEmpty()) {
			int curr[] = q.poll();
			for(int d=0; d<4; d++) {
				int ii= curr[0] + di[d];
				int jj= curr[1] + dj[d];
				if( ii < 0 || ii >= N || jj < 0 || jj >= M) continue;
				if(v[ii][jj] == 0 && map[ii][jj] == 0) {
					v[ii][jj] = v[curr[0]][curr[1]]+1;
					if(max < v[ii][jj]) max = v[ii][jj];
					q.offer(new int[] {ii,jj});
				}
			}
		}
	}
}