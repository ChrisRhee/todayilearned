package JungOl;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 선물의 집을 빠져나올 때 가장 많은 선물의 개수를 구하기
 * 조건
 * 	- 한번 지나간 길은 다시 돌아가지 못한다.
 *  - 출구로 나가는 동안 찾을 수 있는 선물은 가져가도 된다.
 *  - 가장 많은 선물 개수
 * 입력
 *  - 집 한별의 길이 N
 *  - 내부 평면도, 0=길, 1=벽, 2=선물
 *  - 왼쪽 맨 위 0,0이 입구, 오른쪽 맨 아래 (N-1,N-1) 출구 
 * 출력
 * 	- 선물의 집을 빠져나올 때 가장 많은 선물의 개수를 출력
 * 풀이 
 * 	- DFS로 (0,0)에서 (N-1,N-1)까지 간다.
 *  - 되돌아가지 않는 상황에서, 쭉 가면서 최고로 선물을 많이 가져온 경우를 구한다.
 *  - 방문한 곳에 +10, 나오면서 -10 해줘서 다시 재방문 할 수 있게 한다.
 * */
class Main1230_선물의집_서울10반_이찬호{
	public static int N,map[][],ans;
	public static int di[] = {-1,1,0,0};
	public static int dj[] = {0,0,-1,1};
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1230.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		// 초기화
		N= Integer.parseInt(br.readLine());
		map = new int[N][N];
		ans = 0;
		for(int i=0; i<N; i++) {
			String s[] = br.readLine().split(" ");
			for (int j=0; j<N; j++) {
				map[i][j] = Integer.parseInt(s[j]);
			}
		}
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */

		map[0][0] = map[0][0] + 10;
		if(map[0][0] == 10) {
			dfs(0,0,0);
		}
		else {
			dfs(0,0,1);
		}
		System.out.println(ans);
		br.close();
	}

	private static void dfs(int i, int j, int count) {
		for (int d = 0; d < 4; d++) {
			int ii = i + di[d];
			int jj = j + dj[d];
			if (ii < 0 || ii >= N || jj < 0 || jj >= N) continue;
			if (map[ii][jj] == 1) continue;
			// 집도착
			if (ii == N - 1 && jj == N - 1) {
				if (map[ii][jj] == 2) count++;
				if(ans < count) ans = count;
				return;
			}
			
			if (map[ii][jj] == 0 || map[ii][jj] == 2) {
				map[ii][jj] = map[ii][jj] + 10;
				if(map[ii][jj] == 12) 
					dfs(ii, jj, count+1);
				else 
					dfs(ii, jj, count);
				map[ii][jj] = map[ii][jj] - 10;
			}
		}
	}
}