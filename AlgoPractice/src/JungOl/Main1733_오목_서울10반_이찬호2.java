package JungOl;
import java.io.*;
import java.util.Arrays;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Main1733_오목_서울10반_이찬호2{
	public static int[] di= {-1,1, 0,0, -1,1, -1,1};
	public static int[] dj= {0,0, -1,1, -1,1, 1,-1};
	public static int[][] map;
	public static int[] currdir;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1733.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		map = new int[20][20];
		for(int i=1; i<=19; i++) {
			String s[] = br.readLine().split(" ");
			for(int j=1; j<=19; j++) {
				map[i][j] = Integer.parseInt(s[j-1]);
			}
		}
		int win = 0;
		currdir = new int[3];
		loop:for(int i=1; i<=19; i++) {
			for(int j=1; j<=19; j++) {
				if(map[i][j]==0) continue;
				if(!check(map[i][j],i,j)) continue;
				if(map[i][j] == 1 ) {
					win = 1;
					break loop;
				}
				if(map[i][j] == 2 ) {
					win = 2;
					break loop;
				}
			}
		}
		
		if(currdir[2] >0) checkdir();
		
		System.out.println(win);
		if(win > 0) System.out.println(currdir[0]+" "+currdir[1]);
		
		br.close();
	}
	
	private static void checkdir() {
		int ii = currdir[0];
		int jj = currdir[1];
		int d = currdir[2];
		if( d == 1) d =0;
		
		while(true) {
			ii += di[d];
			jj += dj[d];
			if(ii <1 || ii > 19 || jj <1 || jj > 19 || map[ii][jj] ==0) {
				ii -= di[d];
				jj -= dj[d];
				break;
			}
		}
		currdir[0] = ii;
		currdir[1] = jj;
	}

	private static boolean check(int doll, int i, int j) {
		boolean result = false;
		int temp = doll;
		int count = 0;
		int dir[] = new int[8];
		for(int d= 0; d<8; d++) {
			int ii = i;
			int jj = j;
			while(true) {
				ii += di[d];
				jj += dj[d];
				if(ii <1 || ii > 19 || jj <1 || jj > 19 || map[ii][jj] != temp) break;
				if(map[ii][jj]==temp) {
					count ++;
				}
			}
			dir[d]= count;
			count =0;
		}
		
		currdir[0] = i;
		currdir[1] = j;
		if(dir[0]+dir[1] == 4){
			result = true;
			if(dir[0]>0) currdir[2] = 1;
		}else if(dir[2]+dir[3] == 4 ) { 
			result = true;
			if(dir[2]>0) currdir[2] = 2;
		}else if(dir[4]+dir[5] == 4 ) { 
			result = true;
			if(dir[4]>0) currdir[2] = 4;
		}else if(dir[6]+dir[7] == 4 ) { 
			result = true;
			if(dir[7]>0) currdir[2] = 7;
		}
		return result;
	}
}