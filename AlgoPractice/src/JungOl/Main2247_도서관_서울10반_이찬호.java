package JungOl;
import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 도서관 이용시간을 구하자
 * 조건
 * 	- 적어도 한명 이상의 학생이 머물었던 가장 긴 시간
 *  - 전체 시간중 학생이 한명도 머물지 않았던 시간 구하기
 * 입력
 *  - 학생 수
 *  - 들어온시간 S, 나간시간 E
 * 출력
 * 	- 한명이상이 머물었떤 가장 긴 시간과 머물지 않았던 가장 긴시간 출력
 * 풀이 
 * 	- 0~24까지의 배열을 만들고 다 더해서 출력하면 될거같은데..?
 *  - 끝 값을 연결되는 동안 이어준다!
 * */
class Main2247_도서관_서울10반_이찬호{
	public static int  N;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main2247.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N= Integer.parseInt(br.readLine());
		int val[][] = new int [N][2];
		for(int i=0; i<N; i++) {
			String s[] = br.readLine().split(" ");
			int ss = Integer.parseInt(s[0]);
			int ee = Integer.parseInt(s[1]);
			val[i][0] = ss;
			val[i][1] = ee;
		}
		Arrays.sort(val, new Comparator<int[]>() {
			@Override
			public int compare(int[] o1, int[] o2) {
				if(o1[0]-o2[0] == 0) return o2[1]-o1[1];
				return o1[0]-o2[0];
			}
		});
		int longTerm = 0;
		int zeroTerm = 0;
		int start = val[0][0];
		int end = val[0][1];
		
		for(int i=1; i<N; i++) {
			if(val[i][0] <= end) end = Math.max(end, val[i][1]);
			else {
				longTerm = Math.max(end-start, longTerm);
				zeroTerm = Math.max(val[i][0]  - end, zeroTerm); 
				start = val[i][0];
				end = val[i][1];
			}
		}
		longTerm = Math.max(end-start, longTerm);
		System.out.println(longTerm+" "+zeroTerm);
		br.close();
	}
}