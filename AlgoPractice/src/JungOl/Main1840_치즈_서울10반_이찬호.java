package JungOl;
import java.io.*;
import java.util.*;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 치즈 구하기
 * 조건
 * 	- 공기와 접촉된 칸은 한시간이 지나면 녹아서 사라진다.
 *  - 치즈로 둘러쌓여있는 구멍은 공기가 아니므로 여기와 닿은 곳은 안 사라진다.
 *  - 판의 가장자리는 치즈가 놓일 수 없다. 
 *  - 이 때 치즈가 모두 녹아 없어지는데 걸리는 시간과 녹기전 남아있는 치즈조각 칸의 개수구하기
 * 입력
 *  - 사각형 모양 판의 세로와 가로 길이가 주어진다.
 *  - 최대길이는 100. 
 *  - 판 내용이 주어진다.
 *  - 치즈가 없는칸 0, 치즈가 있는칸 1
 * 출력
 * 	- 모두 녹아서 없어지는 데 걸리는 시간 출력
 *  - 녹기 1시간 전에 남아있는 치즈조각 칸 출력 
 * 풀이 
 * 	- 가장자리엔 치즈가 놓이지 않는다. 고로 그 가장자리에서 출발하는 BFS를 실행한다. 가장자리와 연결된 0은 공기이다. 
 *  - 치즈가 다 없어질 때까지 가장자리(ex 0,0)에서 BFS를 시작한다.
 *  - 공기와 치즈의 구멍을 구분하는 키가 저거인듯
 * */
class Main1840_치즈_서울10반_이찬호{
	public static int N,M,map[][];
	public static int[] di= {-1,1,0,0};
	public static int[] dj= {0,0,-1,1};
	public static Queue<int[]> q;
	public static boolean[][] v;
	public static ArrayList<Integer> ans;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1840.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		N = Integer.parseInt(s[0]);
		M = Integer.parseInt(s[1]);
		map = new int[N][M];
		ans = new ArrayList<>();
		q = new LinkedList<>();
		for(int i=0; i<N; i++) {
			s= br.readLine().split(" ");
			for(int j=0; j<M; j++) {
				map[i][j] = Integer.parseInt(s[j]);
			}
		}
		while(true) {
			bfs(0,0);
			if(ans.get(ans.size()-1) == 0) break;
		}
		
		System.out.println(ans.size()-1);
		if(ans.size()-2 < 0) System.out.println(0);
		else System.out.println(ans.get(ans.size()-2));
		br.close();
	}

	private static void bfs(int i, int j) {
		v = new boolean[N][M];
		int cheese =0;
		v[i][j] = true;
		q.offer(new int[] {i,j});
		while(!q.isEmpty()) {
			int[] curr= q.poll();
			for(int d =0; d<di.length; d++) {
				int ii= curr[0]+di[d];
				int jj= curr[1]+dj[d];
				if(ii < 0 || ii >= N || jj < 0 || jj >= M ) continue;
				if(v[ii][jj] == false) {
					v[ii][jj] = true;
					if(map[ii][jj] == 1) {
						map[ii][jj] = 0;
						cheese++;
					}else {
						q.offer(new int[] {ii,jj});
					}
				}
				
			}
		}
		ans.add(cheese);
	}
}