package JungOl;
import java.io.*;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 오목 승리하기
 * 조건
 * 	- 
 * 입력
 *  - 오목판
 * 출력
 * 	- 이긴 돌 색, 오목 완성된 돌들의 가장 왼쪽 or 윗쪽 위치
 * 풀이 
 * - 왼쪽위에서 세로로 내려오면서 체크를 한다. 
 * - 돌 1or2가 발견되면 그 돌들을 8방향으로 체크해준다. 내 상하, 좌우, 대각선의 합이 4인 경우만 5목 성공이다.
 * - solve함수 안에서 dir 배열을 가지고 내 8방향의 정보를 저장해두자.
 * - solve함수안에 currdir 배열에 현재 위치를 저장해둔다. 오목이 성공하면 이 아이들로 위치를 출력한다.
 * - 왼쪽 위에서 세로로 내려오면서 탐색하므로 오목인 경우 무조건 맨 위거나, 맨 왼쪽이다.
 * */
class Main1733_오목_서울10반_이찬호{
	public static int[] di= {-1,1, 0,0, -1,1, -1,1};
	public static int[] dj= {0,0, -1,1, -1,1, 1,-1};
	public static int[][] map;
	public static int[] currdir;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1733.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		map = new int[20][20];
		for(int i=1; i<=19; i++) {
			String s[] = br.readLine().split(" ");
			for(int j=1; j<=19; j++) map[i][j] = Integer.parseInt(s[j-1]);
		}
		int win = 0;
		currdir = new int[2];
		loop:for(int j=1; j<=19; j++) {
			for(int i=1; i<=19; i++) {
				if(map[i][j]==0) continue;
				if(!check(map[i][j],i,j)) continue;
				currdir[0] = i;
				currdir[1] = j;
				if(map[i][j] == 1 ) {
					win = 1;
					break loop;
				}
				if(map[i][j] == 2 ) {
					win = 2;
					break loop;
				}
			}
		}
		System.out.println(win);
		if(win > 0) System.out.println(currdir[0]+" "+currdir[1]);
		br.close();
	}

	private static boolean check(int doll, int i, int j) {
		boolean result = false;
		int dir[] = new int[8];
		int count = 0;
		for(int d= 0; d<8; d++) {
			int ii = i;
			int jj = j;
			while(true) {
				ii += di[d];
				jj += dj[d];
				if(ii <1 || ii > 19 || jj <1 || jj > 19 || map[ii][jj] != doll) break;
				count ++;
			}
			dir[d]= count;
			count =0;
		}
		if(dir[0]+dir[1] == 4 || dir[2]+dir[3] == 4 || dir[4]+dir[5] == 4 || dir[6]+dir[7] == 4) result = true;
		return result;
	}
}