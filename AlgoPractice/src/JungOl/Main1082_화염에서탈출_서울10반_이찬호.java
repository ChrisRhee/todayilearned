package JungOl;
import java.io.*;
import java.util.*;
class Main1082_화염에서탈출_서울10반_이찬호{
	public static Queue<int[]> q;
	public static int di[] = {-1,1,0,0};
	public static int dj[] = {0,0,-1,1};
	public static char m[][] ;
	public static int visit[][],I,J,ans;
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1082.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
		String s[] = br.readLine().split(" ");
		ans = 0;
		q = new LinkedList<>();
		I = Integer.parseInt(s[0]);
		J = Integer.parseInt(s[1]);
		m = new char[I][J];
		visit = new int[I][J];
		
		for(int i=0; i<I; i++) {
			String ss = br.readLine();
			for(int j=0; j<J; j++) {
				m[i][j] = ss.charAt(j);
			}
		}
		
		// 사람먼저 큐에 넣기
		for(int i=0; i<I; i++) {
			for(int j=0; j<J; j++) {
				if(m[i][j]=='S') {
					q.offer(new int[] {i,j});
				}
			}
		}
		
		// 불 넣기
		for(int i=0; i<I; i++) {
			for(int j=0; j<J; j++) {
				if(m[i][j]=='*') {
					q.offer(new int[] {i,j});
				}
			}
		}
		bfs ();
		br.close();
	}
	
	public static void bfs() {
		while(!q.isEmpty()) {   // isEmpty()
			int [] curr = q.poll(); // dequeue()
			for(int d=0; d<4; d++) {
				int ii = curr[0] + di[d];
				int jj = curr[1] + dj[d];
				if( ii < 0 || ii >= I || jj < 0 || jj >= J) continue;
				
				// 불이면 다음칸에 불 붙임
				if(m[curr[0]][curr[1]] == '*' && (m[ii][jj] == '.'||m[ii][jj] == 'S')) {
					m[ii][jj]= '*';
					q.offer(new int[] {ii,jj});
				}
				
				// 사람이면 땅일때만 이동
				else if(m[curr[0]][curr[1]] == 'S'&& (m[ii][jj] == '.'||m[ii][jj] == 'D' )) {
					visit[ii][jj] = visit[curr[0]][curr[1]] +1; 
					if(m[ii][jj] =='D') { 
						System.out.println(visit[ii][jj]); 
						return;
					}
					m[ii][jj]= 'S';
					q.offer(new int[] {ii,jj});
				}
			}
		}
		System.out.println("impossible"); 
	}
}