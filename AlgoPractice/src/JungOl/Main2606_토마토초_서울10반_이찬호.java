package JungOl;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
class Main2606_토마토초_서울10반_이찬호{
	public static int N,M,H;
	public static int di[] = {-1,1,0,0,0,0};
	public static int dj[] = {0,0,-1,1,0,0};
	public static int dh[] = {0,0,0,0,-1,1};
	public static Queue<int[]> q;
	public static int[][][] v;
	public static int[][][] map;
	public static int max;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main2606.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		// 입력받기
		String s[] = br.readLine().split(" ");
		N = Integer.parseInt(s[1]);
		M = Integer.parseInt(s[0]);
		H = Integer.parseInt(s[2]);
		// 초기화
		map = new int[N][M][H];
		q= new LinkedList<>();
		v= new int[N][M][H];
		max = 0;
		
		// map 배치
		for (int h = 0; h < H; h++) {
			for (int i = 0; i < N; i++) {
				s = br.readLine().split(" ");
				for (int j = 0; j < M; j++) {
					map[i][j][h] = Integer.parseInt(s[j]);
				}
			}
		}
		
		// 익은 토마토(1)이면 탐색 시작
		for (int h = 0; h < H; h++) {
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < M; j++) {
					if (map[i][j][h] == 1) {
						q.offer(new int[] { i, j, h});
						v[i][j][h] = 0;
					}
				}
			}
		}
		
		bfs();
		
		for (int h = 0; h < H; h++) {
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < M; j++) {
					if (v[i][j][h] == 0 && map[i][j][h] == 0)
						max = -1;
				}
			}
		}
		
		System.out.println(max);
		br.close();
	}
	
	private static void bfs() {
		while(!q.isEmpty()) {
			int curr[] = q.poll();
			for(int d=0; d<di.length; d++) {
				int ii= curr[0] + di[d];
				int jj= curr[1] + dj[d];
				int hh= curr[2] + dh[d];
				if( ii < 0 || ii >= N || jj < 0 || jj >= M || hh < 0 || hh >= H ) 
					continue;
				if(map[ii][jj][hh]== -1) continue;
				
				if(v[ii][jj][hh] == 0 && map[ii][jj][hh] == 0) {
					v[ii][jj][hh] = v[curr[0]][curr[1]][curr[2]]+1;
					if(max < v[ii][jj][hh])
						max = v[ii][jj][hh];
					q.offer(new int[] {ii,jj,hh});
				}
			}
		}
	}
}