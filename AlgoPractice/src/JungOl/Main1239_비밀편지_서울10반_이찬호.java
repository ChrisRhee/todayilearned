package JungOl;
import java.io.*;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 1글자 달라도 그 문자로 이해 가능하다
 *  - 못 읽는 글자가 나오면 그 위치를 출력한다.
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 문자를 처음부터 체크한다. 체크하면서 다른게 나오면 count를 증가시키는데, 2보다 작으면 해당 문자를 리턴한다.
 *  - 다 돌았는데 못찾았으면 NO를 리턴한다. NO가 나오면 바로 답을 index로 바꿔주고 프로그램을 종료후 출력한다.
 * */
class Main1239_비밀편지_서울10반_이찬호{
	public static String[] alpha= {"0","0","0","0","0","0", //A
								"0","0","1","1","1","1", //B
								"0","1","0","0","1","1", //C
								"0","1","1","1","0","0", //D
								"1","0","0","1","1","0", //E
								"1","0","1","0","0","1", //F
								"1","1","0","1","0","1", //G
								"1","1","1","0","1","0"  //H
	};
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1239.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(br.readLine());
		String s[] = br.readLine().split("");
		String Ans= "";
		for(int i=0; i< T; i++) {
			String temp = check(s[i*6],s[i*6+1],s[i*6+2],s[i*6+3],s[i*6+4],s[i*6+5]);
			if(temp.equals("NO")) {
				Ans = ""+(i+1);
				break;
			}else
				Ans += temp;
		}
		System.out.println(Ans);
		br.close();
	}
	
	private static String check(String s1, String s2, String s3, String s4, String s5, String s6) {
		int count = 0;
		for(int i=0; i<8; i++) {
			count = 0;
			if(!alpha[i*6].equals(s1)) count++;
			if(!alpha[i*6+1].equals(s2)) count++;
			if(!alpha[i*6+2].equals(s3)) count++;
			if(!alpha[i*6+3].equals(s4)) count++;
			if(!alpha[i*6+4].equals(s5)) count++;
			if(!alpha[i*6+5].equals(s6)) count++;
			if(i==0 && count <2) return "A";
			else if(i==1 && count <2) return "B";
			else if(i==2 && count <2) return "C";
			else if(i==3 && count <2) return "D";
			else if(i==4 && count <2) return "E";
			else if(i==5 && count <2) return "F";
			else if(i==6 && count <2) return "G";
			else if(i==7 && count <2) return "H";
		}
		return "NO";
	}
}