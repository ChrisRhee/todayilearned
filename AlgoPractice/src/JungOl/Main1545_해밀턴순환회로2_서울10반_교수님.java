package JungOl;
import java.io.*;
import java.util.*;

public class Main1545_해밀턴순환회로2_서울10반_교수님 {
	public static int INF=987654321;//Integer.MAX_VALUE/2;
	public static int N;
	public static int[][] g;
	public static int[][] m;
	
	public static int solve(int pos, int visited, int start) {
		if(m[pos][visited]!=0) return m[pos][visited];
		if(visited==(1<<N)-1) return m[pos][visited]=g[pos][start];
		
		int ret=INF;
		for(int next=0; next<N; next++) {
			if((visited&(1<<next))==0 && g[pos][next]>0){
				int tmp=g[pos][next]+solve(next,visited|(1<<next),start);
				if(tmp<ret) ret=tmp;
			}
		}
		m[pos][visited]=ret;
//		for(int[] a:m) System.out.println(Arrays.toString(a));
//		System.out.println();
		return ret;
	}
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/main1545.txt"));
		Scanner sc=new Scanner(System.in);
		//int T=sc.nextInt();
		
		//for(int tc=1; tc<=T; tc++) {
			N=sc.nextInt();
			g=new int[N][N];
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					g[i][j]=sc.nextInt();
					if(i!=j && g[i][j]==0 ) g[i][j] = INF;
				}
			}
			
//			int ans=INF;
//			for(int i=0; i<N; i++) {
				m=new int[N][1<<N];
//				int tmp=solve(i,1<<i,i);
//				if(ans>tmp) ans=tmp;
//			}
			
			//System.out.println("#"+tc+" "+ans);
			System.out.println(solve(0,1<<0,0));
		//}
		sc.close();
	}
}
