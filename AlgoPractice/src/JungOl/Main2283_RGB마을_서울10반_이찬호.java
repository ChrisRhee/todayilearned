package JungOl;
import java.io.*;
class Main2283_RGB마을_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main2283.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N= Integer.parseInt(br.readLine());
		int map[][] = new int [N][3];
		int ans = Integer.MAX_VALUE;
		for(int i=0; i<N; i++) {
			String s[] = br.readLine().split(" ");
			for(int j=0; j<3; j++) map[i][j] = Integer.parseInt(s[j]);
			
			if(i == 0 ) continue;
			map[i][0] += Math.min(map[i-1][1], map[i-1][2]);
			map[i][1] += Math.min(map[i-1][0], map[i-1][2]);
			map[i][2] += Math.min(map[i-1][0], map[i-1][1]);
		}
		
		for(int i=0; i<3; i++) 
			if(ans > map[N-1][i]) ans = map[N-1][i];
		
		System.out.println(ans);
		br.close();
	}
}