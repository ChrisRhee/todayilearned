package JungOl;
import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
class Main1077_배낭채우기_서울10반_이찬호{
	public static int N, W, map[][], max, dp[];
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1077.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		N = Integer.parseInt(s[0]);
		W = Integer.parseInt(s[1]);
		max = 0;
		map = new int [N][2];
		dp = new int [W+1];
		
		for (int i=0; i<N; i++) {
			s = br.readLine().split(" ");
			map[i][0] = Integer.parseInt(s[0]);
			map[i][1] = Integer.parseInt(s[1]);
		}
		
		Arrays.sort(map, new Comparator<int[]>() {
			@Override
			public int compare(int[] o1, int[] o2) {
				return o1[0]-o2[0];
			}
		});
		
		for(int i=0; i<N; i++) {
			for(int j=0; j<=W; j++) {
				int quotient = j / map[i][0];
				int res = j % map[i][0];
				if(dp[j] < quotient*map[i][1]+dp[res])
					dp[j] = quotient*map[i][1]+dp[res];
			}
			//System.out.println(Arrays.toString(dp));
		}
		
		System.out.println(dp[W]);
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
		br.close();
	}

}