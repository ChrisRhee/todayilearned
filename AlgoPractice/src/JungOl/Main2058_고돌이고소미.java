package JungOl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

class Main2058_고돌이고소미 {
	public static Queue<int[]> q;
	public static int[][] visit;
	public static int[] di = {-1,-1, 0, 1, 1, 1, 0,-1};
	public static int[] dj = { 0, 1, 1, 1, 0,-1,-1,-1};
	public static int N;
	public static String[][] map;
	public static int arrived = 0;
	public static void main(String args[]) throws Exception {
		System.setIn(new FileInputStream("res/main2058.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		N = Integer.parseInt(br.readLine());
		String s[] = br.readLine().split(" ");
		String s2[] = br.readLine().split(" ");
		q = new LinkedList<>();
		visit = new int[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				visit[i][j] = N*N+1;
			}
		}
		int godol[] = new int[s.length];
		int gosun[] = new int[s2.length];
		map = new String[N][N];
		for (int i = 0; i < s.length; i++) {
			godol[i] = Integer.parseInt(s[i])-1;
			gosun[i] = Integer.parseInt(s2[i])-1;
		}

		for (int i = 0; i < N; i++) {
			s = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				map[i][j] = s[j];
			}
		}
		/*for (int[] is : visit) {
			System.out.println(Arrays.toString(is));
		}*/
		System.out.println("고돌");
		bfs(godol[0], godol[1], godol[2], godol[3]);
		System.out.println("count="+arrived);
		System.out.println("고순");
		bfs(gosun[0], gosun[1], gosun[2], gosun[3]);
		System.out.println("count="+arrived);
		System.out.println(arrived);
	}

	private static void bfs(int i, int j, int fi, int fj) {
		q.offer(new int[] { i, j });
		visit[i][j] = 1;
		while (!q.isEmpty()) {
			int[] curr = q.poll();
			if(curr[0] == fi && curr[1]==fj) arrived = visit[fi][fj];
			for (int d = 0; d < 8; d++) {
				int ii = curr[0] + di[d];
				int jj = curr[1] + dj[d];
				if (ii < 0 || ii >= N || jj < 0 || jj >= N)
					continue;
				for (int[] is : visit) {
					System.out.println(Arrays.toString(is));
				}
				System.out.println();
				if(map[ii][jj].equals("0") && visit[curr[0]][curr[1]] < visit[ii][jj]) { //for (int i = 0; i < N; i++) {) {
					visit[ii][jj] = visit[curr[0]][curr[1]] + 1;
					q.offer(new int[] {ii,jj});
				}
			}

		}
	}
}