package JungOl;
import java.io.*;
import java.util.Stack;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 비가 왔을 때 최대 안전 지역을 구하자
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 물에 안잠긴 최대 안전 지역 개수
 * 풀이 
 * 	- 장마가 0에서 건물의 최대 높이 전까지 차례로 온다. 0부터 탐색을 시작하자.
 *  - 장마 높이 때마다 visit배열을 주고 방문한 곳은 패스, 현재 건물이 비가 온 높이와 같거나 작으면 패스,
 *  - DFS를 돌면서 잠기지 않은 건물들을 차례로 방문하면서 방문 처리를 해주고, 다 돌면 count를 증가시킨다.
 *  - 이렇게 장마 높이마다의 잠긴 영역을 체크한 후 그 영역 중 가장 큰 값을 출력한다.
 * */
class Main2298_안전영역_서울10반_이찬호{
	public static int map[][], N, count;
	public static boolean visit[][];
	public static Stack<int[]> s;
	public static int[] di = {-1,1,0,0};
	public static int[] dj = {0,0,-1,1};
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main2298.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N= Integer.parseInt(br.readLine());
		map = new int[N][N];
		s= new Stack<>();
		int max = 0;
		int ans=0;
		for(int i=0; i<N; i++) {
			String s[] = br.readLine().split(" ");
			for(int j=0; j<N; j++) {
				map[i][j] = Integer.parseInt(s[j]);
				if(map[i][j] > max) max = map[i][j]; 
			}
		}
		
		for(int k=1; k<max; k++) {
			visit = new boolean[N][N];
			count = 0;
			for(int i =0; i<N; i++) {
				for(int j=0; j<N; j++) {
					if(map[i][j] <= k) continue;
					if(visit[i][j] == true) continue;
					visit[i][j] = true;
					s.push(new int[]{i,j});
					dfs(i,j,k);
					if(ans < count) ans = count;
				}
			}
		}
		
		
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
		if(ans == 0) System.out.println(1);
		else System.out.println(ans);
		br.close();
	}
	private static void dfs(int i,int j, int k) {
		while(!s.isEmpty()) {
			int[] curr = s.pop();
			for(int d=0; d<4; d++) {
				int ii= curr[0] + di[d];
				int jj= curr[1] + dj[d];
				if(ii <0 || ii >= N || jj <0 || jj >= N ) continue;
				if(visit[ii][jj] == false && map[ii][jj] > k) {
					visit[ii][jj] = true;
					s.push(new int[] {ii,jj});
				}
			}
		}
		count++;
	}
}