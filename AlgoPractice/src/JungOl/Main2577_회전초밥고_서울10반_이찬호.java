package JungOl;
import java.io.*;
import java.util.*;
class Main2572_회전초밥고_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main2572.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		int max = 0;
		int N = Integer.parseInt(s[0]);
		int d = Integer.parseInt(s[1]);
		int k = Integer.parseInt(s[2]);
		int c = Integer.parseInt(s[3]);
		
		int count = 1;
		int res[] = new int[d+1];
		int cnt[] = new int[d+1];
		int before[] = new int[k];
		int dd = 0;
		Queue<Integer> q = new LinkedList<>();
		res[c] = 1;
		cnt[c] = 1;
		for(int i=0; i<k; i++) {
			dd = Integer.parseInt(br.readLine());
			if(res[dd] >= 0) res[dd]++;
			if(cnt[dd] ==0) {
				cnt[dd]++;
				count++;
			}
			before[i]=dd;
			q.offer(dd);
		}
		
		for(int i=k; i<N; i++) {
			dd = Integer.parseInt(br.readLine());
			if(res[dd] >= 0) res[dd]++;
			if(cnt[dd] ==0) {
				cnt[dd]++;
				count++;
			}
			q.offer(dd);
			int be = q.poll();
			res[be]--;
			if(res[be] ==0) {
				cnt[be]--;
				count--;
			}
			
			if(max<count) max = count;
		}
		
		for(int i=0; i<k-1; i++) {
			dd = before[i];
			if(res[dd] >= 0) res[dd]++;
			if(cnt[dd] ==0) {
				cnt[dd]++;
				count++;
			}
			q.offer(dd);
			int be = q.poll();
			res[be]--;
			if(res[be] ==0) {
				cnt[be]--;
				count--;
			}
			if(max<count) max = count;
		}
		
		System.out.println(max);
		
		br.close();
	}
}