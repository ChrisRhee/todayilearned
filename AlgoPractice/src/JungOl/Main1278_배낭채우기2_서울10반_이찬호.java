package JungOl;
import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
class Main1278_배낭채우기2_서울10반_이찬호{
	public static int N, W, map[][], max, dp[][];
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1077.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		N = Integer.parseInt(s[0]);
		W = Integer.parseInt(s[1]);
		max = 0;
		map = new int [N][2];
		dp = new int [2][W+1];
		
		for (int i=0; i<N; i++) {
			s = br.readLine().split(" ");
			map[i][0] = Integer.parseInt(s[0]);
			map[i][1] = Integer.parseInt(s[1]);
		}
		
		Arrays.sort(map, new Comparator<int[]>() {
			@Override
			public int compare(int[] o1, int[] o2) {
				return o1[0]-o2[0];
			}
		});
		
		for(int i=0; i<N; i++) {
			for(int j=0; j<=W; j++) {
				int quotient = 0;
				int res = 0 ;
				if(j - map[i][0] >= 0) {
					quotient = 1;
					res = j - map[i][0];
				}
				if(dp[1][j] < quotient*map[i][1]+dp[0][res])
					dp[1][j] = quotient*map[i][1]+dp[0][res];
			}
			for(int j=0; j<=W; j++) {
				dp[0][j] = dp[1][j];
			}
		}
		System.out.println(dp[1][W]);
		br.close();
	}

}