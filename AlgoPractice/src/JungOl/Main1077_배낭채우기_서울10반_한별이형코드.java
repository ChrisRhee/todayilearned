package JungOl;
import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
class Main1077_배낭채우기_서울10반_한별이형코드{
	public static int N, W, map[][], max, dp[];
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1077.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		N = Integer.parseInt(s[0]);
		W = Integer.parseInt(s[1]);
		max = 0;
		map = new int [N][2];
		dp = new int [W+1];
		
		for (int i=0; i<N; i++) {
			s = br.readLine().split(" ");
			map[i][0] = Integer.parseInt(s[0]);
			map[i][1] = Integer.parseInt(s[1]);
		}
		
		// 하나씩만 넣을 수 있을 때 
		for(int i=0; i<N; i++) {
			for(int j=W; j>=map[i][0]; j--) {
				dp[j] = Math.max(dp[j], dp[j-map[i][0]]+map[i][1]);
			}
		}
		System.out.println(dp[W]);
		
		// 갯수가 무한일 때
		for(int i=0; i<N; i++) {
			for(int j=map[i][0]; j<=W; j++) {
				dp[j] = Math.max(dp[j], dp[j-map[i][0]]+map[i][1]);
			}
		}
		System.out.println(dp[W]);
		
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
		br.close();
	}

}