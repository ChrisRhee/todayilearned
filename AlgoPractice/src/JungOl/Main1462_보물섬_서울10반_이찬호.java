package JungOl;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
class Main1462_보물섬_서울10반_이찬호{
	public static String land[][];
	public static Queue<int[]> q;
	public static int max; 
	public static int li,lj;
	public static int di[] = {-1,1,0,0};
	public static int dj[] = {0,0,-1,1};
	public static int[][] visit;
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/JungOl1462.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String s[] = br.readLine().split(" ");
		int Ans = 0;
		li = Integer.parseInt(s[0]);
		lj = Integer.parseInt(s[1]);
		land = new String[li][lj];
		
		q = new LinkedList<>();
		for(int i= 0; i<li; i++) {
			s = br.readLine().split("");
			for(int j=0; j<lj; j++) {
				land[i][j] = s[j];
			}
		}
		
		max =0;
		for(int i= 0; i<li; i++) {
			for(int j=0; j<lj; j++) {
				if(land[i][j].equals("L")) 
					bfs(i,j);
			}
		}
		
		System.out.println(max-1);
		/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
		br.close();
	}
	
	private static void bfs(int i, int j) {
		visit = new int[li][lj];
		q.offer(new int[] { i, j });
		visit[i][j] = 1;
		while (!q.isEmpty()) {
			int curr[] = q.poll();
			for (int d = 0; d < 4; d++) {
				int ii = curr[0] + di[d];
				int jj = curr[1] + dj[d];
				if (ii < 0 || ii >= li || jj < 0 || jj >= lj)
					continue;
				if (land[ii][jj].equals("L") && visit[ii][jj] == 0) {
					visit[ii][jj] = visit[curr[0]][curr[1]] + 1;
					// System.out.println("hi");
					if (visit[ii][jj] > max)
						max = visit[ii][jj];
					q.offer(new int[] { ii, jj });
				}
			}
		}
	}
	
}