package JungOl;
import java.io.*;
/**
 * 작성자 : 이찬호
 * 문제
 * 	- 
 * 조건
 * 	- 
 * 입력
 *  - 
 * 출력
 * 	- 
 * 풀이 
 * 	- 
 * */
class Main1681_해밀턴순환회로_서울10반_이찬호{
	public static int map[][], N, ans;
	public static boolean visit[];
	
	private static void dfs(int start, int count, int dis, String ddd) {
		if(count == N-1 ) {
			if(map[start][1]==0) return;
			dis += map[start][1];
			if(ans > dis) ans = dis;
			return;
		}
		if(ans < dis) return;
		for(int i=1; i<=N; i++) {
			if(visit[i] == true) continue;
			if(map[start][i]==0)continue;
			if(i == start) continue;
			visit[i] = true;
			dfs(i, count+1, dis+map[start][i], ddd+map[start][i]+" ");
			visit[i] = false;
		}
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/main1681.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N= Integer.parseInt(br.readLine().trim());
		map = new int[N+1][N+1];
		ans = Integer.MAX_VALUE;
		visit = new boolean[N+1];
		for(int i=1; i<=N; i++) {
			String s[] = br.readLine().split(" ");
			for(int j=1; j<=N; j++) {
				map[i][j] = Integer.parseInt(s[j-1]);
			}
		}
		visit[1] = true;
		dfs(1,0,0,"");
		System.out.println(ans);
		br.close();
	}

	
}