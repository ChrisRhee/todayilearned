
public class UserVO {
	private String name;
	private int age;
	private String userId;
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		builder.append(name);
		builder.append("|");
		builder.append(age);
		builder.append("|");
		builder.append(userId);
		builder.append("]");
		return builder.toString();
	}
	
	
}
