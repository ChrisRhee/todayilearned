package com.ssafy.user;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class UserDAO {
	private DocumentBuilder p;
	
	public UserDAO() {
		try {
			p=DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	// 서버의 모든 user를 출력하세요
	public Collection<UserVO> getUsers() throws SAXException, IOException{

		Collection<UserVO> list = new ArrayList<UserVO>();
		Document d = p.parse("http://70.12.109.160:8888/down/user.xml");
		Element root = d.getDocumentElement();
		NodeList users = root.getElementsByTagName("user");
		for(int i=0; i<users.getLength(); i++) {
			UserVO vo = new UserVO();
			Node t = users.item(i);
			
			// user의 id속성값 attribute 값 가져오기
//			NamedNodeMap map = t.getAttributes();
//			String idValue = map.getNamedItem("id").getNodeValue();
//			vo.setUserId(idValue);
			
			// user의 id속성값 attribute 값 가져오기
			vo.setUserId(t.getAttributes().getNamedItem("id").getNodeValue());
			
			// user에 있는 다른 값들 name, age 가져오기
			NodeList user = t.getChildNodes();
			
			for(int j =0; j< user.getLength(); j++) {
				Node temp2 = user.item(j);
				if(temp2.getNodeType()== Node.ELEMENT_NODE) {
					if(temp2.getNodeName().equals("name")) {
						vo.setName(temp2.getFirstChild().getNodeValue());
					}
					else if(temp2.getNodeName().equals("age")) {
						vo.setAge(Integer.parseInt(temp2.getFirstChild().getNodeValue()));
					}
				}
			}
			list.add(vo);
		}	
		return list;
	}
	
	// 특정 고객 (Id)를 이용해서 고객의 정보를 출력하세요 +_+!
	public UserVO getUser(String userId) throws SAXException, IOException {
		for (UserVO user : getUsers()) {
			if(user.getUserId().equals(userId)) {
				return user;
			}
		}
		return null;
	}
	
}
