import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class SimpleDOMTest {
	public SimpleDOMTest() throws ParserConfigurationException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder p = factory.newDocumentBuilder();
		try {
			Document doc = p.parse("http://70.12.109.160:8888/down/user.xml");
			System.out.println(doc);
			Element root = doc.getDocumentElement();
			System.out.println(root.getNodeName());
			NodeList users = root.getChildNodes();
			System.out.println(users.getLength());
			for (int i = 0; i < users.getLength(); i++) {
				Node n = users.item(i);
				if(n.getNodeType() == Node.ELEMENT_NODE) {
					System.out.println(n.getNodeName());
				}
			}
			
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) throws ParserConfigurationException {
		new SimpleDOMTest();
	}
}
