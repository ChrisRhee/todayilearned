import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

public class SimpleSAXText extends DefaultHandler {
	private String elementFlag= "";
	private UserVO user;
	
	public SimpleSAXText() throws ParserConfigurationException, SAXException, IOException {
		//XML 읽기
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser p = factory.newSAXParser();
		p.parse("http://70.12.109.160:8888/down/user.xml", this);
	}
	
	@Override
	public void startDocument() throws SAXException {
		System.out.println("xml해석 시작");
	}

	@Override
	public void endDocument() throws SAXException {
		System.out.println("xml해석 끗");
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		elementFlag = qName;
		if(elementFlag.equals("user")) {
			user= new UserVO();
			user.setUserId(attributes.getValue("id"));
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		//System.out.println(start+" "+length);
		String s= new String(ch,start,length).trim();
		//System.out.println(s);
		if(s.length()>0) {
			switch(elementFlag) {
				case "name":
					user.setName(s);
					break;
				case "age":
					user.setAge(Integer.parseInt(s));
					break;
					
			}
		}

	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if(qName.equals("user") && user.getAge() >= 20) {
			System.out.println(user);
			user = null;
			elementFlag= "";
		}
	}

	public static void main(String[] args) throws ParserConfigurationException, SAXException {
		
		try {
			new SimpleSAXText();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Program End");
	}
}
