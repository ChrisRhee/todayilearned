package com.ssafy.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssafy.model.MemberDAO;
import com.ssafy.model.MemberVO;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet("/main")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;       
    
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="error.jsp";
		String cmd=request.getParameter("cmd");
		if(cmd==null) cmd="";
		switch(cmd) {
		case "mainUI":
			url=mainUI(request);
			break;
		case "loginAction":
			url=loginAction(request);
			break;
		case "logout":
			url=logout(request);
			break;
				
		}//switch
		
		if(url.charAt(0) == '/')
			request.getRequestDispatcher(url).forward(request, response);
		else
			response.sendRedirect(url);
	}

	private String logout(HttpServletRequest request) {
		// 3.세션 종료
		HttpSession session=request.getSession();
		if(session !=null)
			session.invalidate(); //서버내의 세션을 모두 소멸
		return "/main.jsp";
	}

	private String loginAction(HttpServletRequest request) {
		String id=request.getParameter("id");
		String pw=request.getParameter("pw");
		//로그인 처리 - DBMS - JavaBean(DAO)
		MemberDAO dao=new MemberDAO();
		MemberVO vo=dao.login(id, pw);
		if(vo !=null) {
			//view 에서 사용할 데이터를 메모리에 저장 = 로그인 상태를 유지HttpSession
			//1. 서버는 상태에 따라 세션메모리를 유지하지 않을수 있다. 세션이 없으면 새로 생성
			HttpSession session=request.getSession(true);
			session.setAttribute("loginId", id);
			session.setAttribute("loginVO", vo);
			return "/loginOK.jsp";
		}
		return "/error.jsp";
	}

	private String mainUI(HttpServletRequest request) {
		// 혹시 서버를 통해 브라우저에 전달해야 되는 데이터를 지정
		return "/main.jsp";
	}

}
