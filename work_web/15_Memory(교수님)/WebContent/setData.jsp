<%@ page import="javax.servlet.http.Cookie"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	//1. request 에 데이터 추가
	request.setAttribute("req", "RequestData");

	//2. session
	session.setAttribute("user", "jjeon");
	
	//3. application : 같은 ContextRoot내의 객체가 공유
	application.setAttribute("master", "oopsw@tistory.com");
	
	//4.브라우저에 필요한 데이터 저장 - cookie, Storage
	Cookie c=new Cookie("user", "userData");
	//c.setHttpOnly(true);
	response.addCookie(c);	
%>
<script>	
	//localStorage.javascript_data='javascript에서 저장 권장';
</script>
<jsp:forward page="getData.jsp" />