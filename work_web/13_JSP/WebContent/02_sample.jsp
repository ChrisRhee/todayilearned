<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <% request.getParameter("name").charAt(0); %>
<h1>JSP에는 서버에서 동작할 코드를 삽입할 수있는 것이 가장 큰 장점이다</h1>

<%! // JSP 멤버로 선언 지시자 
	private String name;
	public String toString(){
	return "name : "+name;
	}
%>

<% // _jspService() 내의 구현부로 삽입되는 코드 
	// 내장 객체 : 프로그램 구현 중에 반복적으로 사용되는 객체를 일관된 이름으로 제공
out.print("서비 메서드 내의 구현부<br>");
name= "hello";
%>

<input type="text" value="<%=name%>">