package com.oopsw.simple;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private int count;
    public HelloServlet() {
        super();
        System.out.println("생성자222 : " + ++count);
    }

	public void init(ServletConfig config) throws ServletException {
		System.out.println("init() : " + ++count);
	}

	public void destroy() {
		System.out.println("destroy() : " + ++count);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doGet() : " + ++count);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doPost() : " + ++count);
	}

//	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		System.out.println("service() : " + ++count);
//	}

}
