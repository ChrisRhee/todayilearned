package com.ssafy.login;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/loginAction")
public class LoginActionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doPost work");
		// 1. 사용자를 통해 추가 데이터를 전달 받을 때
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		
		// 2. DBMS에서 이 아이디가 존재하는지 존재여부 확인하기!
		// 근데 우리는 DB가 없으므로 간단하게 진행한다.
		if(id.equals("admin") && pw.equals("1234")) {
			// 관리자 전용 페이지로 이동
			request.getRequestDispatcher("/admin.jsp").forward(request, response);
		}else {
			// 에러 페이지 브라우저에 전송
			response.sendRedirect("error.html");
		}
	}
}
