package com.ssafy.simple;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/userServlet")
public class UserServlet extends HttpServlet {

	// 요구사항에 맞는 필요한 메서드만 재정의 하면 된다 
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// request는 넘어온 데이터를 빼올 때 쓴다.
		// 1. client -> server로 요청
		String m = request.getMethod();
		System.out.println("서버 요청 타입 : " + m); // VM의 콘솔에 출력한다.
		
		// 2. 서버에서 처리한다.

		
		// 브라우저에 처리 결과를 전달하려면?
		// 3. server -> 브라우저로 처리 결과값 전달
		PrintWriter out = response.getWriter();
		out.print("<h1> server -> client message </h1><br>hello");
		out.flush();
		out.close();
	}
	
}
