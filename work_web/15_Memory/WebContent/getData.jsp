<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    <h1>Request</h1>
    ${req}
    
    <h1>Session</h1>
    ${user}
    
    <h1>Application</h1>
    ${master}
    
    <h1>Cookie</h1>
    ${c1}
   	<%
    	Cookie[] cookies = request.getCookies();
   		if(cookies!=null){
  			out.print(cookies[cookies.length-1].getValue());
   		}
    %>
    
    <h3>end</h3>