<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    <%
    // 1. requst 에 데이터 추가
    request.setAttribute("req", "RequestData");
    
    // 2. session
    session.setAttribute("user", "chan");
    
    // 3. application : 같은 Contex.tRoot내의 객체가 공유
    application.setAttribute("master", "0407chan@naver.com");
    
    // 4. 브라우저에 필요한 데이터 저장 - cookie, Storage
    Cookie c1 = new Cookie("use","user_data");
    c1.setHttpOnly(true);
    response.addCookie(c1);
    
    Cookie c2 = new Cookie("use","이찬호다");
    c2.setHttpOnly(true);
    response.addCookie(c2);
    %>
    
    <script>
    	//localStorage.javascript_data='javascript에서 저장 권장';
    </script>
    
    
    <jsp:forward page="getData.jsp" />