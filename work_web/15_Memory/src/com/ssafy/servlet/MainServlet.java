package com.ssafy.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ssafy.model.MemberDao;
import com.ssafy.model.MemberVo;

@WebServlet("/main")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "error.jsp";
		String cmd = request.getParameter("cmd");
		switch(cmd) {
		case "mainUI":
			url = mainUI(request);
			break;
		case "loginAction":
			url = loginAction(request);
			break;
		case "logout":
			url = logout(request);
			break;
		}
		
		if(url.charAt(0) =='/') {
			request.getRequestDispatcher(url).forward(request, response);
		}
		else {
			response.sendRedirect(url);
		}
	}

	private String logout(HttpServletRequest request) {
		// 3. 세션종료
		// 세션이 있는지 확인하자
		HttpSession session = request.getSession();
		if(session != null) {
			session.invalidate();
		}
		return "/main.jsp";
	}

	private String loginAction(HttpServletRequest request) {
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		// 로그인 처리 = DBMS - JavaBean(DAO)
		MemberDao dao = new MemberDao();
		MemberVo vo = dao.login(id, pw);
		
		if(vo!=null) {
			//view에서 사용할 데이터를 메모리에 저장
			// 로그인 상태를 유지하는것 HttpSession이라고 한다.ㅇ
			// 1. 서버는 상태에 따라 세션메모리를 유지하지 않을 수 있다. 그러니 세션이 없으면 새로 생성해라! 라는 의미가 true이다.
			HttpSession session = request.getSession(true);
			session.setAttribute("loginId", id);
			session.setAttribute("loginVo", vo);
			return "/loginOK.jsp";
		}
		else {
			return "/error.jsp";
		}
	}

	private String mainUI(HttpServletRequest request) {
		// 서버를 통해 브라우저로 전달해야하는 데이터를 지정
		return "/main.jsp";
		
	}

}
