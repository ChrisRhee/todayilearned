package com.ssafy.model;

public class MemberVo {
	private String memberName;
	
	public MemberVo() {
		super();
		setMemberName("");
	}
	
	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	
}
