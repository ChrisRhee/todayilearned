package com.ssafy.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("*.do")
public class FrontControllerServlet extends HttpServlet {
	private Map<String, Controller> clist;
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config); // WAS제공되는 config 유지
		clist= new HashMap<>();
		clist.put("/ssafy.do", new SsafyController());
		clist.put("/memberInsert.do", new MemberInsertController());
		clist.put("/memberInsertAction.do", new MemberInsertActionController());
	}
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cmd = request.getRequestURI().substring(request.getContextPath().length());
		System.out.println(cmd);
		System.out.println("frontControllerServlet");
		
		try {
			clist.get(cmd).excute(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
