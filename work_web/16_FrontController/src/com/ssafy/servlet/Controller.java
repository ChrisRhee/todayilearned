package com.ssafy.servlet;

//package com.ssafy.servlet;

import javax.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Controller {

	void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, Exception;

}
