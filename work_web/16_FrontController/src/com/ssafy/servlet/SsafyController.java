package com.ssafy.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SsafyController implements Controller {

	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 시작페이지 역할
		// 서버에서 처리할 내용 있으면 처리 하고
		
		// 리퀘스트 forward 해준다.
		request.getRequestDispatcher("/ssafy.html").forward(request, response);
	}
}
