package com.ssafy.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ControllerServlet
 */
@WebServlet("/controller")
public class ControllerServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="index.html";
		//1. 요청받은 데이터 구분
		String cmd=request.getParameter("cmd");
		System.out.println("cmd:"+cmd);
		//2. JavaBean을 통해 처리
		switch (cmd) {
			case "loginUI":		   url="/login.html";			break;
			case "memberInsertUI": url="/member.html"; 			break;
			case "loginAction":    url=loginAction(request);    break;
			case "memberInsertAction": url=memberInsertAction(request); break;	
		}
		
		//3. JSP페이지로 이동
		if(url.charAt(0) == '/') {
			request.getRequestDispatcher(url).forward(request, response);
		}else {
			response.sendRedirect(url);
		}		
		
	}

	private String memberInsertAction(HttpServletRequest request) {
		//  admin만 아니면 회원가입 ok
		if(request.getParameter("id").equals("admin"))   return "/memberInsert.html";
		
		return "/login.html";
	}

	private String loginAction(HttpServletRequest request) {
		// 로그인 admin/1234
		if(request.getParameter("id").equals("admin") && request.getParameter("pw").equals("1234"))
			return "/loginok.jsp";
		return "/login.html";
	}

}
