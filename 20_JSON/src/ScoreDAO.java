import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class ScoreDAO {
	private Map<String, Integer> scores;
	private static ScoreDAO instance = null;
	private ScoreDAO() {
		scores = new HashMap<>();
	}
	
	public static ScoreDAO newInstance() {
		if(instance==null)
			instance = new ScoreDAO();
		return instance;
	}
	public void addStudentScore(String subject, int score) throws ScoreDefaultValueException {
		if(score <0 || score>100) {
			throw new ScoreDefaultValueException("Score should be 0~100");
		}
		scores.put(subject, score);
	}

	public Map<String, Integer> getScores() {
		return scores;
	}

	public String mapToJson() {
		JsonArray ja = new JsonArray();
		Set<String> keys = scores.keySet();
		for (String key : keys) {
			JsonObject score = new JsonObject();
			score.addProperty(key, scores.get(key).toString());
			ja.add(score);
		}
		
		return ja.toString();
	}

	public void fileJsonSave(String string) {
		try {
			FileWriter f =new FileWriter(string, false);
			f.write(mapToJson());
			f.flush();
			f.close(); 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	
}
