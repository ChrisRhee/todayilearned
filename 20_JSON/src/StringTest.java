import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.Scanner;

public class StringTest {
	public static void main(String[] args) throws IOException {
		//접속한 사람의 이름과 접속 시간을 관리하세욤 - 분석하세욤
//		System.setOut(new PrintStream("out.txt"));
//		Scanner sc = new Scanner(System.in);
//		while(true) {
//			//System.out.print("이름: ");
//			String in = sc.nextLine().trim();
//			if(in==null || in.length()==0) break;
//			System.out.println(new Date()+", "+in);
//		}
		
		FileInputStream fis = new FileInputStream("out.txt");
		DataInputStream dis = new DataInputStream(fis);
		while(true) {
			String s = dis.readLine();
			if(s==null || s.trim().length()==0) break;
			String sa[] = s.split(", ");
			System.out.println(sa[0]+" : ["+sa[1]+"]");
		}
		
	}
}	
