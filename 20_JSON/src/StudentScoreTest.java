public class StudentScoreTest {
	// 학생별로 저장?
	// 메모리에 담았다가 한번에 저장?
	
	public static void main(String[] args) {
		ScoreDAO dao = ScoreDAO.newInstance();
		
		try {
			dao.addStudentScore("국어",100);
		} catch (ScoreDefaultValueException e) {
			e.printStackTrace();
		}
		try {
			dao.addStudentScore("수학",20);
		} catch (ScoreDefaultValueException e) {
			e.printStackTrace();
		}
		try {
			dao.addStudentScore("영어",20);
		} catch (ScoreDefaultValueException e) {
			e.printStackTrace();
		} // 이건 안되는 예제다! 라고 생각하는건 편견! 
		
		System.out.println(dao.getScores());
		System.out.println(dao.mapToJson());
		
		dao.fileJsonSave("chan.txt");
		
	}
}
