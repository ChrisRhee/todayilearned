import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JSONTest {
	
	public static void main(String[] args) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("국어", 50);
		jsonObject.addProperty("수학", 40);
		
		try {
			FileWriter file = new FileWriter("out.txt");
			file.write(jsonObject.toString());
			file.flush();
			file.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	
		System.out.println("Program End");
	}
}
