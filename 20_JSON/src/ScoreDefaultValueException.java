
public class ScoreDefaultValueException extends Exception {
	public ScoreDefaultValueException(String msg) {
		super(msg);
	}
}
