package dp;
import java.io.FileInputStream;
import java.util.Scanner;

class Main9095_123더하기{
	public static int ans;
	
	private static void solve(int num, int val) {
		if(num < val) return;
		if(num == val) {
			ans++;
			return;
		}
		
		for(int i=1; i<=3; i++) {
			solve(num,val+i);
		}
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj9095.txt"));
		
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int i=0; i<T; i++) {
			ans = 0;
			solve(sc.nextInt(),0);
			System.out.println(ans);
		}
		
	}

	
}