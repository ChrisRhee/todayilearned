package backJun;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

class Main16236_아기상어{
	public static int N, ans, min;
	public static int[] di = {-1,1,0,0};
	public static int[] dj = {0,0,-1,1};
	public static boolean[][] visit;
	public static Stack<int[]> q;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj16236.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		
		List<int[]> l = new ArrayList<>();
		q = new Stack<>();
		int map[][] = new int[N][N];
		int shark[] = new int[4];
		for(int i=0; i<N; i++) {
			String s[] = br.readLine().split(" ");
			for (int j=0; j<N; j++) {
				map[i][j] = Integer.parseInt(s[j]);
				if(map[i][j] > 0 && map[i][j] < 7) {
					l.add(new int[] {i,j,map[i][j]});
				}
				if(map[i][j] == 9) {
					shark[0] = i;
					shark[1] = j;
					shark[2] = 2;
				}
			}
		}
		
		Collections.sort(l, new Comparator<int[]>() {
			@Override
			public int compare(int[] o1, int[] o2) {
				if(o1[2] == o2[2]) {
					if(o1[0] == o2[0])
						return o1[1]-o2[1];
					else 
						return o1[0]-o2[0];
				}
				else 
					return o1[2]-o2[2];
			}
		});
		
		//for (int i = 0; i < l.size(); i++) System.out.println(Arrays.toString(l.get(i)));
		
		while(true) {
			int[] t = getFeed(shark,l);
			System.out.println(Arrays.toString(t));
			if(t[0]==0 && t[1] == 0 && t[2] == 0 && t[3] == 0)
				break;
			
			visit = new boolean[N][N];
			min = Integer.MAX_VALUE;
			visit[shark[0]][shark[1]] = true;
			solve(shark, shark[0], shark[1], t, map, 0);
			ans += min;
			System.out.println(min);
			l.remove(t[3]);
			/*boolean ch = bfs(shark, t, map);
			if(ch) {
				ans += Math.abs(si-t[0])+Math.abs(sj-t[1]);
				System.out.println(ans);
				l.remove(t[3]);
			}*/
			//System.out.println(ch);
		}
		
		System.out.println(ans);
	}

	private static int[] getFeed(int[] shark, List<int[]> l) {
		int min = Integer.MAX_VALUE;
		int t[] = new int[4];
		for(int i=0; i<l.size(); i++) {
			if(shark[2] > l.get(i)[2]) {
				if(min > Math.abs(shark[0] - l.get(i)[0])+Math.abs(shark[1] - l.get(i)[1])) {
					min = Math.abs(shark[0] - l.get(i)[0])+Math.abs(shark[1] - l.get(i)[1]);
					t[0] = l.get(i)[0];
					t[1] = l.get(i)[1];
					t[2] = l.get(i)[2];
					t[3] = i;
				}
			}
		}
		return t;
	}
	
	private static void solve(int[] shark, int i, int j, int[] fin, int[][] map, int dis) {
		if(dis > min ) return;
		
		int distance = dis;
		for(int d= 0; d<4; d++) {
			int ii= i+di[d];
			int jj= j+dj[d];
			if(ii < 0 || ii >= N || jj < 0 || jj >= N) continue;
			if(map[ii][jj] > shark[2] || visit[ii][jj] == true) continue;
			
			
			if(map[ii][jj] == 0 || map[ii][jj] == shark[2]) {
				visit [ii][jj] = true;
				solve(shark, ii, jj, fin, map, distance+1);
				visit [ii][jj] = false;
			}
			else if(map[ii][jj] > 0 && map[ii][jj] < shark[2]) {
				if(ii == fin[0] && jj == fin[1]) {
					shark[3] ++;
					if(shark[3] == shark[2]) {
						shark[3] = 0;
						shark[2] ++;
					}
					map[shark[0]][shark[1]] = 0;
					map[ii][jj] = 9;
					shark[0] = ii;
					shark[1] = jj;
					if(min > distance+1) min = distance+1;
					return;
				}
				visit [ii][jj] = true;
				solve(shark, ii, jj, fin, map, distance+1);
				visit [ii][jj] = false;
			}
		}
	}
	
	private static boolean bfs(int[] shark, int[] fin, int[][] map) {
		visit = new boolean[N][N];
		visit[shark[0]][shark[1]] = true;
		q.push(new int[] {shark[0],shark[1]});
		while(!q.isEmpty()) {
			int[] curr = q.pop();
			for(int d= 0; d<4; d++) {
				int ii= curr[0]+di[d];
				int jj= curr[1]+dj[d];
				if(ii < 0 || ii >= N || jj < 0 || jj >= N) continue;
				if(map[ii][jj] > shark[2]) continue;
				
				if(map[ii][jj] == 0 && visit[ii][jj] == false) {
					visit [ii][jj] = true;
					q.push(new int[] {ii,jj});
				}
				else if(map[ii][jj] > 0 && map[ii][jj] < shark[2] && visit[ii][jj] == false) {
					visit [ii][jj] = true;
					q.push(new int[] {ii,jj});
					if(ii == fin[0] && jj == fin[1]) {
						shark[3] ++;
						if(shark[3] == shark[2]) {
							shark[3] = 0;
							shark[2] ++;
						}
						map[shark[0]][shark[1]] = 0;
						map[ii][jj] = 9;
						shark[0] = ii;
						shark[1] = jj;
						return true;
					}
				}
				else if( map[ii][jj] == shark[2] && visit[ii][jj] == false) {
					visit [ii][jj] = true;
					q.push(new int[] {ii,jj});
				}
			}
		}
		
		return false;
	}
}