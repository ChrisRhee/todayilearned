package backJun;
import java.io.FileInputStream;
import java.util.*;

class Main16235_나무재테크{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj16235.txt"));
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt(); // 농장 크기
		int M = sc.nextInt(); // 심겨져있는 나무 개수 
		int K = sc.nextInt(); // k년 후
		int a[][] = new int[N][N]; // 겨울마다 뿌리는 양분
		int map[][] = new int[N][N];
		List<int[]> tree = new ArrayList<>();
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				a[i][j] = sc.nextInt();
				map[i][j] = 5;
			}
		}
		
		for(int i=0; i<M; i++) {
			int ii = sc.nextInt()-1;
			int jj = sc.nextInt()-1;
			int age = sc.nextInt();
			int s = tree.size();
			if(s == 0) tree.add(new int[] {ii, jj, age, 1});
			else {
				for(int j=0; j<s; j++) {
					if(tree.get(j)[2] >= age) {
						tree.add(j, new int[] {ii, jj, age, 1});
						break;
					}
				}
			}
		}
		
		for(int i=0; i<K; i++) { // 햇수만큼
			
			//봄
			int s = tree.size();
			for(int j=0; j<s; j++) { // tree에 있는 애들
				int curr[] = tree.get(j);
				if(map[curr[0]][curr[1]] >= curr[2]) {
					map[curr[0]][curr[1]] -= curr[2];
					curr[2]++;
				}
				else {
					curr[3] = 0;
				}
			}
			
			// 여름
			s = tree.size();
			for(int j=0; j<s; j++) {
				int curr[] = tree.get(j);
				if(curr[3] == 1) continue;
				map[curr[0]][curr[1]] += curr[2]/2;
				tree.remove(j);
				j--;
				s = tree.size();
			}
			
			// 가을
			s = tree.size();
			for(int j=0; j<s; j++) {
				int curr[] = tree.get(j);
				if(curr[2] % 5 == 0) {
					if(curr[0]-1 >= 0) {
						tree.add(0, new int[] {curr[0]-1, curr[1], 1, 1});
						j++;
					}
					if(curr[0]-1 >= 0 && curr[1]-1 >=0) {
						tree.add(0, new int[] {curr[0]-1, curr[1]-1, 1, 1});
						j++;
					}
					if(curr[1]-1 >= 0) {
						tree.add(0, new int[] {curr[0], curr[1]-1, 1, 1});
						j++;
					}
					if(curr[0]+1 < N && curr[1]-1 >=0) {
						tree.add(0, new int[] {curr[0]+1, curr[1]-1, 1, 1});
						j++;
					}
					if(curr[0]+1 < N) {
						tree.add(0, new int[] {curr[0]+1, curr[1], 1, 1});
						j++;
					}
					if(curr[0]+1 < N && curr[1]+1 < N) {
						tree.add(0, new int[] {curr[0]+1, curr[1]+1, 1, 1});
						j++;
					}
					if(curr[1]+1 < N) {
						tree.add(0, new int[] {curr[0], curr[1]+1, 1, 1});
						j++;
					}
					if(curr[0]-1 >= 0 && curr[1]+1 < N) {
						tree.add(0, new int[] {curr[0]-1, curr[1]+1, 1, 1});
						j++;
					}
				}
				s = tree.size();
			}
			
			// 겨울
			for(int k=0; k<N; k++) 
				for(int j=0; j<N; j++) 
					map[k][j] += a[k][j];
			
			/*System.out.println("나무확인");
			for (int j = 0; j < tree.size(); j++) {
				System.out.println(Arrays.toString(tree.get(j)));
			}*/
		}
		
		System.out.println(tree.size());
	}
}