package backJun;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

class Main1003_피보나치함수{
	public static int a,b;
	public static void main(String args[]) throws Exception	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine());
		long map[][] = new long[2][41];
		map[0][0] = 1;
		map[1][0] = 0;
		map[0][1] = 0;
		map[1][1] = 1;
		for(int i=2; i<41; i++) {
			a = 0;
			b = 0;
			fibonacci(i,map);
			map[0][i] = a;
			map[1][i] = b;
		}
		for(int tc = 0; tc<T; tc++) {
			int N = Integer.parseInt(br.readLine());
			System.out.println(map[0][N]+" "+map[1][N]);
		}
		
	}

	public static int fibonacci(int n, long[][] map) {
		if (n == 0) {
			a++;
			return 0;
		} else if (n == 1) {
			b++;
			return 1;
		} else {
			return fibonacci(n - 1, map) + fibonacci(n - 2, map);
		}
	}
}