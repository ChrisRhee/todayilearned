package backJun;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

class Main17135_캐슬디펜스{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj17135.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String s[] = br.readLine().split(" ");
		int N = Integer.parseInt(s[0]);
		int M = Integer.parseInt(s[1]);
		int D = Integer.parseInt(s[2]);
		int map[][] = new int[N+1][M];
		int count[] = new int[M];
		for(int i=0; i<N; i++) {
			s = br.readLine().split(" ");
			for (int j=0; j<M; j++) {
				map[i][j] = Integer.parseInt(s[j]);
				if(map[i][j] ==1 ) count[j]++;
			}
		}
		for (int[] is : map) {
			System.out.println(Arrays.toString(is));
		}
		System.out.println(Arrays.toString(count));
	}
}