package backJun;
import java.io.*;
import java.util.Arrays;

class Main1976_여행가자_서울10반_이찬호{
	public static int map[][], N, M, visit[],plan[];
	
	private static void solve(int i, int start) {
		if(vcheck()== true) return;
		for(int j=start; j<N; j++) {
			if(map[i][j] == 0)continue;
			visit[j] = 1;
			solve(j,j);
		}
	}
	
	private static boolean vcheck() {
		boolean temp = true;
		for(int i=0; i<N; i++) {
			if(visit[i] == 0) {
				temp = false;
				break;
			}
		}
		return temp;
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj1976.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		M = Integer.parseInt(br.readLine());
		map = new int [N][N];
		visit = new int[N];
		int y =0;
		for(int i=0; i<N; i++) {
			String s[] = br.readLine().split(" ");
			for(int j=0; j<N; j++) map[i][j] = Integer.parseInt(s[j]);
		}
		String[] s = br.readLine().split(" ");
		plan = new int[N];
		for(int i=0; i<M; i++) plan[Integer.parseInt(s[i])-1] = 1;
		
		visit[0] = 1;
		for(int i=0; i<M; i++) {
			solve(Integer.parseInt(s[i])-1,0);
			System.out.println(Arrays.toString(visit));
			if(check()) y++;
		}
		
		if(y>0) System.out.println("YES");
		else System.out.println("NO");
		
	}
	private static boolean check() {
		boolean temp = true;
		for(int i=0; i<N; i++) {
			if(visit[i] >= plan[i]) continue;
			temp = false;
			break;
		}
		return temp;
	}
	
}