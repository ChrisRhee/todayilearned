package backJun;
import java.io.*;

class Main6064_카잉달력{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj6064.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		
		for(int i=0; i<n; i++) {
			String s[] = br.readLine().split(" ");
			int M = Integer.parseInt(s[0]);
			int N = Integer.parseInt(s[1]);
			int x = Integer.parseInt(s[2]);
			int y = Integer.parseInt(s[3]);
			int ans = 0;
			int count = 0;
			while(true) {
				if(count > N) {
					ans = -1; 
					break;
				}
				ans = M*count+x;
				count++;
				if((ans%(N) == y && ans%(M) == x ) || (ans%(N) == y%N && ans%(M) == x%M )) break;
			}
			System.out.println(ans);
		}
	}
}