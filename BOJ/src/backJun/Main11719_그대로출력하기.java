package backJun;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

class Main11719_그대로출력하기{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj11719.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = br.readLine();
		System.out.println(s);
		while(true) {
			s = br.readLine();
			if(s == null) break;
			System.out.println(s);
		}
	}
}