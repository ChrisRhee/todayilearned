package backJun;
import java.io.FileInputStream;
import java.util.Scanner;

class Main13458_시험감독{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj13458.txt"));
		
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		int[] map = new int [N];
		for(int i=0; i<N; i++) map[i] = sc.nextInt();
		int D = sc.nextInt();
		int subD= sc.nextInt();
		long count  =0;
		
		for(int i=0; i<N; i++) {
			map[i] = map[i]-D;
			if(map[i] > 0) {
				if(map[i]%subD == 0) count += map[i]/subD;
				else count += map[i]/subD + 1;
			}
		}
		
		System.out.println(count+N);
	}
}