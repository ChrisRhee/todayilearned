package bruteforce;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

class Main17136_색종이붙이기{
	public static int cnt,papers[];
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj17136.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int min = 100;
		
		boolean pos = true;
		int temp[][] = new int[10][10];
		int map[][] = new int[10][10];
		
		for(int i=0; i<10; i++) {
			String s[] = br.readLine().split(" ");
			for(int j=0; j<10; j++) {
				map[i][j] = Integer.parseInt(s[j]);
				temp[i][j] = map[i][j];
			}
		}
		
		int kindex = 5;
		while(true) {
			if(kindex==0) break;
			//if(min!=100) break;
			
			pos = true;
			cnt = 0;
			papers = new int[6];
			for(int i=0; i<10; i++) 
				for(int j=0; j<10; j++) temp[i][j] = map[i][j];
			
			for(int k=kindex; k>=1; k--) {
				for(int i=0; i<10; i++) {
					for(int j=0; j<10; j++) {
						if(temp[i][j]==0) continue;
						if(i+k > 10 || j+k > 10) continue;
						boolean c = check(k,i,j,temp);
						if(c) delete(k,i,j,temp);
					}
				}
				
			}
			
			for(int i=0; i<6; i++) {
				if(papers[i] > 5) {
					pos = false;
					break;
				}
			}
			//System.out.println("1 "+Arrays.toString(papers));
			if(pos) if(min > cnt) min = cnt; 
			
			pos = true;
			cnt = 0;
			papers = new int[6];
			for(int i=0; i<10; i++) 
				for(int j=0; j<10; j++) temp[i][j] = map[i][j];
			
			for(int k=kindex; k>=1; k--) {
				for(int i=0; i<10; i++) {
					for(int j=0; j<10; j++) {
						if(temp[9-i][j]==0) continue;
						if(9-i+k > 10 || j+k > 10) continue;
						
						boolean c = check(k,9-i,j,temp);
						if(c) delete(k,9-i,j,temp);
					}
				}
			}
			
			for(int i=0; i<6; i++) {
				if(papers[i] > 5) {
					pos = false;
					break;
				}
			}
			//System.out.println("2 "+Arrays.toString(papers));
			if(pos) if(min > cnt) min = cnt; 
			
			pos = true;
			cnt = 0;
			papers = new int[6];
			for(int i=0; i<10; i++) 
				for(int j=0; j<10; j++) temp[i][j] = map[i][j];
			
			for(int k=kindex; k>=1; k--) {
				for(int i=0; i<10; i++) {
					for(int j=0; j<10; j++) {
						if(temp[i][9-j]==0) continue;
						if(i+k > 10 || 9-j+k > 10) continue;
						
						boolean c = check(k,i,9-j,temp);
						if(c) delete(k,i,9-j,temp);
					}
				}
			}
			
			for(int i=0; i<6; i++) {
				if(papers[i] > 5) {
					pos = false;
					break;
				}
			}
			//System.out.println("3 "+Arrays.toString(papers));
			if(pos) if(min > cnt) min = cnt; 
			
			pos = true;
			cnt = 0;
			papers = new int[6];
			for(int i=0; i<10; i++) 
				for(int j=0; j<10; j++) temp[i][j] = map[i][j];
			
			for(int k=kindex; k>=1; k--) {
				for(int i=0; i<10; i++) {
					for(int j=0; j<10; j++) {
						if(temp[9-i][9-j]==0) continue;
						if(9-i+k > 10 || 9-j+k > 10) continue;
						
						boolean c = check(k,9-i,9-j,temp);
						if(c) delete(k,9-i,9-j,temp);
					}
				}
			}
			
			for(int i=0; i<6; i++) {
				if(papers[i] > 5) {
					pos = false;
					break;
				}
			}
			//.out.println("4 "+Arrays.toString(papers));
			if(pos) if(min > cnt) min = cnt; 
			
			kindex--;
		}
		
		
		if(min != 100) System.out.println(min);
		else System.out.println(-1);
	}

	private static void delete(int k, int i, int j, int[][] map) {
		for(int ii=i; ii<k+i; ii++) {
			for(int jj=j; jj<k+j; jj++) {
				map[ii][jj] = 0;
			}
		}
		cnt++;
		papers[k]++;
	}

	private static boolean check(int k, int i, int j, int[][] map) {
		for(int ii=i; ii<k+i; ii++) {
			for(int jj=j; jj<k+j; jj++) {
				if(map[ii][jj] == 0) return false;
			}
		}
		return true;
	}
}