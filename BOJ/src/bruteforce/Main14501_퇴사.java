package bruteforce;
import java.io.*;
import java.util.Arrays;

class Main14501_퇴사{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj14501.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine());
		int ans = 0;
		int max = 0;
		boolean visit[] = new boolean[T];
		int map[][] = new int[T][2];
		for(int i=0; i<T; i++) {
			String s[] = br.readLine().split(" ");
			for (int j=0; j<2; j++) {
				map[i][j] = Integer.parseInt(s[j]);
			}
		}
		
		int db[] = new int[T];
		for(int i= T-1; i>=0; i--) {
			if(map[i][0]+i > T) continue;
			if(i < T-1) db[i] = db[i+1];
			
			int sum = 0;
			
			for(int j= i+1; j< i+ map[i][0]; j++) {
				if(visit[j]==true) sum += map[j][1];
			}
			if(sum >= map[i][1]) continue;
			
			
			for(int j= i+1; j< i+ map[i][0]; j++) {
				if(visit[j]==true) {
					db[i] -= map[j][1];
					visit[j] = false;
				}
			}
			
			db[i] += map[i][1];
			visit[i]= true;
//			System.out.println(Arrays.toString(visit));
//			System.out.println(Arrays.toString(db));
		}
		//System.out.println(Arrays.toString(db));
		max = db[0];
		
		for(int i= 0; i< T; i++) {
			if(map[i][0]+i > T) continue;
			ans += map[i][1];
			i += map[i][0]-1;
		}
		if(max < ans) max = ans;
		System.out.println(max);
	}
}