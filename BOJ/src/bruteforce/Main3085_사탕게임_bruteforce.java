package bruteforce;
import java.io.*;

class Main3085_사탕게임_bruteforce{
	public static int[] di = {-1,1,0,0};
	public static int[] dj = {0,0,-1,1};
	public static int ans,n;
	
	private static void solve(int i, int j , char[][] map) {
		
		for(int d=0 ; d<4; d++) {
			int ii = i+di[d];
			int jj = j+dj[d];
			if(ii <0 || ii >= n || jj < 0 || jj >= n ) continue;
			change(i,j,ii,jj,map);
			check(i,j,map);
			change(i,j,ii,jj,map);
		}
	}
	
	private static void check(int i, int j, char[][] map) {
		int count = 1;
		for(int d=0; d<4; d++) {
			int ii = i;
			int jj = j;
			while(true) {
				ii+= di[d];
				jj+= dj[d];
				if(ii <0 || ii >= n || jj < 0 || jj >= n || map[i][j] != map[ii][jj]) break;
				count ++;
			}
			if(ans<count) ans = count;
			if(d ==1) count = 1;
		}
	}

	private static void change(int i, int j, int ii, int jj, char[][] map) {
		char t = map[i][j];
		map[i][j] = map[ii][jj];
		map[ii][jj] = t;
	}

	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj3085.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ans = 0;
		n = Integer.parseInt(br.readLine());
		char map[][] = new char[n][n];
		for(int i=0; i<n; i++) map[i] = br.readLine().toCharArray();
		
		for(int i=0; i<n; i++)
			for(int j=0; j<n; j++)
				solve(i,j,map);
		
		System.out.println(ans);
	}
}