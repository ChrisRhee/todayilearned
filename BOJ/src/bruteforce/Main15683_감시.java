package bruteforce;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * 이름 : 감시
 * 풀이 : 감시카메라의 모든 경우를 다 돌리면서 찾아야할듯 하다
 * */
class Main15683_감시{
	public static int N,M;
	public static int[] di= {-1,1,0,0};
	public static int[] dj= {0,0,-1,1};
	
	public static int[] di3= {-1,0,1,0};
	public static int[] dj3= {0,1,0,-1};
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj15683.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		
		N = Integer.parseInt(s[0]);
		M = Integer.parseInt(s[1]);
		int min = Integer.MAX_VALUE;
		int[][] cam = new int[2][8];
		int cindex = 0;
		
		String map[][] = new String[N][M];
		String temp[][] = new String[N][M];
		
		for(int i=0; i<N; i++) {
			map[i] = br.readLine().split(" ");
		}
		
		for(int i=0; i<N; i++) {
			for(int j=0; j<M; j++) {
				if(map[i][j].equals("1") || map[i][j].equals("3") || map[i][j].equals("4")) {
					cam[0][cindex] = Integer.parseInt(map[i][j]);
					cam[1][cindex] = 4;
					cindex++;
				}
				else if(map[i][j].equals("2")) {
					cam[0][cindex] = Integer.parseInt(map[i][j]);
					cam[1][cindex] = 2;
					cindex++;
				}
				else if(map[i][j].equals("5")) {
					beam(5,0,map,i,j);
				}
			}
		}
		
		while(true) {
			int index = 0;
			int cnt = 0;
			// temp 복제
			for(int i=0; i<N; i++) 
				for(int j=0; j<M; j++) 
					temp[i][j] = map[i][j];
			
			// 빔 다 쏨
			for(int i=0; i<N; i++) {
				for(int j=0; j<M; j++) {
					if(temp[i][j].equals("1")||temp[i][j].equals("2")||temp[i][j].equals("3")||temp[i][j].equals("4")) {
						if(cam[1][index]==0) continue;
						beam(Integer.parseInt(temp[i][j]), cam[1][index], temp, i,j);
						index++;
					}
				}
			}
			
			// 0 갯수 세자
			loop:for(int i=0; i<N; i++) {
				for(int j=0; j<M; j++) {
					if(temp[i][j].equals("0")) {
						cnt++;
						if(cnt > min) continue loop;
					}
				}
			}
			
			if(min > cnt) {
				min = cnt;
			}
			if(isDone(cam)) break;

			//하나 빼고
			cam[1][0]--;
			if(isDone(cam)) break;
			//계산하고
			if(cam[1][0]==0)
				setcount(cam);
		}
		
		System.out.println(min);
	}

	private static boolean isDone(int[][] cam) {
		for(int i=0; i<8; i++){
			if(cam[1][i] != 0)
				return false;
		}
		return true;
	}

	private static void setcount(int[][] cam) {
		for(int i=0; i<7; i++) {
			if(cam[0][i] > 0 && cam[1][i] <= 0) {
				if(cam[0][i+1]!=0 && cam[1][i+1] > 0) {
					cam[1][i+1]--;
					if(cam[0][i]==2) {
						cam[1][i] += 2;
					}
					else {
						cam[1][i] += 4;
					}
				}
			}
		}
	}

	private static void beam(int num, int dir, String[][] map, int i, int j) {
		int ii =0;
		int jj =0;
		switch(num) {
		case 1:
			ii = i;
			jj = j;
			while(true) {
				ii += di[dir-1];
				jj += dj[dir-1];
				if(ii < 0 || ii >= N || jj < 0 || jj >= M || map[ii][jj].equals("6")) break;
				if(map[ii][jj].equals("1") || map[ii][jj].equals("2") || map[ii][jj].equals("3") || map[ii][jj].equals("4")||map[ii][jj].equals("#")) continue;
				if(map[ii][jj].equals("0")) {
					map[ii][jj] = "#";
				}
			}
			break;
		case 2:
			for(int d = (dir-1)*2; d< dir*2 ; d++) {
				ii = i;
				jj = j;
				while(true) {
					ii += di[d];
					jj += dj[d];
					if(ii < 0 || ii >= N || jj < 0 || jj >= M || map[ii][jj].equals("6")) break;
					if(map[ii][jj].equals("1") || map[ii][jj].equals("2") || map[ii][jj].equals("3") || map[ii][jj].equals("4")||map[ii][jj].equals("#")) continue;
					if(map[ii][jj].equals("0")) {
						map[ii][jj] = "#";
					}
				}
			}
			break;
		case 3:
			for(int d = (dir-1); d<(dir-1)+2; d++) {
				if(dir == 4 && d== 1) break;
				ii = i;
				jj = j;
				while(true) {
					if(d == 4) d = 0;
					ii += di3[d];
					jj += dj3[d];
					if(ii < 0 || ii >= N || jj < 0 || jj >= M || map[ii][jj].equals("6")) break;
					if(map[ii][jj].equals("1") || map[ii][jj].equals("2") || map[ii][jj].equals("3") || map[ii][jj].equals("4")||map[ii][jj].equals("#")) continue;
					if(map[ii][jj].equals("0")) {
						map[ii][jj] = "#";
					}
				}
			}
			break;
		case 4:
			for(int d =0; d<4; d++) {
				ii = i;
				jj = j;
				if(dir-1 == d) continue;
				while(true) {
					ii += di[d];
					jj += dj[d];
					if(ii < 0 || ii >= N || jj < 0 || jj >= M || map[ii][jj].equals("6")) break;
					if(map[ii][jj].equals("1") || map[ii][jj].equals("2") || map[ii][jj].equals("3") || map[ii][jj].equals("4")||map[ii][jj].equals("#")) continue;
					if(map[ii][jj].equals("0")) {
						map[ii][jj] = "#";
					}
				}
			}
			break;
		case 5:
			for(int d =0; d<4; d++) {
				ii = i;
				jj = j;
				while(true) {
					ii += di[d];
					jj += dj[d];
					if(ii < 0 || ii >= N || jj < 0 || jj >= M || map[ii][jj].equals("6")) break;
					if(map[ii][jj].equals("1") || map[ii][jj].equals("2") || map[ii][jj].equals("3") || map[ii][jj].equals("4") ||map[ii][jj].equals("#")) continue;
					if(map[ii][jj].equals("0")) {
						map[ii][jj] = "#";
					}
				}
			}
			break;
		}
	}
}