package bruteforce;
import java.io.FileInputStream;
import java.util.*;

class Main1107_리모컨_brutrforce{
	public static int min, map[], minlen,  k;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj1107.txt"));
		Scanner sc = new Scanner(System.in);
		String N = sc.next();
		int len = N.length();
		int n = Integer.parseInt(N);
		k = 0;
		int c = sc.nextInt();
		int res = Math.abs(n-100);
		min = 500001;
		minlen = 500001;
		map = new int[10];
		for(int i=0; i<c; i++) {
			map[sc.nextInt()] = 1;
		}
		
		check(0,n,len,"");
		if( res  < min+minlen)
			System.out.println(res);
		else
			System.out.println(min+minlen);
		
	}
	
	private static void check(int count, int finalCH, int len, String val) {
		if(count == len+2) {
			return;
		}
		
		if(val.length()>0) {
			k = Math.abs(finalCH - Integer.parseInt(val));
			String s = ""+Integer.parseInt(val);
			if(min >= k) {
				min = k;
				minlen = s.length(); 
			}
		}
		
		for(int i=0; i<10; i++) {
			if(map[i] == 1)continue;
			check(count+1, finalCH, len, val+i);
		}
	}	
}