package bruteforce;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

// N개의 자연수 중에 M개를 고른 수열
class Main15654_N과M_5{
	public static int N,M, temp[];
	public static boolean[] visit;
	public static StringBuilder sb;
	
	private static void solve(int start, int count, String str) {
		if(count >= M) {
			sb.append(str).append("\n");
			return;
		}
		for(int i=start; i<N; i++) {
			if(visit[i] == true) continue;
			visit[i] = true;
			solve(0,count+1,str+temp[i]+" ");
			visit[i] = false;
		}
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj15653.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		N=Integer.parseInt(s[0]);
		M=Integer.parseInt(s[1]);
		temp = new int[N];
		visit = new boolean[N];
		sb = new StringBuilder();
		s = br.readLine().split(" ");
		for(int i=0; i<N; i++) {
			temp[i] = Integer.parseInt(s[i]);
		}
		Arrays.sort(temp);
		solve(0,0,"");
		System.out.println(sb.toString());
	}
}