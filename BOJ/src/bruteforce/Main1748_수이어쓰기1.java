package bruteforce;
import java.util.Scanner;

class Main1748_수이어쓰기1{
	public static int power(int a, int b) {
		int temp =1;
		for(int i=0; i<b; i++) temp *= a;
		return temp;
	}
	public static void main(String args[]) throws Exception	{
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		int ans =0;
		for(int i=1; i<10; i++) {
			int a = 9*power(10,i-1);
			if(T >= a) {
				T -= a;
				ans += a*i;
			}else {
				ans += T*i;
				break;
			}
		}
		System.out.println(ans);
	}
}