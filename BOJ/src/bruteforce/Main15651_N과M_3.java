package bruteforce;
import java.io.FileInputStream;
import java.util.*;

// 1부터 N까지 자연수 중에 M개를 고른 수열
// 같은 수를 여러번 골라도 된다.
class Main15651_N과M_3{
	public static int N,M;
	public static StringBuilder sb;
	
	private static void solve(int[] temp,int start, int count, String ans) {
		if(count >= M) {
			sb.append(ans).append("\n");
			return;
		}
		for(int i = 0; i<N; i++) {
			temp[count] = i+1;
			solve(temp,0,count+1, ans+(i+1)+" ");
		}
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj15651.txt"));
		Scanner sc = new Scanner(System.in);
		sb = new StringBuilder();
		N=sc.nextInt();
		M=sc.nextInt();
		int temp[] = new int[M];
		solve(temp,0,0,"");
		System.out.println(sb.toString());
	}
}