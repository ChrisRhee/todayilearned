package bruteforce;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

// 1부터 N까지 자연수 중에 중복 없이 M개를 고른 수열
// 고른 수열은 오름차순 이어야 한다.
class Main15650_N과M_2{
	public static int N,M, map[];
	public static boolean visit[];
	public static StringBuilder sb;
	
	private static void solve(int[] temp,int start, int count,String str) {
		if(count >= M) {
			sb.append(str).append("\n");
			return;
		}
		for(int i = start; i<N; i++) {
			if(visit[i] == true) continue;
			visit[i] = true;
			solve(temp,i+1,count+1, str+map[i]+" ");
			visit[i] = false;
		}
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj15650.txt"));
		Scanner sc = new Scanner(System.in);
		N=sc.nextInt();
		map = new int [N];
		sb = new StringBuilder();
		for(int i=1; i<=N; i++) map[i-1] = i;
		visit = new boolean[N];
		M=sc.nextInt();
		int temp[] = new int[M];
		solve(temp,0,0,"");
		System.out.println(sb.toString());
	}
}