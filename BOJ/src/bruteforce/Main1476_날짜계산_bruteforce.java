package bruteforce;
import java.io.*;
import java.util.Scanner;

class Main1476_날짜계산_bruteforce{
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj1476.txt"));
		Scanner sc = new Scanner(System.in);
		int e = sc.nextInt();
		int s = sc.nextInt();
		int m = sc.nextInt();
		
		int map[] = new int[3];
		map[0] = 0;
		map[1] = 0;
		map[2] = 0;
		for(int i=1; i<=7981; i++) {
			map[0]++;
			map[1]++;
			map[2]++;
			if(map[0] >15) map[0] -= 15;
			if(map[1] >28) map[1] -= 28;
			if(map[2] >19) map[2] -= 19;
			if(e == map[0] && s == map[1] && m == map[2]) {
				System.out.println(i);
				break;
			}
		}
		
	}
}