package bruteforce;
import java.io.FileInputStream;
import java.util.*;

// 1부터 N까지 자연수 중에 M개를 고른 수열
// 같은 수를 여러번 골라도 된다.
// 고른 수열은 내림차순 이어야 한다.
class Main15652_N과M_4{
	public static int N,M;
	public static StringBuilder sb;
	
	private static void solve(int start, int count, String str) {
		if(count >= M) {
			sb.append(str).append("\n");
			return;
		}
		for(int i=start; i<N; i++) {
			solve(i,count+1,str+(i+1)+" ");
		}
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj15651.txt"));
		Scanner sc = new Scanner(System.in);
		sb = new StringBuilder();
		N=sc.nextInt();
		M=sc.nextInt();
		solve(0,0,"");
		System.out.println(sb.toString());
	}
}