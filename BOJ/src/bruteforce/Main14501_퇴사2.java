package bruteforce;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

class Main14501_퇴사2{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj14501.txt"));
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		int max = 0;
		int map[][] = new int[T][2];
		for(int i=0; i<T; i++) {
			map[i][0] = sc.nextInt();
			map[i][1] = sc.nextInt();
		}
		
		int db[] = new int[T+1];
		for(int i= T-1; i>=0; i--) {
			if(db[i] == 0) db[i] = db[i+1];
			if(map[i][0]+i > T) continue;
			db[i] = Math.max((db[i+map[i][0]] + map[i][1]) , db[i]);
			max = Math.max(db[i],max);
			//System.out.println(Arrays.toString(db));
		}
		
		System.out.println(max);
	}
}