package bruteforce;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

// N개의 자연수 중에 M개를 고른 수열
// 고른수열 오른차순
class Main15664_N과M_10{
	public static int N,M, num[];
	public static boolean visit[];
	public static List<String> s; 
	public static StringBuilder sb;
	private static void solve(int count,int start, String val) {
		if(count == M ) {
			if(s.contains(val)) return;
			s.add(val);
			sb.append(val).append("\n");
			return;
		}
		
		for(int i=start; i<N; i++) {
			if(visit[i] == true) continue;
			visit[i] = true;
			solve(count+1,i+1,val+num[i]+" ");
			visit[i] = false;
		}
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj15656.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String ss[] = br.readLine().split(" ");
		N=Integer.parseInt(ss[0]);
		M=Integer.parseInt(ss[1]);
		sb = new StringBuilder();
		num = new int[N];
		visit = new boolean[N];
		s= new ArrayList<>();
		ss = br.readLine().split(" ");
		for(int i=0; i<N; i++) {
			num [i] = Integer.parseInt(ss[i]);
		}
		Arrays.sort(num);
		solve(0,0,"");
		System.out.println(sb.toString());
	}
	
}