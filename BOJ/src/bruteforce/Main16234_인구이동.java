package bruteforce;
import java.io.*;
import java.util.*;

class Main16234_인구이동{
	public static int N,L,R;
	public static int[] di= {-1,1,0,0};
	public static int[] dj= {0,0,-1,1};
	public static boolean visit[][];
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj16234.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		N = Integer.parseInt(s[0]);
		L = Integer.parseInt(s[1]);
		R = Integer.parseInt(s[2]);
		
		int ans = 0;
		int map[][] = new int[N*2-1][N*2-1];
		for(int i=0; i<N; i++) {
			s = br.readLine().split(" ");
			for(int j=0; j<N; j++) 
				map[i*2][j*2] = Integer.parseInt(s[j]);
		}
		
		while(true) {
			if(check(map)) break;
//			for (int[] is : map) System.out.println(Arrays.toString(is));
//			System.out.println();
			visit = new boolean[N*2-1][N*2-1];
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					if(visit[i*2][j*2]== true) continue;
					bfs(map,i*2,j*2);
				}
			}
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					if(i*2+1 < N*2-1 ) {
						map[i*2+1][j*2] = 0;
					}
					if(j*2+1 < N*2-1 ) {
						map[i*2][j*2+1] = 0;
					}
				}
			}
			ans++;
		}
		//for (int[] is : map) System.out.println(Arrays.toString(is));
		System.out.println(ans);
	}

	private static void bfs(int[][] map, int i, int j) {
		List<int[]> l = new ArrayList<>();
		Queue<int[]> q = new LinkedList<>();
		int sum = map[i][j];
		int count = 1;
		l.add(new int[] {i,j});
		q.offer(new int[] {i,j});
		visit[i][j] = true;
		while(!q.isEmpty()) {
			int[] curr = q.poll();
			for(int d= 0; d<4; d++) {
				int ii = curr[0]+di[d]*2;
				int i2 = curr[0]+di[d];
				int jj = curr[1]+dj[d]*2;
				int j2 = curr[1]+dj[d];
				if(ii < 0 || ii >= N*2-1 || jj < 0 || jj >= N*2-1) continue;
				if(map[i2][j2]== -1 && visit[ii][jj] == false) {
					visit[i2][j2] = true;
					visit [ii][jj] = true;
					q.offer(new int[] {ii,jj});
					sum += map[ii][jj];
					l.add(new int[] {ii,jj});
					count ++;
				}
			}
		}
		if(count == 1) return;
		int t = sum/count;
		for(int k=0; k<l.size(); k++) {
			map[l.get(k)[0]][l.get(k)[1]] = t;
		}
	}

	private static boolean check(int[][] map) {
		boolean check = true;
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				if((i+1)*2 < N*2-1 && Math.abs(map[i*2][j*2] - map[(i+1)*2][j*2]) >= L &&
					Math.abs(map[i*2][j*2] - map[(i+1)*2][j*2]) <= R) {
					map[i*2+1][j*2] = -1;
					check = false;
				}
				if((j+1)*2 < N*2-1 && Math.abs(map[i*2][j*2] - map[i*2][(j+1)*2]) >= L &&
						Math.abs(map[i*2][j*2] - map[i*2][(j+1)*2]) <= R) {
					map[i*2][j*2+1] = -1;
					check = false;
				}
			}
		}
		return check;
	}
}