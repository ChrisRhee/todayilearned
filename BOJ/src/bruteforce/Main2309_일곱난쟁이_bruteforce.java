package bruteforce;
import java.io.FileInputStream;
import java.util.*;

class Main2309_일곱난쟁이_bruteforce{
	public static int[] ans;
	
	private static void check(int[] map, int count, int val, boolean[] visit) {
		if(count == 7) {
			if(val == 100) {
				int index = 0;
				for(int i=0 ;i<9; i++) {
					if(visit[i]== true) {
						ans[index] = map[i];
						index++;
					}
				}
			}
			return;
		}
		
		for(int i=0; i<9; i++) {
			if(visit[i]==true) continue;
			visit[i] = true;
			check(map,count+1,val+map[i],visit);
			visit[i] = false;
		}
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj2309.txt"));
		Scanner sc = new Scanner(System.in);
		int[] map = new int[9];
		ans = new int [7];
		boolean visit[] = new boolean[9];
		for(int i=0; i<9; i++) {
			map[i] = sc.nextInt();
		}
		check(map, 0, 0, visit);
		Arrays.sort(ans);
		for(int i=0; i<7; i++) {
			System.out.println(ans[i]);
		}
	}
	
}