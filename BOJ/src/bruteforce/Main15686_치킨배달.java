package bruteforce;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Main15686_치킨배달{
	public static int min;
	public static int result;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj15686.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		int N = Integer.parseInt(s[0]);
		int M = Integer.parseInt(s[1]);
		min = Integer.MAX_VALUE;
		List<int[]> ch = new ArrayList<>();
		List<int[]> home = new ArrayList<>();
		for(int i=0; i<N; i++) {
			s = br.readLine().split(" ");
			for (int j=0; j<N; j++) {
				int t = Integer.parseInt(s[j]);
				if(t == 1) {
					home.add(new int[] {i,j});
				}else if( t== 2) {
					ch.add(new int[] {i,j});
				}
			}
		}
		boolean visit[] = new boolean[ch.size()];
		solve(0,0,M,ch,home,visit);
		System.out.println(min);
		System.out.println(result);
	}

	private static void solve(int index,int count, int m, List<int[]> ch, List<int[]> home, boolean[] visit) {
		if(count == m) {
			int res = calc(ch,home,visit);
			if(min > res) {
				min = res;
			}
			result++;
			return;
		}
		
		for(int i=index; i<ch.size(); i++) {
			if(visit[i]== true) continue;
			visit[i] = true;
			solve(i+1,count+1, m, ch, home, visit);
			visit[i] = false;
		}
	}

	private static int calc(List<int[]> ch, List<int[]> home, boolean[] visit) {
		int result = 0;
		for(int i=0; i<home.size(); i++) {
			int min = Integer.MAX_VALUE;
			for(int j=0; j<ch.size(); j++) {
				if(visit[j] == false) continue;
				int ans = Math.abs(home.get(i)[0] - ch.get(j)[0]) + Math.abs(home.get(i)[1] - ch.get(j)[1]);
				if(min > ans) min = ans;
			}
			result += min;
		}
		return result;
	}
}