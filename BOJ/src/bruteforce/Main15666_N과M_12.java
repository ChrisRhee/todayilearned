package bruteforce;

import java.io.*;
import java.util.*;

// N개의 자연수 중에 M개를 고른 수열
// 고른수열 오른차순
class Main15666_N과M_12{
	public static int N,M, nums[];
	public static StringBuilder sb;
	public static List<Integer> l;
	
	private static void solve(int count, int start, String str) {
		if(count == M) {
			sb.append(str).append("\n");
			return;
		}
		for(int i=start; i<l.size(); i ++) 
			solve(count+1, i, str+l.get(i)+" ");
	}
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj15665.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		N=Integer.parseInt(s[0]);
		M=Integer.parseInt(s[1]);
		sb = new StringBuilder();
		s = br.readLine().split(" ");
		nums = new int[s.length];
		l = new ArrayList<>();
		for(int i=0; i<s.length; i++) {
			nums[i] = Integer.parseInt(s[i]);
			if(!l.contains(nums[i])) l.add(nums[i]);
		}
		Collections.sort(l);
		solve(0,0,"");
		System.out.println(sb.toString());
	}
}