package bruteforce;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

class Main2630_색종이만들기{
	public static int N;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj2630.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		int map[][] = new int[N][N];
		boolean visit[][] = new boolean[N][N];
		
		String s[] = null;
		int ans = 0;
		int ans2 = 0;
		for(int i=0; i<N; i++) {
			s = br.readLine().split(" ");
			for (int j=0; j<N; j++) {
				map[i][j] = Integer.parseInt(s[j]);
			}
		}
		int temp = N;
		int tempcnt = 0;
		while(true) {
			if(temp == 1) break;
			temp /= 2;
			tempcnt++;
		}
		int ps[] = new int[1+tempcnt];
		int ps2[] = new int[1+tempcnt];
		
		int width = 0;
		for(int k=tempcnt; k>=0; k--) {
			width = (int)Math.pow(2,k);
			for(int i=0; i<N; i+= width) {
				for(int j=0; j<N; j+= width) {
					if(visit[i][j]==true) continue;
					boolean c = check(i,j,k,ps,map,visit);
					if(c) vckech(i,j,k,width,visit);
					boolean c2 = check2(i,j,k,ps2,map,visit);
					if(c2) vckech(i,j,k,width,visit);
				}
			}
		}
		//System.out.println(Arrays.toString(ps));
		//System.out.println(Arrays.toString(ps2));
		for(int i=0; i<tempcnt; i++) {
			ans += ps2[i];
			ans2 += ps[i];
		}
		System.out.println(ans);
		System.out.println(ans2);
	}

	private static boolean check2(int i, int j, int k, int[] ps, int[][] map, boolean[][] visit) {
		for(int ii=i; ii<(int)Math.pow(2,k)+i; ii++) {
			for(int jj=j; jj<(int)Math.pow(2,k)+j; jj++) {
				if(map[ii][jj]==1) return false;
			}
		}
		ps[k]++;
		return true;
	}

	private static void vckech(int i, int j,  int k, int width, boolean[][] visit) {
		for(int ii=i; ii<width+i; ii++) {
			for(int jj=j; jj<width+j; jj++) {
				visit[ii][jj] = true;
			}
		}
	}

	private static boolean check(int i, int j, int k, int[] ps, int map[][], boolean[][] visit) {
		for(int ii=i; ii<(int)Math.pow(2,k)+i; ii++) {
			for(int jj=j; jj<(int)Math.pow(2,k)+j; jj++) {
				if(map[ii][jj]==0) return false;
			}
		}
		ps[k]++;
		return true;
	}
}