package simulation;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

class Main14891_톱니바퀴{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj14891.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int ans = 0;
		int top[][] = new int[5][8];
		
		String s[] = null;
		for(int i=1; i<5; i++) {
			s = br.readLine().split("");
			for(int j=0; j<8;j++) {
				top[i][j] = Integer.parseInt(s[j]);
			}
		}
		int N = Integer.parseInt(br.readLine());
		for(int i=0; i<N; i++) {
			s = br.readLine().split(" ");
			boolean visit[] = new boolean[5];
			check(top, Integer.parseInt(s[0]),Integer.parseInt(s[1]), visit);
		}
		
		if(top[1][0]==1) ans += 1;
		if(top[2][0]==1) ans += 2;
		if(top[3][0]==1) ans += 4;
		if(top[4][0]==1) ans += 8;
		System.out.println(ans);
	}
	
	
	private static void check(int[][] top, int tnum, int dir, boolean[] visit) {
		if(visit[tnum]==true) return;
		
		
		switch(tnum) {
		case 1:
			if(top[1][2] != top[2][6]) {
				visit[tnum] = true;
				check(top,2,dir*-1,visit);
			}
			break;
		case 2:
			if(top[2][2] != top[3][6]) {
				visit[tnum] = true;
				check(top,3,dir*-1,visit);
			}
			if(top[1][2] != top[2][6]) {
				visit[tnum] = true;
				check(top,1,dir*-1,visit);
			}
			break;
		case 3:
			if(top[3][2] != top[4][6]) {
				visit[tnum] = true;
				check(top,4,dir*-1,visit);
			}
			if(top[2][2] != top[3][6]) {
				visit[tnum] = true;
				check(top,2,dir*-1,visit);
			}
			break;
		case 4:
			if(top[4][6] != top[3][2]) {
				visit[tnum] = true;
				check(top,3,dir*-1,visit);
			}
			break;
		}
		//체크 다 하고 돌림
		spin(top,tnum,dir);
	}

	private static void spin(int[][] top, int tnum, int dir) {
		if(dir==1) {
			int t = top[tnum][0];
			top[tnum][0] = top[tnum][7];
			top[tnum][7] = top[tnum][6];
			top[tnum][6] = top[tnum][5];
			top[tnum][5] = top[tnum][4];
			top[tnum][4] = top[tnum][3];
			top[tnum][3] = top[tnum][2];
			top[tnum][2] = top[tnum][1];
			top[tnum][1] = t;
		}else {
			int t = top[tnum][0];
			top[tnum][0] = top[tnum][1];
			top[tnum][1] = top[tnum][2];
			top[tnum][2] = top[tnum][3];
			top[tnum][3] = top[tnum][4];
			top[tnum][4] = top[tnum][5];
			top[tnum][5] = top[tnum][6];
			top[tnum][6] = top[tnum][7];
			top[tnum][7] = t;
		}
	}
}