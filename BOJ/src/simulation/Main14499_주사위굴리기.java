package simulation;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

class Main14499_주사위굴리기{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj14499.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		
		int N = Integer.parseInt(s[0]);
		int M = Integer.parseInt(s[1]);
		int X = Integer.parseInt(s[2]);
		int Y = Integer.parseInt(s[3]);
		int num = Integer.parseInt(s[4]);
		int map[][] = new int[N][M];
		
		for(int i=0 ; i<N; i++) {
			s = br.readLine().split(" ");
			for (int j=0; j<M; j++) {
				map[i][j] = Integer.parseInt(s[j]);
			}
		}
		
		
		int dice[][] = new int[4][3];
		if(map[X][Y] == 0)
			map[X][Y] = dice[3][1];
		else {
			dice[3][1] = map[X][Y];
			map[X][Y] = 0;
		}
		
		s = br.readLine().split(" ");
		for(int i=0; i<num; i++) {
			int d = Integer.parseInt(s[i]); 
			//System.out.println("d="+d);
			switch(d) {
			case 1:
				Y = Y + 1;
				if(Y >= M) {
					Y = Y - 1;
					continue;
				}
				break;
			case 2:
				Y = Y - 1;
				if( Y < 0) {
					Y = Y + 1;
					continue;
				}
				break;
			case 3:
				X = X - 1;
				if(X < 0) {
					X = X + 1;
					continue;
				}
				break;
			case 4:
				X = X + 1;
				if(X >= N) {
					X = X - 1;
					continue;
				}
				break;
			}
			roll(d,dice);
			//for (int[] is : dice) System.out.println(Arrays.toString(is));
			//System.out.println(map[X][Y]+" X="+X+" Y="+Y);
			if(map[X][Y] == 0)
				map[X][Y] = dice[3][1];
			else {
				dice[3][1] = map[X][Y];
				map[X][Y] = 0;
			}
			
			System.out.println(dice[1][1]);
			
		}
	}

	// 1:동 2:서 3:남 4:북
	private static void roll(int dir, int[][] dice) {
		int temp = dice[1][1];
		switch(dir) {
		case 1:
			dice[1][1] = dice[1][0];
			dice[1][0] = dice[3][1];
			dice[3][1] = dice[1][2];
			dice[1][2] = temp;
			break;
		case 2:
			dice[1][1] = dice[1][2];
			dice[1][2] = dice[3][1];
			dice[3][1] = dice[1][0];
			dice[1][0] = temp;
			break;
		case 3:
			dice[1][1] = dice[2][1];
			dice[2][1] = dice[3][1];
			dice[3][1] = dice[0][1];
			dice[0][1] = temp;
			break;
		case 4:
			dice[1][1] = dice[0][1];
			dice[0][1] = dice[3][1];
			dice[3][1] = dice[2][1];
			dice[2][1] = temp;
			break;
		}
	}
}