package simulation;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

class Main15685_드래곤커브{
	public static int[] di = {0,-1,0,1};
	public static int[] dj = {1,0,-1,0};
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj15685.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(br.readLine());
		List<int[]> d = new ArrayList<>();
		int map[][] = new int[101][101];
		// 0 = 우
		// 1 = 상
		// 2 = 좌
		// 3 = 하
		d.add(new int[] {0});
		for(int i=1; i<11; i++) {
			int[] t = new int[d.get(i-1).length];
			int[] t2 = new int[d.get(i-1).length];
			for(int j=0; j<d.get(i-1).length; j++) {
				t[j] = d.get(i-1)[j];
				t2[j] = d.get(i-1)[j];
			}
			for(int j=0; j<t2.length; j++) {
				t2[j] --;
				if(t2[j] <0) t2[j] += 4;
				t2[j] += 2;
				t2[j] %= 4;
			}
			int[] t3 = new int[t.length*2];
			for(int j=0; j<t.length; j++) t3[j] = t[j];
			for(int j=0; j<t2.length; j++) 
				t3[t2.length+j] = t2[(t2.length-1) - j];
			d.add(t3);
		}
		
		for(int i=0; i<N; i++) {
			String s[] = br.readLine().split(" ");
			int draJ = Integer.parseInt(s[0]);
			int draI = Integer.parseInt(s[1]);
			int dir = Integer.parseInt(s[2]);
			int age = Integer.parseInt(s[3]);
			map[draI][draJ]++;
			int curr[] = new int[d.get(age).length];
			for(int j=0; j<d.get(age).length; j++) {
				curr[j] = d.get(age)[j];
			}
			int ii= draI;
			int jj= draJ;
			for(int j=0; j<curr.length; j++) {
				curr[j] = (curr[j] + dir)%4;
				ii= ii+di[curr[j]];
				jj= jj+dj[curr[j]];
				map[ii][jj]++;
			}
		}
		
		int cnt = 0;
		for(int i=0; i<100; i++) {
			for (int j=0; j<100; j++) {
				if(map[i][j] > 0 && map[i+1][j] >0 && map[i][j+1]>0 && map[i+1][j+1] >0)
					cnt++;
			}
		}
		System.out.println(cnt);
	}
}