package simulation;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

class Main17144_미세먼지안녕{
	public static int[] di = {-1,0,1,0};
	public static int[] dj = {0,1,0,-1};
	public static int[] ddi = {1,0,-1,0};
	public static int[] ddj = {0,1,0,-1};
	public static int N,M;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj17144.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		int count = 0;
		N = Integer.parseInt(s[0]);
		M = Integer.parseInt(s[1]);
		int T = Integer.parseInt(s[2]);
		
		int map[][] = new int[N][M];
		int temp[][] = new int[N][M];
		int ui = -1;
		int uj = -1;
		int di = -1;
		int dj = -1;
		for(int i=0; i<N; i++) {
			s = br.readLine().split(" ");
			for(int j=0; j<M; j++) {
				map[i][j] = Integer.parseInt(s[j]);
			}
		}
		
		//for (int[] is : map) System.out.println(Arrays.toString(is));
		
		for(int t=0; t<T; t++) {
			count = 0;
			for(int i=0; i<N; i++) {
				for(int j=0; j<M; j++) {
					if(map[i][j] == 0) continue;
					if(map[i][j] == -1) {
						if(ui == -1) {
							ui = i;
							uj = j;
						}else {
							di = i;
							dj = j;
						}
						continue;
					}
					munji(i,j,map,temp);
				}
			}
			
			chungUp(ui,uj,temp);
			chungDown(di,dj,temp);
			for(int i=0; i<N; i++) {
				for(int j=0; j<M; j++) {
					if(map[i][j] == -1)continue;
					map[i][j] = temp[i][j];
					temp[i][j] = 0;
					count += map[i][j];
				}
			}
		}
		System.out.println(count);
	}

	private static void chungDown(int i, int j, int[][] map) {
		int bi = i;
		int bj = j;
		for(int d=0; d<4; d++) {
			int ii = bi+ddi[d];
			int jj = bj+ddj[d];
			if(ii == i && jj == j) return;
			if( ii < i || ii >= N || jj < 0 || jj >= M ) continue;
			d--;
			if(map[bi][bj] == -1) {
				
			}else {
				map[bi][bj] = map[ii][jj];
			}
			map[ii][jj] = 0;
			bi = ii;
			bj = jj;
		}
	}

	private static void chungUp(int i, int j, int[][] map) {
		int bi = i;
		int bj = j;
		for(int d=0; d<4; d++) {
			
			int ii = bi+di[d];
			int jj = bj+dj[d];
			if(ii == i && jj == j) return;
			if( ii <0 || ii > i || jj < 0 || jj >= M ) {
				continue;
			}
			d--;
			if(map[bi][bj] == -1) {
				
			}else {
				map[bi][bj] = map[ii][jj];
			}
			map[ii][jj] = 0;
			bi = ii;
			bj = jj;
		}
	}

	private static void munji(int i, int j, int[][] map, int[][] temp) {
		int t = map[i][j];
		for(int d = 0; d<4; d++) {
			int ii = i+di[d];
			int jj = j+dj[d];
			if( ii <0 || ii >= N || jj < 0 || jj >= M || map[ii][jj] == -1) continue;
			
			temp[ii][jj] += map[i][j]/5;
			t -= map[i][j]/5;
		}
		temp[i][j] += t;
	}
}