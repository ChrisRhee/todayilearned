package simulation;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
class Main14890_경사로{
	public static int Ans,N,L;
	public static int map[][];
	public static int visit[][];
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input4014.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		//int T= Integer.parseInt(br.readLine());
			Ans =0;
			String s[] = br.readLine().split(" ");
			N= Integer.parseInt(s[0]);
			L= Integer.parseInt(s[1]);
			map = new int[N][N];
			visit = new int[N][N];
			for(int i =0; i<N; i++) {
				s = br.readLine().split(" ");
				for(int j=0; j<N; j++) {
					map[i][j] = Integer.parseInt(s[j]);
				}
			}
			
			// 좌-> 우
			for(int i=0; i<N; i++) {
				loop:for(int j=0; j<N+1-L; j++) {
					double num = map[i][j];
					if(visit[i][j] != 0 || j+L >= N) continue;
					for(int k=0; k<L; k++) if(num != map[i][j+k]) continue loop;
					if(map[i][j+L-1] < map[i][j+L]) 
						for(int k=0; k<L; k++) 
							visit[i][j+k] = 4;
				}
			}
			
			
			// 우 -> 좌
			for(int i=0; i<N; i++) {
				loop:for(int j=N-1; j >= L-1; j--) {
					double num = map[i][j];
					if(visit[i][j] != 0 || j-L < 0) continue;
					for(int k=0; k<L; k++) if(num != map[i][j-k]) continue loop;
					if(map[i][j-L+1] < map[i][j-L]) 
						for(int k=0; k<L; k++) 
							visit[i][j-k] = 3;
				}
			}
			
//			for (int[] bs : visit) System.out.println(Arrays.toString(bs));
//			System.out.println();
			for(int i=0; i<N; i++) {
				check(i,0);
			}
			
			visit = new int[N][N];
			// 상-> 하
			for(int i=0; i<N; i++) {
				loop:for(int j=0; j<N+1-L; j++) {
					double num = map[j][i];
					if(visit[j][i] != 0 || j+L >= N) continue;
					for(int k=0; k<L; k++) if(num != map[j+k][i]) continue loop;
					if(map[j+L-1][i] < map[j+L][i]) 
						for(int k=0; k<L; k++) 
							visit[j+k][i] = 2;
				}
			}
			// 하 -> 상
			for(int i=0; i<N; i++) {
				loop:for(int j=N-1; j >= L-1; j--) {
					double num = map[j][i];
					if(visit[j][i] != 0 || j-L < 0) continue;
					for(int k=0; k<L; k++) if(num != map[j-k][i]) continue loop;
					if(map[j-L+1][i] < map[j-L][i]) 
						for(int k=0; k<L; k++) 
							visit[j-k][i] = 1;
				}
			}
						
			
//			for (int[] bs : visit) System.out.println(Arrays.toString(bs));
			
			for(int i=0; i<N; i++) {
				check2(0,i);
			}
			
//			for (double[] ds : map) System.out.println(Arrays.toString(ds));
//			System.out.println();
//			for (double[] ds : map2) System.out.println(Arrays.toString(ds));
			
			System.out.println(Ans);
	}
	private static void check(int i, int j) {
		for( j=0; j<N-1; j++) {
			if(Math.abs(map[i][j]-map[i][j+1]) >1 ) {
				return;
			}
			else if( map[i][j]- map[i][j+1] == -1 && visit[i][j] != 4 ) return;
			else if( map[i][j]- map[i][j+1] == 1 && visit[i][j+1] != 3) return;
		}
		//System.out.println(i);
		Ans++;
	}
	
	
	private static void check2(int i, int j) {
		for( i=0; i<N-1; i++) {
			if(Math.abs(map[i][j]-map[i+1][j]) >1 ) {
				return;
			}
			else if( map[i][j]- map[i+1][j] == -1 && visit[i][j] != 2) return;
			else if( map[i][j]- map[i+1][j] == 1 && visit[i+1][j] != 1) return;
		}
		//System.out.println(j);
		Ans++;
	}
	
}