package simulation;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

class Main14888_연산자끼워넣기{
	public static int min,max;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj14888.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(br.readLine());
		String s[] = br.readLine().split(" ");
		min = Integer.MAX_VALUE;
		max = Integer.MIN_VALUE;
		int num[] = new int[N];
		for(int i=0; i<N; i++) {
			num[i] = Integer.parseInt(s[i]);
		}
		int oper[] = new int[4];
		
		s = br.readLine().split(" ");
		for(int i=0; i<4; i++) {
			oper[i] = Integer.parseInt(s[i]);
		}
		//System.out.println(Arrays.toString(num));
		//System.out.println(Arrays.toString(oper));
		solve(num,oper,N,0,"");
		System.out.println(max);
		System.out.println(min);
	}

	private static void solve(int[] num, int[] oper, int n, int count, String val) {
		if(count >= n-1) {
			//계산 고고
			int t = calc(num,val);
			if(min > t ) min = t;
			if(max < t ) max = t;
			return;
		}
		int to[] = new int[4];
		for(int i=0; i<4; i++) {
			to[i] = oper[i];
		}
		
		for(int i=0; i<4; i++) {
			if(to[i]==0) continue;
			to[i] -= 1;
			solve(num,to,n,count+1,val+i+" ");
			to[i] += 1;
		}
	}

	private static int calc(int[] num, String val) {
		String[] s = val.split(" ");
		int sum = num[0];
		for(int i=0; i<s.length; i++) {
			switch(s[i]) {
			case "0": // +
				sum = sum + num[i+1];
				break;
			case "1": // -
				sum = sum - num[i+1];
				break;
			case "2": // *
				sum = sum * num[i+1];
				break;
			case "3": // /
				if(sum < 0) {
					sum = ((sum*-1) / num[i+1]) * -1;
				}
				else {
					sum = sum / num[i+1];
				}
				break;
			}
		}
		return sum;
	}
}