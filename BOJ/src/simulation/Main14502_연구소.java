package simulation;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

class Main14502_연구소{
	
	public static int N,M,max;
	public static int[] di= {-1,1,0,0};
	public static int[] dj= {0,0,-1,1};
	public static Queue<int[]> q;
	public static boolean[][] v;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj14502.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		N = Integer.parseInt(s[0]);
		M = Integer.parseInt(s[1]);
		q = new LinkedList<>();
		
		max = 0;
		int[][] map = new int [N][M];
		boolean[][] visit = new boolean[N][M];
		for(int i=0; i<N; i++) {
			s= br.readLine().split(" ");
			for(int j=0; j<M; j++) {
				map[i][j] = Integer.parseInt(s[j]);
			}
		}
		
		wall(0,map, visit);
		System.out.println(max);
	}

	private static void wall(int count, int[][] map, boolean[][] visit) {
		if(count == 3) {
			//for (int[] is : map) System.out.println(Arrays.toString(is));
			//System.out.println();
			v = new boolean[N][M];
			int safe = 0;
			int temp[][] = new int[N][M];
			for(int i=0; i<N; i++) {
				for(int j=0; j<M; j++) {
					temp[i][j] = map[i][j];
				}
			}
			
			for(int i=0; i<N; i++) {
				for(int j=0; j<M; j++) {
					if(temp[i][j]==1 || v[i][j] ==true || temp[i][j] == 0) continue;
					bfs(i,j,temp);
				}
			}
			for(int i=0; i<N; i++) {
				for(int j=0; j<M; j++) {
					if(temp[i][j]==0) safe++;
				}
			}
			if(max < safe) {
				max = safe;
			}
			return;
		}
		
		int temp[][] = new int[N][M];
		
		for(int i=0; i<N; i++) {
			for(int j=0; j<M; j++) {
				temp[i][j] = map[i][j];
			}
		}
		
		for(int i=0; i<N; i++) {
			for(int j=0; j<M; j++) {
				if(temp[i][j] > 0) continue;
				if(visit[i][j] == true) continue;
				temp[i][j] = 1;
				visit[i][j] = true;
				wall(count+1, temp, visit);
				visit[i][j] = false;
				temp[i][j] = 0;
			}
		}
		
	}

	private static void bfs(int i, int j, int[][] map) {
		q.offer(new int[] {i,j});
		v[i][j] = true;
		while(!q.isEmpty()) {
			int curr[] = q.poll();
			for(int d=0; d<4; d++) {
				int ii= curr[0]+di[d];
				int jj= curr[1]+dj[d];
				if(ii < 0 || ii >= N || jj < 0 || jj >= M || map[ii][jj] > 0) continue;
				
				if(v[ii][jj]== false && map[ii][jj] == 0) {
					v[ii][jj] = true;
					q.offer(new int[] {ii,jj});
					map[ii][jj] = 2;
				}
			}
		}
	}
}