package simulation;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

class Main13460_구슬탈출2{
	public static int N,M, min;
	public static boolean Rfin,Bfin, fin, ff;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj13460.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		N = Integer.parseInt(s[0]);
		M = Integer.parseInt(s[1]);
		char[][] map = new char[N][M];
		min = Integer.MAX_VALUE;
		fin = false;
		Rfin = false;
		Bfin = false;
		ff = false;
		for(int i=0; i<N; i++) {
			map[i] = br.readLine().toCharArray();
		}
		solve(0,map);
		
		if(fin == false) System.out.println(-1);
		else System.out.println(min);
	}

	private static void solve(int count, char[][] map) {
		if(count > min) return;
		if(Bfin == true) {
			return;
		}
		if(Rfin == true && Bfin == false) {
			fin = true;
			if(min > count) min = count;
			return;
		}
		if(count == 10) {
			return;
		}
		
		for(int i=1 ; i<=4; i++) {
			char [][] temp = new char[N][M];
			for(int j=0; j<N; j++) {
				for(int k=0; k<M; k++) {
					temp[j][k] = map[j][k];
				}
			}
			
			Rfin = false;
			Bfin = false;
			move(i,temp);
			if(Rfin == true && Bfin == false) {
				fin = true;
			}
			solve(count+1, temp);
		}
	}
	
	private static void move(int dir, char[][] map) {
		switch(dir) {
			case 1:
				for(int j=0; j<M; j++) {
					int ii = -1;
					int jj = -1;
					char curr = 'x';
					for(int i=0; i<N-1; i++) {
						if(map[i][j]=='.' && ii == -1 && curr=='x') {
							ii = i;
							jj = j;
							curr = '.';
						}
						if(map[i][j]=='O') {
							ii = i;
							jj = j;
							curr = 'O';
						}
						
						if(map[i][j]=='#' && curr !='x') {
							ii = -1;
							jj = -1;
							curr = 'x';
						}
						
						if(ii > -1 && curr =='.' && (map[i+1][j] == 'R' || map[i+1][j] == 'B') ){
							swap(ii,jj,i+1,j,map);
							i = ii;
							ii = -1;
							jj = -1;
							curr = 'x';
						}
						
						if(ii > -1 && curr =='O' && map[i+1][j] == 'R'){
							map[i+1][j] = '.';
							Rfin = true;
						}
						
						if(ii > -1 && curr =='O' && map[i+1][j] == 'B'){
							map[i+1][j] = '.';
							Bfin = true;
							return;
						}
					}
				}
				break;
			case 2:
				for(int j=0; j<M; j++) {
					int ii = -1;
					int jj = -1;
					char curr = 'x';
					for(int i=N-1; i>=1; i--) {
						if(map[i][j]=='.' && ii == -1 && curr=='x') {
							ii = i;
							jj = j;
							curr = '.';
						}
						if(map[i][j]=='O') {
							ii = i;
							jj = j;
							curr = 'O';
						}
						
						if(map[i][j]=='#' && curr !='x') {
							ii = -1;
							jj = -1;
							curr = 'x';
						}
						
						if(ii > -1 && curr =='.' && (map[i-1][j] == 'R' || map[i-1][j] == 'B') ){
							swap(ii,jj,i-1,j,map);
							i = ii;
							ii = -1;
							jj = -1;
							curr = 'x';
						}
						
						if(ii > -1 && curr =='O' && map[i-1][j] == 'R'){
							map[i-1][j] = '.';
							Rfin = true;
						}
						
						if(ii > -1 && curr =='O' && map[i-1][j] == 'B'){
							map[i-1][j] = '.';
							Bfin = true;
							return;
						}
					}
				}
				break;
			case 3:
				for(int i=0; i<N; i++) {
					int ii = -1;
					int jj = -1;
					char curr = 'x';
					for(int j=0; j<M-1; j++) {
						if(map[i][j]=='.' && ii == -1 && curr=='x') {
							ii = i;
							jj = j;
							curr = '.';
						}
						if(map[i][j]=='O') {
							ii = i;
							jj = j;
							curr = 'O';
						}
						
						if(map[i][j]=='#' && curr !='x') {
							ii = -1;
							jj = -1;
							curr = 'x';
						}
						
						if(ii > -1 && curr =='.' && (map[i][j+1] == 'R' || map[i][j+1] == 'B') ){
							swap(ii,jj,i,j+1,map);
							j= jj;
							ii = -1;
							jj = -1;
							curr = 'x';
						}
						
						if(ii > -1 && curr =='O' && map[i][j+1] == 'R'){
							map[i][j+1] = '.';
							Rfin = true;
						}
						
						if(ii > -1 && curr =='O' && map[i][j+1] == 'B'){
							map[i][j+1] = '.';
							Bfin = true;
							return;
						}
					}
				}
				break;
			case 4:
				for(int i=0; i<N; i++) {
					int ii = -1;
					int jj = -1;
					char curr = 'x';
					for(int j= M-1; j>=1; j--) {
						if(map[i][j]=='.' && ii == -1 && curr=='x') {
							ii = i;
							jj = j;
							curr = '.';
						}
						if(map[i][j]=='O') {
							ii = i;
							jj = j;
							curr = 'O';
						}
						
						if(map[i][j]=='#' && curr !='x') {
							ii = -1;
							jj = -1;
							curr = 'x';
						}
						
						if(ii > -1 && curr =='.' && (map[i][j-1] == 'R' || map[i][j-1] == 'B') ){
							swap(ii,jj,i,j-1,map);
							j = jj;
							ii = -1;
							jj = -1;
							curr = 'x';
						}
						
						if(ii > -1 && curr =='O' && map[i][j-1] == 'R'){
							map[i][j-1] = '.';
							Rfin = true;
						}
						
						if(ii > -1 && curr =='O' && map[i][j-1] == 'B'){
							map[i][j-1] = '.';
							Bfin = true;
							return;
						}
					}
				}
				break;
		}
	}

	private static void swap(int i, int j, int a, int b, char[][] map) {
		char temp = map[i][j];
		map[i][j] = map[a][b];
		map[a][b] = temp;
	}
}