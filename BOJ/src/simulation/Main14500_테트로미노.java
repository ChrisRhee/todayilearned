package simulation;
import java.io.*;

class Main14500_테트로미노{
	private static int di[] = {-1,1,0,0};
	private static int dj[] = {0,0,-1,1};
	private static int ans, n, m;
	
	private static void solve(int count, int value, boolean[][] visit, int[][] map, int i, int j) {
		if(count >= 4) {
			if(ans < value) ans = value;
			return;
		}
		
		for(int d=0; d<4; d++) {
			int ii = i + di[d];
			int jj = j + dj[d];
			if(ii < 0 || ii >= n || jj < 0 || jj >= m || visit[ii][jj] == true) continue;
			visit[ii][jj] = true;
			solve(count+1,value+map[ii][jj], visit, map, ii,jj);
			visit[ii][jj] = false;
		}
	}
	
	private static void solve2(int[][] map, int i, int j) {
		for(int d=0; d<4; d++) {
			int count = 1;
			int temp = map[i][j];
			int ii = i + di[0];
			int jj = j + dj[0];
			if(ii >= 0 && ii < n && jj >= 0 && jj < m ) {
				temp += map[ii][jj];
				count++;
			}
			
			ii = i+di[1];
			jj = j+dj[1];
			if(ii >= 0 && ii < n && jj >= 0 && jj < m ) {
				temp += map[ii][jj];
				count++;
			}
			
			ii = i+di[2];
			jj = j+dj[2];
			if(ii >= 0 && ii < n && jj >= 0 && jj < m ) {
				temp += map[ii][jj];
				count++;
			}
			
			ii = i+di[3];
			jj = j+dj[3];
			if(ii >= 0 && ii < n && jj >= 0 && jj < m ) {
				temp += map[ii][jj];
				count++;
			}
			if(count < 4) continue;
			
			ii = i+di[d];
			jj = j+dj[d];
			if(ii >= 0 && ii < n && jj >= 0 && jj < m ) {
				temp -= map[ii][jj];
				count--;
			}
			if(count == 4)
				if(ans < temp) ans = temp;
		}
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj14500.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		n = Integer.parseInt(s[0]);
		m = Integer.parseInt(s[1]);
		int map[][] = new int[n][m];
		ans = 0;
		
		boolean visit[][] = new boolean[n][m];
		
		for(int i=0; i<n; i++) {
			s = br.readLine().split(" ");
			for(int j=0; j<m; j++) {
				map[i][j] = Integer.parseInt(s[j]);
			}
		}
		
		for(int i=0; i<n; i++) {
			for(int j=0; j<m; j++) {
				visit[i][j] = true;
				solve(1,map[i][j],visit,map,i,j);
				solve2(map,i,j);
				visit[i][j] = false;
			}
		}
		
		System.out.println(ans);
	}
	
	
}