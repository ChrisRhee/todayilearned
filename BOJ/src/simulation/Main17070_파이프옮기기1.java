package simulation;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

class Main17070_파이프옮기기1{
	public static int N,cnt;
	public static Queue<int[]> q;
	public static int[] di = {0,1,1};
	public static int[] dj = {1,1,0};
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj17070.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		q = new LinkedList<>();
		N = Integer.parseInt(br.readLine());
		cnt = 0;
		int map[][] = new int[N][N];
		
		for(int i=0; i<N; i++) {
			String s[] = br.readLine().split(" ");
			for(int j=0; j<N; j++) {
				map[i][j] = Integer.parseInt(s[j]);
			}
		}
		
		// 0:가로  1:대각선  2:세로
		q.offer(new int[] {0,1,0});
		bfs(map);
		System.out.println(cnt);
	}
	
	private static int checkdir(int ii, int jj, int i, int j) {
		if (ii - i == 0 && jj - j != 0)
			return 0;
		else if (ii - i != 0 && jj - j != 0)
			return 1;
		else if (ii - i != 0 && jj - j == 0)
			return 2;
		else
			return -1;
	}
	
	private static void bfs(int[][] map) {
		while(!q.isEmpty()) {
			int curr[] = q.poll();
			
			if(curr[2]==0) {
				for(int d = 0; d<2; d++) {
					int ii = curr[0] + di[d];
					int jj = curr[1] + dj[d];
					if(ii >= N || jj >= N || map[ii][jj]== 1) continue;
					int k = checkdir(ii,jj,curr[0],curr[1]);
					
					if(d== 0) { 
						if(ii == N-1 && jj == N-1 ) {
							cnt++;
							continue;
						}
						q.offer(new int[] {ii,jj,k});
					}
					else if(d==1 && map[ii][jj]==0 && map[ii-1][jj]==0 && map[ii][jj-1] == 0) {
						if(ii == N-1 && jj == N-1 ) {
							cnt++;
							continue;
						}
						q.offer(new int[] {ii,jj,k});
					}
					
				}
			}
			else if(curr[2]==1) {
				for(int d = 0; d<3; d++) {
					int ii = curr[0] + di[d];
					int jj = curr[1] + dj[d];
					if(ii >= N || jj >= N || map[ii][jj]== 1) continue;
					
					int k = checkdir(ii,jj,curr[0],curr[1]);
					
					if(d== 0) { 
						if(ii == N-1 && jj == N-1 ) {
							cnt++;
							continue;
						}
						q.offer(new int[] {ii,jj,k});
					}
					else if(d==1 && map[ii][jj]==0 && map[ii-1][jj]==0 && map[ii][jj-1] == 0) {
						if(ii == N-1 && jj == N-1 ) {
							cnt++;
							continue;
						}
						q.offer(new int[] {ii,jj,k});
					}
					else if(d==2 ) { 
						if(ii == N-1 && jj == N-1 ) {
							cnt++;
							continue;
						}
						q.offer(new int[] {ii,jj,k});
					}
				}
			}
			else if(curr[2]==2) {
				for(int d = 1; d<3; d++) {
					int ii = curr[0] + di[d];
					int jj = curr[1] + dj[d];
					if(ii >= N || jj >= N || map[ii][jj]== 1) continue;
					
					int k = checkdir(ii,jj,curr[0],curr[1]);
					
					if(d==1 && map[ii][jj]==0 && map[ii-1][jj]==0 && map[ii][jj-1] == 0) {
						if(ii == N-1 && jj == N-1 ) {
							cnt++;
							continue;
						}
						q.offer(new int[] {ii,jj,k});
					}
					else if(d==2 ) { 
						if(ii == N-1 && jj == N-1 ) {
							cnt++;
							continue;
						}
						q.offer(new int[] {ii,jj,k});
					}
				}
			}
		}
	}


	
}