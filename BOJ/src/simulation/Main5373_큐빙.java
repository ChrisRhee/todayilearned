package simulation;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

class Main5373_큐빙{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj5373.txt"));
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		
		String cu[] = new String[] {"xxxwwwxxx",
				"xxxwwwxxx",
				"xxxwwwxxx",
				"gggrrrbbb",
				"gggrrrbbb",
				"gggrrrbbb",
				"xxxyyyxxx",
				"xxxyyyxxx",
				"xxxyyyxxx",
				"xxxoooxxx",
				"xxxoooxxx",
				"xxxoooxxx"
		}; 
		for(int tc= 0; tc <T; tc++) {
			char[][] cube = new char[12][9];
			for(int i=0; i<12; i++) {
				cube[i] = cu[i].toCharArray();
			}
			int N = sc.nextInt();
			for(int i=0; i<N; i++) {
				char t1,t2,t3;
				switch(sc.next()) {
				case "F+":
					t1 = cube[2][3];
					t2 = cube[2][4];
					t3 = cube[2][5];
					cube[2][3] = cube[5][2];
					cube[2][4] = cube[4][2];
					cube[2][5] = cube[3][2];
					cube[5][2] = cube[6][5];
					cube[4][2] = cube[6][4];
					cube[3][2] = cube[6][3];
					cube[6][5] = cube[3][6];
					cube[6][4] = cube[4][6];
					cube[6][3] = cube[5][6];
					cube[3][6] = t1;
					cube[4][6] = t2;
					cube[5][6] = t3;
					
					t1 = cube[3][3];
					t2 = cube[3][4];
					cube[3][3] = cube[5][3];
					cube[3][4] = cube[4][3];
					cube[5][3] = cube[5][5];
					cube[4][3] = cube[5][4];
					cube[5][5] = cube[3][5];
					cube[5][4] = cube[4][5];
					cube[3][5] = t1;
					cube[4][5] = t2;
					break;
				case "F-":
					t1 = cube[2][3];
					t2 = cube[2][4];
					t3 = cube[2][5];
					cube[2][3] = cube[3][6];
					cube[2][4] = cube[4][6];
					cube[2][5] = cube[5][6];
					cube[3][6] = cube[6][5];
					cube[4][6] = cube[6][4];
					cube[5][6] = cube[6][3];
					cube[6][5] = cube[5][2];
					cube[6][4] = cube[4][2];
					cube[6][3] = cube[3][2];
					cube[5][2] = t1;
					cube[4][2] = t2;
					cube[3][2] = t3;
					
					t1 = cube[3][3];
					t2 = cube[3][4];
					cube[3][3] = cube[3][5];
					cube[3][4] = cube[4][5];
					cube[3][5] = cube[5][5];
					cube[4][5] = cube[5][4];
					cube[5][5] = cube[5][3];
					cube[5][4] = cube[4][3];
					cube[5][3] = t1;
					cube[4][3] = t2;
					break;
				case "B+":
					t1 = cube[0][3];
					t2 = cube[0][4];
					t3 = cube[0][5];
					cube[0][3] = cube[3][8];
					cube[0][4] = cube[4][8];
					cube[0][5] = cube[5][8];
					cube[3][8] = cube[8][5];
					cube[4][8] = cube[8][4];
					cube[5][8] = cube[8][3];
					cube[8][5] = cube[5][0];
					cube[8][4] = cube[4][0];
					cube[8][3] = cube[3][0];
					cube[5][0] = t1;
					cube[4][0] = t2;
					cube[3][0] = t3;
					
					t1 = cube[9][3];
					cube[9][3] = cube[11][3];
					cube[11][3] = cube[11][5];
					cube[11][5] = cube[9][5];
					cube[9][5] = t1;
					
					t2 = cube[9][4];
					cube[9][4] = cube[10][3];
					cube[10][3] = cube[11][4];
					cube[11][4] = cube[10][5];
					cube[10][5] = t2;
					
					break;
				case "B-":
					t1 = cube[0][3];
					t2 = cube[0][4];
					t3 = cube[0][5];
					cube[0][3] = cube[5][0];
					cube[0][4] = cube[4][0];
					cube[0][5] = cube[3][0];
					cube[5][0] = cube[8][5];
					cube[4][0] = cube[8][4];
					cube[3][0] = cube[8][3];
					cube[8][5] = cube[3][8];
					cube[8][4] = cube[4][8];
					cube[8][3] = cube[5][8];
					cube[3][8] = t1;
					cube[4][8] = t2;
					cube[5][8] = t3;
					
					t1 = cube[9][3];
					cube[9][3] = cube[9][5];
					cube[9][5] = cube[11][5];
					cube[11][5] = cube[11][3];
					cube[11][3] = t1;
					
					t2 = cube[9][4];
					cube[9][4] = cube[10][5];
					cube[10][5] = cube[11][4];
					cube[11][4] = cube[10][3];
					cube[10][3] = t2;
					
					break;
				case "R+":
					t1 = cube[3][5];
					t2 = cube[4][5];
					t3 = cube[5][5];
					cube[3][5] = cube[6][5];
					cube[4][5] = cube[7][5];
					cube[5][5] = cube[8][5];
					cube[6][5] = cube[9][5];
					cube[7][5] = cube[10][5];
					cube[8][5] = cube[11][5];
					cube[9][5] = cube[0][5];
					cube[10][5] = cube[1][5];
					cube[11][5] = cube[2][5];
					cube[0][5] = t1;
					cube[1][5] = t2;
					cube[2][5] = t3;
					
					t1 = cube[3][6];
					cube[3][6] = cube[5][6];
					cube[5][6] = cube[5][8];
					cube[5][8] = cube[3][8];
					cube[3][8] = t1;
					
					t2 = cube[3][7];
					cube[3][7] = cube[4][6];
					cube[4][6] = cube[5][7];
					cube[5][7] = cube[4][8];
					cube[4][8] = t2;
					break;
				case "R-":
					t1 = cube[3][5];
					t2 = cube[4][5];
					t3 = cube[5][5];
					cube[3][5] = cube[0][5];
					cube[4][5] = cube[1][5];
					cube[5][5] = cube[2][5];
					cube[0][5] = cube[9][5];
					cube[1][5] = cube[10][5];
					cube[2][5] = cube[11][5];
					cube[9][5] = cube[6][5];
					cube[10][5] = cube[7][5];
					cube[11][5] = cube[8][5];
					cube[6][5] = t1;
					cube[7][5] = t2;
					cube[8][5] = t3;
					
					t1 = cube[3][6];
					cube[3][6] = cube[3][8];
					cube[3][8] = cube[5][8];
					cube[5][8] = cube[5][6];
					cube[5][6] = t1;
					t2 = cube[3][7];
					cube[3][7] = cube[4][8];
					cube[4][8] = cube[5][7];
					cube[5][7] = cube[4][6];
					cube[4][6] = t2;
					break;
				case "L+":
					t1 = cube[3][3];
					t2 = cube[4][3];
					t3 = cube[5][3];
					cube[3][3] = cube[0][3];
					cube[4][3] = cube[1][3];
					cube[5][3] = cube[2][3];
					cube[0][3] = cube[9][3];
					cube[1][3] = cube[10][3];
					cube[2][3] = cube[11][3];
					cube[9][3] = cube[6][3];
					cube[10][3] = cube[7][3];
					cube[11][3] = cube[8][3];
					cube[6][3] = t1;
					cube[7][3] = t2;
					cube[8][3] = t3;
					
					t1 = cube[3][0];
					cube[3][0] = cube[5][0];
					cube[5][0] = cube[5][2];
					cube[5][2] = cube[3][2];
					cube[3][2] = t1;
					
					t2 = cube[3][1];
					cube[3][1] = cube[4][0];
					cube[4][0] = cube[5][1];
					cube[5][1] = cube[4][2];
					cube[4][2] = t2;
					break;
				case "L-":
					t1 = cube[3][3];
					t2 = cube[4][3];
					t3 = cube[5][3];
					cube[3][3] = cube[6][3];
					cube[4][3] = cube[7][3];
					cube[5][3] = cube[8][3];
					cube[6][3] = cube[9][3];
					cube[7][3] = cube[10][3];
					cube[8][3] = cube[11][3];
					cube[9][3] = cube[0][3];
					cube[10][3] = cube[1][3];
					cube[11][3] = cube[2][3];
					cube[0][3] = t1;
					cube[1][3] = t2;
					cube[2][3] = t3;
					
					t1 = cube[3][0];
					cube[3][0] = cube[3][2];
					cube[3][2] = cube[5][2];
					cube[5][2] = cube[5][0];
					cube[5][0] = t1;
					t2 = cube[3][1];
					cube[3][1] = cube[4][2];
					cube[4][2] = cube[5][1];
					cube[5][1] = cube[4][0];
					cube[4][0] = t2;
					break;
				case "U+":
					t1 = cube[3][3];
					t2 = cube[3][4];
					t3 = cube[3][5];
					cube[3][3] = cube[3][6];
					cube[3][4] = cube[3][7];
					cube[3][5] = cube[3][8];
					cube[3][6] = cube[11][5];
					cube[3][7] = cube[11][4];
					cube[3][8] = cube[11][3];
					cube[11][5] = cube[3][0];
					cube[11][4] = cube[3][1];
					cube[11][3] = cube[3][2];
					cube[3][0] = t1;
					cube[3][1] = t2;
					cube[3][2] = t3;
					
					t1 = cube[0][3];
					cube[0][3] = cube[2][3];
					cube[2][3] = cube[2][5];
					cube[2][5] = cube[0][5];
					cube[0][5] = t1;
					
					t2 = cube[0][4];
					cube[0][4] = cube[1][3];
					cube[1][3] = cube[2][4];
					cube[2][4] = cube[1][5];
					cube[1][5] = t2;
					break;
				case "U-":
					t1 = cube[3][3];
					t2 = cube[3][4];
					t3 = cube[3][5];
					cube[3][3] = cube[3][0];
					cube[3][4] = cube[3][1];
					cube[3][5] = cube[3][2];
					cube[3][0] = cube[11][5];
					cube[3][1] = cube[11][4];
					cube[3][2] = cube[11][3];
					cube[11][5] = cube[3][6];
					cube[11][4] = cube[3][7];
					cube[11][3] = cube[3][8];
					cube[3][6] = t1;
					cube[3][7] = t2;
					cube[3][8] = t3;
					
					t1 = cube[0][3];
					cube[0][3] = cube[0][5];
					cube[0][5] = cube[2][5];
					cube[2][5] = cube[2][3];
					cube[2][3] = t1;
					
					t2 = cube[0][4];
					cube[0][4] = cube[1][5];
					cube[1][5] = cube[2][4];
					cube[2][4] = cube[1][3];
					cube[1][3] = t2;
					break;
				case "D+":
					t1 = cube[5][3];
					t2 = cube[5][4];
					t3 = cube[5][5];
					
					cube[5][3] = cube[5][0];
					cube[5][4] = cube[5][1];
					cube[5][5] = cube[5][2];
					
					cube[5][0] = cube[9][5];
					cube[5][1] = cube[9][4];
					cube[5][2] = cube[9][3];
					
					cube[9][5] = cube[5][6];
					cube[9][4] = cube[5][7];
					cube[9][3] = cube[5][8];
					
					cube[5][6] = t1;
					cube[5][7] = t2;
					cube[5][8] = t3;
					
					t1 = cube[6][3];
					cube[6][3] = cube[8][3];
					cube[8][3] = cube[8][5];
					cube[8][5] = cube[6][5];
					cube[6][5] = t1;
					
					t2 = cube[6][4];
					cube[6][4] = cube[7][3];
					cube[7][3] = cube[8][4];
					cube[8][4] = cube[7][5];
					cube[7][5] = t2;
					break;
				case "D-":
					t1 = cube[5][3];
					t2 = cube[5][4];
					t3 = cube[5][5];
					
					cube[5][3] = cube[5][6];
					cube[5][4] = cube[5][7];
					cube[5][5] = cube[5][8];
					
					cube[5][6] = cube[9][5];
					cube[5][7] = cube[9][4];
					cube[5][8] = cube[9][3];
					
					cube[9][5] = cube[5][0];
					cube[9][4] = cube[5][1];
					cube[9][3] = cube[5][2];
					
					cube[5][0] = t1;
					cube[5][1] = t2;
					cube[5][2] = t3;
					
					t1 = cube[6][3];
					cube[6][3] = cube[6][5];
					cube[6][5] = cube[8][5];
					cube[8][5] = cube[8][3];
					cube[8][3] = t1;
					
					t2 = cube[6][4];
					cube[6][4] = cube[7][5];
					cube[7][5] = cube[8][4];
					cube[8][4] = cube[7][3];
					cube[7][3] = t2;
					break;
				}
				
				
				//for (char[] cs : cube) { System.out.println(Arrays.toString(cs)); }
				//System.out.println(s[i]);
			}
			System.out.println(cube[0][3]+""+cube[0][4]+cube[0][5]);
			System.out.println(cube[1][3]+""+cube[1][4]+cube[1][5]);
			System.out.println(cube[2][3]+""+cube[2][4]+cube[2][5]);
			
			/*
			 * System.out.println(); for (char[] cs : cube) {
			 * System.out.println(Arrays.toString(cs)); }
			 */
		}
		
	}
}