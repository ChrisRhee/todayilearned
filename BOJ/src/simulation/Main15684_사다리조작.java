package simulation;
import java.io.*;

class Main15684_사다리조작{
	public static int N, M, min, lines[], sero[], se;
	public static boolean visit[][];
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj15684.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		
		min = 4;
		M = Integer.parseInt(s[0]);
		int num = Integer.parseInt(s[1]);
		N = Integer.parseInt(s[2]);
		int map[][] = new int[31][11];
		lines = new int[11];
		sero = new int[31];
		for(int i=0; i<num; i++) {
			s = br.readLine().split(" ");
			map[Integer.parseInt(s[0])][Integer.parseInt(s[1])] =1;
			lines[Integer.parseInt(s[1])]++;
			map[Integer.parseInt(s[0])][Integer.parseInt(s[1])+1] =-1;
			sero[Integer.parseInt(s[0])]++;
		}
		
		se = getSero();
		solve(0,map);
		
		if(min == 4) min = -1;
		
		System.out.println(min);
	}

	private static int getSero() {
		int cnt = 10;
		boolean check = false;
		for(int i=1; i<=N; i++) {
			if(sero[i]==0) cnt--;
			if(cnt ==0) {
				cnt = i;
				check = true;
				break;
			}
		}
		if(check)
			return cnt;
		else
			return N;
	}

	private static void solve(int count, int[][] map) {
		if(min < count) return;
		if(count >= 4) {
			return;
		}
		
		if(bfs(map)) {
			if(min > count) min = count;
			return;
		}
		for(int i=1; i<=N; i++) {
			if(i > se) continue;
			for(int j=1; j<M; j++) {
				if(map[i][j]==1 || map[i][j] == -1) continue;
				if(map[i][j]==0 && map[i][j+1] == 1) continue;
				if(lines[j]== 0) continue;
				map[i][j] = 1;
				map[i][j+1] = -1;
				solve(count+1, map);
				map[i][j] = 0;
				map[i][j+1] = 0;
			}
		}
	}

	private static boolean bfs(int[][] map) {
		for(int i=1; i<=M; i++) {
			visit = new boolean[31][11];
			int ii = 1;
			int jj = i;
			while(true) {	
				if(ii == N+1) {
					if(jj != i) return false;
					break;
				}
				//밑으로 가기
				if(visit[ii][jj] == false && map[ii][jj]==0) {
					visit[ii][jj] = true;
					ii++;
				}
				//오른쪽으로 가기
				else if (visit[ii][jj] == false && map[ii][jj] == 1) {
					visit[ii][jj] = true;
					visit[ii][jj+1] = true;
					jj++;
				}
				//왼쪽으로 가기
				else if (visit[ii][jj] == false && map[ii][jj] == -1) {
					visit[ii][jj] = true;
					visit[ii][jj-1] = true;
					jj--;
				}
				//1or-1인데 왼쪽or오른쪽에서 온거면 밑으로
				else if(visit[ii][jj] == true && map[ii][jj] == 1){
					ii++;
				}
				else if(visit[ii][jj] == true && map[ii][jj] == -1){
					ii++;
				}
			}
		}
		return true;
	}
}