package simulation;
import java.io.*;
import java.util.Arrays;

class Main12100_2048{
	public static int N, max;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj12100.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		int[][] map = new int [N][N];
		max = 0;
		
		for(int i=0 ; i<N; i++) {
			String s[] = br.readLine().split(" ");
			for(int j=0 ; j<N; j++) {
				map[i][j] = Integer.parseInt(s[j]); 
			}
		}
		
		
		dfs(0, map);
		
		System.out.println(max);
	}
	
	private static void dfs(int count, int[][] map) {
		if(count == 5) {
			for(int i=0 ; i<N; i++) {
				for(int j=0 ; j<N; j++) {
					if(map[i][j] > max) max = map[i][j];
				}
			}
			return;
		}
		for(int i=1; i<=4; i++) {
			int map2[][] = new int [N][N]; 
			for(int j=0; j<N; j++) {
				for(int k=0; k<N; k++) {
					map2[j][k] = map[j][k];
				}
			}
			
			sum(i,map2);
			dfs(count+1, map2);
		}
		
	}

	private static void swap(int a, int b, int a2, int b2, int[][] map) {
		int temp = map[a][b];
		map[a][b]= map[a2][b2];
		map[a2][b2] = temp;
	}
	
	private static void pushblock(int dir, int[][] map) {
		switch(dir) {
		//상
		case 1:
			for(int i= 0; i<N; i++) {
				int a = -1;
				int b = -1;
				for(int j=0; j<N; j++) {
					if(map[j][i] == 0 && a == -1) {
						a = j; b = i;
					}else if(map[j][i] > 0 && a >= 0) {
						swap(j,i,a,b,map);
						j = a;
						a= -1; b = -1;
					}
				}
			}
			break;
		//하
		case 2:
			for(int i= 0; i<N; i++) {
				int a = -1;
				int b = -1;
				for(int j=N-1; j>=0; j--) {
					if(map[j][i] == 0 && a == -1) {
						a = j; b = i;
					}else if(map[j][i] > 0 && a >= 0) {
						swap(j,i,a,b,map);
						j = a;
						a= -1; b = -1;
					}
				}
			}
			break;
		//좌
		case 3:
			for(int i= 0; i<N; i++) {
				int a = -1;
				int b = -1;
				for(int j=0; j<N; j++) {
					if(map[i][j] == 0 && a == -1) {
						a = i; b = j;
					}else if(map[i][j] > 0 && a >= 0) {
						swap(i,j,a,b,map);
						j = b;
						a= -1; b = -1;
					}
				}
			}
			break;
		//우
		case 4:
			for(int i= 0; i<N; i++) {
				int a = -1;
				int b = -1;
				for(int j=N-1; j>=0; j--) {
					if(map[i][j] == 0 && a == -1) {
						a = i; b = j;
					}else if(map[i][j] > 0 && a >= 0) {
						swap(i,j,a,b,map);
						j = b;
						a= -1; b = -1;
					}
				}
			}
			break;
		}
		
	}
	
	private static void sum(int dir, int[][] map) {
		pushblock(dir,map);
		
		switch(dir) {
		//상
		case 1:
			for(int i= 0; i<N; i++) {
				for(int j=0; j<N-1; j++) {
					if(map[j][i] == map[j+1][i]) {
						map[j][i] += map[j+1][i];
						map[j+1][i] = 0;
						//j++;
					}
				}
			}
			break;
		//하
		case 2:
			for(int i= 0; i<N; i++) {
				for(int j=N-1; j>=1; j--) {
					if(map[j][i] == map[j-1][i]) {
						map[j][i] += map[j-1][i];
						map[j-1][i] = 0;
						//j--;
					}
				}
			}
			break;
		//좌
		case 3:
			for(int i= 0; i<N; i++) {
				for(int j=0; j<N-1; j++) {
					if(map[i][j] == map[i][j+1]) {
						map[i][j] += map[i][j+1];
						map[i][j+1] = 0;
						//j++;
					}
				}
			}
			break;
		//우
		case 4:
			for(int i= 0; i<N; i++) {
				for(int j=N-1; j>=1; j--) {
					if(map[i][j] == map[i][j-1]) {
						map[i][j] += map[i][j-1];
						map[i][j-1] = 0;
						//j--;
					}
				}
			}
			break;
		}
		pushblock(dir,map);
	}
}