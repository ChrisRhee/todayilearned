package simulation;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

class Main14442_벽부수고이동하기2{
	public static int N,M,min;
	public static Queue<int[]> q;
	public static int qv[][];
	public static int[] di= {-1,1,0,0};
	public static int[] dj= {0,0,-1,1};
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj14442.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s[] = br.readLine().split(" ");
		
		N = Integer.parseInt(s[0]);
		M = Integer.parseInt(s[1]);
		qv = new int[N+1][M+1];
		q = new LinkedList<>();
		
		min = 10000001;
		int K = Integer.parseInt(s[2]);
		int map[][] = new int[N+1][M+1];
		
		for(int i=0; i<N; i++) {
			s = br.readLine().split("");
			for(int j=0; j<M; j++) {
				map[i+1][j+1] = Integer.parseInt(s[j]);
			}
		}
		
		
		int t = bfs(map,K);
		if(t > min) return;
		if(min > t && t > 0) min = t;
		
		if(min == 10000001)
			System.out.println(-1);
		else {
			System.out.println(min);
		}
	}

	private static int bfs(int[][] map, int k) {
		qv[1][1] = 1;
		q.offer(new int[] {1,1,k});
		while(!q.isEmpty()) {
			int[] curr = q.poll();
			for(int d =0; d<4; d++) {
				int ii = curr[0]+di[d];
				int jj = curr[1]+dj[d];
				if( ii < 1 || ii > N || jj < 1 || jj > M) continue;
				
				if(ii == N && jj == M) {
					return qv[curr[0]][curr[1]] + 1;
				}
				
				//그냥 땅이면 그냥 진행
				if(qv[ii][jj]==0 && map[ii][jj]==0) {
					qv[ii][jj] = qv[curr[0]][curr[1]] + 1;
					q.offer(new int[] {ii,jj,curr[2]});
				}
				
				// 벽이고 k가 0보다 클때만 ㄱㄱ
				if(qv[ii][jj]==0 && map[ii][jj]==1 && curr[2] >0) {
					qv[ii][jj] = qv[curr[0]][curr[1]] + 1;
					q.offer(new int[] {ii,jj,curr[2]-1});
				}
			}
		}
		return -1;
	}

}