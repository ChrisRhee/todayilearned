package simulation;
import java.io.*;
import java.util.*;
class Main3190_뱀{
	public static int[] di = {-1,0,1,0};
	public static int[] dj = {0,1,0,-1};
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj3190.txt"));
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		int K = sc.nextInt();
		int map[][] = new int[N+1][N+1];
		Queue<int []> q = new LinkedList<>();
		for(int i=0; i<K; i++) {
			map[sc.nextInt()][sc.nextInt()] = 10;
		}
		int L = sc.nextInt();
		int com[][] = new int[L][2];
		for(int i=0; i<L; i++) {
			com[i][0] = sc.nextInt();
			String temp = sc.next().trim();
			// D오른쪽1 , L왼쪽-1
			if(temp.equals("D")) com[i][1] = 1;
			else if(temp.equals("L")) com[i][1] = -1;
		}
		
		int currDir = 1;
		int si = 1;
		int sj = 1;
		int count = 0;
		int Index = 0;
		q.offer(new int[] {si,sj});
		map[si][sj] = 1;
		while(true) {
			count++;
			if(Index >= com.length) Index = 0;
			if(com[Index][0] == 0) {
				currDir += com[Index][1];
				com[Index][1] = 0;
				if(currDir < 0) currDir = 3;
				else if(currDir == 4) currDir = 0;
				Index++;
			}
			for(int i=0; i<com.length; i++) {
				com[i][0]--;
			}
			si = si + di[currDir];
			sj = sj + dj[currDir];
			// 벽에 닿거나, 자신 밟으면 break
			if( si < 1 || si > N || sj < 1 || sj > N || map[si][sj]==1 ) break;
			
			// 바닥이면 꼬리 꺼내고
			if(map[si][sj]==0) {
				q.offer(new int[] {si,sj});
				map[si][sj] = 1;
				int tail[] = q.poll();
				map[tail[0]][tail[1]] = 0;
				
			}
			// 사과면 꼬리 안꺼내고 그냥 이동
			else if(map[si][sj]==10) {
				q.offer(new int[] {si,sj});
				map[si][sj] = 1;
			}
		}
		System.out.println(count);
	}
}