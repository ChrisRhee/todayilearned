package dfs_bfs;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

class Main2667_단지번호붙이기{
	public static int[] di= {-1,1,0,0};
	public static int[] dj= {0,0,-1,1};
	public static int map[][],N,cnt;
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/bj2667.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		map = new int[N][N];
		int count = 1;
		ArrayList<Integer> a = new ArrayList<>();
		int[][] visit = new int[N][N];
		for(int i=0; i<N; i++) {
			String s[]= br.readLine().split("");
			for(int j=0; j<N; j++) {
				map[i][j] = Integer.parseInt(s[j]);
			}
		}
		
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				if(map[i][j] == 0) continue;
				if(visit[i][j] > 0) continue;
				visit[i][j] = count;
				cnt = 1;
				dfs(i,j,visit,count);
				a.add(cnt);
				count++;
			}
		}
		System.out.println(a.size());
		Collections.sort(a);
		for(int i=0; i<a.size(); i++) {
			System.out.println(a.get(i));
		}
	}
	
	private static void dfs(int i, int j, int[][] visit, int count) {
		for(int d=0; d<4; d++) {
			int ii= i+di[d];
			int jj= j+dj[d];
			if(ii < 0 || ii >= N || jj < 0 || jj >= N) continue;
			if(visit[ii][jj] == 0 && map[ii][jj] == 1) {
				visit[ii][jj] = count;
				cnt++;
				dfs(ii,jj,visit,count);
			}
		}
		
	}
}