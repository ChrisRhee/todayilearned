package com.ssafy.model.dto;

public class Book {
	public String isbn1;
	public String isbn2;
	public String isbn3;
	public String isbn;
	public String title;
	public String author;
	public int price;
	
	public Book() {
		this("", "", "", 0);
	}
	public Book(String isbn, String title, String author, int price) {
		super();
		setIsbn(isbn);
		setTitle(title);
		setAuthor(author);
		setPrice(price);
	}
	
	public String getIsbn1() {
		return isbn1;
	}
	public void setIsbn1(String isbn1) {
		this.isbn1 = isbn1;
	}
	public String getIsbn2() {
		return isbn2;
	}
	public void setIsbn2(String isbn2) {
		this.isbn2 = isbn2;
	}
	public String getIsbn3() {
		return isbn3;
	}
	public void setIsbn3(String isbn3) {
		this.isbn3 = isbn3;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Book [isbn1=");
		builder.append(isbn1);
		builder.append(", isbn2=");
		builder.append(isbn2);
		builder.append(", isbn3=");
		builder.append(isbn3);
		builder.append(", isbn=");
		builder.append(isbn);
		builder.append(", title=");
		builder.append(title);
		builder.append(", author=");
		builder.append(author);
		builder.append(", price=");
		builder.append(price);
		builder.append("]");
		return builder.toString();
	}
	
}
