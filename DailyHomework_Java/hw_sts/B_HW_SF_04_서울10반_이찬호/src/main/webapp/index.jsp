<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>메인페이지</title>
<style type="text/css">
h1, h4 {
	text-align: center;
}
a {
	margin: 10px;
}
</style>
</head>
<body>
<h1> 메인 페이지 </h1>
<p/>
<h4>
<c:url value="/book/book" var="bookadd" />
<a href="${bookadd}">도서 등록</a>
<c:url value="/book/booklist" var="booklist" />
<a href="${booklist}">도서 목록</a>
</h4>
</body>
</html>