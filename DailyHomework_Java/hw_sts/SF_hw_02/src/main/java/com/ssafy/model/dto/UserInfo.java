package com.ssafy.model.dto;

public class UserInfo {
	private String userid;
	private String name;
	private String password;
	
	public UserInfo() {
		this("", "", "");
	}
	public UserInfo(String userid, String name, String password) {
		super();
		setUserid(userid);
		setName(name);
		setPassword(password);
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserInfo [userid=");
		builder.append(getUserid());
		builder.append(", name=");
		builder.append(getName());
		builder.append(", password=");
		builder.append(getPassword());
		builder.append("]");
		return builder.toString();
	}
}
