package com.ssafy.aop;

import java.util.Calendar;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import com.ssafy.exception.TimeOutException;

@Component
@Aspect
public class DayTimeCheckAOP {
	@Before("execution(* *DayTime(..))")
	public void checkDayTime() {
		Calendar c = Calendar.getInstance();
		int h = c.get(Calendar.HOUR_OF_DAY);
		if (h < 9 || h > 18) throw new TimeOutException();
	}
}
