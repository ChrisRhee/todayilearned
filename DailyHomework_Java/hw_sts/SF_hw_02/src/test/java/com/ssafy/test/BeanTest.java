package com.ssafy.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.ssafy.config.ApplicationConfig;
import com.ssafy.exception.TimeOutException;
import com.ssafy.model.dto.UserInfo;
import com.ssafy.model.repository.UserRepository;
import com.ssafy.model.service.UserService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=ApplicationConfig.class)
public class BeanTest {
	
	@Autowired
	ApplicationContext ctx;
	
	/*
	@Test
	public void testRepositoryJDBC() {
		UserRepository repo = ctx.getBean("userRepositoryJDBCImpl", UserRepository.class);
		assertThat(repo, is(notNullValue()));
	}
	*/
	
	@Test
	public void testRepositoryMysql() {
		UserRepository repo = ctx.getBean("userRepositoryMysqlImpl", UserRepository.class);
		assertThat(repo, is(notNullValue()));
	}
	
	@Test
	public void testService() {
		UserService service = ctx.getBean(UserService.class);
		assertThat(service, is(notNullValue()));
		UserRepository repo = ctx.getBean("userRepositoryJDBCImpl", UserRepository.class);
		assertThat(repo, is(service.getUserRepo()));
	}
	
	@Test(expected=TimeOutException.class)
	public void testTimeoutMethod() {
		UserService service = ctx.getBean(UserService.class);
		UserInfo info = new UserInfo("hong","홍길동","1234");
		service.joinDayTime(info);
	}
}
