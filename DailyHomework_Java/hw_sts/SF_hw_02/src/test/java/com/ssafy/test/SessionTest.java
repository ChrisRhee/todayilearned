package com.ssafy.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.sql.Connection;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.ssafy.config.ApplicationConfig;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class)
public class SessionTest {

	// 데이타 소스가 잘 만들어지느냐? 
	
	@Autowired
	DataSource ds;

	@Test
	public void testConnection() throws Exception {
		assertThat(ds, is(notNullValue()));
		Connection con = ds.getConnection();
		assertThat(con.getClass().getName(), is("com.mysql.cj.jdbc.ConnectionImpl"));
	}

	
	// 세션 템플렛이 잘 되는가? 테스트 하는거 연결이 잘 됏나? 
	
	@Autowired
	SqlSessionTemplate template;

	@Test
	public void testTemplate() {
		assertThat(template, is(notNullValue()));
	}
}
