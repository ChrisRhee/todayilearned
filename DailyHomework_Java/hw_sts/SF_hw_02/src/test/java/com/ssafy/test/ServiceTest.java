package com.ssafy.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.ssafy.config.ApplicationConfig;
import com.ssafy.model.dto.UserInfo;
import com.ssafy.model.repository.UserRepository;
import com.ssafy.model.service.UserService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class)
public class ServiceTest {

	private static final Logger logger = LoggerFactory.getLogger(ServiceTest.class);
	@Autowired
	UserService uService;
	@Autowired
	UserRepository uRepo;

	@Test
	public void loginTest() {
		UserInfo info = uService.login("hong", "1234");
		assertThat(info.getName(), is("홍길동"));
	}

	@Test
	public void joinFailTest() {
		logger.trace("user service: {}", uService.getClass().getName());
		UserInfo info = new UserInfo("lim", "임꺽정", "1234");
		try {
			int result = uService.joinDayTime(info);
			assertThat(result, is(1));
		} catch (ArithmeticException e) {
			logger.error("join 과정에서 예외 발생: {}", e.getMessage());
		}
		// propation 속성이 required이기 때문에 아직 rollback 되지 않은 상황!! 따라서 아래와 같이 테스트 하면 안됨.
		//UserInfo selected = repo.select(info.getUserId());
		//assertThat(selected, is(nullValue()));
	}

	@Transactional
	@Test
	public void updateTest() {
		UserInfo info = new UserInfo("hong", "HongGilDong", "1234");
		int result = uService.updateDayTime(info);
		assertThat(result, is(1));

		UserInfo selected = uRepo.select(info.getUserid());
		assertThat(selected.getName(), is(info.getName()));
		assertThat(selected.getPassword(), is(info.getPassword()));
	}
	@Transactional
	@Test
	public void deleteTest() {
		String userId = "hong";
		int result = uService.leaveDayTime(userId);
		assertThat(result, is(1));

		UserInfo selected = uRepo.select(userId);
		assertThat(selected, is(nullValue()));
	}
}
