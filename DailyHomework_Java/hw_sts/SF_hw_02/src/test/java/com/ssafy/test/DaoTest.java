package com.ssafy.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.ssafy.config.ApplicationConfig;
import com.ssafy.model.dto.Book;
import com.ssafy.model.dto.UserInfo;
import com.ssafy.model.repository.BookRepository;
import com.ssafy.model.repository.UserRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=ApplicationConfig.class)
@Transactional
@Rollback(true)
// 계속 하려고 실컷 해놓고 롤백 시킴
public class DaoTest {
	
	
	// Dao들이 잘 진행 되는지 체크해보기 테스트
	
	
	
	@Autowired
	@Qualifier("userRepositoryMysqlImpl")
	UserRepository uRepo;
	
//	@Autowired
//	BookRepository bRepo;
	
	
	@Test
	public void testSelectAll() {
		List<UserInfo> list = uRepo.selectAll();
		assertThat(list.size(), is(1));
		System.out.println(list.size());
		System.out.println(list);
		assertThat(list.get(0).getUserid(), is("hong"));
	}
//	@Test
//	public void testSelect() {
//		UserInfo info = uRepo.select("hong");
//		assertThat(info.getName(), is("홍길동"));
//	}
	
	

//	@Test
//	public void testInsert() {
//		UserInfo info = new UserInfo("lim", "임꺽정", "1236");
//		int result = uRepo.insert(info);
//		assertThat(result, is(1));
//
//		UserInfo selected = uRepo.select(info.getUserid());
//		assertThat(selected.getName(), is(info.getName()));
//		assertThat(selected.getPassword(), is(info.getPassword()));
//	}

//	@Test
//	public void testUpdate() {
//		UserInfo info = new UserInfo("hong", "HongGilDong", "1234");
//		int result = uRepo.update(info);
//		assertThat(result, is(1));
//
//		UserInfo selected = uRepo.select(info.getUserid());
//		assertThat(selected.getName(), is(info.getName()));
//		assertThat(selected.getPassword(), is(info.getPassword()));
//	}
//
//	@Test
//	public void testDelete() {
//		String userId = "hong";
//		int result = uRepo.delete(userId);
//		assertThat(result, is(1));
//
//		UserInfo selected = uRepo.select(userId);
//		assertThat(selected, is(nullValue()));
//	}
	
	
	/*
	@Test
	public void testSelectAllBook() {
		List<Book> list = bRepo.selectAll();
		System.out.println(list);
		assertThat(list.size(), is(1));
	}

	@Test
	public void testSelectBook() {
		Book info = bRepo.select("1234");
		System.out.println(info);
		assertThat(info.getAuthor(), is("홍길동"));
	}
	@Test
	public void testInsertBook() {
		Book book = new Book("5678", "제목","홍길동","출판사","좋은책");
		int result = bRepo.insert(book);
		assertThat(result, is(1));
		
		Book selected = bRepo.select(book.getIsbn());
		
		assertThat(selected.getAuthor(), is(book.getAuthor()));
	}
	@Test
	public void testUpdateBook() {
		Book book = new Book("1234", "제목","홍길동","출판사","좋은책");
		int result = bRepo.update(book);
		assertThat(result, is(1));
		
		Book selected = bRepo.select(book.getIsbn());
		assertThat(selected.getTitle(), is(book.getTitle()));
	}
	@Test
	public void testDeleteBook() {
		String isbn = "1234";
		int result = bRepo.delete(isbn);
		assertThat(result, is(1));
		
		Book selected = bRepo.select(isbn);
		assertThat(selected, is(nullValue()));
	}
	*/
}
