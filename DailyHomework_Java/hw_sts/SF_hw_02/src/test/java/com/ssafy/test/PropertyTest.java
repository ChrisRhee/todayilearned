package com.ssafy.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.ssafy.config.ApplicationConfig;
import com.ssafy.exception.TimeOutException;
import com.ssafy.model.dto.UserInfo;
import com.ssafy.model.repository.UserRepository;
import com.ssafy.model.service.UserService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=ApplicationConfig.class)
public class PropertyTest {
	
	@Autowired
	ApplicationContext ctx;
	
	@Value("${db.driverClassName}")
	String driver;
	
	@Test
	public void testDriver() {
		System.out.println(driver);
		assertThat(driver, is("com.mysql.cj.jdbc.Driver"));
	}
	
}