package com.ssafy.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import com.ssafy.aop.DayTimeCheckAOP;
import com.ssafy.model.repository.*;
import com.ssafy.model.service.*;

@Configuration
@ComponentScan(basePackageClasses= {UserRepository.class,BookRepository.class, DayTimeCheckAOP.class})
@EnableAspectJAutoProxy
public class ApplicationConfig {
	@Bean
	public UserService userService(@Qualifier("userRepositoryJDBCImpl") UserRepository repo) {
		UserService service = new UserServiceImpl(repo);
		return service;
	}
	
	@Bean
	public BookService bookService(BookRepository repo) {
		BookService service = new BookServiceImpl(repo);
		return service;
	}
	
}
