package com.ssafy.model.repository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ssafy.model.dto.UserInfo;

@Repository
public class UserRepositoryMysqlImpl implements UserRepository {

	private static final Logger logger=LoggerFactory.getLogger(UserRepositoryMysqlImpl.class);
	
	@Override
	public int insert(UserInfo userinfo) {
		logger.trace("insert() UserInfo: {}",userinfo.getUserid());
		return 0;
	}

	@Override
	public int update(UserInfo userinfo) {
		logger.trace("update() UserInfo: {}",userinfo.getUserid());
		return 0;
	}

	@Override
	public int delete(String userid) {
		logger.trace("delete() id: {}",userid);
		return 0;
	}

	@Override
	public UserInfo select(String userid) {
		logger.trace("select() id: {}",userid);
		return null;
	}

	@Override
	public List<UserInfo> selectAll() {
		logger.trace("selectAllUsers()");
		return null;
	}

}
