package com.ssafy.contact.client;

import java.io.Serializable;

public class Customer implements Serializable{
	private String name;
	private String num;
	private String address;
	
	public Customer(String name, String num, String address) {
		setName(name);
		setNum(num);
		setAddress(address);
	}
	
	
	public Customer(String name, String num) {
		this(name, num, "");
	}

	public Customer(String name) {
		this(name, "");
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getName());
		builder.append(" ");
		builder.append(getNum());
		builder.append(" ");
		builder.append(getAddress());
		return builder.toString();
	}
	
	
	
	
}
