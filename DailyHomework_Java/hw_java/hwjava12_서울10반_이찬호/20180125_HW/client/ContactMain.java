package com.ssafy.contact.client;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.List;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ContactMain implements ActionListener, ItemListener{
	
	private Frame f = new Frame("Phone Book");
	private Label custl = new Label("Phoen Book",Label.CENTER);
	private Label namel  = new Label("이    름",Label.CENTER);
	private Label phonel = new Label("핸드폰",Label.CENTER);
	private Label addrl  = new Label("주    소",Label.CENTER);
	
	private TextField nametf = new TextField();
	private TextField phonetf = new TextField();
	private TextField addrtf = new TextField();
	
	private Button btnINSERT = new Button("INSERT");
	private Button btnDELETE = new Button("DELETE");
	private Button btnUPDATE = new Button("UPDATE");
	private Button btnSEARCH = new Button("SEARCH");
	private Button btnUPLOAD = new Button("UPLOAD");
	private Button btnCLEAR = new Button("CLEAR");
	private Button btnEXIT = new Button("EXIT");
	
	private List list = new List();
	private Panel p1 = new Panel();
	private Panel p2 = new Panel();
	private Panel p2north = new Panel();
	private Panel p2center = new Panel();
	private Panel p2south = new Panel();
	
	private Label msgl= new Label("ok");
	
	private CustomerDAO dao;
	
	public ContactMain() {
		dao = CustomerDAO.getInstance();
		dao.open();
		
		createGUI();
		addEvent();
		showList();
	}
	
	public void createGUI() {
		f.setLayout(new GridLayout(2,1,5,5));
		p1.setLayout(new BorderLayout());
		p2.setLayout(new BorderLayout());
		p1.add(custl, BorderLayout.NORTH);
		p1.add(list, BorderLayout.CENTER);
		p1.add(msgl, BorderLayout.SOUTH);
		msgl.setForeground(Color.RED);
		
		p2north.add(btnINSERT);
		p2north.add(btnDELETE);
		p2north.add(btnUPDATE);
		p2north.add(btnSEARCH);
		
		p2center.setLayout(new GridLayout(3,2,10,10));
		p2center.add(namel); p2center.add(nametf);
		p2center.add(phonel); p2center.add(phonetf);
		p2center.add(addrl); p2center.add(addrtf);
		
		p2south.add(btnUPLOAD);
		p2south.add(btnCLEAR);
		p2south.add(btnEXIT);
		
		p2.add(p2north, BorderLayout.NORTH);
		p2.add(p2center, BorderLayout.CENTER);
		p2.add(p2south, BorderLayout.SOUTH);
	
		f.add(p1);
		f.add(p2);
		
		f.setSize(400, 400);
		f.setVisible(true);
	}
	
	public void addEvent() {
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				f.dispose();
			}
		});
		
		btnINSERT.addActionListener(this);
		btnDELETE.addActionListener(this);
		btnUPDATE.addActionListener(this);
		btnSEARCH.addActionListener(this);
		btnUPLOAD.addActionListener(this);
		btnCLEAR.addActionListener(this);
		btnEXIT.addActionListener(this);
		
		list.addItemListener(this);
	}
	
	public void showList() {
		list.removeAll();
		for (Customer customer : dao.allCust()) {
			list.add(customer.toString());
		}
	}

	public static void main(String[] args) {
		ContactMain cm = new ContactMain();
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println(e.getActionCommand());
		if(e.getSource()==btnINSERT) {
			String name = nametf.getText().trim();
			String phone = phonetf.getText().trim();
			String addr = addrtf.getText().trim();
			if(name.equals("") || phone.equals("") || addr.equals("")) {
				msgl.setText("비어있는 항목이 있습니다.");
				return;
			}
			try {
				dao.insert(name, phone, addr);
				clearText();
				showList();
				msgl.setText("등록: ok");
			} catch (ExistException e1) {
				msgl.setText("등록예외: "+ e1.getMessage());
			}
		}else if(e.getSource()== btnDELETE){
			String name = nametf.getText().trim();
			String phone = phonetf.getText().trim();
			String addr = addrtf.getText().trim();
			if(name.equals("")) {
				msgl.setText("삭제: 먼저 이름을 입력하세요.");
				return;
			}
			try {
				dao.delete(name);
				clearText();
				showList();
				msgl.setText("삭제: ok");
			} catch (NotFoundException e1) {
				msgl.setText("삭제예외: "+ e1.getMessage());
			}
		}else if(e.getSource()== btnUPDATE){
			String name = nametf.getText().trim();
			String phone = phonetf.getText().trim();
			String addr = addrtf.getText().trim();
			if(name.equals("") || phone.equals("")) {
				msgl.setText("비어있는 항목이 있습니다.");
				return;
			}
			try {
				dao.update(name, phone);
				clearText();
				showList();
				msgl.setText("변경: ok");
			} catch (NotFoundException e1) {
				msgl.setText("변경예외: "+ e1.getMessage());
			}
		}else if(e.getSource()== btnSEARCH){
			String name = nametf.getText().trim();
			if(name.equals("")) {
				msgl.setText("검색: 먼저 이름을 입력하세요.");
				return;
			}
			try {
				Customer c = dao.search(name);
				phonetf.setText(c.getNum());
				addrtf.setText(c.getAddress());
				msgl.setText("검색: ok");
			} catch (NotFoundException e1) {
				msgl.setText("검색예외: "+ e1.getMessage());
			}
		}else if(e.getSource()== btnUPLOAD){
			try {
				dao.upload();
				msgl.setText("업로드: ok");
			} catch (Exception e1) {
				msgl.setText("업로드예외: "+ e1.getMessage());
			}
		}else if(e.getSource()== btnCLEAR){
			clearText();
			
		}else if(e.getSource()== btnEXIT){
			try {
				dao.close();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			f.dispose();
		}
	}
	
	private void clearText() {
		nametf.getText();
		phonetf.getText();
		addrtf.getText();
		nametf.setText("");
		phonetf.setText("");
		addrtf.setText("");
		msgl.setText("clear ok");
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		String s= list.getSelectedItem();
		String[] sa = s.split(" ");
		nametf.setText(sa[0]);
		phonetf.setText(sa[1]);
		addrtf.setText(sa[2]);
	}

	
}
