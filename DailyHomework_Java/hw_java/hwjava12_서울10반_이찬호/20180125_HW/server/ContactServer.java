package com.ssafy.contact.server;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.ssafy.contact.client.Customer;

public class ContactServer {
	public static void main(String[] args) {
		ServerSocket ss = null;
		try {
			ss = new ServerSocket(9000);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while(true) {
			ObjectInputStream ois = null;
			Socket s = null;
			try {
				System.out.println("Server Ready");
				s = ss.accept();
				 
				ois = new ObjectInputStream(s.getInputStream());
				
//				List<Customer> list = (ArrayList<Customer>)ois.readObject();
//				for(Customer c:list) {
//					System.out.println(c);
//				}
				while(true) {
					Customer c = (Customer)ois.readObject();
					if(c==null) break;
					System.out.println(c);
				}
			} catch (EOFException e) {
				System.out.println("수신 OK");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					if(ois!=null) s.close();
				} catch(IOException e) {
					e.printStackTrace();
				}
				
				try {
					if(s!=null) s.close();
				} catch(IOException e) {
					e.printStackTrace();
				}
			}
			
		}
	}
}
