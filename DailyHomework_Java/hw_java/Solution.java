package SWExpertAcademy;
import java.util.*;
import java.io.*;

class Solution{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input.txt"));
		//Scanner sc = new Scanner(System.in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		// int T = sc.nextInt();
		// 20
		int T= Integer.parseInt(br.readLine());
		
		// "abcdefghijklmnopqrstuvwxyz"
		String s = br.readLine();
		
		// "AAA BBB CCC DDD"
		String[] sa = br.readLine().split(" ");
		
		// "asjdlfkfjaldskjf"
		char ca[] = br.readLine().toCharArray();
		
		for(int test_case = 1; test_case <= T; test_case++)
		{
			int Answer =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			
			
			System.out.println("#"+test_case + " "+Answer);
		}
		br.close();
	}
}