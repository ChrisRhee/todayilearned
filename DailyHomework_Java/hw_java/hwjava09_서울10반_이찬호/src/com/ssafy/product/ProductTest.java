package com.ssafy.product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.ssafy.customer.Customer;
import com.ssafy.customer.CustomerMgrImpl;
import com.ssafy.customer.ICustomerMgr;

public class ProductTest {
	static IProductMgr mgr = ProductMgrImpl.getInstance() ;
	static ICustomerMgr cusList = CustomerMgrImpl.getInstance();
	static Scanner sc = new Scanner(System.in);
	static List<String[]> sales = new ArrayList<>();
	static int total = 0;
	
	public static void main(String[] args) {
		
		// 상품정보 객체 배열을 활용하여 저장
		mgr.add(new Product(101,"삼성 UHD TV", 140000, 20));
		mgr.add(new Product(102,"이찬호 TV", 10000, 100));
		mgr.add(new Product(201,"찬호 냉장고", 1200000, 20));
		mgr.add(new Product(202,"달별 냉장고", 1300000, 40));
		mgr.add(new Product(103,"SSAFY TV", 240000, 10));
		mgr.add(new Product(104,"Star TV", 500000, 40));
		mgr.add(new Product(203,"개복치 냉장고", 1400000, 30));
		mgr.add(new Product(204,"행복 냉장고", 1500000, 50));
		mgr.add(new Product(205,"냉장고", 1500000, 50));

		// 고객 추가
		cusList.add(new Customer("이찬호", "rch@email.com"));
		cusList.add(new Customer("개복치", "gbc@email.com"));
		cusList.add(new Customer("고릴라", "Gorila@email.com"));
		cusList.add(new Customer("원숭이", "Monkey@email.com"));
		cusList.add(new Customer("돌고래", "Dolphin@email.com"));
		cusList.add(new Customer("상어", "Shark@email.com"));
		cusList.add(new Customer("기린", "giraffe@email.com"));
		cusList.add(new Customer("코뿔소", "Rhinoceros@email.com"));
		
		// 구매 몇개 해놓기
		sales.add(new String[] { ""+(sales.size()+1),"찬호 냉장고", ""+15, ""+mgr.search("행복 냉장고").getPrice()*15});
		total += mgr.search("행복 냉장고").getPrice()*15;
		sales.add(new String[] { ""+(sales.size()+1),"삼성 UHD TV", ""+20, ""+mgr.search("삼성 UHD TV").getPrice()*20});
		total += mgr.search("삼성 UHD TV").getPrice()*20;
		sales.add(new String[] { ""+(sales.size()+1),"행복 냉장고", ""+5, ""+mgr.search("행복 냉장고").getPrice()*5});
		total += mgr.search("행복 냉장고").getPrice()*5;
		sales.add(new String[] { ""+(sales.size()+1),"Star TV", ""+3, ""+mgr.search("Star TV").getPrice()*3});
		total += mgr.search("Star TV").getPrice()*3;
		
		
		int end = -1;
		while(end !=0) {
			System.out.println("--- 상품 재고 관리 ---");
			System.out.println("1. 상품 정보 관리");
			System.out.println("2. 고객 정보 관리");
			System.out.println("3. 상품 판매 관리");
			System.out.println("0. 종료");
			System.out.print("선택: ");
			end = sc.nextInt();
			
			switch(end) {
				case 1: // 상품 정보 관리
					product();
					break;
				case 2: // 고객 정보 관리
					customer();
					break;
				case 3: // 상품 판매 관리
					salesStatus();
					break;
				case 0: // 종료
					break;
			}
			System.out.println();
		}
		sc.close();
	}
	
	
	private static void product() {
		int end=-1;
		while(end!=0) {
			System.out.println("");
			System.out.println("**1.상품 정보 관리**");
			System.out.println("1.상품 등록");
			System.out.println("2.상품 조회");
			System.out.println("3.상품 수정");
			System.out.println("4.상품 삭제");
			System.out.println("5.전체 상품 조회");
			System.out.println("0.종료");
			System.out.print("선택? ");
			end=sc.nextInt();
			switch(end) {
				case 1:{ 
					//* 제품번호, 제품명, 가격 정보, 재고수량
					System.out.print("제품 번호: ");
					int proNum=sc.nextInt();
					System.out.print("제품 이름: ");
					String proName=sc.next();
					System.out.print("가격: ");
					int proPrice=sc.nextInt();
					System.out.print("재고 수량: ");
					int proMount=sc.nextInt();
					Product p=new Product(proNum, proName, proPrice, proMount);
					mgr.add(p);
					break;
				}
				case 2:{  
					System.out.print("조회할 상품 이름: ");
					String proName=sc.next();
					mgr.search(proName);
					break;
				}
				case 3:{ 
					System.out.print("수정할 상품이름: ");
					String proName=sc.next();
					System.out.print("수정할 상품가격: ");
					int proPrice=sc.nextInt();
					mgr.update(proName,proPrice);
					break;
				}
				case 4:{ 
					System.out.print("삭제할 상품이름: ");
					String proName=sc.next();
					mgr.delete(proName);
					break;
				}
				case 5:{ 
					for(Product p: mgr.search()){
						System.out.println(p);
					}
					break;
				}
			}
		}
	}
	
	private static void customer() {
		int end=-1;
		while(end!=0) {
			System.out.println("");
			System.out.println("**2.고객관리**");
			System.out.println("1.고객 등록");
			System.out.println("2.고객 조회");
			System.out.println("3.고객 수정");
			System.out.println("4.고객 삭제");
			System.out.println("5.전체 고객 조회");
			System.out.println("0.종료");
			System.out.print("선택? ");
			end=sc.nextInt();
			switch(end) {
				case 1:{
					System.out.print("고객 이름: ");
					String name=sc.next();
					System.out.print("고객 이메일: ");
					String email=sc.next();
					Customer c=new Customer(name, email);
					cusList.add(c);
					break;
				}
				case 2:{
					System.out.print("조회할 고객이름: ");
					String name=sc.next();
					Customer c=cusList.search(name);
					System.out.println(c);
					break;
				}
				case 3:{
					System.out.print("수정할 고객이름: ");
					String name=sc.next();
					System.out.print("수정할 고객이메일: ");
					String email=sc.next();
					Customer c=new Customer(name, email);
					cusList.update(c);
					break;
				}
				case 4:{
					System.out.print("삭제할 고객이름: ");
					String name=sc.next();
					cusList.delete(name);
					break;
				}
				case 5:{
					for(Customer c:cusList.search()){
						System.out.println(c);
					}
					break;
				}
			}
		}
	}
	
	
	private static void salesStatus() {
		int end=-1;
		while(end!=0) {
			System.out.println("");
			System.out.println("**3.상품 판매 관리**");
			System.out.println("1.상품 구매");
			System.out.println("2.판매 현황");
			System.out.println("0.종료");
			System.out.print("선택? ");
			end=sc.nextInt();
			switch(end) {
				case 1:{ 
					System.out.print("구매할 상품 이름: ");
					String proName = sc.next();
					
					// 번호, 제품이름, 구매 갯수, 총 가격
					if(mgr.search(proName)!=null) {
						System.out.print("구매할 상품 수량: ");
						int proMount = sc.nextInt();
						sales.add(new String[] { ""+(sales.size()+1),proName, ""+proMount, ""+mgr.search(proName).getPrice()*proMount});
						total += mgr.search(proName).getPrice()*proMount;
					}
					else {
						System.out.println("\n"+proName+ " 은(는) 없는 제품입니다.");
					}
					break;
				}
				case 2:{ 
					System.out.println();
					for (String[] strings : sales) {
						System.out.println(Arrays.toString(strings));
					}
					System.out.print("총 금액: " + total);
					System.out.println();
					break;
				}
			}
		}
	}
}