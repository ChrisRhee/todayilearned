package com.ssafy.product;

public class Product {
	/** 멤버 변수
	 * 제품번호, 제품명, 가격 정보, 재고수량
	 * */
	private int num;
	private String name;
	private int price;
	private int mount;
	
	/** Constructors */
	public Product() {
		setName("");
	}

	public Product(int num, String name, int price, int mount) {
		setNum(num);
		setName(name);
		setPrice(price);
		setMount(mount);
	}
	
	
	/** Getters, Setters */
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getMount() {
		return mount;
	}
	public void setMount(int mount) {
		this.mount = mount;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + mount;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + num;
		result = prime * result + price;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (mount != other.mount)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (num != other.num)
			return false;
		if (price != other.price)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getNum());
		builder.append(" | ");
		builder.append(getName());
		builder.append(" | ");
		builder.append(getPrice());
		builder.append("원 | ");
		builder.append(getMount());
		builder.append("개");
		return builder.toString();
	}
	
}
