package com.ssafy.customer;

import java.util.List;

public interface ICustomerMgr {

	void add(Customer c);

	List<Customer> search();

	Customer search(String name);

	void update(Customer c);

	void delete(String name);

}