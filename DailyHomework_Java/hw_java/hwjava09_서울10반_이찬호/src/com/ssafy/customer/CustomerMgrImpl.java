package com.ssafy.customer;

import java.util.ArrayList;
import java.util.List;

public class CustomerMgrImpl implements ICustomerMgr {
	private List<Customer> list; 
	
	// Singleton
	private static CustomerMgrImpl instance = new CustomerMgrImpl();
	
	private CustomerMgrImpl() {
		list = new ArrayList();
	}
	public static CustomerMgrImpl getInstance() {
		return instance;
	}
	
	@Override
	public void add(Customer c) {
		list.add(c);
	}
	
	@Override
	public List<Customer> search() {
		return list;
	}
	
	@Override
	public Customer search(String name) {
		for (Customer customer : list) {
			if(customer.getName().equals(name)) {
				return customer;
			}
		}
		return null;
	}
	@Override
	public void update(Customer c) {
		Customer old = search(c.getName());
		if(old!=null) {
			int index = list.indexOf(old);
			list.set(index, c);
		}
	}
	
	@Override
	public void delete(String name) {
		list.remove(search(name));
	}
}

