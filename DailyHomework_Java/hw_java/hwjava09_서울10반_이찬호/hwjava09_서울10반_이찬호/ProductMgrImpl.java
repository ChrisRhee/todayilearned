package com.ssafy.product;

import java.util.ArrayList;
import java.util.Collection;

public class ProductMgrImpl implements IProductMgr {
	private Collection<Product> pCol ;
	
	// Singleton
	private static ProductMgrImpl instance = null;
	
	private ProductMgrImpl() {
		
	}
	
	private ProductMgrImpl(Collection<Product> products) {
		pCol = products;
	}
	
	public static ProductMgrImpl getInstance() {
		if(instance == null)
			instance = new ProductMgrImpl(new ArrayList());
		return instance;
	}

	
	@Override
	public boolean add(Product product) {
		return pCol.add(product);
	}

	@Override
	public Collection<Product> search() {
		Collection<Product> tmp = new ArrayList<Product>();
		for (Product product : pCol) {
			tmp.add(product);
		}
		return tmp;
	}

	// 제품 번호로 검색
	@Override
	public Product search(int num) {
		for (Product product : pCol) {
			if(product.getNum()== num)
				return product;
		}
		return null;
	}

	// 제품 이름으로 검색
	@Override
	public Product search(String name) {
		for (Product product : pCol) {
			if(product.getName().equals(name))
				return product;
		}
		return null;
	}


	// 제품 이름으로 검색해서 가격 수정하기
	@Override
	public void update(String name, int price) {
		search(name).setPrice(price);
	}
	
	// 제품 번호로 삭제
	@Override
	public void delete(int num) {
		for (Product product : pCol) {
			if(product.getNum() == num)
			{
				pCol.remove(product);
				break;
			}
		}
	}
	
	// 상품 이름으로 삭제
	@Override
	public void delete(String name) {
		for (Product product : pCol) {
			if(product.getName().equals(name))
			{
				pCol.remove(product);
				break;
			}
		}
	}

	@Override
	public int totalPrice() {
		int total = 0;
		for (Product product : pCol) {
			total += product.getPrice() * product.getMount();
		}
		return total;
	}

	

}
