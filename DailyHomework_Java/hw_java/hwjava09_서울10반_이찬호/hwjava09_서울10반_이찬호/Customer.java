package com.ssafy.customer;

public class Customer {
	private String name;
	private String email;
	
	
	public Customer() {
		this("","");
	}

	public Customer(String name, String email) {
		setName(name);
		setEmail(email);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("이름: ");
		builder.append(name);
		builder.append(" | 메일: ");
		builder.append(email);
		return builder.toString();
	}
	
	
	
}
