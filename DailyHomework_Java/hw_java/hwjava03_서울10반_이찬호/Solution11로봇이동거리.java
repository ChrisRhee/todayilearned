package com.ssafy.algo;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution11로봇이동거리 {
	static int T, N;
	static int Answer;
	//                             상     하    좌      우
	static int dir[][] = {{0,0},{-1,0},{1,0},{0,-1},{0,1}};
	
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("Solution11로봇이동거리.txt"));
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			N = sc.nextInt();
			char[][] map=new char[N][N];
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					map[i][j] = sc.next().charAt(0);
				}
			}
			//////////////////////////////
			//( 이 부분에 알고리즘 구현을 한다. )//
			//////////////////////////////
			
			//전체 이동 수
			int move_count =0;
			boolean check = true;
			for(int i =0; i<N ; i++) {
				for(int j=0; j<N; j++) {
					// S는 공백, W는 벽, A,B,C 로봇
					// A는 →,  B는 ← →,  C는 → ← ↑ ↓
					
					int ii = i + dir[0][0];
					int jj = j + dir[0][1];
					//A면 오른쪽이 0,A,B,C 가 될때까지 이동
					// = A 오른쪽에 다른게 나오기 전까지 count 추가 하기
					if (map[i][j]== 'A') {
						ii = i + dir[4][0];
						jj = j + dir[4][1];
						
						// 한칸 오른쪽이 1이면 진행
						if(ii < N && ii >= 0 && jj < N && jj >= 0 ) {
							while(map[ii][jj] == 'S') {
								//카운트 증가시키고,
								move_count++;
								//한칸 옆으로 이동
								ii = ii + dir[4][0];
								jj = jj + dir[4][1];
								if(ii >= N || ii < 0 || jj >= N || jj < 0) {
									check = false;
									break;
								}
							}
							check = true;
						}
						//다른게 나오면 A가 이동할 수 있는 것 끝
					}
					
					//B면 왼쪽, 오른쪽이 0,A,B,C 가 될 때까지 이동
					if (map[i][j]== 'B') {
						//좌우 한번씩 다해봄
						for(int d = 0; d<2 ; d++) {
							ii = i + dir[d+3][0];
							jj = j + dir[d+3][1];
							// 한칸 오른쪽이 1이면 진행
							if(ii < N && ii >= 0 && jj < N && jj >= 0) {
								while(map[ii][jj] == 'S') {
									//카운트 증가시키고,
									move_count++;
									//한칸 옆으로 이동
									ii = ii + dir[d+3][0];
									jj = jj + dir[d+3][1];
									if(ii >= N || ii < 0 || jj >= N || jj < 0) {
										check = false;
										break;
									}
								}
							}
							//다른게 나오면 B가 이동할 수 있는 것 끝
						}
						
					}
					//C면 위, 아래, 왼쪽, 오른쪽이 0,A,B,C 가 될 때까지 이동
					if (map[i][j]== 'C') {
						//좌우 한번씩 다해봄
						for(int d = 0; d<4 ; d++) {
							ii = i + dir[d+1][0];
							jj = j + dir[d+1][1];
							// 한칸 오른쪽이 1이면 진행
							if(ii < N && ii >= 0 && jj < N && jj >= 0 ) {
								while(map[ii][jj] == 'S') {
									//카운트 증가시키고,
									move_count++;
									//한칸 이동
									ii = ii + dir[d+1][0];
									jj = jj + dir[d+1][1];
									if(ii >= N || ii < 0 || jj >= N || jj < 0) {
										check = false;
										break;
									}
								}
								check = true;
							}
							//다른게 나오면 B가 이동할 수 있는 것 끝
						}
					}
					Answer = move_count;
				}
			}

			System.out.println("#"+test_case+" "+Answer);
		}
	}
}
