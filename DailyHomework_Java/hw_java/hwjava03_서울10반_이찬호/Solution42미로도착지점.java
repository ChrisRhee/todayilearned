package com.ssafy.algo;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution42미로도착지점 {
	static int N, Si, Sj, P, D;
	static int Ai, Aj;
	
	//                            상      우    하     좌 
	static int dir[][] = {{0,0},{-1,0},{0,1},{1,0},{0,-1}};
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("Solution42미로도착지점.txt"));
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			N = sc.nextInt();  //미로 크기 N*N
			//시작위치가 1,1이므로 한칸 더 크게 하자
			int maze[][]=new int[N+1][N+1];
			
			Si = sc.nextInt(); //출발점의 i좌표
			Sj = sc.nextInt(); //출발점의 j좌표
			P = sc.nextInt();  //함정 갯수

			int Pi[] = new int[P];  // 함정의 i좌표의 모음
			int Pj[] = new int[P];  // 함정의 j좌표의 모음

			for(int k=0; k<P; k++) {
				Pi[k] = sc.nextInt();
				Pj[k] = sc.nextInt();
			}

			D = sc.nextInt();       // 이동 지시 개수
			int Dd[] = new int[D];  // 방향 모음
			int Da[] = new int[D];  // 이동 칸수 모음

			for(int k=0; k<D; k++) {
				Dd[k] = sc.nextInt();
				Da[k] = sc.nextInt();
			}
			//////////////////////////////
			//( 이 부분에 알고리즘 구현을 한다. )//
			//////////////////////////////
			// 미로에 점퍼 배치, 점퍼는 -1
			jumper:for(int k=0; k<P; k++) {
				maze[Pi[k]][Pj[k]] = -1;
			} //end jumper:for
			
			// 이동을 시작해봅시다. 이동 지시 갯수 D만큼
			move:for(int k=0; k<D; k++) {
				// 시작위치에서 이동지시 방향 * 이동 거리만큼 이동
				// 1 3,  2 1,  3 1,  4 1,  1 2
				
				// 이동거리만큼 한번씩 이동해보자
				for(int m=0 ; m<Da[k]; m++) {
					int ii = Si + dir[Dd[k]][0] ;
					int jj = Sj + dir[Dd[k]][1] ;
					
					// 1이 최소 , N이 최대값이니 포함됨.
					// 그 이동한 것들이 맵 밖으로 나가거나 점퍼를 만나면
					if( 1>ii || ii>N || 1>jj || jj >N || maze[ii][jj]<0) {
						// (0,0)을 갖게 된다.
						Ai = 0;
						Aj = 0;
						break ;//더이상 할 필요 없음.
					}
					maze[Si][Sj] = 10;
					maze[ii][jj] = 5;
					
					// 만약 나가지도 않고, 점퍼도 안만났으면 일단 이렇게 쭉 가
					// 최종까지 안건드리면 알아서 도달할듯
					Si = ii;
					Sj = jj;
					
					Ai = ii;
					Aj = jj;
				}
			}
			System.out.println("#"+test_case+" "+Ai+" "+Aj);
		}
	}
}
























