package com.ssafy.algo;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution21소금쟁이중첩 {
	static long Answer;
	static int N, S;
	
	//     	                      하     우
	static int dir[][] = {{0,0},{1,0},{0,1}};
	static int jump[] = {3,2,1};

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("Solution21소금쟁이중첩.txt"));
		Scanner sc=new Scanner(System.in);
		int T=sc.nextInt();//테스트 케이스 수

		for(int test_case=1; test_case<=T; test_case++){
			N=sc.nextInt(); //배열의 크기 NxN
			int[][] lake=new int[N][N];

			S=sc.nextInt();//소금쟁이(Strider)수
			int[][] strider=new int[S][3];
			for(int k=0; k<S; k++){
				strider[k][0]=sc.nextInt();//행위치
				strider[k][1]=sc.nextInt();//열위치
				strider[k][2]=sc.nextInt();//방향(1:하,2:우)
			}
			//////////////////////////////
			//( 이 부분에 알고리즘 구현을 한다. )//
			//////////////////////////////
			Answer = 0;
			
			loop:for(int s=0; s<S; s++) {
				//현재 자리에 누군가 있다면 그 자리 출력 그리고 종료
				if(lake[strider[s][0]][strider[s][1]]>0) {
					Answer = lake[strider[s][0]][strider[s][1]];
					break loop;
				}
				//방향
				int d = strider[s][2];
				//소금쟁이 y축
				int i = strider[s][0];
				//소금쟁이 x축
				int j = strider[s][1];
				//점프 길이 만큼 뛰어보자
				for(int k=0; k<jump.length; k++) {
				
					// ii 는 새로 점프할 위치, i는 기존 위치
					int ii = i + dir[d][0]*jump[k];
					int jj = j + dir[d][1]*jump[k];
					// 점프할 위치가 넘쳤으면 그 뛰기 전 자리에 멈춤 
					if( 0>ii || ii>=N || 0>jj || jj >=N) {
						//뛰기 전 자리에 소금쟁이 값을 넣음.
						lake [i][j] = s+1;
						//더 뛸 필요없으니 다음 소금쟁이로 고고
						continue loop;
					}
					
					// 점프했는데 그 자리에 누군가 있다면
					if(lake[ii][jj] >0) {
						//그 자리에 있던 값(기존에 그 자리에있던 소금쟁이)을 출력
						//Answer = lake[ii][jj];
						//새롭게 뛴 소금쟁이출력
						Answer = lake[i][j];
						//더 할필요 없으니 루프 끝냄
						break loop;
					}
					
					// 근데 아무도 없었고, 넘치지 않고 제대로 된 자리라면, 그 자리에 소금쟁이가 들어감
					lake[ii][jj] = s+1;
					//그러고 jj만큼 ii만큼 각각 소금쟁이 이동
					i = ii;
					j = jj;
				}
			}
			//for (int n[] : lake) System.out.println(Arrays.toString(n));
			//System.out.println();
			System.out.println("#"+test_case+" "+Answer);
		}
	}
}
