package SWExpertAcademy;
import java.util.Scanner;
import java.util.Stack;
import java.io.FileInputStream;
class Solution5432_쇠막대기자르기_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input5432.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();
		for(int test_case = 1; test_case <= T; test_case++)
		{
			int Answer =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			
			Stack<String> bar = new Stack<String>();
			
			String s[] = sc.next().split("");
			for(int i=0; i<s.length-1; i++) {
				if(s[i].equals("(")) bar.push(s[i]);
				// 닫혔을때
				else  {
					bar.pop();
					// 그 전꺼가 열리는거였으면 레이저 쏜거
					if(s[i-1].equals("(")) {Answer += bar.size();}
					// 닫히는거면 그냥 줄어든거
					else Answer ++;
				}
			}
			System.out.println("#"+test_case + " "+(Answer+1));
		}
		sc.close();
	}
}