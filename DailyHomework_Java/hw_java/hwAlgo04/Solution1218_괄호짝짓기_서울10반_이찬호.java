package SWExpertAcademy;
import java.io.FileInputStream;
import java.util.Scanner;
import java.util.Stack;
class Solution1218_괄호짝짓기_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1218.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int test_case = 1; test_case <= 10; test_case++)
		{
			int T=sc.nextInt();
			int Answer =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			Stack<String> bracket1= new Stack<>();
			Stack<String> bracket2= new Stack<>();
			Stack<String> bracket3= new Stack<>();
			Stack<String> bracket4= new Stack<>();
			
			String str[] = sc.next().split("");
			loop:for(int i=0; i<str.length; i++) {
				switch(str[i]) {
					case "[":
						bracket1.push(str[i]);
						break;
					case "]":
						if(bracket1.size() > 0) bracket1.pop();
						else break loop;
						break;
					case "<":
						bracket2.push(str[i]);
						break;
					case ">":
						if(bracket2.size() > 0) bracket2.pop();
						else break loop;
						break;
					case "(":
						bracket3.push(str[i]);
						break;
					case ")":
						if(bracket3.size() > 0) bracket3.pop();
						else break loop;
						break;
					case "{":
						bracket4.push(str[i]);
						break;
					case "}":
						if(bracket4.size() > 0) bracket4.pop();
						else break loop;
						break;
				}
			}
			if(bracket1.size()+bracket2.size()+bracket3.size()+bracket4.size()==0) Answer =1 ;
			
			System.out.println("#"+test_case + " "+Answer);
		}
		sc.close();
	}
}