package com.ssafy.contact.client;

import java.awt.*;
import java.awt.event.*;

public class ContactMain implements ActionListener,ItemListener{
	private Frame f=new Frame("Phone Book");
	private Label custl=new Label("Phone Book",Label.CENTER);
	private Label namel=new Label("이 름", Label.CENTER);
	private Label phonel=new Label("핸드폰", Label.CENTER);
	private Label addrl=new Label("주 소", Label.CENTER);
	private TextField nametf=new TextField(); 
	private TextField phonetf=new TextField(); 
	private TextField addrtf=new TextField(); 
	private Button insertb=new Button("INSERT"); 
	private Button deleteb=new Button("DELETE"); 
	private Button updateb=new Button("UPDATE"); 
	private Button searchb=new Button("SEARCH"); 
	private Button uploadb=new Button("UPLOAD"); 
	private Button clearb=new Button("CLEAR"); 
	private Button exitb=new Button("EXIT"); 
	private List list=new List();
	private Panel p1=new Panel();
	private Panel p2=new Panel();
	private Panel p2n=new Panel();
	private Panel p2c=new Panel();
	private Panel p2s=new Panel();

	private Label msgl=new Label("ok");//메세지 라벨
	
	public ContactMain() {

	}
	
	public void createGUI() {
		f.setLayout(new GridLayout(2,1,5,5));
		p1.setLayout(new BorderLayout());
		p2.setLayout(new BorderLayout());
		
		p1.add(custl, BorderLayout.NORTH);
		p1.add(list);
		p1.add(msgl, BorderLayout.SOUTH);
		msgl.setForeground(Color.RED);
		
		p2n.add(insertb);
		p2n.add(deleteb);
		p2n.add(updateb);
		p2n.add(searchb);
		
		p2c.setLayout(new GridLayout(3,2,5,7));
		p2c.add(namel);  p2c.add(nametf);
		p2c.add(phonel); p2c.add(phonetf);
		p2c.add(addrl);  p2c.add(addrtf);
		
		p2s.add(uploadb);
		p2s.add(clearb);
		p2s.add(exitb);
		
		p2.add(p2n, BorderLayout.NORTH);
		p2.add(p2c);
		p2.add(p2s, BorderLayout.SOUTH);
		
		f.add(p1);
		f.add(p2);
		
		f.setSize(350, 350);
		f.setVisible(true);
	}
	public void addEvent() {

	}
	public void showList() {
		
	}
	public static void main(String[] args) {
		ContactMain cm=new ContactMain();
	}
	@Override
	public void actionPerformed(ActionEvent e) {

	}
	@Override
	public void itemStateChanged(ItemEvent e) {
		
	}
}
