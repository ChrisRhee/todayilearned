import java.util.Scanner;
import java.io.FileInputStream;
class Solution1210_Ladder1_서울10반_이찬호2{
	public static int N = 100;
	public static int data[][]= new int[N][N];
	
	public static int solve(int i, int end) {
		if(i==0) return end;
		data[i][end] = 0;
		if((end-1 >=0) && data[i][end-1]==1) end--;
		else if((end+1 <100) && data[i][end+1]==1) end++;
		else i--;
		return solve(i, end);
	}
	
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1210.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int test_case = 1; test_case <= 10; test_case++)
		{
			int T=sc.nextInt();
			int Answer =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					data[i][j] = sc.nextInt();
					if(data[i][j] == 2) Answer= j;
				}
			}
			
			System.out.println("#"+T + " "+solve(99,Answer));
		}
		sc.close();
	}
}