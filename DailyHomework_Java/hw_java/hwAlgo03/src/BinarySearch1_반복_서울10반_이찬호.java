
public class BinarySearch1_반복_서울10반_이찬호{
	public static boolean binarySearch(int a[], int key) {
		int start = 0;
		int end = a.length-1;
		while(start <= end) {
			int middle = (start + end)/2;
			if(a[middle] == key) {
				return true;
			}
			else if(a[middle] > key) {
				end = middle -1;
			}
			else start = middle +1;
		}
		return false;
	}
	
	
	public static void main(String args[]) throws Exception	{
		int a[] = new int[] {1,2,3,4,5,6,7,8,9,10};
		System.out.println(binarySearch(a,6));
		System.out.println(binarySearch(a,11));
	}
}