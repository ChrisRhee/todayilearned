import java.util.Scanner;
import java.io.FileInputStream;
class Solution1210_Ladder1_서울10반_이찬호3{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1210.txt"));
		Scanner sc = new Scanner(System.in);
		int N = 100;
		int data[][]= new int[N][N];
		
		for(int test_case = 1; test_case <= 10; test_case++)
		{
			int T=sc.nextInt();
			int Answer =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			for(int i=0; i<N; i++) {
				for(int j=0; j<N; j++) {
					data[i][j] = sc.nextInt();
					if(data[i][j] == 2) Answer= j;
				}
			}
			
			for(int i =99; i>=0;) {
				data[i][Answer] = 0;
				if((Answer-1 >=0) && data[i][Answer-1]==1) Answer--;
				else if((Answer+1 <100) && data[i][Answer+1]==1) Answer++;
				else i--;
			}
			
			System.out.println("#"+T + " "+Answer);
		}
		sc.close();
	}
}