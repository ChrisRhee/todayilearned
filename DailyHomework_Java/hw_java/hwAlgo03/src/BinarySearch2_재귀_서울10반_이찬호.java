
public class BinarySearch2_재귀_서울10반_이찬호{
	public static boolean binarySearch(int a[], int low, int high, int key) {
		int middle;
		//검색 실패
		if(low > high) {
			return false;
		}
		middle = (low + high)/2;
		
		// 찾음
		if(key == a[middle]) {
			return true;
		}
		else if(key < a[middle]) {
			return binarySearch(a, low, middle-1, key);
		}
		else{
			return binarySearch(a, middle+1, high, key);
		}
	}
	
	
	public static void main(String args[]) throws Exception	{
		int a[] = new int[] {1,2,3,4,5,6,7,8,9,10};
		System.out.println(binarySearch(a, 0, 9, 11));
		System.out.println(binarySearch(a, 0, 9, 7));
	}
}