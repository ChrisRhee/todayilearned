package com.ssafy.algo;

import java.util.Scanner;

public class Main540 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int num = 1;
				
		while(num != -1) {
			num = sc.nextInt();
			//3으로 나눠지면 3으로 나눔
			if(num%3 == 0) {
				num = num / 3;
				System.out.println(num);
			}
		}

		sc.close();
	}

}
