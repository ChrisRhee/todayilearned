package com.ssafy.algo;

import java.util.Scanner;

public class Main529 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int input_height = sc.nextInt();
		int input_weight = sc.nextInt();
		int fat_value = input_weight + 100 - input_height;
		System.out.println(fat_value);		
		if(fat_value > 0) {
			System.out.println("Obesity");
		}
		sc.close();
	}
}
