package com.ssafy.algo;

import java.util.Scanner;

public class Main535 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		double grade = sc.nextDouble();
		while(grade < 0 || grade > 4.5) {
			grade = sc.nextDouble();
		}
		
		switch((int)grade) {
			case 4:
				System.out.println("scholarship");
				break;
			case 3:
				System.out.println("next semester");
				break;
			case 2:
				System.out.println("seasonal semester");
				break;
			default:
				System.out.println("retake");
				break;
		}
		sc.close();
	}
}
