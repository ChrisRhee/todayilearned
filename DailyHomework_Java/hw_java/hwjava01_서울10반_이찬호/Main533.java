package com.ssafy.algo;

import java.util.Scanner;

public class Main533 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		String sex = sc.next();
		int age = sc.nextInt();
		
		if (sex.equals("M")) {
			if(age >= 18) {
				System.out.println("MAN");
			}
			else {
				System.out.println("BOY");
			}
		}else if (sex.equals("F")) {
			if(age >= 18) {
				System.out.println("WOMAN");
			}
			else {
				System.out.println("GIRL");
			}
		}
		sc.close();
	}
}
