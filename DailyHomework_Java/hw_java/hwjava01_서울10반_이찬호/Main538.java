package com.ssafy.algo;

import java.util.Scanner;

public class Main538 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num = 1;
		while(num != 0) {
			System.out.print("number? ");
			num = sc.nextInt();
			if(num > 0) {
				System.out.println("positive integer");
			}else if ( num <  0){
				System.out.println("negative integer");
			}
		}
		sc.close();
	}
}
