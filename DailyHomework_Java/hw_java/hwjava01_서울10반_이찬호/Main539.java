package com.ssafy.algo;

import java.util.Scanner;

public class Main539 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int num = 1;
		int sum = 0;
		float num_count = 0.0f;
		float average = 0.0f;
		while(num < 100) {
			num = sc.nextInt();
			num_count++;
			sum = sum + num;
			average = Math.round((sum/num_count)*10.0f)/10.0f;
		}
		
		System.out.println(sum);
		System.out.println(average);
		sc.close();
	}

}
