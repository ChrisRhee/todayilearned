package com.ssafy.algo;

import java.util.Scanner;

public class DigitTest1 {
	public static void main(String[] args) {
		int num = 1;
		int ia[] = new int[] {0,0,0,0,0,0,0,0,0,0};
		Scanner sc = new Scanner(System.in);
		while (num !=0) {
			num = sc.nextInt();
			if(num<100 && num !=0) {
				ia[num/10]++;
			}
		}
		for (int i =0; i<ia.length ; i++) {
			if(ia[i] != 0) {
				System.out.println(i + " : "+ ia[i] + "개");
			}
		}
	}
}
