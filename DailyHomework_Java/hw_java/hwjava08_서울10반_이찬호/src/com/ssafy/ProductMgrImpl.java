package com.ssafy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class ProductMgrImpl implements IProductMgr {
	private Collection<Product> pCol ;
	
	// Singleton
	private static ProductMgrImpl instance = null;
	
	private ProductMgrImpl() {
		this(new HashSet<Product>());
	}
	
	private ProductMgrImpl(Collection<Product> products) {
		pCol = products;
	}
	
	public static ProductMgrImpl getInstance() {
		if(instance == null)
			instance = new ProductMgrImpl(new ArrayList());
		return instance;
	}

	
	@Override
	public boolean add(Product product) {
		return pCol.add(product);
	}

	@Override
	public Collection<Product> search() {
		Collection<Product> tmp = new ArrayList<Product>();
		for (Product product : pCol) {
			tmp.add(product);
		}
		return tmp;
	}

	@Override
	public Product search(int num) {
		for (Product product : pCol) {
			if(product.getNum()== num)
				return product;
		}
		return null;
	}

	@Override
	public Product search(String name) {
		for (Product product : pCol) {
			if(product.getName().equals(name))
				return product;
		}
		return null;
	}

	@Override
	public Collection<Tv> searchTv() {
		Collection<Tv> tmp = new ArrayList<Tv>();
		for (Product product : pCol) {
			if(product instanceof Tv)
				tmp.add((Tv)product);
		}
		return tmp;
	}

	@Override
	public Collection<Refrigerator> searchRefrigerator() {
		Collection<Refrigerator> tmp = new ArrayList<Refrigerator>();
		for (Product product : pCol) {
			if(product instanceof Refrigerator)
				tmp.add((Refrigerator)product);
		}
		return tmp;
	}

	@Override
	public Collection<Refrigerator> searchByLiter() {
		Collection<Refrigerator> tmp = new ArrayList<Refrigerator>();
		for (Product product : pCol) {
			if(product instanceof Refrigerator && ((Refrigerator) product).getLiter() >= 400)
				tmp.add((Refrigerator)product);
		}
		return tmp;
	}

	@Override
	public Collection<Tv> searchByInch() {
		Collection<Tv> tmp = new ArrayList<Tv>();
		for (Product product : pCol) {
			if(product instanceof Tv && ((Tv) product).getInch() >= 50)
				tmp.add((Tv)product);
		}
		return tmp;
	}

	@Override
	public void update(int num, int price) {
		search(num).setPrice(price);
	}

	@Override
	public void delete(int num) {
		for (Product product : pCol) {
			if(product.getNum() == num)
			{
				pCol.remove(product);
				break;
			}
		}
	}

	@Override
	public int totalPrice() {
		int total = 0;
		for (Product product : pCol) {
			total += product.getPrice();
		}
		return total;
	}

}
