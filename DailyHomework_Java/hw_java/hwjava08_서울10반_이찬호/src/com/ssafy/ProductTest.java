package com.ssafy;

public class ProductTest {
	public static void main(String[] args) {
		ProductMgrImpl mgr = ProductMgrImpl.getInstance() ;
		
		//num, name, price, mount, inch or liter;
		// 상품정보 객체 배열을 활용하여 저장
		mgr.add(new Tv(101,"삼성 UHD TV", 140000, 20, 53));
		mgr.add(new Tv(102,"이찬호 TV", 10000, 100, 40));
		mgr.add(new Refrigerator(201,"찬호 냉장고", 1200000, 20, 500));
		mgr.add(new Refrigerator(202,"달별 냉장고", 1300000, 40, 300));
		mgr.add(new Tv(103,"SSAFY TV", 240000, 10, 60));
		mgr.add(new Tv(104,"Star TV", 500000, 40, 35));
		mgr.add(new Refrigerator(203,"개복치 냉장고", 1400000, 30, 450));
		mgr.add(new Refrigerator(204,"행복 냉장고", 1500000, 50, 350));
		
		System.out.println("--- 2. 상품정보 전체를 검색하는 기능");
		for (Product product : mgr.search()) {
			if(product != null) System.out.println(product);
		}
		
		System.out.println("\n--- 3. 상품번호로 검색하는 기능 | 상품번호 : 103");
		System.out.println(mgr.search(103));
		
		System.out.println("\n--- 4. 상품명으로 검색하는 기능 | 상품명 : 개복치 냉장고");
		System.out.println(mgr.search("개복치 냉장고"));
		
		System.out.println("\n--- 5. TV정보만 검색");
		for (Product product : mgr.searchTv()) {
			System.out.println(product);
		}
		
		System.out.println("\n--- 6. Refrigerator정보만 검색");
		for (Product product : mgr.searchRefrigerator()) {
			System.out.println(product);
		}
		
		System.out.println("\n--- 7. 400L이상 Refrigerator정보만 검색");
		for (Product product : mgr.searchByLiter()) {
			System.out.println(product);
		}
		
		System.out.println("\n--- 8. 50inch이상 TV정보만 검색");
		for (Product product : mgr.searchByInch()) {
			System.out.println(product);
		}
		
		System.out.println("\n--- 9. 상품번호로 가격 변경 | 상품번호 201, 가격 11112222");
		System.out.println("변경 전");
		System.out.println(mgr.search(201));
		mgr.update(201, 11112222);
		System.out.println("변경 후");
		System.out.println(mgr.search(201));
		
		
		System.out.println("\n--- 10. 상품번호로 삭제 | 상품번호 202");
		mgr.delete(202);
		for (Product product : mgr.search()) {
			System.out.println(product);
		}
		
		System.out.println("\n--- 11. 전체 재고 상품 금액");
		System.out.println(mgr.totalPrice()+"원");
	}
}
