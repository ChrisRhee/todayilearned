package com.ssafy;

public class Refrigerator extends Product{
	private int liter;

	public Refrigerator(int num, String name, int price, int mount, int liter) {
		super(num, name, price, mount);
		setLiter(liter);
	}
	
	public Refrigerator() {
		setName("");
	}

	public int getLiter() {
		return liter;
	}

	public void setLiter(int liter) {
		this.liter = liter;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + liter;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Refrigerator other = (Refrigerator) obj;
		if (liter != other.liter)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.toString());
		builder.append(" | ");
		builder.append(getLiter());
		builder.append("L");
		return builder.toString();
	}
	
}
