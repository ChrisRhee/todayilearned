package com.ssafy;

public class Tv extends Product{
	private int inch;
	
	public Tv() {
		setName("");
	}

	public Tv(int num, String name, int price, int mount, int inch) {
		super(num, name, price, mount);
		setInch(inch);
	}

	public int getInch() {
		return inch;
	}

	public void setInch(int inch) {
		this.inch = inch;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + inch;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tv other = (Tv) obj;
		if (inch != other.inch)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.toString());
		builder.append(" | ");
		builder.append(getInch());
		builder.append("inch");
		return builder.toString();
	}
	
	
}
