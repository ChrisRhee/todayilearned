package com.ssafy;

import java.util.Collection;

public interface IProductMgr {
	
	/** 상품정보(Tv와 냉장고)를 객체 배열을 활용하여 저장
	 * 입력 : Product 객체
	 * 출력 : x
	 * */
	boolean add(Product product);
	
	/** 상품정보 전체를 검색하는 기능
	 * 입력 : x
	 * 출력 : Product Array
	 * */
	Collection<Product> search();
	
	/** 상품번호로 상품을 검색하는 기능
	 * 입력 : int num (상품번호)
	 * 출력 : Product (맞는 상품)
	 * */
	Product search(int num);
	/** 상품명으로 상품을 검색하는 기능(상품명 부분 검색 가능)
	 * 입력 : String name
	 * 출력 : Product (맞는 상품)
	 * */
	Product search(String name);
	
	/** Tv정보만 검색
	 * 입력 : x
	 * 출력 : Tv만 담은 Product 배열
	 * */
	Collection<Tv> searchTv() ;
	
	/** Refrigerator정보만 검색
	 * 입력 : x
	 * 출력 : Refrigerator만 담은 Product 배열
	 * */
	Collection<Refrigerator> searchRefrigerator() ;
	
	/** 400L이상의 Refrigerator정보만 검색
	 * 입력 : x
	 * 출력 : Refrigerator만 담은 Product 배열
	 * */
	Collection<Refrigerator> searchByLiter() ;
	
	/** 50inch이상의 Tv정보만 검색
	 * 입력 : x
	 * 출력 : 50inch이상 Tv만 담은 Product 배열
	 * */
	Collection<Tv> searchByInch() ;
	
	/** 상품번호와 가격을 입력받아 상품 가격을 변경할 수 있는 기능
	 * 입력 : int num, int price
	 * 출력 : x
	 * */
	void update(int num, int price) ;
	
	/** 상품번호로 상품을 삭제하는 기능
	 * 입력 : int num
	 * 출력 : x
	 * */
	void delete(int num) ;
	
	/** 전체 재고 상품 금액 구하는 기능
	 * 입력 : x
	 * 출력 : int total (전체 금액)
	 * */
	int totalPrice() ;
}
