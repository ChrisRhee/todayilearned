package com.ssafy.contact.client;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAO {
	private List<Customer> list;
	private String filename = "cust.dat";
	
	private static CustomerDAO instance = null;
	
	private CustomerDAO() {
		list = new ArrayList<>();
		list.add(new Customer("이찬호","010-2663-7242","분당"));
		list.add(new Customer("안젤라","010-2663-1234","인천"));
		list.add(new Customer("크리스","010-2663-4567","당분"));
	}

	public static CustomerDAO getInstance() {
		if(instance == null) {
			instance = new CustomerDAO();
		}
		return instance;
	}
	
	public void insert(String name, String phone, String addr) throws ExistException {
		try {
			Customer c = search(name);
			throw new ExistException();
		} catch (NotFoundException e) {
			list.add(new Customer(name,phone,addr));
		}
	}
	
	public Customer search(String name) throws NotFoundException{
		for (Customer customer : list) {
			if(customer.getName().equals(name)) {
				return customer;
			}
		}
		throw new NotFoundException();
	}
	
	public void delete(String name) throws NotFoundException{
		list.remove(search(name));
	}
	
	public void update(String name, String num) throws NotFoundException{
		search(name).setNum(num);
	}
	
	public List<Customer> allCust(){
		return list;
	}
	
	public void open() {
		File f = new File(filename);
		if(!f.exists()) return;
		//list.clear();
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(f));
			list = (ArrayList<Customer>)ois.readObject();
			
//			while(true) {
//				Customer c = (Customer)ois.readObject();
//				list.add(c);
//			}
//		} catch (EOFException e) {
//			System.out.println("읽기 OK");
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if(ois!= null) ois.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void upload() throws Exception{
		ContactThread ct = new ContactThread();
		ct.run();
	}
	
	class ContactThread extends Thread{
		@Override
		public void run() {
			Socket s = null;
			ObjectOutputStream oos =null;
			try {
				s = new Socket("127.0.0.1", 9000);
				oos = new ObjectOutputStream(s.getOutputStream());
				
				//oos.writeObject(list);
				//oos.flush();
				for(Customer c:list) {
					oos.writeObject(c);
					oos.flush();
				}
				
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if(oos!= null) oos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				try {
					if(s!=null) s.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void close() throws Exception{
		File f = new File(filename);
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f));
//		for(Customer c:list) {
//		oos.writeObject(c);
//		oos.flush();
//	}
		oos.writeObject(list);
		oos.flush();
		oos.close();
		
	}
	
}
