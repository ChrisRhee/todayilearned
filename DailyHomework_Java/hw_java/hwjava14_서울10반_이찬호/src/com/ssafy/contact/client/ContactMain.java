package com.ssafy.contact.client;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.List;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputMethodListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ContactMain implements ActionListener, ItemListener{
	
	private JFrame f = new JFrame("Phone Book");
	private JLabel custl = new JLabel("Phoen Book",JLabel.CENTER);
	private JLabel namel  = new JLabel("이    름",JLabel.CENTER);
	private JLabel phonel = new JLabel("핸드폰",JLabel.CENTER);
	private JLabel addrl  = new JLabel("주    소",JLabel.CENTER);
	
	private JTextField nametf = new JTextField();
	private JTextField phonetf = new JTextField();
	private JTextField addrtf = new JTextField();
	
	private JButton btnINSERT = new JButton("INSERT");
	private JButton btnDELETE = new JButton("DELETE");
	private JButton btnUPDATE = new JButton("UPDATE");
	private JButton btnSEARCH = new JButton("SEARCH");
	private JButton btnUPLOAD = new JButton("UPLOAD");
	private JButton btnCLEAR = new JButton("CLEAR");
	private JButton btnEXIT = new JButton("EXIT");
	
	private JList list = new JList();
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p2north = new JPanel();
	private JPanel p2center = new JPanel();
	private JPanel p2south = new JPanel();
	
	private JLabel msgl= new JLabel("ok",JLabel.CENTER);
	
	private CustomerDAO dao;
	
	public ContactMain() {
		dao = CustomerDAO.getInstance();
		
		// main시작할 때 바로 읽음
		dao.open();
		
		createGUI();
		addEvent();
		showList();
	}
	
	public void createGUI() {
		f.setLayout(new GridLayout(2,1,5,5));
		p1.setLayout(new BorderLayout());
		p2.setLayout(new BorderLayout());
		p1.add(custl, BorderLayout.NORTH);
		p1.add(list, BorderLayout.CENTER);
		p1.add(msgl, BorderLayout.SOUTH);
		msgl.setForeground(Color.RED);
		
		p2north.add(btnINSERT);
		p2north.add(btnDELETE);
		p2north.add(btnUPDATE);
		p2north.add(btnSEARCH);
		
		p2center.setLayout(new GridLayout(3,2,10,10));
		p2center.add(namel); p2center.add(nametf);
		p2center.add(phonel); p2center.add(phonetf);
		p2center.add(addrl); p2center.add(addrtf);
		
		p2south.add(btnUPLOAD);
		p2south.add(btnCLEAR);
		p2south.add(btnEXIT);
		
		p2.add(p2north, BorderLayout.NORTH);
		p2.add(p2center, BorderLayout.CENTER);
		p2.add(p2south, BorderLayout.SOUTH);
	
		f.add(p1);
		f.add(p2);
		
		f.setSize(400, 400);
		f.setVisible(true);
	}
	
	public void addEvent() {
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				f.dispose();
			}
		});
		
		btnINSERT.addActionListener(this);
		btnINSERT.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		btnDELETE.addActionListener(this);
		btnUPDATE.addActionListener(this);
		btnSEARCH.addActionListener(this);
		btnUPLOAD.addActionListener(this);
		btnCLEAR.addActionListener(this);
		btnEXIT.addActionListener(this);
		
		list.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				//int index = li.getSelectedIndex();
				
				Customer n = (Customer)list.getSelectedValue();
				nametf.setText(n.getName());
				phonetf.setText(n.getNum());
				addrtf.setText(n.getAddress());
			}
		});
	}
	
	public void showList() {
		list.removeAll();
		list.setListData(dao.allCust().toArray());
	}

	public static void main(String[] args) {
		ContactMain cm = new ContactMain();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println(e.getActionCommand());
		if(e.getSource()==btnINSERT) {
			String name = nametf.getText().trim();
			String phone = phonetf.getText().trim();
			String addr = addrtf.getText().trim();
			if(name.equals("") || phone.equals("") || addr.equals("")) {
				msgl.setText("비어있는 항목이 있습니다.");
				return;
			}
			try {
				dao.insert(name, phone, addr);
				clearText();
				showList();
				msgl.setText("등록: ok");
			} catch (ExistException e1) {
				msgl.setText("등록예외: "+ e1.getMessage());
			}
		}
		else if(e.getSource()==btnDELETE){
			String name = nametf.getText().trim();
			System.out.println("["+name+"]");
			if(name.equals("")) {
				msgl.setText("삭제: 먼저 이름을 입력하세요.");
				return;
			}
			// TODO
			System.out.println("삭제시작");
			try {
				System.out.println(dao.allCust());
				dao.delete(name);
				clearText();
				showList();
				msgl.setText("삭제: ok");
			} catch (NotFoundException e1) {
				msgl.setText("삭제예외: "+ e1.getMessage());
			}
		}else if(e.getSource()== btnUPDATE){
			String name = nametf.getText().trim();
			String phone = phonetf.getText().trim();
			String addr = addrtf.getText().trim();
			if(name.equals("") || phone.equals("")) {
				msgl.setText("비어있는 항목이 있습니다.");
				return;
			}
			try {
				dao.update(name, phone);
				clearText();
				showList();
				msgl.setText("변경: ok");
			} catch (NotFoundException e1) {
				msgl.setText("변경예외: "+ e1.getMessage());
			}
		}else if(e.getSource()== btnSEARCH){
			String name = nametf.getText().trim();
			if(name.equals("")) {
				msgl.setText("검색: 먼저 이름을 입력하세요.");
				return;
			}
			try {
				Customer c = dao.search(name);
				phonetf.setText(c.getNum());
				addrtf.setText(c.getAddress());
				msgl.setText("검색: ok");
			} catch (NotFoundException e1) {
				msgl.setText("검색예외: "+ e1.getMessage());
			}
		}else if(e.getSource()== btnUPLOAD){
			try {
				dao.upload();
				msgl.setText("업로드: ok");
			} catch (Exception e1) {
				msgl.setText("업로드예외: "+ e1.getMessage());
			}
		}else if(e.getSource()== btnCLEAR){
			clearText();
			
		}else if(e.getSource()== btnEXIT){
			try {
				dao.close();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			f.dispose();
		}
	}
	
	private void clearText() {
		nametf.getText();
		phonetf.getText();
		addrtf.getText();
		nametf.setText("");
		phonetf.setText("");
		addrtf.setText("");
		msgl.setText("clear ok");
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		// 1. 이렇게 한줄 받아서 자를수 있고
		//String s= list.getSelectedItem();
		//String[] sa = s.split(" ");
		//nametf.setText(sa[0]);
		//phonetf.setText(sa[1]);
		//addrtf.setText(sa[2]);
		
		
		// 2. 화면 리스트위치와 메모리 위치가 같으므로
		// 위치 값을 가져와서 뿌려주면 이렇게도 뿌릴 수 있다.
		// 이렇게 객체로 접근해서 가져올 수 있다. 
		// 거의 뭐 도른자다. 
		int i = Integer.parseInt(e.getItem().toString());
		System.out.println(dao.allCust().get(i));
		Customer c= dao.allCust().get(i);
		nametf.setText(c.getName());
		phonetf.setText(c.getNum());
		addrtf.setText(c.getAddress());
	}

	
}
