
import java.util.*;
import java.io.*;

class Main1809_탑_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/inputMain1809.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		for(int tc = 1; tc<=1; tc++) {
			// 빌딩 갯수
			int buildings = Integer.parseInt(br.readLine());
			// 빌딩 넣을 스택
			Stack<Integer> stack = new Stack<Integer>();
			Stack<Integer> counts = new Stack<Integer>();
			// 빌딩들 분리해서 입력
			String s[] = br.readLine().split(" ");
			int count[] = new int[s.length];
			int countIndex = 0;
			// 1번째 빌딩은 무조건 0일거니까
			count[0] = 0;
			
			stack.push(Integer.parseInt(s[0]));
			for(int i=1; i<s.length; i++) {
				// 이전거가 지금 들어갈 것보다 같거나 크면?
				if(stack.peek() >= Integer.parseInt(s[i])) {

					count[i] = i;
				}
				// peek이 더 작으면?
				else {
					int ii = i;
					// 크거나 같아질때까지, 스택이 빌때까지
					while(!stack.isEmpty()) {
						// 앞번째 있던 애의 카운트 가져옴
						ii--;
						if(stack.isEmpty() && stack.peek() < Integer.parseInt(s[i])) {
							count[i] = counts.peek();
							stack.pop();
							counts.pop();
						}
						else {
							break;
						}
						//앞에꺼 하나 빼고
					}
				}
				stack.push(Integer.parseInt(s[i]));
				counts.push(count[i]);
			}
			
			System.out.println("#"+tc+" "+Arrays.toString(count));
		}
		br.close();
	}
}