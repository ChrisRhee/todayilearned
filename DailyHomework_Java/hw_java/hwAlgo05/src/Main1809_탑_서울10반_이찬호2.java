
import java.util.*;
import java.io.*;

class Main1809_탑_서울10반_이찬호2{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/inputMain1809.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		for(int tc = 1; tc<=1; tc++) {
			// 빌딩 갯수
			int buildings = Integer.parseInt(br.readLine());
			// 빌딩 넣을 스택
			Stack<Integer> stack = new Stack<Integer>();
			Stack<Integer> countStack = new Stack<Integer>();
			// 빌딩들 분리해서 입력
			String s[] = br.readLine().split(" ");
			int count[] = new int[buildings];
			
			for(int i=0; i<buildings; i++) {
				
				if(i==0) {
					stack.push(Integer.parseInt(s[i]));
					countStack.push(i);
					count[i] = i;
				}
				
				if(stack.size()>0 && countStack.size()>0) {
					int num = stack.peek(); 
					int next = Integer.parseInt(s[i]);
					// 지금게 다음거보다 크면
					if(num >= next) {
						count[i] = i;
						countStack.push(count[i]);
						stack.push(next);
					}
					// 지금게 다음거보다 작으면?
					// 다음거를 땡겨와야함
					else {
						// 다음게 자기랑 같거나 클때까지 땡겨옴
						int ii = i;
						while(num < next ) {
							if(ii <= 0) break;
							ii--;
							
							count[i] = count[ii];
							//countStack.pop();
							//stack.pop();
						}
					}
				}
			}
			System.out.println("#"+tc+" "+Arrays.toString(count));
		}
		
		br.close();
	}
}