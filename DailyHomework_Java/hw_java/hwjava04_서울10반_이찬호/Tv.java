package ssafy.com.Tv;

public class Tv {
	
	private int number;  // 제품번호 - 변하지 않음
	private String name; // 제품이름 - 변하지 않음
	private int price;   // 가격 - 변함
	private int mount;   // 수량 - 변함
	private int inch;    // 인치 - 변하지 않음
	private String type; // 디스플레이타입 - 변하지 않음
	
	/**
	 * Constructor
	 * */
	public Tv() {
	
	}
	
	// 변하는 것들
	public Tv(int price, int mount) {
		this.price = price;
		this.mount = mount;
	}
	
	public Tv(int number, String name, int price, int mount, int inch, String type) {
		this.number = number;
		this.name = name;
		this.price = price;
		this.mount = mount;
		this.inch = inch;
		this.type = type;
	}
	
	/**
	 * Encapsulation - getter, setters
	 * */
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getMount() {
		return mount;
	}
	public void setMount(int mount) {
		this.mount = mount;
	}
	public int getInch() {
		return inch;
	}
	public void setInch(int inch) {
		this.inch = inch;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String toString() {
		String result = "";
		System.out.println("제품 번호 : "+number);
		System.out.println("제품 이름 : "+name);
		System.out.println("가격 정보 : "+price);
		System.out.println("재고 수량 : "+mount);
		System.out.println("인치      : "+inch);
		System.out.println("DP Type  : "+type);
		return result;
	}
	
}
