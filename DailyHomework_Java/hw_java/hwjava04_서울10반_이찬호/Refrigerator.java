package ssafy.com.Refrigerator;

public class Refrigerator {
	private int number;  // 제품번호 - 변하지 않음
	private String name; // 제품이름 - 변하지 않음
	private int price;   // 가격 - 변함
	private int mount;   // 수량 - 변함
	private int size;    // 사이즈 - 변하지 않음
	
	/**
	 * Constructor
	 * */
	public Refrigerator() {
		
	}
	public Refrigerator(int price, int mount) {
		this.price = price;
		this.mount = mount;
	}
	public Refrigerator(int number, String name, int price, int mount, int size) {
		this.number = number;
		this.name = name;
		this.price = price;
		this.mount = mount;
		this.size = size;
	}
	
	/**
	 * Encapsulation - getter, setters
	 * */
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getMount() {
		return mount;
	}
	public void setMount(int mount) {
		this.mount = mount;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	public String toString() {
		String result = "";
		System.out.println("제품 번호 : "+number);
		System.out.println("제품 이름 : "+name);
		System.out.println("가격 정보 : "+price);
		System.out.println("재고 수량 : "+mount);
		System.out.println("용량      : "+size);
		return result;
	}
}
