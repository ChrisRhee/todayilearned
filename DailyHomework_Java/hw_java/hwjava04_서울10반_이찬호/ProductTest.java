package com.ssafy.ProductTest;

import ssafy.com.Refrigerator.Refrigerator;
import ssafy.com.Tv.Tv;

public class ProductTest {
	public static void main(String[] args) {
		Tv t = new Tv();
		// 생성자에서 값을 초기화
		Tv t2 = new Tv(21948,"찬호티비",999000,3,56,"레티나");
		Refrigerator r = new Refrigerator();
		// 생성자에서 값을 초기화
		Refrigerator r2 = new Refrigerator(333,"찬호냉장고",50000,100,100);
		
		// 아무것도 초기화 되지 않은 t
		t.setNumber(99999);
		t.setName("SSAFY");
		t.setPrice(999999);
		t.setMount(9);
		t.setInch(99);
		t.setType("Samsung Display");
		
		// 아무것도 초기화 되지 않은 r
		r.setNumber(123);
		r.setName("수퍼냉장고");
		r.setPrice(900000000);
		r.setMount(2);
		r.setSize(140);
		
		// 출력
		System.out.println(t.toString());
		System.out.println(t2.toString());
		System.out.println(r.toString());
		System.out.println(r2.toString());
	}
}
