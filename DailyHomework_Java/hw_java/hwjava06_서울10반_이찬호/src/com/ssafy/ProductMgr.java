package com.ssafy;

public class ProductMgr {
	private Product products[] = new Product[100];
	private int index;
	
	//singleton
	private static ProductMgr intance = new ProductMgr();
	private ProductMgr() {}
	public static ProductMgr getIntance() {
		return intance;
	}
	
	public void add(Product p) {
		products[index++] = p;
	}
	
	public Product[] Search(){
		Product[] temp = new Product[index];
		for(int i =0; i<index; i++) {
			temp[i] = products[i];
		}
		return temp;
	}
	
	public Product SearchNum(int num){
		for(int i =0; i<index; i++) {
			Product p = products[i];
			if(p.getNum() == num) return p;
		}
		return null;
	}
	
	public Product SearchName(String name){
		for(int i =0; i<index; i++) {
			Product p = products[i];
			if(p.getName().equals(name)) return p;
		}
		return null;
	}
	
	public Product[] SearchPrice(int price){
		Product temp[] = new Product[index];
		int tempIndex=0;
		for(int i =0; i<index; i++) {
			if(products[i].getPrice()<price) {
				temp[tempIndex] = products[i];
				tempIndex++;
			}
		}
		Product result[] = new Product[tempIndex];
		for(int i =0; i<tempIndex; i++) {
			result[i] = temp[i];
		}
		return result;
	}
	
	public Product[] SearchMount(int mount){
		Product temp[] = new Product[index];
		int tempIndex=0;
		for(int i =0; i<index; i++) {
			if(products[i].getMount() > mount) {
				temp[tempIndex] = products[i];
				tempIndex++;
			}
		}
		Product result[] = new Product[tempIndex];
		for(int i =0; i<tempIndex; i++) {
			result[i] = temp[i];
		}
		return result;
	}
	
	public void update(int num, int price) {
		Product p = SearchNum(num);
		if(p!=null) p.setPrice(price);
	}
	
	public void delete(int num) {
		for(int i=0; i<index; i++) {
			if(products[i].getNum()==num) {
				for(int j=i;j<index-1;j++) {
					products[j] = products[j+1];
					products[index]= null;
				}
				index--;
			}
		}
	}
	
	public int size() {
		return index;
	}
	
	public int totalPrice() {
		int total = 0;
		for(int i=0;i<index;i++) {
			total+= products[i].getPrice()*products[i].getMount();
		}
		return total;
	}
	
}
