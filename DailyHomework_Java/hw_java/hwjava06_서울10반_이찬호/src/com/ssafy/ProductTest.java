package com.ssafy;

public class ProductTest {
	public static void main(String[] args) {
		ProductMgr pm = ProductMgr.getIntance();
		pm.add(new Product(101,"이찬호의자",14000,10));
		pm.add(new Product(102,"이찬호책상",24000,60));
		pm.add(new Product(103,"이찬호가방",34000,150));
		pm.add(new Product(104,"이찬호바지",44000,200));
		pm.add(new Product(105,"이찬호셔츠",54000,400));
		pm.add(new Product(106,"이찬호티비",114000,20));
		pm.add(new Product(107,"고구마",100,2000));
		pm.add(new Product(108,"감자",1500,350));
		pm.add(new Product(109,"만두",2000,100));
		pm.add(new Product(110,"개복치",1000,250));
		
		System.out.println(" - 전체 검색");
		Product temp[] = pm.Search();
		for(Product p:temp) if(p!=null) System.out.println(p);
		
		System.out.println("\n - 상품번호로 상품 검색 - 108번");
		System.out.println(pm.SearchNum(108));
		
		System.out.println("\n - 상품명으로 상품 검색 - 개복치");
		System.out.println(pm.SearchName("개복치"));
		
		System.out.println("\n - 상품 가격 변경 - 104번 가격 15000로 변경");
		System.out.println(" - 변경 전");
		temp = pm.Search();
		for(Product p:temp) if(p!=null) System.out.println(p);
		System.out.println(" - 변경 후");
		pm.update(104, 15000);
		temp = pm.Search();
		for(Product p:temp) if(p!=null) System.out.println(p);
		
		System.out.println("\n - 상품 삭제 - 102번,105번 삭제");
		System.out.println(" - 삭제 전");
		temp = pm.Search();
		for(Product p:temp) if(p!=null) System.out.println(p);
		System.out.println(" - 삭제 후");
		pm.delete(102);
		pm.delete(105);
		temp = pm.Search();
		for(Product p:temp) if(p!=null) System.out.println(p);
		
		System.out.println("\n - 전체 재고 량, 재고 상품 금액");
		System.out.println("상품 종류 갯수 : "+pm.size()+"개");
		System.out.println("전체 재고 상품 금액 : "+pm.totalPrice()+"원");
		
		System.out.println("\n - 추가아이디어");
		System.out.println(" - 가격으로 검색(x원보다 저렴한 모든 상품조회) - 20000원");
		Product temp2[] = pm.SearchPrice(20000);
		for(Product p:temp2) if(p!=null) System.out.println(p);
		
		System.out.println("\n - 재고로 검색(x개 이상 있는 모든 상품조회) - 200개");
		Product temp3[] = pm.SearchMount(200);
		for(Product p:temp3) if(p!=null) System.out.println(p);
	}
}
