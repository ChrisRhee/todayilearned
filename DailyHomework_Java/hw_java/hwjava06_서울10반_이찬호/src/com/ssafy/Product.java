package com.ssafy;

public class Product {
	private int num ;
	private String name;
	private int price;
	private int mount;
	
	
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getMount() {
		return mount;
	}
	public void setMount(int mount) {
		this.mount = mount;
	}
	
	public Product(int num, String name, int price, int mount) {
		setNum(num);
		setName(name);
		setPrice(price);
		setMount(mount);
	}
	public Product() {
		this(0, "", 0, 0);
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [num=");
		builder.append(num);
		builder.append(", name=");
		builder.append(name);
		builder.append(", price=");
		builder.append(price);
		builder.append(", mount=");
		builder.append(mount);
		builder.append("]");
		return builder.toString();
	}
	
	
}
