package com.ssafy;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

public interface IProductMgr {
	
	/** 상품정보(Tv와 냉장고)를 객체 배열을 활용하여 저장
	 * 입력 : Product 객체
	 * 출력 : x
	 * @throws DuplicateException 
	 * */
	boolean add(Product product) throws DuplicateException;
	
	/** 상품정보 전체를 검색하는 기능
	 * 입력 : x
	 * 출력 : Product Array
	 * */
	Collection<Product> search();
	
	/** 상품번호로 상품을 검색하는 기능
	 * 입력 : int num (상품번호)
	 * 출력 : Product (맞는 상품)
	 * @throws CodeNotFoundExeption 
	 * */
	Product search(int num) throws CodeNotFoundExeption;

	Collection<Refrigerator> searchByLiter(int liter) throws ProductNotFoundException;

	Collection<Tv> searchByInch(int Inch) throws ProductNotFoundException;
	
	// 클래스 생성시 호출, 파일("book.dat)"에 저장된 파일을 읽어서 ArrayList에 저장
	void Open() throws Exception;
	
	// 프로그램 종료시 호출, ArrayList에 있는 도서 정보를 파일로 저장.
	void Close() throws Exception;
	
}
