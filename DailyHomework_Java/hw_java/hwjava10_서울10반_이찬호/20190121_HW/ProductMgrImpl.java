package com.ssafy;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;

public class ProductMgrImpl implements IProductMgr {
	private Collection<Product> pCol ;
	
	// Singleton
	private static ProductMgrImpl instance = null;
	
	private ProductMgrImpl() {
	}
	
	private ProductMgrImpl(Collection<Product> products) {
		pCol = products;
	}
	
	public static ProductMgrImpl getInstance() {
		if(instance == null)
			instance = new ProductMgrImpl(new ArrayList());
		return instance;
	}

	
	@Override
	public boolean add(Product product) throws DuplicateException {
		for (Product p : pCol) {
			if(p.equals(product)) {
				throw new DuplicateException(p.getName());
			}
		}
		return pCol.add(product);
	}

	@Override
	public Collection<Product> search() {
		Collection<Product> tmp = new ArrayList<Product>();
		for (Product product : pCol) {
			tmp.add(product);
		}
		return tmp;
	}

	// 제품 번호로 검색
	@Override
	public Product search(int num) throws CodeNotFoundExeption {
		boolean check = false;
		for (Product product : pCol) {
			if(product.getNum() == num) {
				check = true;
				return product;
			}
		}
		if(check == false) throw new CodeNotFoundExeption(""+num);
		return null;
	}

	@Override
	public Collection<Refrigerator> searchByLiter(int liter) throws ProductNotFoundException {
		boolean check =false;
		Collection<Refrigerator> tmp = new ArrayList<Refrigerator>();
		for (Product product : pCol) {
			if(product instanceof Refrigerator && ((Refrigerator) product).getLiter() >= liter) {
				tmp.add((Refrigerator)product);
				check = true;
			}
		}
		if(check == false) throw new ProductNotFoundException(""+liter);
		return tmp;
	}

	@Override
	public Collection<Tv> searchByInch(int Inch) throws ProductNotFoundException {
		boolean check =false;
		Collection<Tv> tmp = new ArrayList<Tv>();
		for (Product product : pCol) {
			if(product instanceof Tv && ((Tv) product).getInch() >= Inch) {
				tmp.add((Tv)product);
				check = true;
			}
		}
		if(check == false) throw new ProductNotFoundException(""+Inch);
		return tmp;
	}

	@Override
	public void Open() throws Exception {
		ObjectInputStream ibj = new ObjectInputStream(new FileInputStream("product.dat"));
		Collection<Product> tmp = new ArrayList();
		while(ibj.available() >0) {
			Product temp = (Product)ibj.readObject();
			tmp.add(temp);
		}
		pCol = tmp;
	}

	@Override
	public void Close() throws Exception {
		ObjectOutputStream obj = new ObjectOutputStream(new FileOutputStream("product.dat"));
		obj.writeObject(pCol);
		obj.flush();
		obj.close();
	}

}
