package com.ssafy;

import java.util.Scanner;

public class ProductTest {
	static IProductMgr mgr = ProductMgrImpl.getInstance() ;
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args)  {
		
		// 상품정보 객체 배열을 활용하여 저장
		try {
			mgr.add(new Tv(101,"삼성 UHD TV", 140000, 20,50));
			mgr.add(new Tv(102,"이찬호 TV", 10000, 100,50));
			mgr.add(new Refrigerator(201,"찬호 냉장고", 1200000, 20,300));
			mgr.add(new Refrigerator(202,"달별 냉장고", 1300000, 40,450));
			mgr.add(new Tv(103,"SSAFY TV", 240000, 10,50));
			mgr.add(new Tv(104,"Star TV", 500000, 40,40));
			mgr.add(new Refrigerator(203,"개복치 냉장고", 1400000, 30,499));
			mgr.add(new Refrigerator(204,"행복 냉장고", 1500000, 50,450));
			mgr.add(new Refrigerator(205,"냉장고", 1500000, 20,400));
		}
		catch(DuplicateException e) {
			e.printStackTrace();
		}
		
		System.out.println("\n--- 2. 상품번호로 검색하는 기능 | 상품번호 : 103");
		try {
			System.out.println(mgr.search(103));
		} catch (CodeNotFoundExeption e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		System.out.println("\n--- 3. 400L이상 Refrigerator정보만 검색");
		try {
			for (Product product : mgr.searchByLiter(400)) {
				System.out.println(product);
			}
		} catch (ProductNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\n--- 4. 50inch이상 TV정보만 검색");
		try {
			for (Product product : mgr.searchByInch(50)) {
				System.out.println(product);
			}
		} catch (ProductNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/** 6. 파일에서 값 읽어오기 */
		try {
			mgr.Open();
		}  catch (Exception e) {
			e.printStackTrace();
		}
		
		/** 7. 파일에서 값 저장하기 */
		try {
			mgr.Close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		sc.close();
	}
}