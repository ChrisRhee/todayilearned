package com.ssafy;

public class CodeNotFoundExeption extends Exception {
	public CodeNotFoundExeption(String message) {
		super(message);
	}
}
