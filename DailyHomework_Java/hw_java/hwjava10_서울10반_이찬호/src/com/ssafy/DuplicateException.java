package com.ssafy;

public class DuplicateException extends Exception {
	public DuplicateException(String message) {
		super(message);
	}
}
