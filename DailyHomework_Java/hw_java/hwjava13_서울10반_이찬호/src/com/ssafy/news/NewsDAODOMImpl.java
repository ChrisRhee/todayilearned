package com.ssafy.news;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class NewsDAODOMImpl implements INewsDAO{
	List<News> list;

	@Override
	public List<News> getNewsList(String url) throws ParserConfigurationException, SAXException, IOException {
		connectNews(url);
		return list;
	}

	@Override
	public News search(int index) {
		return null;
	}
	
	private void connectNews(String url) throws SAXException, IOException, ParserConfigurationException {
		// channel
		list = new ArrayList<>();
		Document d = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(url);
		Element root = d.getDocumentElement();
		
		NodeList items = root.getElementsByTagName("item");
		for(int i=0; i<items.getLength(); i++) {
			News wd = new News();
			Node t = items.item(i);
			NodeList news = t.getChildNodes();
			for (int j = 0; j < news.getLength(); j++) {
				Node temp = news.item(j);
				if(temp.getNodeType()==Node.ELEMENT_NODE) {
					switch(temp.getNodeName()) {
						case "title":
							wd.setTitle(temp.getFirstChild().getNodeValue());
							break;
						case "url":
							wd.setLink(temp.getFirstChild().getNodeValue());
							break;
						case "description":
							wd.setDesc(temp.getFirstChild().getNodeValue());
							break;
					}
				}
			}
			list.add(wd);
			//System.out.println(list);
		}
	}
}
