package com.ssafy.news;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public interface INewsDAO {
	
	List<News> getNewsList(String url) throws ParserConfigurationException, SAXException, IOException;
	
	public News search(int index);
}
