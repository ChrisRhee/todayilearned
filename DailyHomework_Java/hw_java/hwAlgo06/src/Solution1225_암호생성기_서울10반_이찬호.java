import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

class Solution1225_암호생성기_서울10반_이찬호{
	public static void main(String args[]) throws Exception	{
		System.setIn(new FileInputStream("res/input1225.txt"));
		//Scanner sc = new Scanner(System.in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		for(int test_case = 1; test_case <= 10; test_case++)
		{
			int T= Integer.parseInt(br.readLine());
			int Answer =0;
			/** 이 부분에 여러분의 알고리즘 구현이 들어갑니다. */
			Queue<Integer> q = new LinkedList();
			String nums[] = br.readLine().split(" ");
			for(int i =0; i<8; i++) {
				q.offer(Integer.parseInt(nums[i]));
			}
			for(int i = 0;  ; i++) {
				Answer = q.poll() - (i%5+1);
				if(Answer <= 0 ) {
					Answer =0;
					q.offer(Answer);
					break;
				}
				q.offer(Answer);
			}
			String a ="";
			while(!q.isEmpty()) {
				a += q.poll()+" ";
			}
			System.out.println("#"+test_case + " "+a);
		}
		br.close();
	}
}