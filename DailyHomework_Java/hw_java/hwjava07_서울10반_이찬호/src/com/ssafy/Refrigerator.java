package com.ssafy;

public class Refrigerator extends Product{
	private int liter;

	public Refrigerator(int num, String name, int price, int mount, int liter) {
		super(num, name, price, mount);
		setLiter(liter);
	}
	
	public Refrigerator() {
		this(0, "", 0, 0, 0);
	}

	public int getLiter() {
		return liter;
	}

	public void setLiter(int liter) {
		this.liter = liter;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.toString());
		builder.append(" | ");
		builder.append(getLiter());
		builder.append("L");
		return builder.toString();
	}
	
}
