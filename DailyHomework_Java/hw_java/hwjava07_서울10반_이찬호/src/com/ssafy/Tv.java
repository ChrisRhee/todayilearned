package com.ssafy;

public class Tv extends Product{
	private int inch;
	
	public Tv() {
		this(0, "", 0, 0, 0);
	}

	public Tv(int num, String name, int price, int mount, int inch) {
		super(num, name, price, mount);
		setInch(inch);
	}

	public int getInch() {
		return inch;
	}

	public void setInch(int inch) {
		this.inch = inch;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.toString());
		builder.append(" | ");
		builder.append(getInch());
		builder.append("inch");
		return builder.toString();
	}
	
	
}
