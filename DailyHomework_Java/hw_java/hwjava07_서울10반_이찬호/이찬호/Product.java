package com.ssafy;

public class Product {
	/** 멤버 변수
	 * 제품번호, 제품명, 가격 정보, 재고수량
	 * */
	private int num;
	private String name;
	private int price;
	private int mount;
	
	
	/** Constructors */
	public Product() {
		this(0, "", 0, 0);
	}

	public Product(int num, String name, int price, int mount) {
		setNum(num);
		setName(name);
		setPrice(price);
		setMount(mount);
	}
	
	
	/** Getters, Setters */
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getMount() {
		return mount;
	}
	public void setMount(int mount) {
		this.mount = mount;
	}
	
	public String comma(int num) {
		StringBuilder sb = new StringBuilder();
		int temp = num/1000;
		if(num>1000) {
			sb.append(num/1000).append(",").append(num%1000);
		}
		else {
			sb.append(num);
		}
		return sb.toString();
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(num);
		builder.append(" | ");
		builder.append(name);
		builder.append(" | ");
		builder.append(comma(price));
		builder.append("원 | ");
		builder.append(mount);
		builder.append("개");
		return builder.toString();
	}
	
}
