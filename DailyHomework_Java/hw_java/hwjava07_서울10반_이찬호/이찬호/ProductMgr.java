package com.ssafy;

public class ProductMgr {
	private Product pArray[] = null;
	private int index ;
	
	// Singleton
	private static ProductMgr instance = null;
	private ProductMgr () {
		pArray = new Product[100];
	}
	public static ProductMgr getInstance() {
		if(instance == null) {
			instance = new ProductMgr();
		}
		return instance;
	}
	
	
	/** 상품정보(Tv와 냉장고)를 객체 배열을 활용하여 저장
	 * 입력 : Product 객체
	 * 출력 : x
	 * */
	public void add(Product product) {
		pArray[index++]= product;
	}
	
	/** 상품정보 전체를 검색하는 기능
	 * 입력 : x
	 * 출력 : Product Array
	 * */
	public Product[] search() {
		Product temp[] = new Product[index];
		for (int i = 0; i < temp.length; i++) {
			if(pArray[i] != null) temp[i] = pArray[i];
		}
		return temp;
	}
	
	/** 상품번호로 상품을 검색하는 기능
	 * 입력 : int num (상품번호)
	 * 출력 : Product (맞는 상품)
	 * */
	public Product search(int num) {
		for (Product product : pArray) {
			if(product != null && product.getNum() == num)
				return product;
		}
		return null;
	}
	
	/** 상품명으로 상품을 검색하는 기능(상품명 부분 검색 가능)
	 * 입력 : String name
	 * 출력 : Product (맞는 상품)
	 * */
	public Product search(String name) {
		for (Product product : pArray) {
			if(product != null && product.getName().equals(name))
				return product;
		}
		return null;
	}
	
	/** Tv정보만 검색
	 * 입력 : x
	 * 출력 : Tv만 담은 Product 배열
	 * */
	public Tv[] searchTv() {
		Tv temp[] = new Tv[index];
		int tempIndex = 0;
		for (int i = 0; i < temp.length; i++) {
			if(pArray[i]!=null && pArray[i] instanceof Tv){
				temp[tempIndex] = (Tv)pArray[i];
				tempIndex++;
			}
		}
		
		return temp;
	}
	
	/** Refrigerator정보만 검색
	 * 입력 : x
	 * 출력 : Refrigerator만 담은 Product 배열
	 * */
	public Refrigerator[] searchRefrigerator() {
		Refrigerator temp[] = new Refrigerator[index];
		int tempIndex = 0;
		for (int i = 0; i < temp.length; i++) {
			if(pArray[i]!=null && pArray[i] instanceof Refrigerator){
				temp[tempIndex] = (Refrigerator)pArray[i];
				tempIndex++;
			}
		}
		
		Refrigerator result[] = new Refrigerator[tempIndex];
		for (int i = 0; i < result.length; i++) {
			result[i] = temp[i];
		}
		return result;
	}
	
	/** 400L이상의 Refrigerator정보만 검색
	 * 입력 : x
	 * 출력 : Refrigerator만 담은 Product 배열
	 * */
	public Refrigerator[] searchOver400() {
		Refrigerator temp[] = new Refrigerator[index];
		int tempIndex = 0;
		for (int i = 0; i < temp.length; i++) {
			if(pArray[i]!=null && pArray[i] instanceof Refrigerator) {
				if(((Refrigerator)pArray[i]).getLiter() >= 400){
					temp[tempIndex] = (Refrigerator)pArray[i];
					tempIndex++;
				}
			}
		}
		
		return temp;
	}
	
	
	/** 50L이상의 Tv정보만 검색
	 * 입력 : x
	 * 출력 : 50L이상 Tv만 담은 Product 배열
	 * */
	public Tv[] searchOver50() {
		Tv temp[] = new Tv[index];
		int tempIndex = 0;
		for (int i = 0; i < temp.length; i++) {
			if(pArray[i]!=null && pArray[i] instanceof Tv) {
				if(((Tv)pArray[i]).getInch() >= 50){
					temp[tempIndex] = (Tv)pArray[i];
					tempIndex++;
				}
			}
		}
		
		return temp;
	}
	
	/** 상품번호와 가격을 입력받아 상품 가격을 변경할 수 있는 기능
	 * 입력 : int num, int price
	 * 출력 : x
	 * */
	public void update(int num, int price) {
		search(num).setPrice(price);
	}
	
	/** 상품번호로 상품을 삭제하는 기능
	 * 입력 : int num
	 * 출력 : x
	 * */
	public void delete(int num) {
		for (int i = 0; i < index-1; i++) {
			if(pArray[i]!= null && pArray[i].getNum() == num) {
				for (int j = i; j < index-1; j++) {
					pArray[j] = pArray[j+1];
					
				}
				index--;
			}
		}
	}
	
	/** 전체 재고 상품 금액 구하는 기능
	 * 입력 : x
	 * 출력 : int total (전체 금액)
	 * */
	public int totalPrice() {
		int total = 0;
		for (Product product : pArray) {
			if(product != null) total += product.getPrice();
		}
		return total;
	}
}
