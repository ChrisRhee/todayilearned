package com.ssafy.Refrigerator;
//제품들의 정보를 저장하고 출력하는 프로그램
// 저장 - 새로 값을 주고 가져올 수 잇어야한다.
// 출력 - 출력 열심히 하자
public class Refrigerator {
	private int number;
	private String name;
	private int price;
	private int mount;
	private int size;
	
	/**
	 * Encapsulation - getter, setters
	 * */
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getMount() {	
		return mount;
	}
	public void setMount(int mount) {
		this.mount = mount;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	/**
	 * Constructor
	 * */
	
	public Refrigerator(int number, String name, int price, int mount, int size) {
		setNumber(number);
		setName(name);
		setPrice(price);
		setMount(mount);
		setSize(size);
	}
	public Refrigerator() {
		
	}
	
	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append("제품번호 : ");
		str.append(number);
		str.append(", 제품이름 : ");
		str.append(name);
		str.append(", 가격정보 : ");
		str.append(price);
		str.append(", 재고수량 : ");
		str.append(mount);
		str.append(", 용량 : ");
		str.append(size);
		return str.toString();
	}
}
