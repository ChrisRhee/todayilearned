package com.ssafy.ProductTest;

import com.ssafy.Refrigerator.Refrigerator;
import com.ssafy.Tv.Tv;

public class ProductTest {
	public static void main(String[] args) {
		Tv t = new Tv(21948,"찬호티비",999000,3,56,"레티나");
		Tv t2 = new Tv();
		// 객체 배열에 추가
		Tv tvs[] = new Tv[]{t,t2};
		
		Refrigerator r = new Refrigerator(333,"찬호냉장고",50000,100,100);
		Refrigerator r2 = new Refrigerator();
		// 객체 배열에 추가
		Refrigerator refrigerators[] = new Refrigerator[]{r,r2};
		
		// 아무것도 초기화 되지 않은 t2
		tvs[1].setNumber(99999);
		tvs[1].setName("SSAFY");
		tvs[1].setPrice(1000000);
		tvs[1].setMount(9);
		tvs[1].setInch(99);
		tvs[1].setType("Samsung Display");
		
		// 아무것도 초기화 되지 않은 r2
		refrigerators[1].setNumber(123);
		refrigerators[1].setName("수퍼냉장고");
		refrigerators[1].setPrice(900000000);
		refrigerators[1].setMount(2);
		refrigerators[1].setSize(140);
		
		// 배열에 담긴 것들 출력
		System.out.println("******************** TV ********************");
		for(int i = 0; i < tvs.length; i++) {
			System.out.println(tvs[i].toString());
		}
		System.out.println();
		
		System.out.println("*************** Refrigerator ***************");
		for(int i = 0; i < tvs.length; i++) {
			System.out.println(refrigerators[i].toString());
		}

	}
}