package com.ssafy.Tv;

public class Tv {
	
	private int number;  // 제품번호 - 변하지 않음
	private String name; // 제품이름 - 변하지 않음
	private int price;   // 가격 - 변함
	private int mount;   // 수량 - 변함
	private int inch;    // 인치 - 변하지 않음
	private String type; // 디스플레이타입 - 변하지 않음
	
	/**
	 * Encapsulation - getter, setters
	 * */
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getMount() {	
		return mount;
	}
	public void setMount(int mount) {
		this.mount = mount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getInch() {
		return inch;
	}
	public void setInch(int inch) {
		this.inch = inch;
	}
	
	/**
	 * Constructor
	 * */
	public Tv(int number, String name, int price, int mount, int inch, String type) {
		setNumber(number);
		setName(name);
		setPrice(price);
		setMount(mount);
		setInch(inch);
		setType(type);
	}
	public Tv() {
		
	}
	
	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append("제품번호 : ");
		str.append(number);
		str.append(", 제품이름 : ");
		str.append(name);
		str.append(", 가격정보 : ");
		str.append(price);
		str.append(", 재고수량 : ");
		str.append(mount);
		str.append(", 인치 : ");
		str.append(inch);
		str.append(", 화면타입 : ");
		str.append(type);
		return str.toString();
	}
	
}
