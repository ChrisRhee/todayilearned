<%@page import="com.ssafy.servlet.Product"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("utf-8"); 
Product p = (Product) request.getAttribute("product"); %>
<h1>상품이 저장되었습니다.</h1>
<table class = "resultTable">
	<tr>
		<td class="result-label">상품번호</td>
		<td><%= p.getProNum() %></td>
	</tr>
	<tr>
		<td class="result-label">상품명</td>
		<td><%= p.getName() %></td>
	</tr>
	<tr>
		<td class="result-label">상품가격</td>
		<td><%= p.getPrice() %></td>
	</tr>
	<tr>
		<td class="result-label">상품설명</td>
		<td><%= p.getDesc() %></td>
	</tr>
</table>

<a href = "">상품 목록</a>