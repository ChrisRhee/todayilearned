package com.ssafy.servlet;

public class Product {
	private String proNum;
	private String name;
	private String price;
	private String desc;
	
	public Product(String proNum, String name, String price, String desc) {
		super();
		setProNum(proNum);
		setName(name);
		setPrice(price);
		setDesc(desc);
	}

	public Product() {
		this("","", "", "");
	}

	public String getProNum() {
		return proNum;
	}

	public void setProNum(String proNum) {
		this.proNum = proNum;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
