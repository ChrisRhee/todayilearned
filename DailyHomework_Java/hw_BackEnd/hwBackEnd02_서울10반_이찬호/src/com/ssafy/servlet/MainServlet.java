package com.ssafy.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet("/controller")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url= "index.html";
		//1. 요청받은 데이터 부분
		String cmd = request.getParameter("cmd");
		System.out.println("cmd:"+cmd);
		
		//2. JavaBean을 통해 처리
		switch(cmd) {
		case "productUI":
			url = "product.html";
			break;
		case "productAdd":
			url = productAdd(request);
			break;
		}
		
		//3. JSP 페이지로 이동
		if(url.charAt(0) == '/') {
			request.getRequestDispatcher(url).forward(request, response);
		}else {
			response.sendRedirect(url);
		}
	}
	
	private String productAdd(HttpServletRequest request) {
		Product p = new Product();
		p.setProNum("1");
		p.setName(request.getParameter("proName"));
		p.setPrice(request.getParameter("proPrice"));
		p.setDesc(request.getParameter("proDes"));
		
		request.setAttribute("product", p);
		return "/result.jsp";
	}
}
