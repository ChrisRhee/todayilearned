package com.ssafy.mainServlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/product")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String proname = request.getParameter("proname");
		String proprice = request.getParameter("proprice");
		String procontent = request.getParameter("procontent");
		response.setCharacterEncoding("euc-kr");
		PrintWriter out = response.getWriter();
		out.print("<h1>상품정보</h1>");
		out.print("<span>상품명 : </span><span>"+proname+"</span><br>");
		out.print("<span>상품가격 : </span><span>"+proprice+"</span><br>");
		out.print("<span>상품설명가격 : </span><span>"+procontent+"</span><br>");
		out.flush();
		out.close();
	}
}
