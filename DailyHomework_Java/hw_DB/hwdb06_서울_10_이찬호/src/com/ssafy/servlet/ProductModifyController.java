package com.ssafy.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.Product;
import com.ssafy.ProductService;
import com.ssafy.ProductServiceImpl;

public class ProductModifyController implements Controller {
	ProductService ps = new ProductServiceImpl();
	
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = request.getParameter("code");
		String name = request.getParameter("name");
		String price = request.getParameter("price");
		List<Product> products;
		try {
			ps.updateProduct(new Product(code, name, Integer.parseInt(price)));
			products = ps.listProducts();
			request.setAttribute("products",products);
			request.getRequestDispatcher("/ProductList.jsp").forward(request, response);
		} catch (Exception e) {
			request.setAttribute("msg", e.getMessage());
			request.getRequestDispatcher("/Error.jsp").forward(request, response);
		}

	}

}
