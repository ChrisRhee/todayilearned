package com.ssafy.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.Product;
import com.ssafy.ProductService;
import com.ssafy.ProductServiceImpl;

public class ProductViewActionController implements Controller {
	ProductService ps = new ProductServiceImpl();
	
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = request.getParameter("code");
		
		try {
			Product product = ps.findProduct(code);
			request.setAttribute("product", product);
			request.getRequestDispatcher("/ProductView.jsp").forward(request, response);
		} catch (Exception e) {
			request.setAttribute("msg", e.getMessage());
			request.getRequestDispatcher("/Error.jsp").forward(request, response);
		}
	}

}
