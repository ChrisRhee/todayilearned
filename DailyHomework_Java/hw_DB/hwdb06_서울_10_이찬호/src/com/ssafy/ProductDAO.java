package com.ssafy;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public interface ProductDAO {
	public void insertProduct(Product product) throws SQLException;
	public void updateProduct(Product product) throws SQLException;
	public void deleteProduct(String code) throws SQLException ;
	public Product findProduct(String code) throws SQLException ;
	public List<Product> listProducts() throws SQLException ;
	public int count() throws SQLException;
	public void close(Statement st);
	public void close(Connection con) ;
	public void close(ResultSet rs);
	public Connection getConnection() throws SQLException;
}
