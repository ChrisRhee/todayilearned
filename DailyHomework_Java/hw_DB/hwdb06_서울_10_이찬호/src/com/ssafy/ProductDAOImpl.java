package com.ssafy;

import java.sql.*;
import java.util.*;

import javax.naming.InitialContext;
import javax.sql.DataSource;

public class ProductDAOImpl implements ProductDAO {
	private static DataSource ds = null;
	static {
		try {
			InitialContext ctx = new InitialContext();
			ds =(DataSource) ctx.lookup("java:comp/env/jdbc/ssafydb");
			System.out.println("LookUp OK");
		}catch(Exception e) {
			System.out.println("LookUp Error");
			e.printStackTrace();
		}
	}
	
	@Override
	public void close(Statement st) {
		try {
			if(st!=null) st.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void close(Connection con) {
		try {
			if(con!= null) con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void close(ResultSet rs) {
		try {
			if(rs!= null) rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int count() throws SQLException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		int cnt = 0;
		try {
			con= getConnection();
			String sql = "select count(*) from product";
			st= con.prepareStatement(sql);
			rs=st.executeQuery(sql);
			while(rs.next()) {
				cnt=rs.getInt(1);
			}
		}finally {
			close(rs);
			close(st);
			close(con);
		}
		return cnt;
	}
	
	@Override
	public Connection getConnection() throws SQLException {
		return ds.getConnection();
	}
	
	@Override
	public void insertProduct(Product product) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = getConnection();
			String sql = "Insert into product values (?, ?, ?)";
			st = con.prepareStatement(sql);
			st.setString(1, product.getCode());
			st.setString(2, product.getName());
			st.setInt(3, product.getPrice());
			int cnt = st.executeUpdate();
			System.out.println("insert : " + cnt);
		}finally {
			close(st);
			close(con);
		}
	}

	@Override
	public void updateProduct(Product product) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = getConnection();
			String sql = "update product set code=?, name=?, price=? where code = ?";
			st = con.prepareStatement(sql);
			st.setString(1, product.getCode());
			st.setString(2, product.getName());
			st.setInt(3, product.getPrice());
			st.setString(4, product.getCode());
			
			int cnt = st.executeUpdate();
			System.out.println("Update : " + cnt);
		} finally {
			close(st);
			close(con);
		}
	}

	@Override
	public void deleteProduct(String code) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = getConnection();
			String sql = "delete from product where code = ?";
			st = con.prepareStatement(sql);
			st.setString(1, code);
			
			int cnt = st.executeUpdate();
			System.out.println("Delete : " + cnt);
		}finally {
			close(st);
			close(con);
		}
	}

	@Override
	public Product findProduct(String code) throws SQLException {
		Product temp = null;
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			String sql = "select * from product where code='"+code+"'";
			st = con.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next()) {
				temp = new Product(rs.getString(1),rs.getString(2),rs.getInt(3));
			}
		} finally {
			close(rs);
			close(st);
			close(con);
		}
		
		return temp;
	}

	@Override
	public List<Product> listProducts() throws SQLException {
		List<Product> temp = new ArrayList<>();
		Product b = null;
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			st = con.createStatement();
			String sql = "select * from product";
			rs = st.executeQuery(sql);
			while(rs.next()) {
				b = new Product(rs.getString(1),rs.getString(2),rs.getInt(3));
				temp.add(b);
			}
		}finally {
			close(rs);
			close(st);
			close(con);
		}
		return temp;
	}

	

}
