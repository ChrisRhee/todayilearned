<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>상품 입력</title>
</head>
<body>
	<h1>상품 입력</h1>
	<form method="post" action="ProductInputAction.do">
		<div>
			상품 번호 <input type="text" name="code">
		</div>
		<div>
			상품 이름 <input type="text" name="name">
		</div>
		<div>
			상품 가격 <input type="number" min='0' name="price">
		</div>
		<div>
			<input type="submit" value="입력">
			<input type="reset" value="취소">
			<input type="button" value="목록" onclick="Plist()">
			<script type="text/javascript">
				function Plist(){
					location.href = "ProductList.do";
				}	
			</script>
		</div>
	</form>
</body>
</html>