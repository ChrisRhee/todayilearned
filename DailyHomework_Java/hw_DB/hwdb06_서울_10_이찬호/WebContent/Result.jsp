<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<% String msg = (String) request.getAttribute("msg"); %>
<h1>결과 페이지</h1>
<h4>정상적으로 <%=msg%> 되었습니다</h4>
<div>
	<a href="ProductInputUI.do">상품 추가 입력</a>
	<a href="ProductList.do">상품 목록</a>
</div>