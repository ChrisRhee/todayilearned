<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.ssafy.*"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>상품 목록</title>
</head>
<body>
<h1>상품 목록</h1>
	<c:choose>
	<c:when test ="${empty products}">
	상품 정보 없음 <br>
	</c:when>
	<c:otherwise>
		<c:forEach var="p" items="${products}">
			${p.code}&nbsp;<a href="ProductView.do?code=${p.code}">${p.name}</a>&nbsp;${p.price}<br>
		</c:forEach>
	</c:otherwise>
	</c:choose>
	
	<div>
	<form method="post" action="ProductInputUI.do">
		<input type="submit" value="상품 추가 하기">
	</form>
	
	</div>
	
</body>
</html>