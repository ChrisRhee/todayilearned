package com.ssafy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProductDAO {
	public static String url = "jdbc:mysql://localhost:3306/ssafydb?characterEncoding=UTF-8&serverTimezone=UTC";
	public static String user = "ssafy";
	public static String password = "ssafy";
	
	static {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void close(Statement st) {
		try {
			if(st!=null) st.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void close(Connection con) {
		try {
			if(con!= null) con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	private void close(ResultSet rs) {
		try {
			if(rs!= null) rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int count() {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		int cnt = 0;
		try {
			con= getConnection();
			String sql = "select count(*) from product";
			st= con.prepareStatement(sql);
			rs=st.executeQuery(sql);
			while(rs.next()) {
				cnt=rs.getInt(1);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rs);
			close(st);
			close(con);
		}
		return cnt;
	}
	
	public Connection getConnection() throws SQLException{
		return DriverManager.getConnection(url, user, password);
	}
	
	public void insertProduct(Product product) throws ClassNotFoundException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = getConnection();
			String sql = "Insert into product values (?, ?, ?)";
			st = con.prepareStatement(sql);
			st.setString(1, product.getCode());
			st.setString(2, product.getName());
			st.setInt(3, product.getPrice());
			int cnt = st.executeUpdate();
			System.out.println("insert : " + cnt);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(st);
			close(con);
		}
	}
	public void updateProduct(Product product) throws ClassNotFoundException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = getConnection();
			String sql = "update product set code=?, name=?, price=? where code = ?";
			st = con.prepareStatement(sql);
			st.setString(1, product.getCode());
			st.setString(2, product.getName());
			st.setInt(3, product.getPrice());
			st.setString(4, product.getCode());
			
			int cnt = st.executeUpdate();
			System.out.println("Update : " + cnt);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(st);
			close(con);
		}
	}
	public void deleteProduct(String code) throws ClassNotFoundException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = getConnection();
			String sql = "delete from product where code = ?";
			st = con.prepareStatement(sql);
			st.setString(1, code);
			
			int cnt = st.executeUpdate();
			System.out.println("Delete : " + cnt);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(st);
			close(con);
		}
	}
	
	public Product findProduct(String code) throws ClassNotFoundException {
		Product temp = null;
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			String sql = "select * from product where code='"+code+"'";
			st = con.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next()) {
				temp = new Product(rs.getString(1),rs.getString(2),rs.getInt(3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rs);
			close(st);
			close(con);
		}
		
		return temp;
	}
	
	public List<Product> listProducts() throws ClassNotFoundException {
		List<Product> temp = new ArrayList<>();
		Product b = null;
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			st = con.createStatement();
			String sql = "select * from product";
			rs = st.executeQuery(sql);
			while(rs.next()) {
				b = new Product(rs.getString(1),rs.getString(2),rs.getInt(3));
				temp.add(b);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rs);
			close(st);
			close(con);
		}
		
		return temp;
	}
}
