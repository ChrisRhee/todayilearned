package com.ssafy;

import java.util.List;

public class Test {
	
	private static void printAllProducts(List<Product> list) {
		for(int i=0; i<list.size(); i++) {
			System.out.println(list.get(i));
		}
	}
	
	public static void main(String[] args) throws ClassNotFoundException {
		ProductDAO dao = new ProductDAO();
		System.out.println("----- findBook ----- ");
		System.out.println(dao.findProduct("1001"));
		System.out.println("\n----- printAllBooks ----- ");
		printAllProducts(dao.listProducts());
		
		System.out.println("\n----- Insert Product 1002 죠스바 ----- ");
		dao.insertProduct(new Product("1002","죠스바", 600));
		printAllProducts(dao.listProducts());
		
		System.out.println("\n----- 1002 deleted ----- ");
		dao.deleteProduct("1002");
		printAllProducts(dao.listProducts());

		System.out.println("\n----- Update Product 죠스바 -> 죠스를 찾아서 ----- ");
		dao.insertProduct(new Product("1002","죠스바", 600));
		dao.updateProduct(new Product("1002","죠스를 찾아서", 15000));
		printAllProducts(dao.listProducts());
		
		System.out.println("\n----- count() ----- ");
		System.out.println(dao.count());
	}
}
