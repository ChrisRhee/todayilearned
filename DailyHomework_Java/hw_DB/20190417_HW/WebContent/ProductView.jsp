<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>상품 정보</title>
</head>
<body>
	<h1>상품 정보</h1>
	<form method="post" action="ProductModify.do">
		<div>
			상품 번호 <input type="text" name="code" value="${product.getCode()}" readonly>
		</div>
		<div>
			상품 이름 <input type="text" name="name" value="${product.getName()}">
		</div>
		<div>
			상품 가격 <input type="number" min='0' name="price" value="${product.getPrice()}">
		</div>
		
		<div>
			<input type="submit" value="수정">
			<input type="button" value="삭제" onclick="Pdelete()">
			<script type="text/javascript">
				function Pdelete(){
					location.href = "ProductDeleteAction.do?code=${product.code}";
				}	
			</script>
		</div>
	</form>
</body>
</html>


