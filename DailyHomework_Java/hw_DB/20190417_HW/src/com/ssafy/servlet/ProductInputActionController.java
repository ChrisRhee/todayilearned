package com.ssafy.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ssafy.Product;
import com.ssafy.ProductService;
import com.ssafy.ProductServiceImpl;

public class ProductInputActionController implements Controller {
	ProductService ps = new ProductServiceImpl();
	
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = request.getParameter("code");
		String name = request.getParameter("name");
		String price = request.getParameter("price");
		
		try {
			ps.insertProduct(new Product(code,name,Integer.parseInt(price)));
			request.setAttribute("msg","입력");
			request.getRequestDispatcher("/Result.jsp").forward(request, response);
		} catch (Exception e) {
			request.setAttribute("msg", e.getMessage());
			request.getRequestDispatcher("/Error.jsp").forward(request, response);
		}
	}

}
