package com.ssafy.servlet;

import java.io.IOException;
import java.util.*;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

/**
 * Servlet implementation class ProductServlet
 */
@WebServlet("*.do")
public class ProductServlet extends HttpServlet {
	
	private Map<String, Controller> clist;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config); // WAS제공되는 config 유지
		clist= new HashMap<>();
		clist.put("/ProductInputUI.do", new ProductInputUIController() );
		clist.put("/ProductInputAction.do", new ProductInputActionController());
		clist.put("/ProductList.do", new ProductListController());
		clist.put("/ProductDeleteAction.do", new ProductDeleteActionController());
		clist.put("/ProductView.do", new ProductViewActionController());
		clist.put("/ProductModify.do", new ProductModifyController());
		
	}
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; Charset=utf-8");
		String cmd = request.getRequestURI().substring(request.getContextPath().length());
		System.out.println(cmd);
		System.out.println("frontControllerServlet");
		clist.get(cmd).excute(request, response);
	}
}
