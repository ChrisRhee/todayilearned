package com.ssafy.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.ssafy.*;

public class ProductDeleteActionController implements Controller {
	ProductService ps = new ProductServiceImpl();
	
	@Override
	public void excute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = request.getParameter("code");
		List<Product> products;
		try {
			ps.deleteProduct(code);
			products = ps.listProducts();
			request.setAttribute("products",products);
			request.getRequestDispatcher("/ProductList.jsp").forward(request, response);
		} catch (Exception e) {
			request.setAttribute("msg", e.getMessage());
			request.getRequestDispatcher("/Error.jsp").forward(request, response);
		}
	}

}
