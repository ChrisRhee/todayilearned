package com.ssafy;

public class ProductException extends Exception {
	public ProductException(String msg) {
		super(msg);
	}
}
