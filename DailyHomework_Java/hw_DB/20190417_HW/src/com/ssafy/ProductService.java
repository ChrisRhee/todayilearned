package com.ssafy;

import java.util.List;

public interface ProductService {
	public int count()  throws ProductException ;
	public void insertProduct(Product product)  throws ProductException ;
	public void updateProduct(Product product)  throws ProductException ;
	public void deleteProduct(String code)  throws ProductException ;
	public Product findProduct(String code)  throws ProductException ;
	public List<Product> listProducts()  throws ProductException ;
}
