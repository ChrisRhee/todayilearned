package com.ssafy;

import java.util.*;

public class ProductServiceImpl implements ProductService {
	ProductDAO dao = new ProductDAOImpl();

	@Override
	public int count() throws ProductException {
		int cnt=0;
		try {
			cnt=dao.count();
		}catch (Exception e) {
			throw new ProductException("클래스를 찾을 수 없습니다");
		}
		return cnt;
	}
	
	public void deleteProduct(String code) throws ProductException{
		try {
			dao.deleteProduct(code);
		}catch (Exception e1) {
			throw new ProductException("물품 삭제 중 오류가 발생했습니다.");
		}
	}

	@Override
	public void insertProduct(Product product) throws ProductException {
		try {
			dao.insertProduct(product);
		} catch (Exception e) {
			throw new ProductException("물품 입력 중 오류가 발생했습니다.");
		}
		
	}

	@Override
	public void updateProduct(Product product) throws ProductException {
		try {
			dao.updateProduct(product);
		} catch (Exception e) {
			throw new ProductException("물품 수정 중 오류가 발생했습니다.");
		}
	}

	@Override
	public Product findProduct(String code) throws ProductException {
		Product product = new Product();
		try {
			product = dao.findProduct(code);
		} catch (Exception e) {
			throw new ProductException("물품 검색 중 오류가 발생했습니다.");
		}
		return product;
	}

	@Override
	public List<Product> listProducts() throws ProductException {
		List<Product> list = new ArrayList<Product>();
		try {
			list = dao.listProducts();
		} catch (Exception e) {
			throw new ProductException("물품 출력 중 오류가 발생했습니다.");
		}
		
		return list;
	}
}