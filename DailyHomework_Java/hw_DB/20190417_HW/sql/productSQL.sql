drop table if exists product;
create table product(
 code char(8) primary key,
 name varchar(50) not null,
 price int not null
);

# 2. 상품 데이터 5개이상 저장
insert into product (code, name, price) 
values('1002', '박규빈의 닭가슴살',120000);
insert into product (code, name, price) 
values('1001', '이찬호의 컴퓨터',1200000);
insert into product (code, name, price) 
values('3001', '원호명의 아이폰',700000);
insert into product (code, name, price) 
values('4002', '고르마의 TV', 400000);
insert into product (code, name, price) 
values('4001', '정재욱의 TV',230000);
insert into product (code, name, price) 
values('4003', '삼성 TV',500000);