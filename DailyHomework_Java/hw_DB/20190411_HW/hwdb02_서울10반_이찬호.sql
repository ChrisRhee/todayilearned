# 1. 상품정보 설계
drop table if exists product;
create table product(
 code char(8) primary key,
 name varchar(50) not null,
 price int not null
);

# 2. 상품 데이터 5개이상 저장
insert into product (code, name, price) 
values('1002', '박규빈의 닭가슴살',120000);
insert into product (code, name, price) 
values('1001', '이찬호의 컴퓨터',1200000);
insert into product (code, name, price) 
values('3001', '원호명의 아이폰',700000);
insert into product (code, name, price) 
values('4002', '고르마의 TV', 400000);
insert into product (code, name, price) 
values('4001', '정재욱의 TV',230000);
insert into product (code, name, price) 
values('4003', '삼성 TV',500000);

# 3. 15인하된 가격의 상품 정보를 출력하세요.
select price*85/100
from product;

# 4. tc관련 상품을 가격 20 인하하여 저장하세요 그 결과를 출력하세요.
update product
set price = price*80/100
where name like '%TV%';
select * from product;

# 5. 저장된 상품 가격의 총 금액을 출력하는 문장 작성 
select sum(price) from product;