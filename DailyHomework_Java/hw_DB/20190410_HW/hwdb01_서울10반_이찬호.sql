--  1) 입사일이 2104년도인 사람의 모든 정도
select *
from emp
where year(hiredate)= 2014;

-- 2) 이름 중 S자가 들어가 있는 사람만 모든 정보
select *
from emp
where ename like '%s%';

-- 3) 커미션이 null
select *
from emp
where comm is null;

-- 4) 부서별 급여의 평균 최고 최저 인원수
select deptno, avg(sal), max(sal), min(sal), count(*)
from emp
group by deptno;

-- 5) 30번 부서의 연봉을 계산 이름, 부서번호, 급여, 연봉(12개월 월급+연말보너스), 연말보너스 = 연말에 급여의 150%지급
select ename 이름, deptno 부서번호 , sal 급여, sal*12+sal*150/100 연봉, sal*150/100 연말보너스
from emp;

-- 6) 급여가 $2,000 이상인 모든 사람은 급여의 15%를 경조비로 내기. 이름, 급여, 경조비 소수점이하 절삭
select ename 이름, sal 급여, truncate(sal*15/100,0) 경조비
from emp
where sal >= 2000;

-- 7) 각부서별 평균월급, 전체 월급, 최고월급, 최저월급을 구하여 평균 월급이 많은 순으로 검색
select avg(sal) 평균월급, sum(sal) 전체월급, max(sal) 최고월급, min(sal) 최저월급
from emp
group by deptno
order by avg(sal);


CREATE TABLE EMP(
 EMPNO INT(4) PRIMARY KEY,
 ENAME VARCHAR(10),
 JOB VARCHAR(9),
 MGR INT(4),
 HIREDATE DATE,
 SAL DECIMAL(7,2),
 COMM DECIMAL(7,2),
 DEPTNO INT(2)
);

INSERT INTO EMP VALUES (7369,'SMITH','CLERK',7902,cast('2010-10-19' as date),800,NULL,20);
INSERT INTO EMP VALUES (7499,'ALLEN','SALESMAN',7698,cast('2000-10-19' as date),1600,300,30);
INSERT INTO EMP VALUES (7521,'WARD','SALESMAN',7698,cast('2013-05-19' as date),1250,500,30);
INSERT INTO EMP VALUES (7566,'JONES','MANAGER',7839,cast('2018-10-19' as date),2975,NULL,20);
INSERT INTO EMP VALUES (7654,'MARTIN','SALESMAN',7698,cast('2008-04-19' as date),1250,1400,30);
INSERT INTO EMP VALUES (7698,'BLAKE','MANAGER',7839,cast('2016-11-19' as date),2850,NULL,30);
INSERT INTO EMP VALUES (7782,'CLARK','MANAGER',7839,cast('2017-10-19' as date),2450,NULL,10);
INSERT INTO EMP VALUES (7788,'SCOTT','ANALYST',7566,cast('2013-10-11' as date),3000,NULL,20);
INSERT INTO EMP VALUES (7839,'KING','PRESIDENT',NULL,cast('2014-08-19' as date),5000,NULL,10);
INSERT INTO EMP VALUES (7844,'TURNER','SALESMAN',7698,cast('2010-10-19' as date),1500,0,30);
INSERT INTO EMP VALUES (7876,'ADAMS','CLERK',7788,cast('2000-09-19' as date),1100,NULL,20);
INSERT INTO EMP VALUES (7900,'JAMES','CLERK',7698,cast('2018-10-19' as date),950,NULL,30);
INSERT INTO EMP VALUES (7902,'FORD','ANALYST',7566,cast('2003-10-19' as date),3000,NULL,20);
INSERT INTO EMP VALUES (7934,'MILLER','CLERK',7782,cast('2002-10-19' as date),1300,NULL,10);