DROP TABLE IF EXISTS BONUS;
DROP TABLE IF EXISTS SALGRADE;
DROP TABLE IF EXISTS EMP;
DROP TABLE IF EXISTS DEPT;

CREATE TABLE DEPT(
 DEPTNO INT(2) PRIMARY KEY,
 DNAME VARCHAR(14),
 LOC VARCHAR(13)
);

CREATE TABLE EMP(
 EMPNO INT(4) PRIMARY KEY,
 ENAME VARCHAR(10),
 JOB VARCHAR(10),
 MGR INT(4),
 HIREDATE DATE,
 SAL DECIMAL(7,2),
 COMM DECIMAL(7,2),
 DEPTNO INT(2),
 foreign key (DEPTNO) REFERENCES DEPT(DEPTNO)
);


INSERT INTO DEPT VALUES (10,'ACCOUNTING','NEW YORK');
INSERT INTO DEPT VALUES (20,'RESEARCH','DALLAS');
INSERT INTO DEPT VALUES (30,'SALES','CHICAGO');
INSERT INTO DEPT VALUES (40,'OPERATIONS','BOSTON');

INSERT INTO EMP VALUES (7369,'SMITH','CLERK',7902,cast('2010-10-19' as date),800,NULL,20);
INSERT INTO EMP VALUES (7499,'ALLEN','SALESMAN',7698,cast('2000-10-19' as date),1600,300,30);
INSERT INTO EMP VALUES (7521,'WARD','SALESMAN',7698,cast('2013-05-19' as date),1250,500,30);
INSERT INTO EMP VALUES (7566,'JONES','MANAGER',7839,cast('2018-10-19' as date),2975,NULL,20);
INSERT INTO EMP VALUES (7654,'MARTIN','SALESMAN',7698,cast('2008-04-19' as date),1250,1400,30);
INSERT INTO EMP VALUES (7698,'BLAKE','MANAGER',7839,cast('2016-11-19' as date),2850,NULL,30);
INSERT INTO EMP VALUES (7782,'CLARK','MANAGER',7839,cast('2017-10-19' as date),2450,NULL,10);
INSERT INTO EMP VALUES (7788,'SCOTT','ANALYST',7566,cast('2013-10-11' as date),3000,NULL,20);
INSERT INTO EMP VALUES (7839,'KING','PRESIDENT',NULL,cast('2014-08-19' as date),5000,NULL,10);
INSERT INTO EMP VALUES (7844,'TURNER','SALESMAN',7698,cast('2010-10-19' as date),1500,0,30);
INSERT INTO EMP VALUES (7876,'ADAMS','CLERK',7788,cast('2000-09-19' as date),1100,NULL,20);
INSERT INTO EMP VALUES (7900,'JAMES','CLERK',7698,cast('2018-10-19' as date),950,NULL,30);
INSERT INTO EMP VALUES (7902,'FORD','ANALYST',7566,cast('2003-10-19' as date),3000,NULL,20);
INSERT INTO EMP VALUES (7934,'MILLER','CLERK',7782,cast('2002-10-19' as date),1300,NULL,10);


# 1. 부서위치 chicago 모든 사원 이름 업무 급여 출력
select ename 이름, JOB 업무, sal 급여
from emp
where deptno in (select deptno from dept where loc = 'CHICAGO');

# 2. 부하직원 없는 사원의 사원 번호
select empno 사원번호, ename 이름, JOB 업무, deptno 부서번호
from emp
where empno not in (select distinct mgr from emp where mgr is not null);

# 3. blake 와 같은 상사를 가진 사원의 이름, 업무 상사번호
select  ename 이름, JOB 업무, mgr 상사번호
from emp
where mgr in (select mgr from emp where ename = 'BLAKE');

# 4. 입사일 제일 오래된 사람 5명
select ename, hiredate
from emp
order by HIREDATE
limit 5;

select ename, datediff(curdate(), hiredate) as '입사일수'
from emp
order by hiredate
limit 5;

# 5. jones의 부하 직원의 이름 업무 부서명
select emp.ename 이름, emp.JOB 업무, dept.DNAME 부서명
from emp join dept
on emp.DEPTNO = dept.DEPTNO
and emp.mgr in (select empno from emp where ename = 'JONES');

